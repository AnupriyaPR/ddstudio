package com.wiinnova.doctorsdiary.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import com.wiinnova.doctorsdiary.R;
import com.wiinnova.doctorsdiary.R.id;
import com.wiinnova.doctorsdiary.R.layout;
import com.wiinnova.doctorsdiary.R.string;

public class StartupActivity extends Activity{

	Button btnExit;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		if (isTablet()) {
		    startActivity(new Intent(StartupActivity.this, SplashScreen_Activity.class));
		    this.finish(); // don't forget to kill startup activity after starting main activity.
		} else {
		    setContentView(R.layout.activity_startup);
		    btnExit=(Button)findViewById(R.id.button1);
		    btnExit.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					finish();
				}
			});
		}
	}
	private boolean isTablet() {
	    if (Boolean.parseBoolean(getResources().getString(R.string.is_supported_screen))) {
	        return true;
	    } else {
	        return false;
	    }
	}
}
