package com.wiinnova.doctorsdiary.activities;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.zip.Checksum;
import java.util.zip.Inflater;

import com.wiinnova.doctorsdiary.fragment.Patient_Current_Medical_Info_Frag;
import com.wiinnova.doctorsdiary.fragments.Diagnosis;
import com.wiinnova.doctorsdiary.fragments.DiagnosisExamination;
import com.wiinnova.doctorsdiary.fragments.DiagnosisSideMenu;
import com.wiinnova.doctorsdiary.fragments.PatientMedicalaHistory;
import com.wiinnova.doctorsdiary.fragments.PatientpersonalInfo;

import com.wiinnova.doctorsdiary.R;
import com.parse.ParseObject;
import com.wiinnova.doctorsdiary.fragment.CurrentVitals;
import com.wiinnova.doctorsdiary.fragment.DD_Account_Settings_Frag;
import com.wiinnova.doctorsdiary.fragment.DD_Frag;
import com.wiinnova.doctorsdiary.fragment.DiagnosisCreate_Gynacologist;
import com.wiinnova.doctorsdiary.fragment.Diagnosis_Create_Frag;
import com.wiinnova.doctorsdiary.fragment.Diagnosis_Frag;
import com.wiinnova.doctorsdiary.fragment.Diagnosis_Frag_Gynacologist;
import com.wiinnova.doctorsdiary.fragment.Diagnosis_View_Frag;
import com.wiinnova.doctorsdiary.fragment.Diagnosisexamination_Gynacologist;
import com.wiinnova.doctorsdiary.fragment.Obstetrics_gynacologist;
import com.wiinnova.doctorsdiary.fragment.PatientCurrentMedicalaInformation_Gynacologist;
import com.wiinnova.doctorsdiary.fragment.Patient_Frag;
import com.wiinnova.doctorsdiary.fragment.Patient_Medical_History_First_Frag;
import com.wiinnova.doctorsdiary.fragment.Patient_Personal_Info_Frag;
import com.wiinnova.doctorsdiary.fragment.Patientpersonalinfo_Gynacoliogist;
import com.wiinnova.doctorsdiary.fragment.Prescriptionmanagement_Gynacologist;
import com.wiinnova.doctorsdiary.fragment.Treatment_Create_Frag;
import com.wiinnova.doctorsdiary.fragment.Treatment_Frag;
import com.wiinnova.doctorsdiary.fragment.SearchPatientresult.onSearchSubmit;
import com.wiinnova.doctorsdiary.fragment.Treatment_View_Frag;
import com.wiinnova.doctorsdiary.popupwindow.OldPatientId_Popupfragment.patientid;
import com.wiinnova.doctorsdiary.supportclasses.Diseasenameclass;

import android.R.bool;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


public class FragmentActivityContainer extends FragmentActivity {

    //Patient Tab items
    int flag;
    private ImageView adminimage;
    private FrameLayout flSideMenu;
    private FrameLayout flFragmentPage;
    private LinearLayout homebutton;
    private LinearLayout patienttab;
    private LinearLayout diagnosistab;
    private LinearLayout prescriptiontab;
    private LinearLayout admintab;

    private LinearLayout patienttabitem1;
    private LinearLayout diagnosistabitem1;
    private LinearLayout prescriptiontabitem1;
    private LinearLayout admintabitem1;

    private LinearLayout patienttabitem2;
    private LinearLayout diagnosistabitem2;
    private LinearLayout prescriptiontabitem2;
    private LinearLayout admintabitem2;

    private TextView tvpatient;
    private TextView tvdiagnosis;
    private TextView tvprescription;


    private String diagnosisobject_id;

    public String getDiagnosisobject_id() {
        return diagnosisobject_id;
    }

    public void setDiagnosisobject_id(String diagnosisobject_id) {
        this.diagnosisobject_id = diagnosisobject_id;
    }

    Fragment menuObj;
    private Fragment currentfragment;
    int activationcode = 0;
    String pid;
    Boolean patientflag;
    private int adminloadflag;


    private Patient_Personal_Info_Frag pntprsnlinfo;
    private Patient_Medical_History_First_Frag pnt_currmed_info;
    private CurrentVitals pnt_curr_medinfo;
    private PatientpersonalInfo pntprsnlinfo_nephro;
    private Patientpersonalinfo_Gynacoliogist pntprsnlinfo_gynacologist;
    private PatientCurrentMedicalaInformation_Gynacologist patientcurrentmedinfo_gynacologist;
    private PatientMedicalaHistory patientMedicalHistory_nephro;
    private Obstetrics_gynacologist patientobstritics;


    private boolean docteredit_patientdetails = false;
    private boolean patientedit_checkflag = true;
    private boolean disablesidemenuitem = false;
    public Patient_Frag patientfrag;
    private boolean oldpatient_currentupdate_check;
    private boolean oldpatient_currentvitals_update_check;
    private boolean patienteditflag = true;
    private int adminloadvalue;
    private boolean doubleBackToExitPressedOnce;

    private FragmentManager fragmentManager;
    private FragmentTransaction fragmentTransaction;
    private ParseObject patientobject;
    private ParseObject Oldpatientobject;
    private ParseObject searchpatientobject;
    private String currentvitalsObjectid;


    public static int check_save = 0;

    public boolean check_admintab;


    //Diagnosis Tab Items
    private Diagnosis_Frag diagfrag;
    private Diagnosis_Create_Frag creatediag;
    Diagnosis_View_Frag viewdiag;
    DiagnosisCreate_Gynacologist creatediag_gynacologist;
    Diagnosis creatediag_nephro;

    private Diagnosis_Frag_Gynacologist diagfrag_gynacologist;
    private DiagnosisSideMenu diagfrag_nephro;


    private Diagnosisexamination_Gynacologist diagexam_gynacologist;
    private DiagnosisExamination diagexam_nephro;


    public static int diag_save_check = 0;


    ParseObject patientdiagnosisparseObj;
    ParseObject patientdiagnosisexamparseObj;
    ArrayList<Diseasenameclass> diseasenamefromparsearraylist = new ArrayList<Diseasenameclass>();

    public ParseObject getpatientdiagnosisparseObj() {
        return patientdiagnosisparseObj;
    }

    public void setpatientdiagnosisparseObj(ParseObject patientdiagnosisparseobject) {
        this.patientdiagnosisparseObj = patientdiagnosisparseobject;
    }

    public ParseObject getdiagnosisexaminationparseObj() {
        return patientdiagnosisexamparseObj;
    }

    public void setdiagnosisexaminationparseObj(ParseObject parseobject) {
        this.patientdiagnosisexamparseObj = parseobject;
    }

    //Prescription Tab Items
    private Treatment_Frag treatementfrag;
    private Treatment_Create_Frag createtrat;
    private Treatment_View_Frag treatmentviewfrag;
    private Prescriptionmanagement_Gynacologist treatmentmanagement_gynacologist;


    private ParseObject patienttreatmentparseObj;
    private ParseObject treatmentmanagementparseObj;


    public boolean datasaved = false;
    private ProgressDialog mProgressDialog;
    public static int treat_save_check = 0;
    public static int treatmangement_save_check = 0;

    public void setpatienttreatmentparseObj(ParseObject patienttreatmentparseObj) {
        this.patienttreatmentparseObj = patienttreatmentparseObj;
    }

    public ParseObject getpatienttreatmentparseObj() {
        return patienttreatmentparseObj;
    }


    public ParseObject getTreatmentmanagementparseObj() {
        return treatmentmanagementparseObj;
    }

    public void setTreatmentmanagementparseObj(
            ParseObject treatmentmanagementparseObj) {
        this.treatmentmanagementparseObj = treatmentmanagementparseObj;
    }


    public ProgressDialog setprogressbar() {
        mProgressDialog = new ProgressDialog(FragmentActivityContainer.this);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setMessage("Storing Diagnosis Data....");
        return mProgressDialog;
    }


    //Admin Tab Item

    private DD_Frag ddfirst;
    DD_Account_Settings_Frag ddsecond;
    List<ParseObject> patientsearchresultObj;
    private onSearchSubmit listener;
    public Button btnSavebutton;
    private boolean admin_default;
    private String specialization;
    private int thisYear;
    private String[] year_array;


    @Override
    protected void onCreate(Bundle SavedInstancestate) {
        // TODO Auto-generated method stub
        super.onCreate(SavedInstancestate);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.tab_container);


        Calendar calendar = Calendar.getInstance();
        thisYear = calendar.get(Calendar.YEAR);

        int size = thisYear - 1950;
        year_array = new String[size + 1];
        for (int i = 1950; i <= thisYear; i++) {
            year_array[i - 1950] = Integer.toString(i);
        }


        SharedPreferences sp = getSharedPreferences("Login", 0);
        specialization = sp.getString("spel", null);

        Bundle i = getIntent().getExtras();
        if (i != null) {
            pid = i.getString("patientid");
            patientflag = i.getBoolean("existingflag");
            adminloadflag = i.getInt("adminloadflagvalue");
        }

        System.out.println("adminloadflag" + adminloadflag);

        if (Home_Page_Activity.patient != null) {
            patientobject = Home_Page_Activity.patient;
            System.out.println("existing obj assigning working");
        }
        if (searchpatientobject != null) {
            System.out.println("search patient obj not null");
            patientobject = searchpatientobject;
        }


        btnSavebutton = (Button) findViewById(R.id.btnSubmit);
        flSideMenu = (FrameLayout) findViewById(R.id.fl_sidemenu_container);
        flFragmentPage = (FrameLayout) findViewById(R.id.fl_fragment_container);

        homebutton = (LinearLayout) findViewById(R.id.ll_homebutton);
        patienttab = (LinearLayout) findViewById(R.id.ll_patienttab);
        diagnosistab = (LinearLayout) findViewById(R.id.ll_diagnosistab);
        prescriptiontab = (LinearLayout) findViewById(R.id.ll_prescriptiontab);
        admintab = (LinearLayout) findViewById(R.id.ll_admintab);

        patienttabitem1 = (LinearLayout) findViewById(R.id.ll_patienttab1);
        diagnosistabitem1 = (LinearLayout) findViewById(R.id.ll_diagnosistab1);
        prescriptiontabitem1 = (LinearLayout) findViewById(R.id.ll_prescriptiontab1);
        admintabitem1 = (LinearLayout) findViewById(R.id.ll_admintab1);

        patienttabitem2 = (LinearLayout) findViewById(R.id.llbelowtab1);
        diagnosistabitem2 = (LinearLayout) findViewById(R.id.llbelowtab2);
        prescriptiontabitem2 = (LinearLayout) findViewById(R.id.llbelowtab3);
        admintabitem2 = (LinearLayout) findViewById(R.id.llbelowtab4);

        adminimage = (ImageView) findViewById(R.id.imageadmin);
        tvpatient = (TextView) findViewById(R.id.patienttext);
        tvdiagnosis = (TextView) findViewById(R.id.diagnosistext);
        tvprescription = (TextView) findViewById(R.id.prescriptiontext);


        if (adminloadflag == 1) {
            admintabloaddefault();
            admin_default = true;

        } else {
            patienttabloaddefaultfromhome();
        }

        homebutton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub

                Home_Page_Activity.patient = null;
                finish();


            }
        });

        patienttab.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                patienttabcolorchange();
                check_admintab = false;
                patientedit_checkflag = true;

                getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);

                if (specialization.equalsIgnoreCase("gynecologist obstetrician")) {
                    if (diag_save_check > 0) {
                        AlertDialog.Builder builder1 = new AlertDialog.Builder(FragmentActivityContainer.this);
                        builder1.setTitle("SAVE ITEM");
                        builder1.setMessage("Do you want save these items");
                        builder1.setCancelable(true);
                        builder1.setPositiveButton("Yes",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        System.out.println("working med");
                                        System.out.println("getFragmentManager().findFragmentById(R.id.fl_fragment_container)" + getFragmentManager().findFragmentById(R.id.fl_fragment_container));

                                        Diagnosis_Frag_Gynacologist.saveData(3);

                                        dialog.cancel();
                                    }
                                });
                        builder1.setNegativeButton("No",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        FragmentActivityContainer.diag_save_check = 0;
                                        Patientpersonalinfo_Gynacoliogist pntprsnlinfo_gynacologist = new Patientpersonalinfo_Gynacoliogist();
                                        Bundle bundle2 = new Bundle();
                                        bundle2.putBoolean("admintab", false);
                                        bundle2.putBoolean("patientflag", patienteditflag);
                                        pntprsnlinfo_gynacologist.setArguments(bundle2);
                                        getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                                        loadFragmentWithBackStack(pntprsnlinfo_gynacologist);
                                    }
                                });

                        AlertDialog alert11 = builder1.create();
                        alert11.show();
                    } else {
                        pntprsnlinfo_gynacologist = new Patientpersonalinfo_Gynacoliogist();

                        Bundle bundle2 = new Bundle();
                        bundle2.putBoolean("admintab", false);
                        bundle2.putBoolean("patientflag", patienteditflag);
                        pntprsnlinfo_gynacologist.setArguments(bundle2);

                        getFragmentManager().beginTransaction()
                                .replace(R.id.fl_fragment_container, pntprsnlinfo_gynacologist)
                                .commit();
                    }


                } else if (specialization.equalsIgnoreCase("Nephrologist")) {

                    pntprsnlinfo_nephro = new PatientpersonalInfo();

                    Bundle bundle2 = new Bundle();
                    bundle2.putBoolean("admintab", false);
                    bundle2.putBoolean("patientflag", patienteditflag);
                    pntprsnlinfo_nephro.setArguments(bundle2);

                    getFragmentManager().beginTransaction()
                            .replace(R.id.fl_fragment_container, pntprsnlinfo_nephro)
                            .commit();

                } else {

                    pntprsnlinfo = new Patient_Personal_Info_Frag();

                    Bundle bundle2 = new Bundle();
                    bundle2.putBoolean("admintab", false);
                    bundle2.putBoolean("patientflag", patienteditflag);
                    pntprsnlinfo.setArguments(bundle2);

                    getFragmentManager().beginTransaction()
                            .replace(R.id.fl_fragment_container, pntprsnlinfo)
                            .commit();

                }
            }


        });

        diagnosistab.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                diagnosistabcolorchange();
                check_admintab = false;
                patientedit_checkflag = true;

                if (specialization.equalsIgnoreCase("gynecologist obstetrician")) {

                    if (FragmentActivityContainer.check_save == 0) {

                        diagexam_gynacologist = new Diagnosisexamination_Gynacologist();

                        getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                        loadFragmentWithBackStack(diagexam_gynacologist);
                    } else {
                        {


                            AlertDialog.Builder builder1 = new AlertDialog.Builder(FragmentActivityContainer.this);
                            builder1.setTitle("SAVE ITEM");
                            builder1.setMessage("Do you want save these items");
                            builder1.setCancelable(true);
                            builder1.setPositiveButton("Yes",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            System.out.println("working med");
                                            System.out.println("getFragmentManager().findFragmentById(R.id.fl_fragment_container)" + getFragmentManager().findFragmentById(R.id.fl_fragment_container));
                                            if (getFragmentManager().findFragmentById(R.id.fl_fragment_container) instanceof Patientpersonalinfo_Gynacoliogist) {
                                                ((FragmentActivityContainer) FragmentActivityContainer.this).getPntprsnlinfo_gynacologist().onPersonal(1, 6);
                                            } else if (getFragmentManager().findFragmentById(R.id.fl_fragment_container) instanceof PatientCurrentMedicalaInformation_Gynacologist) {
                                                ((FragmentActivityContainer) FragmentActivityContainer.this).getPatientcurrentmedinfo_gynacologist().onMedical(1, 6);
                                            } else if (getFragmentManager().findFragmentById(R.id.fl_fragment_container) instanceof Obstetrics_gynacologist) {
                                                ((FragmentActivityContainer) FragmentActivityContainer.this).getPatientObstritics().onObstritics(1, 6);
                                            }
                                            dialog.cancel();
                                        }
                                    });
                            builder1.setNegativeButton("No",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            FragmentActivityContainer.check_save = 0;
                                            diagexam_gynacologist = new Diagnosisexamination_Gynacologist();

                                            getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                                            loadFragmentWithBackStack(diagexam_gynacologist);
                                        }
                                    });

                            AlertDialog alert11 = builder1.create();
                            alert11.show();

                        }
                    }
                } else if (specialization.equalsIgnoreCase("Nephrologist")) {


                    diagexam_nephro = new DiagnosisExamination();

                    getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    loadFragmentWithBackStack(diagexam_nephro);
                } else {

                    creatediag = new Diagnosis_Create_Frag();

                    getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    loadFragmentWithBackStack(creatediag);
                }


            }

        });

        prescriptiontab.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub

                prescriptiontabcolorchange();
                check_admintab = false;
                patientedit_checkflag = true;

                if (specialization.equalsIgnoreCase("gynecologist obstetrician")) {
                    if (check_save == 0) {
                        treatmentmanagement_gynacologist = new Prescriptionmanagement_Gynacologist();

                        getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                        loadFragmentWithBackStack(treatmentmanagement_gynacologist);
                    } else {
                        {


                            AlertDialog.Builder builder1 = new AlertDialog.Builder(FragmentActivityContainer.this);
                            builder1.setTitle("SAVE ITEM");
                            builder1.setMessage("Do you want save these items");
                            builder1.setCancelable(true);
                            builder1.setPositiveButton("Yes",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            System.out.println("working med");
                                            System.out.println("getFragmentManager().findFragmentById(R.id.fl_fragment_container)" + getFragmentManager().findFragmentById(R.id.fl_fragment_container));
                                            if (getFragmentManager().findFragmentById(R.id.fl_fragment_container) instanceof Patientpersonalinfo_Gynacoliogist) {
                                                ((FragmentActivityContainer) FragmentActivityContainer.this).getPntprsnlinfo_gynacologist().onPersonal(1, 7);
                                            } else if (getFragmentManager().findFragmentById(R.id.fl_fragment_container) instanceof PatientCurrentMedicalaInformation_Gynacologist) {
                                                ((FragmentActivityContainer) FragmentActivityContainer.this).getPatientcurrentmedinfo_gynacologist().onMedical(1, 7);
                                            } else if (getFragmentManager().findFragmentById(R.id.fl_fragment_container) instanceof Obstetrics_gynacologist) {
                                                ((FragmentActivityContainer) FragmentActivityContainer.this).getPatientObstritics().onObstritics(1, 7);
                                            }
                                            dialog.cancel();
                                        }
                                    });
                            builder1.setNegativeButton("No",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            FragmentActivityContainer.check_save = 0;
                                            treatmentmanagement_gynacologist = new Prescriptionmanagement_Gynacologist();

                                            getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                                            loadFragmentWithBackStack(treatmentmanagement_gynacologist);
                                        }
                                    });

                            AlertDialog alert11 = builder1.create();
                            alert11.show();

                        }
                    }

                } else {


                    createtrat = new Treatment_Create_Frag();
                    getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    loadFragmentWithBackStack(createtrat);
                }

            }


        });

        admintab.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub

                admintabcolorchange();
                //check_admintab=false;

                System.out.println("admin tab load working......");

                ddsecond = new DD_Account_Settings_Frag();
                getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);

                loadFragmentWithBackStack(ddsecond);


            }


        });

    }

    public void admintabloaddefault() {

        System.out.println("admin tab load default working......");


        patienttabitem1.setBackgroundColor(getResources().getColor(R.color.white));
        patienttabitem2.setBackgroundColor(Color.TRANSPARENT);
        diagnosistabitem1.setBackgroundColor(getResources().getColor(R.color.white));
        prescriptiontabitem1.setBackgroundColor(getResources().getColor(R.color.white));
        adminimage.setImageResource(R.drawable.logo_white);


        admintabitem1.setBackgroundColor(getResources().getColor(R.color.red_color));
        diagnosistabitem2.setBackgroundColor(Color.TRANSPARENT);
        prescriptiontabitem2.setBackgroundColor(Color.TRANSPARENT);
        admintabitem2.setBackgroundColor(getResources().getColor(R.color.red_color));


        tvpatient.setTextColor(getResources().getColor(R.color.dark_blue_color));
        tvdiagnosis.setTextColor(getResources().getColor(R.color.dark_blue_color));
        tvprescription.setTextColor(getResources().getColor(R.color.dark_blue_color));


        ddsecond = new DD_Account_Settings_Frag();

        loadFragment(ddsecond);
    }

    public void patienttabloaddefault() {
        // TODO Auto-generated method stub

        patienttab.setVisibility(View.VISIBLE);
        /*patienttab.setClickable(false);*/
        /*diagnosistab.setVisibility(View.INVISIBLE);
        prescriptiontab.setVisibility(View.INVISIBLE);
		admintab.setVisibility(View.INVISIBLE);*/

        patienttabitem1.setBackgroundColor(getResources().getColor(R.color.red_color));
        patienttabitem2.setBackgroundColor(getResources().getColor(R.color.red_color));
        diagnosistabitem1.setBackgroundColor(getResources().getColor(R.color.white));
        prescriptiontabitem1.setBackgroundColor(getResources().getColor(R.color.white));
        admintabitem1.setBackgroundColor(getResources().getColor(R.color.white));
        adminimage.setImageResource(R.drawable.logo_2);

        diagnosistabitem2.setBackgroundColor(Color.TRANSPARENT);
        prescriptiontabitem2.setBackgroundColor(Color.TRANSPARENT);
        admintabitem2.setBackgroundColor(Color.TRANSPARENT);


        tvpatient.setTextColor(getResources().getColor(R.color.white));
        tvdiagnosis.setTextColor(getResources().getColor(R.color.dark_blue_color));
        tvprescription.setTextColor(getResources().getColor(R.color.dark_blue_color));

        check_save = 0;

        if (specialization.equalsIgnoreCase("gynecologist obstetrician")) {

            pntprsnlinfo_gynacologist = new Patientpersonalinfo_Gynacoliogist();

            Bundle bundle2 = new Bundle();
            bundle2.putBoolean("patientflag", patienteditflag);
            pntprsnlinfo_gynacologist.setArguments(bundle2);

            getFragmentManager().beginTransaction()
                    .replace(R.id.fl_fragment_container, pntprsnlinfo_gynacologist).addToBackStack(null)
                    .commit();

        } else {

            pntprsnlinfo = new Patient_Personal_Info_Frag();
            Bundle bundle2 = new Bundle();
            bundle2.putBoolean("patientflag", patienteditflag);
            pntprsnlinfo.setArguments(bundle2);

            getFragmentManager().beginTransaction()
                    .replace(R.id.fl_fragment_container, pntprsnlinfo)
                    .addToBackStack(null)
                    .commit();
        }
    }


    public void patienttabloaddefaultfromhome() {

        // TODO Auto-generated method stub

        patienttab.setClickable(true);

        patienttabitem1.setBackgroundColor(getResources().getColor(R.color.red_color));
        patienttabitem2.setBackgroundColor(getResources().getColor(R.color.red_color));
        diagnosistabitem1.setBackgroundColor(getResources().getColor(R.color.white));
        prescriptiontabitem1.setBackgroundColor(getResources().getColor(R.color.white));
        admintabitem1.setBackgroundColor(getResources().getColor(R.color.white));
        adminimage.setImageResource(R.drawable.logo_2);

        diagnosistabitem2.setBackgroundColor(Color.TRANSPARENT);
        prescriptiontabitem2.setBackgroundColor(Color.TRANSPARENT);
        admintabitem2.setBackgroundColor(Color.TRANSPARENT);


        tvpatient.setTextColor(getResources().getColor(R.color.white));
        tvdiagnosis.setTextColor(getResources().getColor(R.color.dark_blue_color));
        tvprescription.setTextColor(getResources().getColor(R.color.dark_blue_color));

        FragmentManager fm = getFragmentManager();


        if (specialization.equalsIgnoreCase("gynecologist obstetrician")) {

            pntprsnlinfo_gynacologist = new Patientpersonalinfo_Gynacoliogist();

            Bundle bundle2 = new Bundle();
            //bundle2.putBoolean("admintab", false);
            bundle2.putBoolean("patientflag", patienteditflag);
            pntprsnlinfo_gynacologist.setArguments(bundle2);

            getFragmentManager().beginTransaction()
                    .replace(R.id.fl_fragment_container, pntprsnlinfo_gynacologist)
                    .commit();

        } else if (specialization.equalsIgnoreCase("Nephrologist")) {

            pntprsnlinfo_nephro = new PatientpersonalInfo();

            Bundle bundle2 = new Bundle();
            //bundle2.putBoolean("admintab", false);
            bundle2.putBoolean("patientflag", patienteditflag);
            pntprsnlinfo_nephro.setArguments(bundle2);

            getFragmentManager().beginTransaction()
                    .replace(R.id.fl_fragment_container, pntprsnlinfo_nephro)
                    .commit();

        } else {

            pntprsnlinfo = new Patient_Personal_Info_Frag();
            Bundle bundle2 = new Bundle();
            //bundle2.putBoolean("admintab", false);
            bundle2.putBoolean("patientflag", patienteditflag);
            pntprsnlinfo.setArguments(bundle2);

            getFragmentManager().beginTransaction()
                    .replace(R.id.fl_fragment_container, pntprsnlinfo)
                    .commit();

        }
    }


    private void loadMenu(Fragment menu) {
        // TODO Auto-generated method stub
        if (!menu.equals(menuObj)) {
            menuObj = menu;
            getFragmentManager().beginTransaction()
                    .replace(R.id.fl_sidemenu_container, menu)
                    .commit();
        }
    }

    private void loadMenuwithbackstack(Fragment menu) {
        // TODO Auto-generated method stub
        if (!menu.equals(menuObj)) {
            menuObj = menu;
            getFragmentManager().beginTransaction()
                    .replace(R.id.fl_sidemenu_container, menu)
                    .addToBackStack(null)
                    .commit();
        }
    }


    public void loadFragment(Fragment fragment) {
        currentfragment = fragment;
        getFragmentManager().beginTransaction()
                .replace(R.id.fl_fragment_container, fragment)
                .commit();
    }

    public void loadFragmentWithBackStack(Fragment fragment) {
        currentfragment = fragment;
        getFragmentManager().beginTransaction()
                .replace(R.id.fl_fragment_container, fragment)
                .commit();
    }


    //Patient Tab Item get and Set Methods

    public ParseObject getPatientobject() {
        return patientobject;
    }

    public void setPatientobject(ParseObject patientobject) {
        this.patientobject = patientobject;
    }

    public ParseObject getOldPatientobject() {
        return Oldpatientobject;
    }

    public void setOldPatientobject(ParseObject patientobject) {
        this.Oldpatientobject = patientobject;
    }

    public Patient_Personal_Info_Frag getpersonalinfo() {
        return pntprsnlinfo;
        // TODO Auto-generated method stub
    }

    public void setpersonalinfo(Patient_Personal_Info_Frag personalfrag) {
        this.pntprsnlinfo = personalfrag;
        // TODO Auto-generated method stub
    }

    public void setmedicalhistory(Patient_Medical_History_First_Frag pntcurrmedinfo) {
        // TODO Auto-generated method stub
        this.pnt_currmed_info = pntcurrmedinfo;
    }

    public Patient_Medical_History_First_Frag getmedicalhistory() {
        return pnt_currmed_info;

    }

    public void setcurrentvitals(CurrentVitals pntcurrmedinfo) {
        this.pnt_curr_medinfo = pntcurrmedinfo;
    }

    public CurrentVitals getcurrentvitals() {
        return pnt_curr_medinfo;
    }

    //Gynecologist Getters and Setters Method

    public Patientpersonalinfo_Gynacoliogist getPntprsnlinfo_gynacologist() {
        return pntprsnlinfo_gynacologist;
    }

    public PatientpersonalInfo getPntprsnlinfo_nephro() {
        return pntprsnlinfo_nephro;
    }

    public void setPntprsnlinfo_gynacologist(
            Patientpersonalinfo_Gynacoliogist pntprsnlinfo_gynacologist) {
        this.pntprsnlinfo_gynacologist = pntprsnlinfo_gynacologist;
    }

    public void setPntprsnlinfo_nephro(
            PatientpersonalInfo pntprsnlinfo_neInfo) {
        this.pntprsnlinfo_nephro = pntprsnlinfo_neInfo;
    }

    public PatientCurrentMedicalaInformation_Gynacologist getPatientcurrentmedinfo_gynacologist() {
        return patientcurrentmedinfo_gynacologist;
    }

    public PatientMedicalaHistory getPatientMedicalHistory_nephro() {
        return patientMedicalHistory_nephro;
    }

    public void setPatientcurrentmedinfo_gynacologist(
            PatientCurrentMedicalaInformation_Gynacologist patientcurrentmedinfo_gynacologist) {
        this.patientcurrentmedinfo_gynacologist = patientcurrentmedinfo_gynacologist;
    }

    public void setPatientMedicalHistory_nephro(
            PatientMedicalaHistory patientMedicalHistory_nephro) {
        this.patientMedicalHistory_nephro = patientMedicalHistory_nephro;
    }


    public void setPatientObstritics(
            Obstetrics_gynacologist patientObst) {
        this.patientobstritics = patientObst;
    }


    public Obstetrics_gynacologist getPatientObstritics() {
        return patientobstritics;

    }


    public boolean getdoctereditPatientflag() {
        return docteredit_patientdetails;
    }

    public boolean getpatientflagvalue() {
        return patientedit_checkflag;
    }

    public void setpatientflagvalue(boolean flag) {
        this.patientedit_checkflag = flag;

    }

    public boolean getdisablesidemenuitem() {
        return disablesidemenuitem;
    }

    public void setpatientfrag(Patient_Frag patientfragObj) {
        this.patientfrag = patientfragObj;

    }

    public Patient_Frag getpatientfrag() {
        return patientfrag;

    }

    public boolean getoldpatientupdateflag() {
        return oldpatient_currentupdate_check;

    }

    public void setoldpatientupdateflag(boolean flag) {
        oldpatient_currentupdate_check = flag;
    }

    public boolean getoldpatientcurrentvitalsupdateflag() {
        return oldpatient_currentvitals_update_check;

    }

    public void setoldpatientcurrentvitalsupdateflag(boolean flag) {
        oldpatient_currentvitals_update_check = flag;
    }

    public void setCurrentvitalsObjectid(String objectid) {
        this.currentvitalsObjectid = objectid;
    }

    public String getCurrentvitalsObjectid() {
        return currentvitalsObjectid;
    }

    //Patient Tab Side menu Items Color Change
    public void getpatient_personal_info_colorchange() {
        Patient_Frag.pinfo.setBackgroundColor(getResources().getColor(R.color.red_color));
        Patient_Frag.pcurrMedInfo.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
        Patient_Frag.pmedHist.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
        Patient_Frag.pmeCurrent.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
    }

    public void getmedicalhistory_colorchange() {
        Patient_Frag.pinfo.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
        Patient_Frag.pcurrMedInfo.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
        Patient_Frag.pmedHist.setBackgroundColor(getResources().getColor(R.color.red_color));
        Patient_Frag.pmeCurrent.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
    }

    public void getcurrentmedinfo_colorchange() {
        Patient_Frag.pinfo.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
        Patient_Frag.pcurrMedInfo.setBackgroundColor(getResources().getColor(R.color.red_color));
        Patient_Frag.pmedHist.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
        Patient_Frag.pmeCurrent.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
    }

    public void getcurrent_vitals_colorchange() {
        Patient_Frag.pinfo.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
        Patient_Frag.pcurrMedInfo.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
        Patient_Frag.pmedHist.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
        Patient_Frag.pmeCurrent.setBackgroundColor(getResources().getColor(R.color.red_color));
    }

    public void getdiagnosis_create_colorchange() {
        Diagnosis_Frag.createDiag.setBackgroundColor(getResources().getColor(R.color.red_color));
        Diagnosis_Frag.viewDiag.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
    }

    public void getprescription_create_colorchange() {
        Treatment_Frag.createtm.setBackgroundColor(getResources().getColor(R.color.red_color));
//        Treatment_Frag.viewtm.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
    }


    //Diagnosis Tab Item get and set methods
    public Diagnosis_View_Frag getdiagnosisviewfrag() {
        return viewdiag;
    }

    public void setdiagnosisviewfrag(Diagnosis_View_Frag frag) {
        this.viewdiag = frag;
    }

    public Diagnosis_Create_Frag getdiagnosiscreatefrag() {
        return creatediag;
    }

    public Diagnosis_Frag getdiagnosisfrag() {
        return diagfrag;
    }

    public void setdiagnosisfrag(Diagnosis_Frag frag) {
        this.diagfrag = frag;
    }

    public void setdiagnosiscreatefrag(Diagnosis_Create_Frag frag) {
        this.creatediag = frag;
    }


    public void setdiagnosiscreatefrag_gynacologist(DiagnosisCreate_Gynacologist frag) {
        this.creatediag_gynacologist = frag;
    }

    public DiagnosisCreate_Gynacologist getdiagnosiscreatefrag_gynacologist() {
        return creatediag_gynacologist;
    }

    public void setdiagnosiscreatefrag_nephro(Diagnosis frag) {
        this.creatediag_nephro = frag;
    }

    public Diagnosis getdiagnosiscreatefrag_nephro() {
        return creatediag_nephro;
    }

    public Diagnosis_Frag_Gynacologist getDiagfrag_gynacologist() {
        return diagfrag_gynacologist;
    }

    public DiagnosisSideMenu getDiagfrag_nephro() {
        return diagfrag_nephro;
    }

    public void setDiagfrag_gynacologist(
            Diagnosis_Frag_Gynacologist diagfrag_gynacologist) {
        this.diagfrag_gynacologist = diagfrag_gynacologist;
    }

    public void setDiagfrag_nephro(
            DiagnosisSideMenu diagfrag_nephro) {
        this.diagfrag_nephro = diagfrag_nephro;
    }

    public Diagnosisexamination_Gynacologist getDiagexam_gynacologist() {
        return diagexam_gynacologist;
    }

    public void setDiagexam_gynacologist(
            Diagnosisexamination_Gynacologist diagexam_gynacologist) {
        this.diagexam_gynacologist = diagexam_gynacologist;
    }

    public void setDiagexam_nephro(
            DiagnosisExamination diagexam_nephro) {
        this.diagexam_nephro = diagexam_nephro;
    }

    public DiagnosisExamination getDiagexam_nephro() {
        return diagexam_nephro;
    }


    public ParseObject getPatientdiagnosisparseobject() {
        return patientdiagnosisparseObj;
    }

    public void setdiseasenamefromparse(ArrayList<Diseasenameclass> diseasenamefromparsearraylist) {
        this.diseasenamefromparsearraylist = diseasenamefromparsearraylist;
    }

    public ArrayList<Diseasenameclass> getdiseasenamefromparse() {
        return diseasenamefromparsearraylist;
    }

    //Prescription Tab Item get and set methods
    public Treatment_Create_Frag gettreatmentcreatefrag() {
        return createtrat;
    }

    public void settreatmentcreatefrag(Treatment_Create_Frag treatfrag) {
        createtrat = treatfrag;
    }


    public void settreatmentfrag(Treatment_Frag treatfrag) {
        this.treatementfrag = treatfrag;
    }

    public Treatment_Frag gettreatmentfrag() {
        return treatementfrag;
    }


    public Treatment_View_Frag gettreatmentviewcreatefrag() {
        return treatmentviewfrag;
    }

    public void settreatmentviewfrag(Treatment_View_Frag treatviewfrag) {
        this.treatmentviewfrag = treatviewfrag;
    }

    public Prescriptionmanagement_Gynacologist getTreatmentmanagement_gynacologist() {
        return treatmentmanagement_gynacologist;
    }

    public void setTreatmentmanagement_gynacologist(
            Prescriptionmanagement_Gynacologist treatmentmanagement_gynacologist) {
        this.treatmentmanagement_gynacologist = treatmentmanagement_gynacologist;
    }


    public boolean getdatasaved() {
        return datasaved;
    }

    public void setdatasaved(boolean value) {
        this.datasaved = value;
    }

    //Admin Tab Item get and set methods
    public void setPatientserachresultObj(List<ParseObject> patientobject) {
        this.patientsearchresultObj = patientobject;
    }

    public List<ParseObject> getPatientserachresultObj() {
        return patientsearchresultObj;
    }

    public void seteditlistener(onSearchSubmit listener) {
        this.listener = listener;
    }

    public onSearchSubmit geteditlistener() {
        return listener;
    }


    //Back Button Press
    @Override
    public void onBackPressed() {
        backpressedmethod();
    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
        if (adminloadflag == 1) {
            patienttab.setVisibility(View.INVISIBLE);
            diagnosistab.setVisibility(View.INVISIBLE);
            prescriptiontab.setVisibility(View.INVISIBLE);
        }

    }

    public boolean diagnosistabvisibility() {

        if (diagnosistab.getVisibility() == View.VISIBLE) {
            return true;
        } else {
            return false;
        }


    }

    public boolean prescriptiontabvisibility() {

        System.out.println("getvisibility" + prescriptiontab.getVisibility());
        if (prescriptiontab.getVisibility() == View.VISIBLE) {
            return true;
        } else {
            return false;
        }


    }


    public void patienttabcolorchange() {
        // TODO Auto-generated method stub
        patienttabitem1.setBackgroundColor(getResources().getColor(R.color.red_color));
        patienttabitem2.setBackgroundColor(getResources().getColor(R.color.red_color));
        diagnosistabitem1.setBackgroundColor(getResources().getColor(R.color.white));
        prescriptiontabitem1.setBackgroundColor(getResources().getColor(R.color.white));
        admintabitem1.setBackgroundColor(getResources().getColor(R.color.white));

        diagnosistabitem2.setBackgroundColor(Color.TRANSPARENT);
        prescriptiontabitem2.setBackgroundColor(Color.TRANSPARENT);
        admintabitem2.setBackgroundColor(Color.TRANSPARENT);
        adminimage.setImageResource(R.drawable.logo_2);

        tvpatient.setTextColor(getResources().getColor(R.color.white));
        tvdiagnosis.setTextColor(getResources().getColor(R.color.dark_blue_color));
        tvprescription.setTextColor(getResources().getColor(R.color.dark_blue_color));
    }


    public void diagnosistabcolorchange() {
        // TODO Auto-generated method stub
        patienttabitem1.setBackgroundColor(getResources().getColor(R.color.white));
        patienttabitem2.setBackgroundColor(Color.TRANSPARENT);
        diagnosistabitem1.setBackgroundColor(getResources().getColor(R.color.red_color));
        prescriptiontabitem1.setBackgroundColor(getResources().getColor(R.color.white));
        admintabitem1.setBackgroundColor(getResources().getColor(R.color.white));
        adminimage.setImageResource(R.drawable.logo_2);
        diagnosistabitem2.setBackgroundColor(getResources().getColor(R.color.red_color));
        prescriptiontabitem2.setBackgroundColor(Color.TRANSPARENT);
        admintabitem2.setBackgroundColor(Color.TRANSPARENT);


        tvpatient.setTextColor(getResources().getColor(R.color.dark_blue_color));
        tvdiagnosis.setTextColor(getResources().getColor(R.color.white));
        tvprescription.setTextColor(getResources().getColor(R.color.dark_blue_color));

    }

    public void prescriptiontabcolorchange() {
        // TODO Auto-generated method stub
        patienttabitem1.setBackgroundColor(getResources().getColor(R.color.white));
        patienttabitem2.setBackgroundColor(Color.TRANSPARENT);
        diagnosistabitem1.setBackgroundColor(getResources().getColor(R.color.white));
        prescriptiontabitem1.setBackgroundColor(getResources().getColor(R.color.red_color));
        admintabitem1.setBackgroundColor(getResources().getColor(R.color.white));
        adminimage.setImageResource(R.drawable.logo_2);

        diagnosistabitem2.setBackgroundColor(Color.TRANSPARENT);
        prescriptiontabitem2.setBackgroundColor(getResources().getColor(R.color.red_color));
        admintabitem2.setBackgroundColor(Color.TRANSPARENT);

        tvpatient.setTextColor(getResources().getColor(R.color.dark_blue_color));
        tvdiagnosis.setTextColor(getResources().getColor(R.color.dark_blue_color));
        tvprescription.setTextColor(getResources().getColor(R.color.white));
    }

    public void admintabcolorchange() {
        // TODO Auto-generated method stub

        System.out.println("color change admin tab....");


        patienttabitem1.setBackgroundColor(getResources().getColor(R.color.white));
        patienttabitem2.setBackgroundColor(Color.TRANSPARENT);
        diagnosistabitem1.setBackgroundColor(getResources().getColor(R.color.white));
        prescriptiontabitem1.setBackgroundColor(getResources().getColor(R.color.white));
        adminimage.setImageResource(R.drawable.logo_white);

        admintabitem1.setBackgroundColor(getResources().getColor(R.color.red_color));
        diagnosistabitem2.setBackgroundColor(Color.TRANSPARENT);
        prescriptiontabitem2.setBackgroundColor(Color.TRANSPARENT);
        admintabitem2.setBackgroundColor(getResources().getColor(R.color.red_color));


        tvpatient.setTextColor(getResources().getColor(R.color.dark_blue_color));
        tvdiagnosis.setTextColor(getResources().getColor(R.color.dark_blue_color));
        tvprescription.setTextColor(getResources().getColor(R.color.dark_blue_color));
    }

    public void enabletabItemvisibility() {
        admintab.setVisibility(View.VISIBLE);
    }

    public void setsubmitbuttonActivation(int code) {
        this.activationcode = code;
    }

    public void addsubmitbuttonActivation() {
        activationcode++;
    }

    public int getsubmitbuttonActivation() {
        return activationcode;
    }

    public void setcheckadmintab(boolean value) {
        this.check_admintab = value;
    }

    public boolean getcheckadmintab() {
        return check_admintab;
    }

    public void SetpatienttabInvisibe() {
        patienttab.setVisibility(View.INVISIBLE);
    }

    public boolean getadmintabdefault() {
        return admin_default;
    }

    public String[] getyeararray() {
        return year_array;
    }

    public void backpressedmethod() {

        FragmentManager fm = getFragmentManager();
        SharedPreferences scanpid = getSharedPreferences("ScannedPid", 0);
        scanpid.edit().remove("scanpid").commit();

        SharedPreferences fnname = getSharedPreferences("fnName", 0);
        fnname.edit().remove("fNname").commit();

        Home_Page_Activity.patient = null;
        Log.e("Back stack count", fm.getBackStackEntryCount() + "");
        if (fm.getBackStackEntryCount() == 0) {

            System.out.println("back pressed workinggggggggg");
            if (doubleBackToExitPressedOnce) {
                //				super.onBackPressed();
                finish();
                Home_Page_Activity.patient = null;
                //Patient_Main_Frag.check_save=0;
                check_save = 0;
                return;
            }
            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this, "Press again to exit.", Toast.LENGTH_SHORT).show();

            new Handler().postDelayed(new Runnable() {


                @Override
                public void run() {
                    doubleBackToExitPressedOnce = false;
                    //	finish();
                }
            }, 2000);
        } else {
            System.out.println("backstack name" + fm.getBackStackEntryCount());

			/*if(fm.getBackStackEntryCount() > 1)*/
            fm.popBackStack();
        }

    }


}
