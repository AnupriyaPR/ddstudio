package com.wiinnova.doctorsdiary.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.wiinnova.doctorsdiary.R;
import com.parse.Parse;
import com.parse.ParseObject;
import com.wiinnova.doctorsdiary.fragment.LoginFragment;
import com.wiinnova.doctorsdiary.supportclasses.Diseasenameclass;

import java.util.ArrayList;
import java.util.List;


public class Login_Activity extends FragmentActivity {

	private TextView forgotpassword;
	String docRegistartionnumber;
	Button logIn,registerButton;
	EditText uEmail,uPass;
	//TextView reg;
	ProgressDialog mProgressDialog;
	String userEmail;
	String password;
	String unm;
	String pass;
	LinearLayout mainLayout;
	private boolean doubleBackToExitPressedOnce;
	String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
	ParseObject patient;
	ArrayList<Diseasenameclass> diseasenamearraylist=new ArrayList<Diseasenameclass>();
	List<ParseObject> diseaseobjects=new ArrayList<ParseObject>();
	
	
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.login_activity);
		
		getSupportFragmentManager().beginTransaction().replace(R.id.container, new LoginFragment()).commit();
		
		Parse.initialize(this, "vrA1DYO8NtY4TkNCEagJshdUxLJDowGiWrBfkXwA", "Dv7FuFQCag8Oj1k0upq8EUnLbDfXlqATpNuCaYE1");

		
		registerButton=(Button) findViewById(R.id.regbutt);
		mainLayout=(LinearLayout)findViewById(R.id.lmain);

		registerButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub

				Intent i=new Intent(Login_Activity.this,Register_Activity.class);
				startActivity(i);



			}
		});

		mainLayout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				hideKeyboard(arg0);
			}
		});

	}

	//}
	protected void hideKeyboard(View view) {
		InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		in.hideSoftInputFromWindow(view.getWindowToken(),
				InputMethodManager.HIDE_NOT_ALWAYS);
	}



	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return false;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onBackPressed() {
		if (doubleBackToExitPressedOnce) {
			super.onBackPressed();
			return;
		}

		this.doubleBackToExitPressedOnce = true;
		Toast.makeText(this, "Press again to exit.", Toast.LENGTH_SHORT).show();

		new Handler().postDelayed(new Runnable() {

			@Override
			public void run() {
				doubleBackToExitPressedOnce=false;                       
			}
		}, 5000);
	} 
	boolean isValidEmail(CharSequence target) {
		if (target == null) {
			return false;
		} else {
			return android.util.Patterns.EMAIL_ADDRESS.matcher(target)
					.matches();
		}
	}
}
