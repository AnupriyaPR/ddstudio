package com.wiinnova.doctorsdiary.activities;


import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;

import com.wiinnova.doctorsdiary.R;
import com.wiinnova.doctorsdiary.fragment.Register_Create_Account_One_Frag;


public class Register_Activity extends FragmentActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
	            WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.register_activity);
		
		FragmentManager fragmentManager = getFragmentManager();
		FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
		
		Register_Create_Account_One_Frag create_account = new Register_Create_Account_One_Frag();
		fragmentTransaction.replace(R.id.container, create_account);
		fragmentTransaction.addToBackStack("Frag1");
		fragmentTransaction.commit();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return false;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		return super.onOptionsItemSelected(item);
	}
	
	@Override
    public void onBackPressed()
    {
        FragmentManager fm = getFragmentManager();
        Log.e("frag count", String.valueOf(fm.getBackStackEntryCount()));
        if(fm.getBackStackEntryCount()<2){
        	finish();
        }
        else{
        	Log.e("frag count", String.valueOf(fm.getBackStackEntryCount()));
	      fm.popBackStack(); 
        } 
    }
}
