package com.wiinnova.doctorsdiary.activities;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.wiinnova.doctorsdiary.R;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.SaveCallback;
import com.wiinnova.doctorsdiary.supportclasses.ConnectionDetector;

import java.util.ArrayList;
import java.util.List;

public class Apkauthentication extends Activity
{
	private EditText etAuthentication;
	private Button btnSubmit;
	ProgressDialog dialog;
	private LinearLayout llMain;
	ParseObject patient;
	List<ParseObject> diseaseobjects=new ArrayList<ParseObject>();
	ConnectionDetector connection;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.authentication_activity);
		etAuthentication=(EditText)findViewById(R.id.etAuthentication);
		btnSubmit=(Button)findViewById(R.id.btnSubmit);
		llMain=(LinearLayout)findViewById(R.id.lmain);
		connection=new ConnectionDetector(Apkauthentication.this);



		final TelephonyManager telephonyManager = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);

		llMain.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				hideKeyboard(arg0);
			}
		});


		btnSubmit.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub

				System.out.println("telephonyManager.getDeviceId()"+telephonyManager.getDeviceId());



				if(etAuthentication.getText().length()==0){

					etAuthentication.setError("Auhentication Number");
					etAuthentication.requestFocus();

				}else{
					etAuthentication.setError(null);
				}

				if(connection.isConnectedToInternet()){

					if(etAuthentication.getText().length()!=0){

						dialog=new ProgressDialog(Apkauthentication.this);
						dialog.setCancelable(false);
						dialog.setMessage("Please wait.....");
						dialog.show();
						ParseQuery<ParseObject> query = ParseQuery.getQuery("Authentication");
						query.whereEqualTo("authenticationNumber",etAuthentication.getText().toString());
						query.findInBackground(new FindCallback<ParseObject>() {
							@Override
							public void done(List<ParseObject> objects, ParseException e) {
								// TODO Auto-generated method stub

								if(e==null){
									if(objects.size()>0){
										if(objects.get(0).getBoolean("authenticationFlag")==false){
											objects.get(0).put("authenticationFlag", true);
											objects.get(0).put("deviceImei", telephonyManager.getDeviceId());
											objects.get(0).saveInBackground(new SaveCallback() {
												@Override
												public void done(ParseException e) {
													// TODO Auto-generated method stub
													dialog.dismiss();

													if(e==null){
														Toast.makeText(Apkauthentication.this,"Successfully verified", Toast.LENGTH_LONG).show();
														//fetechingdiseasenames_server();
														Intent i = new Intent(Apkauthentication.this, Login_Activity.class);
														startActivity(i);
														finish();

														SharedPreferences appauthentication=getSharedPreferences("Authentication", 0);
														SharedPreferences.Editor Ed=appauthentication.edit();
														Ed.clear();
														Ed.putBoolean("authenticationflag", true);            
														Ed.commit();
													}



												}
											});


										}else{
											dialog.dismiss();
											Toast.makeText(Apkauthentication.this,"The number already authenticated", Toast.LENGTH_LONG).show();
										}
									}else{
										dialog.dismiss();
										Toast.makeText(Apkauthentication.this,"The enterd authentication number is incorrect", Toast.LENGTH_LONG).show();

									}
								}else{
									dialog.dismiss();
									Toast.makeText(Apkauthentication.this,"Something went wrong.Please try again later.", Toast.LENGTH_LONG).show();
								}


							}
						});


					}
				}else{

					Toast.makeText(Apkauthentication.this, "Internet is not connected.Please Switch On Net Connectivity", Toast.LENGTH_LONG).show();
				}
			}
		});

	}

	/*private void fetechingdiseasenames_server() {
		// TODO Auto-generated method stub
		ParseQuery<ParseObject> query = ParseQuery.getQuery("DiseaseDatabase");
		query.fromLocalDatastore();
		query.findInBackground(new FindCallback<ParseObject>() {

			@Override
			public void done(List<ParseObject> objects, ParseException e) {
				// TODO Auto-generated method stub
				if(objects.size()==0){

					ParseQuery<ParseObject> query = ParseQuery.getQuery("DiseaseDatabase");
					query.setLimit(1000);

					query.whereNotEqualTo("DiseaseName",null);
					query.findInBackground(new FindCallback<ParseObject>() {

						@Override
						public void done(List<ParseObject> objects, ParseException e) {

							// TODO Auto-generated method stub
							if (e == null) {
								if (objects.size() > 0) {
									// query found a user

									Log.e("Login", "Id exist");
									patient=objects.get(0);
									diseaseobjects.addAll(objects);

									diseaseselect_server();


								}
								else {

									Toast msg=Toast.makeText(Apkauthentication.this, "Some problem in your network connection.Please check and try agian... ", Toast.LENGTH_LONG);
									msg.show();

								}
							} 
							else {
								Toast.makeText(Apkauthentication.this, "Sorry, something went wrong", Toast.LENGTH_LONG).show();
							}

						}


					});



				}


			}
		});




	}

	private void diseaseselect_server() {
		// TODO Auto-generated method stub

		ParseQuery<ParseObject> query = ParseQuery.getQuery("DiseaseDatabase");
		query.setLimit(589);
		query.setSkip(1000);
		query.whereNotEqualTo("DiseaseName",null);
		query.findInBackground(new FindCallback<ParseObject>() {

			@Override
			public void done(List<ParseObject> objects, ParseException e) {

				// TODO Auto-generated method stub
				if (e == null) {
					if (objects.size() > 0) {

						Log.e("Login", "Id exist");
						patient=objects.get(0);
						diseaseobjects.addAll(objects);
						Toast.makeText(Apkauthentication.this, "count"+diseaseobjects.size(),Toast.LENGTH_LONG).show();
						ParseObject.pinAllInBackground(diseaseobjects);
						if(ParseObject.pinAllInBackground(objects) != null){
							System.out.println("pin successs");
						}

					}
					else {

						Toast msg=Toast.makeText(Apkauthentication.this, "Some problem in your network connection.Please check and try agian...", Toast.LENGTH_LONG);
						msg.show();
					}
				} 
				else {
					Toast.makeText(Apkauthentication.this, "Sorry, something went wrong", Toast.LENGTH_LONG).show();
				}
			}
		});
	}

	 */


	protected void hideKeyboard(View view) {
		InputMethodManager in = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
		in.hideSoftInputFromWindow(view.getWindowToken(),
				InputMethodManager.HIDE_NOT_ALWAYS);
	}


}
