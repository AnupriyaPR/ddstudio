package com.wiinnova.doctorsdiary.activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ClipData;
import android.content.ClipDescription;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.DragEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.wiinnova.doctorsdiary.R;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Image;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;

public class printTestActivity extends Activity{
	
	   ImageView img;
	   String msg;
	   private RelativeLayout.LayoutParams layoutParams;
	   private View mRootView;
	
	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.print_test);
		
		  img=(ImageView)findViewById(R.id.imageView);
		  
		//Create a directory for your PDF
		  File pdfDir = new File(Environment.getExternalStorageDirectory() + "/MyApp");
		  boolean isPresent = true;
		  
		 /* File pdfDir = new File(Environment.getExternalStoragePublicDirectory(
		          Environment.DIRECTORY_DOCUMENTS), "MyApp");*/
		  if (!pdfDir.exists()) {
			    isPresent = pdfDir.mkdir();
			}
		  
		 /* if (isPresent) {*/
			    File pdfFile = new File(pdfDir.getAbsolutePath(),"myPdfFile.pdf"); 
			/*}*/
		  
		/*  if (!pdfDir.exists()){
		      pdfDir.mkdir();
		  }*/
		  
		//Then take the screen shot 
		  Bitmap screen; View v1 = getWindow().getDecorView().getRootView();
		  v1.setDrawingCacheEnabled(true);
		  screen = Bitmap.createBitmap(v1.getDrawingCache());
		  v1.setDrawingCacheEnabled(false);

		  //Now create the name of your PDF file that you will generate
		 /* File pdfFile = new File(pdfDir, "myPdfFile.pdf");*/

		  try {
	            Document  document = new Document();

	            PdfWriter.getInstance(document, new FileOutputStream(pdfFile));
	            document.open();
	            ByteArrayOutputStream stream = new ByteArrayOutputStream();
	            screen.compress(Bitmap.CompressFormat.PNG, 100, stream);
	            byte[] byteArray = stream.toByteArray();
	            addImage(document,byteArray);
	            document.close();
	        }
	        catch (Exception e){
	            e.printStackTrace();
	        }
	    
		  
		  img.setOnLongClickListener(new View.OnLongClickListener() {
		         @Override
		         public boolean onLongClick(View v) {
		            ClipData.Item item = new ClipData.Item((CharSequence)v.getTag());
		            String[] mimeTypes = {ClipDescription.MIMETYPE_TEXT_PLAIN};
		            
		            ClipData dragData = new ClipData(v.getTag().toString(),mimeTypes, item);
		            View.DragShadowBuilder myShadow = new View.DragShadowBuilder(img);
		            
		            v.startDrag(dragData,myShadow,null,0);
		            return true;
		         }
		      });
		      
		      img.setOnDragListener(new View.OnDragListener() {
		         @Override
		         public boolean onDrag(View v, DragEvent event) {
		            switch(event.getAction())
		            {
		               case DragEvent.ACTION_DRAG_STARTED:
		               layoutParams = (RelativeLayout.LayoutParams)v.getLayoutParams();
		               Log.d(msg, "Action is DragEvent.ACTION_DRAG_STARTED");
		               
		               // Do nothing
		               break;
		               
		               case DragEvent.ACTION_DRAG_ENTERED:
		               Log.d(msg, "Action is DragEvent.ACTION_DRAG_ENTERED");
		               int x_cord = (int) event.getX();
		               int y_cord = (int) event.getY();
		               break;
		               
		               case DragEvent.ACTION_DRAG_EXITED :
		               Log.d(msg, "Action is DragEvent.ACTION_DRAG_EXITED");
		               x_cord = (int) event.getX();
		               y_cord = (int) event.getY();
		               layoutParams.leftMargin = x_cord;
		               layoutParams.topMargin = y_cord;
		               v.setLayoutParams(layoutParams);
		               break;
		               
		               case DragEvent.ACTION_DRAG_LOCATION  :
		               Log.d(msg, "Action is DragEvent.ACTION_DRAG_LOCATION");
		               x_cord = (int) event.getX();
		               y_cord = (int) event.getY();
		               break;
		               
		               case DragEvent.ACTION_DRAG_ENDED   :
		               Log.d(msg, "Action is DragEvent.ACTION_DRAG_ENDED");
		               
		               // Do nothing
		               break;
		               
		               case DragEvent.ACTION_DROP:
		               Log.d(msg, "ACTION_DROP event");
		               
		               // Do nothing
		               break;
		               default: break;
		            }
		            return true;
		         }
		      });
		      
		      img.setOnTouchListener(new View.OnTouchListener() {
		         @Override
		         public boolean onTouch(View v, MotionEvent event) {
		            if (event.getAction() == MotionEvent.ACTION_DOWN) {
		               ClipData data = ClipData.newPlainText("", "");
		               View.DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(img);
		               
		               img.startDrag(data, shadowBuilder, img, 0);
		               img.setVisibility(View.INVISIBLE);
		               return true;
		            }
		            else
		            {
		               return false;
		            }
		         }
		      });
		
		
		
	}

private static void addImage(Document document,byte[] byteArray)
{
    Image image = null;
    try
    {
       image = Image.getInstance(byteArray);
    }
    catch (BadElementException e)
    {
        // TODO Auto-generated catch block
        e.printStackTrace();
    }
    catch (MalformedURLException e)
    {
        // TODO Auto-generated catch block
        e.printStackTrace();
    }
    catch (IOException e)
    {
        // TODO Auto-generated catch block
        e.printStackTrace();
    }
    // image.scaleAbsolute(150f, 150f);
    try
    {
        document.add(image);
    } catch (DocumentException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
    }
}

}
