package com.wiinnova.doctorsdiary.activities;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.SaveCallback;
import com.wiinnova.doctorsdiary.R;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.wiinnova.doctorsdiary.popupwindow.NewPatientId_Popupfragment;
import com.wiinnova.doctorsdiary.popupwindow.NewPatientId_Popupfragment.onSaveListener;
import com.wiinnova.doctorsdiary.popupwindow.OldPatientId_Popupfragment;
import com.wiinnova.doctorsdiary.popupwindow.OldPatientId_Popupfragment.onSubmitListener;
import com.wiinnova.doctorsdiary.popupwindow.Scanner_Popup;
import com.wiinnova.doctorsdiary.popupwindow.Scanner_Popup.onScannerpopuplistener;
import com.wiinnova.doctorsdiary.supportclasses.AppUtil;
import com.wiinnova.doctorsdiary.supportclasses.ConnectionDetector;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.regex.Pattern;

public class Home_Page_Activity extends Activity implements onSubmitListener, onSaveListener, Serializable, onScannerpopuplistener {

    transient ImageView logout;
    transient LinearLayout scanNewPatient, enterNewPatient, enterOldPatient, scanexist, scanbioNew, scanOldbio;


    ConnectionDetector networkconnection_check;


    OldPatientId_Popupfragment opIdObj;
    Scanner_Popup spIdObj;
    NewPatientId_Popupfragment npIdObj;

    ProgressDialog mProgressDialog;
    String patientId1, patientId2;
    private boolean doubleBackToExitPressedOnce;
    public static ParseObject patient;
    int adminloadflag;
//	private Tracker mTracker;

    transient EditText oldpid;
    transient EditText newpid;
    transient TextView tvNewpatientId;
    transient TextView tvScanNewpatientId;
    transient TextView tvNewbiometricId;
    transient TextView tvOldpatientId;
    transient TextView tvScanOldpatientId;
    transient TextView tvOldbiometricId;

    transient ImageView iv_home_enter;
    transient ImageView iv_scan_patient;
    transient ImageView iv_scan_biometric;

    transient ImageView iv_home_enter1;
    transient ImageView iv_scan_patient1;
    transient ImageView iv_scan_biometric1;
    transient RelativeLayout lladmin;

    transient LinearLayout topheader;
    transient LinearLayout container;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.home_page_activity);

        networkconnection_check = new ConnectionDetector(this);


        iv_home_enter = (ImageView) findViewById(R.id.ivhome_enter);
        iv_scan_patient = (ImageView) findViewById(R.id.iv_home_scan);
        iv_scan_biometric = (ImageView) findViewById(R.id.ivhome_scan_biometric);
        iv_home_enter1 = (ImageView) findViewById(R.id.ivhome_enter_patient);
        iv_scan_patient1 = (ImageView) findViewById(R.id.ivhome_scan_patient);
        iv_scan_biometric1 = (ImageView) findViewById(R.id.ivhome_scan_biometric1);

        tvNewpatientId = (TextView) findViewById(R.id.tvnewpatientid);
        tvOldpatientId = (TextView) findViewById(R.id.tvoldpatientid);
        tvNewbiometricId = (TextView) findViewById(R.id.tvnewbiometrictid);
        tvOldbiometricId = (TextView) findViewById(R.id.tvoldbiometric);
        tvScanOldpatientId = (TextView) findViewById(R.id.tvscan_oldpatientid);
        tvScanNewpatientId = (TextView) findViewById(R.id.tvscan_newpatientid);

        lladmin = (RelativeLayout) findViewById(R.id.llnadmin);


        enterNewPatient = (LinearLayout) findViewById(R.id.enternew);
        scanNewPatient = (LinearLayout) findViewById(R.id.scannew);
        enterOldPatient = (LinearLayout) findViewById(R.id.enterold);
        logout = (ImageView) findViewById(R.id.ivlogout);
        scanexist = (LinearLayout) findViewById(R.id.scanold);
        scanbioNew = (LinearLayout) findViewById(R.id.scanbionew);
        scanOldbio = (LinearLayout) findViewById(R.id.scanoldbio);
        topheader = (LinearLayout) findViewById(R.id.cal1);
        container = (LinearLayout) findViewById(R.id.ml2);


        try {
            PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            String appVersion = pInfo.versionName;
            TextView txtAppVersion = (TextView) findViewById(R.id.txtversion);
            txtAppVersion.setText("App version: " + appVersion);
        } catch (PackageManager.NameNotFoundException ex) {
            ex.printStackTrace();

        }

        container.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                hideKeyboard(v);
            }
        });
        topheader.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                hideKeyboard(v);
            }
        });


        lladmin.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub


                adminloadflag = 1;
                Intent i = new Intent(Home_Page_Activity.this, FragmentActivityContainer.class);
                i.putExtra("adminloadflagvalue", adminloadflag);
                i.putExtra("patientflag", false);
                startActivity(i);


            }
        });

        scanbioNew.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stuz

            }
        });

        scanOldbio.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

            }
        });


        enterNewPatient.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub

                Bundle b = new Bundle();
                //b.putSerializable("listener", Home_Page_Activity.this);
                npIdObj = new NewPatientId_Popupfragment();
                npIdObj.setmListener(Home_Page_Activity.this);
                npIdObj.setCancelable(true);
                npIdObj.show(getFragmentManager(), "jj1jjjjjjj");
            }
        });

        scanNewPatient.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {

				/*	Intent intent = new Intent("com.google.zxing.client.android.SCAN");
                intent.setClassName(Home_Page_Activity.this, "com.google.zxing.client.android.CaptureActivity");
				intent.putExtra("SCAN_FORMATS","QR_CODE,CODE_128");


				startActivityForResult(intent, 1001);*/


                Bundle b = new Bundle();
                //b.putSerializable("listener", Home_Page_Activity.this);
                b.putInt("scantype", 0);
                spIdObj = new Scanner_Popup();
                spIdObj.setListener(Home_Page_Activity.this);
                spIdObj.setArguments(b);
                spIdObj.setCancelable(true);
                spIdObj.show(getFragmentManager(), "jj1jjjjjjj");

            }
        });

        enterOldPatient.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                opIdObj = new OldPatientId_Popupfragment();
                opIdObj.setmListener(Home_Page_Activity.this);
                opIdObj.setCancelable(true);
                opIdObj.show(getFragmentManager(), "jj1jjjjjjj");


            }
        });
        logout.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub


                AlertDialog.Builder alert = new AlertDialog.Builder(Home_Page_Activity.this);

                alert.setTitle("Logout");
                alert.setMessage("Are you sure you want to logout?");


                alert.setCancelable(false);

                alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {


                        Intent i = new Intent(Home_Page_Activity.this, Login_Activity.class);
                        startActivity(i);

                        SharedPreferences preferences = getSharedPreferences("Login", 0);
                        preferences.edit().remove("Unm").commit();
                        preferences.edit().remove("Psw").commit();
                        finish();


                    }
                });

                alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        // Canceled.


                    }
                });

                alert.show();

            }
        });

        scanexist.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                Bundle b = new Bundle();
                //b.putSerializable("listener", Home_Page_Activity.this);
                b.putInt("scantype", 1);
                spIdObj = new Scanner_Popup();
                spIdObj.setListener(Home_Page_Activity.this);
                spIdObj.setArguments(b);
                spIdObj.setCancelable(true);
                spIdObj.show(getFragmentManager(), "jj1jjjjjjj");


            }
        });
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Press again to exit.", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 5000);
    }

    @Override
    public void onSubmit(String arg) {
        // TODO Auto-generated method stub
        patientId1 = arg;
        opIdObj.dismiss();

        SharedPreferences scanpid = getSharedPreferences("NewPatID", 0);
        SharedPreferences.Editor Ed = scanpid.edit();
        Ed.putString("NewID", patientId1);
        Ed.commit();
        oldpatient_details_saving(patientId1);

    }

    private void oldpatient_details_saving(final String patientid) {
        // TODO Auto-generated method stub
        mProgressDialog = new ProgressDialog(Home_Page_Activity.this);
        mProgressDialog.setMessage("Retrieving Patient Information...");
        mProgressDialog.show();

        ParseQuery<ParseObject> query = ParseQuery.getQuery("Patients");
        if (!AppUtil.haveNetwokConnection(Home_Page_Activity.this))
            query.fromLocalDatastore();
        query.whereEqualTo("patientID", patientid);
        query.findInBackground(new FindCallback<ParseObject>() {

            @Override
            public void done(List<ParseObject> objects, ParseException e) {

                // TODO Auto-generated method stub

                if (e == null) {
                    if (objects.size() > 0) {

                        System.out.println("objects size....grater 0");

                        // query found a user
                        mProgressDialog.dismiss();
                        patient = objects.get(0);

                        SharedPreferences scanpid = getSharedPreferences("NewPatID", 0);
                        SharedPreferences.Editor Ed = scanpid.edit();
                        Ed.clear();
                        Ed.putString("NewID", patientId1);
                        Ed.commit();

                        ////save application details(version code,device version etc) to log table for reference.
                        saveDataToLog();

                        Intent i = new Intent(Home_Page_Activity.this, FragmentActivityContainer.class);
                        i.putExtra("patientid", patientId1);
                        i.putExtra("existingflag", true);
                        startActivity(i);
                    } else {


                        mProgressDialog.dismiss();
                        Toast msg = Toast.makeText(Home_Page_Activity.this, "Invalid Patient Id", Toast.LENGTH_LONG);
                        msg.show();

                    }
                } else {
                    mProgressDialog.dismiss();
                    e.printStackTrace();
                    Toast.makeText(Home_Page_Activity.this, "Sorry, something went wrong", Toast.LENGTH_LONG).show();
                }

            }
        });

    }

    @Override
    public void onSave(String arg) {
        // TODO Auto-generated method stub
        patientId2 = arg;
        npIdObj.dismiss();

        newpatient_details(patientId2);

    }

    private void newpatient_details(final String patientId2) {
        // TODO Auto-generated method stub

        mProgressDialog = new ProgressDialog(Home_Page_Activity.this);
        mProgressDialog.setMessage("Opening new patient ID...");
        mProgressDialog.show();


        ParseQuery<ParseObject> query = ParseQuery.getQuery("Patients");
        query.whereEqualTo("patientID", patientId2);
        if (!AppUtil.haveNetwokConnection(Home_Page_Activity.this))
            query.fromLocalDatastore();
        query.findInBackground(new FindCallback<ParseObject>() {

            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                // TODO Auto-generated method stub
                if (e == null) {

                    if (objects.size() > 0) {

                        mProgressDialog.dismiss();
                        Toast.makeText(Home_Page_Activity.this, "Patient Id Exist", Toast.LENGTH_SHORT).show();

                    } else {

                        mProgressDialog.dismiss();
                        SharedPreferences scanpid = getSharedPreferences("NewPatID", 0);
                        SharedPreferences.Editor Ed = scanpid.edit();
                        Ed.clear();
                        Ed.putString("NewID", patientId2);
                        Ed.commit();
                        SharedPreferences fnname = getSharedPreferences("fnName", 0);
                        SharedPreferences.Editor spObj = fnname.edit();
                        spObj.clear();
                        spObj.commit();


                        Intent i = new Intent(Home_Page_Activity.this, FragmentActivityContainer.class);
                        i.putExtra("patientid", patientId2);
                        i.putExtra("existingflag", false);

                        startActivity(i);
                    }

                } else {
                    mProgressDialog.dismiss();
                    Toast.makeText(Home_Page_Activity.this, "Sorry, something went wrong", Toast.LENGTH_LONG).show();

                }

            }

        });
    }


    protected void hideKeyboard(View view) {
        InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        in.hideSoftInputFromWindow(view.getWindowToken(),
                InputMethodManager.HIDE_NOT_ALWAYS);
    }


    @Override
    public void Scanner(String patientid, int scanType) {
        // TODO Auto-generated method stub
        spIdObj.dismiss();
        //Toast.makeText(Home_Page_Activity.this, "patient id"+patientid, Toast.LENGTH_LONG).show();
        System.out.println("scanner patient id after scanning" + patientid);
        if (scanType == 0) {
            Scanned_patientid_exist_check(patientid);
        } else if (scanType == 1) {
            exist_patient_scan(patientid);
        }

    }

    public void Scanned_patientid_exist_check(final String patientscanid) {


        mProgressDialog = new ProgressDialog(Home_Page_Activity.this);

        mProgressDialog.setMessage("Opening new patient ID....");
        mProgressDialog.show();
        ParseQuery<ParseObject> query = ParseQuery.getQuery("Patients");
        query.fromLocalDatastore();
        query.whereEqualTo("patientID", patientscanid);

        query.findInBackground(new FindCallback<ParseObject>() {

            @Override
            public void done(List<ParseObject> objects, ParseException e) {

                // TODO Auto-generated method stub
                if (e == null) {
                    if (objects.size() > 0) {
                        mProgressDialog.dismiss();
                        Log.e("Login", "Id exist");
                        Toast.makeText(Home_Page_Activity.this, "Patient Id exist", Toast.LENGTH_LONG).show();
                    } else {

                        ParseQuery<ParseObject> query = ParseQuery.getQuery("Diagnosis");
                        query.fromLocalDatastore();
                        query.whereEqualTo("patientID", patientscanid);
                        query.findInBackground(new FindCallback<ParseObject>() {

                            @Override
                            public void done(List<ParseObject> objects, ParseException e) {
                                // TODO Auto-generated method stub
                                if (e == null) {
                                    if (objects.size() > 0) {
                                        mProgressDialog.dismiss();
                                        Log.e("Login", "Id exist");
                                        Toast.makeText(Home_Page_Activity.this, "Patient Id exist", Toast.LENGTH_LONG).show();
                                    } else {

                                        mProgressDialog.dismiss();
                                        SharedPreferences scanpid = getSharedPreferences("NewPatID", 0);
                                        SharedPreferences.Editor Ed = scanpid.edit();
                                        Ed.clear();
                                        Ed.putString("NewID", patientscanid);
                                        Ed.commit();
                                        System.out.println("patientscanid" + patientscanid);
                                        Intent i = new Intent(Home_Page_Activity.this, FragmentActivityContainer.class);
                                        startActivity(i);
                                    }
                                }
                            }
                        });
                    }
                } else {

                    mProgressDialog.dismiss();
                    Toast.makeText(Home_Page_Activity.this, "Sorry, something went wrong", Toast.LENGTH_LONG).show();
                }

            }
        });
    }

    public void exist_patient_scan(final String patientscanid) {
        mProgressDialog = new ProgressDialog(Home_Page_Activity.this);

        mProgressDialog.setMessage("Opening new patient ID....");
        mProgressDialog.show();
        ParseQuery<ParseObject> query = ParseQuery.getQuery("Patients");
        query.fromLocalDatastore();
        query.whereEqualTo("patientID", patientscanid);
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                // TODO Auto-generated method stub
                if (e == null) {
                    if (objects.size() > 0) {
                        // query found a user
                        mProgressDialog.dismiss();
                        Log.e("Login", "Id exist");
                        patient = objects.get(0);

                        SharedPreferences scanpid = getSharedPreferences("NewPatID", 0);
                        SharedPreferences.Editor Ed = scanpid.edit();
                        Ed.clear();
                        Ed.putString("NewID", patientscanid);
                        Ed.commit();


                        Intent i = new Intent(Home_Page_Activity.this, FragmentActivityContainer.class);
                        startActivity(i);
                    } else {

                        ParseQuery<ParseObject> query = ParseQuery.getQuery("Diagnosis");
                        query.fromLocalDatastore();
                        query.whereEqualTo("patientID", patientscanid);
                        query.findInBackground(new FindCallback<ParseObject>() {

                            @Override
                            public void done(List<ParseObject> objects, ParseException e) {
                                // TODO Auto-generated method stub
                                if (e == null) {
                                    if (objects.size() > 0) {
                                        mProgressDialog.dismiss();
                                        SharedPreferences scanpid = getSharedPreferences("NewPatID", 0);
                                        SharedPreferences.Editor Ed = scanpid.edit();
                                        Ed.clear();
                                        Ed.putString("NewID", patientscanid);
                                        Ed.commit();

                                        saveDataToLog();
                                        Intent i = new Intent(Home_Page_Activity.this, FragmentActivityContainer.class);
                                        startActivity(i);

                                    } else {
                                        mProgressDialog.dismiss();
                                        Toast.makeText(Home_Page_Activity.this, "Patient ID Does not exist", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            }
                        });
                    }
                } else {
                    Toast.makeText(Home_Page_Activity.this, "Sorry, something went wrong", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

	/*@Override
    protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		GoogleAnalytics.getInstance(Home_Page_Activity.this).reportActivityStart(this);
	}


	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		GoogleAnalytics.getInstance(Home_Page_Activity.this).reportActivityStop(this);
	}
	 */

    /*	@Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        // TODO Auto-generated method stub
        //super.onActivityResult(requestCode, resultCode, data);
    String	contents = intent.getStringExtra("SCAN_RESULT");
        Toast.makeText(Home_Page_Activity.this, "Result...."+contents, Toast.LENGTH_LONG).show();
    }*/
    public void saveDataToLog() {
        ////////////////save to Log table

        SharedPreferences spLogin = getSharedPreferences("Login", 0);

        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
        final String formattedDate = df.format(calendar.getTime());
        if (!spLogin.getString("LogDate", "").equals(formattedDate)) {
            String docId = spLogin.getString("docterRegnumber", null);
            String docName = spLogin.getString("drFLname", null);
            String specialization = spLogin.getString("spel", null);
            String patientId = patientId1;
            String osVersion = android.os.Build.VERSION.RELEASE;
            String deviceType = android.os.Build.DEVICE + " " + android.os.Build.MODEL;
            String appVersion = null;
            String email = null;
            try {
                PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
                appVersion = pInfo.versionName;
            } catch (PackageManager.NameNotFoundException ex) {
                ex.printStackTrace();

            }
            Pattern emailPattern = Patterns.EMAIL_ADDRESS; // API level 8+
            Account[] accounts = AccountManager.get(Home_Page_Activity.this).getAccounts();
            for (Account account : accounts) {
                if (emailPattern.matcher(account.name).matches()) {
                    email = account.name;
                }
            }
            ParseObject parseLog = new ParseObject("Log");
            parseLog.put("docID", docId);
            parseLog.put("deviceUserEmail", email);
            parseLog.put("docName", docName);
            parseLog.put("deviceOSVersion", osVersion);
            parseLog.put("deviceType", deviceType);
            parseLog.put("appVersion", appVersion);
            parseLog.put("specialization", specialization);
            parseLog.put("patientIDQuery", patientId);
            parseLog.saveInBackground(new SaveCallback() {
                @Override
                public void done(ParseException e) {
                    if (e == null) {
                        Log.d("Successsss", "fkdlfj");

                        SharedPreferences spLogin = getSharedPreferences("Login", 0);
                        SharedPreferences.Editor ED = spLogin.edit();
                        ED.putString("LogDate", formattedDate);
                        ED.commit();

                    } else {

                        Log.d("Successsss", e.toString() + "fkdlfj");
                    }
                }
            });
        }
    }
}



