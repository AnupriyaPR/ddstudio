package com.wiinnova.doctorsdiary.activities;


import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;

import com.wiinnova.doctorsdiary.R;
public class SplashScreen_Activity extends Activity {

	private static final String TAG = "tag value";
	// Splash screen timer
	private static int SPLASH_TIME_OUT = 3000;

	String unm,pass;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.splash_activity);
		
		
	

		new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {
				// This method will be executed once the timer is over
				// Start your app main activity

				SharedPreferences sp1=SplashScreen_Activity.this.getSharedPreferences("Login", 0);

				String unm=sp1.getString("Unm", null);       
				String pass = sp1.getString("Psw", null);

				if(unm!=null&&pass!=null){

					Intent i=new Intent(SplashScreen_Activity.this,Home_Page_Activity.class);
					startActivity(i);

					finish();
					Log.e("splash", "finish");

				}

				else{

					SharedPreferences sp=getSharedPreferences("Authentication", 0);
					Boolean authenticationflag=sp.getBoolean("authenticationflag",false);

					if(authenticationflag==true){
						Intent i = new Intent(SplashScreen_Activity.this, Login_Activity.class);
						startActivity(i);
						finish();
						Log.e("splash", "finish");
					}else{
						Intent i = new Intent(SplashScreen_Activity.this, Apkauthentication.class);
						startActivity(i);
						finish();
						Log.e("splash", "finish");
					}
				}
			}
		}, SPLASH_TIME_OUT);
	}
	

}
