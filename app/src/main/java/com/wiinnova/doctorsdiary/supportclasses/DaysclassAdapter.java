package com.wiinnova.doctorsdiary.supportclasses;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.RelativeLayout;
import android.widget.TextView;


import com.wiinnova.doctorsdiary.R;

import java.util.ArrayList;

public class DaysclassAdapter extends BaseAdapter{
	private Context context;
	String listdata[];
	String selected_days[];
	ArrayList<CheckBox> checkboxes=new ArrayList<CheckBox>();
	ArrayList<RelativeLayout> containers=new ArrayList<RelativeLayout>();
	ArrayList<Boolean> selectedcheckbox=new ArrayList<Boolean>();
	boolean flag=true;

	String[] daysarray = {"D","M","T","W","Th","F","S","Su" };  




	public DaysclassAdapter(Context context, String[] array, String[] selecteddays)
	{
		this.context=context;
		this.listdata=array;
		this.selected_days=selecteddays;



		for(int i=0;i<8;i++){
			selectedcheckbox.add(i, false);
		}
		if(selected_days!=null){
			System.out.println("selected days size"+selected_days.length);

			for(int j=0;j<8;j++){

				for(int k=0;k<selected_days.length;k++){
					if(selecteddays[k]!=null){
						if(selected_days[k].equalsIgnoreCase(array[j])){
							selectedcheckbox.set(j, true);
						}
					}
				}
			}
		}
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return listdata.length;
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return listdata[position];
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		if (convertView == null) 
		{
			LayoutInflater mInflater = (LayoutInflater)
					context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
			convertView = mInflater.inflate(R.layout.disease_listitem, null);

		}
		RelativeLayout container=(RelativeLayout)convertView.findViewById(R.id.lldiseasecontainer);
		TextView txtTitle = (TextView) convertView.findViewById(R.id.title);   
		final CheckBox checkbox=(CheckBox) convertView.findViewById(R.id.chklist);
		checkbox.setTag(position);
		checkboxes.add(checkbox);
		containers.add(container);
		if(selectedcheckbox.get(0)==true){
			System.out.println("working......");
			if(position!=0){
				checkbox.setEnabled(false);
				checkbox.setChecked(false);
				System.out.println("false check working...");
				container.setClickable(false);
				for(int i=0;i<selectedcheckbox.size();i++){
					selectedcheckbox.set(position, false);
				}
			}

		}else{
			checkbox.setEnabled(true);
			container.setClickable(true);
		}
		System.out.println("checkbox size"+checkboxes.size());
		txtTitle.setText(listdata[position]);
		if(selected_days!=null){
			if(selectedcheckbox.get(position)==true){
				checkbox.setChecked(true);
				System.out.println("position checked"+position);
			}else{
				checkbox.setChecked(false);
				if(selectedcheckbox.get(0)==true){
					checkbox.setEnabled(false);
				}
			}
			notifyDataSetChanged();
		}
		checkbox.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(checkbox.isChecked()){
					selectedcheckbox.set(position, true);
					System.out.println("position checked"+position);
				}else{
					System.out.println("false working.......");
					selectedcheckbox.set(position, false);
				}
				notifyDataSetChanged();
			}
		});

		container.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				checkbox.toggle();
				if( checkbox.isChecked()){
					selectedcheckbox.set(position, true);
					System.out.println("position checked"+position);
				}else{
					System.out.println("false working.......");
					selectedcheckbox.set(position, false);
				}
				notifyDataSetChanged();
			}
		});

		return convertView;
	}

	public ArrayList<String> getSelecteditem()
	{
		ArrayList<String> selectdays=new ArrayList<String>();
		for(int i=0;i<selectedcheckbox.size();i++){
			if(selectedcheckbox.get(i)==true){
				selectdays.add(listdata[i]);
			}
		}
		return selectdays;
	}


}
