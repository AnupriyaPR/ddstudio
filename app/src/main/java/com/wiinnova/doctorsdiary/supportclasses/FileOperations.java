package com.wiinnova.doctorsdiary.supportclasses;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.app.FragmentActivity;
import android.util.Log;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.Image;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.parser.PdfReaderContentParser;
import com.itextpdf.text.pdf.parser.SimpleTextExtractionStrategy;
import com.itextpdf.text.pdf.parser.TextExtractionStrategy;
import com.parse.ParseObject;

import org.json.JSONException;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.StringTokenizer;

public class FileOperations{


	Context context;
	static PdfWriter writer;
	Document document;
	Font font;
	Font textfont;

	public FileOperations() {
	}

	public Boolean write(FragmentActivity fragmentActivity, String fname, ParseObject combinedobject) {
		try {


			context=fragmentActivity;
			String fpath = "/sdcard/" + fname + ".pdf";
			File file = new File(fpath);
			// If file does not exists, then create it
			if (!file.exists()) {
				file.createNewFile();
			}

			font = new Font(FontFamily.HELVETICA, 10, Font.BOLD, BaseColor.BLACK);
			textfont= new Font(FontFamily.HELVETICA, 10, Font.BOLD, BaseColor.BLACK);
			//step 1
			document = new Document();
			// step 2
			writer =PdfWriter.getInstance(document,
					new FileOutputStream(file.getAbsoluteFile()));


/*
			Rectangle rect = new Rectangle(30, 30, 550, 800);
			writer.setBoxSize("art", rect);
			HeaderFooterPageEvent event = new HeaderFooterPageEvent();
			writer.setPageEvent(event);*/

			// step 3
			document.open();
			// step 4

			/* document.add(new Paragraph("This is Page One"));
		     document.newPage();
		     document.add(new Paragraph("This is Page two"));*/


			/*PdfContentByte name = writer.getDirectContent();
			PdfPTable nametable = new PdfPTable(2);
			nametable.setTotalWidth(504);

			PdfPCell patientname = new PdfPCell(new Phrase("PATIENT NAME:"+" "+combinedobject.getString("patientname"),font));
			PdfPCell cellTwo = new PdfPCell(new Phrase("DOCTOR NAME:"+" "+"Dr."+" "+combinedobject.getString("doctername"),font));

			symptomstable.addCell(cellOne);
			symptomstable.addCell(cellTwo);*/


			absText2("PATIENT NAME:"+" "+combinedobject.getString("patientname"),40, 670, 10);

			absText2("DOCTOR NAME:"+" "+"Dr."+" "+combinedobject.getString("doctername"),300, 670, 10);

			absText2("PATIENT ID:"+" "+combinedobject.getString("id"),40, 640, 10);

			absText2("DATE:"+" "+combinedobject.getString("createdAt"),300,640,10);

			absText2("VITALS",40,610,10);



			/*absText1("Weight",40,610,13);
			absText1("Systolic/Diastolic",120,610,13);
			absText1("Pulse",230,610,13);
			absText1("Respiratory Rate",310,610,13);
			absText1("SP O2",430,610,13);
			absText1("Temperature",520,610,13);
			 */
			InputStream weightstream = context.getAssets().open("weight.png");
			Bitmap weightbmp = BitmapFactory.decodeStream(weightstream);
			ByteArrayOutputStream streamweight = new ByteArrayOutputStream();
			weightbmp.compress(Bitmap.CompressFormat.PNG, 100, streamweight);
			Image weightLogo = Image.getInstance(streamweight.toByteArray());


			weightLogo.setAbsolutePosition(50,580);
			weightLogo.scalePercent(25);
			document.add(weightLogo); 

			if(combinedobject.getString("weight_result")!=null && combinedobject.getString("weight_result")!="Nil"){
				absText(combinedobject.getString("weight_result")+" "+"kg",40,560,10);
			}else{
				absText("--"+" "+"kg",50,560,10);
			}

			InputStream pressureStream = context.getAssets().open("bloodpressure.png");
			Bitmap pressurebmp = BitmapFactory.decodeStream(pressureStream);
			ByteArrayOutputStream streampressure= new ByteArrayOutputStream();
			pressurebmp.compress(Bitmap.CompressFormat.PNG, 100, streampressure);
			Image pressureStreamLogo = Image.getInstance(streampressure.toByteArray());
			pressureStreamLogo.setAbsolutePosition(140,580);
			pressureStreamLogo.scalePercent(25);
			document.add(pressureStreamLogo); 

			if(combinedobject.getString("bloodPressure_result")!=null && combinedobject.getString("bloodPressure_result")!="Nil"){
				absText(combinedobject.getString("bloodPressure_result")+" "+"Hg mm",110,560,10);
			}else{
				absText("--"+" "+"Hg mm",120,560,10);
			}

			InputStream pulseStream = context.getAssets().open("pulse.png");
			Bitmap pulsebmp = BitmapFactory.decodeStream(pulseStream);
			ByteArrayOutputStream streampulse = new ByteArrayOutputStream();
			pulsebmp.compress(Bitmap.CompressFormat.PNG, 100, streampulse);
			Image pulseLogo = Image.getInstance(streampulse.toByteArray());
			pulseLogo.setAbsolutePosition(240,580);
			pulseLogo.scalePercent(25);
			document.add(pulseLogo); 

			if(combinedobject.getString("pulse_result")!=null && combinedobject.getString("pulse_result")!="Nil"){
				absText(combinedobject.getString("pulse_result")+" "+"beats/min",210,560,10);
			}else{
				absText("--"+" "+"beats/min",220,560,10);
			}

			InputStream respiratoryrateinputStream = context.getAssets().open("respiratoryrate.png");
			Bitmap respiratoryratebmp = BitmapFactory.decodeStream(respiratoryrateinputStream);
			ByteArrayOutputStream streamrespiratoryrate = new ByteArrayOutputStream();
			respiratoryratebmp.compress(Bitmap.CompressFormat.PNG, 100, streamrespiratoryrate);
			Image respiratoryrateLogo = Image.getInstance(streamrespiratoryrate.toByteArray());
			respiratoryrateLogo.setAbsolutePosition(340,580);
			respiratoryrateLogo.scalePercent(25);
			document.add(respiratoryrateLogo); 


			if(combinedobject.getString("respRate_result")!=null && combinedobject.getString("respRate_result")!="Nil"){
				absText(combinedobject.getString("respRate_result")+" "+"breaths/min",300,560,10);
			}else{
				absText("--"+" "+"breaths/min",310,560,10);
			}

			InputStream spo2inputStream = context.getAssets().open("spo2.png");
			Bitmap spo2bmp = BitmapFactory.decodeStream(spo2inputStream);
			ByteArrayOutputStream streamspo2 = new ByteArrayOutputStream();
			spo2bmp.compress(Bitmap.CompressFormat.PNG, 100, streamspo2);
			Image spo2Logo = Image.getInstance(streamspo2.toByteArray());
			spo2Logo.setAbsolutePosition(440,580);
			spo2Logo.scalePercent(25);
			document.add(spo2Logo); 

			if(combinedobject.getString("spo2_result")!=null && combinedobject.getString("spo2_result")!="Nil"){
				absText(combinedobject.getString("spo2_result")+" "+"%",430,560,10);
			}else{
				absText("--"+" "+"%",440,560,10);
			}

			InputStream temperatureinputStream = context.getAssets().open("temperature.png");
			Bitmap temperaturebmp = BitmapFactory.decodeStream(temperatureinputStream);
			ByteArrayOutputStream streamtemperature = new ByteArrayOutputStream();
			temperaturebmp.compress(Bitmap.CompressFormat.PNG, 100, streamtemperature);
			Image temperatureLogo = Image.getInstance(streamtemperature.toByteArray());
			temperatureLogo.setAbsolutePosition(530,580);
			temperatureLogo.scalePercent(25);
			document.add(temperatureLogo); 

			if(combinedobject.getString("temperature")!=null && combinedobject.getString("temperature")!="Nil"){
				absText(combinedobject.getString("temperature")+" "+"c",520,560,10);
			}else{
				absText("--"+" "+"c",520,560,10);
			}


			PdfContentByte cb4 = writer.getDirectContent();
			PdfPTable symptomstable = new PdfPTable(2);
			symptomstable.setTotalWidth(504);



			PdfPCell cellOne = new PdfPCell(new Phrase("SYMPTOMS",font));
			PdfPCell cellTwo = new PdfPCell(new Phrase("SYNDROMES",font));
			PdfPCell cellThree;
			PdfPCell cellFour;


			cellOne.setBorder(Rectangle.NO_BORDER);
			cellTwo.setBorder(Rectangle.NO_BORDER);
			/*symptomstable.addCell("Symptoms");
			symptomstable.addCell("Symptoms");*/
			if(combinedobject.getString("symptoms")!=null){
				cellThree = new PdfPCell(new Phrase(combinedobject.getString("symptoms")));

				//symptomstable.addCell(combinedobject.getString("symptoms"));
			}else{
				cellThree = new PdfPCell(new Phrase("Nil"));
				//symptomstable.addCell("Nil");
			}
			cellThree.setBorder(Rectangle.NO_BORDER);
			if(combinedobject.getString("syndromes")!=null){

				cellFour = new PdfPCell(new Phrase(combinedobject.getString("syndromes")));

				//symptomstable.addCell(combinedobject.getString("syndromes"));
			}else{

				cellFour = new PdfPCell(new Phrase("Nil"));
				//symptomstable.addCell("Nil");
			}
			cellFour.setBorder(Rectangle.NO_BORDER);

			symptomstable.addCell(cellOne);
			symptomstable.addCell(cellTwo);
			symptomstable.addCell(cellThree);
			symptomstable.addCell(cellFour);

			symptomstable.writeSelectedRows(0, -1, 40, 550, cb4);



			PdfContentByte cb3 = writer.getDirectContent();
			PdfPTable diagnosistable = new PdfPTable(2);
			diagnosistable.setTotalWidth(504);

			PdfPCell diagnosiscellOne = new PdfPCell(new Phrase("DISEASE",font));
			PdfPCell diagnosiscellTwo = new PdfPCell(new Phrase("ADDITIONAL COMMENTS",font));
			PdfPCell diagnosiscellThree;
			PdfPCell diagnosiscellFour;

			diagnosiscellOne.setBorder(Rectangle.NO_BORDER);
			diagnosiscellTwo.setBorder(Rectangle.NO_BORDER);
			/*PdfPCell diagnosiscell;
			diagnosistable.addCell("Disease");
			diagnosistable.addCell("Additional Comments");*/
			if(combinedobject.getString("suspectedDisease")!=null){
				diagnosiscellThree= new PdfPCell(new Phrase(combinedobject.getString("suspectedDisease")));
				//diagnosistable.addCell(combinedobject.getString("suspectedDisease"));
			}else{
				//diagnosistable.addCell("Nil");

				diagnosiscellThree= new PdfPCell(new Phrase("Nil"));

			}
			diagnosiscellThree.setBorder(Rectangle.NO_BORDER);
			if(combinedobject.getString("additionalComments")!=null){
				//diagnosistable.addCell(combinedobject.getString("additionalComments"));

				diagnosiscellFour=new PdfPCell(new Phrase(combinedobject.getString("additionalComments")));

			}else{
				//diagnosistable.addCell("Nil");

				diagnosiscellFour=new PdfPCell(new Phrase("Nil"));
			}
			diagnosiscellFour.setBorder(Rectangle.NO_BORDER);

			diagnosistable.addCell(diagnosiscellOne);
			diagnosistable.addCell(diagnosiscellTwo);
			diagnosistable.addCell(diagnosiscellThree);
			diagnosistable.addCell(diagnosiscellFour);



			diagnosistable.writeSelectedRows(0, -1, 40, 550-(symptomstable.getTotalHeight()+30), cb3);



			PdfContentByte cb2 = writer.getDirectContent();

			PdfPTable table = new PdfPTable(5);
			table.setTotalWidth(480);

			PdfPCell cell;
			PdfPCell drug= new PdfPCell(new Phrase("DRUGS",font));
			drug.setHorizontalAlignment(Element.ALIGN_CENTER);
			drug.setBorderWidth(.1f);

			PdfPCell dosage= new PdfPCell(new Phrase("DOSAGE",font));
			dosage.setHorizontalAlignment(Element.ALIGN_CENTER);
			dosage.setBorderWidth(.1f);


			PdfPCell startdate= new PdfPCell(new Phrase("START DATE",font));
			startdate.setHorizontalAlignment(Element.ALIGN_CENTER);
			startdate.setBorderWidth(.1f);


			/*PdfPCell duration= new PdfPCell(new Phrase("DURATION",font));;
			duration.setHorizontalAlignment(Element.ALIGN_CENTER);*/
			PdfPCell days= new PdfPCell(new Phrase("DAYS",font));;
			days.setHorizontalAlignment(Element.ALIGN_CENTER);
			days.setBorderWidth(.1f);


			PdfPCell frequency= new PdfPCell(new Phrase("FREQUENCY",font));
			frequency.setHorizontalAlignment(Element.ALIGN_CENTER);
			frequency.setBorderWidth(.1f);

			// we add a cell with colspan 3
			cell = new PdfPCell(new Phrase("Cell with colspan 3"));
			/*cell.setColspan(3);
			table.addCell(cell);
			// now we add a cell with rowspan 2
			cell = new PdfPCell(new Phrase("Cell with rowspan 2"));
			cell.setRowspan(2);
			table.addCell(cell);*/
			// we add the four remaining cells with addCell()
			table.addCell(drug);
			table.addCell(dosage);
			table.addCell(startdate);
			/*table.addCell(duration);*/
			table.addCell(days);
			table.addCell(frequency);



			if(combinedobject.getJSONArray("drug")!=null){
				for(int j=0;j<combinedobject.getJSONArray("drug").length();j++){
					try {
						PdfPCell drugcell=new PdfPCell(new Phrase(combinedobject.getJSONArray("drug").getString(j)));
						drugcell.setHorizontalAlignment(Element.ALIGN_CENTER);
						drugcell.setBorderWidth(.1f);


						PdfPCell dosagecell=new PdfPCell(new Phrase(combinedobject.getJSONArray("dosage").getString(j)+" "+"mg"));
						dosagecell.setHorizontalAlignment(Element.ALIGN_CENTER);
						dosagecell.setBorderWidth(.1f);


						PdfPCell drugStartDatecell=new PdfPCell(new Phrase(combinedobject.getJSONArray("drugStartDate").getString(j)));
						drugStartDatecell.setHorizontalAlignment(Element.ALIGN_CENTER);
						drugStartDatecell.setBorderWidth(.1f);

						PdfPCell dayscell;
						PdfPCell quantitycell;


						table.addCell(drugcell);
						table.addCell(dosagecell);
						table.addCell(drugStartDatecell);

						if(combinedobject.getJSONArray("days").getString(j).endsWith(",")){
							String str=combinedobject.getJSONArray("days").getString(j);
							if (str.endsWith(",")) {
								str = str.substring(0, str.length() - 1);

								dayscell=new PdfPCell(new Phrase(str));
								dayscell.setHorizontalAlignment(Element.ALIGN_CENTER);
								dayscell.setBorderWidth(.1f);
								table.addCell(dayscell);
							}
						}else{
							dayscell=new PdfPCell(new Phrase(combinedobject.getJSONArray("days").getString(j)));
							dayscell.setHorizontalAlignment(Element.ALIGN_CENTER);
							dayscell.setBorderWidth(.1f);
							table.addCell(dayscell);
							//table.addCell(combinedobject.getJSONArray("days").getString(j));
						}

						if(combinedobject.getJSONArray("quantity").getString(j).endsWith(",")){
							String str=combinedobject.getJSONArray("quantity").getString(j).replaceAll("\\(.*?\\)","");

							if (str.endsWith(",")) {
								str = str.substring(0, str.length() - 1);

								quantitycell=new PdfPCell(new Phrase(str));
								quantitycell.setHorizontalAlignment(Element.ALIGN_CENTER);
								quantitycell.setBorderWidth(.1f);
								table.addCell(quantitycell);
							}
						}else{
							String str=combinedobject.getJSONArray("quantity").getString(j).replaceAll("\\(.*?\\)","");
							quantitycell=new PdfPCell(new Phrase(str));
							quantitycell.setHorizontalAlignment(Element.ALIGN_CENTER);
							quantitycell.setBorderWidth(.1f);
							table.addCell(quantitycell);

							//table.addCell(combinedobject.getJSONArray("quantity").getString(j));
						}
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}

			//250
			System.out.println("table height"+table.getTotalHeight());
			
			/*float diseasetablestart;*/
			if(table.getTotalHeight()<200){


				table.writeSelectedRows(0, -1, 40, (550-(symptomstable.getTotalHeight()+30))-(diagnosistable.getTotalHeight()+30), cb2);
				float diseasetablestart=(550-(symptomstable.getTotalHeight()+30))-(diagnosistable.getTotalHeight()+30);

				
				if(table.getTotalHeight()<=100){
					
					
					PdfContentByte cb5 = writer.getDirectContent();
					PdfPTable treatmenttable = new PdfPTable(2);
					treatmenttable.setTotalWidth(504);



					PdfPCell treatmentcellOne = new PdfPCell(new Phrase("TREATMENT",font));
					PdfPCell treatmentcellTwo = new PdfPCell(new Phrase("FOLLOWUP",font));
					PdfPCell treatmentcellThree;
					PdfPCell treatmentcellFour;



					treatmentcellOne.setBorder(Rectangle.NO_BORDER);
					treatmentcellTwo.setBorder(Rectangle.NO_BORDER);
					/*symptomstable.addCell("Symptoms");
				symptomstable.addCell("Symptoms");*/
					if(combinedobject.getString("treatment")!=null){
						treatmentcellThree = new PdfPCell(new Phrase(combinedobject.getString("treatment")));

						//symptomstable.addCell(combinedobject.getString("symptoms"));
					}else{
						treatmentcellThree = new PdfPCell(new Phrase("Nil"));
						//symptomstable.addCell("Nil");
					}
					treatmentcellThree.setBorder(Rectangle.NO_BORDER);
					if(combinedobject.getString("followup")!=null){

						treatmentcellFour = new PdfPCell(new Phrase(combinedobject.getString("followup")));

						//symptomstable.addCell(combinedobject.getString("syndromes"));
					}else{

						treatmentcellFour = new PdfPCell(new Phrase("Nil"));
						//symptomstable.addCell("Nil");
					}
					treatmentcellFour.setBorder(Rectangle.NO_BORDER);

					treatmenttable.addCell(treatmentcellOne);
					treatmenttable.addCell(treatmentcellTwo);
					treatmenttable.addCell(treatmentcellThree);
					treatmenttable.addCell(treatmentcellFour);
					//60
					treatmenttable.writeSelectedRows(0, -1, 40,diseasetablestart-(table.getTotalHeight()+20) , cb5);



					InputStream docterStream = context.getAssets().open("app_icon_dd_pdfimage.png");
					Bitmap docterbmp = BitmapFactory.decodeStream(docterStream);
					ByteArrayOutputStream docterlogo = new ByteArrayOutputStream();
					docterbmp.compress(Bitmap.CompressFormat.PNG, 100, docterlogo);
					Image docterLogo = Image.getInstance(docterlogo.toByteArray());
					docterLogo.setAbsolutePosition(55,(diseasetablestart-(table.getTotalHeight()+20))-(treatmenttable.getTotalHeight()+60));

					docterLogo.scalePercent(35);

					document.add(docterLogo); 
					int size= (int) (diseasetablestart-(table.getTotalHeight()+20)-(treatmenttable.getTotalHeight()+80));
					absText2("Building a Healthier and Stronger Nation!", 40,size, 10);
					
					
					
				}
				
				


			}

			else{
				int firsttable;
				int secondtable;
				int tablerownumber=table.getRows().size();
				
				System.out.println("table rowsss"+tablerownumber);

				
					
					firsttable=2;
					secondtable=tablerownumber-firsttable;
					
			
				

				PdfContentByte cb21 = writer.getDirectContent();

				PdfPTable table1 = new PdfPTable(5);
				table1.setTotalWidth(480);

				PdfPCell cell1;
				PdfPCell drug1= new PdfPCell(new Phrase("DRUGS",font));
				drug1.setHorizontalAlignment(Element.ALIGN_CENTER);
				drug1.setBorderWidth(.1f);

				PdfPCell dosage1= new PdfPCell(new Phrase("DOSAGE",font));
				dosage1.setHorizontalAlignment(Element.ALIGN_CENTER);
				dosage1.setBorderWidth(.1f);


				PdfPCell startdate1= new PdfPCell(new Phrase("START DATE",font));
				startdate1.setHorizontalAlignment(Element.ALIGN_CENTER);
				startdate1.setBorderWidth(.1f);


				/*PdfPCell duration= new PdfPCell(new Phrase("DURATION",font));;
				duration.setHorizontalAlignment(Element.ALIGN_CENTER);*/
				PdfPCell days1= new PdfPCell(new Phrase("DAYS",font));;
				days1.setHorizontalAlignment(Element.ALIGN_CENTER);
				days1.setBorderWidth(.1f);


				PdfPCell frequency1= new PdfPCell(new Phrase("FREQUENCY",font));
				frequency1.setHorizontalAlignment(Element.ALIGN_CENTER);
				frequency1.setBorderWidth(.1f);

				// we add a cell with colspan 3
				cell = new PdfPCell(new Phrase("Cell with colspan 3"));
				/*cell.setColspan(3);
				table.addCell(cell);
				// now we add a cell with rowspan 2
				cell = new PdfPCell(new Phrase("Cell with rowspan 2"));
				cell.setRowspan(2);
				table.addCell(cell);*/
				// we add the four remaining cells with addCell()
				table1.addCell(drug1);
				table1.addCell(dosage1);
				table1.addCell(startdate1);
				/*table.addCell(duration);*/
				table1.addCell(days1);
				table1.addCell(frequency1);



				if(combinedobject.getJSONArray("drug")!=null){
					for(int j=0;j<combinedobject.getJSONArray("drug").length()-2;j++){
						try {
							PdfPCell drugcell=new PdfPCell(new Phrase(combinedobject.getJSONArray("drug").getString(j)));
							drugcell.setHorizontalAlignment(Element.ALIGN_CENTER);
							drugcell.setBorderWidth(.1f);


							PdfPCell dosagecell=new PdfPCell(new Phrase(combinedobject.getJSONArray("dosage").getString(j)+" "+"mg"));
							dosagecell.setHorizontalAlignment(Element.ALIGN_CENTER);
							dosagecell.setBorderWidth(.1f);


							PdfPCell drugStartDatecell=new PdfPCell(new Phrase(combinedobject.getJSONArray("drugStartDate").getString(j)));
							drugStartDatecell.setHorizontalAlignment(Element.ALIGN_CENTER);
							drugStartDatecell.setBorderWidth(.1f);

							PdfPCell dayscell;
							PdfPCell quantitycell;


							table1.addCell(drugcell);
							table1.addCell(dosagecell);
							table1.addCell(drugStartDatecell);

							if(combinedobject.getJSONArray("days").getString(j).endsWith(",")){
								String str=combinedobject.getJSONArray("days").getString(j);
								if (str.endsWith(",")) {
									str = str.substring(0, str.length() - 1);

									dayscell=new PdfPCell(new Phrase(str));
									dayscell.setHorizontalAlignment(Element.ALIGN_CENTER);
									dayscell.setBorderWidth(.1f);
									table1.addCell(dayscell);
								}
							}else{
								dayscell=new PdfPCell(new Phrase(combinedobject.getJSONArray("days").getString(j)));
								dayscell.setHorizontalAlignment(Element.ALIGN_CENTER);
								dayscell.setBorderWidth(.1f);
								table1.addCell(dayscell);
								//table.addCell(combinedobject.getJSONArray("days").getString(j));
							}

							if(combinedobject.getJSONArray("quantity").getString(j).endsWith(",")){
								String str=combinedobject.getJSONArray("quantity").getString(j).replaceAll("\\(.*?\\)","");

								if (str.endsWith(",")) {
									str = str.substring(0, str.length() - 1);

									quantitycell=new PdfPCell(new Phrase(str));
									quantitycell.setHorizontalAlignment(Element.ALIGN_CENTER);
									quantitycell.setBorderWidth(.1f);
									table1.addCell(quantitycell);
								}
							}else{
								String str=combinedobject.getJSONArray("quantity").getString(j).replaceAll("\\(.*?\\)","");
								quantitycell=new PdfPCell(new Phrase(str));
								quantitycell.setHorizontalAlignment(Element.ALIGN_CENTER);
								quantitycell.setBorderWidth(.1f);
								table1.addCell(quantitycell);

							}
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}

				
				System.out.println("table1 height"+table1.getTotalHeight());
				table1.writeSelectedRows(0, -1, 40, (550-(symptomstable.getTotalHeight()+30))-(diagnosistable.getTotalHeight()+30), cb21);

				document.newPage();

				PdfContentByte cb22 = writer.getDirectContent();

				PdfPTable table2 = new PdfPTable(5);
				table2.setTotalWidth(480);

				PdfPCell cell2;
				PdfPCell drug2= new PdfPCell(new Phrase("DRUGS",font));
				drug2.setHorizontalAlignment(Element.ALIGN_CENTER);
				drug2.setBorderWidth(.1f);

				PdfPCell dosage2= new PdfPCell(new Phrase("DOSAGE",font));
				dosage2.setHorizontalAlignment(Element.ALIGN_CENTER);
				dosage2.setBorderWidth(.1f);


				PdfPCell startdate2= new PdfPCell(new Phrase("START DATE",font));
				startdate2.setHorizontalAlignment(Element.ALIGN_CENTER);
				startdate2.setBorderWidth(.1f);


				/*PdfPCell duration= new PdfPCell(new Phrase("DURATION",font));;
				duration.setHorizontalAlignment(Element.ALIGN_CENTER);*/
				PdfPCell days2= new PdfPCell(new Phrase("DAYS",font));;
				days2.setHorizontalAlignment(Element.ALIGN_CENTER);
				days2.setBorderWidth(.1f);


				PdfPCell frequency2= new PdfPCell(new Phrase("FREQUENCY",font));
				frequency2.setHorizontalAlignment(Element.ALIGN_CENTER);
				frequency2.setBorderWidth(.1f);

				// we add a cell with colspan 3
				cell = new PdfPCell(new Phrase("Cell with colspan 3"));
				/*cell.setColspan(3);
				table.addCell(cell);
				// now we add a cell with rowspan 2
				cell = new PdfPCell(new Phrase("Cell with rowspan 2"));
				cell.setRowspan(2);
				table.addCell(cell);*/
				// we add the four remaining cells with addCell()
				table2.addCell(drug2);
				table2.addCell(dosage2);
				table2.addCell(startdate2);
				/*table.addCell(duration);*/
				table2.addCell(days2);
				table2.addCell(frequency2);



				if(combinedobject.getJSONArray("drug")!=null){
					for(int j=2;j<combinedobject.getJSONArray("drug").length();j++){
						try {
							PdfPCell drugcell=new PdfPCell(new Phrase(combinedobject.getJSONArray("drug").getString(j)));
							drugcell.setHorizontalAlignment(Element.ALIGN_CENTER);
							drugcell.setBorderWidth(.1f);


							PdfPCell dosagecell=new PdfPCell(new Phrase(combinedobject.getJSONArray("dosage").getString(j)+" "+"mg"));
							dosagecell.setHorizontalAlignment(Element.ALIGN_CENTER);
							dosagecell.setBorderWidth(.1f);


							PdfPCell drugStartDatecell=new PdfPCell(new Phrase(combinedobject.getJSONArray("drugStartDate").getString(j)));
							drugStartDatecell.setHorizontalAlignment(Element.ALIGN_CENTER);
							drugStartDatecell.setBorderWidth(.1f);

							PdfPCell dayscell;
							PdfPCell quantitycell;


							table2.addCell(drugcell);
							table2.addCell(dosagecell);
							table2.addCell(drugStartDatecell);

							if(combinedobject.getJSONArray("days").getString(j).endsWith(",")){
								String str=combinedobject.getJSONArray("days").getString(j);
								if (str.endsWith(",")) {
									str = str.substring(0, str.length() - 1);

									dayscell=new PdfPCell(new Phrase(str));
									dayscell.setHorizontalAlignment(Element.ALIGN_CENTER);
									dayscell.setBorderWidth(.1f);
									table2.addCell(dayscell);
								}
							}else{
								dayscell=new PdfPCell(new Phrase(combinedobject.getJSONArray("days").getString(j)));
								dayscell.setHorizontalAlignment(Element.ALIGN_CENTER);
								dayscell.setBorderWidth(.1f);
								table2.addCell(dayscell);
								//table.addCell(combinedobject.getJSONArray("days").getString(j));
							}

							if(combinedobject.getJSONArray("quantity").getString(j).endsWith(",")){
								String str=combinedobject.getJSONArray("quantity").getString(j).replaceAll("\\(.*?\\)","");

								if (str.endsWith(",")) {
									str = str.substring(0, str.length() - 1);

									quantitycell=new PdfPCell(new Phrase(str));
									quantitycell.setHorizontalAlignment(Element.ALIGN_CENTER);
									quantitycell.setBorderWidth(.1f);
									table2.addCell(quantitycell);
								}
							}else{
								String str=combinedobject.getJSONArray("quantity").getString(j).replaceAll("\\(.*?\\)","");
								quantitycell=new PdfPCell(new Phrase(str));
								quantitycell.setHorizontalAlignment(Element.ALIGN_CENTER);
								quantitycell.setBorderWidth(.1f);
								table2.addCell(quantitycell);

							}
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}

				table2.writeSelectedRows(0, -1, 40, 670, cb22);




				//float diseasetablestart=(670-(symptomstable.getTotalHeight()+30))-(diagnosistable.getTotalHeight()+30);
				float diseasetablestart=(670-table2.getTotalHeight());

				PdfContentByte cb5 = writer.getDirectContent();
				PdfPTable treatmenttable = new PdfPTable(2);
				treatmenttable.setTotalWidth(504);



				PdfPCell treatmentcellOne = new PdfPCell(new Phrase("TREATMENT",font));
				PdfPCell treatmentcellTwo = new PdfPCell(new Phrase("FOLLOWUP",font));
				PdfPCell treatmentcellThree;
				PdfPCell treatmentcellFour;



				treatmentcellOne.setBorder(Rectangle.NO_BORDER);
				treatmentcellTwo.setBorder(Rectangle.NO_BORDER);
				/*symptomstable.addCell("Symptoms");
			symptomstable.addCell("Symptoms");*/
				if(combinedobject.getString("treatment")!=null){
					treatmentcellThree = new PdfPCell(new Phrase(combinedobject.getString("treatment")));

					//symptomstable.addCell(combinedobject.getString("symptoms"));
				}else{
					treatmentcellThree = new PdfPCell(new Phrase("Nil"));
					//symptomstable.addCell("Nil");
				}
				treatmentcellThree.setBorder(Rectangle.NO_BORDER);
				if(combinedobject.getString("followup")!=null){

					treatmentcellFour = new PdfPCell(new Phrase(combinedobject.getString("followup")));

					//symptomstable.addCell(combinedobject.getString("syndromes"));
				}else{

					treatmentcellFour = new PdfPCell(new Phrase("Nil"));
					//symptomstable.addCell("Nil");
				}
				treatmentcellFour.setBorder(Rectangle.NO_BORDER);

				treatmenttable.addCell(treatmentcellOne);
				treatmenttable.addCell(treatmentcellTwo);
				treatmenttable.addCell(treatmentcellThree);
				treatmenttable.addCell(treatmentcellFour);
				//60
				treatmenttable.writeSelectedRows(0, -1, 40,(diseasetablestart-30) , cb5);



				InputStream docterStream = context.getAssets().open("app_icon_dd_pdfimage.png");
				Bitmap docterbmp = BitmapFactory.decodeStream(docterStream);
				ByteArrayOutputStream docterlogo = new ByteArrayOutputStream();
				docterbmp.compress(Bitmap.CompressFormat.PNG, 100, docterlogo);
				Image docterLogo = Image.getInstance(docterlogo.toByteArray());
				docterLogo.setAbsolutePosition(55,(diseasetablestart-30)-(treatmenttable.getTotalHeight()+40));

				docterLogo.scalePercent(35);

				document.add(docterLogo); 
				int size= (int) ((diseasetablestart-30)-(treatmenttable.getTotalHeight()+50));
				absText2("Building a Healthier and Stronger Nation!", 40,size, 10);


				/*
			HeaderFooterPageEvent event = new HeaderFooterPageEvent();
			writer.setPageEvent(event);
	        document.open();
	        document.add(new Paragraph("This is Page One"));
	        document.newPage();
	        document.add(new Paragraph("This is Page two"));*/


			}
			
			if(table.getTotalHeight()<200 && table.getTotalHeight()>100 ){
				document.newPage();
				
				PdfContentByte cb5 = writer.getDirectContent();
				PdfPTable treatmenttable = new PdfPTable(2);
				treatmenttable.setTotalWidth(504);



				PdfPCell treatmentcellOne = new PdfPCell(new Phrase("TREATMENT",font));
				PdfPCell treatmentcellTwo = new PdfPCell(new Phrase("FOLLOWUP",font));
				PdfPCell treatmentcellThree;
				PdfPCell treatmentcellFour;



				treatmentcellOne.setBorder(Rectangle.NO_BORDER);
				treatmentcellTwo.setBorder(Rectangle.NO_BORDER);
				/*symptomstable.addCell("Symptoms");
			symptomstable.addCell("Symptoms");*/
				if(combinedobject.getString("treatment")!=null){
					treatmentcellThree = new PdfPCell(new Phrase(combinedobject.getString("treatment")));

					//symptomstable.addCell(combinedobject.getString("symptoms"));
				}else{
					treatmentcellThree = new PdfPCell(new Phrase("Nil"));
					//symptomstable.addCell("Nil");
				}
				treatmentcellThree.setBorder(Rectangle.NO_BORDER);
				if(combinedobject.getString("followup")!=null){

					treatmentcellFour = new PdfPCell(new Phrase(combinedobject.getString("followup")));

					//symptomstable.addCell(combinedobject.getString("syndromes"));
				}else{

					treatmentcellFour = new PdfPCell(new Phrase("Nil"));
					//symptomstable.addCell("Nil");
				}
				treatmentcellFour.setBorder(Rectangle.NO_BORDER);

				treatmenttable.addCell(treatmentcellOne);
				treatmenttable.addCell(treatmentcellTwo);
				treatmenttable.addCell(treatmentcellThree);
				treatmenttable.addCell(treatmentcellFour);
				//60
				treatmenttable.writeSelectedRows(0, -1, 40,670 , cb5);



				InputStream docterStream = context.getAssets().open("app_icon_dd_pdfimage.png");
				Bitmap docterbmp = BitmapFactory.decodeStream(docterStream);
				ByteArrayOutputStream docterlogo = new ByteArrayOutputStream();
				docterbmp.compress(Bitmap.CompressFormat.PNG, 100, docterlogo);
				Image docterLogo = Image.getInstance(docterlogo.toByteArray());
				docterLogo.setAbsolutePosition(55,670-(treatmenttable.getTotalHeight()+60));

				docterLogo.scalePercent(35);

				document.add(docterLogo); 
				int size= (int) (670-(treatmenttable.getTotalHeight()+80));
				absText2("Building a Healthier and Stronger Nation!", 40,size, 10);

				
				
				
			}

			document.close();

			Log.d("Suceess", "Sucess");
			return true;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}

	public String read(String fname) {
		BufferedReader br = null;
		String response = null;
		try {
			StringBuffer output = new StringBuffer();
			String fpath = "/sdcard/" + fname + ".pdf";

			PdfReader reader = new PdfReader(new FileInputStream(fpath));
			PdfReaderContentParser parser = new PdfReaderContentParser(reader);

			StringWriter strW = new StringWriter();

			TextExtractionStrategy strategy;
			for (int i = 1; i <= reader.getNumberOfPages(); i++) {
				strategy = parser.processContent(i,
						new SimpleTextExtractionStrategy());

				strW.write(strategy.getResultantText());

			}

			response = strW.toString();

		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
		return response;
	}

	private static void absText(String text, int x, int y,int size){

		try {
			PdfContentByte cb = writer.getDirectContent();
			BaseFont bf = BaseFont.createFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
			cb.saveState();
			cb.beginText();
			cb.moveText(x, y);
			cb.setFontAndSize(bf, size);
			cb.showText(text);
			cb.endText();




			cb.restoreState();



		} catch (DocumentException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}


	private static void absText1(String text, int x, int y,int size){

		try {
			PdfContentByte cb = writer.getDirectContent();
			BaseFont bf = BaseFont.createFont(BaseFont.TIMES_BOLD, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
			cb.saveState();
			cb.beginText();
			cb.moveText(x, y);
			cb.setFontAndSize(bf, size);
			cb.showText(text);
			cb.endText();



			cb.restoreState();


		} catch (DocumentException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}


	private static void absText2(String text, int x, int y,int size){

		try {
			PdfContentByte cb = writer.getDirectContent();
			BaseFont bf = BaseFont.createFont(BaseFont.HELVETICA_BOLD, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
			cb.saveState();
			cb.beginText();
			cb.moveText(x, y);
			cb.setFontAndSize(bf, size);
			cb.showText(text);
			cb.endText();



			cb.restoreState();



		} catch (Exception e) {
			e.printStackTrace();
		}
	}





	public String addLinebreaks(String input, int maxLineLength) {
		StringTokenizer tok = new StringTokenizer(input, " ");
		StringBuilder output = new StringBuilder(input.length());
		int lineLen = 0;
		while (tok.hasMoreTokens()) {
			String word = tok.nextToken();

			if (lineLen + word.length() > maxLineLength) {
				output.append("\n");
				lineLen = 0;
			}
			output.append(word);
			lineLen += word.length();
		}
		return output.toString();
	}

}
