package com.wiinnova.doctorsdiary.supportclasses;

public class prescriptionelements {

	private String drug;
	private String dosage;
	private String startdate;
	private String duration;
	private String days;
	private String quantity;

	public prescriptionelements(String drug,String dosage,String startdate,String duration,String days,String quantity) {
		// TODO Auto-generated constructor stub
		this.drug=drug;
		this.dosage=dosage;
		this.startdate=startdate;
		this.duration=duration;
		this.days=days;
		this.quantity=quantity;

	}


	public String getDrug() {
		return drug;
	}

	public void setDrug(String drug) {
		this.drug = drug;
	}

	public String getDosage() {
		return dosage;
	}

	public void setDosage(String dosage) {
		this.dosage = dosage;
	}

	public String getStartdate() {
		return startdate;
	}

	public void setStartdate(String startdate) {
		this.startdate = startdate;
	}

	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	public String getDays() {
		return days;
	}

	public void setDays(String days) {
		this.days = days;
	}

	public String getQuantity() {
		return quantity;
	}

	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}


}
