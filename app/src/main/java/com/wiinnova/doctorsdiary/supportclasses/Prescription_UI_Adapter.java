package com.wiinnova.doctorsdiary.supportclasses;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.wiinnova.doctorsdiary.R;
import com.wiinnova.doctorsdiary.activities.FragmentActivityContainer;
import com.wiinnova.doctorsdiary.popupwindow.DaysSelector_Popup;
import com.wiinnova.doctorsdiary.popupwindow.DaysSelector_Popup.onDaysSelectListener;
import com.wiinnova.doctorsdiary.popupwindow.SpinnerPopup;
import com.wiinnova.doctorsdiary.popupwindow.SpinnerPopup.onSubmitListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class Prescription_UI_Adapter extends ArrayAdapter<prescriptionelements>{


	int count=4;
	Context context;
	private  int mResource;
	String[] quantity_array;
	List<prescriptionelements> objects1;
	SpinnerPopup spObj;
	private onDaysSelectListener daysSelectedListener;
	DaysSelector_Popup daysselectObj;
	prescriptionelements elements;
	String selecteddays[];
	EditText Drugname;
	private DatePickerDialog startDatePickerDialog;
	private SimpleDateFormat dateFormatter;
	private onSubmitListener quantitySelectedListener;
	ArrayList<String> selected_days=new ArrayList<String>();
	ArrayList<String> selected_days_medicine=new ArrayList<String>();
	View quantityview;


	public Prescription_UI_Adapter(Context context,int ViewResource, List<prescriptionelements> objects) {
		super(context, ViewResource, objects);
		// TODO Auto-generated constructor stub
		this.context=context;
		this.mResource=ViewResource;

		Log.e("==================", String.valueOf(count));

		objects1=objects;

		daysSelectedListener = new onDaysSelectListener() {

			@Override
			public void onDays(String[] outputStrArr, int selectedrow) {
				// TODO Auto-generated method stub
				doOnDaysSelected(outputStrArr, selectedrow);
			}


		};
		dateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
		quantity_array=getContext().getResources().getStringArray(R.array.quantity_array);
	}
	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	public void incrementcount(){
		//System.out.println("count valu"+count);
		this.count++;
		//System.out.println("count valu1"+count);
		Prescription_UI_Adapter.this.notifyDataSetChanged();
	}

	ViewHolder holder;


	@Override
	public View getView( final int position, View convertView,  ViewGroup parent)  {
		// TODO Auto-generated method stub

		//		 elements=getItem(position);


		LayoutInflater inflater = LayoutInflater.from(getContext());
		if (convertView == null){
			holder = new ViewHolder();
			convertView = inflater.inflate(R.layout.add_more_drugs, null);
			holder.SINo=(TextView)convertView.findViewById(R.id.tvNumber);
			holder.Drugname=(EditText)convertView.findViewById(R.id.etDrug);
			holder.Drugdosage=(EditText)convertView.findViewById(R.id.etDosage);
			holder.Startdate=(TextView)convertView.findViewById(R.id.etStartdate);
			holder.Duration=(EditText)convertView.findViewById(R.id.etDuration);
			holder.Days=(LinearLayout)convertView.findViewById(R.id.add_days_container);
			holder.Quantity=(TextView)convertView.findViewById(R.id.srQuantity);
			holder.ivmonday=(ImageView)convertView.findViewById(R.id.days_monday);
			holder.ivtuesday=(ImageView)convertView.findViewById(R.id.days_tuesday);
			holder.ivwednesday=(ImageView)convertView.findViewById(R.id.days_wed);
			holder.ivthursday=(ImageView)convertView.findViewById(R.id.days_thu);
			holder.ivfriday=(ImageView)convertView.findViewById(R.id.days_fri);
			holder.ivsaturday=(ImageView)convertView.findViewById(R.id.days_sat);
			holder.ivsunday=(ImageView)convertView.findViewById(R.id.days_sun);

			holder.remove=(RelativeLayout)convertView.findViewById(R.id.rlImagecross);
			holder.maincontainer=(LinearLayout)convertView.findViewById(R.id.ctml7);



			convertView.setTag(holder);
		}else
			holder = (ViewHolder) convertView.getTag();

		//System.out.println("set tag positions"+position);

		holder.Drugname.setTag(position);
		holder.Drugdosage.setTag(position);
		holder.Startdate.setTag(position);
		holder.Duration.setTag(position);
		holder.Quantity.setTag(position);
		holder.Days.setTag(position);

		holder.ivmonday.setTag(position);
		holder.ivtuesday.setTag(position);
		holder.ivwednesday.setTag(position);
		holder.ivthursday.setTag(position);
		holder.ivfriday.setTag(position);
		holder.ivsaturday.setTag(position);
		holder.ivsunday.setTag(position);


		holder.setPosition(position);


		Log.e("tag  Value===", String.valueOf(holder.Startdate.getTag()));



		holder.SINo.setText(""+(position + 1));

		holder.Quantity.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				quantityview=v;
				/*FragmentActivity activity = (FragmentActivity)(context);*/
				FragmentManager fm = ((Activity) context).getFragmentManager();

				Bundle bundle=new Bundle();
				bundle.putInt("tagvalue", (Integer) v.getTag());
				bundle.putString("name", "Quantity");
				bundle.putStringArray("items",quantity_array);
				spObj=new SpinnerPopup();
				spObj.setSubmitListener(quantitySelectedListener);
				spObj.setArguments(bundle);
				spObj.show(fm, "tag");


			}
		});



		quantitySelectedListener=new onSubmitListener() {

			@Override
			public void onSubmit(String arg, int positionvalue) {
				// TODO Auto-generated method stub

				//System.out.println("quantity value"+arg);
				objects1.get(positionvalue).setQuantity(arg);
				((TextView)quantityview).setText(arg);
				if(arg.length()!=0){
					submitButtonActivation();
				}


				//System.out.println("position valueee"+positionvalue);
				//System.out.println("position val"+position);




				spObj.dismiss();


			}
		};



		//System.out.println("drug name1"+objects1.get(position).getDrug());

		holder.Drugname.setText(getItem(position).getDrug());
		holder.Drugdosage.setText(getItem(position).getDosage());
		holder.Startdate.setText(getItem(position).getStartdate());
		holder.Quantity.setText(getItem(position).getQuantity());

		holder.Duration.setText(getItem(position).getDuration());

		//System.out.println("position days"+position);


		if(objects1.get(position).getDays()!=""){

			submitButtonActivation();
			
			//System.out.println("no days.....");
			String[] _arrdays=getItem(position).getDays().split(",");
			selected_days.clear();
			for(int i=0;i<_arrdays.length;i++){
				selected_days.add(_arrdays[i]);
			}


			if(selected_days.contains("M")){
				submitButtonActivation();
				holder.ivmonday.setImageResource(R.drawable.days_monday_2);
			}else{
				holder.ivmonday.setImageResource(R.drawable.days_monday_1);
			}

			if(selected_days.contains("T")){
				submitButtonActivation();
				holder.ivtuesday.setImageResource(R.drawable.days_tuesday_2);
			}else{
				holder.ivtuesday.setImageResource(R.drawable.days_tuesday_1);
			}

			if(selected_days.contains("W")){
				submitButtonActivation();
				holder.ivwednesday.setImageResource(R.drawable.days_wed_2);
			}else{
				holder.ivwednesday.setImageResource(R.drawable.days_wed_1);
			}

			if(selected_days.contains("TH")){
				submitButtonActivation();
				holder.ivthursday.setImageResource(R.drawable.days_thu_2);
			}else{
				holder.ivthursday.setImageResource(R.drawable.days_thu_1);
			}

			if(selected_days.contains("F")){
				submitButtonActivation();
				holder.ivfriday.setImageResource(R.drawable.days_fri_2);
			}else{
				holder.ivfriday.setImageResource(R.drawable.days_fri_1);
			}

			if(selected_days.contains("S")){
				submitButtonActivation();
				System.out.println("color changed...");
				holder.ivsaturday.setImageResource(R.drawable.days_sat_2);
			}else{
				holder.ivsaturday.setImageResource(R.drawable.days_sat_1);
			}

			if(selected_days.contains("SU")){
				submitButtonActivation();
				holder.ivsunday.setImageResource(R.drawable.days_sun_2);
			}else{
				holder.ivsunday.setImageResource(R.drawable.days_sun_1);
			}




		}else{
			//System.out.println("working,,");
			
			holder.ivmonday.setImageResource(R.drawable.days_monday_1);
			holder.ivtuesday.setImageResource(R.drawable.days_tuesday_1);
			holder.ivwednesday.setImageResource(R.drawable.days_wed_1);
			holder.ivthursday.setImageResource(R.drawable.days_thu_1);
			holder.ivfriday.setImageResource(R.drawable.days_fri_1);
			holder.ivsaturday.setImageResource(R.drawable.days_sat_1);
			holder.ivsunday.setImageResource(R.drawable.days_sun_1);
		}



		holder.remove.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				//System.out.println("position..."+position);
				objects1.remove(position);
				Prescription_UI_Adapter.this.notifyDataSetChanged();
			}
		});	

		holder.Days.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Popup_days_selectorwindow((Integer) v.getTag());
			}
		});



		holder.Drugname.addTextChangedListener(new Mytextwatcher(holder.Drugname, position));
		holder.Drugdosage.addTextChangedListener(new Mytextwatcher(holder.Drugdosage, position));
		holder.Duration.addTextChangedListener(new Mytextwatcher(holder.Duration, position));



		holder.Startdate.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub


				//FragmentActivity activity = (FragmentActivity)(context);
				FragmentManager fm = ((Activity) context).getFragmentManager();
				Log.e("tag  Value===", String.valueOf(arg0.getTag()));
				DateDialogFragment datepicker=new DateDialogFragment((TextView)arg0, position);
				datepicker.show(fm, "showDate");

			}
		});


		Calendar newCalendar = Calendar.getInstance();

		startDatePickerDialog = new DatePickerDialog(getContext(), new OnDateSetListener() {

			public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

				Calendar newDate = Calendar.getInstance();
				newDate.set(year, monthOfYear, dayOfMonth);
				if(position==(Integer)holder.Startdate.getTag()){
					//	System.out.println("position date"+position);
					objects1.get(position).setStartdate(dateFormatter.format(newDate.getTime()));
					holder.Startdate.setText(dateFormatter.format(newDate.getTime()));
				}
			}
		},newCalendar.get(Calendar.YEAR),newCalendar.get(Calendar.MONTH),newCalendar.get(Calendar.DAY_OF_MONTH));
		startDatePickerDialog.getDatePicker().setMinDate(Calendar.getInstance().getTimeInMillis()- 1000);
		startDatePickerDialog.getDatePicker().setCalendarViewShown(false);


		return convertView;
	}




	static class ViewHolder{

		public TextView SINo;
		public EditText Drugname;
		public EditText Drugdosage;
		public TextView Startdate;
		public EditText Duration;
		public LinearLayout Days;
		public TextView Quantity;
		public ImageView ivmonday;
		public ImageView ivtuesday;
		public ImageView ivwednesday;
		public ImageView ivthursday;
		public ImageView ivfriday;
		public ImageView ivsaturday;
		public ImageView ivsunday;
		public RelativeLayout remove;
		LinearLayout maincontainer;

		private int position;

		public int getPosition() {
			return position;
		}

		public void setPosition(int position) {
			this.position = position;
		}

	}

	public void Popup_days_selectorwindow(int row)
	{

		//FragmentActivity activity = (FragmentActivity)(context);
		FragmentManager fm = ((Activity) context).getFragmentManager();

		daysselectObj=new DaysSelector_Popup();
		daysselectObj.setDynamicDaysListener(daysSelectedListener);

		Bundle bundle=new Bundle();
		bundle.putInt("row", row);
		String selected_send=objects1.get(row).getDays();
		String[] arrdays=selected_send.split(",");
		bundle.putStringArray("selecteddays",arrdays);
		daysselectObj.setArguments(bundle);
		daysselectObj.show(fm, "tag");
	}

	private void doOnDaysSelected(String[] outputStrArr, int selectedrow) {
		// TODO Auto-generated method stub
		daysselectObj.dismiss();

		if(outputStrArr.length!=0){
			submitButtonActivation();
		}else{
			objects1.get(selectedrow).setDays("");
		}
		//System.out.println("tag days position...."+selectedrow);
		String selected="";

		for(int i=0;i<outputStrArr.length;i++){
			//System.out.println("days...."+outputStrArr[i]);
			if(outputStrArr[i].equals("Daily")){
				selected="M,"+"T,"+"W,"+"TH,"+"F,"+"S,"+"SU,";
				objects1.get(selectedrow).setDays(selected);
			}else{

				if((outputStrArr[i].equals("Monday"))){
					selected+="M,";
				}if((outputStrArr[i].equals("Tuesday"))){
					selected+="T,";
				}if((outputStrArr[i].equals("Wednesday"))){
					selected+="W,";
				}if((outputStrArr[i].equals("Thursday"))){
					selected+="TH,";
				}if((outputStrArr[i].equals("Friday"))){
					selected+="F,";
				}if((outputStrArr[i].equals("Saturday"))){
					selected+="S,";
				}if((outputStrArr[i].equals("Sunday"))){
					selected+="SU,";
				}

				objects1.get(selectedrow).setDays(selected);

			}

		}

		notifyDataSetChanged();

	}

	public class Mytextwatcher implements TextWatcher{

		private EditText text;
		int position;

		private Mytextwatcher(EditText et,int position) {
			this.text = et;
			this.position=position;
		}
		@Override
		public void afterTextChanged(Editable string) {
			// TODO Auto-generated method stub
			//System.out.println("tag........"+(Integer)text.getTag());

			if(position==(Integer)text.getTag() && text.getId()==R.id.etDrug){
				objects1.get(position).setDrug(string.toString());
			}
			if(position==(Integer)text.getTag() && text.getId()==R.id.etDosage){
				objects1.get(position).setDosage(string.toString());
			}
			if(position==(Integer)text.getTag() && text.getId()==R.id.etDuration){
				objects1.get(position).setDuration(string.toString());
			}
			//System.out.println("position"+position);
		}
		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
			// TODO Auto-generated method stub
		}
		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
			// TODO Auto-generated method stub

			if(s.length()==0){
				submitButtonDeactivation();
			}else if(start==0 && before==0 && count==1){
				submitButtonActivation();
			}
		}

	}


	public class DateDialogFragment extends DialogFragment  implements OnDateSetListener{

		TextView text;
		Integer position;
		public DateDialogFragment(TextView text,Integer position)
		{
			this.text=text;
			this.position=position;
		}
		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			Calendar cal=Calendar.getInstance();
			int year=cal.get(Calendar.YEAR);
			int month=cal.get(Calendar.MONTH);
			int day=cal.get(Calendar.DAY_OF_MONTH);
			return new DatePickerDialog(getActivity(), this, year, month, day);
		}
		public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
			showSetDate(year,monthOfYear,dayOfMonth,text,position);
		}

	}

	public void showSetDate(int year,int month,int day,TextView text,Integer position) {
		if(position==(Integer)text.getTag() && text.getId()==R.id.etStartdate){
			objects1.get(position).setStartdate(day+"-"+month+"-"+year);
		}
		text.setText(day+"-"+month+"-"+year);
	}

	private void submitButtonActivation() {
		// TODO Auto-generated method stub
		((FragmentActivityContainer)getContext()).gettreatmentfrag().onTreatmentbtn(1);


	}
	private void submitButtonDeactivation() {
		// TODO Auto-generated method stub
		((FragmentActivityContainer)getContext()).gettreatmentfrag().onTreatmentbtn(0);

	}



}





