package com.wiinnova.doctorsdiary.supportclasses;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.parse.ParseObject;
import com.wiinnova.doctorsdiary.R;

import java.util.List;


public class CustomListName extends ArrayAdapter<ParseObject>{
	private final Activity context;
	private final List<ParseObject> searchitem;

	public CustomListName(Activity context,
			List<ParseObject> searcheditems) {
		super(context, R.layout.list_single, searcheditems);
		this.context = context;
		this.searchitem = searcheditems;
		
		
	}
	@Override
	public View getView(int position, View view, ViewGroup parent) {
		LayoutInflater inflater = context.getLayoutInflater();
		View rowView= inflater.inflate(R.layout.list_single, null, true);
		TextView txtTitle = (TextView) rowView.findViewById(R.id.tvSpecialist);
		/*ImageView imageView = (ImageView) rowView.findViewById(R.id.img);*/
		txtTitle.setText(searchitem.get(position).getString("firstName"));
	/*	imageView.setImageResource(R.drawable.student);*/
		return rowView;
	}
	
	@Override
	public ParseObject getItem(int position) {
		// TODO Auto-generated method stub
		return searchitem.get(position);
	}
}

