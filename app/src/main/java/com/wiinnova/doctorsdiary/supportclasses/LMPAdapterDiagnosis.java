package com.wiinnova.doctorsdiary.supportclasses;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.parse.ParseObject;
import com.wiinnova.doctorsdiary.R;
import com.wiinnova.doctorsdiary.activities.FragmentActivityContainer;
import com.wiinnova.doctorsdiary.fragment.Obstetrics_gynacologist;
import com.wiinnova.doctorsdiary.fragment.Obstetrics_gynacologist;
import com.wiinnova.doctorsdiary.fragment.PatientCurrentMedicalaInformation_Gynacologist;

import java.util.ArrayList;
import java.util.Calendar;

import com.wiinnova.doctorsdiary.fragment.Obstetrics_gynacologist;

import org.json.JSONArray;
import org.json.JSONException;

import static com.wiinnova.doctorsdiary.fragment.Obstetrics_gynacologist.lmpdate_array;
import static com.wiinnova.doctorsdiary.fragment.Obstetrics_gynacologist.lmpdysmenorrhea_array;
import static com.wiinnova.doctorsdiary.fragment.Obstetrics_gynacologist.lmpflow_array;
import static com.wiinnova.doctorsdiary.fragment.Obstetrics_gynacologist.menstrualcyclelist;
import static com.wiinnova.doctorsdiary.fragment.Obstetrics_gynacologist.menstrualdayslist;
import static com.wiinnova.doctorsdiary.fragment.Obstetrics_gynacologist.menstrualregularlist;


/**
 * Created by ubundu on 16/3/16.
 */
public class LMPAdapterDiagnosis extends BaseAdapter {

    ArrayList<Integer> count;
    Activity activity;
    Obstetrics_gynacologist fragment;
    private static LayoutInflater inflater = null;
    Holder holder;
    DatePickerDialog startDatePickerDialog1;
    int position;
    int daysId;
    int cycleId;
    ArrayList<String> patientobject;

    public LMPAdapterDiagnosis(Activity activity, ArrayList<Integer> count, Obstetrics_gynacologist fragment, ArrayList<String> parseObject) {
        this.activity = activity;
        this.count = count;
        this.fragment = fragment;
        patientobject = parseObject;
        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return count.size();
    }

    public class Holder {
        TextView txtdate;
        TextView txtFlow;
        TextView txtDysmennoria;
        TextView txtLabel;
        CheckBox checkRegular;
        CheckBox checkIrregular;
        EditText numDays;
        EditText numCycle;
        RelativeLayout lytClose;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        this.position = position;
        holder = new Holder();
        View rowView;
        rowView = inflater.inflate(R.layout.add_mensturalhistory, null);
        holder.txtdate = (TextView) rowView.findViewById(R.id.date_menstural);
        holder.txtFlow = (TextView) rowView.findViewById(R.id.flow_menstural);
        holder.txtDysmennoria = (TextView) rowView.findViewById(R.id.dysmenorrhea_menstural);
        holder.txtLabel = (TextView) rowView.findViewById(R.id.txtLabel);
        holder.checkRegular = (CheckBox) rowView.findViewById(R.id.cbregular);
        holder.checkIrregular = (CheckBox) rowView.findViewById(R.id.cbirregular);
        holder.numCycle = (EditText) rowView.findViewById(R.id.cyclepicker);
        holder.numDays = (EditText) rowView.findViewById(R.id.dayspicker);
        holder.lytClose = (RelativeLayout) rowView.findViewById(R.id.rlImagecross);

        holder.txtLabel.setText("Last-Menstrual Period(LMP)");

        ////////////////////Set numberPickers//////////////////////////////////////////////
//        holder.numCycle.setMaxValue(30);
//        holder.numCycle.setMinValue(0);
////        holder.numDays.setMaxValue(30);
//        holder.numDays.setMinValue(0);
        try {
            Log.d("NumberGet", Obstetrics_gynacologist.menstrualcyclelist.get(position) + " fg");
        } catch (Exception e) {
            e.printStackTrace();
        }


        if (fragment == null) {
            holder.lytClose.setVisibility(View.INVISIBLE);
            holder.numCycle.setKeyListener(null);
            holder.numDays.setKeyListener(null);
            holder.checkIrregular.setEnabled(false);
            holder.checkRegular.setEnabled(false);
            holder.txtdate.setEnabled(false);
            holder.txtDysmennoria.setEnabled(false);
            holder.txtFlow.setEnabled(false);

        } else if (position == 0) {
            holder.txtLabel.setText("Last-Menstrual Period(LMP)");
            holder.txtLabel.setTypeface(null, Typeface.BOLD);
            holder.txtLabel.setTextSize(17);
            holder.lytClose.setVisibility(View.INVISIBLE);
        }

        ////////////////////set initial Value
        try {
            holder.numDays.setText(Obstetrics_gynacologist.menstrualdayslist.get(position));
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            holder.numCycle.setText(Obstetrics_gynacologist.menstrualcyclelist.get(position));
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            holder.txtdate.setText(Obstetrics_gynacologist.lmpdate_array.get(position));

            Log.d("DATESLOcal", Obstetrics_gynacologist.lmpdate_array.toString() + " ghf");
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            holder.txtFlow.setText(Obstetrics_gynacologist.lmpflow_array.get(position));
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            holder.txtDysmennoria.setText(Obstetrics_gynacologist.lmpdysmenorrhea_array.get(position));
            holder.txtFlow.setText(Obstetrics_gynacologist.lmpflow_array.get(position));
        } catch (Exception e) {
            e.printStackTrace();
        }



        ///////////////////////set checkbox
        try {
            if (Obstetrics_gynacologist.menstrualregularlist.get(position).equals("Regular")) {
                holder.checkRegular.setChecked(true);
            } else if (Obstetrics_gynacologist.menstrualregularlist.get(position).equals("Irregular")) {
                holder.checkIrregular.setChecked(true);

            }
        } catch (IndexOutOfBoundsException e) {
            holder.checkRegular.setChecked(false);
            holder.checkIrregular.setChecked(false);
            e.printStackTrace();

        }

        holder.checkRegular.setTag(position);
        holder.checkIrregular.setTag(position);
        holder.txtdate.setTag(position);
        holder.txtFlow.setTag(position);
        holder.txtDysmennoria.setTag(position);
        holder.lytClose.setTag(position);
        holder.numDays.setTag(position);
        holder.numCycle.setTag(position);


        holder.checkIrregular.setOnClickListener(onClickListener);
        holder.checkRegular.setOnClickListener(onClickListener);
        holder.txtdate.setOnClickListener(onClickListener);
        holder.txtFlow.setOnClickListener(onClickListener);
        holder.txtDysmennoria.setOnClickListener(onClickListener);
        /////////////////////Close one row
        holder.lytClose.setOnClickListener(onClickListener);

        //////////////////////////////Number picker
        holder.numDays.addTextChangedListener(new GenericTextWatcher(holder.numDays));
        holder.numCycle.addTextChangedListener(new GenericTextWatcher(holder.numCycle));


        return rowView;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.cbregular:
                    fragment.setCheckBox(Integer.parseInt(v.getTag() + ""), "regular");
                    break;

                case R.id.cbirregular:
                    fragment.setCheckBox(Integer.parseInt(v.getTag() + ""), "irregular");
                    break;
                case R.id.date_menstural:
                    fragment.callDatePicker(Integer.parseInt(v.getTag() + ""));
                    break;
                case R.id.flow_menstural:
                    fragment.showFlow(Integer.parseInt(v.getTag() + ""));
                    break;
                case R.id.dysmenorrhea_menstural:
                    fragment.showDysmenorrhea(Integer.parseInt(v.getTag() + ""));
                    break;
                case R.id.rlImagecross:
                    fragment.closeRow(Integer.parseInt(v.getTag() + ""));
                    break;
            }
        }
    };

    private class GenericTextWatcher implements TextWatcher {

        private View view;

        private GenericTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            String text = editable.toString();
            Log.d("TextGet", text + " gfh");
            switch (view.getId()) {
                case R.id.dayspicker:
                    fragment.setMenstrualDays(Integer.parseInt(view.getTag().toString()));
                    break;
                case R.id.cyclepicker:
                    fragment.setMenstrualCycle(Integer.parseInt(view.getTag().toString()));
                    break;
            }
        }
    }
}
