package com.wiinnova.doctorsdiary.supportclasses;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.wiinnova.doctorsdiary.R;

import java.util.ArrayList;

public class DiseaseclassAdapter extends BaseAdapter {
	private Context context;
	private ArrayList<Diseasenameclass> diseaseclassitems;
	ArrayList<Boolean> selectedcheckbox=new ArrayList<Boolean>();
	ArrayList<Diseasenameclass> selectedmedicines=new ArrayList<Diseasenameclass>();
	ArrayList<Diseasenameclass> selectcheck=new ArrayList<Diseasenameclass>();

	public DiseaseclassAdapter(Context context, ArrayList<Diseasenameclass> diseaseclassitems, ArrayList<Diseasenameclass> selecteddisease){
		this.context = context;
		this.diseaseclassitems = diseaseclassitems;
		for(int i=0;i<diseaseclassitems.size();i++){
			selectedcheckbox.add(i, false);
			diseaseclassitems.get(i).setSelected(false);
		}
		System.out.println("diseaseclassitems size"+diseaseclassitems.size());
		selectcheck=selecteddisease;
		if(selectcheck!=null){

			System.out.println("select check size"+selectcheck.size());

			for(int j=0;j<diseaseclassitems.size();j++){
				for(int k=0;k<selectcheck.size();k++){
					if(selectcheck.get(k).getDiseasename().equalsIgnoreCase(diseaseclassitems.get(j).getDiseasename())){
						selectedcheckbox.set(j,true);
						diseaseclassitems.get(j).setSelected(true);
						System.out.println("j value in constructor"+j);
					}
				}
			}
		}
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return diseaseclassitems.size();
	}

	@Override
	public Diseasenameclass getItem(int position) {
		// TODO Auto-generated method stub
		return diseaseclassitems.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		if (convertView == null) 
		{
			LayoutInflater mInflater = (LayoutInflater)
					context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
			convertView = mInflater.inflate(R.layout.disease_listitem, null);
		}


		RelativeLayout container=(RelativeLayout)convertView.findViewById(R.id.lldiseasecontainer);
		TextView txtTitle = (TextView) convertView.findViewById(R.id.title);   
		final CheckBox checkbox=(CheckBox) convertView.findViewById(R.id.chklist); 
		if(diseaseclassitems.get(position).getDiseasename()!=null){
			txtTitle.setText(diseaseclassitems.get(position).getDiseasename());
		}
		//checkboxarraylist.add(position, checkbox);

		if(selectcheck!=null)
		{
			if(selectedcheckbox.get(position)==true){
				checkbox.setChecked(true);
				System.out.println("position checked"+position);
			}else{
				checkbox.setChecked(false);
			}
		}

		container.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				checkbox.toggle();
				System.out.println("container clicked.....");
				if(checkbox.isChecked()){
					selectedcheckbox.set(position, true);
					diseaseclassitems.get(position).setSelected(true);
					System.out.println("position checked"+position);
				}else{
					System.out.println("false working.......");
					selectedcheckbox.set(position, false);
					diseaseclassitems.get(position).setSelected(false);
				}

			}
		});

		checkbox.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				System.out.println("container clicked.....");
				if(checkbox.isChecked()){
					diseaseclassitems.get(position).setSelected(true);
					selectedcheckbox.set(position, true);
					System.out.println("position checked"+position);
				}else{
					diseaseclassitems.get(position).setSelected(false);
					System.out.println("false working.......");
					selectedcheckbox.set(position, false);
				}

			}
		});



		return convertView;
	}

	public ArrayList<Diseasenameclass> getSelecteditem()
	{
		selectedmedicines.clear();
		/*for(int i=0;i<selectedcheckbox.size();i++){
			if(selectedcheckbox.get(i)==true){
				selectedmedicines.add(diseaseclassitems.get(i));
			}
		}*/
		for(int i=0;i<diseaseclassitems.size();i++){
			if(diseaseclassitems.get(i).isSelected()){
				selectedmedicines.add(diseaseclassitems.get(i));
			}
		}
		return selectedmedicines;
	}

}
