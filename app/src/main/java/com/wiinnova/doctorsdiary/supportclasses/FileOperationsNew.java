package com.wiinnova.doctorsdiary.supportclasses;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.draw.VerticalPositionMark;
import com.itextpdf.text.pdf.parser.PdfReaderContentParser;
import com.itextpdf.text.pdf.parser.SimpleTextExtractionStrategy;
import com.itextpdf.text.pdf.parser.TextExtractionStrategy;
import com.parse.ParseObject;

import org.json.JSONException;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.StringTokenizer;

public class FileOperationsNew {



	Context context;
	static PdfWriter writer;
	Document document;
	Font font;
	Font textfont;


	public FileOperationsNew(){

	}

	public Boolean write(Context fragmentActivity, String fname, ParseObject combinedobject){
		try {
			context=fragmentActivity;
			String fpath = "/sdcard/" + fname + ".pdf";
			File file = new File(fpath);


			// If file does not exists, then create it
			/*if (!file.exists()) {*/
			file.createNewFile();
			/*}else{*/

			

			SharedPreferences sp1=context.getSharedPreferences("Login", 0);

			float header=sp1.getFloat("header", 0);       
			float footer = sp1.getFloat("footer", 0);
			float left = sp1.getFloat("left", 0);
			float right = sp1.getFloat("right", 0);
			float title=sp1.getFloat("title", 0);
			float content=sp1.getFloat("content", 0);

			

			font = new Font(FontFamily.HELVETICA, title, Font.BOLD, BaseColor.BLACK);
			textfont= new Font(FontFamily.HELVETICA, content , Font.NORMAL, BaseColor.BLACK);
			
			//document = new Document();
			
			System.out.println("header value"+header);
			System.out.println("right value"+right);
			System.out.println("left value"+left);
			System.out.println("footer value"+footer);

			document=new Document(PageSize.A4,left, right, header, footer);
			//document=new Document(PageSize.A4,30f, 120f, 180f, 90f);

			//document=new Document(new Rectangle(500f, 1200f),  30f, 30f, 100f, 50f);
			System.out.println("height doc....>"+document.getPageSize());

			// step 2

			writer =PdfWriter.getInstance(document,
					new FileOutputStream(file.getAbsoluteFile()));
			document.open();

			/*Rectangle rect = new Rectangle(100, 100, 550, 600);
	        writer.setBoxSize("art", rect);
	        HeaderFooterPageEvent event = new HeaderFooterPageEvent();
	        writer.setPageEvent(event);
	        document.open();
	      //  document.add(new Paragraph("This is Page One"));
	        document.newPage();
	       // document.add(new Paragraph("This is Page two"));
			 */	      
			Chunk glue = new Chunk(new VerticalPositionMark());
			Paragraph name=new Paragraph(); 
			/*Paragraph name1 = new Paragraph("PATIENT NAME:"+" ",font);
			Paragraph name2 = new Paragraph(combinedobject.getString("patientname"),textfont);	*/
			name.add(new Chunk("PATIENT NAME:"+" ",font)); 
		    name.add(new Chunk(combinedobject.getString("patientname").toString(),textfont)); 
		    
		    name.add(new Chunk(glue));
		    
		   // Paragraph id=new Paragraph(); 
		    name.add(new Chunk("PATIENT ID:"+" ",font));
		    name.add(new Chunk(combinedobject.getString("id"),textfont));
		    
		    document.add(name);
		    
			//name.add(new Chunk(glue));
			/*name.add("DOCTOR NAME:"+" "+"Dr."+" "+combinedobject.getString("doctername"));
			document.add(name);*/

		    
		    
			/*Paragraph id1 = new Paragraph("PATIENT ID:"+" ",font);
			Paragraph id2 = new Paragraph(combinedobject.getString("id"),textfont);*/
			
			//document.add(id);
			/*id.add(new Chunk(glue));*/
			
			
			/*Paragraph date1 = new Paragraph("DATE:"+" ",font);
			Paragraph date2 = new Paragraph(combinedobject.getString("createdAt"),textfont);*/
			
			Paragraph date=new Paragraph();
			
			date.add(new Chunk(glue));
			
			date.add(new Chunk("DATE:"+" ",font));
			date.add(new Chunk(combinedobject.getString("createdAt"),textfont));
			document.add(date);

			//Paragraph vitalsSpace = new Paragraph("");

			document.add( Chunk.NEWLINE );



			Paragraph vitals = new Paragraph("VITALS:",font);
			document.add(vitals);


			InputStream weightstream = context.getAssets().open("weight.png");
			Bitmap weightbmp = BitmapFactory.decodeStream(weightstream);
			ByteArrayOutputStream streamweight = new ByteArrayOutputStream();
			weightbmp.compress(Bitmap.CompressFormat.PNG, 100, streamweight);
			Image weightLogo = Image.getInstance(streamweight.toByteArray());

			weightLogo.scalePercent(25);


			InputStream pressureStream = context.getAssets().open("bloodpressure.png");
			Bitmap pressurebmp = BitmapFactory.decodeStream(pressureStream);
			ByteArrayOutputStream streampressure= new ByteArrayOutputStream();
			pressurebmp.compress(Bitmap.CompressFormat.PNG, 100, streampressure);
			Image pressureStreamLogo = Image.getInstance(streampressure.toByteArray());

			pressureStreamLogo.scalePercent(25);


			InputStream pulseStream = context.getAssets().open("pulse.png");
			Bitmap pulsebmp = BitmapFactory.decodeStream(pulseStream);
			ByteArrayOutputStream streampulse = new ByteArrayOutputStream();
			pulsebmp.compress(Bitmap.CompressFormat.PNG, 100, streampulse);
			Image pulseLogo = Image.getInstance(streampulse.toByteArray());

			pulseLogo.scalePercent(25);



			InputStream respiratoryrateinputStream = context.getAssets().open("respiratoryrate.png");
			Bitmap respiratoryratebmp = BitmapFactory.decodeStream(respiratoryrateinputStream);
			ByteArrayOutputStream streamrespiratoryrate = new ByteArrayOutputStream();
			respiratoryratebmp.compress(Bitmap.CompressFormat.PNG, 100, streamrespiratoryrate);
			Image respiratoryrateLogo = Image.getInstance(streamrespiratoryrate.toByteArray());

			respiratoryrateLogo.scalePercent(25);



			InputStream spo2inputStream = context.getAssets().open("spo2.png");
			Bitmap spo2bmp = BitmapFactory.decodeStream(spo2inputStream);
			ByteArrayOutputStream streamspo2 = new ByteArrayOutputStream();
			spo2bmp.compress(Bitmap.CompressFormat.PNG, 100, streamspo2);
			Image spo2Logo = Image.getInstance(streamspo2.toByteArray());

			spo2Logo.scalePercent(25);


			InputStream temperatureinputStream = context.getAssets().open("temperature.png");
			Bitmap temperaturebmp = BitmapFactory.decodeStream(temperatureinputStream);
			ByteArrayOutputStream streamtemperature = new ByteArrayOutputStream();
			temperaturebmp.compress(Bitmap.CompressFormat.PNG, 100, streamtemperature);
			Image temperatureLogo = Image.getInstance(streamtemperature.toByteArray());

			temperatureLogo.scalePercent(25);




			PdfPTable vitalstable = new PdfPTable(6);
			//vitalstable.setTotalWidth(document.getPageSize().getWidth());
			//vitalstable.setWidths(document.getPageSize().getWidth());
			vitalstable.setWidthPercentage(100);
			PdfPCell cell = new PdfPCell();
			PdfPCell cell1 = new PdfPCell();
			PdfPCell cell2 = new PdfPCell();
			PdfPCell cell3 = new PdfPCell();
			PdfPCell cell4 = new PdfPCell();
			PdfPCell cell5 = new PdfPCell();

			PdfPCell cell6; 
			PdfPCell cell7; 
			PdfPCell cell8;
			PdfPCell cell9;
			PdfPCell cell10;
			PdfPCell cell11;



			cell.addElement(new Chunk(weightLogo, 0, 0,true));
			cell1.addElement(new Chunk(pressureStreamLogo, 0, 0,true));
			cell2.addElement(new Chunk(pulseLogo, 0, 0,true));
			cell3.addElement(new Chunk(respiratoryrateLogo, 0, 0,true));
			cell4.addElement(new Chunk(spo2Logo, 0, 0,true));
			cell5.addElement(new Chunk(temperatureLogo, 0, 0,true));

			cell.setBorder(Rectangle.NO_BORDER);
			cell1.setBorder(Rectangle.NO_BORDER);
			cell2.setBorder(Rectangle.NO_BORDER);
			cell3.setBorder(Rectangle.NO_BORDER);
			cell4.setBorder(Rectangle.NO_BORDER);
			cell5.setBorder(Rectangle.NO_BORDER);


			if(combinedobject.getString("weight_result")!=null && combinedobject.getString("weight_result")!="Nil"){
				cell6=new PdfPCell(new Phrase(combinedobject.getString("weight_result")+" "+"kg",textfont));
			}else{
				cell6=new PdfPCell(new Phrase("--"+" "+"kg",textfont));
			}

			if(combinedobject.getString("bloodPressure_result")!=null && combinedobject.getString("bloodPressure_result")!="Nil"){
				cell7=new PdfPCell(new Phrase(combinedobject.getString("bloodPressure_result")+" "+"Hg mm",textfont));
			}else{
				cell7=new PdfPCell(new Phrase("--"+" "+"Hg mm",textfont));
			}

			if(combinedobject.getString("pulse_result")!=null && combinedobject.getString("pulse_result")!="Nil"){
				cell8=new PdfPCell(new Phrase(combinedobject.getString("pulse_result")+" "+"beats/min",textfont));

			}else{
				cell8=new PdfPCell(new Phrase("--"+" "+"beats/min",textfont));
			}

			if(combinedobject.getString("respRate_result")!=null && combinedobject.getString("respRate_result")!="Nil"){
				cell9=new PdfPCell(new Phrase(combinedobject.getString("respRate_result")+" "+"breaths/min",textfont));

			}else{
				cell9=new PdfPCell(new Phrase("--"+" "+"breaths/min",textfont));

			}


			if(combinedobject.getString("spo2_result")!=null && combinedobject.getString("spo2_result")!="Nil"){
				cell10=new PdfPCell(new Phrase(combinedobject.getString("spo2_result")+" "+"%",textfont));
			}else{
				cell10=new PdfPCell(new Phrase("--"+" "+"%",textfont));

			}

			if(combinedobject.getString("temperature")!=null && combinedobject.getString("temperature")!="Nil"){
				cell11=new PdfPCell(new Phrase(combinedobject.getString("temperature")+" "+"c",textfont));

			}else{
				cell11=new PdfPCell(new Phrase("--"+" "+"c",textfont));

			}



			cell6.setBorder(Rectangle.NO_BORDER);
			cell7.setBorder(Rectangle.NO_BORDER);
			cell8.setBorder(Rectangle.NO_BORDER);
			cell9.setBorder(Rectangle.NO_BORDER);
			cell10.setBorder(Rectangle.NO_BORDER);
			cell11.setBorder(Rectangle.NO_BORDER);


			vitalstable.addCell(cell);
			vitalstable.addCell(cell1);
			vitalstable.addCell(cell2);
			vitalstable.addCell(cell3);
			vitalstable.addCell(cell4);
			vitalstable.addCell(cell5);


			vitalstable.addCell(cell6);
			vitalstable.addCell(cell7);
			vitalstable.addCell(cell8);
			vitalstable.addCell(cell9);
			vitalstable.addCell(cell10);
			vitalstable.addCell(cell11);

			document.add(vitalstable);

			document.add( Chunk.NEWLINE );



			PdfPTable symptomstable = new PdfPTable(2);
			symptomstable.setWidthPercentage(100);



			PdfPCell cellOne = new PdfPCell(new Phrase("SYMPTOMS",font));
			PdfPCell cellTwo = new PdfPCell(new Phrase("SYNDROMES",font));
			PdfPCell cellThree;
			PdfPCell cellFour;


			cellOne.setBorder(Rectangle.NO_BORDER);
			cellTwo.setBorder(Rectangle.NO_BORDER);

			if(combinedobject.getString("symptoms")!=null){
				cellThree = new PdfPCell(new Phrase(combinedobject.getString("symptoms"),textfont));

			}else{
				cellThree = new PdfPCell(new Phrase("Nil",textfont));

			}
			cellThree.setBorder(Rectangle.NO_BORDER);
			if(combinedobject.getString("syndromes")!=null){

				cellFour = new PdfPCell(new Phrase(combinedobject.getString("syndromes"),textfont));

			}else{

				cellFour = new PdfPCell(new Phrase("Nil",textfont));

			}
			cellFour.setBorder(Rectangle.NO_BORDER);

			symptomstable.addCell(cellOne);
			symptomstable.addCell(cellTwo);
			symptomstable.addCell(cellThree);
			symptomstable.addCell(cellFour);

			document.add(symptomstable);


			//document.add( Chunk.NEWLINE );


			PdfPTable diagnosistable = new PdfPTable(2);
			diagnosistable.setWidthPercentage(100);

			PdfPCell diagnosiscellOne = new PdfPCell(new Phrase("DISEASE",font));
			PdfPCell diagnosiscellTwo = new PdfPCell(new Phrase("ADDITIONAL COMMENTS",font));
			PdfPCell diagnosiscellThree;
			PdfPCell diagnosiscellFour;

			diagnosiscellOne.setBorder(Rectangle.NO_BORDER);
			diagnosiscellTwo.setBorder(Rectangle.NO_BORDER);
			if(combinedobject.getString("suspectedDisease")!=null){
				diagnosiscellThree= new PdfPCell(new Phrase(combinedobject.getString("suspectedDisease"),textfont));
			}else{
				diagnosiscellThree= new PdfPCell(new Phrase("Nil",textfont));

			}
			diagnosiscellThree.setBorder(Rectangle.NO_BORDER);
			if(combinedobject.getString("additionalComments")!=null){
				diagnosiscellFour=new PdfPCell(new Phrase(combinedobject.getString("additionalComments"),textfont));

			}else{
				diagnosiscellFour=new PdfPCell(new Phrase("Nil",textfont));
			}
			diagnosiscellFour.setBorder(Rectangle.NO_BORDER);

			diagnosistable.addCell(diagnosiscellOne);
			diagnosistable.addCell(diagnosiscellTwo);
			diagnosistable.addCell(diagnosiscellThree);
			diagnosistable.addCell(diagnosiscellFour);


			document.add(diagnosistable);

			document.add( Chunk.NEWLINE );

			PdfPTable table = new PdfPTable(4);
			table.setWidthPercentage(100);

			/*PdfPCell cell;*/
			PdfPCell drug= new PdfPCell(new Phrase("DRUGS",font));
			drug.setHorizontalAlignment(Element.ALIGN_CENTER);
			//drug.setBorderWidth(.1f);
			drug.setBorder(Rectangle.NO_BORDER);


			PdfPCell dosage= new PdfPCell(new Phrase("DOSAGE",font));
			dosage.setHorizontalAlignment(Element.ALIGN_CENTER);
			//dosage.setBorderWidth(.1f);

			dosage.setBorder(Rectangle.NO_BORDER);


			PdfPCell frequency= new PdfPCell(new Phrase("FREQUENCY",font));
			frequency.setHorizontalAlignment(Element.ALIGN_CENTER);
			//frequency.setBorderWidth(.1f);
			frequency.setBorder(Rectangle.NO_BORDER);

			/*
			PdfPCell startdate= new PdfPCell(new Phrase("START DATE",font));
			startdate.setHorizontalAlignment(Element.ALIGN_CENTER);
			startdate.setBorderWidth(.1f);
			 */

			PdfPCell duration= new PdfPCell(new Phrase("DURATION",font));;
			duration.setHorizontalAlignment(Element.ALIGN_CENTER);
			//duration.setBorderWidth(.1f);
			duration.setBorder(Rectangle.NO_BORDER);

		/*	PdfPCell days= new PdfPCell(new Phrase("DAYS",font));;
			days.setHorizontalAlignment(Element.ALIGN_CENTER);
			//days.setBorderWidth(.1f);
			days.setBorder(Rectangle.NO_BORDER);*/




			//cell = new PdfPCell(new Phrase("Cell with colspan 3"));

			// we add the four remaining cells with addCell()
			table.addCell(drug);
			table.addCell(dosage);
			table.addCell(frequency);
			table.addCell(duration);
			/*table.addCell(duration);*/
			//table.addCell(days);




			if(combinedobject.getJSONArray("drug")!=null){
				for(int j=0;j<combinedobject.getJSONArray("drug").length();j++){
					
					
					try {
						PdfPCell drugcell=new PdfPCell(new Phrase(combinedobject.getJSONArray("drug").getString(j),textfont));
						drugcell.setHorizontalAlignment(Element.ALIGN_CENTER);
						//drugcell.setBorderWidth(.1f);
						drugcell.setBorder(Rectangle.NO_BORDER);
						System.out.println("drug name...."+combinedobject.getJSONArray("drug").getString(j));


						PdfPCell dosagecell=new PdfPCell(new Phrase(combinedobject.getJSONArray("dosage").getString(j),textfont));
						dosagecell.setHorizontalAlignment(Element.ALIGN_CENTER);
						dosagecell.setBorder(Rectangle.NO_BORDER);
						System.out.println("dosage name...."+combinedobject.getJSONArray("dosage").getString(j));



						/*PdfPCell drugStartDatecell=new PdfPCell(new Phrase(combinedobject.getJSONArray("drugStartDate").getString(j)));
						drugStartDatecell.setHorizontalAlignment(Element.ALIGN_CENTER);
						drugStartDatecell.setBorderWidth(.1f);*/

						PdfPCell drugduration=new PdfPCell(new Phrase(combinedobject.getJSONArray("duration").getString(j),textfont));
						drugduration.setHorizontalAlignment(Element.ALIGN_CENTER);
						drugduration.setBorder(Rectangle.NO_BORDER);
						System.out.println("duration name...."+combinedobject.getJSONArray("duration").getString(j));


						//PdfPCell dayscell;
						PdfPCell quantitycell;


						table.addCell(drugcell);
						table.addCell(dosagecell);


						if(combinedobject.getJSONArray("quantity").getString(j).endsWith(",")){
							String str=combinedobject.getJSONArray("quantity").getString(j).replaceAll("\\(.*?\\)","");

							if (str.endsWith(",")) {
								str = str.substring(0, str.length() - 1);

								quantitycell=new PdfPCell(new Phrase(str,textfont));
								quantitycell.setHorizontalAlignment(Element.ALIGN_CENTER);
								quantitycell.setBorder(Rectangle.NO_BORDER);
								table.addCell(quantitycell);
							}
						}else{
							String str=combinedobject.getJSONArray("quantity").getString(j).replaceAll("\\(.*?\\)","");
							quantitycell=new PdfPCell(new Phrase(str,textfont));
							quantitycell.setHorizontalAlignment(Element.ALIGN_CENTER);
							quantitycell.setBorder(Rectangle.NO_BORDER);
							table.addCell(quantitycell);

							//table.addCell(combinedobject.getJSONArray("quantity").getString(j));
						}
						
						System.out.println("quantity name...."+combinedobject.getJSONArray("quantity").getString(j));
						



						table.addCell(drugduration);

						/*if(combinedobject.getJSONArray("days").getString(j).endsWith(",")){
							String str=combinedobject.getJSONArray("days").getString(j);
							if (str.endsWith(",")) {
								str = str.substring(0, str.length() - 1);

								dayscell=new PdfPCell(new Phrase(str,textfont));
								dayscell.setHorizontalAlignment(Element.ALIGN_CENTER);
								dayscell.setBorder(Rectangle.NO_BORDER);
								table.addCell(dayscell);
							}
						}else{
							dayscell=new PdfPCell(new Phrase(combinedobject.getJSONArray("days").getString(j),textfont));
							dayscell.setHorizontalAlignment(Element.ALIGN_CENTER);
							dayscell.setBorder(Rectangle.NO_BORDER);
							table.addCell(dayscell);
							//table.addCell(combinedobject.getJSONArray("days").getString(j));
						}
						
						System.out.println("days name...."+combinedobject.getJSONArray("days").getString(j));*/


					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}

			document.add(table);
			document.add( Chunk.NEWLINE );


			PdfPTable treatmenttable = new PdfPTable(2);
			treatmenttable.setWidthPercentage(100);



			PdfPCell treatmentcellOne = new PdfPCell(new Phrase("TREATMENT",font));
			PdfPCell treatmentcellTwo = new PdfPCell(new Phrase("FOLLOWUP",font));
			PdfPCell treatmentcellThree;
			PdfPCell treatmentcellFour;



			treatmentcellOne.setBorder(Rectangle.NO_BORDER);
			treatmentcellTwo.setBorder(Rectangle.NO_BORDER);

			if(combinedobject.getString("treatment")!=null){
				treatmentcellThree = new PdfPCell(new Phrase(combinedobject.getString("treatment"),textfont));

			}else{
				treatmentcellThree = new PdfPCell(new Phrase("Nil",textfont));

			}
			treatmentcellThree.setBorder(Rectangle.NO_BORDER);
			if(combinedobject.getString("followup")!=null){

				treatmentcellFour = new PdfPCell(new Phrase(combinedobject.getString("followup"),textfont));

			}else{
				treatmentcellFour = new PdfPCell(new Phrase("Nil",textfont));

			}
			treatmentcellFour.setBorder(Rectangle.NO_BORDER);

			treatmenttable.addCell(treatmentcellOne);
			treatmenttable.addCell(treatmentcellTwo);
			treatmenttable.addCell(treatmentcellThree);
			treatmenttable.addCell(treatmentcellFour);

			document.add(treatmenttable);



			document.close();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}




		return true;


	}

	public String read(String fname) {
		BufferedReader br = null;
		String response = null;
		try {
			StringBuffer output = new StringBuffer();
			String fpath = "/sdcard/" + fname + ".pdf";

			PdfReader reader = new PdfReader(new FileInputStream(fpath));
			PdfReaderContentParser parser = new PdfReaderContentParser(reader);

			StringWriter strW = new StringWriter();

			TextExtractionStrategy strategy;
			for (int i = 1; i <= reader.getNumberOfPages(); i++) {
				strategy = parser.processContent(i,
						new SimpleTextExtractionStrategy());

				strW.write(strategy.getResultantText());

			}

			response = strW.toString();

		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
		return response;
	}

	private static void absText(String text, int x, int y,int size){

		try {
			PdfContentByte cb = writer.getDirectContent();
			BaseFont bf = BaseFont.createFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
			cb.saveState();
			cb.beginText();
			cb.moveText(x, y);
			cb.setFontAndSize(bf, size);
			cb.showText(text);
			cb.endText();




			cb.restoreState();



		} catch (DocumentException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}


	private static void absText1(String text, int x, int y,int size){

		try {
			PdfContentByte cb = writer.getDirectContent();
			BaseFont bf = BaseFont.createFont(BaseFont.TIMES_BOLD, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
			cb.saveState();
			cb.beginText();
			cb.moveText(x, y);
			cb.setFontAndSize(bf, size);
			cb.showText(text);
			cb.endText();



			cb.restoreState();


		} catch (DocumentException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}


	private static void absText2(String text, int x, int y,int size){

		try {
			PdfContentByte cb = writer.getDirectContent();
			BaseFont bf = BaseFont.createFont(BaseFont.HELVETICA_BOLD, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
			cb.saveState();
			cb.beginText();
			cb.moveText(x, y);
			cb.setFontAndSize(bf, size);
			cb.showText(text);
			cb.endText();



			cb.restoreState();



		} catch (Exception e) {
			e.printStackTrace();
		}
	}





	public String addLinebreaks(String input, int maxLineLength) {
		StringTokenizer tok = new StringTokenizer(input, " ");
		StringBuilder output = new StringBuilder(input.length());
		int lineLen = 0;
		while (tok.hasMoreTokens()) {
			String word = tok.nextToken();

			if (lineLen + word.length() > maxLineLength) {
				output.append("\n");
				lineLen = 0;
			}
			output.append(word);
			lineLen += word.length();
		}
		return output.toString();
	}

}
