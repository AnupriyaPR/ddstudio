package com.wiinnova.doctorsdiary.supportclasses;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ClipData;
import android.content.Context;
import android.support.v7.view.menu.MenuView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.ParseObject;
import com.wiinnova.doctorsdiary.R;
import com.wiinnova.doctorsdiary.fragment.Diagnosis_Frag_Gynacologist;
import com.wiinnova.doctorsdiary.fragment.Diagnosisexamination_gynacologist_view;
import com.wiinnova.doctorsdiary.popupwindow.SpinnerPopup;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

/**
 * Created by ubundu on 21/3/16.
 */
public class ArrayAdapterDiagnosisView extends BaseAdapter {
    Activity activity;
    List<ParseObject> objects;
    private static LayoutInflater inflater = null;
    Diagnosisexamination_gynacologist_view fragment;
    HashMap<Integer, View> hashMap = new HashMap<Integer, View>();
    ViewHolder holder;
    ArrayList<LinearLayout> arrLinear = new ArrayList<LinearLayout>();
    ArrayList<ImageView> arrImg = new ArrayList<ImageView>();
    ArrayList<ListView> arrListView = new ArrayList<ListView>();

    public ArrayAdapterDiagnosisView(Activity activity, List<ParseObject> objects, Diagnosisexamination_gynacologist_view fragment) {
        this.activity = activity;
        this.objects = objects;
        this.fragment = fragment;
        hashMap = new HashMap<Integer, View>();
        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public class ViewHolder {
        ListView lstMenstrualPeriod;
        LinearLayout container;
        TextView flName, uniqueID;
        EditText etweight;
        EditText etheight;
        EditText etspo;
        EditText etpulse;
        EditText etrespiratory;
        EditText ettemperature;
        EditText etpressure;

        CheckBox cbnormal;
        CheckBox cbabnormal;
        EditText etexternalgenetalia;

        CheckBox cblumpno;
        CheckBox cblumpyes;
        EditText etlump;

        CheckBox cbgalactorrheano;
        CheckBox cbgalactorrheayes;
        EditText etgalactorrhea;

        CheckBox cbbreastotherno;
        CheckBox cbbreastotheryes;
        EditText etbreastother;

        CheckBox cbwelldevelopedno;
        CheckBox cbwelldevelopedyes;
        EditText etwelldeveloped;

        CheckBox cbhairno;
        CheckBox cbhairyes;
        EditText ethair;

        CheckBox cbacneno;
        CheckBox cbacneyes;
        EditText etacne;

        CheckBox cbsecondarysexotherno;
        CheckBox cbsecondarysexotheryes;
        EditText etsecondarysexother;

        CheckBox cbpreabdomennormal;
        CheckBox cbpreabdomenabnormal;
        EditText etpreabdomen;

        CheckBox cbhealthyno;
        CheckBox cbhealthyyes;
        EditText ethealthy;

        CheckBox cbbleedingno;
        CheckBox cbbleedingyes;
        EditText etbleeding;

        CheckBox cblbcno;
        CheckBox cblbcyes;
        EditText etlbc;

        TextView tvavaf;
        TextView tvrvrf;
        EditText etuterusothers;


        SpinnerPopup spObj;
        SpinnerPopup.onSubmitListener spinnerpopupListener;
        int flag;

        LinearLayout datecontainer;
        TextView date;


        String bmi;
        String weight_result;
        String spo2_result;
        String pulse_result;
        String respRate_result;
        String height_result;
        String bloodGroup_result;
        String bloodPressure_result;
        String temp_result;


        EditText etBloodGroup;
        EditText etbmi;
        EditText etdiapressure;
        EditText etsyspressure;
        ImageView arrow;

    }

    @Override
    public int getCount() {
        return objects.size();
    }

    @Override
    public int getViewTypeCount() {

        if (getCount() != 0)
            return getCount();

        return 1;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        holder = new ViewHolder();
        View v = convertView;
        if (v == null) {

            v = inflater.inflate(R.layout.diagnosisexaminationviewgynacologist, null);

            ParseObject patientobject = objects.get(position);

            holder.container = (LinearLayout) v.findViewById(R.id.scroll);
            holder.date = (TextView) v.findViewById(R.id.cdcurrdate);
            holder.datecontainer = (LinearLayout) v.findViewById(R.id.cdl2);
            holder.arrow = (ImageView) v.findViewById(R.id.ivarrow);
            holder.lstMenstrualPeriod = (ListView) v.findViewById(R.id.lstMenstrualPeriod);

            arrLinear.add(holder.container);
            arrImg.add(holder.arrow);
            arrListView.add(holder.lstMenstrualPeriod);

            holder.datecontainer.setTag(R.integer.tag1, position);
            holder.datecontainer.setTag(R.integer.tag2, patientobject);
            holder.container.setTag("container" + position);
            holder.arrow.setTag("img" + position);


            holder.container.setVisibility(View.GONE);

            holder.etweight = (EditText) v.findViewById(R.id.etweight);
            holder.etheight = (EditText) v.findViewById(R.id.etheight);
            holder.etbmi = (EditText) v.findViewById(R.id.etbmi);
            holder.etspo = (EditText) v.findViewById(R.id.etspo);
            holder.etpulse = (EditText) v.findViewById(R.id.etpulse);
            holder.etrespiratory = (EditText) v.findViewById(R.id.etrespiratory);
            holder.ettemperature = (EditText) v.findViewById(R.id.ettemperature);
            holder.etBloodGroup = (EditText) v.findViewById(R.id.etbloodgroup);
            holder.etdiapressure = (EditText) v.findViewById(R.id.etbloodpressure);
            holder.etsyspressure = (EditText) v.findViewById(R.id.etbloodpressure1);


            holder.cbnormal = (CheckBox) v.findViewById(R.id.cbnormal);
            holder.cbabnormal = (CheckBox) v.findViewById(R.id.cbabnormal);
            holder.etexternalgenetalia = (EditText) v.findViewById(R.id.etexternalgenetalia);

            holder.cblumpno = (CheckBox) v.findViewById(R.id.cblumpno);
            holder.cblumpyes = (CheckBox) v.findViewById(R.id.cblumpyes);
            holder.etlump = (EditText) v.findViewById(R.id.etlump);

            holder.cbgalactorrheano = (CheckBox) v.findViewById(R.id.cbgalactorrheano);
            holder.cbgalactorrheayes = (CheckBox) v.findViewById(R.id.cbgalactorrheayes);
            holder.etgalactorrhea = (EditText) v.findViewById(R.id.etgalactorrhea);

            holder.cbbreastotherno = (CheckBox) v.findViewById(R.id.cbotherno);
            holder.cbbreastotheryes = (CheckBox) v.findViewById(R.id.cbotheryes);
            holder.etbreastother = (EditText) v.findViewById(R.id.etother);

            holder.cbwelldevelopedno = (CheckBox) v.findViewById(R.id.cbwelldevelopedno);
            holder.cbwelldevelopedyes = (CheckBox) v.findViewById(R.id.cbwelldevelopedyes);
            holder.etwelldeveloped = (EditText) v.findViewById(R.id.etwelldeveloped);

            holder.cbhairno = (CheckBox) v.findViewById(R.id.cbhairno);
            holder.cbhairyes = (CheckBox) v.findViewById(R.id.cbhairyes);
            holder.ethair = (EditText) v.findViewById(R.id.ethair);

            holder.cbacneno = (CheckBox) v.findViewById(R.id.cbacneno);
            holder.cbacneyes = (CheckBox) v.findViewById(R.id.cbacneyes);
            holder.etacne = (EditText) v.findViewById(R.id.etacne);

            holder.cbsecondarysexotherno = (CheckBox) v.findViewById(R.id.cbotherno1);
            holder.cbsecondarysexotheryes = (CheckBox) v.findViewById(R.id.cbotheryes1);
            holder.etsecondarysexother = (EditText) v.findViewById(R.id.etother1);

            holder.cbpreabdomennormal = (CheckBox) v.findViewById(R.id.cbabdomenno);
            holder.cbpreabdomenabnormal = (CheckBox) v.findViewById(R.id.cbabdomenyes);
            holder.etpreabdomen = (EditText) v.findViewById(R.id.etAbdomen);

            holder.cbhealthyno = (CheckBox) v.findViewById(R.id.cbhealthyno);
            holder.cbhealthyyes = (CheckBox) v.findViewById(R.id.cbhealthyyes);
            holder.ethealthy = (EditText) v.findViewById(R.id.ethealthy);

            holder.cbbleedingno = (CheckBox) v.findViewById(R.id.cbbleedingno);
            holder.cbbleedingyes = (CheckBox) v.findViewById(R.id.cbbleedingyes);
            holder.etbleeding = (EditText) v.findViewById(R.id.etbleeding);

            holder.cblbcno = (CheckBox) v.findViewById(R.id.cblbcno);
            holder.cblbcyes = (CheckBox) v.findViewById(R.id.cblbcyes);
            ;
            holder.etlbc = (EditText) v.findViewById(R.id.etlbc);
            ;

            holder.tvavaf = (TextView) v.findViewById(R.id.avafsize);
            holder.tvrvrf = (TextView) v.findViewById(R.id.rvrfsize);
            holder.etuterusothers = (EditText) v.findViewById(R.id.etvaginal);


            holder.etweight.setEnabled(false);
            holder.etheight.setEnabled(false);
            holder.etspo.setEnabled(false);
            holder.etpulse.setEnabled(false);
            holder.etrespiratory.setEnabled(false);
            holder.ettemperature.setEnabled(false);
            holder.etdiapressure.setEnabled(false);
            holder.etsyspressure.setEnabled(false);

            holder.cbnormal.setEnabled(false);
            holder.cbabnormal.setEnabled(false);
            holder.etexternalgenetalia.setEnabled(false);

            holder.cblumpno.setEnabled(false);
            holder.cblumpyes.setEnabled(false);
            holder.etlump.setEnabled(false);

            holder.cbgalactorrheano.setEnabled(false);
            ;
            holder.cbgalactorrheayes.setEnabled(false);
            ;
            holder.etgalactorrhea.setEnabled(false);
            ;

            holder.cbbreastotherno.setEnabled(false);
            ;
            holder.cbbreastotheryes.setEnabled(false);
            ;
            holder.etbreastother.setEnabled(false);
            ;

            holder.cbwelldevelopedno.setEnabled(false);
            ;
            holder.cbwelldevelopedyes.setEnabled(false);
            ;
            holder.etwelldeveloped.setEnabled(false);
            ;

            holder.cbhairno.setEnabled(false);
            ;
            holder.cbhairyes.setEnabled(false);
            ;
            holder.ethair.setEnabled(false);
            ;

            holder.cbacneno.setEnabled(false);
            ;
            holder.cbacneyes.setEnabled(false);
            ;
            holder.etacne.setEnabled(false);
            ;

            holder.cbsecondarysexotherno.setEnabled(false);
            ;
            holder.cbsecondarysexotheryes.setEnabled(false);
            ;
            holder.etsecondarysexother.setEnabled(false);
            ;

            holder.cbpreabdomennormal.setEnabled(false);
            ;
            holder.cbpreabdomenabnormal.setEnabled(false);
            ;
            holder.etpreabdomen.setEnabled(false);
            ;

            holder.cbhealthyno.setEnabled(false);
            ;
            holder.cbhealthyyes.setEnabled(false);
            ;
            holder.ethealthy.setEnabled(false);
            ;

            holder.cbbleedingno.setEnabled(false);
            ;
            holder.cbbleedingyes.setEnabled(false);
            ;
            holder.etbleeding.setEnabled(false);
            ;

            holder.cblbcno.setEnabled(false);
            ;
            holder.cblbcyes.setEnabled(false);
            ;
            holder.etlbc.setEnabled(false);
            ;

            holder.tvavaf.setEnabled(false);
            ;
            holder.tvrvrf.setEnabled(false);
            ;
            holder.etuterusothers.setEnabled(false);


            holder.arrow.setBackgroundResource(R.drawable.triangle_arrow);

            holder.datecontainer.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    Log.d("POSITION_PASS", v.getTag(R.integer.tag1).toString() + " fdv");
                    int position = Integer.parseInt(v.getTag(R.integer.tag1).toString());
                    holder.container = arrLinear.get(position);
                    holder.arrow = arrImg.get(position);

                    if (holder.container.getVisibility() == View.VISIBLE) {
                        holder.container.setVisibility(View.GONE);
                        holder.arrow.setBackgroundResource(R.drawable.triangle_arrow);

                    } else {
                        holder.arrow.setBackgroundResource(R.drawable.triangle_up);
                        holder.container.setVisibility(View.VISIBLE);
//                        fragment.viewLmp((ParseObject) v.getTag(R.integer.tag2), arrListView.get(position));
                    }
//                    fragment.expandView(Integer.parseInt(v.getTag(R.integer.tag1).toString()));
                }
            });


//            if (type == 1) {

            if (patientobject.getString("createdDate") != null) {
                holder.date.setText(patientobject.getString("createdDate").toString());

            } else {

            }

            holder.weight_result = patientobject.getString("weight");
            if (holder.weight_result != null) {

                if (holder.weight_result.toString() != null && !(holder.weight_result.toString().equals("Nil"))) {
                    holder.etweight.setText(holder.weight_result.toString());
                }
            }

            holder.spo2_result = patientobject.getString("spo2");

            if (holder.spo2_result != null) {

                if (holder.spo2_result.toString() != null && !(holder.spo2_result.toString().equals("Nil"))) {
                    holder.etspo.setText(holder.spo2_result.toString());
                }
            }

            holder.bmi = patientobject.getString("bmi");


            if (holder.bmi != null) {

                if (holder.bmi.toString() != null && !(holder.bmi.toString().equals("Nil"))) {
                    holder.etbmi.setText(holder.bmi.toString());
                }
            }


            holder.pulse_result = patientobject.getString("pulse");


            if (holder.pulse_result != null) {

                if (holder.pulse_result.toString() != null && !(holder.pulse_result.toString().equals("Nil"))) {
                    holder.etpulse.setText(holder.pulse_result.toString());
                }
            }

            holder.respRate_result = patientobject.getString("respiratoryrate");

            if (holder.respRate_result != null) {

                if (holder.respRate_result.toString() != null && !(holder.respRate_result.toString().equals("Nil"))) {
                    holder.etrespiratory.setText(holder.respRate_result.toString());
                }
            }


            holder.height_result = patientobject.getString("height");


            if (holder.height_result != null) {

                if (holder.height_result.toString() != null && !(holder.height_result.toString().equals("Nil"))) {
                    holder.etheight.setText(holder.height_result.toString());
                }
            }


            holder.bloodGroup_result = patientobject.getString("bloodgroup");


            if (holder.bloodGroup_result != null) {

                if (holder.bloodGroup_result.toString() != null && !(holder.bloodGroup_result.toString().equals("Nil"))) {
                    holder.etBloodGroup.setText(holder.bloodGroup_result.toString());
                    holder.etBloodGroup.setEnabled(false);
                }

            }

            holder.bloodPressure_result = patientobject.getString("bloodpressure");
            String pressure[] = null;

            if (holder.bloodPressure_result != null) {
                pressure = patientobject.getString("bloodpressure").split("/");
                if (!(holder.bloodPressure_result.equals("Nil"))) {
                    if (pressure[0] != null) {
                        holder.etdiapressure.setText(pressure[0].toString());
                    }
                    if (pressure[1] != null) {
                        holder.etsyspressure.setText(pressure[1].toString());
                    }
                }
            }

            holder.temp_result = patientobject.getString("temperature");


            if (holder.temp_result != null) {
                if (holder.temp_result.toString() != null && !(holder.temp_result.toString().equals("Nil"))) {
                    holder.ettemperature.setText(holder.temp_result.toString());
                }

            }
                                    /*update_date=patientobject.getJSONArray("testedDate");
                                    if(update_date!=null){
										String result="";
										try{
											result=update_date.getString(update_date.length()-1);
										}catch(Exception e){
											e.printStackTrace();
										}
									}
									currentvitals_id=patientobject.getJSONArray("currentvitalsId");
									((FragmentActivityContainer)getActivity()).getpatientfrag().onButtonactivation(0);*/


            if (patientobject.getString("externalgenetalia") != null) {
                String externalgenetalia[] = patientobject.getString("externalgenetalia").split(",");
                if (externalgenetalia[0].equalsIgnoreCase("Normal")) {
                    holder.cbnormal.setChecked(true);
                } else if (externalgenetalia[0].equalsIgnoreCase("Abnormal")) {
                    holder.cbabnormal.setChecked(true);
                }
                if (!externalgenetalia[1].equalsIgnoreCase("Nil")) {
                    holder.etexternalgenetalia.setText(externalgenetalia[1]);
                }
            }


            if (patientobject.getString("breast_lump") != null) {
                String breastlump[] = patientobject.getString("breast_lump").split(",");
                if (breastlump[0].equalsIgnoreCase("No")) {
                    holder.cblumpno.setChecked(true);
                } else if (breastlump[0].equalsIgnoreCase("Yes")) {
                    holder.cblumpyes.setChecked(true);
                }
                if (!breastlump[1].equalsIgnoreCase("Nil")) {
                    holder.etlump.setText(breastlump[1]);
                }
            }


            if (patientobject.getString("breast_galactorrhea") != null) {
                String breastgalactorrhea[] = patientobject.getString("breast_galactorrhea").split(",");
                if (breastgalactorrhea[0].equalsIgnoreCase("No")) {
                    holder.cbgalactorrheano.setChecked(true);
                } else if (breastgalactorrhea[0].equalsIgnoreCase("Yes")) {
                    holder.cbgalactorrheayes.setChecked(true);
                }
                if (!breastgalactorrhea[1].equalsIgnoreCase("Nil")) {
                    holder.etgalactorrhea.setText(breastgalactorrhea[1]);
                }
            }


            if (patientobject.getString("breast_other") != null) {
                String breastother[] = patientobject.getString("breast_other").split(",");
                if (breastother[0].equalsIgnoreCase("No")) {
                    holder.cbbreastotherno.setChecked(true);
                } else if (breastother[0].equalsIgnoreCase("Yes")) {
                    holder.cbbreastotheryes.setChecked(true);
                }
                if (!breastother[1].equalsIgnoreCase("Nil")) {
                    holder.etbreastother.setText(breastother[1]);
                }
            }


            if (patientobject.getString("secondarysex_welldeveloped") != null) {
                String secondarysex_welldeveloped[] = patientobject.getString("secondarysex_welldeveloped").split(",");
                if (secondarysex_welldeveloped[0].equalsIgnoreCase("No")) {
                    holder.cbwelldevelopedno.setChecked(true);
                } else if (secondarysex_welldeveloped[0].equalsIgnoreCase("Yes")) {
                    holder.cbwelldevelopedyes.setChecked(true);
                }
                if (!secondarysex_welldeveloped[1].equalsIgnoreCase("Nil")) {
                    holder.etwelldeveloped.setText(secondarysex_welldeveloped[1]);
                }
            }


            if (patientobject.getString("secondarysex_hair") != null) {
                String secondarysex_hair[] = patientobject.getString("secondarysex_hair").split(",");
                if (secondarysex_hair[0].equalsIgnoreCase("No")) {
                    holder.cbhairno.setChecked(true);
                } else if (secondarysex_hair[0].equalsIgnoreCase("Yes")) {
                    holder.cbhairyes.setChecked(true);
                }
                if (!secondarysex_hair[1].equalsIgnoreCase("Nil")) {
                    holder.ethair.setText(secondarysex_hair[1]);
                }
            }


            if (patientobject.getString("secondarysex_acne") != null) {
                String secondarysex_acne[] = patientobject.getString("secondarysex_acne").split(",");
                if (secondarysex_acne[0].equalsIgnoreCase("No")) {
                    holder.cbacneno.setChecked(true);
                } else if (secondarysex_acne[0].equalsIgnoreCase("Yes")) {
                    holder.cbacneyes.setChecked(true);
                }
                if (!secondarysex_acne[1].equalsIgnoreCase("Nil")) {
                    holder.etacne.setText(secondarysex_acne[1]);
                }
            }


            if (patientobject.getString("secondarysex_other") != null) {
                String secondarysex_other[] = patientobject.getString("secondarysex_other").split(",");
                if (secondarysex_other[0].equalsIgnoreCase("No")) {
                    holder.cbsecondarysexotherno.setChecked(true);
                } else if (secondarysex_other[0].equalsIgnoreCase("Yes")) {
                    holder.cbsecondarysexotheryes.setChecked(true);
                }
                if (!secondarysex_other[1].equalsIgnoreCase("Nil")) {
                    holder.etsecondarysexother.setText(secondarysex_other[1]);
                }
            }


            if (patientobject.getString("preabdomenexamination") != null) {
                String preabdomenexamination[] = patientobject.getString("preabdomenexamination").split(",");
                if (preabdomenexamination[0].equalsIgnoreCase("Normal")) {
                    holder.cbpreabdomennormal.setChecked(true);
                } else if (preabdomenexamination[0].equalsIgnoreCase("Abnormal")) {
                    holder.cbpreabdomenabnormal.setChecked(true);
                }
                if (!preabdomenexamination[1].equalsIgnoreCase("Nil")) {
                    holder.etpreabdomen.setText(preabdomenexamination[1]);
                }
            }


            if (patientobject.getString("cervix_healthy") != null) {
                String cervix_healthy[] = patientobject.getString("cervix_healthy").split(",");
                if (cervix_healthy[0].equalsIgnoreCase("No")) {
                    holder.cbhealthyno.setChecked(true);
                } else if (cervix_healthy[0].equalsIgnoreCase("Yes")) {
                    holder.cbhealthyyes.setChecked(true);
                }
                if (!cervix_healthy[1].equalsIgnoreCase("Nil")) {
                    holder.ethealthy.setText(cervix_healthy[1]);
                }
            }


            if (patientobject.getString("cervix_bleeding") != null) {
                String cervix_bleeding[] = patientobject.getString("cervix_bleeding").split(",");
                if (cervix_bleeding[0].equalsIgnoreCase("No")) {
                    holder.cbbleedingno.setChecked(true);
                } else if (cervix_bleeding[0].equalsIgnoreCase("Yes")) {
                    holder.cbbleedingyes.setChecked(true);
                }
                if (!cervix_bleeding[1].equalsIgnoreCase("Nil")) {
                    holder.etbleeding.setText(cervix_bleeding[1]);
                }
            }


            if (patientobject.getString("cervix_lbc") != null) {
                String cervix_lbc[] = patientobject.getString("cervix_lbc").split(",");
                if (cervix_lbc[0].equalsIgnoreCase("No")) {
                    holder.cblbcno.setChecked(true);
                } else if (cervix_lbc[0].equalsIgnoreCase("Yes")) {
                    holder.cblbcyes.setChecked(true);
                }
                if (!cervix_lbc[1].equalsIgnoreCase("Nil")) {
                    holder.etlbc.setText(cervix_lbc[1]);
                }
            }


            if (patientobject.getString("uterus_avaf") != null) {
                String uterus_avaf = patientobject.getString("uterus_avaf");
                if (!uterus_avaf.equalsIgnoreCase("Nil")) {
                    holder.tvavaf.setText(uterus_avaf);
                }
            }


            if (patientobject.getString("uterus_rvrf") != null) {
                String uterus_rvrf = patientobject.getString("uterus_rvrf");
                if (!uterus_rvrf.equalsIgnoreCase("Nil")) {
                    holder.tvrvrf.setText(uterus_rvrf);
                }
            }


            if (patientobject.getString("uterus_others") != null) {
                String uterus_others = patientobject.getString("uterus_others");
                if (!uterus_others.equalsIgnoreCase("Nil")) {
                    holder.etuterusothers.setText(uterus_others);
                }
            }
            v.setTag(holder);
//
//            if (holder.weight_result != null ||
//                    holder.spo2_result != null ||
//                    holder.bmi != null ||
//                    holder.pulse_result != null ||
//                    holder.respRate_result != null ||
//                    holder.height_result != null ||
//                    holder.bloodGroup_result != null ||
//                    holder.bloodPressure_result != null ||
//                    holder.temp_result != null ||
//                    patientobject.getString("externalgenetalia") != null ||
//                    patientobject.getString("breast_lump") != null ||
//                    patientobject.getString("breast_galactorrhea") != null ||
//                    patientobject.getString("breast_other") != null ||
//                    patientobject.getString("secondarysex_welldeveloped") != null ||
//                    patientobject.getString("secondarysex_hair") != null ||
//                    patientobject.getString("secondarysex_acne") != null ||
//                    patientobject.getString("secondarysex_other") != null ||
//                    patientobject.getString("preabdomenexamination") != null ||
//                    patientobject.getString("cervix_healthy") != null ||
//                    patientobject.getString("cervix_bleeding") != null ||
//                    patientobject.getString("cervix_lbc") != null ||
//                    patientobject.getString("uterus_avaf") != null ||
//                    patientobject.getString("uterus_rvrf") != null ||
//                    patientobject.getString("uterus_others") != null) {
////                    return v;
//            } else {
////                v = convertView;
//
//            }
//            } else {
////                holder = (ViewHolder) v.getTag();
////                v = convertView;
//                objects.remove(position);
//                notifyDataSetChanged();
//
//                Log.d("TAG!1111", position + " fdv");
//            }
//            hashMap.put(position, v);
        }
//        else {
//            holder = (ViewHolder) v.getTag();
//            v = convertView;
//        }

        return v;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }
}
