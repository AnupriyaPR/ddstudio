package com.wiinnova.doctorsdiary.supportclasses;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.wiinnova.doctorsdiary.R;
import com.wiinnova.doctorsdiary.fragment.Patient_Current_Medical_Info_Frag;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

import static com.wiinnova.doctorsdiary.fragment.Patient_Current_Medical_Info_Frag.term;
import static com.wiinnova.doctorsdiary.fragment.Patient_Current_Medical_Info_Frag.abortion;
import static com.wiinnova.doctorsdiary.fragment.Patient_Current_Medical_Info_Frag.type1;
import static com.wiinnova.doctorsdiary.fragment.Patient_Current_Medical_Info_Frag.type2;
import static com.wiinnova.doctorsdiary.fragment.Patient_Current_Medical_Info_Frag.health;
import static com.wiinnova.doctorsdiary.fragment.Patient_Current_Medical_Info_Frag.child;
import static com.wiinnova.doctorsdiary.fragment.Patient_Current_Medical_Info_Frag.jsonYear;
import static com.wiinnova.doctorsdiary.fragment.Patient_Current_Medical_Info_Frag.week;

/**
 * Created by ubundu on 31/3/16.
 */
public class ArrayAdapterPregnancyPrevious extends BaseAdapter {
    Activity activity;
    Patient_Current_Medical_Info_Frag fragment;
    private static LayoutInflater inflater = null;

    public ArrayAdapterPregnancyPrevious(Activity activity, Patient_Current_Medical_Info_Frag fragment) {
        this.activity = activity;
        this.fragment = fragment;
        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount() {

        Log.d("Pregnancydsdsf", term.length + " d");
        return term.length;
    }

    public class ViewHolder {
        TextView txtTerm;
        TextView txtAbotion;
        TextView txtType1;
        TextView txtType2;
        TextView txtHealth;
        TextView txtChild;
        TextView txtYear;
        TextView txtWeek;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.layout_pregnancy_previous, null);
        }
        ViewHolder holder = new ViewHolder();
        holder.txtAbotion = (TextView) convertView.findViewById(R.id.txtAbortion);
        holder.txtChild = (TextView) convertView.findViewById(R.id.txtChild);
        holder.txtHealth = (TextView) convertView.findViewById(R.id.txtHealth);
        holder.txtTerm = (TextView) convertView.findViewById(R.id.txtTerm);
        holder.txtType1 = (TextView) convertView.findViewById(R.id.txtType1);
        holder.txtType2 = (TextView) convertView.findViewById(R.id.txtType2);
        holder.txtWeek = (TextView) convertView.findViewById(R.id.txtWeek);
        holder.txtYear = (TextView) convertView.findViewById(R.id.txtYear);

//        if (jsonYear[position].equals("Nil") && week[position].equals("Nil") && type2[position].equals("Nil") && type1[position].equals("Nil")
//                && term[position].equals("Nil") && abortion[position].equals("Nil") && child[position].equals("Nil") && health[position].equals("Nil")) {
//            /*if all fields are nil, remove that */
//            ArrayList<String> testTerm = new ArrayList<String>();
//            for (int i = 0; i < term.length; i++) {
//                testTerm.add(term[i]);
//            }
//            testTerm.remove(position);
//            for (int i = 0; i < testTerm.size(); i++) {
//                term[i] = testTerm.get(i);
//            }
//            notifyDataSetChanged();
////               fragment.removePregnancyRaw(position);
//        } else {
            Log.d("YearGet", jsonYear[position]);
            holder.txtYear.setText(jsonYear[position]);
            holder.txtWeek.setText(week[position]);
            holder.txtType2.setText(type2[position]);
            holder.txtType1.setText(type1[position]);
            holder.txtTerm.setText(term[position]);
            holder.txtAbotion.setText(abortion[position]);
            holder.txtChild.setText(child[position]);
            holder.txtHealth.setText(health[position]);
//        }
        return convertView;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


}
