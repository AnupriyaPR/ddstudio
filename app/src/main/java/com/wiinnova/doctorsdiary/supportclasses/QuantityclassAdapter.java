package com.wiinnova.doctorsdiary.supportclasses;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.wiinnova.doctorsdiary.R;

import java.util.ArrayList;

public class QuantityclassAdapter extends BaseAdapter{

	private Context context;
	private ArrayList<Quantitynameclass> quantityItems;
	private ArrayList<Quantitynameclass> selectedquantity;

	ArrayList<CheckBox> checkboxes=new ArrayList<CheckBox>();
	ArrayList<RelativeLayout> containers=new ArrayList<RelativeLayout>();
	ArrayList<Boolean> selectedcheckbox=new ArrayList<Boolean>();
	ArrayList<Quantitynameclass> selectquantity=new ArrayList<Quantitynameclass>();
	boolean flag=true;

	public QuantityclassAdapter(Context activity,ArrayList<Quantitynameclass> quantiyname, ArrayList<Quantitynameclass> selectedquantiyname) {
		// TODO Auto-generated constructor stub

		this.context=activity;
		this.quantityItems=quantiyname;
		this.selectedquantity=selectedquantiyname;

		for(int i=0;i<quantityItems.size();i++){
			selectedcheckbox.add(i, false);
			quantityItems.get(i).setSelected(false);
		}
		if(selectedquantiyname!=null){
			for(int j=0;j<quantityItems.size();j++){
				for(int k=0;k<selectedquantiyname.size();k++){

					if(selectedquantiyname.get(k).getQuantityname().equalsIgnoreCase(quantityItems.get(j).getQuantityname())){
						System.out.println("selected check box "+j);
						selectedcheckbox.set(j, true);
						quantityItems.get(j).setSelected(true);
					}
				}
			}
		}

	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return quantityItems.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return quantityItems.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		if (convertView == null) 
		{
			LayoutInflater mInflater = (LayoutInflater)
					context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
			convertView = mInflater.inflate(R.layout.disease_listitem, null);

		}
		RelativeLayout container=(RelativeLayout)convertView.findViewById(R.id.lldiseasecontainer);
		TextView txtTitle = (TextView) convertView.findViewById(R.id.title);   
		final CheckBox checkbox=(CheckBox) convertView.findViewById(R.id.chklist);
		checkbox.setTag(position);
		checkboxes.add(checkbox);
		containers.add(container);

		if(quantityItems.get(position).getQuantityname()!=null){
			txtTitle.setText(quantityItems.get(position).getQuantityname());
		}

		if(selectedquantity!=null){
			if(selectedcheckbox.get(position)==true){
				checkbox.setChecked(true);
				System.out.println("position checked"+position);
			}else{
				checkbox.setChecked(false);
			}
		}

		/*if(selectedquantity!=null){
			if(quantityItems.get(position).isSelected()){
				checkbox.setChecked(true);
				System.out.println("position checked"+position);
			}else{
				checkbox.setChecked(false);

			}
		}*/

		checkbox.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(checkbox.isChecked()){
					selectedcheckbox.set(position, true);
					quantityItems.get(position).setSelected(true);
				}else{
					System.out.println("false working.......");
					selectedcheckbox.set(position, false);
					quantityItems.get(position).setSelected(false);
				}
				//notifyDataSetChanged();
			}
		});

		container.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				checkbox.toggle();
				if( checkbox.isChecked()){
					selectedcheckbox.set(position, true);
					quantityItems.get(position).setSelected(true);
				}else{
					selectedcheckbox.set(position, false);
					quantityItems.get(position).setSelected(false);
				}
				//notifyDataSetChanged();
			}
		});

		return convertView;
	}

	public ArrayList<Quantitynameclass> getSelectedquantityitem()
	{
		selectquantity.clear();
		System.out.println("quantity item size@@@@@####"+quantityItems.size());
		for(int i=0;i<quantityItems.size();i++){
			if(quantityItems.get(i).isSelected()){
				selectquantity.add(quantityItems.get(i));
			}
		}
		return selectquantity;
	}



}
