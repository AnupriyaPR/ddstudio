package com.wiinnova.doctorsdiary.supportclasses;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;

import com.wiinnova.doctorsdiary.R;
import com.wiinnova.doctorsdiary.activities.FragmentActivityContainer;

/**
 * Created by ubundu on 10/3/16.
 */
public class PublicTextWatcher implements TextWatcher {
    View view;

    public PublicTextWatcher(View view) {
        this.view = view;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        switch (view.getId()) {
            case R.id.pstate:
                break;
        }

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {


    }

    @Override
    public void afterTextChanged(Editable s) {
    }
}

