package com.wiinnova.doctorsdiary.supportclasses;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wiinnova.doctorsdiary.R;
import com.parse.ParseObject;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.List;

public class Expandablepatientmedinfolistadapter extends BaseExpandableListAdapter {

	private final Activity context;
	List<ParseObject> obj;
	ParseObject vitalsobject;
	String formattedDate;
	String formattedDate1;
	String[] days;

	public Expandablepatientmedinfolistadapter(Activity context,List<ParseObject> patientobject,ParseObject Object) {
		// TODO Auto-generated constructor stub

		this.context=context;
		this.obj=patientobject;
		this.vitalsobject=Object;
		System.out.println("parse object"+obj.size());

	}


	
	@Override
	public Object getChild(int grouppositionn, int childposition) {
		// TODO Auto-generated method stub
		return childposition;
	}

	@Override
	public long getChildId(int arg0, int arg1) {
		// TODO Auto-generated method stub
		return 0;
	}

	
	
	@Override
	public View getChildView(int groupposition, int childposition, boolean islastchild, View convertView,
			ViewGroup parent) {
		// TODO Auto-generated method stub

		System.out.println("layout inflate calling1 ");
		if(convertView==null){

			LayoutInflater infalInflater = context.getLayoutInflater();
			convertView = infalInflater.inflate(R.layout.expandablelist_childitem, null);
			
		}

			EditText metSymptoms =(EditText)convertView.findViewById(R.id.etsymptoms);
			EditText metSyndromes=(EditText)convertView.findViewById(R.id.etsyndromes);
			EditText metComments=(EditText)convertView.findViewById(R.id.etcomments);
			EditText metSuspected=(EditText)convertView.findViewById(R.id.etsuspecteddisease);


			EditText metWeight=(EditText)convertView.findViewById(R.id.etweight);
			EditText metBloodPresure=(EditText)convertView.findViewById(R.id.etbloodpressure);
			EditText metPulse=(EditText)convertView.findViewById(R.id.etpulse);
			EditText metRespiratory=(EditText)convertView.findViewById(R.id.etrespiratory);
			EditText metSpo2=(EditText)convertView.findViewById(R.id.etsp);
			EditText metTemperature=(EditText)convertView.findViewById(R.id.ettemperature);
			LinearLayout lldrugs=(LinearLayout)convertView.findViewById(R.id.lldrugscontainer);
			 lldrugs.removeAllViews();
			



		 metSymptoms.setText(obj.get(groupposition).getString("symptoms"));
		 metSyndromes.setText(obj.get(groupposition).getString("syndromes"));
		 metSuspected.setText(obj.get(groupposition).getString("suspectedDisease"));
		 metComments.setText(obj.get(groupposition).getString("additionalComments"));

		 metWeight.setText(vitalsobject.getString("weight"));
		 metBloodPresure.setText(vitalsobject.getString("bloodPressure"));
		 metSpo2.setText(vitalsobject.getString("spo2"));
		 metTemperature.setText(vitalsobject.getString("temp"));
		 metPulse.setText(vitalsobject.getString("pulse"));
		 metRespiratory.setText(vitalsobject.getString("respRate"));


		 metSymptoms.setEnabled(false);
		 metSyndromes.setEnabled(false);
		 metSymptoms.setEnabled(false);
		 metSuspected.setEnabled(false);
		 metComments.setEnabled(false);
		 metWeight.setEnabled(false);
		 metBloodPresure.setEnabled(false);
		 metSpo2.setEnabled(false);
		 metTemperature.setEnabled(false);
		 metPulse.setEnabled(false);
		 metRespiratory.setEnabled(false);

		 metSuspected.measure(0, 0);
		 metComments.measure(0, 0);

		int susheight= metSuspected.getMeasuredHeight();
		int comheight= metComments.getMeasuredHeight();
		int symptheight= metSymptoms.getMeasuredHeight();
		int syndheight= metSyndromes.getMeasuredHeight();
		System.out.println("metSuspected height"+ metSuspected.getMeasuredHeight());
		System.out.println("metComments height"+ metComments.getMeasuredHeight());

		if(symptheight>syndheight){
			 metSyndromes.setHeight(symptheight);
		}if(symptheight<syndheight){
			 metSymptoms.setHeight(syndheight);
		}if(susheight>comheight){
			 metComments.setHeight(susheight);
		}if(susheight<comheight){
			 metSuspected.setHeight(comheight);
		}

		System.out.println("successssssssssss"+groupposition);
		JSONArray drugObj=obj.get(groupposition).getJSONArray("drug");
		JSONArray dosageObj=obj.get(groupposition).getJSONArray("dosage");
		JSONArray dateObj=obj.get(groupposition).getJSONArray("drugStartDate");
		JSONArray durationObj=obj.get(groupposition).getJSONArray("duration");
		JSONArray QuantityObj=obj.get(groupposition).getJSONArray("quantity");

		JSONArray daysObj=obj.get(groupposition).getJSONArray("days");

		System.out.println("drugobject sie"+drugObj.length());
		System.out.println("layout inflate calling");
		for(int j=0;j<drugObj.length();j++){
			View patient_treatment_details1;
				LayoutInflater inflater1 = LayoutInflater.from(context);

				patient_treatment_details1 = inflater1.inflate(R.layout.patientmedinfodrugsinflate, null); 

				EditText drug=(EditText)patient_treatment_details1.findViewById(R.id.ctmdrugedit);
				EditText dosage=(EditText)patient_treatment_details1.findViewById(R.id.ctmdrugdosage);
				TextView drugdate=(TextView)patient_treatment_details1.findViewById(R.id.ctmdrugdate);
				EditText duration=(EditText)patient_treatment_details1.findViewById(R.id.ctmdrugduration);
				EditText quantity=(EditText)patient_treatment_details1.findViewById(R.id.ctmdrugquantity);
				ImageView  iv_monday=(ImageView)patient_treatment_details1.findViewById(R.id.ivdays1_monday);
				ImageView iv_tuesday=(ImageView)patient_treatment_details1.findViewById(R.id.ivdays1_tuesday);
				ImageView iv_wed=(ImageView)patient_treatment_details1.findViewById(R.id.ivdays1_wed);
				ImageView iv_thu=(ImageView)patient_treatment_details1.findViewById(R.id.ivdays1_thu);
				ImageView iv_fri=(ImageView)patient_treatment_details1.findViewById(R.id.ivdays1_fri);
				ImageView iv_sat=(ImageView)patient_treatment_details1.findViewById(R.id.ivdays1_sat);
				ImageView iv_sun=(ImageView)patient_treatment_details1.findViewById(R.id.ivdays1_sun);
				CheckBox daily=(CheckBox)patient_treatment_details1.findViewById(R.id.ctmdrugdaily);

				
				 lldrugs.addView(patient_treatment_details1);
			
			try {
				 drug.setText(drugObj.getString(j));
				 dosage.setText(dosageObj.getString(j));
				 duration.setText(durationObj.getString(j));
				 quantity.setText(QuantityObj.getString(j));
				 drugdate.setText(dateObj.getString(j));

				if(daysObj!=null){
					if(daysObj.length()!=0){

						if(daysObj.getString(j).equals("ALL"))
						{
							System.out.println("workinggggggggggg");
							 daily.setChecked(true);
							 iv_monday.setImageResource(R.drawable.days_monday_2);
							 iv_tuesday.setImageResource(R.drawable.days_tuesday_2);
							 iv_wed.setImageResource(R.drawable.days_wed_2);
							 iv_thu.setImageResource(R.drawable.days_thu_2);
							 iv_fri.setImageResource(R.drawable.days_fri_2);
							 iv_sat.setImageResource(R.drawable.days_sat_2);
							 iv_sun.setImageResource(R.drawable.days_sun_2);

						}else{
							String var=daysObj.getString(j);
							System.out.println("variableeeee"+var);
							System.out.println("length"+var.length());
							//days=new String[var.length()];
							days=var.split(",");
							for(int k=0;k<days.length;k++){

								System.out.println("days"+days);
								System.out.println("days valueeeeeeeeee"+days[k]);

								if(days[k].equals("M")){
									 iv_monday.setImageResource(R.drawable.days_monday_2);
								}if(days[k].equals("T")){
									 iv_tuesday.setImageResource(R.drawable.days_tuesday_2);
								}if(days[k].equals("W")){
									 iv_wed.setImageResource(R.drawable.days_wed_2);
								}if(days[k].equals("TH")){
									 iv_thu.setImageResource(R.drawable.days_thu_2);
								}if(days[k].equals("F")){
									 iv_fri.setImageResource(R.drawable.days_fri_2);
								}if(days[k].equals("S")){
									 iv_sat.setImageResource(R.drawable.days_sat_2);
								}if(days[k].equals("SU")){
									 iv_sun.setImageResource(R.drawable.days_sun_2);
								}
							}


						}

						 drug.setEnabled(false);
						 dosage.setEnabled(false);
						 drugdate.setEnabled(false);
						 duration.setEnabled(false);
						 daily.setEnabled(false);
						 quantity.setEnabled(false);
					}else{

						 drug.setVisibility(View.GONE);
						 dosage.setVisibility(View.GONE);
						 drugdate.setVisibility(View.GONE);
						 duration.setVisibility(View.GONE);
						 daily.setVisibility(View.GONE);
						 quantity.setVisibility(View.GONE);

					}
				}



			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}





		return convertView;
	}

	@Override
	public int getChildrenCount(int arg0) {
		// TODO Auto-generated method stub
		return 1;
	}

	@Override
	public Object getGroup(int arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getGroupCount() {
		// TODO Auto-generated method stub
		return obj.size();
	}

	@Override
	public long getGroupId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		if(convertView==null){

			LayoutInflater infalInflater = context.getLayoutInflater();
			convertView = infalInflater.inflate(R.layout.expandablelist_item, null);

		}

		LinearLayout datecontaier=(LinearLayout)convertView.findViewById(R.id.lldatecontainer);
		final ImageView arrow=(ImageView)convertView.findViewById(R.id.ivarrow);
		TextView tvdate=(TextView)convertView.findViewById(R.id.cdcurrdate);

		arrow.setBackgroundResource(R.drawable.signup_arrow_down);
		System.out.println("groupposition"+groupPosition);
		formattedDate=obj.get(groupPosition).getString("createdAt");
		System.out.println("dateeeeeeeee"+obj.get(groupPosition).getString("createdAt"));
		tvdate.setText(formattedDate);
		
		if(isExpanded){
			arrow.setBackgroundResource(R.drawable.signup_arrow_up);
		}
		
		return convertView;
	}

	@Override
	public boolean hasStableIds() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		// TODO Auto-generated method stub
		return true;
	}

}
