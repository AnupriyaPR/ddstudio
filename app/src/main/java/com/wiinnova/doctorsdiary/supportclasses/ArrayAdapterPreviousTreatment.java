package com.wiinnova.doctorsdiary.supportclasses;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.parse.ParseObject;
import com.wiinnova.doctorsdiary.R;

import java.util.List;

/**
 * Created by ubundu on 22/3/16.
 */
public class ArrayAdapterPreviousTreatment extends BaseAdapter {
    Activity activity;
    List<ParseObject> objectslist;
    private static LayoutInflater inflater = null;
    ViewHolder holder;
    String formattedDate;

    public ArrayAdapterPreviousTreatment(Activity activity, List<ParseObject> objectslist) {
        this.activity = activity;
        this.objectslist = objectslist;
        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount() {
        return objectslist.size();
    }
    public class ViewHolder{

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        holder = new ViewHolder();
        View v = convertView;
        if (v == null) {

            LinearLayout datecontaier = (LinearLayout) v.findViewById(R.id.lldatecontainer);
            final LinearLayout detailscontainer = (LinearLayout) v.findViewById(R.id.llpatient_details_container);

            final ImageView arrow = (ImageView) v.findViewById(R.id.ivarrow);
            arrow.setBackgroundResource(R.drawable.triangle_arrow);
            TextView tvdate = (TextView) v.findViewById(R.id.cdcurrdate);

            formattedDate = objectslist.get(position).getString("createdAt");
            System.out.println("dateeeeeeeee" + objectslist.get(position).getString("createdAt"));
            tvdate.setText(formattedDate);

            v = inflater.inflate(R.layout.diagnosisexaminationviewgynacologist, null);
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public int getViewTypeCount() {

        if (getCount() != 0)
            return getCount();

        return 1;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}
