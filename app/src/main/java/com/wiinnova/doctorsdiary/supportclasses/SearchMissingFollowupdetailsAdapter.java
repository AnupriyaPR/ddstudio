package com.wiinnova.doctorsdiary.supportclasses;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.wiinnova.doctorsdiary.R;
import com.parse.ParseObject;

import java.util.List;

public class SearchMissingFollowupdetailsAdapter extends BaseAdapter{
	private LayoutInflater inflater;
	private int selected = -1;
	private Context context;
	List<ParseObject>  results;
	List<String>  patientid;
	List<String>  firstname;
	List<String>  lastname;
	List<String>  lastvisit;
	List<String>  phonenumber;
	List<String>  email;
	List<String>  followup;


	public SearchMissingFollowupdetailsAdapter(Context context, List<String>  patientid,List<String>  firstname,List<String>  lastname,List<String>  followup,List<String>  lastvisit,List<String>  phonenumber,List<String>  email) {
		// TODO Auto-generated constructor stub
		this.context = context;
		System.out.println("patientid"+patientid.size());
		System.out.println("firstname"+firstname.size());

		this.patientid = patientid;
		this.firstname = firstname;
		this.lastname = lastname;
		this.lastvisit = lastvisit;
		this.phonenumber = phonenumber;
		this.email = email;
		this.followup = followup;
		inflater = LayoutInflater.from(context);
	}
	

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return patientid.size();
	}

	@Override
	public 	String getItem(int position) {
		// TODO Auto-generated method stub
		return patientid.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	public void setSelected(int position){
		this.selected = position;
		SearchMissingFollowupdetailsAdapter.this.notifyDataSetChanged();
	}

	public int getSelected(){
		return this.selected;
	}

	ViewHolder holder;

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		if (convertView == null){
			holder = new ViewHolder();
			convertView = inflater.inflate(R.layout.search_missing_followup_details_list, null); 
			holder.followup = (TextView) convertView.findViewById(R.id.followup);
			holder.slNo = (TextView) convertView.findViewById(R.id.slno);
			holder.patientId = (TextView) convertView.findViewById(R.id.patientid);
			holder.Firstname = (TextView) convertView.findViewById(R.id.firstname);
			holder.Lastname = (TextView) convertView.findViewById(R.id.lastname);
			holder.Lastvisit = (TextView) convertView.findViewById(R.id.lastvisit);
			holder.Phonenumber = (TextView) convertView.findViewById(R.id.phone);
			holder.Email = (TextView) convertView.findViewById(R.id.email);
			convertView.setTag(holder);
		}
		else
			holder = (ViewHolder) convertView.getTag();

		holder.slNo.setText(""+(position + 1));
		System.out.println("position number"+position);
		
		holder.patientId.setText(patientid.get(position));
		holder.Firstname.setText(firstname.get(position));
		holder.Lastname.setText(lastname.get(position));
		holder.Lastvisit.setText(lastvisit.get(position));
		holder.Phonenumber.setText(phonenumber.get(position));
		holder.Email.setText(email.get(position));
		holder.followup.setText(followup.get(position));


		if (selected == position)
			convertView.setBackgroundColor(context.getResources().getColor(R.color.blue_color));
		else
			convertView.setBackgroundColor(Color.TRANSPARENT);

		return convertView;
	}

	static class ViewHolder{
		public TextView Email;
		public TextView Phonenumber;
		public TextView Lastvisit;
		public TextView Lastname;
		public TextView Firstname;
		public TextView patientId;
		public TextView slNo;
		public TextView followup;
		TextView item;
		TextView key;
	}

}


