package com.wiinnova.doctorsdiary.supportclasses;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;

/**
 * Created by ubundu on 18/3/16.
 */
public class AppUtil {

    public static boolean haveNetwokConnection(Context context) {
        boolean isConnected = false;
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
        return isConnected;
    }

    public static void justifyListViewHeightBasedOnChildren(ListView listView) {
        try {

            ListAdapter adapter = listView.getAdapter();

            if (adapter == null) {
                return;
            }
            ViewGroup vg = listView;
            int totalHeight = 0;
            for (int i = 0; i < adapter.getCount(); i++) {
                View listItem = adapter.getView(i, null, vg);
                listItem.measure(0, 0);
                totalHeight += listItem.getMeasuredHeight();
            }

            ViewGroup.LayoutParams par = listView.getLayoutParams();
            par.height = totalHeight + (listView.getDividerHeight() * (adapter.getCount() - 1));
            listView.setLayoutParams(par);
            listView.requestLayout();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
