package com.wiinnova.doctorsdiary.supportclasses;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.wiinnova.doctorsdiary.R;

public class SpiinerAdapter extends ArrayAdapter<String>{
	private Context activity;
	private String[] elements;
	
	public SpiinerAdapter(Context context, String[] objects) {
		super(context, R.layout.spinner_item,objects);
		// TODO Auto-generated constructor stub
	this.activity=context;
	this.elements=objects;
	
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		
		LayoutInflater inflater = (LayoutInflater) activity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        final View rowView = inflater.inflate(R.layout.spinner_item, parent, false);
        System.out.println("elements"+elements[position]);
        TextView tv=(TextView)rowView.findViewById(R.id.tvItem);
        tv.setText(elements[position]);
        return rowView;

	}
	
	
	

}
