package com.wiinnova.doctorsdiary.supportclasses;

import java.io.Serializable;

public class Diseasenameclass implements Serializable{

	String diseasename;
	String icdnumber;
	boolean selected=false;


	public boolean isSelected() {
		return selected;
	}
	public void setSelected(boolean selected) {
		this.selected = selected;
	}
	public String getDiseasename() {
		return diseasename;
	}
	public void setDiseasename(String diseasename) {
		this.diseasename = diseasename;
	}
	public String getIcdnumber() {
		return icdnumber;
	}
	public void setIcdnumber(String icdnumber) {
		this.icdnumber = icdnumber;
	}
}
