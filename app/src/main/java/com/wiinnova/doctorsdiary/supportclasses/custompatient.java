package com.wiinnova.doctorsdiary.supportclasses;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wiinnova.doctorsdiary.R;
import com.parse.ParseObject;

import org.json.JSONArray;
import org.json.JSONException;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class custompatient extends BaseAdapter {
	private final Activity context;
	List<ParseObject> obj1;
	List<ParseObject> obj2;
	ParseObject vitalsobject;
	String formattedDate;
	String formattedDate1;
	String[] days;


	public custompatient(Activity context,List<ParseObject> object1,List<ParseObject> object2,ParseObject Object) {
		// TODO Auto-generated constructor stub

		this.context=context;
		this.obj1=object1;
		this.obj2=object2;
		this.vitalsobject=Object;
		//System.out.println("parse object"+obj);

	}

	@Override
	public View getView(final int position, View view, ViewGroup parent) {
		// TODO Auto-generated method stub
		View rowView=view;
		//if(obj.get(1).getDouble("typeFlag")==1){
		LayoutInflater inflater = context.getLayoutInflater();

		rowView= inflater.inflate(R.layout.patientmedifolistitems, null, true);

		
		
	
			System.out.println("type flag 1 working..............");
				LinearLayout datecontaier=(LinearLayout)rowView.findViewById(R.id.lldatecontainer);
				final LinearLayout detailscontainer=(LinearLayout)rowView.findViewById(R.id.llpatient_details_container);

				TextView tvdate=(TextView)rowView.findViewById(R.id.cdcurrdate);
				Date create=obj1.get(position).getCreatedAt();



				Calendar c = Calendar.getInstance();
				System.out.println("Current time => " + c.getTime());
				c.setTime(create);
				SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
				formattedDate = df.format(c.getTime());
				tvdate.setText(formattedDate);


				final EditText metSymptoms=(EditText)rowView.findViewById(R.id.etsymptoms);
				final EditText metSyndromes=(EditText)rowView.findViewById(R.id.etsyndromes);
				final EditText metComments=(EditText)rowView.findViewById(R.id.etcomments);
				final EditText metSuspected=(EditText)rowView.findViewById(R.id.etsuspecteddisease);


				final EditText metWeight=(EditText)rowView.findViewById(R.id.etweight);
				final EditText	metBloodPresure=(EditText)rowView.findViewById(R.id.etbloodpressure);
				final EditText metPulse=(EditText)rowView.findViewById(R.id.etpulse);
				final EditText metRespiratory=(EditText)rowView.findViewById(R.id.etrespiratory);
				final EditText metSpo2=(EditText)rowView.findViewById(R.id.etsp);
				final EditText metTemperature=(EditText)rowView.findViewById(R.id.ettemperature);
				final LinearLayout lldrugs=(LinearLayout)rowView.findViewById(R.id.lldrugscontainer);

				TextView mtvPatientid=(TextView)rowView.findViewById(R.id.cduniqueid);
				TextView mtvPatientname=(TextView)rowView.findViewById(R.id.tvfirst_last_name);

				datecontaier.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View arg0) {
						// TODO Auto-generated method stub
						if(detailscontainer.getVisibility()==View.VISIBLE){
							detailscontainer.setVisibility(View.GONE);
						}else{
							detailscontainer.setVisibility(View.VISIBLE);
							metSymptoms.setText(obj1.get(position).getString("symptoms"));
							metSyndromes.setText(obj1.get(position).getString("syndromes"));
							metSuspected.setText(obj1.get(position).getString("suspectedDisease"));
							metComments.setText(obj1.get(position).getString("additionalComments"));

							metWeight.setText(vitalsobject.getString("weight"));
							metBloodPresure.setText(vitalsobject.getString("bloodPressure"));
							metSpo2.setText(vitalsobject.getString("spo2"));
							metTemperature.setText(vitalsobject.getString("temp"));
							metPulse.setText(vitalsobject.getString("pulse"));
							metRespiratory.setText(vitalsobject.getString("respRate"));


							metSymptoms.setEnabled(false);
							metSyndromes.setEnabled(false);
							metSymptoms.setEnabled(false);
							metSuspected.setEnabled(false);
							metComments.setEnabled(false);
							metWeight.setEnabled(false);
							metBloodPresure.setEnabled(false);
							metSpo2.setEnabled(false);
							metTemperature.setEnabled(false);
							metPulse.setEnabled(false);
							metRespiratory.setEnabled(false);

							metSuspected.measure(0, 0);
							metComments.measure(0, 0);

							int susheight=metSuspected.getMeasuredHeight();
							int comheight=metComments.getMeasuredHeight();
							int symptheight=metSymptoms.getMeasuredHeight();
							int syndheight=metSyndromes.getMeasuredHeight();
							System.out.println("metSuspected height"+metSuspected.getMeasuredHeight());
							System.out.println("metComments height"+metComments.getMeasuredHeight());

							if(symptheight>syndheight){
								metSyndromes.setHeight(symptheight);
							}if(symptheight<syndheight){
								metSymptoms.setHeight(syndheight);
							}if(susheight>comheight){
								metComments.setHeight(susheight);
							}if(susheight<comheight){
								metSuspected.setHeight(comheight);
							}



							for(int i=0;i<obj2.size();i++){
								if(obj2.get(i).getDouble("typeFlag")==2){

									Date create=obj2.get(i).getCreatedAt();

									Calendar c = Calendar.getInstance();
									System.out.println("Current time => " + c.getTime());
									c.setTime(create);
									SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
									formattedDate1 = df.format(c.getTime());

									if(formattedDate.equals(formattedDate1)){
										System.out.println("successssssssssss"+i);
										JSONArray drugObj=obj2.get(i).getJSONArray("drug");
										JSONArray dosageObj=obj2.get(i).getJSONArray("dosage");
										JSONArray dateObj=obj2.get(i).getJSONArray("drugStartDate");
										JSONArray durationObj=obj2.get(i).getJSONArray("duration");
										JSONArray QuantityObj=obj2.get(i).getJSONArray("quantity");

										JSONArray daysObj=obj2.get(i).getJSONArray("days");
										for(int j=0;j<drugObj.length();j++){

											LayoutInflater inflater1 = LayoutInflater.from(context);
											View patient_treatment_details1 = inflater1.inflate(R.layout.patientmedinfodrugsinflate, null); 

											EditText drug=(EditText)patient_treatment_details1.findViewById(R.id.ctmdrugedit);
											EditText dosage=(EditText)patient_treatment_details1.findViewById(R.id.ctmdrugdosage);
											TextView drugdate=(TextView)patient_treatment_details1.findViewById(R.id.ctmdrugdate);
											EditText duration=(EditText)patient_treatment_details1.findViewById(R.id.ctmdrugduration);
											EditText quantity=(EditText)patient_treatment_details1.findViewById(R.id.ctmdrugquantity);
											final ImageView  iv_monday=(ImageView)patient_treatment_details1.findViewById(R.id.ivdays1_monday);
											final ImageView  iv_tuesday=(ImageView)patient_treatment_details1.findViewById(R.id.ivdays1_tuesday);
											final ImageView  iv_wed=(ImageView)patient_treatment_details1.findViewById(R.id.ivdays1_wed);
											final ImageView  iv_thu=(ImageView)patient_treatment_details1.findViewById(R.id.ivdays1_thu);
											final ImageView  iv_fri=(ImageView)patient_treatment_details1.findViewById(R.id.ivdays1_fri);
											final ImageView  iv_sat=(ImageView)patient_treatment_details1.findViewById(R.id.ivdays1_sat);
											final ImageView  iv_sun=(ImageView)patient_treatment_details1.findViewById(R.id.ivdays1_sun);
											final CheckBox	daily=(CheckBox)patient_treatment_details1.findViewById(R.id.ctmdrugdaily);



											lldrugs.addView(patient_treatment_details1);

											try {
												drug.setText(drugObj.getString(j));
												dosage.setText(dosageObj.getString(j));
												duration.setText(durationObj.getString(j));
												quantity.setText(QuantityObj.getString(j));
												drugdate.setText(dateObj.getString(j));

												if(daysObj!=null){
													if(daysObj.length()!=0){

														if(daysObj.getString(j).equals("ALL"))
														{
															System.out.println("workinggggggggggg");
															daily.setChecked(true);
															iv_monday.setImageResource(R.drawable.days_monday_2);
															iv_tuesday.setImageResource(R.drawable.days_tuesday_2);
															iv_wed.setImageResource(R.drawable.days_wed_2);
															iv_thu.setImageResource(R.drawable.days_thu_2);
															iv_fri.setImageResource(R.drawable.days_fri_2);
															iv_sat.setImageResource(R.drawable.days_sat_2);
															iv_sun.setImageResource(R.drawable.days_sun_2);

														}else{
															String var=daysObj.getString(j);
															System.out.println("variableeeee"+var);
															System.out.println("length"+var.length());
															//days=new String[var.length()];
															days=var.split(",");
															for(int k=0;k<days.length;k++){

																System.out.println("days"+days);
																System.out.println("days valueeeeeeeeee"+days[k]);

																if(days[k].equals("M")){
																	iv_monday.setImageResource(R.drawable.days_monday_2);
																}if(days[k].equals("T")){
																	iv_tuesday.setImageResource(R.drawable.days_tuesday_2);
																}if(days[k].equals("W")){
																	iv_wed.setImageResource(R.drawable.days_wed_2);
																}if(days[k].equals("TH")){
																	iv_thu.setImageResource(R.drawable.days_thu_2);
																}if(days[k].equals("F")){
																	iv_fri.setImageResource(R.drawable.days_fri_2);
																}if(days[k].equals("S")){
																	iv_sat.setImageResource(R.drawable.days_sat_2);
																}if(days[k].equals("SU")){
																	iv_sun.setImageResource(R.drawable.days_sun_2);
																}
															}


														}
													}else{
														drug.setVisibility(View.GONE);
														dosage.setVisibility(View.GONE);
														drugdate.setVisibility(View.GONE);
														duration.setVisibility(View.GONE);
														daily.setVisibility(View.GONE);
														quantity.setVisibility(View.GONE);

													}
												}



											} catch (JSONException e) {
												// TODO Auto-generated catch block
												e.printStackTrace();
											}

										}



									}



								}
							}
						}
					}

				});
		
		
		

		return rowView;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub

		return obj1.size();

	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		
		return obj1.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return obj1.indexOf(getItem(position));
	}


}
