package com.wiinnova.doctorsdiary.supportclasses;

public class Quantitynameclass {
	
	String quantityname;
	boolean selected=false;
	
	
	public String getQuantityname() {
		return quantityname;
	}
	public void setQuantityname(String quantityname) {
		this.quantityname = quantityname;
	}
	public boolean isSelected() {
		return selected;
	}
	public void setSelected(boolean selected) {
		this.selected = selected;
	}

}
