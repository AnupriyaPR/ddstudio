package com.wiinnova.doctorsdiary.supportclasses;

import android.app.DialogFragment;
import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.wiinnova.doctorsdiary.R;

import java.util.ArrayList;


public class QuantitySelector_Popup extends DialogFragment
{ 
	ListView listItems;
	String[] spinnerItems;
	ArrayList<Quantitynameclass> selected=new ArrayList<Quantitynameclass>();
	ArrayList<Quantitynameclass> notselected=new ArrayList<Quantitynameclass>();
	CustomList customlist;
	String Name;
	TextView title;
	QuantityclassAdapter adapter;
	EditText inputSearch;
	onQuantitySubmitListener mListener;
	int positionval=0;
	private String[] selectedquantity;
	private Button submit;
	private String[] selectedquantitylist;
	private int selectedrow;
	String[] outputStrArritems;
	private ArrayList<Quantitynameclass> quantiyname;
	private ArrayList<Quantitynameclass> selectedquantiyname=new ArrayList<Quantitynameclass>();


	public interface onQuantitySubmitListener { 
		void onquantitySubmit(ArrayList<Quantitynameclass> outputStrArr, int selectedrow);  
	} 

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
		View view =inflater.inflate(R.layout.quantityselectorpopup,container, false);
		Bundle mArgs = getArguments();

		if( getArguments() != null){
			if(getArguments().containsKey("tagvalue") ){
				positionval=mArgs.getInt("tagvalue");

			}
		}

		quantiyname=(ArrayList<Quantitynameclass>)mArgs.getSerializable("quantiyname");

		selectedrow=mArgs.getInt("row");
		spinnerItems = mArgs.getStringArray("items");
		Name=mArgs.getString("name");
		selectedquantity=mArgs.getStringArray("selectedquantity");
		listItems=(ListView)view.findViewById(R.id.listView1);
		submit=(Button)view.findViewById(R.id.quantitysubmit);
		inputSearch = (EditText)view.findViewById(R.id.inputSearch);
		title=(TextView)view.findViewById(R.id.title);

		if(selectedquantity!=null){
			for(int i=0;i<selectedquantity.length;i++){
				Quantitynameclass quantitynameObj=new Quantitynameclass();
				quantitynameObj.setQuantityname(selectedquantity[i]);;
				selectedquantiyname.add(quantitynameObj);
			}

		}




		adapter=new QuantityclassAdapter(getActivity(),quantiyname,selectedquantiyname);
		title.setText(Name);

		listItems.setAdapter(adapter);



		inputSearch.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub

				selected.clear();
				notselected.clear();
				
				for(int i=0;i<quantiyname.size();i++){
					if(quantiyname.get(i).getQuantityname()!=null){
						if(quantiyname.get(i).getQuantityname().toLowerCase().contains(s.toString().toLowerCase())){
							selected.add(quantiyname.get(i));
						}else{
							notselected.add(quantiyname.get(i));
						}

					}
				}

				if(notselected.size()!=0){
					for(int j=0;j<notselected.size();j++){
						selected.add(notselected.get(j));
					}
				}

				ArrayList<Quantitynameclass> selectedItemsdisease = new ArrayList<Quantitynameclass>();
				selectedItemsdisease=adapter.getSelectedquantityitem();

				adapter=new QuantityclassAdapter(getActivity(),selected,selectedItemsdisease);
				listItems.setAdapter(adapter);
				adapter.notifyDataSetChanged();
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub.

			}
		});



		submit.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				ArrayList<Quantitynameclass> outputStrArr = new ArrayList<Quantitynameclass>();
				outputStrArr.clear();
				outputStrArr=adapter.getSelectedquantityitem();
				mListener.onquantitySubmit(outputStrArr,selectedrow);
				//mListener.onSubmit(customlist.getItem(position),positionval);
			}
		});

		return view;

	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		inputSearch.postDelayed(new Runnable() {
			@Override
			public void run() {
				InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(
						Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(inputSearch.getWindowToken(), 0);
			}
		}, 100);
	}


	public void setSubmitListener(onQuantitySubmitListener listener){
		this.mListener = listener;
	}


}
