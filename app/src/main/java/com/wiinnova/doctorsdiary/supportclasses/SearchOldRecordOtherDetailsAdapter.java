package com.wiinnova.doctorsdiary.supportclasses;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.wiinnova.doctorsdiary.R;
import com.parse.ParseObject;

import java.util.List;


public class SearchOldRecordOtherDetailsAdapter extends BaseAdapter{
	private LayoutInflater inflater;
	private int selected = -1;
	private Context context;
	List<ParseObject>  results;

	public SearchOldRecordOtherDetailsAdapter(Context context, List<ParseObject>  results) {
		// TODO Auto-generated constructor stub
		this.context = context;
		this.results = results;
		inflater = LayoutInflater.from(context);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return results.size();
	}

	@Override
	public 	ParseObject getItem(int position) {
		// TODO Auto-generated method stub
		return results.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	public void setSelected(int position){
		this.selected = position;
		SearchOldRecordOtherDetailsAdapter.this.notifyDataSetChanged();
	}

	public int getSelected(){
		return this.selected;
	}

	ViewHolder holder;

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		if (convertView == null){
			holder = new ViewHolder();
			convertView = inflater.inflate(R.layout.search_old_record_other_details_list, null); 
			holder.slNo = (TextView) convertView.findViewById(R.id.slno);
			holder.patientId = (TextView) convertView.findViewById(R.id.patientid);
			holder.Firstname = (TextView) convertView.findViewById(R.id.firstname);
			holder.Lastname = (TextView) convertView.findViewById(R.id.lastname);
			holder.DOB = (TextView) convertView.findViewById(R.id.dob);
			convertView.setTag(holder);
		}
		else
			holder = (ViewHolder) convertView.getTag();

		holder.slNo.setText(""+(position + 1));

		/*holder.name.setText(getItem(position).getFamilyChildName());
		holder.uid.setText(getItem(position).getStudentId());
		holder.dob.setText(getItem(position).getFamilyChildDob());*/
	
			holder.patientId.setText(getItem(position).getString("patientID"));
			holder.Firstname.setText(getItem(position).getString("firstName"));
			holder.Lastname.setText(getItem(position).getString("lastName"));
			holder.DOB.setText(getItem(position).getString("dob"));
		


		if (selected == position)
			convertView.setBackgroundColor(context.getResources().getColor(R.color.blue_color));
		else
			convertView.setBackgroundColor(Color.TRANSPARENT);

		return convertView;
	}

	static class ViewHolder{
		public TextView DOB;
		public TextView Lastname;
		public TextView Firstname;
		public TextView patientId;
		public TextView slNo;
		TextView item;
		TextView key;
	}

}


