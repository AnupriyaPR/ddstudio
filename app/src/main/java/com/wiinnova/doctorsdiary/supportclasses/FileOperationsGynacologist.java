package com.wiinnova.doctorsdiary.supportclasses;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.StringTokenizer;

import org.json.JSONException;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.List;
import com.itextpdf.text.ListItem;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.TextField;
import com.itextpdf.text.pdf.draw.VerticalPositionMark;
import com.itextpdf.text.pdf.parser.PdfReaderContentParser;
import com.itextpdf.text.pdf.parser.SimpleTextExtractionStrategy;
import com.itextpdf.text.pdf.parser.TextExtractionStrategy;
import com.parse.ParseObject;

public class FileOperationsGynacologist {






	Context context;
	static PdfWriter writer;
	Document document;
	Font font;
	Font textfont;


	public FileOperationsGynacologist() {
		// TODO Auto-generated constructor stub
	}

	public Boolean write(Context fragmentActivity, String fname, ParseObject combinedobject){
		try {
			context=fragmentActivity;
			String fpath = "/sdcard/" + fname + ".pdf";
			File file = new File(fpath);

			file.createNewFile();

			SharedPreferences sp1=context.getSharedPreferences("Login", 0);

			float header=sp1.getFloat("header", 0);
			float footer = sp1.getFloat("footer", 0);
			float left = sp1.getFloat("left", 0);
			float right = sp1.getFloat("right", 0);
			float title=sp1.getFloat("title", 0);
			float content=sp1.getFloat("content", 0);

			int templateposition=sp1.getInt("template",-1);



			font = new Font(FontFamily.HELVETICA, title, Font.BOLD, BaseColor.BLACK);
			textfont= new Font(FontFamily.HELVETICA, content , Font.NORMAL, BaseColor.BLACK);

			//document = new Document();

			System.out.println("header value"+header);
			System.out.println("right value"+right);
			System.out.println("left value"+left);
			System.out.println("footer value"+footer);



			if(templateposition==0){

				document=new Document(PageSize.A4,left, right, header, footer);
				writer =PdfWriter.getInstance(document,new FileOutputStream(file.getAbsoluteFile()));
				document.open();

				Chunk glue = new Chunk(new VerticalPositionMark());

				Paragraph name=new Paragraph();
				name.add(new Chunk("PATIENT NAME:"+" ",font));
				if(combinedobject.getString("patientname")!=null)
					name.add(new Chunk(combinedobject.getString("patientname").toString(),textfont));
				name.add(new Chunk(glue));

				name.add(new Chunk("DATE:"+" ",font));
				name.add(new Chunk(combinedobject.getString("createdAt"),textfont));
				document.add(name);


				Paragraph patientid=new Paragraph();

				patientid.add(new Chunk("PATIENT ID:"+" ",font));
				patientid.add(new Chunk(combinedobject.getString("id"),textfont));
				patientid.add(new Chunk(glue));

				patientid.add(new Chunk("Age:"+" ",font));
				if(combinedobject.getNumber("PatientAge")!=null)
					patientid.add(new Chunk(combinedobject.getNumber("PatientAge")+"",textfont));

				document.add(patientid);

				document.add( Chunk.NEWLINE );

				PdfPTable table = new PdfPTable(5);
				table.setWidthPercentage(100);


				if(combinedobject.getJSONArray("drug")!=null){
					for(int j=0;j<combinedobject.getJSONArray("drug").length();j++){


						System.out.println("Drug working..");

						try {
							PdfPCell emptycell=new PdfPCell(new Phrase("",textfont));
							emptycell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							emptycell.setBorder(Rectangle.NO_BORDER);

							PdfPCell drugcell=new PdfPCell(new Phrase(combinedobject.getJSONArray("drug").getString(j),textfont));
							drugcell.setHorizontalAlignment(Element.ALIGN_LEFT);
							drugcell.setBorder(Rectangle.NO_BORDER);
							System.out.println("drug name...."+combinedobject.getJSONArray("drug").getString(j));


							PdfPCell dosagecell=new PdfPCell(new Phrase(combinedobject.getJSONArray("dosage").getString(j),textfont));
							dosagecell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							dosagecell.setBorder(Rectangle.NO_BORDER);
							System.out.println("dosage name...."+combinedobject.getJSONArray("dosage").getString(j));

							PdfPCell drugduration;
							if(combinedobject.getJSONArray("duration").getString(j)!=null &&combinedobject.getJSONArray("duration").getString(j).length()!=0)
								drugduration=new PdfPCell(new Phrase(combinedobject.getJSONArray("duration").getString(j)+" "+"days",textfont));
							else
								drugduration=new PdfPCell(new Phrase(combinedobject.getJSONArray("duration").getString(j),textfont));

							drugduration.setHorizontalAlignment(Element.ALIGN_RIGHT);
							drugduration.setBorder(Rectangle.NO_BORDER);
							System.out.println("duration name...."+combinedobject.getJSONArray("duration").getString(j));


							//PdfPCell dayscell;
							PdfPCell quantitycell;

							table.addCell(emptycell);
							table.addCell(drugcell);
							table.addCell(dosagecell);



							if(combinedobject.getJSONArray("quantity").getString(j).endsWith(",")){
								String str=combinedobject.getJSONArray("quantity").getString(j).replaceAll("\\(.*?\\)","");

								if (str.endsWith(",")) {
									str = str.substring(0, str.length() - 1);

									quantitycell=new PdfPCell(new Phrase(str,textfont));
									quantitycell.setHorizontalAlignment(Element.ALIGN_RIGHT);
									quantitycell.setBorder(Rectangle.NO_BORDER);
									table.addCell(quantitycell);
								}
							}else{
								String str=combinedobject.getJSONArray("quantity").getString(j).replaceAll("\\(.*?\\)","");
								quantitycell=new PdfPCell(new Phrase(str,textfont));
								quantitycell.setHorizontalAlignment(Element.ALIGN_RIGHT);
								quantitycell.setBorder(Rectangle.NO_BORDER);
								table.addCell(quantitycell);

								//table.addCell(combinedobject.getJSONArray("quantity").getString(j));
							}

							System.out.println("quantity name...."+combinedobject.getJSONArray("quantity").getString(j));

							table.addCell(drugduration);







						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}

				document.add(table);
				document.add( Chunk.NEWLINE );


				Paragraph followup=new Paragraph();

				followup.add(new Chunk(glue));

				followup.add(new Chunk("Follow up:"+" ",font));
				if(combinedobject.getString("followup")!=null)
					followup.add(new Chunk(combinedobject.getString("followup"),textfont));
				document.add(followup);



				document.newPage();


				document.add( Chunk.NEWLINE );



				PdfPTable symptomstable = new PdfPTable(2);
				symptomstable.setWidthPercentage(100);


				System.out.println("MarriageHistoryYear"+combinedobject.getString("MarriageHistoryYear"));

				PdfPCell cellOne = new PdfPCell(new Phrase("ML-"+combinedobject.getString("MarriageHistoryYear"),textfont));
				PdfPCell cellTwo = new PdfPCell(new Phrase("SYMPTOMS",font));
				PdfPCell cellThree;
				PdfPCell cellFour;
				PdfPCell cellAllergy=new PdfPCell(new Phrase("ALLERGY HISTORY",font));
				PdfPCell cellAllergy1=new PdfPCell(new Phrase("DISEASE",font));

				PdfPCell cellAllergycolumn=new PdfPCell();
				PdfPCell cellAllergycolumn1;

				PdfPCell cellFive=new PdfPCell(new Phrase("MENSTRUAL HISTORY",font));
				PdfPCell cellSix=new PdfPCell(new Phrase("EXTERNAL GENETALIA",font));
				PdfPCell cellSeven=new PdfPCell();
				PdfPCell cellEight=new PdfPCell();
				PdfPCell cellNine=new PdfPCell(new Phrase("PRESENT AND PAST HISTORY",font));;
				PdfPCell cellTen=new PdfPCell(new Phrase("SECONDARY SEX",font));;
				PdfPCell cellEleven = new PdfPCell();
				PdfPCell cellTwelve=new PdfPCell();
				PdfPCell cellThirtien=new PdfPCell(new Phrase("FAMILY HISTORY",font));
				PdfPCell cellFourtien=new PdfPCell(new Phrase("BREAST",font));

				PdfPCell cellFiftien = new PdfPCell();
				PdfPCell cellSixtien = new PdfPCell();

				PdfPCell cellSeventien=new PdfPCell(new Phrase("SURGICAL HISTORY",font));
				PdfPCell cellEightien=new PdfPCell(new Phrase("P/S",font));

				PdfPCell cellNintien = new PdfPCell();
				PdfPCell cellTwenty = new PdfPCell();


				PdfPCell cellTwentyone=new PdfPCell(new Phrase("DRUG HISTORY",font));
				PdfPCell cellTwentytwo=new PdfPCell(new Phrase("P/V",font));

				PdfPCell cellTwentythree = new PdfPCell();
				PdfPCell cellTwentyfour = new PdfPCell();


				PdfPCell cellTwentyfive = new PdfPCell();
				PdfPCell cellTwentysix = new PdfPCell();


				cellTwentysix.setBorder(Rectangle.NO_BORDER);

				cellOne.setBorder(Rectangle.NO_BORDER);
				cellTwo.setBorder(Rectangle.NO_BORDER);
				cellFive.setBorder(Rectangle.NO_BORDER);
				cellSix.setBorder(Rectangle.NO_BORDER);
				cellNine.setBorder(Rectangle.NO_BORDER);
				cellTen.setBorder(Rectangle.NO_BORDER);
				cellThirtien.setBorder(Rectangle.NO_BORDER);
				cellFourtien.setBorder(Rectangle.NO_BORDER);
				cellSeventien.setBorder(Rectangle.NO_BORDER);
				cellEightien.setBorder(Rectangle.NO_BORDER);
				cellTwentyone.setBorder(Rectangle.NO_BORDER);
				cellTwentytwo.setBorder(Rectangle.NO_BORDER);

				cellAllergy.setBorder(Rectangle.NO_BORDER);
				cellAllergy1.setBorder(Rectangle.NO_BORDER);

				if(combinedobject.getString("Lmp_date")!=null){
					//combinedobject.getString("ObstricusHistory")
					cellThree = new PdfPCell(new Phrase("LMP"+" "+"-"+" "+combinedobject.getString("Lmp_date").replace(","," ")+"\n\n",textfont));

				}else{
					cellThree = new PdfPCell(new Phrase("LMP-"+"\n\n",textfont));

				}
				cellThree.setBorder(Rectangle.NO_BORDER);
				if(combinedobject.getString("symptoms")!=null){

					String symptomstring=method(combinedobject.getString("symptoms").trim());

					cellFour = new PdfPCell(new Phrase(symptomstring,textfont));

				}else{

					cellFour = new PdfPCell(new Phrase("Nil",textfont));

				}
				cellFour.setBorder(Rectangle.NO_BORDER);

				List allergylist=new List();
				allergylist.setListSymbol("");

				boolean allergyflag=false;



				if(combinedobject.getString("AllergyHistory")!=null){



					String[] allergynames=combinedobject.getString("AllergyHistory").split("&");

					boolean allergyhistorystatus=false;

					for(int j=0;j<allergynames.length;j++){
						String allergynameString="";

						if(!allergynames[j].equalsIgnoreCase("NA") && !allergynames[j].equalsIgnoreCase("Nil")){

							String[] allergyname=allergynames[j].split(",");

							for(int k=0;k<allergyname.length;k++){

								if(!allergyname[k].equalsIgnoreCase("Nil")){
									allergyflag=true;
									allergynameString+=allergyname[k]+" ";
								}
							}
							if(allergyflag==true){
								allergylist.add(new ListItem(allergynameString,textfont));
							}
						}
					}

					if(allergyflag==false){
						allergylist.add(new ListItem("Nil",textfont));
					}

				}else{

					allergylist.add(new ListItem("Nil",textfont));
				}

				cellAllergycolumn.addElement(allergylist);
				cellAllergycolumn.setBorder(Rectangle.NO_BORDER);




				if(combinedobject.getString("suspectedDisease")!=null){

					String disease=method(combinedobject.getString("suspectedDisease"));
					cellAllergycolumn1= new PdfPCell(new Phrase(disease,textfont));
				}else{
					cellAllergycolumn1= new PdfPCell(new Phrase("Nil",textfont));

				}





				cellAllergycolumn1.setBorder(Rectangle.NO_BORDER);





				List list = new List();

				list.setListSymbol("");

				boolean menstrualflag=false;

				if(combinedobject.getString("Menstrual_Regular_Irregular")!=null){
					list.add(new ListItem(combinedobject.getString("Menstrual_Regular_Irregular"),textfont));
					menstrualflag=true;
				}
				/*else
				list.add(new ListItem("Regular/Irregular",textfont));*/

				if(combinedobject.getString("Menstrual_Cycle")!=null){
					list.add(new ListItem("Cycle"+" "+"-"+" "+combinedobject.getString("Menstrual_Cycle"),textfont));
					menstrualflag=true;
				}
				/*else
				list.add(new ListItem("Cycle",textfont));*/

				if(combinedobject.getString("Menstrual_Days")!=null){
					list.add(new ListItem("Days"+" "+"-"+" "+combinedobject.getString("Menstrual_Days"),textfont));
					menstrualflag=true;
				}
				/*else
				list.add(new ListItem("Days",textfont));*/

				if(combinedobject.getString("Lmp_flow")!=null){
					list.add(new ListItem("Flow"+" "+"-"+" "+combinedobject.getString("Lmp_flow").replace(","," "),textfont));
					menstrualflag=true;
				}
				/*else
				list.add(new ListItem("Flow",textfont));*/
				if(combinedobject.getString("Lmp_dysmenorrhea")!=null){
					list.add(new ListItem("Dysmenorrhea"+" "+"-"+" "+combinedobject.getString("Lmp_dysmenorrhea").replace(",",""),textfont));
					menstrualflag=true;
				}
				/*else
				list.add(new ListItem("Dysmenorrhea",textfont));*/

				if(menstrualflag==false)
					list.add(new ListItem("Nil",textfont));

				cellSeven.addElement(list);
				cellSeven.setBorder(Rectangle.NO_BORDER);

				if(combinedobject.getString("externalgenetalia")!=null){

					String genetalia[]=combinedobject.getString("externalgenetalia").split(",");
					String genetalia_external="";

					if(genetalia[0].equalsIgnoreCase("Nil")){
						genetalia_external+=genetalia[0];

						if(genetalia[1].equalsIgnoreCase("Nil")){
							genetalia_external+=","+genetalia[1];
						}
						cellEight = new PdfPCell(new Phrase(genetalia_external,textfont));
					}else{

						cellEight = new PdfPCell(new Phrase("Nil",textfont));
					}
				}else{
					cellEight = new PdfPCell(new Phrase("Nil",textfont));
				}
				cellEight.setBorder(Rectangle.NO_BORDER);

				List presentandpastlist = new List();
				presentandpastlist.setListSymbol("");

				if(combinedobject.getString("majorIllness")!=null && combinedobject.getString("majorIllnessStatus")!=null){


					boolean illnessnil=false;

					String majorillness[]=combinedobject.getString("majorIllness").split(",");
					String majorillnessstatus[]=combinedobject.getString("majorIllnessStatus").split(",");

					for(int k=0;k<majorillness.length;k++){

						if(!majorillnessstatus[k].equalsIgnoreCase("NA")) {



							String illnessstatus[]=majorillnessstatus[k].split("&");
							String illnessstatusname="";
							if(!illnessstatus[0].equalsIgnoreCase("Nil") && !illnessstatus[0].equalsIgnoreCase("NA")){

								illnessnil=true;

								illnessstatusname+=illnessstatus[0];
								if(!illnessstatus[1].equalsIgnoreCase("Nil")){
									illnessstatusname+=","+illnessstatus[1];
								}
								presentandpastlist.add(new ListItem(majorillness[k]+" "+"-"+" "+illnessstatusname,textfont));
							}


						}

					}
					if(illnessnil==false){

						presentandpastlist.add(new ListItem("Nil",textfont));
					}

				}else{

					presentandpastlist.add(new ListItem("Nil",textfont));
				}
				cellEleven.addElement(presentandpastlist);
				cellEleven.setBorder(Rectangle.NO_BORDER);


				List secondarysex=new List();

				boolean secondary=false;
				secondarysex.setListSymbol("");

				if(combinedobject.getString("secondarysex_welldeveloped")!=null){

					String secondarywell[]=combinedobject.getString("secondarysex_welldeveloped").split(",");
					String secondarywelldeveloped="";
					if(!secondarywell[0].equalsIgnoreCase("Nil")){
						secondarywelldeveloped+=secondarywell[0];
						if(!secondarywell[1].equalsIgnoreCase("Nil")){
							secondarywelldeveloped+=","+secondarywell[1];
						}
						secondarysex.add(new ListItem("Well Developed"+" "+"-"+" "+secondarywelldeveloped,textfont));
						secondary=true;
					}
				}


				if(combinedobject.getString("secondarysex_hair")!=null){

					String secondaryhair[]=combinedobject.getString("secondarysex_hair").split(",");
					String secondarysexhair="";
					if(!secondaryhair[0].equalsIgnoreCase("Nil")){
						secondarysexhair+=secondaryhair[0];
						if(!secondaryhair[1].equalsIgnoreCase("Nil")){
							secondarysexhair+=","+secondaryhair[1];
						}
						secondarysex.add(new ListItem("Hair"+" "+"-"+" "+secondarysexhair,textfont));
						secondary=true;
					}
				}


				if(combinedobject.getString("secondarysex_acne")!=null){

					String secondaryacne[]=combinedobject.getString("secondarysex_acne").split(",");
					String secondarysexacne="";
					if(!secondaryacne[0].equalsIgnoreCase("Nil")){
						secondarysexacne+=secondaryacne[0];
						if(!secondaryacne[1].equalsIgnoreCase("Nil")){
							secondarysexacne+=","+secondaryacne[1];
						}
						secondarysex.add(new ListItem("Acne"+" "+"-"+" "+secondarysexacne,textfont));
						secondary=true;
					}
				}

				if(combinedobject.getString("secondarysex_other")!=null){


					String secondaryother[]=combinedobject.getString("secondarysex_other").split(",");
					String secondarysexother="";
					if(!secondaryother[0].equalsIgnoreCase("Nil")){
						secondarysexother+=secondaryother[0];
						if(!secondaryother[1].equalsIgnoreCase("Nil")){
							secondarysexother+=","+secondaryother[1];
						}
						secondarysex.add(new ListItem("Other"+" "+"-"+" "+secondarysexother,textfont));
						secondary=true;
					}
				}

				if(secondary==false){
					secondarysex.add(new ListItem("Nil",textfont));
				}

				cellTwelve.addElement(secondarysex);
				cellTwelve.setBorder(Rectangle.NO_BORDER);




				List familyhistory=new List();

				familyhistory.setListSymbol("");

				if(combinedobject.getString("majorIllnessFamily")!=null&&combinedobject.getString("majorIllnessFamilyStatus")!=null){

					String[] family={"Father","Mother","Sibling","Grandfather","Grandmother"};

					String[] familyillness=combinedobject.getString("majorIllnessFamilyStatus").split(";");

					boolean familyhistorystatusvalue=false;
					boolean familyhist=false;

					for(int j=0;j<5;j++){
						boolean familyhistorystatus=false;
						String familyillnessString="";

						if(!familyillness[j].equalsIgnoreCase("NA")){

							familyhistorystatusvalue=true;

							String[] illness=familyillness[j].split(",");

							for(int k=0;k<illness.length;k++){

								if(!illness[k].equalsIgnoreCase("Nil")){


									familyhistorystatus=true;
									familyhist=true;

									familyillnessString+=illness[k]+" ";


								}
							}
							if(familyhistorystatus==true){
								familyhistory.add(new ListItem(family[j]+" "+"-"+" "+familyillnessString,textfont));
							}
						}
					}

					if(familyhistorystatusvalue==false){
						familyhistory.add(new ListItem("Nil",textfont));
					}
					if(familyhist==false){
						familyhistory.add(new ListItem("Nil",textfont));
					}

				}else{

					familyhistory.add(new ListItem("Nil",textfont));
				}


				cellFiftien.addElement(familyhistory);
				cellFiftien.setBorder(Rectangle.NO_BORDER);

				List breasthistory=new List();

				boolean breastflag=false;
				breasthistory.setListSymbol("");

				if(combinedobject.getString("breast_lump")!=null){

					String breast[]=combinedobject.getString("breast_lump").split(",");
					String breaststring="";
					if(!breast[0].equalsIgnoreCase("Nil")){
						breaststring+=breast[0];
						if(!breast[1].equalsIgnoreCase("Nil")){
							breaststring+=","+breast[1];
						}
						breasthistory.add(new ListItem("Lump"+" "+"-"+" "+breaststring,textfont));
						breastflag=true;
					}
				}


				if(combinedobject.getString("breast_galactorrhea")!=null){

					String breast[]=combinedobject.getString("breast_galactorrhea").split(",");
					String breaststring="";
					if(!breast[0].equalsIgnoreCase("Nil")){
						breaststring+=breast[0];
						if(!breast[1].equalsIgnoreCase("Nil")){
							breaststring+=","+breast[1];
						}
						breasthistory.add(new ListItem("Galactorrhea"+" "+"-"+" "+breaststring,textfont));
						breastflag=true;
					}

				}



				if(combinedobject.getString("breast_other")!=null)
				{

					String breast[]=combinedobject.getString("breast_other").split(",");
					String breaststring="";
					if(!breast[0].equalsIgnoreCase("Nil")){
						breaststring+=breast[0];
						if(!breast[1].equalsIgnoreCase("Nil")){
							breaststring+=","+breast[1];
						}
						breasthistory.add(new ListItem("Other"+" "+"-"+" "+breaststring,textfont));
						breastflag=true;
					}

				}


				if(breastflag==false){
					breasthistory.add(new ListItem("Nil",textfont));
				}



				cellSixtien.addElement(breasthistory);
				cellSixtien.setBorder(Rectangle.NO_BORDER);


				List surgicalhistory=new List();

				surgicalhistory.setListSymbol("");


				if(combinedobject.getString("Surgeries")!=null){

					boolean Surgeryflag=false;
					String[] surgery=combinedobject.getString("Surgeries").split(",");

					for(int i=0;i<surgery.length;i++){
						if(!surgery[i].equalsIgnoreCase("Nil")&&!surgery[i].equalsIgnoreCase("NA")){
							Surgeryflag=true;
							surgicalhistory.add(new ListItem(surgery[i],textfont));
						}
					}

					if(Surgeryflag==false){
						surgicalhistory.add(new ListItem("Nil",textfont));
					}

					cellNintien.addElement(surgicalhistory);

				}else{

					surgicalhistory.add(new ListItem("Nil",textfont));
					cellNintien.addElement(surgicalhistory);

				}
				cellNintien.setBorder(Rectangle.NO_BORDER);

				List speculum=new List();

				speculum.setListSymbol("");

				boolean cervixflag=false;

				if(combinedobject.getString("cervix_healthy")!=null){

					String cervix[]=combinedobject.getString("cervix_healthy").split(",");
					String cervixstring="";

					if(!cervix[0].equalsIgnoreCase("Nil")){
						cervixstring+=cervix[0];
						if(!cervix[1].equalsIgnoreCase("Nil")){

							cervixstring+=","+cervix[1];

						}
						speculum.add(new ListItem("Healthy"+" "+"-"+" "+cervixstring,textfont));
						cervixflag=true;
					}
				}


				if(combinedobject.getString("cervix_bleeding")!=null){

					String cervix[]=combinedobject.getString("cervix_bleeding").split(",");
					String cervixstring="";

					if(!cervix[0].equalsIgnoreCase("Nil")){
						cervixstring+=cervix[0];
						if(!cervix[1].equalsIgnoreCase("Nil")){

							cervixstring+=","+cervix[1];

						}
						speculum.add(new ListItem("Bleeding"+" "+"-"+" "+cervixstring,textfont));
						cervixflag=true;
					}

				}



				if(combinedobject.getString("cervix_lbc")!=null){

					String cervix[]=combinedobject.getString("cervix_lbc").split(",");
					String cervixstring="";

					if(!cervix[0].equalsIgnoreCase("Nil")){
						cervixstring+=cervix[0];
						if(!cervix[1].equalsIgnoreCase("Nil")){
							cervixstring+=","+cervix[1];
						}
						speculum.add(new ListItem("LBC"+" "+"-"+" "+cervixstring,textfont));
						cervixflag=true;
					}
				}
				if(cervixflag==false){
					speculum.add(new ListItem("Nil",textfont));
				}


				cellTwenty.addElement(speculum);
				cellTwenty.setBorder(Rectangle.NO_BORDER);

				List medication=new List();

				medication.setListSymbol("");

				if(combinedobject.getString("allergiesMedication")!=null){
					String[] medi=combinedobject.getString("allergiesMedication").split(",");
					String[] reac=combinedobject.getString("allergiesReaction").split(",");

					boolean medicationflag=false;

					for(int i=0;i<medi.length;i++){

						String reaction="";

						if(!medi[i].equalsIgnoreCase("NA") && !medi[i].equalsIgnoreCase("Nil")){

							medicationflag=true;

							if(!reac[i].equalsIgnoreCase("NA") && !reac[i].equalsIgnoreCase("Nil")){
								reaction+=","+reac[i];
							}
							medication.add(new ListItem(medi[i]+"--"+reaction,textfont));
						}

					}

					if(medicationflag==false){
						medication.add(new ListItem("Nil",textfont));
					}

					cellTwentythree.addElement(medication);

				}else{

					medication.add(new ListItem("Nil",textfont));
					cellTwentythree.addElement(medication);

				}
				cellTwentythree.setBorder(Rectangle.NO_BORDER);





				List uterus=new List();

				uterus.setListSymbol("");

				boolean uterusflag=false;

				if(combinedobject.getString("uterus_avaf")!=null){
					if(combinedobject.getString("uterus_avaf").length()!=0){

						String uterusname=combinedobject.getString("uterus_avaf");
						String uterusstring="";

						if(!uterusname.equalsIgnoreCase("Nil")){
							uterusstring=combinedobject.getString("uterus_avaf");

							uterus.add(new ListItem("AVAF"+" "+"-"+" "+uterusstring,textfont));
							uterusflag=true;
						}
					}
				}



				if(combinedobject.getString("uterus_rvrf")!=null){

					if(combinedobject.getString("uterus_rvrf").length()!=0){
						String uterusname=combinedobject.getString("uterus_rvrf");
						String uterusstring="";

						if(!uterusname.equalsIgnoreCase("Nil")){
							uterusstring=combinedobject.getString("uterus_rvrf");

							uterus.add(new ListItem("RVRF"+" "+"-"+" "+uterusstring,textfont));
							uterusflag=true;
						}
					}

				}



				if(combinedobject.getString("uterus_others")!=null){
					if(combinedobject.getString("uterus_others").length()!=0){

						String uterusname=combinedobject.getString("uterus_others");
						String uterusstring="";

						if(!uterusname.equalsIgnoreCase("Nil")){
							uterusstring=combinedobject.getString("uterus_others");

							uterus.add(new ListItem("Others"+" "+"-"+" "+uterusstring,textfont));
							uterusflag=true;
						}
					}
				}


				if(uterusflag==false){
					uterus.add(new ListItem("Nil",textfont));
				}




				cellTwentyfour.addElement(uterus);
				cellTwentyfour.setBorder(Rectangle.NO_BORDER);


				List vitals=new List();

				vitals.setListSymbol("");

				if(combinedobject.getString("weight")!=null && combinedobject.getString("weight")!="Nil"){

					vitals.add(new ListItem("Weight"+" "+"-"+" "+combinedobject.getString("weight")+" "+"kg",textfont));

				}


				if(combinedobject.getString("bloodpressure")!=null && combinedobject.getString("bloodpressure")!="Nil"){

					vitals.add(new ListItem("BloodPressure"+" "+"-"+" "+combinedobject.getString("bloodpressure")+" "+"Hg mm",textfont));

				}

				if(combinedobject.getString("pulse")!=null && combinedobject.getString("pulse")!="Nil"){

					vitals.add(new ListItem("Pulse"+" "+"-"+" "+combinedobject.getString("pulse")+" "+"beats/min",textfont));

				}

				if(combinedobject.getString("respiratoryrate")!=null && combinedobject.getString("respiratoryrate")!="Nil"){

					vitals.add(new ListItem("Respiratoryrate"+" "+"-"+" "+combinedobject.getString("respiratoryrate")+" "+"breaths/min",textfont));

				}
				if(combinedobject.getString("spo2")!=null && combinedobject.getString("spo2")!="Nil"){

					vitals.add(new ListItem("SPO2"+" "+"-"+" "+combinedobject.getString("spo2")+" "+"%",textfont));

				}
				if(combinedobject.getString("temperature")!=null && combinedobject.getString("temperature")!="Nil"){

					vitals.add(new ListItem("Temperature"+" "+"-"+" "+combinedobject.getString("temperature")+" "+"c",textfont));

				}

				cellTwentyfive.addElement(vitals);
				cellTwentyfive.setBorder(Rectangle.NO_BORDER);



				symptomstable.addCell(cellOne);
				symptomstable.addCell(cellTwo);
				symptomstable.addCell(cellThree);
				symptomstable.addCell(cellFour);
				symptomstable.addCell(cellAllergy);
				symptomstable.addCell(cellAllergy1);
				symptomstable.addCell(cellAllergycolumn);
				symptomstable.addCell(cellAllergycolumn1);
				symptomstable.addCell(cellFive);
				symptomstable.addCell(cellSix);
				symptomstable.addCell(cellSeven);
				symptomstable.addCell(cellEight);
				symptomstable.addCell(cellNine);
				symptomstable.addCell(cellTen);
				symptomstable.addCell(cellEleven);
				symptomstable.addCell(cellTwelve);
				symptomstable.addCell(cellThirtien);
				symptomstable.addCell(cellFourtien);
				symptomstable.addCell(cellFiftien);
				symptomstable.addCell(cellSixtien);
				symptomstable.addCell(cellSeventien);
				symptomstable.addCell(cellEightien);
				symptomstable.addCell(cellNintien);
				symptomstable.addCell(cellTwenty);
				symptomstable.addCell(cellTwentyone);
				symptomstable.addCell(cellTwentytwo);
				symptomstable.addCell(cellTwentythree);
				symptomstable.addCell(cellTwentyfour);
				symptomstable.addCell(cellTwentyfive);
				symptomstable.addCell(cellTwentysix);

				document.add(symptomstable);

				System.out.println("document create");

				document.close();


			}

			else if(templateposition==1){

				document=new Document(PageSize.A4,left, right, header, footer);
				//document=new Document(PageSize.A4,30f, 120f, 180f, 90f);

				//document=new Document(new Rectangle(500f, 1200f),  30f, 30f, 100f, 50f);
				System.out.println("height doc....>"+document.getPageSize());

				// step 2

				writer =PdfWriter.getInstance(document,
						new FileOutputStream(file.getAbsoluteFile()));
				document.open();


				Chunk glue = new Chunk(new VerticalPositionMark());
				Paragraph name=new Paragraph();
				name.add(new Chunk("PATIENT NAME:"+" ",font));
				name.add(new Chunk(combinedobject.getString("patientname").toString(),textfont));
				name.add(new Chunk(glue));

				name.add(new Chunk("PATIENT ID:"+" ",font));
				name.add(new Chunk(combinedobject.getString("id"),textfont));
				document.add(name);

				Paragraph date=new Paragraph();

				date.add(new Chunk(glue));

				date.add(new Chunk("DATE:"+" ",font));
				date.add(new Chunk(combinedobject.getString("createdAt"),textfont));
				document.add(date);

				document.add( Chunk.NEWLINE );

				Paragraph vitals = new Paragraph("VITALS:",font);
				document.add(vitals);


				InputStream weightstream = context.getAssets().open("weight.png");
				Bitmap weightbmp = BitmapFactory.decodeStream(weightstream);
				ByteArrayOutputStream streamweight = new ByteArrayOutputStream();
				weightbmp.compress(Bitmap.CompressFormat.PNG, 100, streamweight);
				Image weightLogo = Image.getInstance(streamweight.toByteArray());

				weightLogo.scalePercent(25);


				InputStream pressureStream = context.getAssets().open("bloodpressure.png");
				Bitmap pressurebmp = BitmapFactory.decodeStream(pressureStream);
				ByteArrayOutputStream streampressure= new ByteArrayOutputStream();
				pressurebmp.compress(Bitmap.CompressFormat.PNG, 100, streampressure);
				Image pressureStreamLogo = Image.getInstance(streampressure.toByteArray());

				pressureStreamLogo.scalePercent(25);


				InputStream pulseStream = context.getAssets().open("pulse.png");
				Bitmap pulsebmp = BitmapFactory.decodeStream(pulseStream);
				ByteArrayOutputStream streampulse = new ByteArrayOutputStream();
				pulsebmp.compress(Bitmap.CompressFormat.PNG, 100, streampulse);
				Image pulseLogo = Image.getInstance(streampulse.toByteArray());

				pulseLogo.scalePercent(25);



				InputStream respiratoryrateinputStream = context.getAssets().open("respiratoryrate.png");
				Bitmap respiratoryratebmp = BitmapFactory.decodeStream(respiratoryrateinputStream);
				ByteArrayOutputStream streamrespiratoryrate = new ByteArrayOutputStream();
				respiratoryratebmp.compress(Bitmap.CompressFormat.PNG, 100, streamrespiratoryrate);
				Image respiratoryrateLogo = Image.getInstance(streamrespiratoryrate.toByteArray());

				respiratoryrateLogo.scalePercent(25);



				InputStream spo2inputStream = context.getAssets().open("spo2.png");
				Bitmap spo2bmp = BitmapFactory.decodeStream(spo2inputStream);
				ByteArrayOutputStream streamspo2 = new ByteArrayOutputStream();
				spo2bmp.compress(Bitmap.CompressFormat.PNG, 100, streamspo2);
				Image spo2Logo = Image.getInstance(streamspo2.toByteArray());

				spo2Logo.scalePercent(25);


				InputStream temperatureinputStream = context.getAssets().open("temperature.png");
				Bitmap temperaturebmp = BitmapFactory.decodeStream(temperatureinputStream);
				ByteArrayOutputStream streamtemperature = new ByteArrayOutputStream();
				temperaturebmp.compress(Bitmap.CompressFormat.PNG, 100, streamtemperature);
				Image temperatureLogo = Image.getInstance(streamtemperature.toByteArray());

				temperatureLogo.scalePercent(25);




				PdfPTable vitalstable = new PdfPTable(6);
				vitalstable.setWidthPercentage(100);
				PdfPCell cell = new PdfPCell();
				PdfPCell cell1 = new PdfPCell();
				PdfPCell cell2 = new PdfPCell();
				PdfPCell cell3 = new PdfPCell();
				PdfPCell cell4 = new PdfPCell();
				PdfPCell cell5 = new PdfPCell();

				PdfPCell cell6;
				PdfPCell cell7;
				PdfPCell cell8;
				PdfPCell cell9;
				PdfPCell cell10;
				PdfPCell cell11;



				cell.addElement(new Chunk(weightLogo, 0, 0,true));
				cell1.addElement(new Chunk(pressureStreamLogo, 0, 0,true));
				cell2.addElement(new Chunk(pulseLogo, 0, 0,true));
				cell3.addElement(new Chunk(respiratoryrateLogo, 0, 0,true));
				cell4.addElement(new Chunk(spo2Logo, 0, 0,true));
				cell5.addElement(new Chunk(temperatureLogo, 0, 0,true));

				cell.setBorder(Rectangle.NO_BORDER);
				cell1.setBorder(Rectangle.NO_BORDER);
				cell2.setBorder(Rectangle.NO_BORDER);
				cell3.setBorder(Rectangle.NO_BORDER);
				cell4.setBorder(Rectangle.NO_BORDER);
				cell5.setBorder(Rectangle.NO_BORDER);


				if(combinedobject.getString("weight")!=null && combinedobject.getString("weight")!="Nil"){
					cell6=new PdfPCell(new Phrase(combinedobject.getString("weight")+" "+"kg",textfont));
				}else{
					cell6=new PdfPCell(new Phrase("--"+" "+"kg",textfont));
				}

				if(combinedobject.getString("bloodPressure")!=null && combinedobject.getString("bloodPressure")!="Nil"){
					cell7=new PdfPCell(new Phrase(combinedobject.getString("bloodPressure")+" "+"Hg mm",textfont));
				}else{
					cell7=new PdfPCell(new Phrase("--"+" "+"Hg mm",textfont));
				}

				if(combinedobject.getString("pulse")!=null && combinedobject.getString("pulse")!="Nil"){
					cell8=new PdfPCell(new Phrase(combinedobject.getString("pulse")+" "+"beats/min",textfont));

				}else{
					cell8=new PdfPCell(new Phrase("--"+" "+"beats/min",textfont));
				}

				if(combinedobject.getString("respiratoryrate")!=null && combinedobject.getString("respiratoryrate")!="Nil"){
					cell9=new PdfPCell(new Phrase(combinedobject.getString("respiratoryrate")+" "+"breaths/min",textfont));

				}else{
					cell9=new PdfPCell(new Phrase("--"+" "+"breaths/min",textfont));

				}


				if(combinedobject.getString("spo2")!=null && combinedobject.getString("spo2")!="Nil"){
					cell10=new PdfPCell(new Phrase(combinedobject.getString("spo2")+" "+"%",textfont));
				}else{
					cell10=new PdfPCell(new Phrase("--"+" "+"%",textfont));

				}

				if(combinedobject.getString("temperature")!=null && combinedobject.getString("temperature")!="Nil"){
					cell11=new PdfPCell(new Phrase(combinedobject.getString("temperature")+" "+"c",textfont));

				}else{
					cell11=new PdfPCell(new Phrase("--"+" "+"c",textfont));

				}



				cell6.setBorder(Rectangle.NO_BORDER);
				cell7.setBorder(Rectangle.NO_BORDER);
				cell8.setBorder(Rectangle.NO_BORDER);
				cell9.setBorder(Rectangle.NO_BORDER);
				cell10.setBorder(Rectangle.NO_BORDER);
				cell11.setBorder(Rectangle.NO_BORDER);


				vitalstable.addCell(cell);
				vitalstable.addCell(cell1);
				vitalstable.addCell(cell2);
				vitalstable.addCell(cell3);
				vitalstable.addCell(cell4);
				vitalstable.addCell(cell5);


				vitalstable.addCell(cell6);
				vitalstable.addCell(cell7);
				vitalstable.addCell(cell8);
				vitalstable.addCell(cell9);
				vitalstable.addCell(cell10);
				vitalstable.addCell(cell11);

				document.add(vitalstable);

				document.add( Chunk.NEWLINE );



				PdfPTable symptomstable = new PdfPTable(2);
				symptomstable.setWidthPercentage(100);



				PdfPCell cellOne = new PdfPCell(new Phrase("SYMPTOMS",font));
				PdfPCell cellTwo = new PdfPCell(new Phrase("SYNDROMES",font));
				PdfPCell cellThree;
				PdfPCell cellFour;


				cellOne.setBorder(Rectangle.NO_BORDER);
				cellTwo.setBorder(Rectangle.NO_BORDER);

				if(combinedobject.getString("symptoms")!=null){
					cellThree = new PdfPCell(new Phrase(combinedobject.getString("symptoms"),textfont));

				}else{
					cellThree = new PdfPCell(new Phrase("Nil",textfont));

				}
				cellThree.setBorder(Rectangle.NO_BORDER);
				if(combinedobject.getString("syndromes")!=null){

					cellFour = new PdfPCell(new Phrase(combinedobject.getString("syndromes"),textfont));

				}else{

					cellFour = new PdfPCell(new Phrase("Nil",textfont));

				}
				cellFour.setBorder(Rectangle.NO_BORDER);

				symptomstable.addCell(cellOne);
				symptomstable.addCell(cellTwo);
				symptomstable.addCell(cellThree);
				symptomstable.addCell(cellFour);

				document.add(symptomstable);


				//document.add( Chunk.NEWLINE );


				PdfPTable diagnosistable = new PdfPTable(2);
				diagnosistable.setWidthPercentage(100);

				PdfPCell diagnosiscellOne = new PdfPCell(new Phrase("DISEASE",font));
				PdfPCell diagnosiscellTwo = new PdfPCell(new Phrase("ADDITIONAL COMMENTS",font));
				PdfPCell diagnosiscellThree;
				PdfPCell diagnosiscellFour;

				diagnosiscellOne.setBorder(Rectangle.NO_BORDER);
				diagnosiscellTwo.setBorder(Rectangle.NO_BORDER);
				if(combinedobject.getString("suspectedDisease")!=null){
					diagnosiscellThree= new PdfPCell(new Phrase(combinedobject.getString("suspectedDisease"),textfont));
				}else{
					diagnosiscellThree= new PdfPCell(new Phrase("Nil",textfont));

				}
				diagnosiscellThree.setBorder(Rectangle.NO_BORDER);
				if(combinedobject.getString("additionalComments")!=null){
					diagnosiscellFour=new PdfPCell(new Phrase(combinedobject.getString("additionalComments"),textfont));

				}else{
					diagnosiscellFour=new PdfPCell(new Phrase("Nil",textfont));
				}
				diagnosiscellFour.setBorder(Rectangle.NO_BORDER);

				diagnosistable.addCell(diagnosiscellOne);
				diagnosistable.addCell(diagnosiscellTwo);
				diagnosistable.addCell(diagnosiscellThree);
				diagnosistable.addCell(diagnosiscellFour);


				document.add(diagnosistable);

				document.add( Chunk.NEWLINE );

				PdfPTable table = new PdfPTable(4);
				table.setWidthPercentage(100);

				//PdfPCell cellkk;



				PdfPCell drug= new PdfPCell(new Phrase("DRUGS",font));
				drug.setHorizontalAlignment(Element.ALIGN_CENTER);
				//drug.setBorderWidth(.1f);
				drug.setBorder(Rectangle.NO_BORDER);


				PdfPCell dosage= new PdfPCell(new Phrase("DOSAGE",font));
				dosage.setHorizontalAlignment(Element.ALIGN_CENTER);
				//dosage.setBorderWidth(.1f);

				dosage.setBorder(Rectangle.NO_BORDER);


				PdfPCell frequency= new PdfPCell(new Phrase("FREQUENCY",font));
				frequency.setHorizontalAlignment(Element.ALIGN_CENTER);
				frequency.setBorder(Rectangle.NO_BORDER);

				PdfPCell duration= new PdfPCell(new Phrase("DURATION",font));;
				duration.setHorizontalAlignment(Element.ALIGN_CENTER);
				duration.setBorder(Rectangle.NO_BORDER);

				/*PdfPCell days= new PdfPCell(new Phrase("DAYS",font));;
				days.setHorizontalAlignment(Element.ALIGN_CENTER);
				days.setBorder(Rectangle.NO_BORDER);*/

				// we add the four remaining cells with addCell()

				table.addCell(drug);
				table.addCell(dosage);
				table.addCell(frequency);
				table.addCell(duration);
				/*table.addCell(days);*/




				if(combinedobject.getJSONArray("drug")!=null){
					for(int j=0;j<combinedobject.getJSONArray("drug").length();j++){


						try {





							PdfPCell drugcell=new PdfPCell(new Phrase(combinedobject.getJSONArray("drug").getString(j),textfont));
							drugcell.setHorizontalAlignment(Element.ALIGN_CENTER);
							drugcell.setBorder(Rectangle.NO_BORDER);
							System.out.println("drug name...."+combinedobject.getJSONArray("drug").getString(j));


							PdfPCell dosagecell=new PdfPCell(new Phrase(combinedobject.getJSONArray("dosage").getString(j),textfont));
							dosagecell.setHorizontalAlignment(Element.ALIGN_CENTER);
							dosagecell.setBorder(Rectangle.NO_BORDER);
							System.out.println("dosage name...."+combinedobject.getJSONArray("dosage").getString(j));


							PdfPCell drugduration=new PdfPCell(new Phrase(combinedobject.getJSONArray("duration").getString(j),textfont));
							drugduration.setHorizontalAlignment(Element.ALIGN_CENTER);
							drugduration.setBorder(Rectangle.NO_BORDER);
							System.out.println("duration name...."+combinedobject.getJSONArray("duration").getString(j));


							/*PdfPCell dayscell;*/
							PdfPCell quantitycell;


							table.addCell(drugcell);
							table.addCell(dosagecell);


							if(combinedobject.getJSONArray("quantity").getString(j).endsWith(",")){
								String str=combinedobject.getJSONArray("quantity").getString(j).replaceAll("\\(.*?\\)","");

								if (str.endsWith(",")) {
									str = str.substring(0, str.length() - 1);

									quantitycell=new PdfPCell(new Phrase(str,textfont));
									quantitycell.setHorizontalAlignment(Element.ALIGN_CENTER);
									quantitycell.setBorder(Rectangle.NO_BORDER);
									table.addCell(quantitycell);
								}
							}else{
								String str=combinedobject.getJSONArray("quantity").getString(j).replaceAll("\\(.*?\\)","");
								quantitycell=new PdfPCell(new Phrase(str,textfont));
								quantitycell.setHorizontalAlignment(Element.ALIGN_CENTER);
								quantitycell.setBorder(Rectangle.NO_BORDER);
								table.addCell(quantitycell);

								//table.addCell(combinedobject.getJSONArray("quantity").getString(j));
							}

							System.out.println("quantity name...."+combinedobject.getJSONArray("quantity").getString(j));




							table.addCell(drugduration);

							/*if(combinedobject.getJSONArray("days").getString(j).endsWith(",")){
								String str=combinedobject.getJSONArray("days").getString(j);
								if (str.endsWith(",")) {
									str = str.substring(0, str.length() - 1);

									dayscell=new PdfPCell(new Phrase(str,textfont));
									dayscell.setHorizontalAlignment(Element.ALIGN_CENTER);
									dayscell.setBorder(Rectangle.NO_BORDER);
									table.addCell(dayscell);
								}
							}else{
								dayscell=new PdfPCell(new Phrase(combinedobject.getJSONArray("days").getString(j),textfont));
								dayscell.setHorizontalAlignment(Element.ALIGN_CENTER);
								dayscell.setBorder(Rectangle.NO_BORDER);
								table.addCell(dayscell);
								//table.addCell(combinedobject.getJSONArray("days").getString(j));
							}

							System.out.println("days name...."+combinedobject.getJSONArray("days").getString(j));*/


						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}

				document.add(table);
				document.add( Chunk.NEWLINE );


				PdfPTable treatmenttable = new PdfPTable(2);
				treatmenttable.setWidthPercentage(100);



				PdfPCell treatmentcellOne = new PdfPCell(new Phrase("TREATMENT",font));
				PdfPCell treatmentcellTwo = new PdfPCell(new Phrase("FOLLOWUP",font));
				PdfPCell treatmentcellThree;
				PdfPCell treatmentcellFour;



				treatmentcellOne.setBorder(Rectangle.NO_BORDER);
				treatmentcellTwo.setBorder(Rectangle.NO_BORDER);

				if(combinedobject.getString("treatment")!=null){
					treatmentcellThree = new PdfPCell(new Phrase(combinedobject.getString("treatment"),textfont));

				}else{
					treatmentcellThree = new PdfPCell(new Phrase("Nil",textfont));

				}
				treatmentcellThree.setBorder(Rectangle.NO_BORDER);
				if(combinedobject.getString("followup")!=null){

					treatmentcellFour = new PdfPCell(new Phrase(combinedobject.getString("followup"),textfont));

				}else{
					treatmentcellFour = new PdfPCell(new Phrase("Nil",textfont));

				}
				treatmentcellFour.setBorder(Rectangle.NO_BORDER);

				treatmenttable.addCell(treatmentcellOne);
				treatmenttable.addCell(treatmentcellTwo);
				treatmenttable.addCell(treatmentcellThree);
				treatmenttable.addCell(treatmentcellFour);

				document.add(treatmenttable);

				document.close();
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}




		return true;



	}

	public String read(String fname) {
		BufferedReader br = null;
		String response = null;
		try {
			StringBuffer output = new StringBuffer();
			String fpath = "/sdcard/" + fname + ".pdf";

			PdfReader reader = new PdfReader(new FileInputStream(fpath));
			PdfReaderContentParser parser = new PdfReaderContentParser(reader);

			StringWriter strW = new StringWriter();

			TextExtractionStrategy strategy;
			for (int i = 1; i <= reader.getNumberOfPages(); i++) {
				strategy = parser.processContent(i,
						new SimpleTextExtractionStrategy());

				strW.write(strategy.getResultantText());

			}

			response = strW.toString();

		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
		return response;
	}

	private static void absText(String text, int x, int y,int size){

		try {
			PdfContentByte cb = writer.getDirectContent();
			BaseFont bf = BaseFont.createFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
			cb.saveState();
			cb.beginText();
			cb.moveText(x, y);
			cb.setFontAndSize(bf, size);
			cb.showText(text);
			cb.endText();




			cb.restoreState();



		} catch (DocumentException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}


	private static void absText1(String text, int x, int y,int size){

		try {
			PdfContentByte cb = writer.getDirectContent();
			BaseFont bf = BaseFont.createFont(BaseFont.TIMES_BOLD, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
			cb.saveState();
			cb.beginText();
			cb.moveText(x, y);
			cb.setFontAndSize(bf, size);
			cb.showText(text);
			cb.endText();



			cb.restoreState();


		} catch (DocumentException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}


	private static void absText2(String text, int x, int y,int size){

		try {
			PdfContentByte cb = writer.getDirectContent();
			BaseFont bf = BaseFont.createFont(BaseFont.HELVETICA_BOLD, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
			cb.saveState();
			cb.beginText();
			cb.moveText(x, y);
			cb.setFontAndSize(bf, size);
			cb.showText(text);
			cb.endText();



			cb.restoreState();



		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String addLinebreaks(String input, int maxLineLength) {
		StringTokenizer tok = new StringTokenizer(input, " ");
		StringBuilder output = new StringBuilder(input.length());
		int lineLen = 0;
		while (tok.hasMoreTokens()) {
			String word = tok.nextToken();

			if (lineLen + word.length() > maxLineLength) {
				output.append("\n");
				lineLen = 0;
			}
			output.append(word);
			lineLen += word.length();
		}
		return output.toString();
	}

	public String method(String str) {
		if (str != null && str.length() > 0 && str.charAt(str.length()-1)==',') {
			str = str.substring(0, str.length()-1);
		}
		return str;
	}

}
