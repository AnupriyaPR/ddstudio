package com.wiinnova.doctorsdiary.supportclasses;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.wiinnova.doctorsdiary.R;

import java.util.ArrayList;


public class CustomList extends ArrayAdapter<String>{
	private final Activity context;
	private final ArrayList<String> web;
	 private LayoutInflater inflater;

	public CustomList(Activity context,
			ArrayList<String> searcheditems) {
		super(context, R.layout.list_single, searcheditems);
		this.context = context;
		this.web = searcheditems;
		this.inflater=LayoutInflater.from(context);
		
		
	}
	ViewHolder holder;
	@Override
	public View getView(int position, View view, ViewGroup parent) {
		  holder = new ViewHolder();
		  view = inflater.inflate(R.layout.list_single,
                  null);
		//LayoutInflater inflater = context.getLayoutInflater();
		//View rowView= inflater.inflate(R.layout.list_single, null, true);
		holder.txtTitle = (TextView) view.findViewById(R.id.tvSpecialist);
		/*ImageView imageView = (ImageView) rowView.findViewById(R.id.img);*/
		holder.txtTitle.setText(web.get(position));
	/*	imageView.setImageResource(R.drawable.student);*/
		return view;
	}
	
	 static class ViewHolder {
		 TextView txtTitle;
	 }
	
}

