package com.wiinnova.doctorsdiary.supportclasses;

import android.app.Activity;
import android.content.Context;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.parse.ParseObject;
import com.wiinnova.doctorsdiary.R;
import com.wiinnova.doctorsdiary.fragment.CrashReporter;
import com.wiinnova.doctorsdiary.fragment.Treatment_Create_Frag;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
//
import static com.wiinnova.doctorsdiary.fragment.Treatment_Create_Frag.AlDateView;
import static com.wiinnova.doctorsdiary.fragment.Treatment_Create_Frag.AlDosage;
import static com.wiinnova.doctorsdiary.fragment.Treatment_Create_Frag.AlDrugs;
import static com.wiinnova.doctorsdiary.fragment.Treatment_Create_Frag.drugObj;
import static com.wiinnova.doctorsdiary.fragment.Treatment_Create_Frag.dosageObj;
import static com.wiinnova.doctorsdiary.fragment.Treatment_Create_Frag.drugdosage;
import static com.wiinnova.doctorsdiary.fragment.Treatment_Create_Frag.drugduration;
import static com.wiinnova.doctorsdiary.fragment.Treatment_Create_Frag.drugquantity;
import static com.wiinnova.doctorsdiary.fragment.Treatment_Create_Frag.drugstartdate;
import static com.wiinnova.doctorsdiary.fragment.Treatment_Create_Frag.durationObj;
import static com.wiinnova.doctorsdiary.fragment.Treatment_Create_Frag.map;

/**
 * Created by ubundu on 29/3/16.
 */
public class ArrayAdapterDrug extends BaseAdapter {
    ArrayList<Integer> listCount;
    Activity activity;
    private static LayoutInflater inflater = null;
    String[] diseasename;
    ViewHolder holder;
    Treatment_Create_Frag frag;
    List<ArrayList> array;
    int position;
    ArrayList<String> drugObj;
    ArrayList<View> arrayView;

    public ArrayAdapterDrug(Activity activity, ArrayList<Integer> listCount, Treatment_Create_Frag frag, List<ArrayList> array, ArrayList<String> drugObj) {
        this.activity = activity;
        this.listCount = listCount;
        this.frag = frag;
        this.array = array;
        map = new HashMap<Integer, View>();
        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.drugObj = drugObj;
        arrayView = new ArrayList<View>();
    }

    @Override
    public int getCount() {
        return listCount.size();
    }

    public class ViewHolder {
        LinearLayout days_container;
        AutoCompleteTextView drug;
        EditText dosage;
        TextView startdate;
        EditText duration;
        TextView quantity;
        TextView tvnumber;
        RelativeLayout ivRemove;
        LinearLayout lytMain;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View v = convertView;
        Log.d("ListCont", position + " hg");
//        this.position = listCount.get(positionGet);
//        if (v == null) {

//        this.position = positionGet;
//        if (map.get(position) == null) {

        if (arrayView.size() < position) {
            v = arrayView.get(position);
        } else
            v = null;
        if (v == null) {
            v = inflater.inflate(R.layout.add_more_drugs_new, null);
            holder = new ViewHolder();
            holder.days_container = (LinearLayout) v.findViewById(R.id.add_days_container);
            holder.drug = (AutoCompleteTextView) v.findViewById(R.id.etDrug);
            holder.dosage = (EditText) v.findViewById(R.id.etDosage);
            holder.startdate = (TextView) v.findViewById(R.id.etStartdate);
            holder.duration = (EditText) v.findViewById(R.id.etDuration);
            holder.quantity = (TextView) v.findViewById(R.id.srQuantity);
            holder.tvnumber = (TextView) v.findViewById(R.id.tvNumber);
            holder.ivRemove = (RelativeLayout) v.findViewById(R.id.rlImagecross);
            holder.lytMain = (LinearLayout) v.findViewById(R.id.llhidden_layout);
            holder.quantity.setTag(position);
            holder.startdate.setTag(position);
            holder.tvnumber.setText((position + 1) + "");


            if (position == 0) {
                holder.ivRemove.setVisibility(View.INVISIBLE);
            }

            try {
                holder.drug.setText(drugObj.get(position).toString());
                holder.dosage.setText(drugdosage.get(position).toString());
                holder.startdate.setText(drugstartdate.get(position).toString());
                holder.duration.setText(drugduration.get(position).toString());
                holder.quantity.setText(drugquantity.get(position).toString());
            } catch (IndexOutOfBoundsException e) {
                e.printStackTrace();
            }

            holder.duration.addTextChangedListener(new GenericTextWatcher(holder.duration, position));
            holder.dosage.addTextChangedListener(new GenericTextWatcher(holder.dosage, position));
            holder.startdate.addTextChangedListener(new GenericTextWatcher(holder.startdate, position));
            holder.drug.addTextChangedListener(new GenericTextWatcher(holder.drug, position));
            holder.quantity.addTextChangedListener(new GenericTextWatcher(holder.quantity, position));

            v.setTag(holder);

//        map.put(listCount.get(position), v);


            holder.ivRemove.setTag(position);
            holder.quantity.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    frag.setFrequency(Integer.parseInt(v.getTag().toString()));
                }
            });
            holder.ivRemove.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    arrayView.remove(position);
                    Log.d("ArrayViewSize", arrayView.size() + " d");
                    frag.removeRow(position, drugObj);
                }
            });
            holder.startdate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    frag.startDatepicker((Integer) v.getTag());
                }
            });
            arrayView.add(v);
        } else {
            v = arrayView.get(position);
        }

        return v;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }


//    @Override
//    public int getViewTypeCount() {
//
//        if (getCount() != 0)
//            return getCount();
//        else
//            return 1;
//    }
//
//    @Override
//    public int getItemViewType(int position) {
//        return position;
//    }


    public class GenericTextWatcher implements TextWatcher {
        View v;
        int position;

        public GenericTextWatcher(View v, int position) {
            this.v = v;
            this.position = position;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

            if (s.length() == 0) {
                frag.submitButtonDeactivation();
            } else if (start == 0 && before == 0 && count == 10) {
                frag.submitButtonActivation();
            }
        }

        @Override
        public void afterTextChanged(Editable s) {
            try {
                switch (v.getId()) {
                    case R.id.etDrug:
                        drugObj.add(position, s.toString());
                        break;
                    case R.id.etDosage:
                        dosageObj.put(position, s.toString());
                        break;
                    case R.id.etDuration:
                        durationObj.put(position, s.toString());
                        break;

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }


    }
}
