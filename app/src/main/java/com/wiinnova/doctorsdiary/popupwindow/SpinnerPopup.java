package com.wiinnova.doctorsdiary.popupwindow;

import android.app.DialogFragment;
import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.wiinnova.doctorsdiary.R;
import com.wiinnova.doctorsdiary.R.id;
import com.wiinnova.doctorsdiary.R.layout;
import com.wiinnova.doctorsdiary.supportclasses.CustomList;

import java.util.ArrayList;


public class SpinnerPopup extends DialogFragment
{ 
	ListView listItems;
	String[] spinnerItems;
	ArrayList<String> searcheditems=new ArrayList<String>();
	CustomList customlist;
	String Name;
	TextView title;

	EditText inputSearch;
	onSubmitListener mListener;
	int positionval=0;


	public interface onSubmitListener {  
		void onSubmit(String arg, int position);

		
	} 

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
		View view =inflater.inflate(R.layout.popup_fragment,container, false);
		Bundle mArgs = getArguments();

		if( getArguments() != null){
			if(getArguments().containsKey("tagvalue") ){
				positionval=mArgs.getInt("tagvalue");

			}
		}
		spinnerItems = mArgs.getStringArray("items");
		Name=mArgs.getString("name");
		//mListener = (onSubmitListener) mArgs.getSerializable("listener");
		listItems=(ListView)view.findViewById(R.id.listView1);
		inputSearch = (EditText)view.findViewById(R.id.inputSearch);
		title=(TextView)view.findViewById(R.id.title);
		title.setText(Name);

		if(Name.equals("Gender") || Name.equals("Marital Status") || Name.equals("Blood Group")){
			inputSearch.setVisibility(view.GONE);
		}

		for(int i=0;i<spinnerItems.length;i++){
			searcheditems.add(spinnerItems[i]);
		}

		customlist=new CustomList(getActivity(), searcheditems);
		listItems.setAdapter(customlist);

		inputSearch.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				searcheditems.clear();

				for(int i=0;i<spinnerItems.length;i++){
					if(spinnerItems[i].toLowerCase().contains(s.toString().toLowerCase())){
						System.out.println("spinner items"+spinnerItems[i]);
						searcheditems.add(spinnerItems[i]);
					}
				}
				customlist=new CustomList(getActivity(), searcheditems);
				listItems.setAdapter(customlist);
				//customlist.getFilter().filter(inputSearch.getText().toString());
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});

		listItems.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long arg3) {
				// TODO Auto-generated method stub
				mListener.onSubmit(customlist.getItem(position),positionval);
			}
		});
		return view;

	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		inputSearch.postDelayed(new Runnable() {
			@Override
			public void run() {
				InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(
						Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(inputSearch.getWindowToken(), 0);
			}
		}, 100);
	}


	public void setSubmitListener(onSubmitListener listener){
		this.mListener = listener;
	}
	

}
