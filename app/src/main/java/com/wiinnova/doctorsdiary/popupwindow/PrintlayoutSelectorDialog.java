package com.wiinnova.doctorsdiary.popupwindow;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.LinearLayout;

import com.wiinnova.doctorsdiary.R;
import com.wiinnova.doctorsdiary.activities.FragmentActivityContainer;

public class PrintlayoutSelectorDialog extends DialogFragment {


	GridView gridcontainer;

	SharedPreferences sp;


	public Integer[] mThumbIds = {

			R.layout.printlayout_item,	
			R.layout.printlayout_general
	};

	private String fromvalue;


	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		// TODO Auto-generated method stub

		AlertDialog.Builder b=new AlertDialog.Builder(getActivity());

		LayoutInflater i = getActivity().getLayoutInflater();
		View v = i.inflate(R.layout.popup_printlayout,null);

		sp=getActivity().getSharedPreferences("Login", 0);

		Bundle mArgs = getArguments();
		fromvalue=mArgs.getString("from");

		gridcontainer=(GridView)v.findViewById(R.id.layoutgrid);

		gridcontainer.setAdapter(new ImageAdapter(getActivity(), mThumbIds));

		gridcontainer.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long arg3) {
				// TODO Auto-generated method stub

				SharedPreferences.Editor Ed=sp.edit();
				Ed.putInt("template",position);  
				Ed.commit();


				if(fromvalue.equalsIgnoreCase("prescription")){

					if(((FragmentActivityContainer)getActivity()).gettreatmentcreatefrag()!=null){

						if(((FragmentActivityContainer)getActivity()).gettreatmentcreatefrag().isVisible()){

							((FragmentActivityContainer)getActivity()).gettreatmentcreatefrag().onPrescription();

						}else if(((FragmentActivityContainer)getActivity()).gettreatmentviewcreatefrag()!=null)	{

							if(((FragmentActivityContainer)getActivity()).gettreatmentviewcreatefrag().isVisible())
							{
								((FragmentActivityContainer)getActivity()).gettreatmentviewcreatefrag().onviewPrescription();
							}					
						}

					}else if(((FragmentActivityContainer)getActivity()).gettreatmentviewcreatefrag()!=null){

						if(((FragmentActivityContainer)getActivity()).gettreatmentviewcreatefrag().isVisible())
						{
							((FragmentActivityContainer)getActivity()).gettreatmentviewcreatefrag().onviewPrescription();
						}		

					}

				}


				dismiss();


			}
		});


		b.setView(v);
		return b.create();


	}


}

class ImageAdapter extends BaseAdapter {
	private Context context;
	private final Integer[] mThumbIds;

	public ImageAdapter(Context context, Integer[] mThumbIds) {
		this.context = context;
		this.mThumbIds = mThumbIds;
	}

	public View getView(int position, View convertView, ViewGroup parent) {

		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		View gridView;

		if (convertView == null) {

			gridView = new View(context);

			// get layout from mobile.xml
			gridView = inflater.inflate(R.layout.printcontainer_layout, null);

			// set image based on selected text
			LinearLayout llrow = (LinearLayout) gridView
					.findViewById(R.id.llgridrowcontainer);


			View to_add = inflater.inflate(mThumbIds[position],
					llrow,false);
			llrow.addView(to_add);


		} else {
			gridView = (View) convertView;
		}

		return gridView;
	}

	@Override
	public int getCount() {
		return mThumbIds.length;
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

}
