package com.wiinnova.doctorsdiary.popupwindow;

import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ListView;

import com.wiinnova.doctorsdiary.R;
import com.parse.ParseObject;
import com.wiinnova.doctorsdiary.fragment.AdminPatients;
import com.wiinnova.doctorsdiary.supportclasses.CustomListName;

import java.util.ArrayList;
import java.util.List;

public class SearchPatientNamePopup extends DialogFragment{

	ArrayList<String> objects=new ArrayList<String>();
	ArrayList<String> patients=new ArrayList<String>();
	ArrayList<String> searcheditems=new ArrayList<String>();
	ListView listItems;
	EditText inputSearch;

	List<ParseObject> patient;
	CustomListName customlist;
	
	dismispopup mListener;
	/*public interface onnamesubmit{
		void onName(ParseObject obj);
	}
	*/
	public interface dismispopup{
		void popupdismiss(int arg);
	}
	@Override

	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
		View view =inflater.inflate(R.layout.popup_search_patientname,container, false);

		patient=((AdminPatients)getTargetFragment()).getpatientsobject();
		patients.clear();

		Bundle mArgs = getArguments();
		patients=mArgs.getStringArrayList("name");
		objects=mArgs.getStringArrayList("objectId");
		mListener=(dismispopup)mArgs.getSerializable("listener");
		listItems=(ListView)view.findViewById(R.id.listView1);
		inputSearch = (EditText)view.findViewById(R.id.inputSearch);

		customlist=new CustomListName(getActivity(), patient);
		listItems.setAdapter(customlist);

		/*inputSearch.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				searcheditems.clear();

				for(int i=0;i<patients.size();i++){
					if(patients.get(i).contains(s.toString().toLowerCase())){

						searcheditems.add(patients.get(i));
					}
				}
				customlist=new CustomList(getActivity(), searcheditems);
				listItems.setAdapter(customlist);
				//customlist.getFilter().filter(inputSearch.getText().toString());
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});*/
		listItems.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long arg3) {
				// TODO Auto-generated method stub
				//((Patient_Tab_Activity)getActivity()).geteditlistener().onName(customlist.getItem(position));
				
				mListener.popupdismiss(position);
			}
		});

		return view;
	}


}
