package com.wiinnova.doctorsdiary.popupwindow;


import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

import com.wiinnova.doctorsdiary.R;
import com.wiinnova.doctorsdiary.R.id;
import com.wiinnova.doctorsdiary.R.layout;
import com.parse.ParseObject;

public class OldPatientId_Popupfragment extends DialogFragment{

	
	EditText etPatientId;
	Button btnSubmit;
	onSubmitListener mListener; 
		String patientId;

	public interface onSubmitListener {  
		void onSubmit(String arg);  
	} 
	
	public interface patientid{
		void onName(ParseObject obj);
	}
	


	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		AlertDialog.Builder b=new AlertDialog.Builder(getActivity());
		
		LayoutInflater i = getActivity().getLayoutInflater();
		View v = i.inflate(R.layout.popup_oldpatient_id,null);

		etPatientId=(EditText)v.findViewById(R.id.etPatientid);

		btnSubmit=(Button)v.findViewById(R.id.btnSubmit);
		

		Bundle mArgs = getArguments();

		b.setView(v);
		
		etPatientId.setFilters(new InputFilter[] {new InputFilter.AllCaps()});

		btnSubmit.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub

				if(TextUtils.isEmpty(etPatientId.getText().toString())) {
					etPatientId.setError("Enter Patient Id ");
					return;
				}
				else
				{
					patientId=etPatientId.getText().toString();
					mListener.onSubmit(patientId);
				}

			}
		});

		return b.create();
	}
	
	public onSubmitListener getmListener() {
		return mListener;
	}



	public void setmListener(onSubmitListener mListener) {
		this.mListener = mListener;
	}






}
