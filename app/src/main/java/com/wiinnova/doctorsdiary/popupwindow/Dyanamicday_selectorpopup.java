package com.wiinnova.doctorsdiary.popupwindow;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.wiinnova.doctorsdiary.R;

import java.util.ArrayList;

public class Dyanamicday_selectorpopup  extends DialogFragment {

	ListView lvItems;
	Button submit;
	ArrayAdapter<String> adapter;
	String[] array = {"Monday", "Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday" };  
	onDyanamicDaysSelectListener mListener; 
	String val;
	ArrayList<String> selected;
	String[] selecteddays;
	EditText inputsearch;
	int selectedrow;


	public interface onDyanamicDaysSelectListener {  
		void onDyanamicDays(String[] outputStrArr, int selectedrow);
	} 

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		getDialog().setCanceledOnTouchOutside(false);
		return super.onCreateView(inflater, container, savedInstanceState);
	}
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		AlertDialog.Builder b=new AlertDialog.Builder(getActivity());

		LayoutInflater i = getActivity().getLayoutInflater();
		View v = i.inflate(R.layout.popup_suspecteddisease,null);

		Bundle mArgs = getArguments();
//		mListener = (onDyanamicDaysSelectListener) mArgs.getSerializable("listener");
		selectedrow=mArgs.getInt("row");
		selecteddays=mArgs.getStringArray("selecteddays");
		lvItems=(ListView)v.findViewById(R.id.ListView01);
		submit=(Button)v.findViewById(R.id.submit);
		inputsearch=(EditText)v.findViewById(R.id.inputSearch);

		adapter=new ArrayAdapter<String>(getActivity(),
				android.R.layout.simple_list_item_multiple_choice, array);
		lvItems.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);

		lvItems.setAdapter(adapter);

		inputsearch.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				Dyanamicday_selectorpopup.this.adapter.getFilter().filter(s);
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});	



		if(selecteddays!=null){

			for(int j=0;j<array.length;j++){
				for(int k=0;k<selecteddays.length;k++){
					if(array[j]==selecteddays[k]){
						lvItems.setItemChecked(j, true);
					}
				}
			}
		}

		b.setView(v);

		submit.setOnClickListener(new OnClickListener() {		
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				SparseBooleanArray checked = lvItems.getCheckedItemPositions();
				ArrayList<String> selectedItems = new ArrayList<String>();
				selectedItems.clear();
				for (int i = 0; i < checked.size(); i++) {
					// Item position in adapter
					int position = checked.keyAt(i);
					// Add sport if it is checked i.e.) == TRUE!
					if (checked.valueAt(i))
						selectedItems.add(adapter.getItem(position));
				}
				String[] outputStrArr = new String[selectedItems.size()];

				for (int i = 0; i < selectedItems.size(); i++) {
					outputStrArr[i] = selectedItems.get(i);
					System.out.println("selected"+outputStrArr[i]);
				}
				mListener.onDyanamicDays(outputStrArr,selectedrow);
			}
		});
		return b.create();
	}
	
	public void setDynamicDaysListener(onDyanamicDaysSelectListener listener){
		this.mListener = listener;
	}

}
