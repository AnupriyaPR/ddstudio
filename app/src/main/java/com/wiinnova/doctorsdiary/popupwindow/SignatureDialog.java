package com.wiinnova.doctorsdiary.popupwindow;


import android.app.DialogFragment;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.wiinnova.doctorsdiary.R;
import com.wiinnova.doctorsdiary.supportclasses.SignatureView;


public class SignatureDialog extends DialogFragment{

	private SignatureView signatureView;
	private Button ok_button;
	private Button clear_button;
	private OnSignListener listener;

	public interface OnSignListener {
		public void onSigned(Bitmap bitmap);
	}
	
	public SignatureDialog(OnSignListener listener) {
		// TODO Auto-generated constructor stub
		this.listener = listener;
	}
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.signature, null);
		
		getDialog().setTitle("Signature");
		signatureView = (SignatureView) view.findViewById(R.id.imageView_signature);
		ok_button = (Button) view.findViewById(R.id.ok_button);
		clear_button = (Button) view.findViewById(R.id.clear_button);
		
		
		ok_button.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (signatureView.getBitmap() != null){
					if (listener != null)
						listener.onSigned(signatureView.getBitmap());
				}
				getDialog().dismiss();
			}
		});
		
		clear_button.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				signatureView.clear();
			}
		});
		
		
		
		return view;
	}
}
