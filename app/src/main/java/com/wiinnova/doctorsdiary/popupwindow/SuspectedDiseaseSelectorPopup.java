package com.wiinnova.doctorsdiary.popupwindow;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.wiinnova.doctorsdiary.R;
import com.wiinnova.doctorsdiary.R.id;
import com.wiinnova.doctorsdiary.R.layout;
import com.wiinnova.doctorsdiary.supportclasses.DiseaseclassAdapter;
import com.wiinnova.doctorsdiary.supportclasses.Diseasenameclass;

import java.util.ArrayList;

public class SuspectedDiseaseSelectorPopup extends DialogFragment{

	ListView lvItems;
	Button submit;
	//ArrayAdapter<Diseasenameclass> adapter;
	String[] array = {"Arthritis", "Asthma", "Chlamydia","Flu","Heart Dieseas","Meningitis","Tuberculosis(TB)" };  
	onSubmitListenerSds mListener; 
	String val;
	ArrayList<String> selected;
	ArrayList<Diseasenameclass> selecteddisease=new ArrayList<Diseasenameclass>();
	EditText inputsearch;
	ArrayList<Diseasenameclass> diseasenameObj;
	ArrayList<Diseasenameclass> checkselecteddiseasenameObj=new ArrayList<Diseasenameclass>();
	ArrayList<Diseasenameclass> not_checkselecteddiseasenameObj=new ArrayList<Diseasenameclass>();
	DiseaseclassAdapter adapter;


	public interface onSubmitListenerSds {  
		void onSubmit(ArrayList<Diseasenameclass> selectedItems);  
	} 

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		getDialog().setCanceledOnTouchOutside(true);
		return super.onCreateView(inflater, container, savedInstanceState);
	}
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		AlertDialog.Builder b=new AlertDialog.Builder(getActivity());

		LayoutInflater i = getActivity().getLayoutInflater();
		View v = i.inflate(R.layout.popup_suspecteddisease,null);

		Bundle mArgs = getArguments();

		diseasenameObj=(ArrayList<Diseasenameclass>)mArgs.getSerializable("diseasenamearray");

		//mListener = (onSubmitListener) mArgs.getSerializable("listener");
		lvItems=(ListView)v.findViewById(R.id.ListView01);
		selecteddisease=(ArrayList<Diseasenameclass>)mArgs.getSerializable("selecteddisease");
		submit=(Button)v.findViewById(R.id.submit);
		inputsearch=(EditText)v.findViewById(R.id.inputSearch);
		System.out.println("diseasenameObj size"+diseasenameObj.size());


		adapter=new DiseaseclassAdapter(getActivity(),diseasenameObj,selecteddisease);
		lvItems.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
		lvItems.setAdapter(adapter);

		inputsearch.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				//	adapter.getFilter().filter(s);

				System.out.println("s value"+s+"@@@start"+start+"@@@before"+before+"@@@@count"+count);

				checkselecteddiseasenameObj.clear();
				not_checkselecteddiseasenameObj.clear();
				for(int i=0;i<diseasenameObj.size();i++){
					if(diseasenameObj.get(i).getDiseasename()!=null){
						if(diseasenameObj.get(i).getDiseasename().toLowerCase().contains(s.toString().toLowerCase())){
							checkselecteddiseasenameObj.add(diseasenameObj.get(i));
						}else{
							not_checkselecteddiseasenameObj.add(diseasenameObj.get(i));
						}
					}
				}
				if(not_checkselecteddiseasenameObj.size()!=0){
					for(int j=0;j<not_checkselecteddiseasenameObj.size();j++){
						checkselecteddiseasenameObj.add(not_checkselecteddiseasenameObj.get(j));
					}
				}

				ArrayList<Diseasenameclass> selectedItemsdisease = new ArrayList<Diseasenameclass>();
				//selectedItemsdisease.clear();
				selectedItemsdisease=adapter.getSelecteditem();

				adapter=new DiseaseclassAdapter(getActivity(),checkselecteddiseasenameObj,selectedItemsdisease);
				lvItems.setAdapter(adapter);
				adapter.notifyDataSetChanged();


			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});	



		/*if(selecteddisease!=null){

			for(int j=0;j<selected.size();j++){
				for(int k=0;k<selecteddisease.length;k++){
					if(selected.get(j)==selecteddisease[k]){
						lvItems.setItemChecked(j, true);
					}
				}
			}
		}*/

		b.setView(v);

		submit.setOnClickListener(new OnClickListener() {		
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				SparseBooleanArray checked = lvItems.getCheckedItemPositions();
				ArrayList<Diseasenameclass> selectedItems = new ArrayList<Diseasenameclass>();
				selectedItems.clear();
				selectedItems=adapter.getSelecteditem();
				mListener.onSubmit(selectedItems);
			}
		});
		return b.create();
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		inputsearch.postDelayed(new Runnable() {
			@Override
			public void run() {
				InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(
						Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(inputsearch.getWindowToken(), 0);
			}
		}, 100);

	}

	public void setSubmitListenerSds(onSubmitListenerSds listener){
		this.mListener = listener;
	}


}
