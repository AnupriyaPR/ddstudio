package com.wiinnova.doctorsdiary.popupwindow;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

import com.wiinnova.doctorsdiary.R;
import com.wiinnova.doctorsdiary.R.id;
import com.wiinnova.doctorsdiary.R.layout;

public class NewPatientId_Popupfragment extends DialogFragment{

	
	EditText etPatientId;
	Button btnSubmit;
	onSaveListener mListener; 
	

	String patientId;
	
	public interface onSaveListener {  
		void onSave(String arg);  
	} 

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		AlertDialog.Builder b=new AlertDialog.Builder(getActivity());

		LayoutInflater i = getActivity().getLayoutInflater();
		View v = i.inflate(R.layout.popup_newpatient_id,null);

		etPatientId=(EditText)v.findViewById(R.id.etPatientid);

		btnSubmit=(Button)v.findViewById(R.id.btnSubmit);

		Bundle mArgs = getArguments();
		//mListener = (onSaveListener) mArgs.getSerializable("listener");

		b.setView(v);
		
		etPatientId.setFilters(new InputFilter[] {new InputFilter.AllCaps()});
		
		btnSubmit.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub

				if(TextUtils.isEmpty(etPatientId.getText().toString())) {
					etPatientId.setError("Enter Patient Id ");
					return;
				}
				else
				{
					patientId=etPatientId.getText().toString();
					mListener.onSave(patientId);
				}

			}
		});

		return b.create();
	}
	public onSaveListener getmListener() {
		return mListener;
	}

	public void setmListener(onSaveListener mListener) {
		this.mListener = mListener;
	}


}
