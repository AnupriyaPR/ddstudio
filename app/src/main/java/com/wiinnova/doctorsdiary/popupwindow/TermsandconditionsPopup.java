package com.wiinnova.doctorsdiary.popupwindow;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.wiinnova.doctorsdiary.R;
import com.wiinnova.doctorsdiary.popupwindow.SignatureDialog.OnSignListener;
import com.wiinnova.doctorsdiary.supportclasses.ImageUtils;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class TermsandconditionsPopup extends DialogFragment
{
	SimpleDateFormat dateFormatter;
	DatePickerDialog signDatePickerDialog;
	Calendar myCalendar;
	private TextView date_picker_1;
	private Button Submitbtn;
	private ImageView ivSignature;
	private String SignatureImage;
	onTermsandcondition listener;
	
	private TextView tvhead5;
	private TextView tvhead6;
	
	
	String formattedDate;
	private TextView tvhead9;
	private TextView tvhead10;
	private TextView tvhead15;
	private TextView tvhead27;
	private TextView tvhead34;
	private TextView tvhead40;
	private TextView tvhead41;
	private TextView tvhead44;
	private TextView tvhead45;
	private TextView tvhead46;
	private TextView tvhead28;
	private TextView tvhead29;
	private TextView tvhead30;
	private TextView tvhead31;
	private TextView tvhead32;
	private TextView tvhead33;
	
	public interface onTermsandcondition{
		public void termsandcondition(String signature, String date);
	}

	public TermsandconditionsPopup(onTermsandcondition listener){
		this.listener=listener;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		getDialog().setCanceledOnTouchOutside(true);
		return super.onCreateView(inflater, container, savedInstanceState);
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		AlertDialog.Builder b=new AlertDialog.Builder(getActivity());
		LayoutInflater i=getActivity().getLayoutInflater();
		View v = i.inflate(R.layout.terms_conditions_popup,null);
		ivSignature=(ImageView)v.findViewById(R.id.parent_guardian_signature);
		Submitbtn=(Button)v.findViewById(R.id.btnsubmit);
		date_picker_1 = (TextView)v.findViewById(R.id.date_1_text);

		tvhead5=(TextView)v.findViewById(R.id.tvhead5);
		tvhead6=(TextView)v.findViewById(R.id.tvhead6);
		tvhead9=(TextView)v.findViewById(R.id.tvhead9);
		tvhead10=(TextView)v.findViewById(R.id.tvhead10);
		tvhead15=(TextView)v.findViewById(R.id.tvhead15);
		tvhead27=(TextView)v.findViewById(R.id.tvhead27);
		tvhead34=(TextView)v.findViewById(R.id.tvhead34);
		tvhead40=(TextView)v.findViewById(R.id.tvhead40);
		tvhead41=(TextView)v.findViewById(R.id.tvhead41);
		tvhead44=(TextView)v.findViewById(R.id.tvhead44);
		tvhead45=(TextView)v.findViewById(R.id.tvhead45);
		tvhead46=(TextView)v.findViewById(R.id.tvhead46);
		tvhead28=(TextView)v.findViewById(R.id.tvhead28);
		tvhead29=(TextView)v.findViewById(R.id.tvhead29);
		tvhead30=(TextView)v.findViewById(R.id.tvhead30);
		tvhead31=(TextView)v.findViewById(R.id.tvhead31);
		tvhead32=(TextView)v.findViewById(R.id.tvhead32);
		tvhead33=(TextView)v.findViewById(R.id.tvhead33);
		

		Calendar c = Calendar.getInstance();
		System.out.println("Current time => " + c.getTime());
		SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
		formattedDate = df.format(c.getTime());
		date_picker_1.setText(formattedDate);
		
		
		String term4=getActivity().getResources().getString(R.string.terms4);
		String term5=getActivity().getResources().getString(R.string.terms5);
		String term9=getActivity().getResources().getString(R.string.terms9);
		String term10=getActivity().getResources().getString(R.string.terms10);
		String term15=getActivity().getResources().getString(R.string.terms15);
		String term27=getActivity().getResources().getString(R.string.terms27);
		String term34=getActivity().getResources().getString(R.string.terms34);
		String term40=getActivity().getResources().getString(R.string.terms40);
		String term41=getActivity().getResources().getString(R.string.terms41);
		String term44=getActivity().getResources().getString(R.string.terms44);
		String term45=getActivity().getResources().getString(R.string.terms45);
		String term46=getActivity().getResources().getString(R.string.terms46);
		String term28=getActivity().getResources().getString(R.string.terms28);
		String term29=getActivity().getResources().getString(R.string.terms29);
		String term30=getActivity().getResources().getString(R.string.terms30);
		String term31=getActivity().getResources().getString(R.string.terms31);
		String term32=getActivity().getResources().getString(R.string.terms32);
		String term33=getActivity().getResources().getString(R.string.terms33);

		
	
		tvhead5.setText(Html.fromHtml("�<b>Information you give us</b>"+term4));
		tvhead6.setText(Html.fromHtml("�<b>Information we collect about you</b>"+term5));
		tvhead9.setText(Html.fromHtml("�<b>Information we receive from other sources</b>"+term9));
		tvhead10.setText(Html.fromHtml("�<b>Metadata</b>"+term10));
		tvhead15.setText(Html.fromHtml("�<b>Information you give to us</b>"+term15));
		tvhead27.setText(Html.fromHtml("�<b>Information we collect about you</b>"+term27));
		tvhead34.setText(Html.fromHtml("�<b>Information we receive from other sources</b>"+term34));
		tvhead40.setText(Html.fromHtml("�"+term40));
		tvhead41.setText(Html.fromHtml("�"+term41));
		tvhead44.setText(Html.fromHtml("�"+term44));
		tvhead45.setText(Html.fromHtml("�"+term45));
		tvhead46.setText(Html.fromHtml("�"+term46));
		tvhead28.setText(Html.fromHtml("�"+term28));
		tvhead29.setText(Html.fromHtml("�"+term29));
		tvhead30.setText(Html.fromHtml("�"+term30));
		tvhead31.setText(Html.fromHtml("�"+term31));
		tvhead32.setText(Html.fromHtml("�"+term32));
		tvhead33.setText(Html.fromHtml("�"+term33));
		  
		ivSignature.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				SignatureDialog dialog = new SignatureDialog(new OnSignListener() {

					@Override
					public void onSigned(Bitmap bitmap) {
						// TODO Auto-generated method stub

					
						
						ivSignature.setBackgroundResource(0);
						ivSignature.setImageBitmap(bitmap);
						SignatureImage = ImageUtils.encodeTobase64(bitmap);

					}
				});
				dialog.show(getFragmentManager(), "parent_guardian_signature");
			}
		} );


		Submitbtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				if(listener!=null){
					if(SignatureImage!=null && date_picker_1.getText().toString()!=null){
						listener.termsandcondition(SignatureImage,date_picker_1.getText().toString());
					}
				}
				getDialog().dismiss();

			}
		});

		b.setView(v);
		return b.create();
	}


}
