package com.wiinnova.doctorsdiary.popupwindow;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import com.wiinnova.doctorsdiary.R;
import com.wiinnova.doctorsdiary.supportclasses.DaysclassAdapter;

import java.util.ArrayList;

public class DaysSelector_Popup  extends DialogFragment{

	ListView lvItems;
	Button submit;
	ArrayAdapter<String> adapter;
	String[] array = {"Daily","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday" };  
	onDaysSelectListener mListener; 
	String val;
	ArrayList<String> selected;
	String[] selecteddays;
/*	EditText inputsearch;
*/	int selectedrow;

	//DaysclassAdapter daysAdapter;
	

	public interface onDaysSelectListener {  
		void onDays(String[] outputStrArr, int selectedrow);
	} 

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		getDialog().setCanceledOnTouchOutside(true);
		return super.onCreateView(inflater, container, savedInstanceState);
	}
	
	
	@Override
	public void onActivityCreated(Bundle arg0) {
		// TODO Auto-generated method stub
		super.onActivityCreated(arg0);
		System.out.println("activity created.....");
	}
	
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		AlertDialog.Builder b=new AlertDialog.Builder(getActivity());

		LayoutInflater i = getActivity().getLayoutInflater();
		View v = i.inflate(R.layout.popup_selecteddays,null);

		Bundle mArgs = getArguments();
		selectedrow=mArgs.getInt("row");
		selecteddays=mArgs.getStringArray("selecteddays");
		
		System.out.println("selecteddays"+selecteddays+"array"+array);

		System.out.println("getActivity()"+getActivity());
		
		
		lvItems=(ListView)v.findViewById(R.id.ListView01);
		submit=(Button)v.findViewById(R.id.submit);
		final DaysclassAdapter daysAdapter=new DaysclassAdapter(getActivity(),array,selecteddays);
		lvItems.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
		lvItems.setAdapter(daysAdapter);


		b.setView(v);

		submit.setOnClickListener(new OnClickListener() {		
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
			
				
				String[] outputStrArr = new String[daysAdapter.getSelecteditem().size()];
				for(int i=0;i<daysAdapter.getSelecteditem().size();i++){
					outputStrArr[i]=daysAdapter.getSelecteditem().get(i);
				}
				
				mListener.onDays(outputStrArr,selectedrow);
				
				
			}
		});
		return b.create();
	}
	
	public void setDynamicDaysListener(onDaysSelectListener listener){
		this.mListener = listener;
	}
}

