package com.wiinnova.doctorsdiary.popupwindow;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.ContentResolver;
import android.os.Bundle;
import android.provider.Settings;
import android.provider.Settings.SettingNotFoundException;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager.LayoutParams;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

import com.wiinnova.doctorsdiary.R;

public class SystemBrightnessPopup extends DialogFragment{
	//Seek bar object
	private SeekBar brightbar;

	//Variable to store brightness value
	private int brightness;
	//Content resolver used as a handle to the system's settings
	private ContentResolver cResolver;
	//Window object, that will store a reference to the current window
	private Window window;

	TextView txtPerc;

@Override
public View onCreateView(LayoutInflater inflater, ViewGroup container,
		Bundle savedInstanceState) {
	// TODO Auto-generated method stub
	getDialog().setCancelable(false);
	getDialog().setCanceledOnTouchOutside(true);
	return super.onCreateView(inflater, container, savedInstanceState);
}
	
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		AlertDialog.Builder b=new AlertDialog.Builder(getActivity());

		
		LayoutInflater i = getActivity().getLayoutInflater();
		View v = i.inflate(R.layout.popup_systembrightness,null);

		  //Instantiate seekbar object
        brightbar = (SeekBar)v. findViewById(R.id.brightbar);
 
        txtPerc = (TextView)v.findViewById(R.id.txtPercentage);
 
        //Get the content resolver
        cResolver = getActivity().getContentResolver();
 
        //Get the current window
        window = getActivity().getWindow();
 
        //Set the seekbar range between 0 and 255
        brightbar.setMax(255);
        //Set the seek bar progress to 1
        brightbar.setKeyProgressIncrement(1);
        
        try
        {
            //Get the current system brightness
            brightness = Settings.System.getInt(cResolver, Settings.System.SCREEN_BRIGHTNESS);
        } 
        catch (SettingNotFoundException e) 
        {
            //Throw an error case it couldn't be retrieved
            Log.e("Error", "Cannot access system brightness");
            e.printStackTrace();
        }
 
        b.setView(v);
        //Set the progress of the seek bar based on the system's brightness
        brightbar.setProgress(brightness);
        brightbar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() 
        {
            public void onStopTrackingTouch(SeekBar seekBar) 
            {
                //Set the system brightness using the brightness variable value
                Settings.System.putInt(cResolver, Settings.System.SCREEN_BRIGHTNESS, brightness);
                //Get the current window attributes
                LayoutParams layoutpars = window.getAttributes();
                //Set the brightness of this window
                layoutpars.screenBrightness = brightness / (float)255;
                //Apply attribute changes to this window
                window.setAttributes(layoutpars);
            }
 
            public void onStartTrackingTouch(SeekBar seekBar) 
            {
                //Nothing handled here
            }
 
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) 
            {
                //Set the minimal brightness level
                //if seek bar is 20 or any value below
                if(progress<=20)
                {
                    //Set the brightness to 20
                    brightness=20;
                }
                else //brightness is greater than 20
                {
                    //Set brightness variable based on the progress bar 
                    brightness = progress;
                }
                //Calculate the brightness percentage
                float perc = (brightness /(float)255)*100;
                //Set the brightness percentage 
                txtPerc.setText((int)perc +" %");
            }
        });    
		return b.create();
	}
}
