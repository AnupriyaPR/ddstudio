package com.wiinnova.doctorsdiary.popupwindow;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.hardware.Camera;
import android.hardware.Camera.AutoFocusCallback;
import android.hardware.Camera.PreviewCallback;
import android.hardware.Camera.Size;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.wiinnova.doctorsdiary.R;
import com.parse.ParseObject;
import com.wiinnova.doctorsdiary.fragment.CameraPreview;

import net.sourceforge.zbar.Config;
import net.sourceforge.zbar.Image;
import net.sourceforge.zbar.ImageScanner;
import net.sourceforge.zbar.Symbol;
import net.sourceforge.zbar.SymbolSet;

public class Scanner_Popup extends DialogFragment  {

	private Camera mCamera;
	private CameraPreview mPreview;
	private Handler autoFocusHandler;

	public static ParseObject patient1;
	private boolean barcodeScanned = false;
	private boolean previewing = true;

	TextView scanText;
	Button scanButton;
	ImageScanner scanner;
	int scanType;

	static {
		System.loadLibrary("iconv");
	} 
	String pid,butval;
	ProgressDialog mProgressDialog;
	onScannerpopuplistener listener;




	public interface onScannerpopuplistener{
		void Scanner(String patientid, int scanType);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		getDialog().setCanceledOnTouchOutside(true);
		
	
		
		return super.onCreateView(inflater, container, savedInstanceState);
	}
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		AlertDialog.Builder b=new AlertDialog.Builder(getActivity());
		LayoutInflater i=getActivity().getLayoutInflater();

		View v = i.inflate(R.layout.scan_barcode_regpatient_activity,null);
		
		Bundle mArgs = getArguments();
		//listener = (onScannerpopuplistener) mArgs.getSerializable("listener");
		scanType=mArgs.getInt("scantype");
		autoFocusHandler = new Handler();
		mCamera = getCameraInstance();
		mCamera.setAutoFocusMoveCallback(null);
		



		/*Intent intent = new Intent("com.google.zxing.client.android.SCAN");
		intent.setClassName(getActivity(), "com.google.zxing.client.android.CaptureActivity");
		intent.putExtra("SCAN_FORMATS","QR_CODE,CODE_128");

		startActivityForResult(intent, 1001);
*/


		System.out.println("popup working");

		/* Instance barcode scanner */
		scanner = new ImageScanner();
		scanner.setConfig(0, Config.X_DENSITY, 3);
		scanner.setConfig(0, Config.Y_DENSITY, 3);


		mPreview = new CameraPreview(getActivity().getApplicationContext(), mCamera, previewCb, autoFocusCB);
		FrameLayout preview = (FrameLayout)v.findViewById(R.id.cameraPreview);
		preview.addView(mPreview);

		scanText = (TextView)v.findViewById(R.id.scanText);
		b.setView(v);

		return b.create();
	}
	public onScannerpopuplistener getListener() {
		return listener;
	}
	public void setListener(onScannerpopuplistener listener) {
		this.listener = listener;
	}
	

	public void onPause() {
		super.onPause();
		releaseCamera();
	}

	/** A safe way to get an instance of the Camera object. */
	public static Camera getCameraInstance(){
		Camera c = null;
		try {
			c = Camera.open();
		} catch (Exception e){
		}
		return c;
	}

	private void releaseCamera() {
		if (mCamera != null) {
			previewing = false;
			mCamera.setPreviewCallback(null);
			mCamera.release();
			mCamera = null;
		}
	}

	private Runnable doAutoFocus = new Runnable() {
		public void run() {
			if (previewing)
				mCamera.autoFocus(autoFocusCB);
		}
	};

	// Mimic continuous auto-focusing
	AutoFocusCallback autoFocusCB = new AutoFocusCallback() {
		public void onAutoFocus(boolean success, Camera camera) {
			autoFocusHandler.postDelayed(doAutoFocus, 1000);
		}
	};


	PreviewCallback previewCb = new PreviewCallback() {
		public void onPreviewFrame(byte[] data, Camera camera) {
			Camera.Parameters parameters = camera.getParameters();
			Size size = parameters.getPreviewSize();

			Image barcode = new Image(size.width, size.height, "Y800");
			barcode.setData(data);

			int result = scanner.scanImage(barcode);

			if (result != 0) {
				previewing = false;
				mCamera.setPreviewCallback(null);
				mCamera.stopPreview();

				SymbolSet syms = scanner.getResults();
				for (Symbol sym : syms) {

					pid=sym.getData();
					scanText.setText("Patient Id:" + sym.getData());
					barcodeScanned = true;
				}


				listener.Scanner(pid,scanType);

			}
		}
	};

	/*public void onActivityResult(int requestCode, int resultCode, Intent intent) {
		String contents = intent.getStringExtra("SCAN_RESULT");
		listener.Scanner(contents,scanType);
		Toast.makeText(getActivity(), "Result...."+contents, Toast.LENGTH_LONG).show();


	};*/
}
