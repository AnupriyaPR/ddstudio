package com.wiinnova.doctorsdiary.fragment;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wiinnova.doctorsdiary.R;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.wiinnova.doctorsdiary.activities.FragmentActivityContainer;
import com.wiinnova.doctorsdiary.activities.Home_Page_Activity;
import com.wiinnova.doctorsdiary.supportclasses.AppUtil;

import java.util.List;

public class ManagementGynacologist_View extends Fragment {

    TextView dflName, duniqueID;
    String puid, filan;
    TextView symptoms, syndromes, additionalcumm, date, llist;
    ProgressDialog mProgressDialog;
    private LinearLayout layoutcontainer;
    ParseObject patient2;
    ParseObject patient;
    int skip = 0;
    private long currentDateandTime;
    private String formattedDate;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        View view = inflater.inflate(R.layout.managementgynacologistview, null);

        dflName = (TextView) view.findViewById(R.id.tvfirstlastName);
        duniqueID = (TextView) view.findViewById(R.id.cduniqueid);
        layoutcontainer = (LinearLayout) view.findViewById(R.id.diadynamic);

        FragmentActivityContainer.treatmangement_save_check = 0;

        ((FragmentActivityContainer) getActivity()).gettreatmentfrag().onTreatmentmanagementbtn(0);

        if (Home_Page_Activity.patient != null) {
            puid = Home_Page_Activity.patient.getString("patientID");
            String fname = Home_Page_Activity.patient.getString("firstName");
            String lname = Home_Page_Activity.patient.getString("lastName");
            System.out.println("patient id1,,,,,," + puid);
            if (fname == null) {
                fname = "FirstName";
            }
            if (lname == null) {
                lname = "LastName";
            }
            dflName.setText(fname + " " + lname);

        } else {

            SharedPreferences scanpidnew = getActivity().getSharedPreferences("NewPatID", 0);
            String scnewPid = scanpidnew.getString("NewID", null);
            SharedPreferences scanpid1 = getActivity().getSharedPreferences("ScannedPid", 0);
            String scPid = scanpid1.getString("scanpid", null);
            if (scPid != null) {
                puid = scPid;
                SharedPreferences fnname1 = getActivity().getSharedPreferences("fnName", 0);
                dflName.setText(fnname1.getString("fNname", "FirstName" + " " + "LastName"));
                System.out.println("name diag view.." + fnname1.getString("fNname", "FirstName" + " " + "LastName"));
            }

            if (scnewPid != null) {

                puid = scnewPid;


                SharedPreferences fnname1 = getActivity().getSharedPreferences("fnName", 0);
                dflName.setText(fnname1.getString("fNname", "FirstName" + " " + "LastName"));
                System.out.println("name diag view.." + fnname1.getString("fNname", "FirstName" + " " + "LastName"));
            }
        }

        duniqueID.setText(puid);


        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setCancelable(false);
        mProgressDialog.setMessage("Retrieving Examination Data...");
        mProgressDialog.show();
        System.out.println("object taken from main frag not working");


        fetchingdata_from_server();

        return view;
    }


    private void fetchingdata_from_server() {
        // TODO Auto-generated method stub

        final ParseQuery<ParseObject> query = ParseQuery.getQuery("Diagnosis");
        if (!AppUtil.haveNetwokConnection(getActivity()))
            query.fromLocalDatastore();
        query.whereEqualTo("patientID", puid);
        query.addDescendingOrder("createdDate");


        mProgressDialog.setOnCancelListener(new OnCancelListener() {

            @Override
            public void onCancel(DialogInterface dialog) {
                // TODO Auto-generated method stub
                query.cancel();
            }
        });

        query.findInBackground(new FindCallback<ParseObject>() {

            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                // TODO Auto-generated method stub


                System.out.println("objectsssssss size" + objects.size());

                for (int i = 0; i < objects.size(); i++) {

                    ParseObject managementtreatmentparseObj = objects.get(i);
                    double type = managementtreatmentparseObj.getDouble("typeFlag");

                    LayoutInflater inflater = LayoutInflater.from(getActivity());
                    if (inflater != null) {

                        View view = inflater.inflate(R.layout.prescriptionmanagement_gynecologist, null);
                        if (type == 2) {

                            CheckBox cbconservative;
                            CheckBox cbsurgical;
                            EditText etlineoftreatment;

                            CheckBox cbphysical;
                            CheckBox cbpelvic;
                            EditText etexercise;

                            CheckBox cbsalt;
                            CheckBox cbsugar;

                            CheckBox cbhighprotein;
                            CheckBox cbother;
                            EditText etDiet;

                            EditText etsometext;


                            TextView ctmcurrdate = (TextView) view.findViewById(R.id.ctmcurrdate);
                            TextView cdhead = (TextView) view.findViewById(R.id.cdhead);
                            /*TextView Name = (TextView)view.findViewById(R.id.tvname);
                            TextView uniqueID = (TextView)view.findViewById(R.id.cduniqueid);*/

							/*Name.setVisibility(View.INVISIBLE);
							uniqueID.setVisibility(View.INVISIBLE);*/
                            cdhead.setVisibility(View.INVISIBLE);

                            cbsurgical = (CheckBox) view.findViewById(R.id.cbsurgical);
                            cbconservative = (CheckBox) view.findViewById(R.id.cbconservation);

                            cbphysical = (CheckBox) view.findViewById(R.id.cbphysical);

                            cbpelvic = (CheckBox) view.findViewById(R.id.cbpelvic);

                            cbsalt = (CheckBox) view.findViewById(R.id.cbsalt);

                            cbsugar = (CheckBox) view.findViewById(R.id.cbsugar);

                            cbhighprotein = (CheckBox) view.findViewById(R.id.cbhighprotein);

                            cbother = (CheckBox) view.findViewById(R.id.cbother);

                            etlineoftreatment = (EditText) view.findViewById(R.id.etlineoftreatment);
                            etDiet = (EditText) view.findViewById(R.id.etother);
                            etexercise = (EditText) view.findViewById(R.id.etgeneral);
                            etsometext = (EditText) view.findViewById(R.id.etexersise);


                            if (managementtreatmentparseObj.getString("createdDate") != null) {
                                ctmcurrdate.setText(managementtreatmentparseObj.getString("createdDate"));
                            }


                            if (managementtreatmentparseObj.getString("lineoftreatment") != null) {
                                String[] lineoftreatment = managementtreatmentparseObj.getString("lineoftreatment").split(",");

                                if (lineoftreatment[0].equals("Conservation")) {
                                    cbconservative.setChecked(true);
                                } else if (lineoftreatment[0].equals("Surgical")) {
                                    cbsurgical.setChecked(true);
                                }

                                if (!lineoftreatment[1].equalsIgnoreCase("Nil")) {
                                    etlineoftreatment.setText(lineoftreatment[1].toString());
                                }
                            }


                            if (managementtreatmentparseObj.getString("general_exercise") != null) {
                                String[] exercise = managementtreatmentparseObj.getString("general_exercise").split(",");

                                if (exercise[0].equals("Physical")) {
                                    cbphysical.setChecked(true);
                                } else if (exercise[0].equals("Pelvic")) {
                                    cbpelvic.setChecked(true);
                                }

                                if (!exercise[1].equalsIgnoreCase("Nil")) {
                                    etexercise.setText(exercise[1].toString());
                                }
                            }


                            if (managementtreatmentparseObj.getString("general_diet") != null) {
                                String diet = managementtreatmentparseObj.getString("general_diet");

                                if (diet.equalsIgnoreCase("salt")) {
                                    cbsalt.setChecked(true);
                                } else if (diet.equalsIgnoreCase("sugar")) {
                                    cbsugar.setChecked(true);
                                }
                            }


                            if (managementtreatmentparseObj.getString("general_diet_highprotein") != null) {
                                String dietsecond[] = managementtreatmentparseObj.getString("general_diet_highprotein").split(",");

                                if (dietsecond[0].equals("High Protein")) {
                                    cbhighprotein.setChecked(true);
                                } else if (dietsecond[0].equals("Other")) {
                                    cbother.setChecked(true);
                                }
                                if (!dietsecond[1].equalsIgnoreCase("Nil")) {
                                    etDiet.setText(dietsecond[1].toString());
                                }
                            }


                            if (managementtreatmentparseObj.getString("exercise") != null) {
                                if (!managementtreatmentparseObj.getString("exercise").equals("Nil")) {

                                    etexercise.setText(managementtreatmentparseObj.getString("exercise").toString());
                                }
                            }


                            cbconservative.setEnabled(false);
                            cbsurgical.setEnabled(false);
                            etlineoftreatment.setEnabled(false);
                            cbphysical.setEnabled(false);
                            cbpelvic.setEnabled(false);
                            etexercise.setEnabled(false);
                            cbsalt.setEnabled(false);
                            cbsugar.setEnabled(false);
                            cbhighprotein.setEnabled(false);
                            cbother.setEnabled(false);
                            etDiet.setEnabled(false);
                            etsometext.setEnabled(false);


                            if (managementtreatmentparseObj.getString("createdDate") != null ||
                                    managementtreatmentparseObj.getString("lineoftreatment") != null ||
                                    managementtreatmentparseObj.getString("general_exercise") != null ||
                                    managementtreatmentparseObj.getString("general_diet") != null ||
                                    managementtreatmentparseObj.getString("general_diet_highprotein") != null ||
                                    managementtreatmentparseObj.getString("exercise") != null) {

                                layoutcontainer.addView(view);
                            }
                        }


                    }


                }

                mProgressDialog.dismiss();
            }
        });


    }


}
