package com.wiinnova.doctorsdiary.fragment;

import android.app.Application;

import com.wiinnova.doctorsdiary.R;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.StandardExceptionParser;
import com.google.android.gms.analytics.Tracker;
import com.parse.FindCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.wiinnova.doctorsdiary.supportclasses.AnalyticsTrackers;

import org.acra.ACRA;
import org.acra.ReportingInteractionMode;
import org.acra.annotation.ReportsCrashes;

import java.util.ArrayList;
import java.util.List;

@ReportsCrashes(formKey = "", // will not be used
mailTo = "info@wiinnova.com",
mode = ReportingInteractionMode.TOAST,
resToastText = R.string.crash_toast_text)  

public class CrashReporter extends Application{

	private static List<ParseObject>  diseaseobjects = new ArrayList<ParseObject>();
	private static List<ParseObject>  symptomsobjects = new ArrayList<ParseObject>();
	private static List<ParseObject>  medicineobjects = new ArrayList<ParseObject>();
	int localobjectsize;
	int localsymptomsobjectsize;
	int localmedicineobjectsize;
	int skip=0;

	public static final String TAG = CrashReporter.class
			.getSimpleName();

	private static CrashReporter mInstance;



	public static synchronized CrashReporter getInstance() {
		return mInstance;
	}

	public synchronized Tracker getGoogleAnalyticsTracker() {
		AnalyticsTrackers analyticsTrackers = AnalyticsTrackers.getInstance();
		return analyticsTrackers.get(AnalyticsTrackers.Target.APP);
	}

	/***
	 * Tracking screen view
	 *
	 * @param screenName screen name to be displayed on GA dashboard
	 */
	public void trackScreenView(String screenName) {
		Tracker t = getGoogleAnalyticsTracker();

		// Set screen name.
		t.setScreenName(screenName);

		// Send a screen view.
		t.send(new HitBuilders.ScreenViewBuilder().build());

		GoogleAnalytics.getInstance(this).dispatchLocalHits();
	}

	/***
	 * Tracking exception
	 *
	 * @param e exception to be tracked
	 */
	public void trackException(Exception e) {
		if (e != null) {
			Tracker t = getGoogleAnalyticsTracker();

			t.send(new HitBuilders.ExceptionBuilder()
			.setDescription(
					new StandardExceptionParser(this, null)
					.getDescription(Thread.currentThread().getName(), e))
					.setFatal(false)
					.build()
					);
		}
	}

	/***
	 * Tracking event
	 *
	 * @param category event category
	 * @param action   action of the event
	 * @param label    label
	 */
	public void trackEvent(String category, String action, String label) {
		Tracker t = getGoogleAnalyticsTracker();

		// Build and send an Event.
		t.send(new HitBuilders.EventBuilder().setCategory(category).setAction(action).setLabel(label).build());
	}

	@Override
	public void onCreate(){
		// TODO Auto-generated method stub
		super.onCreate();


		ACRA.init(this);
		Parse.enableLocalDatastore(this);
		Parse.initialize(this, "vrA1DYO8NtY4TkNCEagJshdUxLJDowGiWrBfkXwA", "Dv7FuFQCag8Oj1k0upq8EUnLbDfXlqATpNuCaYE1");

		mInstance = this;

		AnalyticsTrackers.initialize(this);
		AnalyticsTrackers.getInstance().get(AnalyticsTrackers.Target.APP);

		/*diseaseobjects=new ArrayList<ParseObject>();
		fetechingdiseasenames_server();*/

		fetechdiseasefromserver();
		fetechsymptomsfromserver();
		
		fetechmedicinenamefromserver();





		/**
		 * Gets the default {@link Tracker} for this {@link Application}.
		 * @return tracker
		 */
	}

	private void fetechmedicinenamefromserver() {
		// TODO Auto-generated method stub
		ParseQuery<ParseObject> query = ParseQuery.getQuery("MedicineList");
		query.fromLocalDatastore();
		query.findInBackground(new FindCallback<ParseObject>() {
			
			@Override
			public void done(List<ParseObject> objects, ParseException e) {
				// TODO Auto-generated method stub
				
				if(e==null){
					localmedicineobjectsize=objects.size();
					ParseQuery<ParseObject> query = ParseQuery.getQuery("MedicineList");
					if(localmedicineobjectsize!=0){
						query.fromLocalDatastore();
					}
					
					query.setLimit(1000);
					query.findInBackground(getAllmedicineObjects());
				}
				
			}
		});
		
		
		
	}

	private void fetechsymptomsfromserver() {
		// TODO Auto-generated method stub

		ParseQuery<ParseObject> query = ParseQuery.getQuery("SymptomsDB");
		query.fromLocalDatastore();
		query.findInBackground(new FindCallback<ParseObject>() {

			@Override
			public void done(List<ParseObject> objects, ParseException e) {
				// TODO Auto-generated method stub

				if(e==null){
					localsymptomsobjectsize=objects.size();
					System.out.println("localobjectsize"+localsymptomsobjectsize);
					ParseQuery<ParseObject> query = ParseQuery.getQuery("SymptomsDB");
					if(localsymptomsobjectsize!=0){
						query.fromLocalDatastore();
					}


					query.setLimit(1000);
					query.findInBackground(getAllsymptomsObjects());
				}
			}
		});
	}

	private void fetechdiseasefromserver() {
		// TODO Auto-generated method stub
		ParseQuery<ParseObject> query = ParseQuery.getQuery("DiseaseDatabase");
		query.fromLocalDatastore();
		query.findInBackground(new FindCallback<ParseObject>() {

			@Override
			public void done(List<ParseObject> objects, ParseException e) {
				// TODO Auto-generated method stub

				if(e==null){

					localobjectsize=objects.size();
					System.out.println("localobjectsize"+localobjectsize);
					ParseQuery<ParseObject> query = ParseQuery.getQuery("DiseaseDatabase");
					if(localobjectsize!=0){
						query.fromLocalDatastore();
					}


					query.setLimit(1000);
					query.findInBackground(getAllObjects());
				}
			}
		});
	}

	private FindCallback getAllObjects() {
		// TODO Auto-generated method stub

		return new FindCallback<ParseObject>() {

			@Override
			public void done(List<ParseObject> objects, ParseException e) {
				// TODO Auto-generated method stub

				if (e == null) {

					diseaseobjects.addAll(objects);

					int limit =1000;
					if (objects.size() == limit){
						skip = skip + limit;
						ParseQuery query = new ParseQuery("DiseaseDatabase");
						if(localobjectsize!=0){
							query.fromLocalDatastore();
						}
						query.setSkip(skip);
						query.setLimit(limit);
						query.findInBackground(getAllObjects());
					}
					else {
						if(ParseObject.pinAllInBackground(diseaseobjects) != null){
							System.out.println("pin successs"+diseaseobjects.size());
						}

						ParseObject.pinAllInBackground(diseaseobjects);
					}


				}
			}
		};
	}

	
	private FindCallback<ParseObject> getAllmedicineObjects() {
		// TODO Auto-generated method stub
		return new FindCallback<ParseObject>() {

			@Override
			public void done(List<ParseObject> objects, ParseException e) {
				// TODO Auto-generated method stub

				if (e == null) {

					medicineobjects.addAll(objects);

					int limit =1000;
					if (objects.size() == limit){
						skip = skip + limit;
						ParseQuery query = new ParseQuery("MedicineList");
						if(localmedicineobjectsize!=0){
							query.fromLocalDatastore();
						}
						query.setSkip(skip);
						query.setLimit(limit);
						query.findInBackground(getAllmedicineObjects());
					}
					else {
						if(ParseObject.pinAllInBackground(medicineobjects) != null){
							System.out.println("pin successs"+medicineobjects.size());
						}

						ParseObject.pinAllInBackground(medicineobjects);
					}


				}
			}
		};
	}
	

	private FindCallback getAllsymptomsObjects() {
		// TODO Auto-generated method stub

		return new FindCallback<ParseObject>() {

			@Override
			public void done(List<ParseObject> objects, ParseException e) {
				// TODO Auto-generated method stub

				if (e == null) {

					symptomsobjects.addAll(objects);

					int limit =1000;
					if (objects.size() == limit){
						skip = skip + limit;
						ParseQuery query = new ParseQuery("SymptomsDB");
						if(localobjectsize!=0){
							query.fromLocalDatastore();
						}
						query.setSkip(skip);
						query.setLimit(limit);
						query.findInBackground(getAllsymptomsObjects());
					}
					else {
						if(ParseObject.pinAllInBackground(symptomsobjects) != null){
							System.out.println("pin successs"+symptomsobjects.size());
						}

						ParseObject.pinAllInBackground(symptomsobjects);
					}


				}
			}
		};
	}




	public List<ParseObject> getDiseasenameObjects(){
		return diseaseobjects;
	}
	public void setDiseasenameObjects(List<ParseObject>  obj){
		this.diseaseobjects=obj;
	}

	public List<ParseObject> getSymptomsnameObjects(){
		return symptomsobjects;
	}
	public void setSymptomsnameObjects(List<ParseObject>  obj){
		this.symptomsobjects=obj;
	}
	
	public List<ParseObject> getMedicinenameObjects(){
		return medicineobjects;
	}
	public void setMedicinenameObjects(List<ParseObject>  obj){
		this.medicineobjects=obj;
	}
	
	

}
