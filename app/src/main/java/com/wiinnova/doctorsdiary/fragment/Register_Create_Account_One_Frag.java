package com.wiinnova.doctorsdiary.fragment;


import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.wiinnova.doctorsdiary.R;
import com.parse.FindCallback;
import com.parse.FunctionCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.SaveCallback;
import com.wiinnova.doctorsdiary.activities.Home_Page_Activity;
import com.wiinnova.doctorsdiary.popupwindow.SpinnerPopup;
import com.wiinnova.doctorsdiary.popupwindow.SpinnerPopup.onSubmitListener;
import com.wiinnova.doctorsdiary.popupwindow.SuspectedDiseaseSelectorPopup.onSubmitListenerSds;
import com.wiinnova.doctorsdiary.popupwindow.TermsandconditionsPopup;
import com.wiinnova.doctorsdiary.popupwindow.TermsandconditionsPopup.onTermsandcondition;
import com.wiinnova.doctorsdiary.supportclasses.QualificationSelector_Popup;
import com.wiinnova.doctorsdiary.supportclasses.QualificationSelector_Popup.onQualificationSubmitListener;
import com.wiinnova.doctorsdiary.supportclasses.Qualificationnameclass;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Register_Create_Account_One_Frag extends Fragment implements  Serializable{

	private Button next;
	private Button submit;
	private Button cancel;

	private EditText strName;
	private EditText cName;
	private EditText pin;
	private EditText phno;
	private EditText superSpl;
	private EditText docRegno;
	private EditText firstName;
	private EditText lastName;
	private EditText email;
	private EditText confEmail;
	private EditText createPass;
	private EditText confPass;

	private TextView qualificationsSpin;
	private TextView specializationSpin;
	private TextView accreditionSpin;
	private TextView state;
	private TextView cbTerms;
	private TextView countrySpin;

	private ListView listview;


	private String[] country_array; 

	private LinearLayout ll;
	private LinearLayout accSpin;
	private RelativeLayout container;

	private onSubmitListener spinnerpopupListener;
	private ProgressDialog mProgressDialog;
	private onSubmitListenerSds diagnosisListener;

	int localobjectsize;
	int skip=0;
	int flag;
	String fName,lName,eMail,pass,strname,cityname,stat,pinzip,phone,supSpl,dregNo,country,qualification,specialization,accredition;
	String[]  myaccrArray=null;
	String[]  myqulrArray=null;
	String[]  mysplrArray=null;
	String[]  state_array;
	String fullname;
	String Signature;
	String Signdate;
	ParseObject patient;
	List<ParseObject> diseaseobjects=new ArrayList<ParseObject>();
	SpinnerPopup spObj;
	protected QualificationSelector_Popup spObjQualification;
	ArrayList<Qualificationnameclass> quantitynamearraylist=new ArrayList<Qualificationnameclass>();
	String[] selectedQuantity_first;
	private onQualificationSubmitListener quantitySelectedListener;
	String Selectedquantity_first="";


	@Override
	public View onCreateView(LayoutInflater inflater,
			ViewGroup container, Bundle savedInstanceState) {

		View myView = inflater.inflate(R.layout.reg_create_account_one_frag, container,false);


		container=(RelativeLayout)myView.findViewById(R.id.topContainer);

		fetchingSystemKeys();

		cbTerms=(TextView)myView.findViewById(R.id.cbterms);
		state_array=getResources().getStringArray(R.array.state_array);  
		country_array=getResources().getStringArray(R.array.countries_array);  
		firstName=(EditText)myView.findViewById(R.id.cafstname);
		lastName=(EditText)myView.findViewById( R.id.calstname);
		email=(EditText)myView.findViewById(R.id.caemail);
		confEmail=(EditText)myView.findViewById(R.id.calconfemail);
		createPass=(EditText)myView.findViewById(R.id.capass);
		confPass=(EditText)myView.findViewById(R.id.calconfpass);
		next=(Button)myView.findViewById(R.id.canext);

		ll=(LinearLayout)myView.findViewById(R.id.call4);
		accSpin=(LinearLayout)myView.findViewById(R.id.catl6);

		//Spinners
		countrySpin=(TextView)myView.findViewById(R.id.catcountry);
		accreditionSpin=(TextView)myView.findViewById(R.id.cataccredition);
		qualificationsSpin=(TextView)myView.findViewById(R.id.catqualifications);
		specializationSpin=(TextView)myView.findViewById(R.id.catspecialization);

		//Edit Text
		strName=(EditText)myView.findViewById(R.id.catstname);
		cName=(EditText)myView.findViewById(R.id.catcitname);
		pin=(EditText)myView.findViewById(R.id.catpincode);
		state=(TextView)myView.findViewById(R.id.catstate);
		phno=(EditText)myView.findViewById(R.id.catphno);
		superSpl=(EditText)myView.findViewById(R.id.catsupspecial);
		docRegno=(EditText)myView.findViewById(R.id.catdocregno);
		
		


		//Buttons
		cancel=(Button)myView.findViewById(R.id.canext);
		submit=(Button)myView.findViewById(R.id.submit);

		spinnerpopupListener=new onSubmitListener() {

			@Override
			public void onSubmit(String arg, int position) {
				// TODO Auto-generated method stub

				if(flag==0){
					countrySpin.setText(arg);
				}
				else if(flag==1){
					accreditionSpin.setText(arg);
				}else if(flag==2){
					qualificationsSpin.setText(arg);
				}else if(flag==3){
					specializationSpin.setText(arg);
				}else if(flag==4){
					state.setText(arg);
				}
				spObj.dismiss();
			}
		};
		
		quantitySelectedListener=new onQualificationSubmitListener() {

			@Override
			public void onquantitySubmit(ArrayList<Qualificationnameclass> outputStr, int positionvalue) {
				// TODO Auto-generated method stub

				System.out.println("position value@@@@@@@@"+positionvalue);

				String quantity="";
				String[] outputStrArr=new String[outputStr.size()];
				for(int i=0;i<outputStr.size();i++){

					if(i!=outputStr.size()-1)
						quantity+=outputStr.get(i).getQualificationname()+",";
					else
						quantity+=outputStr.get(i).getQualificationname();

					outputStrArr[i]=outputStr.get(i).getQualificationname();
				}

				if(flag==2){

					Selectedquantity_first="";
					selectedQuantity_first=outputStrArr;
					for(int i=0;i<selectedQuantity_first.length;i++){
						Selectedquantity_first+=selectedQuantity_first[i]+",";

					}

					qualificationsSpin.setText(quantity);
					spObjQualification.dismiss();
				}
				
				
				
			}


		};




		cbTerms.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				TermsandconditionsPopup dialog=new TermsandconditionsPopup(new onTermsandcondition() {

					@Override
					public void termsandcondition(String signature,String date) {
						// TODO Auto-generated method stub
						System.out.println("signature"+signature);
						Signature=signature;
						Signdate=date;

					}
				});	
				dialog.show(getFragmentManager(),"jjjjjjjjj");

			}
		});



		container.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				hideKeyboard(v);

			}
		});

		countrySpin.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub

				flag=0;
				Bundle bundle=new Bundle();
				bundle.putString("name", "Country");
				bundle.putStringArray("items",country_array);
				//bundle.putSerializable("listener", Register_Create_Account_One_Frag.this);
				spObj=new SpinnerPopup();
				spObj.setSubmitListener(spinnerpopupListener);
				spObj.setArguments(bundle);
				spObj.show(getFragmentManager(), "tag");


			}
		});

		state.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub

				flag=4;
				Bundle bundle=new Bundle();
				bundle.putString("name", "State");
				bundle.putStringArray("items",state_array);
				//	bundle.putSerializable("listener", Register_Create_Account_One_Frag.this);
				spObj=new SpinnerPopup();
				spObj.setSubmitListener(spinnerpopupListener);
				spObj.setArguments(bundle);
				spObj.show(getFragmentManager(), "tag");


			}
		});



		accreditionSpin.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub

				if(myaccrArray==null){
					fetchingSystemKeys();
				}

				if(myaccrArray!=null){
					if(myaccrArray.length>0){
						flag=1;
						Bundle bundle=new Bundle();
						bundle.putString("name", "Accredition");
						bundle.putStringArray("items",myaccrArray);
						//bundle.putSerializable("listener", Register_Create_Account_One_Frag.this);
						spObj=new SpinnerPopup();
						spObj.setSubmitListener(spinnerpopupListener);
						spObj.setArguments(bundle);
						spObj.show(getFragmentManager(), "tag");
					}
				}else{
					Toast.makeText(getActivity(), "Internet is not connected.Please Switch On Net Connectivity", Toast.LENGTH_LONG).show();
				}
			}
		});

		qualificationsSpin.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if(myqulrArray==null){
					fetchingSystemKeys();
				}


				if(myqulrArray!=null){
					if(myqulrArray.length>0){
						flag=2;
//						Bundle bundle=new Bundle();
//						bundle.putString("name", "Qualifications");
//						bundle.putStringArray("items",myqulrArray);
						//bundle.putSerializable("listener", Register_Create_Account_One_Frag.this);
						/*spObj=new SpinnerPopup();
						spObj.setSubmitListener(spinnerpopupListener);
						spObj.setArguments(bundle);
						spObj.show(getFragmentManager(), "tag");*/
//						spObjQualification = new SuspectedDiseaseSelectorPopup();
//						spObjQualification.setSubmitListenerSds(diagnosisListener);

						/*Bundle bundle=new Bundle();

						bundle.putSerializable("diseasenamearray",diseasenamearraylist);
						bundle.putSerializable("selecteddisease", disease_array);*/
//						spObjQualification.setArguments(bundle);
//						spObjQualification.show(getFragmentManager(), "tag");
						
						
						
						Bundle bundle=new Bundle();
						bundle.putInt("row", flag);
						bundle.putString("name", "Qualifications");
						bundle.putStringArray("items",myqulrArray);
						bundle.putSerializable("quantiyname", quantitynamearraylist);
						bundle.putStringArray("selectedquantity", selectedQuantity_first);
						spObjQualification=new QualificationSelector_Popup();
						spObjQualification.setQualiSubmitListener(quantitySelectedListener);
						spObjQualification.setArguments(bundle);
						spObjQualification.show(getFragmentManager(), "tag");

					}
				}else{
					Toast.makeText(getActivity(), "Internet is not connected.Please Switch On Net Connectivity", Toast.LENGTH_LONG).show();
				}
			}
		});

		specializationSpin.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if(mysplrArray==null){
					fetchingSystemKeys();
				}
				if(mysplrArray!=null){
					if(mysplrArray.length>0){
						flag=3;
						Bundle bundle=new Bundle();
						bundle.putString("name", "Specialization");
						bundle.putStringArray("items",mysplrArray);
						//bundle.putSerializable("listener", Register_Create_Account_One_Frag.this);
						spObj=new SpinnerPopup();
						spObj.setSubmitListener(spinnerpopupListener);
						spObj.setArguments(bundle);
						spObj.show(getFragmentManager(), "tag");
					}
				}else{
					Toast.makeText(getActivity(), "Internet is not connected.Please Switch On Net Connectivity", Toast.LENGTH_LONG).show();
				}
			}
		});

		submit.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				//hideKeyboard(arg0);

				boolean validfields=true;
				hideKeyboard(arg0);




				final String fname=firstName.getText().toString().trim();
				final String lname=lastName.getText().toString().trim();
				final String eMail=email.getText().toString().trim();
				String ceMail=confEmail.getText().toString().trim();
				final String pass=createPass.getText().toString().trim();
				String cpass=confPass.getText().toString().trim();
				String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
				int length=pass.length();
				boolean ok = pass.matches("^(?=.*[A-Z])(?=.*\\d)(?!.*(AND|NOT)).*[a-z].*");


				strname=strName.getText().toString();
				cityname=cName.getText().toString();
				stat=state.getText().toString();
				pinzip=pin.getText().toString().trim();
				phone=phno.getText().toString().trim();
				supSpl=superSpl.getText().toString();
				dregNo=docRegno.getText().toString();

				if(TextUtils.isEmpty(phno.getText().toString())){
					phno.setError("Enter Phone Number ");
					phno.requestFocus();
					validfields=false;

				}else{
					phno.setError(null);
				}
				if(TextUtils.isEmpty(docRegno.getText().toString())){
					docRegno.setError("Enter Docter Registration Number ");
					docRegno.requestFocus();
					validfields=false;

				}else{
					docRegno.setError(null);
				}

				if(TextUtils.isEmpty(pin.getText().toString())){
					pin.setError("Enter Pincode ");
					pin.requestFocus();
					validfields=false;
				}else{
					pin.setError(null);
				}

				if(TextUtils.isEmpty(cName.getText().toString())){
					cName.setError("Enter City Name ");
					cName.requestFocus();
					validfields=false;
				}else{
					cName.setError(null);
				}

				if(TextUtils.isEmpty(strName.getText().toString())){
					strName.setError("Enter Street Name ");
					strName.requestFocus();
					validfields=false;
				}else{
					strName.setError(null);
				}

				if(TextUtils.isEmpty(confPass.getText().toString())){
					confPass.setError("Re Enter Password ");
					confPass.requestFocus();
					validfields=false;

				}else{
					confPass.setError(null);
				}

				if(TextUtils.isEmpty(createPass.getText().toString())){
					createPass.setError("Enter Password ");
					createPass.requestFocus();
					validfields=false;

				}else{
					createPass.setError(null);
				}

				if(TextUtils.isEmpty(confEmail.getText().toString())){
					confEmail.setError("Enter Confirm E-mail ");
					confEmail.requestFocus();
					validfields=false;

				}else{
					confEmail.setError(null);
				}

				if(TextUtils.isEmpty(email.getText().toString())){
					email.setError("Enter  E-mail ");
					email.requestFocus();
					validfields=false;

				}else{
					email.setError(null);
				}

				if(TextUtils.isEmpty(lastName.getText().toString())){
					lastName.setError("Enter  LastName ");
					lastName.requestFocus();
					validfields=false;

				}else{
					lastName.setError(null);
				}
				if(TextUtils.isEmpty(firstName.getText().toString())){
					firstName.setError("Enter  Name ");
					firstName.requestFocus();
					validfields=false;

				}else{
					firstName.setError(null);
				}

				if(countrySpin.getText().length()==0){
					Toast msg=Toast.makeText(getActivity(), "Select Country", Toast.LENGTH_LONG);
					msg.show();
					validfields=false;
				}
				else if(qualificationsSpin.getText().length()==0){
					Toast msg=Toast.makeText(getActivity(), "Select Qualification", Toast.LENGTH_LONG);
					msg.show();
					validfields=false;
				}else if(specializationSpin.getText().length()==0){
					Toast msg=Toast.makeText(getActivity(), "Select Specialization", Toast.LENGTH_LONG);
					msg.show();
					validfields=false;
				}else if(accreditionSpin.getText().length()==0){
					Toast msg=Toast.makeText(getActivity(), "Select Accredition", Toast.LENGTH_LONG);
					msg.show();
					validfields=false;
				}else if(state.getText().length()==0){
					Toast msg=Toast.makeText(getActivity(), "Select State", Toast.LENGTH_LONG);
					msg.show();
					validfields=false;
				}


				System.out.println("sign date"+Signdate);
				if(Signature!=null && !((Signdate.trim()).equals("DATE"))){
					if(validfields==true){
						if((eMail.equals(ceMail))&&(pass.equals(cpass))){
							if (eMail.matches(emailPattern))
							{
								if(length>=8&&length<=14){
									if(ok){

										country= countrySpin.getText().toString();
										qualification= qualificationsSpin.getText().toString();
										specialization= specializationSpin.getText().toString();
										accredition= accreditionSpin.getText().toString();


										if((qualification.equals("Qualifications"))||(specialization.equals("Specialization"))||(accredition.equals("Accredition"))||(country.equals("country"))){

											Toast.makeText(getActivity(),"Select All the values",Toast.LENGTH_SHORT).show();

										}
										else{
											if(phone.length() > 6 && phone.length() < 14)
											{
												if(pinzip.length()==6)
												{
													mProgressDialog = new ProgressDialog(getActivity());
													mProgressDialog.setMessage("Please Wait....");
													mProgressDialog.show();
													ParseQuery<ParseObject> query = ParseQuery.getQuery("UserMaster");
													query.whereEqualTo("docRegistrationNumber",dregNo);

													query.findInBackground(new FindCallback<ParseObject>() {

														@Override
														public void done(List<ParseObject> objects, ParseException e) {

															// TODO Auto-generated method stub
															if (e == null) {
																if (objects.size() > 0) {
																	// query found a user
																	Log.e("Login", "Id exist");
																	mProgressDialog.dismiss();
																	Toast.makeText(getActivity(),"Docter Registration number exist...",Toast.LENGTH_SHORT).show();
																}
																else {

																	ParseQuery<ParseObject> query = ParseQuery.getQuery("UserMaster");
																	query.whereEqualTo("email",eMail);
																	query.findInBackground(new FindCallback<ParseObject>() {

																		@Override
																		public void done(List<ParseObject> object, ParseException e) {
																			// TODO Auto-generated method stub
																			if(e==null){
																				if(object.size()>0){
																					mProgressDialog.dismiss();
																					Toast.makeText(getActivity(),"Docter E-mail exist...",Toast.LENGTH_SHORT).show();
																				}else{

																					fetechdiseasefromserver();


																					ParseObject userMast=new ParseObject("UserMaster");

																					userMast.put("accredition", accredition);
																					userMast.put("city", cityname);
																					userMast.put("country", country);
																					userMast.put("docRegistrationNumber", dregNo);
																					userMast.put("email", eMail);

																					userMast.put("firstName", fname);
																					userMast.put("lastName", lname);
																					userMast.put("password", pass);
																					userMast.put("phone", phone);
																					userMast.put("pincode", pinzip);
																					userMast.put("qualification", qualification);
																					userMast.put("specialization", specialization);
																					userMast.put("state", stat);
																					userMast.put("street", strname);
																					userMast.put("superSpecialization", supSpl);
																					userMast.put("Signature",Signature);
																					userMast.put("Signdate", Signdate);


																					userMast.pinInBackground(new SaveCallback() {

																						public void done(ParseException e) {
																							mProgressDialog.dismiss();

																							if (e == null) {
																								myObjectSavedSuccessfully();
																							} else {
																								myObjectSaveDidNotSucceed();
																							}
																						}



																						private void myObjectSaveDidNotSucceed() {
																							// TODO Auto-generated method stub
																							Toast msg1=Toast.makeText(getActivity(), "Registration Failed", Toast.LENGTH_LONG);
																							msg1.show();

																						}

																						private void myObjectSavedSuccessfully() {
																							// TODO Auto-generated method stub
																							Toast msg=Toast.makeText(getActivity(), "Registration Successfully Completed", Toast.LENGTH_LONG);
																							msg.show();
																							fullname=fName+lName;
																							System.out.println(fullname);
																							sendconfirmationmail();
																							String drfnln=firstName.getText().toString()+" "+lastName.getText().toString();



																							SharedPreferences sp=getActivity().getSharedPreferences("Login", 0);
																							SharedPreferences.Editor Ed=sp.edit();


																							Ed.putString("docterRegnumber", dregNo);
																							Ed.putString("Unm",eMail);              
																							Ed.putString("Psw",pass); 
																							Ed.putString("drFLname",drfnln);
																							Ed.putString("fName",firstName.getText().toString());
																							Ed.putString("lName",lastName.getText().toString());
																							Ed.putString("Accredi",accredition);
																							Ed.putString("cityName",cityname);
																							Ed.putString("Country",country);
																							Ed.putString("mailId",eMail);
																							Ed.putString("phNo",phone);
																							Ed.putString("pinCode",pinzip);
																							Ed.putString("qual",qualification);
																							Ed.putString("spel",specialization);
																							Ed.putString("State",stat);
																							Ed.putString("Street",strname);
																							Ed.putString("SuperSpl",supSpl);
																							
																							Ed.putFloat("header",180f);              
																							Ed.putFloat("footer",90f); 
																							Ed.putFloat("left",30f);
																							Ed.putFloat("right",120f);
																							Ed.putFloat("title", 8f);
																							Ed.putFloat("content", 8f);
																							
																							Ed.commit();

																							Intent i=new Intent(getActivity(),Home_Page_Activity.class);
																							startActivity(i);
																							getActivity().finish();

																						}


																					});

																					userMast.saveEventually();

																				}
																			}

																		}
																	});


																}
															} 
															else {
																Toast.makeText(getActivity(), "Sorry, something went wrong", Toast.LENGTH_LONG).show();
															}

														}
													});


												}
												else{
													Toast.makeText(getActivity(),"InValid pinzip",Toast.LENGTH_SHORT).show();
												}
											}
											else {
												Toast.makeText(getActivity(),"InValid phone",Toast.LENGTH_SHORT).show();
											}
										}
									}
									else{
										Toast.makeText(getActivity(),"Password must contain at least one uppercase,one lowercase,and one Digit",Toast.LENGTH_SHORT).show();
									}

								}
								else{
									Toast.makeText(getActivity(),"Password must be 8-14 characters",Toast.LENGTH_SHORT).show();
								}
							}
							else{
								Toast.makeText(getActivity(),"Invalid email address", Toast.LENGTH_SHORT).show();
							}
						}
						else{
							Toast msg=Toast.makeText(getActivity(), "Conformation Password/email does not match", Toast.LENGTH_SHORT);
							msg.show();
						}
					}
				}else{
					Toast.makeText(getActivity(), "Please check Terms and Conditions", Toast.LENGTH_LONG).show();
				}
			}
		});

		cancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				getActivity().finish();
			}
		}); 

		return myView;
	}

	protected void hideKeyboard(View view) {
		InputMethodManager in = (InputMethodManager) getActivity().getSystemService(getActivity().INPUT_METHOD_SERVICE);
		in.hideSoftInputFromWindow(view.getWindowToken(),
				InputMethodManager.HIDE_NOT_ALWAYS);
	}

	private void sendconfirmationmail()
	{
		// TODO Auto-generated method stub
		Map<String, String> params = new HashMap();
		params.put("email",email.getText().toString().trim());
		params.put("username",firstName.getText().toString()+" "+lastName.getText().toString());
		ParseCloud.callFunctionInBackground("mailConfirmationDD", params, new FunctionCallback<Object>() {
			@Override
			public void done(Object arg0, ParseException arg1) {
				// TODO Auto-generated method stub
				Log.e("send mail", "response: " + arg1);
			}
		});

	}


	private void fetechdiseasefromserver() {
		// TODO Auto-generated method stub
		ParseQuery<ParseObject> query = ParseQuery.getQuery("DiseaseDatabase");
		query.fromLocalDatastore();
		query.findInBackground(new FindCallback<ParseObject>() {

			@Override
			public void done(List<ParseObject> objects, ParseException arg1) {
				// TODO Auto-generated method stub

				localobjectsize=objects.size();
				if(objects.size()==0){
					System.out.println("localobjectsize"+localobjectsize);
					ParseQuery<ParseObject> query = ParseQuery.getQuery("DiseaseDatabase");
					if(localobjectsize!=0){
						query.fromLocalDatastore();
					}
					query.setLimit(1000);
					query.findInBackground(getAllObjects());
				}

			}
		});
	}

	private FindCallback getAllObjects() {
		// TODO Auto-generated method stub

		return new FindCallback<ParseObject>() {

			@Override
			public void done(List<ParseObject> objects, ParseException e) {
				// TODO Auto-generated method stub

				if (e == null) {

					diseaseobjects.addAll(objects);

					int limit =1000;
					if (objects.size() == limit){
						skip = skip + limit;
						ParseQuery query = new ParseQuery("DiseaseDatabase");
						if(localobjectsize!=0){
							query.fromLocalDatastore();
						}
						query.setSkip(skip);
						query.setLimit(limit);
						query.findInBackground(getAllObjects());
					}
					else {
						if(ParseObject.pinAllInBackground(diseaseobjects) != null){
							System.out.println("pin successs"+diseaseobjects.size());
						}

						ParseObject.pinAllInBackground(diseaseobjects);
					}


				}
			}
		};
	}

	private void fetchingSystemKeys(){

		ParseQuery<ParseObject> query = new ParseQuery<ParseObject>("SystemKeys");
		query.findInBackground(new FindCallback<ParseObject>() {
			@Override
			public void done(List<ParseObject> objects, ParseException e) {
				// TODO Auto-generated method stub
				//mProgressDialog.dismiss();
				Log.e("Login", "done");
				if(e==null){ 
					for (int i = 0; i < objects.size(); i++) {
						ParseObject object = objects.get(i);
						String key =  object.getString("key");

						String values=object.getString("value");
						String[] value = values.split(",");
						Log.e("parse value",String.valueOf(value[0]) );
						if(key.equals("accredition"))
						{

							if(value!=null){
								myaccrArray = new String[value.length];
								for(int j = 0; j < value.length; j++){    
									myaccrArray[j]=value[j];
									Log.v("result------value",  myaccrArray[j]);
								}
							}
						}

						if(key.equals("qualifications"))
						{

							if(value!=null){
								myqulrArray = new String[value.length];
								for(int j = 0; j < value.length; j++){    
									myqulrArray[j]=value[j];
									Log.v("result------value", myqulrArray[j]);
								}
								
								for(int k=0;k<myqulrArray.length;k++){
									Qualificationnameclass quantitynameObj=new Qualificationnameclass();
									quantitynameObj.setQualificationname(myqulrArray[k]);;
									quantitynamearraylist.add(quantitynameObj);
								}
							}
						}
						if(key.equals("specialization"))
						{
							if(value!=null){
								mysplrArray = new String[value.length];
								for(int j = 0; j < value.length; j++){    
									mysplrArray[j]=value[j];
									Log.v("result------value", mysplrArray[j]);
								}
							}  
						}

						else{
							Log.e("Login", "exception");
							//e.printStackTrace();
						}

					}

				}
			}});




	}

}

