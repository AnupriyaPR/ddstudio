package com.wiinnova.doctorsdiary.fragment;


import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wiinnova.doctorsdiary.R;

public class Assign_drugs_main extends Fragment implements OnClickListener {
	
	//LinearLayout
	private LinearLayout llSearch_Assigned_Drugs;
	private LinearLayout llAdd_NewDrug_Database;
	private LinearLayout llAdd_NewDrug_NDatabase;
	private LinearLayout llDelete_Drug;
	private LinearLayout llSearch_Drug_List;
	
	//TextView
	private TextView tvSearch_Assigned_Drugs;
	private TextView tvAdd_NewDrug_Database;
	private TextView tvAdd_NewDrug_NDatabase;
	private TextView tvDelete_Drug;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view=inflater.inflate(R.layout.assinfrequent_drugs, null);
		Initialize_Components(view);
		setup_Listeners();
		return view;
	}
	private void setup_Listeners() {
		// TODO Auto-generated method stub
		llSearch_Assigned_Drugs.setOnClickListener(this);
		llAdd_NewDrug_Database.setOnClickListener(this);
		llAdd_NewDrug_NDatabase.setOnClickListener(this);
		llDelete_Drug.setOnClickListener(this);
	}
	private void Initialize_Components(View view) {
		// TODO Auto-generated method stub
		//LinearLayout
		llSearch_Assigned_Drugs=(LinearLayout)view.findViewById(R.id.llsearch_drugs);
		llAdd_NewDrug_Database=(LinearLayout)view.findViewById(R.id.lladd_drug_database);
		llAdd_NewDrug_NDatabase=(LinearLayout)view.findViewById(R.id.lladd_drug_not_database);
		llDelete_Drug=(LinearLayout)view.findViewById(R.id.lldelete_drug);
		llSearch_Drug_List=(LinearLayout)view.findViewById(R.id.llsearch_drugs_list);
		
		//TextView
		tvSearch_Assigned_Drugs=(TextView)view.findViewById(R.id.tvsearch_drugs);
		tvAdd_NewDrug_Database=(TextView)view.findViewById(R.id.tvadd_drug_database);
		tvAdd_NewDrug_NDatabase=(TextView)view.findViewById(R.id.tvadd_drug_not_database);
		tvDelete_Drug=(TextView)view.findViewById(R.id.tvdelete_drug);
	}
	@SuppressLint("ResourceAsColor")
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.llsearch_drugs:

			tvSearch_Assigned_Drugs.setTextColor(getResources().getColor(R.color.actionbar_color));
			tvAdd_NewDrug_Database.setTextColor(getResources().getColor(R.color.black));
			tvAdd_NewDrug_NDatabase.setTextColor(getResources().getColor(R.color.black));
			tvDelete_Drug.setTextColor(getResources().getColor(R.color.black));
		
			llSearch_Assigned_Drugs.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
			llAdd_NewDrug_Database.setBackgroundColor(getResources().getColor(R.color.actionbar_color));
			llAdd_NewDrug_NDatabase.setBackgroundColor(getResources().getColor(R.color.actionbar_color));
			llDelete_Drug.setBackgroundColor(getResources().getColor(R.color.actionbar_color));

			llSearch_Drug_List.setVisibility(v.VISIBLE);

			break;

		case R.id.lladd_drug_database:
			
			tvSearch_Assigned_Drugs.setTextColor(getResources().getColor(R.color.black));
			tvAdd_NewDrug_Database.setTextColor(getResources().getColor(R.color.actionbar_color));
			tvAdd_NewDrug_NDatabase.setTextColor(getResources().getColor(R.color.black));
			tvDelete_Drug.setTextColor(getResources().getColor(R.color.black));
		
			
			llSearch_Assigned_Drugs.setBackgroundColor(getResources().getColor(R.color.actionbar_color));
			llAdd_NewDrug_Database.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
			llAdd_NewDrug_NDatabase.setBackgroundColor(getResources().getColor(R.color.actionbar_color));
			llDelete_Drug.setBackgroundColor(getResources().getColor(R.color.actionbar_color));

			llSearch_Drug_List.setVisibility(v.INVISIBLE);
			break;

		case R.id.lladd_drug_not_database:
			
			tvSearch_Assigned_Drugs.setTextColor(getResources().getColor(R.color.black));
			tvAdd_NewDrug_Database.setTextColor(getResources().getColor(R.color.black));
			tvAdd_NewDrug_NDatabase.setTextColor(getResources().getColor(R.color.actionbar_color));
			tvDelete_Drug.setTextColor(getResources().getColor(R.color.black));
		
			llSearch_Assigned_Drugs.setBackgroundColor(getResources().getColor(R.color.actionbar_color));
			llAdd_NewDrug_Database.setBackgroundColor(getResources().getColor(R.color.actionbar_color));
			llAdd_NewDrug_NDatabase.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
			llDelete_Drug.setBackgroundColor(getResources().getColor(R.color.actionbar_color));

			llSearch_Drug_List.setVisibility(v.INVISIBLE);


			break;

		case R.id.lldelete_drug:
			
			tvSearch_Assigned_Drugs.setTextColor(getResources().getColor(R.color.black));
			tvAdd_NewDrug_Database.setTextColor(getResources().getColor(R.color.black));
			tvAdd_NewDrug_NDatabase.setTextColor(getResources().getColor(R.color.black));
			tvDelete_Drug.setTextColor(getResources().getColor(R.color.actionbar_color));
		
			llSearch_Assigned_Drugs.setBackgroundColor(getResources().getColor(R.color.actionbar_color));
			llAdd_NewDrug_Database.setBackgroundColor(getResources().getColor(R.color.actionbar_color));
			llAdd_NewDrug_NDatabase.setBackgroundColor(getResources().getColor(R.color.actionbar_color));
			llDelete_Drug.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));

			llSearch_Drug_List.setVisibility(v.INVISIBLE);

			break;

		}
	}
	
	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		CrashReporter.getInstance().trackScreenView("Appointmentn Schedule");
	}


}
