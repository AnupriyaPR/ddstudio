package com.wiinnova.doctorsdiary.fragment;

import android.app.Fragment;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.wiinnova.doctorsdiary.R;
import com.parse.ParseObject;
import com.wiinnova.doctorsdiary.activities.FragmentActivityContainer;
import com.wiinnova.doctorsdiary.supportclasses.SearchOldRecordOtherDetailsAdapter;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

public class SearchPatientresult extends Fragment {

	private TextView date;
	private TextView doctername;
	private ListView listsearchresult;
	private Button btnSearch;
	List<ParseObject> results;
	SearchOldRecordOtherDetailsAdapter adapter;
	private DD_Frag ddfirst;

	public interface onSearchSubmit{

		void onSearch(ParseObject obj);
	}


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View v=inflater.inflate(R.layout.searchresults_layout_fragment, null);

		ddfirst = new  DD_Frag();
		Bundle bundle=new Bundle();
		bundle.putInt("sidemenuitem", 3);
		ddfirst.setArguments(bundle);
		getFragmentManager().beginTransaction()
		.replace(R.id.fl_sidemenu_container, ddfirst)
		.commit();

		if(((FragmentActivityContainer)getActivity()).prescriptiontabvisibility()==false && ((FragmentActivityContainer)getActivity()).diagnosistabvisibility()==false)
			((FragmentActivityContainer)getActivity()).SetpatienttabInvisibe();




		btnSearch=(Button)v.findViewById(R.id.btsearch);
		date=(TextView)v.findViewById(R.id.tvdate);
		doctername=(TextView)v.findViewById(R.id.tvdrname);
		listsearchresult=(ListView)v.findViewById(R.id.search_old_record_result_item_list);

		View headerView = ((LayoutInflater)getActivity().getSystemService(getActivity().LAYOUT_INFLATER_SERVICE)).inflate(R.layout.search_result_other_details_list_header, null, false);
		listsearchresult.addHeaderView(headerView);	

		SharedPreferences docter_name=getActivity().getSharedPreferences("Login", 0);
		String docter_first_name=docter_name.getString("fName", null);  
		String docter_last_name=docter_name.getString("lName", null);  

		doctername.setText(docter_first_name+" "+docter_last_name);


		Calendar c = Calendar.getInstance();
		System.out.println("Current time => " + c.getTime());
		SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
		String formattedDate = df.format(c.getTime());
		date.setText(formattedDate);



		results=((FragmentActivityContainer)getActivity()).getPatientserachresultObj();
		adapter=new SearchOldRecordOtherDetailsAdapter(getActivity(), results);

		listsearchresult.setAdapter(adapter);

		listsearchresult.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				adapter.setSelected(arg2-1);

			}
		});


		btnSearch.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub


				if (adapter.getSelected() > -1){

					((FragmentActivityContainer)getActivity()).setOldPatientobject(adapter.getItem(adapter.getSelected()));
					((FragmentActivityContainer)getActivity()).setpatientflagvalue(false);
					((FragmentActivityContainer)getActivity()).setcheckadmintab(true);
					((FragmentActivityContainer)getActivity()).patienttabloaddefault();
					
				}
				else
					Toast.makeText(getActivity(), "Please select a record", Toast.LENGTH_LONG).show();

			}
		});
		return v;
	}
	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		
		CrashReporter.getInstance().trackScreenView("Search Patient Page");
		((FragmentActivityContainer)getActivity()).setcheckadmintab(false);
	}
	
	
		
}
