package com.wiinnova.doctorsdiary.fragment;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.print.PrintManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.wiinnova.doctorsdiary.R;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.wiinnova.doctorsdiary.activities.FragmentActivityContainer;
import com.wiinnova.doctorsdiary.activities.Home_Page_Activity;
import com.wiinnova.doctorsdiary.fragment.Treatment_Frag.onPriscriptionviewprint;
import com.wiinnova.doctorsdiary.supportclasses.AppUtil;
import com.wiinnova.doctorsdiary.supportclasses.Diseasenameclass;
import com.wiinnova.doctorsdiary.supportclasses.FileOperationsGynacologist;
import com.wiinnova.doctorsdiary.supportclasses.FileOperationsNew;
import com.wiinnova.doctorsdiary.supportclasses.PrintHelper;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class Treatment_View_Frag extends Fragment implements onPriscriptionviewprint {


    ArrayList<Diseasenameclass> diseasenamearraylist = new ArrayList<Diseasenameclass>();

    ArrayList<String> treatmenttext = new ArrayList<String>();
    ArrayList<String> followuptext = new ArrayList<String>();


    Button vtEdit, vtmSave, vprintPri;
    TextView vstartDate, vendDate, vfllupDate, vDate, vDrag, vDosage, vQnty, vAddinfo, tvTreatment, tvFollowup;
    TextView flvtmName, vuniqID;
    String vpuid, flname;
    int _lastOpenPrescription;
    String selecteddate;

    int position = -1;


    List<ParseObject> prescriptionObject = new ArrayList<ParseObject>();
    ProgressDialog mProgressDialog;
    LinearLayout layout;
    LinearLayout layout_inflater;

    TextView uniqueid;
    TextView tvfirstlastName;
    TextView drug;
    TextView dosage;
    TextView date;
    TextView duration;
    TextView quantity;


    TextView tvdrug;
    LinearLayout lldosage;
    TextView tvstartdate;
    LinearLayout llduration;
    TextView tvdaily;
    TextView tvdays;
    TextView tvquality;

    String[] days;
    private Treatment_Frag treatementfrag;

    JSONArray drugObj;
    JSONArray dosageObj;
    JSONArray dateObj;
    JSONArray durationObj;
    JSONArray QuantityObj;
    JSONArray daysObj;

    String treatment;
    String followup;
    String formattedDate;

    ParseObject patientcurrentvitalsdetails;

    List<LinearLayout> selectedprescription = new ArrayList<LinearLayout>();
    List<ImageView> selectedarrow = new ArrayList<ImageView>();
    List<TextView> selecteddatelist = new ArrayList<TextView>();
    List<Integer> selectedrow = new ArrayList<Integer>();
    private String doctername;
    String patientname;

    List<ParseObject> _suspecteddiseasenamesObj;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.treatment_view_frag, null);


        tvdrug = (TextView) view.findViewById(R.id.tvdrug);
        lldosage = (LinearLayout) view.findViewById(R.id.lldosage);
        tvstartdate = (TextView) view.findViewById(R.id.tvstartdate);
        llduration = (LinearLayout) view.findViewById(R.id.llduration);
        /*tvdaily=(TextView)view.findViewById(R.id.tvdaily);*/
        tvdays = (TextView) view.findViewById(R.id.tvdays);
        tvquality = (TextView) view.findViewById(R.id.tvquality);
        uniqueid = (TextView) view.findViewById(R.id.cduniqueid);
        tvfirstlastName = (TextView) view.findViewById(R.id.tvfirstlastName);

        layout = (LinearLayout) view.findViewById(R.id.trdynamic);


        _suspecteddiseasenamesObj = ((CrashReporter) getActivity().getApplicationContext()).getDiseasenameObjects();

        FragmentActivityContainer.treatmangement_save_check = 0;

//		((FragmentActivityContainer)getActivity()).gettreatmentfrag().onTreatmentmanagementbtn(0);


        if (_suspecteddiseasenamesObj != null) {

            diseasenamearraylist.clear();

            for (int i = 0; i < _suspecteddiseasenamesObj.size(); i++) {
                Diseasenameclass diseasenameObj = new Diseasenameclass();
                System.out.println("working..." + _suspecteddiseasenamesObj.get(i).getString("DiseaseName"));
                diseasenameObj.setDiseasename(_suspecteddiseasenamesObj.get(i).getString("DiseaseName"));
                diseasenameObj.setIcdnumber(_suspecteddiseasenamesObj.get(i).getString("ICDNumber"));
                diseasenamearraylist.add(diseasenameObj);
            }
        }


		/*	if(getFragmentManager().findFragmentById(R.id.fl_sidemenu_container) instanceof Treatment_Frag){

		}else{
			treatementfrag = new  Treatment_Frag();
			getFragmentManager().beginTransaction()
			.replace(R.id.fl_sidemenu_container, treatementfrag)
			.commit();
		}
		if(treatementfrag!=null){
			((FragmentActivityContainer)getActivity()).settreatmentfrag(treatementfrag);
		}*/

        ((FragmentActivityContainer) getActivity()).settreatmentviewfrag(this);


        if (Home_Page_Activity.patient != null) {

            vpuid = Home_Page_Activity.patient.getString("patientID");
            String fname = Home_Page_Activity.patient.getString("firstName");
            String lname = Home_Page_Activity.patient.getString("lastName");

            if (fname == null) {
                fname = "FirstName";
            }
            if (lname == null) {
                lname = "LastName";
            }
            tvfirstlastName.setText(fname + " " + lname);

        } else {
            SharedPreferences scanpidnew = getActivity().getSharedPreferences("NewPatID", 0);
            String scnewPid = scanpidnew.getString("NewID", null);
            SharedPreferences scanpid1 = getActivity().getSharedPreferences("ScannedPid", 0);
            String scPid = scanpid1.getString("scanpid", null);
            if (scPid != null) {

                vpuid = scPid;
                SharedPreferences fnname1 = getActivity().getSharedPreferences("fnName", 0);
                tvfirstlastName.setText(fnname1.getString("fNname", "FirstName" + " " + "LastName"));
            }
            if (scnewPid != null) {
                vpuid = scnewPid;
                SharedPreferences fnname1 = getActivity().getSharedPreferences("fnName", 0);
                tvfirstlastName.setText(fnname1.getString("fNname", "FirstName" + " " + "LastName"));
            }
        }

        uniqueid.setText(vpuid);

        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setCancelable(false);
        mProgressDialog.setMessage("Retrieving Prescription Data....");

        final ParseQuery<ParseObject> query = ParseQuery.getQuery("Diagnosis");
        mProgressDialog.show();
        if (!AppUtil.haveNetwokConnection(getActivity()))
            query.fromLocalDatastore();
        query.whereEqualTo("patientID", vpuid);
        query.addDescendingOrder("createdAt");

        mProgressDialog.setOnCancelListener(new OnCancelListener() {

            @Override
            public void onCancel(DialogInterface arg0) {
                // TODO Auto-generated method stub
                query.cancel();
            }
        });

        SharedPreferences docter_name = getActivity().getSharedPreferences("Login", 0);
        String docter_first_name = docter_name.getString("fName", null);
        String docter_last_name = docter_name.getString("lName", null);

        doctername = docter_first_name + " " + docter_last_name;
        patientname = tvfirstlastName.getText().toString();

        query.findInBackground(new FindCallback<ParseObject>() {


            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                // TODO Auto-generated method stub
                if (Treatment_View_Frag.this.isVisible()) {
                    Log.e("Login", "done");

                    if (e == null) {
                        int index = 0;

                        prescriptionObject = objects;
                        treatmenttext.clear();
                        followuptext.clear();
                        selectedrow.clear();
                        selecteddatelist.clear();
                        for (int i = 0; i < objects.size(); i++) {

                            ParseObject object = objects.get(i);
                            double type = object.getDouble("typeFlag");

                            LayoutInflater inflater = LayoutInflater.from(getActivity());
                            if (inflater != null) {

                                View patient_treatment_details = inflater.inflate(R.layout.treatment_view_dynamic_new_first, null);
                                if (type == 2) {

                                    drugObj = object.getJSONArray("drug");
                                    dosageObj = object.getJSONArray("dosage");
                                    dateObj = object.getJSONArray("drugStartDate");
                                    durationObj = object.getJSONArray("duration");
                                    QuantityObj = object.getJSONArray("quantity");
                                    daysObj = object.getJSONArray("days");

                                    System.out.println("days" + daysObj);
                                    System.out.println("jsonObject" + drugObj);

                                    treatment = object.getString("treatment");
                                    followup = object.getString("followup");
                                    formattedDate = object.getString("createdDate");

                                    treatmenttext.add(treatment);
                                    followuptext.add(followup);

									/*Date create=object.getCreatedAt();*/


                                    layout_inflater = (LinearLayout) patient_treatment_details.findViewById(R.id.ctml4);
                                    vstartDate = (TextView) patient_treatment_details.findViewById(R.id.ctmcurrdate);
                                    tvTreatment = (TextView) patient_treatment_details.findViewById(R.id.etTreatment);
                                    tvFollowup = (TextView) patient_treatment_details.findViewById(R.id.tvfollowup);
                                    final LinearLayout _datecontainer = (LinearLayout) patient_treatment_details.findViewById(R.id.lldatecontainer);
                                    final LinearLayout detailscontainer = (LinearLayout) patient_treatment_details.findViewById(R.id.lldetailsContainer);
                                    final ImageView arrow = (ImageView) patient_treatment_details.findViewById(R.id.ivarrow);
                                    arrow.setBackgroundResource(R.drawable.triangle_arrow);

                                    arrow.setTag(i);
                                    _datecontainer.setTag(index);
                                    index++;
                                    System.out.println("arrow tag" + i);
                                    selecteddatelist.add(vstartDate);
                                    selectedrow.add(i);

                                    _datecontainer.setOnClickListener(new OnClickListener() {

                                        @Override
                                        public void onClick(View arg0) {
                                            // TODO Auto-generated method stub


                                            if (detailscontainer.getVisibility() == View.VISIBLE) {
                                                detailscontainer.setVisibility(View.GONE);
                                                arrow.setBackgroundResource(R.drawable.triangle_arrow);


                                            } else {

                                                for (int i = 0; i < selectedprescription.size(); i++) {

                                                    if (selectedprescription.get(i).getVisibility() == View.VISIBLE) {
                                                        selectedprescription.get(i).setVisibility(View.GONE);
                                                        selectedarrow.get(i).setBackgroundResource(R.drawable.triangle_arrow);
                                                    }
                                                }

                                                arrow.setBackgroundResource(R.drawable.triangle_up);
                                                detailscontainer.setVisibility(View.VISIBLE);

                                                selectedprescription.add(detailscontainer);

                                                selectedarrow.add(arrow);
                                                _lastOpenPrescription = (Integer) arrow.getTag();
                                                System.out
                                                        .println("selected row" + _lastOpenPrescription);

                                                //	selecteddate=vstartDate.getText().toString();
                                                selecteddate = selecteddatelist.get((Integer) _datecontainer.getTag()).getText().toString();

                                                _lastOpenPrescription = selectedrow.get((Integer) _datecontainer.getTag());
                                                System.out
                                                        .println("selected another row" + _lastOpenPrescription);


                                            }


                                        }
                                    });


                                    tvTreatment.setText(treatment);
                                    tvFollowup.setText(followup);

									/*Calendar c = Calendar.getInstance();
                            System.out.println("Current time => " + c.getTime());
							c.setTime(create);
							SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
							String formattedDate = df.format(c.getTime());*/
                                    vstartDate.setText(formattedDate);

                                    if (drugObj != null) {
                                        for (int j = 0; j < drugObj.length(); j++) {
                                            LayoutInflater inflater1 = LayoutInflater.from(getActivity());

                                            if (inflater1 != null) {
                                                View patient_treatment_details1 = inflater1.inflate(R.layout.treatment_view_dynamic_new_second, null);

                                                drug = (TextView) patient_treatment_details1.findViewById(R.id.ctmdrugedit71);
                                                dosage = (TextView) patient_treatment_details1.findViewById(R.id.ctmdrugdosage71);
                                                date = (TextView) patient_treatment_details1.findViewById(R.id.ctmdrugdate71);
                                                duration = (TextView) patient_treatment_details1.findViewById(R.id.ctmdrugduration71);
                                                quantity = (TextView) patient_treatment_details1.findViewById(R.id.ctmdrugquantity71);
                                                final ImageView iv_monday = (ImageView) patient_treatment_details1.findViewById(R.id.days_monday);
                                                final ImageView iv_tuesday = (ImageView) patient_treatment_details1.findViewById(R.id.days_tuesday);
                                                final ImageView iv_wed = (ImageView) patient_treatment_details1.findViewById(R.id.days_wed);
                                                final ImageView iv_thu = (ImageView) patient_treatment_details1.findViewById(R.id.days_thu);
                                                final ImageView iv_fri = (ImageView) patient_treatment_details1.findViewById(R.id.days_fri);
                                                final ImageView iv_sat = (ImageView) patient_treatment_details1.findViewById(R.id.days_sat);
                                                final ImageView iv_sun = (ImageView) patient_treatment_details1.findViewById(R.id.days_sun);
                                                //final CheckBox	daily=(CheckBox)patient_treatment_details1.findViewById(R.id.cbDaily);

                                                drug.setEnabled(false);
                                                dosage.setEnabled(false);
                                                date.setEnabled(false);
                                                duration.setEnabled(false);
                                                quantity.setEnabled(false);

                                                //daily.setEnabled(false);

                                                try {
                                                    drug.setText(drugObj.getString(j));
                                                    dosage.setText(dosageObj.getString(j));
                                                    date.setText(dateObj.getString(j));
                                                    duration.setText(durationObj.getString(j));

                                                    String drug_Quantity = QuantityObj.getString(j).replaceAll("\\(.*?\\)", "");
                                                    quantity.setText(drug_Quantity);

                                                    if (daysObj != null) {
                                                        if (daysObj.length() != 0) {

                                                            if (daysObj.getString(j).equals("ALL")) {
                                                                System.out.println("workinggggggggggg");
                                                                //daily.setChecked(true);
                                                                iv_monday.setImageResource(R.drawable.days_monday_2);
                                                                iv_tuesday.setImageResource(R.drawable.days_tuesday_2);
                                                                iv_wed.setImageResource(R.drawable.days_wed_2);
                                                                iv_thu.setImageResource(R.drawable.days_thu_2);
                                                                iv_fri.setImageResource(R.drawable.days_fri_2);
                                                                iv_sat.setImageResource(R.drawable.days_sat_2);
                                                                iv_sun.setImageResource(R.drawable.days_sun_2);

                                                            } else {
                                                                String var = daysObj.getString(j);
                                                                System.out.println("variableeeee" + var);
                                                                System.out.println("length" + var.length());
                                                                //days=new String[var.length()];
                                                                days = var.split(",");
                                                                for (int k = 0; k < days.length; k++) {

                                                                    System.out.println("days" + days);
                                                                    System.out.println("days valueeeeeeeeee" + days[k]);

                                                                    if (days[k].equals("Mo")) {
                                                                        iv_monday.setImageResource(R.drawable.days_monday_2);
                                                                    }
                                                                    if (days[k].equals("Tu")) {
                                                                        iv_tuesday.setImageResource(R.drawable.days_tuesday_2);
                                                                    }
                                                                    if (days[k].equals("We")) {
                                                                        iv_wed.setImageResource(R.drawable.days_wed_2);
                                                                    }
                                                                    if (days[k].equals("Th")) {
                                                                        iv_thu.setImageResource(R.drawable.days_thu_2);
                                                                    }
                                                                    if (days[k].equals("Fr")) {
                                                                        iv_fri.setImageResource(R.drawable.days_fri_2);
                                                                    }
                                                                    if (days[k].equals("Sa")) {
                                                                        iv_sat.setImageResource(R.drawable.days_sat_2);
                                                                    }
                                                                    if (days[k].equals("Su")) {
                                                                        iv_sun.setImageResource(R.drawable.days_sun_2);
                                                                    }
                                                                }


                                                            }
                                                        } else {
//															tvdrug.setVisibility(View.GONE);
//															lldosage.setVisibility(View.GONE);
//															tvstartdate.setVisibility(View.GONE);
//															llduration.setVisibility(View.GONE);
//															tvdaily.setVisibility(View.GONE);
//															tvdays.setVisibility(View.GONE);
//															tvquality.setVisibility(View.GONE);

                                                        }
                                                    }


                                                } catch (JSONException e1) {
                                                    // TODO Auto-generated catch block
                                                    e1.printStackTrace();
                                                }


												/*if(drugObj!=null && dosageObj!=null && dateObj!=null && durationObj!=null && QuantityObj!=null && daysObj!=null && treatment.length()!=0 && followup.length()!=0){

													if(drugObj.length()!=0 && dosageObj.length()!=0 && dateObj.length()!=0 && durationObj.length()!=0 && QuantityObj.length()!=0 && daysObj.length()!=0 && treatment.length()!=0 && followup.length()!=0){
												 */
                                                layout_inflater.addView(patient_treatment_details1);
                                                /*}
                                                }*/


                                            }
                                        }
                                    }


                                    if (drugObj != null || treatment != null || followup != null) {

                                        if (drugObj.length() != 0 || treatment.length() != 0 || followup.length() != 0) {

                                            layout.addView(patient_treatment_details);
                                        }
                                    }


                                    //Treatment_Main_Frag.treat_save_check=0;

                                }

                            }
                        }
                    } else {
                        Toast.makeText(getActivity(), "Invalid Patient", Toast.LENGTH_SHORT).show();
                    }
                }

                mProgressDialog.dismiss();
            }
        });
        return view;
    }

    @Override
    public void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
        CrashReporter.getInstance().trackScreenView("Patient Treatment View");
    }

    @Override
    public void onviewPrescription() {
        // TODO Auto-generated method stub


        if (prescriptionObject.size() > 0) {

            SharedPreferences sp = getActivity().getSharedPreferences("Login", 0);
            String specialization = sp.getString("spel", null);
            if (!specialization.equalsIgnoreCase("gynecologist obstetrician")) {
                getpatientcurrentvitals(_lastOpenPrescription);
            } else {
                getpatientdetailsgynacologist(_lastOpenPrescription);
            }
        } else
            Toast.makeText(getActivity(), "Please create prescription before print", Toast.LENGTH_LONG).show();
    }

    private void getpatientcurrentvitals(final int lastOpenPrescription) {

        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setCancelable(false);
        mProgressDialog.setMessage("Retrieving Patient Data...");
        mProgressDialog.show();

        ParseQuery<ParseObject> query = ParseQuery.getQuery("Patients");
        if (!AppUtil.haveNetwokConnection(getActivity()))
            query.fromLocalDatastore();
        query.whereEqualTo("patientID", vpuid);
        query.findInBackground(new FindCallback<ParseObject>() {

            @Override
            public void done(List<ParseObject> object, ParseException e) {
                // TODO Auto-generated method stub
                if (e == null) {

                    if (object.size() > 0) {

                        System.out.println("object grater than zero");
                        patientcurrentvitalsdetails = object.get(0);
                        getpatientdetails(lastOpenPrescription);

                    } else {

                        getpatientdetails(lastOpenPrescription);
                    }
                }

                mProgressDialog.dismiss();
            }

        });
    }

    private void getpatientdetails(final int lastOpenPrescription) {
        // TODO Auto-generated method stub

        final ParseQuery<ParseObject> query = ParseQuery.getQuery("Diagnosis");
        if (!AppUtil.haveNetwokConnection(getActivity()))
            query.fromLocalDatastore();
        query.whereEqualTo("patientID", vpuid);
        query.whereEqualTo("createdDate", selecteddate);
        /*	query.addDescendingOrder("createdatetime");*/

        query.findInBackground(new FindCallback<ParseObject>() {

            int diagnosisindex;

            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                // TODO Auto-generated method stub


                //Toast.makeText(getActivity(),"objects size"+objects.size(), Toast.LENGTH_LONG).show();

                if (objects.size() > 0) {
                    if (e == null) {

                        Toast.makeText(getActivity(), "date" + selecteddate, Toast.LENGTH_LONG).show();

                        ArrayList arrayList1 = new ArrayList();

                        for (int i = 0; i < objects.size(); i++) {
                            if (objects.get(i).getDouble("typeFlag") == 1) {
                                arrayList1.add(objects.get(i).getNumber("createdatetime"));
                            }
                        }

                        Long val1 = (long) 0;
                        if (arrayList1.size() != 0) {
                            val1 = Long.parseLong(Collections.max(arrayList1).toString());
                        }
                        for (int j = 0; j < objects.size(); j++) {
                            if (objects.get(j).getNumber("createdatetime") == val1) {
                                diagnosisindex = j;
                            }
                        }


                        System.out.println("diagnosisindex grater than zero");

                        final ParseObject combinedobject = new ParseObject("");
                        combinedobject.put("createdAt", formattedDate);

                        System.out.println("diagnosisindex grater than zero");

                        if (objects.get(diagnosisindex).getDouble("typeFlag") == 1) {

                            System.out.println("type 1 working");

                            if (objects.get(diagnosisindex).getString("additionalComments") != null) {
                                combinedobject.put("additionalComments", objects.get(diagnosisindex).getString("additionalComments"));
                            }

                            if (objects.get(diagnosisindex).getString("suspectedDisease") != null) {
                                //	combinedobject.put("suspectedDisease",objects.get(diagnosisindex).getString("suspectedDisease"));
                                combinedobject.put("diagnosisObjId", objects.get(diagnosisindex).getString("diagnosisId"));

                                String[] value1 = (objects.get(diagnosisindex).getString("suspectedDisease").split(","));
                                ArrayList<String> diseasenamecheck = new ArrayList<String>();

                                for (int val = 0; val < value1.length; val++) {

                                    diseasenamecheck.add(value1[val]);

                                }

                                if (_suspecteddiseasenamesObj.size() != 0 && _suspecteddiseasenamesObj != null) {
                                    //compairing_code_taken_disease_fromparse(objects.get(diagnosisindex).getString("suspectedDisease"));

                                    String[] value = objects.get(diagnosisindex).getString("suspectedDisease").split(",");
                                    if (value != null)
                                        for (int k = 0; k < value.length; k++) {

                                            System.out.println("code valuessss from parse" + value[k]);
                                        }
                                    String[] myaccrArray = null;
                                    if (value != null) {
                                        myaccrArray = new String[value.length];
                                        for (int j = 0; j < value.length; j++) {
                                            for (int k = 0; k < diseasenamearraylist.size(); k++) {
                                                if ((diseasenamearraylist.get(k).getIcdnumber()) != null) {
                                                    String icd_from_list = diseasenamearraylist.get(k).getIcdnumber().toString().trim();
                                                    String icd_from_selected = value[j].toString().trim();
                                                    if (icd_from_list.equalsIgnoreCase(icd_from_selected)) {
                                                        myaccrArray[j] = diseasenamearraylist.get(k).getDiseasename();
                                                    }
                                                }
                                            }
                                            String disease = "";
                                            for (int i = 0; i < myaccrArray.length; i++) {
                                                disease += myaccrArray[i] + "\n";
                                            }

                                            //ettext.setText(disease);
                                            combinedobject.put("suspectedDisease", disease);
                                        }
                                    }


                                } else {


                                    final ProgressDialog mProgress = new ProgressDialog(getActivity());
                                    mProgress.setMessage("Fetching Details.....");
                                    mProgress.show();
                                    ParseQuery<ParseObject> query = ParseQuery.getQuery("DiseaseDatabase");
                                    if (!AppUtil.haveNetwokConnection(getActivity()))
                                        query.fromLocalDatastore();
                                    query.whereContainedIn("ICDNumber", diseasenamecheck);
                                    query.findInBackground(new FindCallback<ParseObject>() {

                                        @Override
                                        public void done(List<ParseObject> object, ParseException e) {
                                            // TODO Auto-generated method stub
                                            if (e == null) {
                                                if (object.size() > 0) {
                                                    String disease = "";
                                                    for (int diseasesize = 0; diseasesize < object.size(); diseasesize++) {
                                                        System.out.println("names disease" + object.get(diseasesize).getString("DiseaseName"));
                                                        disease += object.get(diseasesize).getString("DiseaseName") + "\n";
                                                    }
                                                    combinedobject.put("suspectedDisease", disease);
                                                }
                                            }

                                            mProgress.dismiss();
                                        }
                                    });
                                }
                            } else {
                                combinedobject.put("suspectedDisease", "Nil");
                            }

                            if (objects.get(diagnosisindex).getString("symptoms") != null) {
                                combinedobject.put("symptoms", objects.get(diagnosisindex).getString("symptoms"));
                            }
                            if (objects.get(diagnosisindex).getString("syndromes") != null) {
                                combinedobject.put("syndromes", objects.get(diagnosisindex).getString("syndromes"));
                            }
                        }
                        combinedobject.put("treatment", prescriptionObject.get(lastOpenPrescription).getString("treatment"));
                        combinedobject.put("followup", prescriptionObject.get(lastOpenPrescription).getString("followup"));

                        if (prescriptionObject.get(lastOpenPrescription).getJSONArray("days") != null) {
                            System.out.println("lastopen prescription" + lastOpenPrescription);

                            System.out.println("days" + prescriptionObject.get(lastOpenPrescription).getJSONArray("days"));
                            combinedobject.put("days", prescriptionObject.get(lastOpenPrescription).getJSONArray("days"));
                        }
                        if (prescriptionObject.get(lastOpenPrescription).getJSONArray("drugStartDate") != null) {
                            System.out.println("drugStartDate" + prescriptionObject.get(lastOpenPrescription).getJSONArray("drugStartDate"));
                            combinedobject.put("drugStartDate", prescriptionObject.get(lastOpenPrescription).getJSONArray("drugStartDate"));
                        }
                        if (prescriptionObject.get(lastOpenPrescription).getJSONArray("dosage") != null) {
                            combinedobject.put("dosage", prescriptionObject.get(lastOpenPrescription).getJSONArray("dosage"));
                        }
                        if (prescriptionObject.get(lastOpenPrescription).getJSONArray("drug") != null) {
                            System.out.println("drug name" + prescriptionObject.get(lastOpenPrescription).getJSONArray("drug"));
                            combinedobject.put("drug", prescriptionObject.get(lastOpenPrescription).getJSONArray("drug"));
                        }
                        if (prescriptionObject.get(lastOpenPrescription).getJSONArray("duration") != null) {
                            combinedobject.put("duration", prescriptionObject.get(lastOpenPrescription).getJSONArray("duration"));
                        }
                        if (prescriptionObject.get(lastOpenPrescription).getJSONArray("quantity") != null) {
                            combinedobject.put("quantity", prescriptionObject.get(lastOpenPrescription).getJSONArray("quantity"));
                        }
						/*
					if(treatment!=null){
						combinedobject.put("treatment",treatment);
					}else{*/
						/*combinedobject.put("treatment",tvTreatment.getText().toString());*/
						/*}*/

						/*if(followup!=null){
						combinedobject.put("followup",followup);
					}else{*/
						/*combinedobject.put("followup",tvFollowup.getText().toString());*/
						/*}*/

                        if (doctername != null) {
                            combinedobject.put("doctername", doctername);
                        }

                        if (patientname != null) {
                            combinedobject.put("patientname", patientname);
                        }

                        combinedobject.put("id", uniqueid.getText().toString());


						/*tvTreatment.setText(treatment);
					tvFollowup.setText(followup);*/


                        JSONArray weight_result = new JSONArray();
                        JSONArray spo2_result = new JSONArray();
                        JSONArray pulse_result = new JSONArray();
                        JSONArray respRate_result = new JSONArray();
                        JSONArray height_result = new JSONArray();
                        JSONArray bloodGroup_result = new JSONArray();
                        JSONArray bloodPressure_result = new JSONArray();
                        JSONArray temperature = new JSONArray();

                        if (patientcurrentvitalsdetails != null) {

                            if (patientcurrentvitalsdetails.getJSONArray("weight") != null) {
                                weight_result = patientcurrentvitalsdetails.getJSONArray("weight");
                            }
                            if (patientcurrentvitalsdetails.getJSONArray("spo2") != null) {
                                spo2_result = patientcurrentvitalsdetails.getJSONArray("spo2");
                            }
                            if (patientcurrentvitalsdetails.getJSONArray("pulse") != null) {
                                pulse_result = patientcurrentvitalsdetails.getJSONArray("pulse");
                            }
                            if (patientcurrentvitalsdetails.getJSONArray("respRate") != null) {
                                respRate_result = patientcurrentvitalsdetails.getJSONArray("respRate");
                            }
                            if (patientcurrentvitalsdetails.getJSONArray("height") != null) {
                                height_result = patientcurrentvitalsdetails.getJSONArray("height");
                            }
                            if (patientcurrentvitalsdetails.getJSONArray("bloodGroup") != null) {
                                bloodGroup_result = patientcurrentvitalsdetails.getJSONArray("bloodGroup");
                            }
                            if (patientcurrentvitalsdetails.getJSONArray("bloodPressure") != null) {
                                bloodPressure_result = patientcurrentvitalsdetails.getJSONArray("bloodPressure");
                            }
                            if (patientcurrentvitalsdetails.getJSONArray("temp") != null) {
                                temperature = patientcurrentvitalsdetails.getJSONArray("temp");
                            }
                            JSONArray objectid = patientcurrentvitalsdetails.getJSONArray("diagnosisObjectid");
                            ArrayList<String> object = new ArrayList<String>();

                            if (patientcurrentvitalsdetails.getJSONArray("diagnosisObjectid") != null) {

                                for (int k = 0; k < objectid.length(); k++) {

                                    try {

                                        if (combinedobject.getString("diagnosisObjId") != null) {

                                            if (combinedobject.getString("diagnosisObjId").equals(objectid.getString(k))) {

                                                position = k;
                                            }
                                        }

                                    } catch (JSONException e1) {

                                        // TODO Auto-generated catch block
                                        e1.printStackTrace();
                                    }
                                }
                            }
                        }


                        //if(position!=-1){

                        try {

                            if (weight_result.length() > 0)
                                combinedobject.put("weight_result", weight_result.getString(weight_result.length() - 1));
                            if (spo2_result.length() > 0)
                                combinedobject.put("spo2_result", spo2_result.getString(spo2_result.length() - 1));
                            if (temperature.length() > 0)
                                combinedobject.put("temperature", temperature.getString(temperature.length() - 1));
                            if (pulse_result.length() > 0)
                                combinedobject.put("pulse_result", pulse_result.getString(pulse_result.length() - 1));
                            if (respRate_result.length() > 0)
                                combinedobject.put("respRate_result", respRate_result.getString(respRate_result.length() - 1));
                            if (bloodPressure_result.length() > 0)
                                combinedobject.put("bloodPressure_result", bloodPressure_result.getString(bloodPressure_result.length() - 1));


							/*combinedobject.put("weight_result",weight_result.getString(position));
							combinedobject.put("spo2_result",spo2_result.getString(position));
							combinedobject.put("temperature",temperature.getString(position));
							combinedobject.put("pulse_result",pulse_result.getString(position));
							combinedobject.put("respRate_result",respRate_result.getString(position));
							combinedobject.put("bloodPressure_result",bloodPressure_result.getString(position));*/

                        } catch (JSONException e1) {
                            // TODO Auto-generated catch block
                            e1.printStackTrace();
                        }

                        //}

                        filewrite(combinedobject);

                    } else {

                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(getActivity(), "Please select One Prescription", Toast.LENGTH_LONG).show();
                }
            }

        });

    }


    private void getpatientdetailsgynacologist(final int lastOpenPrescription) {
        // TODO Auto-generated method stub

        final ParseQuery<ParseObject> query = ParseQuery.getQuery("Diagnosis");
        if (!AppUtil.haveNetwokConnection(getActivity()))
            query.fromLocalDatastore();
        query.whereEqualTo("patientID", vpuid);
        query.whereEqualTo("createdDate", selecteddate);
		/*	query.addDescendingOrder("createdatetime");*/

        query.findInBackground(new FindCallback<ParseObject>() {

            int diagnosisindex;

            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                // TODO Auto-generated method stub


                //Toast.makeText(getActivity(),"objects size"+objects.size(), Toast.LENGTH_LONG).show();

                if (objects.size() > 0) {
                    if (e == null) {

                        Toast.makeText(getActivity(), "date" + selecteddate, Toast.LENGTH_LONG).show();

                        ArrayList arrayList1 = new ArrayList();

                        for (int i = 0; i < objects.size(); i++) {
                            if (objects.get(i).getDouble("typeFlag") == 1) {
                                arrayList1.add(objects.get(i).getNumber("createdatetime"));
                            }
                        }

                        Long val1 = (long) 0;
                        if (arrayList1.size() != 0) {
                            val1 = Long.parseLong(Collections.max(arrayList1).toString());
                        }
                        for (int j = 0; j < objects.size(); j++) {
                            if (objects.get(j).getNumber("createdatetime") == val1) {
                                diagnosisindex = j;
                            }
                        }


                        System.out.println("diagnosisindex grater than zero");

                        final ParseObject combinedobject = new ParseObject("");
                        combinedobject.put("createdAt", formattedDate);

                        System.out.println("diagnosisindex grater than zero");

                        if (objects.get(diagnosisindex).getDouble("typeFlag") == 1) {

                            System.out.println("type 1 working");

                            if (objects.get(diagnosisindex).getString("additionalComments") != null) {
                                combinedobject.put("additionalComments", objects.get(diagnosisindex).getString("additionalComments"));
                            }

                            if (objects.get(diagnosisindex).getString("suspectedDisease") != null) {
                                //	combinedobject.put("suspectedDisease",objects.get(diagnosisindex).getString("suspectedDisease"));
                                combinedobject.put("diagnosisObjId", objects.get(diagnosisindex).getString("diagnosisId"));

                                String[] value1 = (objects.get(diagnosisindex).getString("suspectedDisease").split(","));
                                ArrayList<String> diseasenamecheck = new ArrayList<String>();

                                for (int val = 0; val < value1.length; val++) {

                                    diseasenamecheck.add(value1[val]);

                                }

                                if (_suspecteddiseasenamesObj.size() != 0 && _suspecteddiseasenamesObj != null) {
                                    //compairing_code_taken_disease_fromparse(objects.get(diagnosisindex).getString("suspectedDisease"));

                                    String[] value = objects.get(diagnosisindex).getString("suspectedDisease").split(",");
                                    if (value != null)
                                        for (int k = 0; k < value.length; k++) {

                                            System.out.println("code valuessss from parse" + value[k]);
                                        }
                                    String[] myaccrArray = null;
                                    if (value != null) {
                                        myaccrArray = new String[value.length];
                                        for (int j = 0; j < value.length; j++) {
                                            for (int k = 0; k < diseasenamearraylist.size(); k++) {
                                                if ((diseasenamearraylist.get(k).getIcdnumber()) != null) {
                                                    String icd_from_list = diseasenamearraylist.get(k).getIcdnumber().toString().trim();
                                                    String icd_from_selected = value[j].toString().trim();
                                                    if (icd_from_list.equalsIgnoreCase(icd_from_selected)) {
                                                        myaccrArray[j] = diseasenamearraylist.get(k).getDiseasename();
                                                    }
                                                }
                                            }
                                            String disease = "";
                                            for (int i = 0; i < myaccrArray.length; i++) {
                                                disease += myaccrArray[i] + "\n";
                                            }

                                            //ettext.setText(disease);
                                            combinedobject.put("suspectedDisease", disease);
                                        }
                                    }


                                } else {


                                    final ProgressDialog mProgress = new ProgressDialog(getActivity());
                                    mProgress.setMessage("Fetching Details.....");
                                    mProgress.show();
                                    ParseQuery<ParseObject> query = ParseQuery.getQuery("DiseaseDatabase");
                                    if (!AppUtil.haveNetwokConnection(getActivity()))
                                        query.fromLocalDatastore();
                                    query.whereContainedIn("ICDNumber", diseasenamecheck);
                                    query.findInBackground(new FindCallback<ParseObject>() {

                                        @Override
                                        public void done(List<ParseObject> object, ParseException e) {
                                            // TODO Auto-generated method stub
                                            if (e == null) {
                                                if (object.size() > 0) {
                                                    String disease = "";
                                                    for (int diseasesize = 0; diseasesize < object.size(); diseasesize++) {
                                                        System.out.println("names disease" + object.get(diseasesize).getString("DiseaseName"));
                                                        disease += object.get(diseasesize).getString("DiseaseName") + "\n";
                                                    }
                                                    combinedobject.put("suspectedDisease", disease);
                                                }
                                            }

                                            mProgress.dismiss();
                                        }
                                    });
                                }
                            } else {
                                combinedobject.put("suspectedDisease", "Nil");
                            }

                            if (objects.get(diagnosisindex).getString("symptoms") != null) {
                                combinedobject.put("symptoms", objects.get(diagnosisindex).getString("symptoms"));
                            }
                            if (objects.get(diagnosisindex).getString("syndromes") != null) {
                                combinedobject.put("syndromes", objects.get(diagnosisindex).getString("syndromes"));
                            }
                        }
                        combinedobject.put("treatment", prescriptionObject.get(lastOpenPrescription).getString("treatment"));
                        combinedobject.put("followup", prescriptionObject.get(lastOpenPrescription).getString("followup"));

                        if (prescriptionObject.get(lastOpenPrescription).getJSONArray("days") != null) {
                            System.out.println("lastopen prescription" + lastOpenPrescription);

                            System.out.println("days" + prescriptionObject.get(lastOpenPrescription).getJSONArray("days"));
                            combinedobject.put("days", prescriptionObject.get(lastOpenPrescription).getJSONArray("days"));
                        }
                        if (prescriptionObject.get(lastOpenPrescription).getJSONArray("drugStartDate") != null) {
                            System.out.println("drugStartDate" + prescriptionObject.get(lastOpenPrescription).getJSONArray("drugStartDate"));
                            combinedobject.put("drugStartDate", prescriptionObject.get(lastOpenPrescription).getJSONArray("drugStartDate"));
                        }
                        if (prescriptionObject.get(lastOpenPrescription).getJSONArray("dosage") != null) {
                            combinedobject.put("dosage", prescriptionObject.get(lastOpenPrescription).getJSONArray("dosage"));
                        }
                        if (prescriptionObject.get(lastOpenPrescription).getJSONArray("drug") != null) {
                            System.out.println("drug name" + prescriptionObject.get(lastOpenPrescription).getJSONArray("drug"));
                            combinedobject.put("drug", prescriptionObject.get(lastOpenPrescription).getJSONArray("drug"));
                        }
                        if (prescriptionObject.get(lastOpenPrescription).getJSONArray("duration") != null) {
                            combinedobject.put("duration", prescriptionObject.get(lastOpenPrescription).getJSONArray("duration"));
                        }
                        if (prescriptionObject.get(lastOpenPrescription).getJSONArray("quantity") != null) {
                            combinedobject.put("quantity", prescriptionObject.get(lastOpenPrescription).getJSONArray("quantity"));
                        }
						/*
					if(treatment!=null){
						combinedobject.put("treatment",treatment);
					}else{*/
						/*combinedobject.put("treatment",tvTreatment.getText().toString());*/
						/*}*/

						/*if(followup!=null){
						combinedobject.put("followup",followup);
					}else{*/
						/*combinedobject.put("followup",tvFollowup.getText().toString());*/
						/*}*/

                        if (doctername != null) {
                            combinedobject.put("doctername", doctername);
                        }

                        if (patientname != null) {
                            combinedobject.put("patientname", patientname);
                        }

                        combinedobject.put("id", uniqueid.getText().toString());


						/*tvTreatment.setText(treatment);
					tvFollowup.setText(followup);*/


						/*	JSONArray weight_result = new JSONArray();
					JSONArray spo2_result= new JSONArray();
					JSONArray pulse_result= new JSONArray();
					JSONArray respRate_result= new JSONArray();
					JSONArray height_result= new JSONArray();
					JSONArray bloodGroup_result= new JSONArray();
					JSONArray bloodPressure_result= new JSONArray();
					JSONArray temperature= new JSONArray();

					if(patientcurrentvitalsdetails!=null){

						if(patientcurrentvitalsdetails.getJSONArray("weight")!=null){
							weight_result=patientcurrentvitalsdetails.getJSONArray("weight");
						}
						if(patientcurrentvitalsdetails.getJSONArray("spo2")!=null){
							spo2_result=patientcurrentvitalsdetails.getJSONArray("spo2");
						}
						if(patientcurrentvitalsdetails.getJSONArray("pulse")!=null){
							pulse_result=patientcurrentvitalsdetails.getJSONArray("pulse");
						}
						if(patientcurrentvitalsdetails.getJSONArray("respRate")!=null){
							respRate_result=patientcurrentvitalsdetails.getJSONArray("respRate");
						}
						if(patientcurrentvitalsdetails.getJSONArray("height")!=null){
							height_result=patientcurrentvitalsdetails.getJSONArray("height");
						}
						if(patientcurrentvitalsdetails.getJSONArray("bloodGroup")!=null){
							bloodGroup_result=patientcurrentvitalsdetails.getJSONArray("bloodGroup");
						}
						if(patientcurrentvitalsdetails.getJSONArray("bloodPressure")!=null){
							bloodPressure_result=patientcurrentvitalsdetails.getJSONArray("bloodPressure");
						}
						if(patientcurrentvitalsdetails.getJSONArray("temp")!=null){
							temperature=patientcurrentvitalsdetails.getJSONArray("temp");		
						}
						JSONArray objectid=patientcurrentvitalsdetails.getJSONArray("diagnosisObjectid");
						ArrayList<String> object=new ArrayList<String>();

						if(patientcurrentvitalsdetails.getJSONArray("diagnosisObjectid")!=null){

							for(int k=0;k<objectid.length();k++){

								try {

									if(combinedobject.getString("diagnosisObjId")!=null){

										if(combinedobject.getString("diagnosisObjId").equals(objectid.getString(k))){

											position=k;
										}
									}

								} catch (JSONException e1) {

									// TODO Auto-generated catch block
									e1.printStackTrace();
								}
							}
						}
					}
						 */


                        //if(position!=-1){


                        if (objects.get(diagnosisindex).getString("weight") != null) {
                            combinedobject.put("weight", objects.get(diagnosisindex).getString("weight"));
                        }
                        if (objects.get(diagnosisindex).getString("height") != null) {
                            combinedobject.put("height", objects.get(diagnosisindex).getString("height"));
                        }
                        if (objects.get(diagnosisindex).getString("bloodgroup") != null) {
                            combinedobject.put("bloodgroup", objects.get(diagnosisindex).getString("bloodgroup"));
                        }
                        if (objects.get(diagnosisindex).getString("bloodpressure") != null) {
                            combinedobject.put("bloodpressure", objects.get(diagnosisindex).getString("bloodpressure"));
                        }
                        if (objects.get(diagnosisindex).getString("spo2") != null) {
                            combinedobject.put("spo2", objects.get(diagnosisindex).getString("spo2"));
                        }
                        if (objects.get(diagnosisindex).getString("pulse") != null) {
                            combinedobject.put("pulse", objects.get(diagnosisindex).getString("pulse"));
                        }
                        if (objects.get(diagnosisindex).getString("respiratoryrate") != null) {
                            combinedobject.put("respiratoryrate", objects.get(diagnosisindex).getString("respiratoryrate"));
                        }
                        if (objects.get(diagnosisindex).getString("temperature") != null) {
                            combinedobject.put("temperature", objects.get(diagnosisindex).getString("temperature"));
                        }


                        //}

                        filewritegynacologist(combinedobject);

                    } else {

                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(getActivity(), "Please select One Prescription", Toast.LENGTH_LONG).show();
                }
            }

        });

    }


    public void filewritegynacologist(ParseObject combinedobject) {

        FileOperationsGynacologist fop = new FileOperationsGynacologist();
        System.out.println("combinedobject" + combinedobject.getString("weight_result"));
        fop.write(getActivity(), uniqueid.getText().toString(), combinedobject);
        if (fop.write(getActivity(), uniqueid.getText().toString(), combinedobject)) {
            //progressdialog.dismiss();
            Toast.makeText(getActivity(),
                    uniqueid.getText().toString() + ".pdf created", Toast.LENGTH_SHORT)
                    .show();

            String pathname = "/sdcard/" + uniqueid.getText().toString() + ".pdf";
            printfile(pathname);

        } else {

            Toast.makeText(getActivity(), "pdf not created",
                    Toast.LENGTH_SHORT).show();
        }

    }


    public void filewrite(ParseObject combinedobject) {

        FileOperationsNew fop = new FileOperationsNew();
        System.out.println("combinedobject" + combinedobject.getString("weight_result"));
        fop.write(getActivity(), uniqueid.getText().toString(), combinedobject);
        if (fop.write(getActivity(), uniqueid.getText().toString(), combinedobject)) {
            //progressdialog.dismiss();
            Toast.makeText(getActivity(),
                    uniqueid.getText().toString() + ".pdf created", Toast.LENGTH_SHORT)
                    .show();

            String pathname = "/sdcard/" + uniqueid.getText().toString() + ".pdf";
            printfile(pathname);

        } else {

            Toast.makeText(getActivity(), "pdf not created",
                    Toast.LENGTH_SHORT).show();
        }

    }

    @SuppressLint("NewApi")
    private void printfile(String pathname) {
        // TODO Auto-generated method stub
        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion >= android.os.Build.VERSION_CODES.KITKAT) {
            PrintManager printManager = (PrintManager) getActivity().getSystemService(Context.PRINT_SERVICE);
            String jobName = getString(R.string.app_name) + " Document";
            PrintHelper pda = new PrintHelper(getActivity(), pathname);
            printManager.print(jobName, pda, null);
        } else {
            Toast.makeText(getActivity(), "Print works only KITKAT or Above Versions", Toast.LENGTH_LONG).show();
        }
    }

    public void compairing_code_taken_disease_fromparse(String diseasename, EditText ettext) {
        String[] value = diseasename.split(",");
        if (value != null)
            for (int k = 0; k < value.length; k++) {

                System.out.println("code valuessss from parse" + value[k]);
            }
        String[] myaccrArray = null;
        if (value != null) {
            myaccrArray = new String[value.length];
            for (int j = 0; j < value.length; j++) {
                for (int k = 0; k < diseasenamearraylist.size(); k++) {
                    if ((diseasenamearraylist.get(k).getIcdnumber()) != null) {
                        String icd_from_list = diseasenamearraylist.get(k).getIcdnumber().toString().trim();
                        String icd_from_selected = value[j].toString().trim();
                        if (icd_from_list.equalsIgnoreCase(icd_from_selected)) {
                            myaccrArray[j] = diseasenamearraylist.get(k).getDiseasename();
                        }
                    }
                }
                String disease = "";
                for (int i = 0; i < myaccrArray.length; i++) {
                    disease += myaccrArray[i] + "\n";
                }

                ettext.setText(disease);

            }
        }

    }


}
