package com.wiinnova.doctorsdiary.fragment;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.wiinnova.doctorsdiary.R;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.SaveCallback;
import com.wiinnova.doctorsdiary.activities.FragmentActivityContainer;
import com.wiinnova.doctorsdiary.activities.Home_Page_Activity;
import com.wiinnova.doctorsdiary.fragment.Diagnosis_Frag_Gynacologist.onexaminationcreate;
import com.wiinnova.doctorsdiary.popupwindow.SpinnerPopup;
import com.wiinnova.doctorsdiary.popupwindow.SpinnerPopup.onSubmitListener;
import com.wiinnova.doctorsdiary.supportclasses.LMPAdapter;
import com.wiinnova.doctorsdiary.supportclasses.LMPAdapterDiagnosis;

import org.json.JSONArray;
import org.json.JSONException;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class Diagnosisexamination_Gynacologist extends Fragment implements onexaminationcreate {


    TextView flName, uniqueID;
    private EditText etweight;
    private EditText etheight;
    private EditText etspo;
    private EditText etpulse;
    private EditText etrespiratory;
    private EditText ettemperature;
    private EditText etpressure;

    private CheckBox cbnormal;
    private CheckBox cbabnormal;
    private EditText etexternalgenetalia;

    private CheckBox cblumpno;
    private CheckBox cblumpyes;
    private EditText etlump;

    private CheckBox cbgalactorrheano;
    private CheckBox cbgalactorrheayes;
    private EditText etgalactorrhea;

    private CheckBox cbbreastotherno;
    private CheckBox cbbreastotheryes;
    private EditText etbreastother;

    private CheckBox cbwelldevelopedno;
    private CheckBox cbwelldevelopedyes;
    private EditText etwelldeveloped;

    private CheckBox cbhairno;
    private CheckBox cbhairyes;
    private EditText ethair;

    private CheckBox cbacneno;
    private CheckBox cbacneyes;
    private EditText etacne;

    private CheckBox cbsecondarysexotherno;
    private CheckBox cbsecondarysexotheryes;
    private EditText etsecondarysexother;

    private CheckBox cbpreabdomennormal;
    private CheckBox cbpreabdomenabnormal;
    private EditText etpreabdomen;

    private CheckBox cbhealthyno;
    private CheckBox cbhealthyyes;
    private EditText ethealthy;

    private CheckBox cbbleedingno;
    private CheckBox cbbleedingyes;
    private EditText etbleeding;

    private CheckBox cblbcno;
    private CheckBox cblbcyes;
    private EditText etlbc;

    private TextView tvavaf;
    private TextView tvrvrf;
    private EditText etuterusothers;


    private SpinnerPopup spObj;
    private onSubmitListener spinnerpopupListener;
    private int flag;


    String weight_result;
    String spo2_result;
    String pulse_result;
    String respRate_result;
    String height_result;
    String bloodGroup_result;
    String bloodPressure_result;
    String temp_result;
    JSONArray update_date;
    JSONArray currentvitals_id;

    JSONArray weight_result_array;
    JSONArray spo2_result_array;
    JSONArray pulse_result_array;
    JSONArray respRate_result_array;
    JSONArray height_result_array;
    JSONArray bloodGroup_result_array;
    JSONArray bloodPressure_result_array;
    JSONArray temp_result_array;
    JSONArray update_date_array;
    JSONArray currentvitals_id_array;


    ParseObject patientobject;

    private EditText etBloodGroup;
    private TextView tvName;
    private String[] avaf_array;
    private Diagnosis_Frag_Gynacologist diagfrag;

    private EditText etbmi;
    private double heightInt;
    private double weightInt;
    private String[] bloodgroup_array;
    private onSubmitListener bloodGroupSpinnerListener;
    private EditText etdiapressure;
    private EditText etsyspressure;
    private CharSequence pid;
    private ProgressDialog mProgressDialog;
    private SimpleDateFormat df;
    private String formattedDate;
    private LinearLayout mainlayout;
    private ScrollView scroll;
    private String bmi;
    private long currentDateandTime;
    private TextView tvCmtoFtConvert;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        View v = inflater.inflate(R.layout.diagnosisexamination_gynecologist, null);


        avaf_array = getResources().getStringArray(R.array.avaf);
        bloodgroup_array = getResources().getStringArray(R.array.bloodgroup_array);


        mainlayout = (LinearLayout) v.findViewById(R.id.llmaincontainer);
        scroll = (ScrollView) v.findViewById(R.id.scroll);

        flName = (TextView) v.findViewById(R.id.tvfirstlastName);
        uniqueID = (TextView) v.findViewById(R.id.cduniqueid);
        tvCmtoFtConvert = (TextView) v.findViewById(R.id.tv_cmtoft);
        etweight = (EditText) v.findViewById(R.id.etweight);
        etheight = (EditText) v.findViewById(R.id.etheight);
        etbmi = (EditText) v.findViewById(R.id.etbmi);
        etspo = (EditText) v.findViewById(R.id.etspo);
        etpulse = (EditText) v.findViewById(R.id.etpulse);
        etrespiratory = (EditText) v.findViewById(R.id.etrespiratory);
        ettemperature = (EditText) v.findViewById(R.id.ettemperature);
        etBloodGroup = (EditText) v.findViewById(R.id.etbloodgroup);
        etdiapressure = (EditText) v.findViewById(R.id.etbloodpressure);
        etsyspressure = (EditText) v.findViewById(R.id.etbloodpressure1);


        cbnormal = (CheckBox) v.findViewById(R.id.cbnormal);
        cbabnormal = (CheckBox) v.findViewById(R.id.cbabnormal);
        etexternalgenetalia = (EditText) v.findViewById(R.id.etexternalgenetalia);

        cblumpno = (CheckBox) v.findViewById(R.id.cblumpno);
        cblumpyes = (CheckBox) v.findViewById(R.id.cblumpyes);
        etlump = (EditText) v.findViewById(R.id.etlump);

        cbgalactorrheano = (CheckBox) v.findViewById(R.id.cbgalactorrheano);
        cbgalactorrheayes = (CheckBox) v.findViewById(R.id.cbgalactorrheayes);
        etgalactorrhea = (EditText) v.findViewById(R.id.etgalactorrhea);

        cbbreastotherno = (CheckBox) v.findViewById(R.id.cbotherno);
        cbbreastotheryes = (CheckBox) v.findViewById(R.id.cbotheryes);
        etbreastother = (EditText) v.findViewById(R.id.etother);

        cbwelldevelopedno = (CheckBox) v.findViewById(R.id.cbwelldevelopedno);
        cbwelldevelopedyes = (CheckBox) v.findViewById(R.id.cbwelldevelopedyes);
        etwelldeveloped = (EditText) v.findViewById(R.id.etwelldeveloped);

        cbhairno = (CheckBox) v.findViewById(R.id.cbhairno);
        cbhairyes = (CheckBox) v.findViewById(R.id.cbhairyes);
        ethair = (EditText) v.findViewById(R.id.ethair);

        cbacneno = (CheckBox) v.findViewById(R.id.cbacneno);
        cbacneyes = (CheckBox) v.findViewById(R.id.cbacneyes);
        etacne = (EditText) v.findViewById(R.id.etacne);

        cbsecondarysexotherno = (CheckBox) v.findViewById(R.id.cbotherno1);
        cbsecondarysexotheryes = (CheckBox) v.findViewById(R.id.cbotheryes1);
        etsecondarysexother = (EditText) v.findViewById(R.id.etother1);

        cbpreabdomennormal = (CheckBox) v.findViewById(R.id.cbabdomenno);
        cbpreabdomenabnormal = (CheckBox) v.findViewById(R.id.cbabdomenyes);
        etpreabdomen = (EditText) v.findViewById(R.id.etAbdomen);

        cbhealthyno = (CheckBox) v.findViewById(R.id.cbhealthyno);
        cbhealthyyes = (CheckBox) v.findViewById(R.id.cbhealthyyes);
        ethealthy = (EditText) v.findViewById(R.id.ethealthy);

        cbbleedingno = (CheckBox) v.findViewById(R.id.cbbleedingno);
        cbbleedingyes = (CheckBox) v.findViewById(R.id.cbbleedingyes);
        etbleeding = (EditText) v.findViewById(R.id.etbleeding);

        cblbcno = (CheckBox) v.findViewById(R.id.cblbcno);
        cblbcyes = (CheckBox) v.findViewById(R.id.cblbcyes);
        ;
        etlbc = (EditText) v.findViewById(R.id.etlbc);
        ;

        tvavaf = (TextView) v.findViewById(R.id.avafsize);
        tvrvrf = (TextView) v.findViewById(R.id.rvrfsize);
        etuterusothers = (EditText) v.findViewById(R.id.etvaginal);

        /*///////////////call LMP

        LMP(getActivity(), v);
*/
//		Set_Hint();

        tvCmtoFtConvert.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                if (tvCmtoFtConvert.getText().toString().equalsIgnoreCase("cm")) {
                    tvCmtoFtConvert.setText("ft");
                    double heightInt1;
                    if (etheight.getText().toString() != null) {
                        heightInt1 = Double.parseDouble(!etheight.getText().toString().equals("") ?
                                etheight.getText().toString() : "0");
                        double hinfeet = heightInt1 * 0.032808;
                        etheight.setText(String.valueOf(hinfeet));

                    }

                } else {
                    tvCmtoFtConvert.setText("cm");
                    double heightInt1;
                    if (etheight.getText().toString() != null) {
                        heightInt1 = Double.parseDouble(!etheight.getText().toString().equals("") ?
                                etheight.getText().toString() : "0");
                        double hincm = heightInt1 / 0.032808;
                        etheight.setText(String.valueOf(hincm));
                    }
                }

            }
        });


        etbmi.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

                submitButtonActivation();

            }
        });


        etweight.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

                submitButtonActivation();

            }
        });


        etBloodGroup.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

                submitButtonActivation();

            }
        });

        etdiapressure.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub

				/*if(before==1){
                    flag=true;
				}else{
					flag=false;
				}*/
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
                /*	if(!(s.toString().contains("/")) && flag==false){
                    if(s.length()==3){
						s.append("/");
						System.out.println("text 3");
					}
				}*/

                submitButtonActivation();
            }
        });

        etsyspressure.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub

				/*if(before==1){
                    flag=true;
				}else{
					flag=false;
				}*/
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
                /*if(!(s.toString().contains("/")) && flag==false){
                    if(s.length()==3){
						s.append("/");
						System.out.println("text 3");
					}
				}*/


                submitButtonActivation();
            }

        });

        etspo.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

                submitButtonActivation();

            }
        });

        ettemperature.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

                submitButtonActivation();
            }
        });

        etheight.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

                submitButtonActivation();

            }
        });

        etpulse.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

                submitButtonActivation();

            }
        });

        etrespiratory.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

                submitButtonActivation();

            }
        });


        if (Home_Page_Activity.patient != null) {

            System.out.println("patient not null");
            pid = Home_Page_Activity.patient.getString("patientID");

            String fname = Home_Page_Activity.patient.getString("firstName");
            String lname = Home_Page_Activity.patient.getString("lastName");
            if (fname == null) {
                fname = "FirstName";
            }
            if (lname == null) {
                lname = "LastName";
            }

            flName.setText(fname + " " + lname);
            uniqueID.setText(pid);


        } else {

            SharedPreferences scanpidnew = getActivity().getSharedPreferences("NewPatID", 0);
            String scnewPid = scanpidnew.getString("NewID", null);
            SharedPreferences scanpid1 = getActivity().getSharedPreferences("ScannedPid", 0);
            String scPid = scanpid1.getString("scanpid", null);
            System.out.println("scanpersonal id" + scnewPid);

            if (scPid != null) {
                pid = scPid;
                uniqueID.setText(pid);
                SharedPreferences fnname1 = getActivity().getSharedPreferences("fnName", 0);
                flName.setText(fnname1.getString("fNname", "FirstName" + " " + "LastName"));
            }

            if (scnewPid != null) {
                pid = scnewPid;
                uniqueID.setText(pid);
                SharedPreferences fnname1 = getActivity().getSharedPreferences("fnName", 0);
                flName.setText(fnname1.getString("fNname", "FirstName" + " " + "LastName"));
            }
        }


        mainlayout.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                hideKeyboard(v);
            }
        });

        scroll.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                hideKeyboard(v);

            }
        });


        bloodGroupSpinnerListener = new SpinnerPopup.onSubmitListener() {

            @Override
            public void onSubmit(String arg, int position) {
                // TODO Auto-generated method stub

                etBloodGroup.setText(arg);
                spObj.dismiss();
            }
        };


        //patientobject=((FragmentActivityContainer)getActivity()).getdiagnosisexaminationparseObj();
        patientobject = ((FragmentActivityContainer) getActivity()).getpatientdiagnosisparseObj();

        ((FragmentActivityContainer) getActivity()).setDiagexam_gynacologist(this);

        if (getFragmentManager().findFragmentById(R.id.fl_sidemenu_container) instanceof Diagnosis_Frag_Gynacologist) {


        } else {
            diagfrag = new Diagnosis_Frag_Gynacologist();
            getFragmentManager().beginTransaction()
                    .replace(R.id.fl_sidemenu_container, diagfrag)
                    .commit();

            if (diagfrag != null) {
                ((FragmentActivityContainer) getActivity()).setDiagfrag_gynacologist(diagfrag);
            }
        }

        ((FragmentActivityContainer) getActivity()).getDiagfrag_gynacologist().setmenuitemvalue(1);

        etBloodGroup.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub

                Bundle bundle = new Bundle();
                bundle.putString("name", "Blood Group");
                bundle.putStringArray("items", bloodgroup_array);
                //bundle.putSerializable("listener",CurrentVitals.this);
                spObj = new SpinnerPopup();
                spObj.setSubmitListener(bloodGroupSpinnerListener);
                spObj.setArguments(bundle);
                spObj.show(getFragmentManager(), "tag");


            }
        });

        etBloodGroup.setOnFocusChangeListener(new OnFocusChangeListener() {

            @Override
            public void onFocusChange(View arg0, boolean b) {
                // TODO Auto-generated method stub

                if (b == true) {


                    Bundle bundle = new Bundle();
                    bundle.putString("name", "Blood Group");
                    bundle.putStringArray("items", bloodgroup_array);
                    //bundle.putSerializable("listener",CurrentVitals.this);
                    spObj = new SpinnerPopup();
                    spObj.setSubmitListener(bloodGroupSpinnerListener);
                    spObj.setArguments(bundle);
                    spObj.show(getFragmentManager(), "tag");

                }


            }
        });


        spinnerpopupListener = new onSubmitListener() {

            @Override
            public void onSubmit(String arg, int position) {
                // TODO Auto-generated method stub
                Log.d("OutputGet", arg + " flag " + flag);
                if (flag == 0) {
                    tvavaf.setText(arg);
                    submitButtonActivation();
                }
                if (flag == 1) {
                    tvrvrf.setText(arg);
                    submitButtonActivation();

                }
                spObj.dismiss();


            }
        };


        cbnormal.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub


                if (cbnormal.isChecked()) {
                    cbabnormal.setChecked(false);
                    submitButtonActivation();

                }

            }
        });

        cbabnormal.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub

                if (cbabnormal.isChecked()) {
                    cbnormal.setChecked(false);
                    submitButtonActivation();
                }

            }
        });

        cblumpno.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                if (cblumpno.isChecked()) {
                    cblumpyes.setChecked(false);
                    submitButtonActivation();

                }

            }
        });
        cblumpyes.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub

                if (cblumpyes.isChecked()) {
                    cblumpno.setChecked(false);
                    submitButtonActivation();

                }

            }
        });

        cbgalactorrheano.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub

                if (cbgalactorrheano.isChecked()) {
                    cbgalactorrheayes.setChecked(false);
                    submitButtonActivation();

                }

            }
        });

        cbgalactorrheayes.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub

                if (cbgalactorrheayes.isChecked()) {
                    cbgalactorrheano.setChecked(false);
                    submitButtonActivation();

                }

            }
        });

        cbbreastotherno.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub


                if (cbbreastotherno.isChecked()) {
                    cbbreastotheryes.setChecked(false);
                    submitButtonActivation();

                }

            }
        });

        cbbreastotheryes.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub


                if (cbbreastotheryes.isChecked()) {
                    cbbreastotherno.setChecked(false);
                    submitButtonActivation();

                }

            }
        });

        cbwelldevelopedno.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub


                if (cbwelldevelopedno.isChecked()) {
                    cbwelldevelopedyes.setChecked(false);
                    submitButtonActivation();

                }

            }
        });

        cbwelldevelopedyes.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub


                if (cbwelldevelopedyes.isChecked()) {
                    cbwelldevelopedno.setChecked(false);
                    submitButtonActivation();

                }

            }
        });

        cbhairno.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub


                if (cbhairno.isChecked()) {
                    cbhairyes.setChecked(false);
                    submitButtonActivation();

                }

            }
        });

        cbhairyes.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub


                if (cbhairyes.isChecked()) {
                    cbhairno.setChecked(false);
                    submitButtonActivation();

                }

            }
        });


        cbacneno.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub


                if (cbacneno.isChecked()) {
                    cbacneyes.setChecked(false);
                    submitButtonActivation();

                }

            }
        });

        cbacneyes.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub


                if (cbacneyes.isChecked()) {
                    cbacneno.setChecked(false);
                    submitButtonActivation();

                }

            }
        });

        cbsecondarysexotherno.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {


                // TODO Auto-generated method stub
                if (cbsecondarysexotherno.isChecked()) {
                    cbsecondarysexotheryes.setChecked(false);
                    submitButtonActivation();

                } else {
                    submitButtonDeactivation();
                }

            }
        });


        cbsecondarysexotheryes.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {


                // TODO Auto-generated method stub
                if (cbsecondarysexotheryes.isChecked()) {
                    cbsecondarysexotherno.setChecked(false);
                    submitButtonActivation();

                } else {
                    submitButtonDeactivation();
                }

            }
        });

        cbpreabdomennormal.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub


                if (cbpreabdomennormal.isChecked()) {
                    cbpreabdomenabnormal.setChecked(false);
                    submitButtonActivation();

                }
            }
        });

        cbpreabdomenabnormal.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub


                if (cbpreabdomenabnormal.isChecked()) {
                    cbpreabdomennormal.setChecked(false);
                    submitButtonActivation();

                }
            }
        });

        cbhealthyno.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub


                if (cbhealthyno.isChecked()) {
                    cbhealthyyes.setChecked(false);

                    submitButtonActivation();

                }
            }
        });

        cbhealthyyes.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub


                if (cbhealthyyes.isChecked()) {
                    cbhealthyno.setChecked(false);
                    submitButtonActivation();

                }
            }
        });

        cbbleedingno.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub


                if (cbbleedingno.isChecked()) {
                    cbbleedingyes.setChecked(false);
                    submitButtonActivation();

                }
            }
        });

        cbbleedingyes.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub


                if (cbbleedingyes.isChecked()) {
                    cbbleedingno.setChecked(false);
                    submitButtonActivation();

                }
            }
        });

        cblbcno.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub


                if (cblbcno.isChecked()) {
                    cblbcyes.setChecked(false);
                    submitButtonActivation();

                }
            }
        });

        cblbcyes.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub


                if (cblbcyes.isChecked()) {
                    cblbcno.setChecked(false);

                    submitButtonActivation();

                }
            }
        });

        tvavaf.setOnClickListener(new OnClickListener() {


            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                flag = 0;
                Bundle bundle = new Bundle();
                bundle.putString("name", "AVAF");
                bundle.putStringArray("items", avaf_array);
                spObj = new SpinnerPopup();
                spObj.setSubmitListener(spinnerpopupListener);
                spObj.setArguments(bundle);
                spObj.show(getFragmentManager(), "tag");


            }
        });
        tvrvrf.setOnClickListener(new OnClickListener() {


            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                flag = 1;
                Bundle bundle = new Bundle();
                bundle.putString("name", "RVRF");
                bundle.putStringArray("items", avaf_array);
                spObj = new SpinnerPopup();
                spObj.setSubmitListener(spinnerpopupListener);
                spObj.setArguments(bundle);
                spObj.show(getFragmentManager(), "tag");


            }
        });

		/*Drawable img  = getActivity().getResources().getDrawable( R.drawable.weight );
        Drawable img1 = getActivity().getResources().getDrawable( R.drawable.height );
		Drawable img2 = getActivity().getResources().getDrawable( R.drawable.bloodgroup );
		Drawable img3 = getActivity().getResources().getDrawable( R.drawable.spo2 );
		Drawable img4 = getActivity().getResources().getDrawable( R.drawable.pulse );
		Drawable img5 = getActivity().getResources().getDrawable( R.drawable.respiratoryrate );
		Drawable img6 = getActivity().getResources().getDrawable( R.drawable.temperature );

		String kg = "kg";
		etweight.setCompoundDrawablesWithIntrinsicBounds(img, null, new TextDrawable(kg), null);
				etweight.setCompoundDrawablePadding(kg.length()*10);

		String cm = "cm";
		etheight.setCompoundDrawablesWithIntrinsicBounds(img1, null, new TextDrawable(cm), null);
				etheight.setCompoundDrawablePadding(cm.length()*10);

		String bmi = "bmi";
		etheight.setCompoundDrawablesWithIntrinsicBounds(img2, null, new TextDrawable(bmi), null);
				etheight.setCompoundDrawablePadding(bmi.length()*10);

		String spo = "%";
		etspo.setCompoundDrawablesWithIntrinsicBounds(img3, null, new TextDrawable(spo), null);
				etspo.setCompoundDrawablePadding(spo.length()*10);

		String pulse="beats/mins";
		etpulse.setCompoundDrawablesWithIntrinsicBounds(img4, null, new TextDrawable(pulse), null);
				etpulse.setCompoundDrawablePadding(pulse.length()*10);


		String respiratory="breaths/mins";
		etrespiratory.setCompoundDrawablesWithIntrinsicBounds(img5, null, new TextDrawable(respiratory), null);
				etrespiratory.setCompoundDrawablePadding(respiratory.length()*10);

		String temp="\u00B0";
		ettemperature.setCompoundDrawablesWithIntrinsicBounds(img6, null, new TextDrawable(temp), null);
				ettemperature.setCompoundDrawablePadding(temp.length()*10);
		 */


        TextWatcher textwatcher = new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

                calculateBMI(etweight, etheight);


            }
        };

        etheight.addTextChangedListener(textwatcher);
        etweight.addTextChangedListener(textwatcher);


        return v;
    }


    @Override
    public void onResume() {
        // TODO Auto-generated method stub
        super.onResume();

        CrashReporter.getInstance().trackScreenView("Patient Current Vitals");

        etBloodGroup.setEnabled(true);
        /*if(patientedit_checkflag==false){

			etWeight.setEnabled(false);
			etBloodGroup.setEnabled(false);
			etBloodPressuresystolic.setEnabled(false);
			etBloodPressurediastolic.setEnabled(false);
			etSp.setEnabled(false);
			etTemperature.setEnabled(false);
			etHeight.setEnabled(false);
			etPulse.setEnabled(false);
			etRespiratory.setEnabled(false);

		}*/

        if (patientobject != null) {

			/*String fname=patientobject.getString("firstName");
            String lname=patientobject.getString("lastName");
			if(fname==null){
				fname="FirstName";
			}
			if(lname==null){
				lname="LastName";
			}
			tvName.setText(fname +" "+lname);*/

            weight_result = patientobject.getString("weight");
            if (weight_result != null) {

                if (weight_result.toString() != null && !(weight_result.toString().equals("Nil"))) {
                    etweight.setText(weight_result.toString());
                }
            }

            bmi = patientobject.getString("bmi");
            if (bmi != null) {

                if (bmi.toString() != null && !(bmi.toString().equals("Nil"))) {
                    etbmi.setText(bmi.toString());
                }
            }

            spo2_result = patientobject.getString("spo2");

            if (spo2_result != null) {

                if (spo2_result.toString() != null && !(spo2_result.toString().equals("Nil"))) {
                    etspo.setText(spo2_result.toString());
                }
            }


            pulse_result = patientobject.getString("pulse");

            if (pulse_result != null) {

                if (pulse_result.toString() != null && !(pulse_result.toString().equals("Nil"))) {
                    etpulse.setText(pulse_result.toString());
                }
            }

            respRate_result = patientobject.getString("respiratoryrate");

            if (respRate_result != null) {

                if (respRate_result.toString() != null && !(respRate_result.toString().equals("Nil"))) {
                    etrespiratory.setText(respRate_result.toString());
                }
            }


            height_result = patientobject.getString("height");

            if (height_result != null) {

                if (height_result.toString() != null && !(height_result.toString().equals("Nil"))) {
                    etheight.setText(height_result.toString());
                }
            }


            bloodGroup_result = patientobject.getString("bloodgroup");

            if (bloodGroup_result != null) {

                if (bloodGroup_result.toString() != null && !(bloodGroup_result.toString().equals("Nil"))) {
                    etBloodGroup.setText(bloodGroup_result.toString());
                    etBloodGroup.setEnabled(false);
                }

            }

            bloodPressure_result = patientobject.getString("bloodpressure");
            String pressure[] = null;
            if (bloodPressure_result != null) {
                pressure = patientobject.getString("bloodpressure").split("/");
                if (!(bloodPressure_result.equals("Nil"))) {
                    if (pressure[0] != null) {
                        etsyspressure.setText(pressure[0].toString());
                    }
                    if (pressure[1] != null) {
                        etdiapressure.setText(pressure[1].toString());
                    }
                }
            }

            temp_result = patientobject.getString("temperature");
            if (temp_result != null) {
                if (temp_result.toString() != null && !(temp_result.toString().equals("Nil"))) {
                    ettemperature.setText(temp_result.toString());
                }

            }
            /*update_date=patientobject.getJSONArray("testedDate");
            if(update_date!=null){
				String result="";
				try{
					result=update_date.getString(update_date.length()-1);
				}catch(Exception e){
					e.printStackTrace();
				}
			}
			currentvitals_id=patientobject.getJSONArray("currentvitalsId");
			((FragmentActivityContainer)getActivity()).getpatientfrag().onButtonactivation(0);*/

            if (patientobject.getString("externalgenetalia") != null) {
                String externalgenetalia[] = patientobject.getString("externalgenetalia").split(",");
                if (externalgenetalia[0].equalsIgnoreCase("Normal")) {
                    cbnormal.setChecked(true);
                } else if (externalgenetalia[0].equalsIgnoreCase("Abnormal")) {
                    cbabnormal.setChecked(true);
                }
                if (!externalgenetalia[1].equalsIgnoreCase("Nil")) {
                    etexternalgenetalia.setText(externalgenetalia[1]);
                }
            }

            if (patientobject.getString("breast_lump") != null) {
                String breastlump[] = patientobject.getString("breast_lump").split(",");
                if (breastlump[0].equalsIgnoreCase("No")) {
                    cblumpno.setChecked(true);
                } else if (breastlump[0].equalsIgnoreCase("Yes")) {
                    cblumpyes.setChecked(true);
                }
                if (!breastlump[1].equalsIgnoreCase("Nil")) {
                    etlump.setText(breastlump[1]);
                }
            }


            if (patientobject.getString("breast_galactorrhea") != null) {
                String breastgalactorrhea[] = patientobject.getString("breast_galactorrhea").split(",");
                if (breastgalactorrhea[0].equalsIgnoreCase("No")) {
                    cbgalactorrheano.setChecked(true);
                } else if (breastgalactorrhea[0].equalsIgnoreCase("Yes")) {
                    cbgalactorrheayes.setChecked(true);
                }
                if (!breastgalactorrhea[1].equalsIgnoreCase("Nil")) {
                    etgalactorrhea.setText(breastgalactorrhea[1]);
                }
            }

            if (patientobject.getString("breast_other") != null) {
                String breastother[] = patientobject.getString("breast_other").split(",");
                if (breastother[0].equalsIgnoreCase("No")) {
                    cbbreastotherno.setChecked(true);
                } else if (breastother[0].equalsIgnoreCase("Yes")) {
                    cbbreastotheryes.setChecked(true);
                }
                if (!breastother[1].equalsIgnoreCase("Nil")) {
                    etbreastother.setText(breastother[1]);
                }

            }

            if (patientobject.getString("secondarysex_welldeveloped") != null) {
                String secondarysex_welldeveloped[] = patientobject.getString("secondarysex_welldeveloped").split(",");
                if (secondarysex_welldeveloped[0].equalsIgnoreCase("No")) {
                    cbwelldevelopedno.setChecked(true);
                } else if (secondarysex_welldeveloped[0].equalsIgnoreCase("Yes")) {
                    cbwelldevelopedyes.setChecked(true);
                }
                if (!secondarysex_welldeveloped[1].equalsIgnoreCase("Nil")) {
                    etwelldeveloped.setText(secondarysex_welldeveloped[1]);
                }
            }

            if (patientobject.getString("secondarysex_hair") != null) {
                String secondarysex_hair[] = patientobject.getString("secondarysex_hair").split(",");
                if (secondarysex_hair[0].equalsIgnoreCase("No")) {
                    cbhairno.setChecked(true);
                } else if (secondarysex_hair[0].equalsIgnoreCase("Yes")) {
                    cbhairyes.setChecked(true);
                }
                if (!secondarysex_hair[1].equalsIgnoreCase("Nil")) {
                    ethair.setText(secondarysex_hair[1]);
                }
            }


            if (patientobject.getString("secondarysex_acne") != null) {
                String secondarysex_acne[] = patientobject.getString("secondarysex_acne").split(",");
                if (secondarysex_acne[0].equalsIgnoreCase("No")) {
                    cbacneno.setChecked(true);
                } else if (secondarysex_acne[0].equalsIgnoreCase("Yes")) {
                    cbacneyes.setChecked(true);
                }
                if (!secondarysex_acne[1].equalsIgnoreCase("Nil")) {
                    etacne.setText(secondarysex_acne[1]);
                }
            }

            if (patientobject.getString("secondarysex_other") != null) {
                String secondarysex_other[] = patientobject.getString("secondarysex_other").split(",");
                if (secondarysex_other[0].equalsIgnoreCase("No")) {
                    cbsecondarysexotherno.setChecked(true);
                } else if (secondarysex_other[0].equalsIgnoreCase("Yes")) {
                    cbsecondarysexotheryes.setChecked(true);
                }
                if (!secondarysex_other[1].equalsIgnoreCase("Nil")) {
                    etsecondarysexother.setText(secondarysex_other[1]);
                }
            }

            if (patientobject.getString("preabdomenexamination") != null) {
                String preabdomenexamination[] = patientobject.getString("preabdomenexamination").split(",");
                if (preabdomenexamination[0].equalsIgnoreCase("Normal")) {
                    cbpreabdomenabnormal.setChecked(true);
                } else if (preabdomenexamination[0].equalsIgnoreCase("Abnormal")) {
                    cbpreabdomenabnormal.setChecked(true);
                }
                if (!preabdomenexamination[1].equalsIgnoreCase("Nil")) {
                    etpreabdomen.setText(preabdomenexamination[1]);
                }
            }

            if (patientobject.getString("cervix_healthy") != null) {
                String cervix_healthy[] = patientobject.getString("cervix_healthy").split(",");
                if (cervix_healthy[0].equalsIgnoreCase("No")) {
                    cbhealthyno.setChecked(true);
                } else if (cervix_healthy[0].equalsIgnoreCase("Yes")) {
                    cbhairyes.setChecked(true);
                }
                if (!cervix_healthy[1].equalsIgnoreCase("Nil")) {
                    ethealthy.setText(cervix_healthy[1]);
                }
            }

            if (patientobject.getString("cervix_bleeding") != null) {
                String cervix_bleeding[] = patientobject.getString("cervix_bleeding").split(",");
                if (cervix_bleeding[0].equalsIgnoreCase("No")) {
                    cbbleedingno.setChecked(true);
                } else if (cervix_bleeding[0].equalsIgnoreCase("Yes")) {
                    cbbleedingyes.setChecked(true);
                }
                if (!cervix_bleeding[1].equalsIgnoreCase("Nil")) {
                    etbleeding.setText(cervix_bleeding[1]);
                }
            }

            if (patientobject.getString("cervix_lbc") != null) {
                String cervix_lbc[] = patientobject.getString("cervix_lbc").split(",");
                if (cervix_lbc[0].equalsIgnoreCase("No")) {
                    cblbcno.setChecked(true);
                } else if (cervix_lbc[0].equalsIgnoreCase("Yes")) {
                    cblbcyes.setChecked(true);
                }
                if (!cervix_lbc[1].equalsIgnoreCase("Nil")) {
                    etlbc.setText(cervix_lbc[1]);
                }
            }

            if (patientobject.getString("uterus_avaf") != null) {
                String uterus_avaf = patientobject.getString("uterus_avaf");
                if (!uterus_avaf.equalsIgnoreCase("Nil")) {
                    tvavaf.setText(uterus_avaf);
                }
            }

            if (patientobject.getString("uterus_rvrf") != null) {
                String uterus_rvrf = patientobject.getString("uterus_rvrf");
                if (!uterus_rvrf.equalsIgnoreCase("Nil")) {
                    tvrvrf.setText(uterus_rvrf);
                }
            }

            if (patientobject.getString("uterus_others") != null) {
                String uterus_others = patientobject.getString("uterus_others");
                if (!uterus_others.equalsIgnoreCase("Nil")) {
                    etuterusothers.setText(uterus_others);
                }
            }

         /*   ///////////////////////////for LMP
            List[] arrayArryayList = {lmpdate_array, lmpflow_array, lmpdysmenorrhea_array, menstrualregularlist, menstrualdayslist, menstrualcyclelist};
            String[] fildNames = {"Lmp_date", "Lmp_flow", "Lmp_dysmenorrhea", "Menstrual_Regular_Irregular", "Menstrual_Days", "Menstrual_Cycle"};
            fillLMPArrays(patientobject, arrayArryayList, fildNames);
            ///////////////////////////set adapter
            lmpAdapter = new LMPAdapterDiagnosis(getActivity(), listCount, this, null);
            lstMenstrualPeriod.setAdapter(lmpAdapter);
            justifyListViewHeightBasedOnChildren(lstMenstrualPeriod);
            //////////////////////////////////////////////////////////*/

            submitButtonDeactivation();

        }


    }

   /* public void fillLMPArrays(ParseObject patientobject, List[] array, String[] fieldName) {
        try {
            for (int k = 0; k < array.length; k++) {
                listCount.clear();
                JSONArray jsonArray = patientobject.getJSONArray(fieldName[k]);
                for (int i = 0; i < jsonArray.length(); i++) {
                    ArrayList<String> arrayList = (ArrayList<String>) array[k];
                    arrayList.add(jsonArray.get(i).toString());
                    listCount.add(0);

                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            e.printStackTrace();
            listCount.clear();
            listCount.add(0);
        }
    }*/

    protected void hideKeyboard(View view) {
        InputMethodManager in = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        in.hideSoftInputFromWindow(view.getWindowToken(),
                InputMethodManager.HIDE_NOT_ALWAYS);
    }

    private void submitButtonActivation() {
        // TODO Auto-generated method stub
        Diagnosis_Frag_Gynacologist diagnosissidefrag = ((FragmentActivityContainer) getActivity()).getDiagfrag_gynacologist();
        if (diagnosissidefrag != null)
            ((FragmentActivityContainer) getActivity()).getDiagfrag_gynacologist().onDiagnosisbtn(1);
    }

    private void submitButtonDeactivation() {
        // TODO Auto-generated method stub
        Diagnosis_Frag_Gynacologist diagnosissidefrag = ((FragmentActivityContainer) getActivity()).getDiagfrag_gynacologist();
        if (diagnosissidefrag != null)
            ((FragmentActivityContainer) getActivity()).getDiagfrag_gynacologist().onDiagnosisbtn(0);
    }


    @Override
    public void onexamination(final int arg) {
        // TODO Auto-generated method stub


        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setCancelable(false);
        mProgressDialog.setMessage("Storing Diagnosis Data....");

        mProgressDialog.show();


        if (patientobject == null) {
            patientobject = new ParseObject("Diagnosis");
        }


        if (uniqueID.getText().toString() != null) {
            patientobject.put("patientID", uniqueID.getText().toString());
        }
        patientobject.put("typeFlag", 1);

        if (etweight.getText().toString().length() != 0) {
            patientobject.put("weight", etweight.getText().toString());
        }

        if (etheight.getText().toString().length() != 0) {
            patientobject.put("height", etheight.getText().toString());
        }

        if (etbmi.getText().toString().length() != 0) {
            patientobject.put("bmi", etbmi.getText().toString());
        }
        if (etBloodGroup.getText().toString().length() != 0) {
            patientobject.put("bloodgroup", etBloodGroup.getText().toString());
        }

        if (etdiapressure.getText().toString().length() != 0 && etsyspressure.getText().toString().length() != 0) {
            String pressure = etdiapressure.getText().toString() + "/" + etsyspressure.getText().toString();
            patientobject.put("bloodpressure", pressure);
        }
        if (etspo.getText().toString().length() != 0) {
            patientobject.put("spo2", etspo.getText().toString());
        }

        if (etpulse.getText().toString().length() != 0) {
            patientobject.put("pulse", etpulse.getText().toString());
        }
        if (etrespiratory.getText().toString().length() != 0) {
            patientobject.put("respiratoryrate", etrespiratory.getText().toString());
        }
        if (ettemperature.getText().toString().length() != 0) {
            patientobject.put("temperature", ettemperature.getText().toString());
        }

        String externalgenetalia, edittext_externalgenetalia;
        if (cbnormal.isChecked()) {
            externalgenetalia = cbnormal.getText().toString();
        } else if (cbabnormal.isChecked()) {
            externalgenetalia = cbabnormal.getText().toString();
        } else {
            externalgenetalia = "Nil";
        }
        if (etexternalgenetalia.getText().toString().length() != 0) {
            edittext_externalgenetalia = etexternalgenetalia.getText().toString();
        } else {
            edittext_externalgenetalia = "Nil";
        }

        patientobject.put("externalgenetalia", externalgenetalia + "," + edittext_externalgenetalia);


        String lump, edittext_lump;
        if (cblumpno.isChecked()) {
            lump = cblumpno.getText().toString();
        } else if (cblumpyes.isChecked()) {
            lump = cblumpyes.getText().toString();
        } else {
            lump = "Nil";
        }
        if (etlump.getText().toString().length() != 0) {
            edittext_lump = etlump.getText().toString();
        } else {
            edittext_lump = "Nil";
        }

        patientobject.put("breast_lump", lump + "," + edittext_lump);


        String galactorrhea, edittext_galactorrhea;
        if (cbgalactorrheano.isChecked()) {
            galactorrhea = cbgalactorrheano.getText().toString();
        } else if (cbgalactorrheayes.isChecked()) {
            galactorrhea = cbgalactorrheayes.getText().toString();
        } else {
            galactorrhea = "Nil";
        }
        if (etgalactorrhea.getText().toString().length() != 0) {
            edittext_galactorrhea = etgalactorrhea.getText().toString();
        } else {
            edittext_galactorrhea = "Nil";
        }

        patientobject.put("breast_galactorrhea", galactorrhea + "," + edittext_galactorrhea);


        String breastother, edittext_breastother;
        if (cbbreastotherno.isChecked()) {
            breastother = cbbreastotherno.getText().toString();
        } else if (cbbreastotheryes.isChecked()) {
            breastother = cbbreastotheryes.getText().toString();
        } else {
            breastother = "Nil";
        }
        if (etbreastother.getText().toString().length() != 0) {
            edittext_breastother = etbreastother.getText().toString();
        } else {
            edittext_breastother = "Nil";
        }

        patientobject.put("breast_other", breastother + "," + edittext_breastother);


        String welldeveloped, edittext_welldeveloped;
        if (cbwelldevelopedno.isChecked()) {
            welldeveloped = cbwelldevelopedno.getText().toString();
        } else if (cbwelldevelopedyes.isChecked()) {
            welldeveloped = cbwelldevelopedyes.getText().toString();
        } else {
            welldeveloped = "Nil";
        }
        if (etwelldeveloped.getText().toString().length() != 0) {
            edittext_welldeveloped = etwelldeveloped.getText().toString();
        } else {
            edittext_welldeveloped = "Nil";
        }

        patientobject.put("secondarysex_welldeveloped", welldeveloped + "," + edittext_welldeveloped);


        String hair, edittext_hair;
        if (cbhairno.isChecked()) {
            hair = cbhairno.getText().toString();
        } else if (cbhairyes.isChecked()) {
            hair = cbhairyes.getText().toString();
        } else {
            hair = "Nil";
        }
        if (ethair.getText().toString().length() != 0) {
            edittext_hair = ethair.getText().toString();
        } else {
            edittext_hair = "Nil";
        }

        patientobject.put("secondarysex_hair", hair + "," + edittext_hair);


        String acne, edittext_acne;
        if (cbacneno.isChecked()) {
            acne = cbacneno.getText().toString();
        } else if (cbacneyes.isChecked()) {
            acne = cbacneyes.getText().toString();
        } else {
            acne = "Nil";
        }
        if (etacne.getText().toString().length() != 0) {
            edittext_acne = etacne.getText().toString();
        } else {
            edittext_acne = "Nil";
        }

        patientobject.put("secondarysex_acne", acne + "," + edittext_acne);


        String secondarysex_other, edittext_other;
        if (cbsecondarysexotherno.isChecked()) {
            secondarysex_other = cbsecondarysexotherno.getText().toString();
        } else if (cbsecondarysexotheryes.isChecked()) {
            secondarysex_other = cbsecondarysexotheryes.getText().toString();
        } else {
            secondarysex_other = "Nil";
        }
        if (etsecondarysexother.getText().toString().length() != 0) {
            edittext_other = etsecondarysexother.getText().toString();
        } else {
            edittext_other = "Nil";
        }

        patientobject.put("secondarysex_other", secondarysex_other + "," + edittext_other);


        String abdomen, edittext_abdomen;
        if (cbpreabdomennormal.isChecked()) {
            abdomen = cbpreabdomennormal.getText().toString();
        } else if (cbpreabdomenabnormal.isChecked()) {
            abdomen = cbpreabdomenabnormal.getText().toString();
        } else {
            abdomen = "Nil";
        }
        if (etpreabdomen.getText().toString().length() != 0) {
            edittext_abdomen = etpreabdomen.getText().toString();
        } else {
            edittext_abdomen = "Nil";
        }

        patientobject.put("preabdomenexamination", abdomen + "," + edittext_abdomen);


        String healthy, edittext_healthy;
        if (cbhealthyno.isChecked()) {
            healthy = cbhealthyno.getText().toString();
        } else if (cbhealthyyes.isChecked()) {
            healthy = cbhealthyyes.getText().toString();
        } else {
            healthy = "Nil";
        }
        if (ethealthy.getText().toString().length() != 0) {
            edittext_healthy = ethealthy.getText().toString();
        } else {
            edittext_healthy = "Nil";
        }

        patientobject.put("cervix_healthy", healthy + "," + edittext_healthy);


        String bleeding, edittext_bleeing;
        if (cbbleedingno.isChecked()) {
            bleeding = cbbleedingno.getText().toString();
        } else if (cbbleedingyes.isChecked()) {
            bleeding = cbbleedingyes.getText().toString();
        } else {
            bleeding = "Nil";
        }
        if (etbleeding.getText().toString().length() != 0) {
            edittext_bleeing = etbleeding.getText().toString();
        } else {
            edittext_bleeing = "Nil";
        }

        patientobject.put("cervix_bleeding", bleeding + "," + edittext_bleeing);


        String lbc, edittext_lbc;
        if (cblbcno.isChecked()) {
            lbc = cblbcno.getText().toString();
        } else if (cblbcyes.isChecked()) {
            lbc = cblbcyes.getText().toString();
        } else {
            lbc = "Nil";
        }
        if (etlbc.getText().toString().length() != 0) {
            edittext_lbc = etlbc.getText().toString();
        } else {
            edittext_lbc = "Nil";
        }

        patientobject.put("cervix_lbc", lbc + "," + edittext_lbc);

        patientobject.put("uterus_avaf", tvavaf.getText().toString());
        patientobject.put("uterus_rvrf", tvrvrf.getText().toString());
        patientobject.put("uterus_others", etuterusothers.getText().toString());

        Calendar c = Calendar.getInstance();
        System.out.println("Current time => " + c.getTime());
        df = new SimpleDateFormat("dd-MMM-yyyy");
        formattedDate = df.format(c.getTime());


        Calendar c1 = Calendar.getInstance();
        System.out.println("Current time => " + c.getTime());
        SimpleDateFormat df1 = new SimpleDateFormat("dd-MMM-yyyy");
        SimpleDateFormat sdf1 = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss");

        currentDateandTime = c1.getTimeInMillis();

        ////////////////////////////set a Uniqe id for identify todays entry.

        if (((FragmentActivityContainer) getActivity()).getDiagnosisobject_id() != null) {
            patientobject.put("diagnosisId", ((FragmentActivityContainer) getActivity()).getDiagnosisobject_id());
        } else {
            patientobject.put("diagnosisId", Long.toString(currentDateandTime));
        }
        /////////////////////////////////////////////////////////////

        patientobject.put("createdDate", formattedDate);

        patientobject.put("createdatetime", currentDateandTime);

    /*    ///////////////////////////for LMP

        patientobject.put("Lmp_date", lmpdate_array);
        patientobject.put("Lmp_flow", lmpflow_array);
        patientobject.put("Lmp_dysmenorrhea", lmpdysmenorrhea_array);
        patientobject.put("Menstrual_Regular_Irregular", menstrualregularlist);
        patientobject.put("Menstrual_Days", menstrualdayslist);
        patientobject.put("Menstrual_Cycle", menstrualcyclelist);*/


        System.out.println("working......");


        patientobject.pinInBackground(new SaveCallback() {

                                          @Override
                                          public void done(ParseException e) {
                                              // TODO Auto-generated method stub
                                              mProgressDialog.dismiss();
                                              if (e == null) {
                                                  if (((FragmentActivityContainer) getActivity()).getDiagnosisobject_id() == null) {
                                                      ((FragmentActivityContainer) getActivity()).setDiagnosisobject_id(Long.toString(currentDateandTime));
                                                  }
                                                  submitButtonDeactivation();
                                                  Toast.makeText(getActivity(), "Successfully Saved", Toast.LENGTH_LONG).show();

                                                  FragmentActivityContainer.diag_save_check=0;
                                                  FragmentManager fragmentManager = getFragmentManager();
                                                  FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                                                  if (arg == 2) {
                                                      DiagnosisCreate_Gynacologist createDiagnosisFrag = new DiagnosisCreate_Gynacologist();
                                                      fragmentTransaction.replace(R.id.fl_fragment_container, createDiagnosisFrag);
                                                      fragmentTransaction.commit();
                                                  } else if (arg == 1) {
                                                      Diagnosisexamination_Gynacologist diagnosisExamination = new Diagnosisexamination_Gynacologist();
                                                      fragmentTransaction.replace(R.id.fl_fragment_container, diagnosisExamination);
                                                      fragmentTransaction.commit();
                                                  }else if(arg==3){
                                                      Patientpersonalinfo_Gynacoliogist diagnosisExamination = new Patientpersonalinfo_Gynacoliogist();
                                                      Bundle bundle2 = new Bundle();
                                                      bundle2.putBoolean("admintab", false);
                                                      bundle2.putBoolean("patientflag", true);
                                                      diagnosisExamination.setArguments(bundle2);
                                                      fragmentTransaction.replace(R.id.fl_fragment_container, diagnosisExamination);
                                                      fragmentTransaction.commit();
                                                  }


                                                  ((FragmentActivityContainer) getActivity()).setpatientdiagnosisparseObj(patientobject);
                                              } else

                                              {
                                                  e.printStackTrace();
                                              }


                                          }
                                      }

        );

        patientobject.saveEventually();

    }

	/*private void calculateBMI (EditText weight, EditText height) {

		if (height != null)
			heightInt = Double.parseDouble(!height.getText().toString().equals("")?
					height.getText().toString() : "0");

		if (weight != null)
			weightInt = Double.parseDouble(!weight.getText().toString().equals("")?
					weight.getText().toString() : "0");

//		float bmi=(float) (weightInt * 4.88 / (heightInt * heightInt));	
		float bmi =(float)(weightInt/((heightInt * 0.01)*(heightInt * 0.01)));
		etbmi.setText(Float.toString(bmi));

	}*/

    private void calculateBMI(EditText weight, EditText height) {
        try {

            if (height != null) {
                if (tvCmtoFtConvert.getText().toString().equalsIgnoreCase("cm")) {
                    heightInt = Double.parseDouble(!height.getText().toString().equals("") ?
                            height.getText().toString() : "0");
                } else {

                    double heightinFt = Double.parseDouble(!height.getText().toString().equals("") ?
                            height.getText().toString() : "0");
                    heightInt = heightinFt / 0.032808;
                }
            }

            if (weight != null)
                weightInt = Double.parseDouble(!weight.getText().toString().equals("") ?
                        weight.getText().toString() : "0");

//		float bmi=(float) (weightInt * 4.88 / (heightInt * heightInt));	
            float bmi = (float) (weightInt / ((heightInt * 0.01) * (heightInt * 0.01)));
            if (Float.toString(bmi).equalsIgnoreCase("infinity") || Float.toString(bmi).equalsIgnoreCase("nan"))
                etbmi.setText("0");
            else {
//		    etbmi.setText(Float.toString(bmi));
                etbmi.setText(String.format("%.2f", (double) bmi));

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

//	private void Set_Hint() {
//		// TODO Auto-generated method stub
//		etweight.setHint(Html.fromHtml( "</font>" + "<small>" + "Weight" + "</small>" ));
//		etheight.setHint(Html.fromHtml( "<small>" + "Height" + "</small>" ));
//		etsyspressure.setHint(Html.fromHtml( "<small>" + "Systolic" + "</small>" ));
//		etdiapressure.setHint(Html.fromHtml( "<small>" + "Diastolic" + "</small>" ));
//		etspo.setHint(Html.fromHtml( "<small>" + "SP O2" + "</small>" ));
//		ettemperature.setHint(Html.fromHtml( "<small>" + "Temperature" + "</small>" ));
//		etpulse.setHint(Html.fromHtml( "<small>" + "Pulse" + "</small>" ));
//		etrespiratory.setHint(Html.fromHtml( "<small>" + "Respiratory Rate" + "</small>" ));
//		etbmi.setHint(Html.fromHtml( "<small>" + "BMI" + "</small>" ));
//
//
//
//	}


}
