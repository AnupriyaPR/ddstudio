package com.wiinnova.doctorsdiary.fragment;

import android.app.Fragment;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.wiinnova.doctorsdiary.R;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Statistics extends Fragment{

	TextView doctername;
	TextView currDate;
	private DD_Frag ddfirst;
	/*protected BarChart mChart;
	protected String[] mMonths = new String[] {
			"21-24", "33-45", "46-50", "51-60", ">60"
	};
	*/
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub

		View view=inflater.inflate(R.layout.statistics, container, false);
		
		ddfirst = new  DD_Frag();
		Bundle bundle=new Bundle();
		bundle.putInt("sidemenuitem", 2);
		ddfirst.setArguments(bundle);
		getFragmentManager().beginTransaction()
		.replace(R.id.fl_sidemenu_container, ddfirst)
		.commit();
		
		Initialize_Components(view);
		setnamedate();
		/*mChart = (BarChart)view.findViewById(R.id.chart1);
		mChart.setPinchZoom(false);
		mChart.setDrawBorders(false);
		mChart.setDrawGridBackground(false);
		
		XAxis xl = mChart.getXAxis();
		xl.setPosition(XAxisPosition.BOTTOM);
		xl.setDrawAxisLine(false);
		xl.setDrawGridLines(false);
		xl.setGridLineWidth(0.1f);
		xl.setTextColor(Color.RED);
		
		YAxis yl = mChart.getAxisLeft();
		yl.setDrawAxisLine(false);
		yl.setDrawGridLines(false);
		yl.setDrawLabels(false);
		yl.setGridLineWidth(0.3f);

		//	        yl.setInverted(true);

		YAxis yr = mChart.getAxisRight();
		yr.setDrawAxisLine(false);
		yr.setDrawGridLines(false);
		//	        yr.setInverted(true);
		yr.setDrawLabels(false);

		setData(5, 50);
		mChart.setDescription("");*/

		return view;
	}

	private void Initialize_Components(View view) {
		// TODO Auto-generated method stub
		doctername=(TextView)view.findViewById(R.id.tvdoctername);
		currDate=(TextView)view.findViewById(R.id.tvdate);
	}

	/*private void setData(int count, float range) {
		// TODO Auto-generated method stub
		ArrayList<String> xVals = new ArrayList<String>();
		for (int i = 0; i < count; i++) {
			xVals.add(mMonths[i]);
		}

		ArrayList<BarEntry> yVals1 = new ArrayList<BarEntry>();

		for (int i = 0; i < count; i++) {
			float mult = (range + 1);
			float val = (float) (Math.random() * mult);
			yVals1.add(new BarEntry(val, i));
		}

		BarDataSet set1 = new BarDataSet(yVals1, "");
		set1.setBarSpacePercent(35f);
		set1.setDrawValues(false);


		ArrayList<BarDataSet> dataSets = new ArrayList<BarDataSet>();
		dataSets.add(set1);

		BarData data = new BarData(xVals,dataSets);
		data.setDrawValues(false);
		data.setValueTextSize(10f);
		//data.setValueTypeface(tf);

		mChart.setData(data);
	}*/
	private void setnamedate() {
		// TODO Auto-generated method stub
		SharedPreferences docter_name=getActivity().getSharedPreferences("Login", 0);
		String docter_first_name=docter_name.getString("fName", null);  
		String docter_last_name=docter_name.getString("lName", null);  
		
		doctername.setText(docter_first_name+" "+docter_last_name);
		

		Calendar c = Calendar.getInstance();
		System.out.println("Current time => " + c.getTime());
		SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
		String formattedDate = df.format(c.getTime());
		currDate.setText(formattedDate);
		
	}

}
