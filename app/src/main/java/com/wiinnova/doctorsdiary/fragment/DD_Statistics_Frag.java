package com.wiinnova.doctorsdiary.fragment;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.wiinnova.doctorsdiary.R;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class DD_Statistics_Frag extends Fragment {
	
	
	TextView flName,currDate;
	String drFname,drLname;
	
	
   @Override
   public View onCreateView(LayoutInflater inflater,
      ViewGroup container, Bundle savedInstanceState) {
	   
	   View myView = inflater.inflate(R.layout.dd_statistics_frag, container,false);
	   
	   DD_Frag. accset.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
		  DD_Frag.stati.setBackgroundColor(getResources().getColor(R.color.red_color));
		  DD_Frag.adminpat.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
		   
	   
	   
	   flName=(TextView)myView.findViewById(R.id.ddsfrtandlastname);
	   currDate=(TextView)myView.findViewById(R.id.ddscurrdate);
	   
	   
	   SharedPreferences sp1=getActivity().getSharedPreferences("Login", 0);

	   String unm=sp1.getString("Unm", null);       
	   String drflname = sp1.getString("drFLname", null);
	   
	   if(unm!=null){
		   
		 
		  
		   
		   flName.setText(drflname);
			
			Log.e("splash", "finish");
		   
	   }
	   Calendar c = Calendar.getInstance();
	   	System.out.println("Current time => " + c.getTime());
	   	SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
	   	String formattedDate = df.format(c.getTime());
	   	currDate.setText(formattedDate);
	   
	return myView;
   }
}