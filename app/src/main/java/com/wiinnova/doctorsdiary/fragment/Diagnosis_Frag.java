package com.wiinnova.doctorsdiary.fragment;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.wiinnova.doctorsdiary.R;
import com.parse.ParseObject;
import com.wiinnova.doctorsdiary.activities.FragmentActivityContainer;
import com.wiinnova.doctorsdiary.fragment.Diagnosis_Create_Frag.onbuttondiagnosis;



public class Diagnosis_Frag extends Fragment implements onbuttondiagnosis {

//   public interface ondiagnosiscreate {
//        void ondiagnosis(int arg);
//    }

    public static RelativeLayout createDiag;
    public static RelativeLayout viewDiag;
    RelativeLayout rlContacts;
    Button btnsubmit;
    //ImageView btnBack;
    ParseObject patientdiagnosisparseObj;

    Fragment createDiagnosisFrag;
    Diagnosis_View_Frag viewdiag;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.diagonsis_frag, null);

        ((FragmentActivityContainer) getActivity()).diagnosistabcolorchange();
        patientdiagnosisparseObj = ((FragmentActivityContainer) getActivity()).getpatientdiagnosisparseObj();
        createDiag = (RelativeLayout) view.findViewById(R.id.crtdia);

        viewDiag = (RelativeLayout) view.findViewById(R.id.viewdia);
        btnsubmit = (Button) view.findViewById(R.id.btnSubmit);
        //btnBack=(ImageView)view.findViewById(R.id.btnback);

        createDiag.setBackgroundColor(getResources().getColor(R.color.red_color));
        viewDiag.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));

        createDiagnosisFrag = ((FragmentActivityContainer) getActivity()).getdiagnosiscreatefrag();
        btnsubmit.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                ((FragmentActivityContainer) getActivity()).getdiagnosiscreatefrag().ondiagnosis(1);

            }
        });

	/*	btnBack.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				((FragmentActivityContainer)getActivity()).backpressedmethod();
			}
		});*/

        createDiag.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                createDiag.setBackgroundColor(getResources().getColor(R.color.red_color));
                viewDiag.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
                FragmentManager fragmentManager = getFragmentManager();

                createDiagnosisFrag = new Diagnosis_Create_Frag();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

                fragmentTransaction.replace(R.id.fl_fragment_container, createDiagnosisFrag);
                fragmentTransaction.commit();

				/*else{

					AlertDialog.Builder builder1 = new AlertDialog.Builder(getActivity());
					builder1.setTitle("SAVE ITEM");
					builder1.setMessage("Do you want save these items");
					builder1.setCancelable(true);
					builder1.setPositiveButton("Yes",
							new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id) {
							dialog.cancel();
						}
					});
					builder1.setNegativeButton("No",
							new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id) {

							FragmentActivityContainer.diag_save_check=0;

							createDiag.setBackgroundColor(getResources().getColor(R.color.red_color));
							viewDiag.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));


							android.support.v4.app.FragmentManager fragmentManager = getFragmentManager();

							Fragment creatediag=fragmentManager.findFragmentByTag("Frag11");
							if(((FragmentActivityContainer)getTargetFragment()).getdiagnosiscreatefrag()==null) {
								creatediag= new Diagnosis_Create_Frag();
							}else{
								creatediag=new Diagnosis_Create_Frag();

										//((FragmentActivityContainer)getTargetFragment()).getdiagnosiscreatefrag();
							}
							creatediag.setTargetFragment((Diagnosis_Frag.this).getTargetFragment(), 0);
							android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

							fragmentTransaction.replace(R.id.fragment_container22, creatediag);
							//fragmentTransaction.addToBackStack("Frag11");
							//fragmentTransaction.remove(Fragment);
							fragmentTransaction.commit();
						}
					});

					AlertDialog alert11 = builder1.create();
					alert11.show();

				}*/


            }
        });

        viewDiag.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                //if(FragmentActivityContainer.diag_save_check==0 ||FragmentActivityContainer.diag_save_check==1 ){

                createDiag.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
                viewDiag.setBackgroundColor(getResources().getColor(R.color.red_color));


                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                Diagnosis_View_Frag viewdiag = new Diagnosis_View_Frag();
                fragmentTransaction.replace(R.id.fl_fragment_container, viewdiag);
                fragmentTransaction.commit();

				/*}else{
                    AlertDialog.Builder builder1 = new AlertDialog.Builder(getActivity());
					builder1.setTitle("SAVE ITEM");
					builder1.setMessage("Do you want save these items");
					builder1.setCancelable(true);
					builder1.setPositiveButton("Yes",
							new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id) {

							((FragmentActivityContainer)getActivity()).getdiagnosiscreatefrag().ondiagnosis(1);

							dialog.cancel();
						}
					});
					builder1.setNegativeButton("No",
							new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id) {

							FragmentActivityContainer.diag_save_check=0;

							createDiag.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
							viewDiag.setBackgroundColor(getResources().getColor(R.color.red_color));


							android.support.v4.app.FragmentManager fragmentManager = getFragmentManager();
							android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
							Diagnosis_View_Frag viewdiag = new Diagnosis_View_Frag();
							fragmentTransaction.replace(R.id.fragment_container22, viewdiag);
							fragmentTransaction.addToBackStack("Frag12");
							//fragmentTransaction.remove(Fragment);
							fragmentTransaction.commit();
						}
					});

					AlertDialog alert11 = builder1.create();
					alert11.show();
				}*/
            }
        });
        return view;
    }

    @Override
    public void onDiagnosisbtn(int arg) {
        // TODO Auto-generated method stub
        System.out.println("arg value" + arg);
        if (Diagnosis_Frag.this.isVisible()) {
            System.out.println("arg value1" + arg);
            if (arg == 1) {
                btnsubmit.setBackgroundDrawable(getResources().getDrawable(R.drawable.submitbtn_background));
                btnsubmit.setTextColor(getResources().getColor(R.color.actionbar_color));
                btnsubmit.setEnabled(true);
                FragmentActivityContainer.diag_save_check = 2;
            }

            if (arg == 0) {
                btnsubmit.setBackgroundDrawable(getResources().getDrawable(R.drawable.submitbtn_background_beforesave));
                btnsubmit.setTextColor(getResources().getColor(R.color.grey));
                btnsubmit.setEnabled(false);
                FragmentActivityContainer.diag_save_check = 0;
            }
        }
    }
}