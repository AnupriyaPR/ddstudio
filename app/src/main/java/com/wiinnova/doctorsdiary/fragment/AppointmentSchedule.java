package com.wiinnova.doctorsdiary.fragment;

import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.wiinnova.doctorsdiary.R;
import com.parse.FindCallback;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.wiinnova.doctorsdiary.supportclasses.SearchAppointmentdetailsAdapter;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;

public class AppointmentSchedule extends Fragment{


	SimpleDateFormat df;
	SimpleDateFormat dateformat;
	private String current;
	private SimpleDateFormat dateFormatter1;
	private Date currentdate;
	ImageView iv_date;
	private DatePickerDialog startDatePickerDialog;
	private DateFormat dateFormatter;
	TextView tv_date;
	ListView lvAppointdetails;
	int flag;
	TextView tvDate;
	ProgressDialog mProgressdialog;
	TextView tvDoctername;

	ArrayList<String> visitdate=new ArrayList<String>();
	ArrayList<String> lastvisitdate=new ArrayList<String>();
	ArrayList<String> patientid=new ArrayList<String>();

	private int noOfTimesCalled=0;
	SearchAppointmentdetailsAdapter adapter;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View v=inflater.inflate(R.layout.appointment_schedule, null);


		dateFormatter1 = new SimpleDateFormat("dd-MM-yyyy");
		dateFormatter= new SimpleDateFormat("dd-MMM-yyyy");
		dateformat=new SimpleDateFormat("dd/MM/yyyy");

		tvDoctername=(TextView)v.findViewById(R.id.tvdrname);
		tvDate=(TextView)v.findViewById(R.id.tvdate);
		iv_date=(ImageView)v.findViewById(R.id.ivstartdate);
		tv_date=(TextView)v.findViewById(R.id.startDate);
		lvAppointdetails=(ListView)v.findViewById(R.id.search_old_record_result_item_list);

		View headerView = ((LayoutInflater)getActivity().getSystemService(getActivity().LAYOUT_INFLATER_SERVICE)).inflate(R.layout.appoint_details_list_header, null, false);
		lvAppointdetails.addHeaderView(headerView);	

		Calendar c = Calendar.getInstance();
		System.out.println("Current time => " + c.getTime());
		df = new SimpleDateFormat("dd-MMM-yyyy");
		current = df.format(c.getTime());
		try {
			currentdate=(Date)dateFormatter1.parse(current);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


		SharedPreferences docter_name=getActivity().getSharedPreferences("Login", 0);
		String docter_first_name=docter_name.getString("fName", null);  
		String docter_last_name=docter_name.getString("lName", null);  

		tvDoctername.setText(docter_first_name+" "+docter_last_name);



		tvDate.setText(current);








		iv_date.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub

				startDatePickerDialog.show();

			}
		});
		
		tv_date.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				startDatePickerDialog.show();
			}
		});
		


		Calendar newCalendar = Calendar.getInstance();
		startDatePickerDialog = new DatePickerDialog(getActivity(), new OnDateSetListener() {

			private Calendar newDate;



			public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {


				newDate = Calendar.getInstance();
				newDate.set(year, monthOfYear, dayOfMonth);				
				tv_date.setText(dateFormatter.format(newDate.getTime()));

				/*if(noOfTimesCalled%2==0){*/
				if(view.isShown())
					finddatefollowup(tv_date.getText().toString());
				/*}*/

				//System.out.println("noOfTimesCalled"+noOfTimesCalled);

				/*noOfTimesCalled++;*/
			}

		},newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

		return v;
	}


	private void finddatefollowup(String followup) {
		// TODO Auto-generated method stub

		System.out.println("followups"+followup);
		
		lastvisitdate.clear();
		patientid.clear();
		flag=0;

		mProgressdialog=new ProgressDialog(getActivity());
		mProgressdialog.setMessage("Please Wait");
		mProgressdialog.show();

		ParseQuery<ParseObject> query= ParseQuery.getQuery("Diagnosis");
		query.fromLocalDatastore();
		query.whereEqualTo("followup", followup);
		query.findInBackground(new FindCallback<ParseObject>() {

			@Override
			public void done(List<ParseObject> objects, com.parse.ParseException e) {
				// TODO Auto-generated method stub

				if(e==null){
					System.out.println("patientid size"+patientid.size());

					if(objects.size()>0){
						for(int i=0;i<objects.size();i++){
							System.out.println("objects size"+objects.size());
							String patientIdValue=objects.get(i).getString("patientID");
							patientid.add(patientIdValue);
						}

						for(int z=0;z<patientid.size();z++){
							findlastvisitdate(patientid.get(z));
						}


					}else{
						mProgressdialog.dismiss();
						patientid.clear();
						lastvisitdate.clear();
						List<String> firstname=new ArrayList<String>();
						List<String> lastname=new ArrayList<String>();
						List<String> phonenumber=new ArrayList<String>();
						List<String> email=new ArrayList<String>();

						adapter=new SearchAppointmentdetailsAdapter(getActivity(), patientid, firstname, lastname, lastvisitdate, phonenumber, email);
						lvAppointdetails.setAdapter(adapter);

						
						Toast.makeText(getActivity(), "No Appointment", Toast.LENGTH_LONG).show();
					}
				}
			}
		});
	}

	private void findlastvisitdate(String patientIdValue) {
		// TODO Auto-generated method stub
		visitdate.clear();
		System.out.println("lastvisit enterd");
		ParseQuery<ParseObject> query1= ParseQuery.getQuery("Diagnosis");
		query1.fromLocalDatastore();
		query1.whereEqualTo("patientID", patientIdValue);
		query1.findInBackground(new FindCallback<ParseObject>() {

			@Override
			public void done(List<ParseObject> object, com.parse.ParseException e) {
				// TODO Auto-generated method stub
				if(e==null){
					System.out.println("lastvisit size"+lastvisitdate.size());
					System.out.println("visitdate size"+visitdate.size());
					System.out.println("working2....");
					if(object.size()>0){

						System.out.println("working3....");
						for(int j=0;j<object.size();j++){
							visitdate.add(object.get(j).getString("createdDate"));
							System.out.println("visit date working....");

						}
						flag++;
						System.out.println("flag value...."+flag);
						lastvisitdate.add(Collections.max(visitdate));
						visitdate.clear();

						if(flag==patientid.size()){
							fethpatientdetails();
						}
					}else{
						mProgressdialog.dismiss();
						System.out.println("no element");
					}

				}else{
					e.printStackTrace();
				}

			}

		});

	}

	private void fethpatientdetails() {
		// TODO Auto-generated method stub

		final ArrayList<String> firstname=new ArrayList<String>();
		final ArrayList<String> lastname=new ArrayList<String>();
		final ArrayList<String> phonenumber=new ArrayList<String>();
		final ArrayList<String> email=new ArrayList<String>();

		ParseQuery<ParseObject> query2= ParseQuery.getQuery("Patients");
		query2.fromLocalDatastore();
		query2.whereContainedIn("patientID", patientid);
		query2.findInBackground(new FindCallback<ParseObject>() {

			@Override
			public void done(List<ParseObject> objects, com.parse.ParseException e) {
				// TODO Auto-generated method stub
				mProgressdialog.dismiss();
				if(e==null){
					System.out.println("working1....");
					if(objects.size()>0){
						System.out.println("working....");
						for(int k=0;k<objects.size();k++){

							firstname.add(objects.get(k).getString("firstName"));
							lastname.add(objects.get(k).getString("lastName"));
							phonenumber.add(objects.get(k).getString("phoneNumber"));
							email.add(objects.get(k).getString("email"));
						}
						adapter=new SearchAppointmentdetailsAdapter(getActivity(), patientid, firstname, lastname, lastvisitdate, phonenumber, email);
						lvAppointdetails.setAdapter(adapter);

						/*patientid.clear();
						firstname.clear();
						lastname.clear();
						lastvisitdate.clear();
						phonenumber.clear();
						email.clear();*/


					}else{
						mProgressdialog.dismiss();
					}
				}


			}
		});

	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		CrashReporter.getInstance().trackScreenView("Appointmentn Schedule");
	}
	
}
