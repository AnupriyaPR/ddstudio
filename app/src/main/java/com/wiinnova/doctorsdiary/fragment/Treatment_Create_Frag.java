package com.wiinnova.doctorsdiary.fragment;


import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import org.json.JSONArray;
import org.json.JSONException;

import com.wiinnova.doctorsdiary.R;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.SaveCallback;
import com.wiinnova.doctorsdiary.activities.FragmentActivityContainer;
import com.wiinnova.doctorsdiary.activities.Home_Page_Activity;
import com.wiinnova.doctorsdiary.fragment.Treatment_Frag.onPrescriptionprint;
import com.wiinnova.doctorsdiary.fragment.Treatment_Frag.ontreatmentfrag;
import com.wiinnova.doctorsdiary.popupwindow.DaysSelector_Popup;
import com.wiinnova.doctorsdiary.popupwindow.Dyanamicday_selectorpopup;
import com.wiinnova.doctorsdiary.popupwindow.Dyanamicday_selectorpopup.onDyanamicDaysSelectListener;
import com.wiinnova.doctorsdiary.popupwindow.SuspectedDiseaseSelectorPopup;
import com.wiinnova.doctorsdiary.popupwindow.DaysSelector_Popup.onDaysSelectListener;
import com.wiinnova.doctorsdiary.popupwindow.SpinnerPopup;
import com.wiinnova.doctorsdiary.popupwindow.SpinnerPopup.onSubmitListener;
import com.wiinnova.doctorsdiary.supportclasses.AppUtil;
import com.wiinnova.doctorsdiary.supportclasses.ArrayAdapterDrug;
import com.wiinnova.doctorsdiary.supportclasses.DatePickerDialogWithMaxMinRange;
import com.wiinnova.doctorsdiary.supportclasses.Diseasenameclass;
import com.wiinnova.doctorsdiary.supportclasses.FileOperations;
import com.wiinnova.doctorsdiary.supportclasses.FileOperationsGynacologist;
import com.wiinnova.doctorsdiary.supportclasses.FileOperationsNew;
import com.wiinnova.doctorsdiary.supportclasses.PrintHelper;
import com.wiinnova.doctorsdiary.supportclasses.QuantitySelector_Popup;
import com.wiinnova.doctorsdiary.supportclasses.Quantitynameclass;
import com.wiinnova.doctorsdiary.supportclasses.QuantitySelector_Popup.onQuantitySubmitListener;

import android.R.integer;
import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Environment;
import android.print.PrintManager;
import android.support.v4.content.ContextCompat;
import android.text.AlteredCharSequence;
import android.text.Editable;
import android.text.Html;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.MultiAutoCompleteTextView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class Treatment_Create_Frag extends Fragment implements View.OnClickListener, ontreatmentfrag, Serializable, onPrescriptionprint {

    public interface ontreatmentbtnvisible {
        void onTreatmentbtn(int arg);
    }

    ArrayList<String> arrDrug = new ArrayList<>();

    public static JSONArray drugObj = new JSONArray();
    public static JSONArray dosageObj = new JSONArray();
    public static JSONArray dateObj = new JSONArray();
    public static JSONArray durationObj = new JSONArray();
    public static JSONArray QuantityObj = new JSONArray();
    public static JSONArray daysObj = new JSONArray();
    ListView lstDrug;

    //DatePickerDialogWithMaxMinRange datePickerDialog= null;
    DatePickerDialog.OnDateSetListener datePickerOnDateSetListener;
    List<ParseObject> _suspecteddiseasenamesObj;
    ArrayList<Diseasenameclass> diseasenamearraylist = new ArrayList<Diseasenameclass>();
    ArrayList<Quantitynameclass> quantitynamearraylist = new ArrayList<Quantitynameclass>();
    ParseObject patient;
    ParseObject patientcurrentvitalsdetails;

    ScrollView topscrollview;
    private boolean savedflag = false;
    ProgressDialog progressdialog;
    String currentdate;
    LinearLayout firstrow_days;
    LinearLayout secondrow_days;
    LinearLayout thirdrow_days;
    LinearLayout fourthrow_days;
    String[] quantity_array;
    QuantitySelector_Popup spObj;
    Button btnSubmit;
    EditText et_Treatment;
    Button ctmClear, ctmSave;
    TextView startDate, endDate, fllupDate, currDate;
    TextView fltmName, uniqID;
    public static String vpuid;
    String formattedDate;
    EditText createDrag, createDosage, createQnty, createAddinfo;
    private DatePickerDialog startDatePickerDialog, startDatePickerDialog1, startDatePickerDialog2, startDatePickerDialog3, startDatePickerDialog4, startDatePickerDialog5, endDatePickerDialog, fllupDatePickerDialog;
    private SimpleDateFormat dateFormatter;
    //DatePickerDialog1964 b;
    ProgressDialog mProgressDialog;
    String uniId, ctmdrug, ctmdosafge, ctmqnty, ctmaddinfo, ctmStDate, ctmEddate, ctmFUDate, ctmDate;
    int flag;
    DaysSelector_Popup daysselectObj;
    int position = -1;

    String[] selectedQuantity_first;
    String[] selectedQuantity_second;
    String[] selectedQuantity_third;
    String[] selectedQuantity_fourth;
    String[] selectedQuantity_fifth;

    String[] days_array;
    String[] days_array1;
    String[] days_array2;
    String[] days_array3;
    String[] days_array4;
    String[] days_array5;
    String[] days_array_dyanamic;
    String[] checkbox_days_array_dyanamic;

    String[] days_array11;
    String[] days_array22;
    String[] days_array33;
    String[] days_array44;
    String[] days_array55;

    ArrayList<String> selected_days = new ArrayList<String>();
    ArrayList<Integer> removedindex = new ArrayList<Integer>();

    ParseObject patienttreatmentparseObj;


    String drug_name, drug_dosage, drug_startdate, drug_duration, drug_quantity, drug_name1, drug_dosage1, drug_startdate1, drug_duration1, drug_quantity1, drug_name2, drug_dosage2, drug_startdate2, drug_duration2, drug_quantity2, drug_name3, drug_dosage3, drug_startdate3, drug_duration3, drug_quantity3;
    //New variables

    String[] days;
    String[] days1;
    String[] days2;
    String[] days3;

//    ImageView iv_mon_1;
//    ImageView iv_tue_1;
//    ImageView iv_wed_1;
//    ImageView iv_thu_1;
//    ImageView iv_fri_1;
//    ImageView iv_sat_1;
//    ImageView iv_sun_1;
//
//    ImageView iv_mon_2;
//    ImageView iv_tue_2;
//    ImageView iv_wed_2;
//    ImageView iv_thu_2;
//    ImageView iv_fri_2;
//    ImageView iv_sat_2;
//    ImageView iv_sun_2;
//
//    ImageView iv_mon_3;
//    ImageView iv_tue_3;
//    ImageView iv_wed_3;
//    ImageView iv_thu_3;
//    ImageView iv_fri_3;
//    ImageView iv_sat_3;
//    ImageView iv_sun_3;
//
//    ImageView iv_mon_4;
//    ImageView iv_tue_4;
//    ImageView iv_wed_4;
//    ImageView iv_thu_4;
//    ImageView iv_fri_4;
//    ImageView iv_sat_4;
//    ImageView iv_sun_4;


    String treatment;
    int datepickerposition;
    int frequencyPosition;
    String dailyArray1[];
    String dailyArray2[];
    String dailyArray3[];
    String dailyArray4[];
    String dailyArray5[];
    String dailyArray6[];
    String dailyArray7[];


    public static ArrayList<String> drugname = new ArrayList<String>();
    public static ArrayList<String> drugdosage = new ArrayList<String>();
    public static ArrayList<String> drugstartdate = new ArrayList<String>();
    public static ArrayList<String> drugduration = new ArrayList<String>();
    public static ArrayList<String> drugquantity = new ArrayList<String>();

    JSONArray drugnamearray;
    JSONArray drugdosagearray;
    JSONArray drugstartdatearray;
    JSONArray drugdurationarray;
    JSONArray drugquantityarray;
    JSONArray listdaysarray;


    String selecteddays[];

    LinearLayout addDrugs;
    LinearLayout llMoreDrugs;

    TextView tvStartdate;
    TextView tvStartdate1;
    TextView tvStartdate2;
    TextView tvStartdate3;
    TextView tvFollowup;

    AutoCompleteTextView etdrug1;
    EditText etdosage1;
    EditText etduration1;
    //CheckBox cbdaily1;
    TextView srquantity1;

    AutoCompleteTextView etdrug2;
    EditText etdosage2;
    EditText etduration2;
    //CheckBox cbdaily2;
    TextView srquantity2;

    AutoCompleteTextView etdrug3;
    EditText etdosage3;
    EditText etduration3;
    //CheckBox cbdaily3;
    TextView srquantity3;

    AutoCompleteTextView etdrug4;
    EditText etdosage4;
    EditText etduration4;
    //CheckBox cbdaily4;
    TextView srquantity4;

    EditText etTreatment;
    ImageView ivPrint;
    int i = 3, j = 5, index = 5, dateviewindex = -1;

    int dayindex = 0, checkboxtag = 0, quantitytag = 0;
    int imagedaystag = 4;
    int positionval = 3;

    LinearLayout topcontainer;
    ScrollView svcontainer;
    ArrayList<String> dynamicalselectdays = new ArrayList<String>();

    public static ArrayList<EditText> AlDrugs = new ArrayList<EditText>();
    public static ArrayList<EditText> AlDosage = new ArrayList<EditText>();
    public static ArrayList<TextView> AlDateView = new ArrayList<TextView>();
    /*ArrayList<ImageView> AlDatePicker=new ArrayList<ImageView>();*/
    public static ArrayList<EditText> AlDuration = new ArrayList<EditText>();
    public static ArrayList<CheckBox> AlDays = new ArrayList<CheckBox>();
    public static ArrayList<TextView> AlQuantity = new ArrayList<TextView>();


    ArrayList<String> List_Dynamic_Days = new ArrayList<String>();

//    ArrayList<ArrayList<ImageView>> AlDaysList = new ArrayList<ArrayList<ImageView>>();

    //new

    HashMap<Integer, String[]> daysSelectedArray = new HashMap<Integer, String[]>();
    HashMap<Integer, String[]> quantitySelectedArray = new HashMap<Integer, String[]>();

    ArrayList<String> Selecteddaysdyanamic = new ArrayList<String>();
    ArrayList<String> Selectedquantitydyanamic = new ArrayList<String>();

    String Selecteddays_first = "";
    String Selecteddays_second = "";
    String Selecteddays_third = "";
    String Selecteddays_fourth = "";
    String Selecteddays_dyanamic = "";


    String Selectedquantity_first = "";
    String Selectedquantity_second = "";
    String Selectedquantity_third = "";
    String Selectedquantity_fourth = "";
    String Selectedquantity_dyanamic = "";


    protected int pos = 0;

    private onDaysSelectListener daysSelectedListener;

    private onQuantitySubmitListener quantitySelectedListener;

    private LinearLayout maincontainer;

    private Treatment_Frag treatementfrag;

    public Treatment_Frag treatmentfragObj;

    private String seconds;

    private long currentDateandTime;
    private String doctername;
    private String specialization;
    private ProgressDialog mgProgressDialog;

    ParseObject patientpersonalInformation;
    public static List<ParseObject> _diseasenamesObj;
    private String[] diseasename;
    public ArrayAdapter adapter;
    private String treatmentObjectId;
    ArrayAdapterDrug adapterDrug;
    ArrayList<Integer> listCount;
    public static HashMap<Integer, View> map;

    @SuppressLint("NewApi")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.treatment_create_fragment, null);

		/*if(getFragmentManager().findFragmentById(R.id.fl_sidemenu_container) instanceof Treatment_Frag){

			((FragmentActivityContainer)getActivity()).getprescription_create_colorchange();

		}else{*/


        SharedPreferences sp = getActivity().getSharedPreferences("Login", 0);
        specialization = sp.getString("spel", null);


        if (!specialization.equalsIgnoreCase("gynecologist obstetrician")) {

            treatementfrag = new Treatment_Frag();
            getFragmentManager().beginTransaction()
                    .replace(R.id.fl_sidemenu_container, treatementfrag)
                    .commit();

            if (treatementfrag != null) {
                ((FragmentActivityContainer) getActivity()).settreatmentfrag(treatementfrag);
            }

            ((FragmentActivityContainer) getActivity()).gettreatmentfrag().onTreatmentmanagementbtn(0);
        } else {

            treatementfrag = new Treatment_Frag();
            treatmentfragObj = ((FragmentActivityContainer) getActivity()).gettreatmentfrag();
            if (treatmentfragObj == null) {
                treatmentfragObj = new Treatment_Frag();
                getFragmentManager().beginTransaction()
                        .replace(R.id.fl_sidemenu_container, treatementfrag)
                        .commit();

            }
        }


        if (specialization.equalsIgnoreCase("gynecologist obstetrician")) {
            treatmentfragObj = ((FragmentActivityContainer) getActivity()).gettreatmentfrag();
            if (treatmentfragObj != null)
                ((FragmentActivityContainer) getActivity()).gettreatmentfrag().medicinecolorchange();
        }

        quantity_array = getResources().getStringArray(R.array.quantity_array);

        for (int i = 0; i < quantity_array.length; i++) {
            Quantitynameclass quantitynameObj = new Quantitynameclass();
            quantitynameObj.setQuantityname(quantity_array[i]);
            ;
            quantitynamearraylist.add(quantitynameObj);
        }

		/*}*/

        _suspecteddiseasenamesObj = ((CrashReporter) getActivity().getApplicationContext()).getDiseasenameObjects();

        if (_suspecteddiseasenamesObj != null) {

            diseasenamearraylist.clear();

            for (int i = 0; i < _suspecteddiseasenamesObj.size(); i++) {
                Diseasenameclass diseasenameObj = new Diseasenameclass();
                System.out.println("working..." + _suspecteddiseasenamesObj.get(i).getString("DiseaseName"));
                diseasenameObj.setDiseasename(_suspecteddiseasenamesObj.get(i).getString("DiseaseName"));
                diseasenameObj.setIcdnumber(_suspecteddiseasenamesObj.get(i).getString("ICDNumber"));
                diseasenamearraylist.add(diseasenameObj);
            }
        }


        ((FragmentActivityContainer) getActivity()).settreatmentcreatefrag(this);
        patienttreatmentparseObj = ((FragmentActivityContainer) getActivity()).getpatienttreatmentparseObj();
        FragmentActivityContainer.treatmangement_save_check = 0;


        firstrow_days = (LinearLayout) view.findViewById(R.id.days1_container);
        secondrow_days = (LinearLayout) view.findViewById(R.id.days2_container);
        thirdrow_days = (LinearLayout) view.findViewById(R.id.days3_container);
        fourthrow_days = (LinearLayout) view.findViewById(R.id.days4_container);

        llMoreDrugs = (LinearLayout) view.findViewById(R.id.addDrugs);
        svcontainer = (ScrollView) view.findViewById(R.id.svContainer);
        topcontainer = (LinearLayout) view.findViewById(R.id.topContainer);

        maincontainer = (LinearLayout) view.findViewById(R.id.rltopcontainer);
        uniqID = (TextView) view.findViewById(R.id.cduniqueid);
        currDate = (TextView) view.findViewById(R.id.ctmcurrdate);
        fltmName = (TextView) view.findViewById(R.id.tvfirst_last_name);
        et_Treatment = (EditText) view.findViewById(R.id.etTreatment);
        addDrugs = (LinearLayout) view.findViewById(R.id.ctml8);


//        iv_mon_1 = (ImageView) view.findViewById(R.id.ivdays1_monday);
//        iv_tue_1 = (ImageView) view.findViewById(R.id.ivdays1_tuesday);
//        iv_wed_1 = (ImageView) view.findViewById(R.id.ivdays1_wed);
//        iv_thu_1 = (ImageView) view.findViewById(R.id.ivdays1_thu);
//        iv_fri_1 = (ImageView) view.findViewById(R.id.ivdays1_fri);
//        iv_sat_1 = (ImageView) view.findViewById(R.id.ivdays1_sat);
//        iv_sun_1 = (ImageView) view.findViewById(R.id.ivdays1_sun);
//
//        iv_mon_2 = (ImageView) view.findViewById(R.id.ivdays2_monday);
//        iv_tue_2 = (ImageView) view.findViewById(R.id.ivdays2_tuesday);
//        iv_wed_2 = (ImageView) view.findViewById(R.id.ivdays2_wed);
//        iv_thu_2 = (ImageView) view.findViewById(R.id.ivdays2_thu);
//        iv_fri_2 = (ImageView) view.findViewById(R.id.ivdays2_fri);
//        iv_sat_2 = (ImageView) view.findViewById(R.id.ivdays2_sat);
//        iv_sun_2 = (ImageView) view.findViewById(R.id.ivdays2_sun);
//
//
//        iv_mon_3 = (ImageView) view.findViewById(R.id.ivdays3_monday);
//        iv_tue_3 = (ImageView) view.findViewById(R.id.ivdays3_tuesday);
//        iv_wed_3 = (ImageView) view.findViewById(R.id.ivdays3_wed);
//        iv_thu_3 = (ImageView) view.findViewById(R.id.ivdays3_thu);
//        iv_fri_3 = (ImageView) view.findViewById(R.id.ivdays3_fri);
//        iv_sat_3 = (ImageView) view.findViewById(R.id.ivdays3_sat);
//        iv_sun_3 = (ImageView) view.findViewById(R.id.ivdays3_sun);
//
//
//        iv_mon_4 = (ImageView) view.findViewById(R.id.ivdays4_monday);
//        iv_tue_4 = (ImageView) view.findViewById(R.id.ivdays4_tuesday);
//        iv_wed_4 = (ImageView) view.findViewById(R.id.ivdays4_wed);
//        iv_thu_4 = (ImageView) view.findViewById(R.id.ivdays4_thu);
//        iv_fri_4 = (ImageView) view.findViewById(R.id.ivdays4_fri);
//        iv_sat_4 = (ImageView) view.findViewById(R.id.ivdays4_sat);
//        iv_sun_4 = (ImageView) view.findViewById(R.id.ivdays4_sun);
//
//        firstrow_days.setOnClickListener(this);
//        secondrow_days.setOnClickListener(this);
//        thirdrow_days.setOnClickListener(this);
//        fourthrow_days.setOnClickListener(this);
//
//        iv_mon_1.setOnClickListener(this);
//        iv_tue_1.setOnClickListener(this);
//        iv_wed_1.setOnClickListener(this);
//        iv_thu_1.setOnClickListener(this);
//        iv_fri_1.setOnClickListener(this);
//        iv_sat_1.setOnClickListener(this);
//        iv_sun_1.setOnClickListener(this);
//
//        iv_mon_2.setOnClickListener(this);
//        iv_tue_2.setOnClickListener(this);
//        iv_wed_2.setOnClickListener(this);
//        iv_thu_2.setOnClickListener(this);
//        iv_fri_2.setOnClickListener(this);
//        iv_sat_2.setOnClickListener(this);
//        iv_sun_2.setOnClickListener(this);
//
//        iv_mon_3.setOnClickListener(this);
//        iv_tue_3.setOnClickListener(this);
//        iv_wed_3.setOnClickListener(this);
//        iv_thu_3.setOnClickListener(this);
//        iv_fri_3.setOnClickListener(this);
//        iv_sat_3.setOnClickListener(this);
//        iv_sun_3.setOnClickListener(this);
//
//        iv_mon_4.setOnClickListener(this);
//        iv_tue_4.setOnClickListener(this);
//        iv_wed_4.setOnClickListener(this);
//        iv_thu_4.setOnClickListener(this);
//        iv_fri_4.setOnClickListener(this);
//        iv_sat_4.setOnClickListener(this);
//        iv_sun_4.setOnClickListener(this);


        tvStartdate = (TextView) view.findViewById(R.id.ctmdrugdate);
        tvStartdate1 = (TextView) view.findViewById(R.id.ctmdrugdate51);
        tvStartdate2 = (TextView) view.findViewById(R.id.ctmdrugdate61);
        tvStartdate3 = (TextView) view.findViewById(R.id.ctmdrugdate71);


        etdrug1 = (AutoCompleteTextView) view.findViewById(R.id.ctmdrugedit);
        etdrug2 = (AutoCompleteTextView) view.findViewById(R.id.ctmdrugedit51);
        etdrug3 = (AutoCompleteTextView) view.findViewById(R.id.ctmdrugedit61);
        etdrug4 = (AutoCompleteTextView) view.findViewById(R.id.ctmdrugedit71);


        etdrug2.setHint(Html.fromHtml("<font size=\"12\">" + "Drugs" + "</font>" + "<small>" + "</small>"));
        etdrug3.setHint(Html.fromHtml("<font size=\"12\">" + "Drugs" + "</font>" + "<small>" + "</small>"));
        etdrug4.setHint(Html.fromHtml("<font size=\"12\">" + "Drugs" + "</font>" + "<small>" + "</small>"));

        etdosage1 = (EditText) view.findViewById(R.id.ctmdrugdosage);
        etdosage2 = (EditText) view.findViewById(R.id.ctmdrugdosage51);
        etdosage3 = (EditText) view.findViewById(R.id.ctmdrugdosage61);
        etdosage4 = (EditText) view.findViewById(R.id.ctmdrugdosage71);


        etdosage2.setHint(Html.fromHtml("<font size=\"12\">" + "Dosage" + "</font>" + "<small>" + "</small>"));
        etdosage3.setHint(Html.fromHtml("<font size=\"12\">" + "Dosage" + "</font>" + "<small>" + "</small>"));
        etdosage4.setHint(Html.fromHtml("<font size=\"12\">" + "Dosage" + "</font>" + "<small>" + "</small>"));


        etduration1 = (EditText) view.findViewById(R.id.ctmdrugduration);
        etduration2 = (EditText) view.findViewById(R.id.ctmdrugduration51);
        etduration3 = (EditText) view.findViewById(R.id.ctmdrugduration61);
        etduration4 = (EditText) view.findViewById(R.id.ctmdrugduration71);


        etduration2.setHint(Html.fromHtml("<font size=\"12\">" + "Duration" + "</font>" + "<small>" + "</small>"));
        etduration3.setHint(Html.fromHtml("<font size=\"12\">" + "Duration" + "</font>" + "<small>" + "</small>"));
        etduration4.setHint(Html.fromHtml("<font size=\"12\">" + "Duration" + "</font>" + "<small>" + "</small>"));

        srquantity1 = (TextView) view.findViewById(R.id.ctmdrugquantity);
        srquantity2 = (TextView) view.findViewById(R.id.ctmdrugquantity51);
        srquantity3 = (TextView) view.findViewById(R.id.ctmdrugquantity61);
        srquantity4 = (TextView) view.findViewById(R.id.ctmdrugquantity71);

        srquantity2.setHint(Html.fromHtml("<font size=\"12\">" + "Frequency" + "</font>" + "<small>" + "</small>"));
        srquantity3.setHint(Html.fromHtml("<font size=\"12\">" + "Frequency" + "</font>" + "<small>" + "</small>"));
        srquantity4.setHint(Html.fromHtml("<font size=\"12\">" + "Frequency" + "</font>" + "<small>" + "</small>"));


        _diseasenamesObj = ((CrashReporter) getActivity().getApplicationContext()).getMedicinenameObjects();

        diseasename = new String[_diseasenamesObj.size()];

        for (int k = 0; k < _diseasenamesObj.size(); k++) {
            diseasename[k] = _diseasenamesObj.get(k).getString("MedicineName");
        }

        adapter = new ArrayAdapter(getActivity(), android.R.layout.simple_list_item_1, diseasename);


        //////////////////////////////////Set listview for drugs
        lstDrug = (ListView) view.findViewById(R.id.lstDrug);
        listCount = new ArrayList<Integer>();
        for (int i = 0; i < 4; i++) {
            listCount.add(i);
            arrDrug.add("");
//            EditText ed = new EditText(getActivity());
//            AlDrugs.add(ed);
//            AlDosage.add(ed);
//            AlDateView.add(ed);
//            AlDuration.add(ed);
//            AlQuantity.add(ed);
        }
        List<ArrayList> array = new ArrayList<ArrayList>();
//        array.add(AlDrugs);
//        array.add(AlDosage);
//        array.add(AlDateView);
//        array.add(AlDuration);
//        array.add(AlQuantity);
        Log.d("AlDrugsSize", AlDrugs.size() + " fdg");
        adapterDrug = new ArrayAdapterDrug(getActivity(), listCount, this, array, arrDrug);
        lstDrug.setAdapter(adapterDrug);
        AppUtil.justifyListViewHeightBasedOnChildren(lstDrug);
        ////////////////////////////////////////////////////////

        etTreatment = (EditText) view.findViewById(R.id.etTreatment);
        tvFollowup = (TextView) view.findViewById(R.id.tvfollowup);
        //dateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
        dateFormatter = new SimpleDateFormat("dd-MMM-yyyy");
        SharedPreferences docter_name = getActivity().getSharedPreferences("Login", 0);
        String docter_first_name = docter_name.getString("fName", null);
        String docter_last_name = docter_name.getString("lName", null);

        doctername = docter_first_name + " " + docter_last_name;

        daysSelectedListener = new onDaysSelectListener() {

            @Override
            public void onDays(String[] outputStrArr, int selectedrow) {
                // TODO Auto-generated method stub
                doOnDaysSelected(outputStrArr, selectedrow);
            }
        };


        quantitySelectedListener = new onQuantitySubmitListener() {

            @Override
            public void onquantitySubmit(ArrayList<Quantitynameclass> outputStr, int positionvalue) {
                // TODO Auto-generated method stub

                System.out.println("position value@@@@@@@@" + positionvalue);

                String quantity = "";
                String[] outputStrArr = new String[outputStr.size()];
                for (int i = 0; i < outputStr.size(); i++) {

                    if (i != outputStr.size() - 1)
                        quantity += outputStr.get(i).getQuantityname() + ",";
                    else
                        quantity += outputStr.get(i).getQuantityname();

                    outputStrArr[i] = outputStr.get(i).getQuantityname();
                }
//                View v = lstDrug.getChildAt(frequencyPosition);
//                TextView txtFreq = (TextView) v.findViewById(R.id.srQuantity);
//                txtFreq.setText(quantity);

                quantitySelectedArray.put(frequencyPosition, outputStrArr);
                try {
                    QuantityObj.put(frequencyPosition, quantity);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (flag == 0) {

                    Selectedquantity_first = "";
                    selectedQuantity_first = outputStrArr;
                    for (int i = 0; i < selectedQuantity_first.length; i++) {
                        Selectedquantity_first += selectedQuantity_first[i] + ",";

                    }

                    srquantity1.setText(quantity);
                }
                if (flag == 1) {

                    Selectedquantity_second = "";
                    selectedQuantity_second = outputStrArr;
                    for (int i = 0; i < selectedQuantity_second.length; i++) {
                        Selectedquantity_second += selectedQuantity_second[i] + ",";
                    }

                    srquantity2.setText(quantity);
                }
                if (flag == 2) {

                    Selectedquantity_third = "";
                    selectedQuantity_third = outputStrArr;
                    for (int i = 0; i < selectedQuantity_third.length; i++) {
                        Selectedquantity_third += selectedQuantity_third[i] + ",";
                    }

                    srquantity3.setText(quantity);
                }
                if (flag == 3) {

                    Selectedquantity_fourth = "";
                    selectedQuantity_fourth = outputStrArr;
                    for (int i = 0; i < selectedQuantity_fourth.length; i++) {
                        Selectedquantity_fourth += selectedQuantity_fourth[i] + ",";
                    }
                    srquantity4.setText(quantity);
                }
                if (flag == 4) {
                    for (int i = 0; i < AlQuantity.size(); i++) {
                        int index = (Integer) AlQuantity.get(i).getTag() + 1;
                        System.out.println("quantity tag" + AlQuantity.get(i).getTag().toString());
                        if (Integer.parseInt(AlQuantity.get(i).getTag().toString()) == positionvalue) {
                            int hashmapindexvalue = positionvalue + 5;
                            AlQuantity.get(i).setText(quantity);
                            quantitySelectedArray.put(index, outputStrArr);
                        }
                    }


                }
                System.out.println("submitbutton activation working.......");
                submitButtonActivation();
                spObj.dismiss();
            }


        };


        srquantity1.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                System.out.println("selected quantity first" + selectedQuantity_first);

                flag = 0;
                Bundle bundle = new Bundle();
                bundle.putString("name", "Quantity");
                bundle.putInt("row", flag);
                bundle.putSerializable("quantiyname", quantitynamearraylist);
                bundle.putStringArray("selectedquantity", selectedQuantity_first);
                bundle.putStringArray("items", quantity_array);
                spObj = new QuantitySelector_Popup();
                spObj.setSubmitListener(quantitySelectedListener);
                spObj.setArguments(bundle);
                spObj.show(getFragmentManager(), "tag");
            }
        });

        srquantity2.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub

                System.out.println("selected quantity first" + selectedQuantity_second);
                flag = 1;

                Bundle bundle = new Bundle();
                bundle.putInt("row", flag);
                bundle.putString("name", "Quantity");
                bundle.putStringArray("items", quantity_array);
                bundle.putSerializable("quantiyname", quantitynamearraylist);
                bundle.putStringArray("selectedquantity", selectedQuantity_second);
                //bundle.putSerializable("listener",Treatment_Create_Frag.this);
                spObj = new QuantitySelector_Popup();
                spObj.setSubmitListener(quantitySelectedListener);
                spObj.setArguments(bundle);
                spObj.show(getFragmentManager(), "tag");
            }
        });

        srquantity3.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub

                flag = 2;
                Bundle bundle = new Bundle();
                bundle.putInt("row", flag);
                bundle.putString("name", "Quantity");
                bundle.putStringArray("items", quantity_array);
                bundle.putSerializable("quantiyname", quantitynamearraylist);
                bundle.putStringArray("selectedquantity", selectedQuantity_third);
                spObj = new QuantitySelector_Popup();
                spObj.setSubmitListener(quantitySelectedListener);
                spObj.setArguments(bundle);
                spObj.show(getFragmentManager(), "tag");
            }
        });

        srquantity4.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub

                flag = 3;
                Bundle bundle = new Bundle();
                bundle.putInt("row", flag);
                bundle.putString("name", "Quantity");
                bundle.putStringArray("items", quantity_array);
                bundle.putSerializable("quantiyname", quantitynamearraylist);
                bundle.putStringArray("selectedquantity", selectedQuantity_fourth);
                spObj = new QuantitySelector_Popup();
                spObj.setSubmitListener(quantitySelectedListener);
                spObj.setArguments(bundle);
                spObj.show(getFragmentManager(), "tag");
            }
        });


        etdrug1.setAdapter(adapter);
        //etdrug1.setTokenizer(new AutoCompleteTextView.CommaTokenizer());

        etdrug2.setAdapter(adapter);
        //.setTokenizer(new AutoCompleteTextView.CommaTokenizer());


        etdrug3.setAdapter(adapter);
        //etdrug3.setTokenizer(new AutoCompleteTextView.CommaTokenizer());


        etdrug4.setAdapter(adapter);
        //etdrug4.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());


        etdrug1.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                if (s.length() == 0) {
                    submitButtonDeactivation();
                } else if (start == 0 && before == 0 && count == 1) {
                    submitButtonActivation();
                }

                System.out.println("start" + start);
                System.out.println("before" + before);
                System.out.println("count" + count);
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }
        });

        etdrug2.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                if (s.length() == 0) {
                    submitButtonDeactivation();
                } else if (start == 0 && before == 0 && count == 1) {
                    submitButtonActivation();
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });


        etdrug3.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                if (s.length() == 0) {
                    submitButtonDeactivation();
                } else if (start == 0 && before == 0 && count == 1) {
                    submitButtonActivation();
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });


        etdrug3.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                if (s.length() == 0) {
                    submitButtonDeactivation();
                } else if (s.length() == 1) {
                    submitButtonActivation();
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });

        etdrug4.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                if (s.length() == 0) {
                    submitButtonDeactivation();
                } else if (s.length() == 1) {

                    submitButtonActivation();
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });

        etdosage1.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                if (s.length() == 0) {
                    submitButtonDeactivation();
                } else if (start == 0 && before == 0 && count == 1) {
                    submitButtonActivation();
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });

        etdosage2.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                if (s.length() == 0) {
                    submitButtonDeactivation();
                } else if (start == 0 && before == 0 && count == 1) {
                    submitButtonActivation();
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });

        etdosage3.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                if (s.length() == 0) {
                    submitButtonDeactivation();
                } else if (start == 0 && before == 0 && count == 1) {
                    submitButtonActivation();
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });

        etdosage4.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                if (s.length() == 0) {
                    submitButtonDeactivation();
                } else if (start == 0 && before == 0 && count == 1) {
                    submitButtonActivation();
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });

        etduration1.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                if (s.length() == 0) {
                    submitButtonDeactivation();
                } else if (start == 0 && before == 0 && count == 1) {
                    submitButtonActivation();
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });

        etduration2.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                if (s.length() == 0) {
                    submitButtonDeactivation();
                } else if (start == 0 && before == 0 && count == 1) {
                    submitButtonActivation();
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });

        etduration3.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                if (s.length() == 0) {
                    submitButtonDeactivation();
                } else if (start == 0 && before == 0 && count == 1) {
                    submitButtonActivation();
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });

        etduration4.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                if (s.length() == 0) {
                    submitButtonDeactivation();
                } else if (start == 0 && before == 0 && count == 1) {
                    submitButtonActivation();
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });

        tvStartdate.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                if (s.length() == 0) {
                    submitButtonDeactivation();
                } else if (start == 0 && before == 0 && count == 10) {
                    submitButtonActivation();
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });


        tvStartdate1.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                if (s.length() == 0) {
                    submitButtonDeactivation();
                } else if (start == 0 && before == 0 && count == 10) {
                    submitButtonActivation();
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });


        tvStartdate2.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                if (s.length() == 0) {
                    submitButtonDeactivation();
                } else if (start == 0 && before == 0 && count == 10) {
                    submitButtonActivation();
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });


        tvStartdate3.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                if (s.length() == 0) {
                    submitButtonDeactivation();
                } else if (start == 0 && before == 0 && count == 10) {
                    submitButtonActivation();
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });


        etTreatment.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                if (s.length() == 0) {
                    submitButtonDeactivation();
                } else if (start == 0 && before == 0 && count == 1) {
                    submitButtonActivation();
                }

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });

        tvFollowup.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                if (s.length() == 0) {
                    submitButtonDeactivation();
                } else if (start == 0 && before == 0 && count == 10) {
                    submitButtonActivation();
                }
                System.out.println("tvfollowup" + start + before + count);
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });


        Calendar newCalendar = Calendar.getInstance();

        Calendar cal = Calendar.getInstance();

        startDatePickerDialog = new DatePickerDialog(getActivity(), new OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                Log.d("DatePickerPosition", "111111111");
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                tvStartdate.setText(dateFormatter.format(newDate.getTime()));
            }
        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
        //startDatePickerDialog.getDatePicker().setMinDate(Calendar.getInstance().getTimeInMillis()- 1000);
        cal.set(Calendar.HOUR_OF_DAY, cal.getMinimum(Calendar.HOUR_OF_DAY));
        cal.set(Calendar.MINUTE, cal.getMinimum(Calendar.MINUTE));
        cal.set(Calendar.SECOND, cal.getMinimum(Calendar.SECOND));
        cal.set(Calendar.MILLISECOND, cal.getMinimum(Calendar.MILLISECOND));
        startDatePickerDialog.getDatePicker().setMinDate(cal.getTimeInMillis());
        /*	cal.add(Calendar.YEAR, 85);
        cal.set(Calendar.HOUR_OF_DAY, cal.getMaximum(Calendar.HOUR_OF_DAY));
		cal.set(Calendar.MINUTE, cal.getMaximum(Calendar.MINUTE));
		cal.set(Calendar.SECOND, cal.getMaximum(Calendar.SECOND));
		cal.set(Calendar.MILLISECOND, cal.getMaximum(Calendar.MILLISECOND));
		startDatePickerDialog.getDatePicker().setMaxDate(cal.getTimeInMillis());*/

        startDatePickerDialog.getDatePicker().setCalendarViewShown(false);


        startDatePickerDialog1 = new DatePickerDialog(getActivity(), new OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Log.d("DatePickerPosition", "2222222");
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                tvStartdate1.setText(dateFormatter.format(newDate.getTime()));
            }
        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
        //startDatePickerDialog1.getDatePicker().setMinDate(Calendar.getInstance().getTimeInMillis()- 1000);
        cal.set(Calendar.HOUR_OF_DAY, cal.getMinimum(Calendar.HOUR_OF_DAY));
        cal.set(Calendar.MINUTE, cal.getMinimum(Calendar.MINUTE));
        cal.set(Calendar.SECOND, cal.getMinimum(Calendar.SECOND));
        cal.set(Calendar.MILLISECOND, cal.getMinimum(Calendar.MILLISECOND));
        startDatePickerDialog1.getDatePicker().setMinDate(cal.getTimeInMillis());
        /*	cal.add(Calendar.YEAR, 85);
        cal.set(Calendar.HOUR_OF_DAY, cal.getMaximum(Calendar.HOUR_OF_DAY));
		cal.set(Calendar.MINUTE, cal.getMaximum(Calendar.MINUTE));
		cal.set(Calendar.SECOND, cal.getMaximum(Calendar.SECOND));
		cal.set(Calendar.MILLISECOND, cal.getMaximum(Calendar.MILLISECOND));
		startDatePickerDialog.getDatePicker().setMaxDate(cal.getTimeInMillis());*/

        startDatePickerDialog1.getDatePicker().setCalendarViewShown(false);


        startDatePickerDialog2 = new DatePickerDialog(getActivity(), new OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Log.d("DatePickerPosition", "3333333");
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                tvStartdate2.setText(dateFormatter.format(newDate.getTime()));
            }
        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
        //startDatePickerDialog2.getDatePicker().setMinDate(Calendar.getInstance().getTimeInMillis()- 1000);
        cal.set(Calendar.HOUR_OF_DAY, cal.getMinimum(Calendar.HOUR_OF_DAY));
        cal.set(Calendar.MINUTE, cal.getMinimum(Calendar.MINUTE));
        cal.set(Calendar.SECOND, cal.getMinimum(Calendar.SECOND));
        cal.set(Calendar.MILLISECOND, cal.getMinimum(Calendar.MILLISECOND));
        startDatePickerDialog2.getDatePicker().setMinDate(cal.getTimeInMillis());
        /*	cal.add(Calendar.YEAR, 85);
        cal.set(Calendar.HOUR_OF_DAY, cal.getMaximum(Calendar.HOUR_OF_DAY));
		cal.set(Calendar.MINUTE, cal.getMaximum(Calendar.MINUTE));
		cal.set(Calendar.SECOND, cal.getMaximum(Calendar.SECOND));
		cal.set(Calendar.MILLISECOND, cal.getMaximum(Calendar.MILLISECOND));
		startDatePickerDialog.getDatePicker().setMaxDate(cal.getTimeInMillis());*/

        startDatePickerDialog2.getDatePicker().setCalendarViewShown(false);


        startDatePickerDialog3 = new DatePickerDialog(getActivity(), new OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Log.d("DatePickerPosition", "4444444444444444");
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                tvStartdate3.setText(dateFormatter.format(newDate.getTime()));
            }
        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
        //startDatePickerDialog3.getDatePicker().setMinDate(Calendar.getInstance().getTimeInMillis()- 1000);
        cal.set(Calendar.HOUR_OF_DAY, cal.getMinimum(Calendar.HOUR_OF_DAY));
        cal.set(Calendar.MINUTE, cal.getMinimum(Calendar.MINUTE));
        cal.set(Calendar.SECOND, cal.getMinimum(Calendar.SECOND));
        cal.set(Calendar.MILLISECOND, cal.getMinimum(Calendar.MILLISECOND));
        startDatePickerDialog3.getDatePicker().setMinDate(cal.getTimeInMillis());
        /*	cal.add(Calendar.YEAR, 85);
        cal.set(Calendar.HOUR_OF_DAY, cal.getMaximum(Calendar.HOUR_OF_DAY));
		cal.set(Calendar.MINUTE, cal.getMaximum(Calendar.MINUTE));
		cal.set(Calendar.SECOND, cal.getMaximum(Calendar.SECOND));
		cal.set(Calendar.MILLISECOND, cal.getMaximum(Calendar.MILLISECOND));
		startDatePickerDialog.getDatePicker().setMaxDate(cal.getTimeInMillis());*/

        startDatePickerDialog3.getDatePicker().setCalendarViewShown(false);


        startDatePickerDialog4 = new DatePickerDialog(getActivity(), new OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
//
//                Log.d("DatePickerPosition", "555555555555555");
////                for (int i = 0; i < AlDateView.size(); i++) {
//                Calendar newDate = Calendar.getInstance();
//                newDate.set(year, monthOfYear, dayOfMonth);
//
////                System.out.println("datepickerposition new" + datepickerposition);
////                System.out.println("(Integer)AlDateView.get(i).getTag()" + (Integer) AlDateView.get(i).getTag());
//
//                Log.d("DatepickerSize", AlDateView.size() + " g");
////                    if (datepickerposition == (Integer) AlDateView.get(i).getTag()) {
//                View v = lstDrug.getChildAt(datepickerposition);
//                TextView txtDate = (TextView) v.findViewById(R.id.etStartdate);
//                txtDate.setText(dateFormatter.format(newDate.getTime()));
//                try {
//                    dateObj.put(datepickerposition, dateFormatter.format(newDate.getTime()));
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//                    }
//                }
                {

                    for (int i = 0; i < AlDateView.size(); i++) {
                        Calendar newDate = Calendar.getInstance();
                        newDate.set(year, monthOfYear, dayOfMonth);

                        System.out.println("datepickerposition new" + datepickerposition);
                        System.out.println("(Integer)AlDateView.get(i).getTag()" + (Integer) AlDateView.get(i).getTag());

                        if (datepickerposition == (Integer) AlDateView.get(i).getTag()) {
                            AlDateView.get(i).setText(dateFormatter.format(newDate.getTime()));
                        }
                    }
                }
            }
        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
        //	startDatePickerDialog4.getDatePicker().setMinDate(Calendar.getInstance().getTimeInMillis()- 1000);
        cal.set(Calendar.HOUR_OF_DAY, cal.getMinimum(Calendar.HOUR_OF_DAY));
        cal.set(Calendar.MINUTE, cal.getMinimum(Calendar.MINUTE));
        cal.set(Calendar.SECOND, cal.getMinimum(Calendar.SECOND));
        cal.set(Calendar.MILLISECOND, cal.getMinimum(Calendar.MILLISECOND));
        startDatePickerDialog4.getDatePicker().setMinDate(cal.getTimeInMillis());
        /*	cal.add(Calendar.YEAR, 85);
        cal.set(Calendar.HOUR_OF_DAY, cal.getMaximum(Calendar.HOUR_OF_DAY));
		cal.set(Calendar.MINUTE, cal.getMaximum(Calendar.MINUTE));
		cal.set(Calendar.SECOND, cal.getMaximum(Calendar.SECOND));
		cal.set(Calendar.MILLISECOND, cal.getMaximum(Calendar.MILLISECOND));
		startDatePickerDialog.getDatePicker().setMaxDate(cal.getTimeInMillis());*/

        startDatePickerDialog4.getDatePicker().setCalendarViewShown(false);


        startDatePickerDialog5 = new DatePickerDialog(getActivity(), new OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Log.d("DatePickerPosition", "6666666666666666");
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                tvFollowup.setText(dateFormatter.format(newDate.getTime()));
            }
        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
        //	startDatePickerDialog5.getDatePicker().setMinDate(Calendar.getInstance().getTimeInMillis()- 1000);
        cal.set(Calendar.HOUR_OF_DAY, cal.getMinimum(Calendar.HOUR_OF_DAY) + 24);
        cal.set(Calendar.MINUTE, cal.getMinimum(Calendar.MINUTE));
        cal.set(Calendar.SECOND, cal.getMinimum(Calendar.SECOND));
        cal.set(Calendar.MILLISECOND, cal.getMinimum(Calendar.MILLISECOND));
        startDatePickerDialog5.getDatePicker().setMinDate(cal.getTimeInMillis());

		/*	cal.add(Calendar.YEAR, 85);
        cal.set(Calendar.HOUR_OF_DAY, cal.getMaximum(Calendar.HOUR_OF_DAY));
		cal.set(Calendar.MINUTE, cal.getMaximum(Calendar.MINUTE));
		cal.set(Calendar.SECOND, cal.getMaximum(Calendar.SECOND));
		cal.set(Calendar.MILLISECOND, cal.getMaximum(Calendar.MILLISECOND));
		startDatePickerDialog.getDatePicker().setMaxDate(cal.getTimeInMillis());*/

        startDatePickerDialog5.getDatePicker().setCalendarViewShown(false);


        Calendar c = Calendar.getInstance();
        System.out.println("Current time => " + c.getTime());
        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");

        SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss");
        currentDateandTime = c.getTimeInMillis();

        //Log.e("cure Timeee====", String.valueOf(c.getTimeInMillis()));

        formattedDate = df.format(c.getTime());
        currDate.setText(formattedDate);


        if (Home_Page_Activity.patient != null) {
            vpuid = Home_Page_Activity.patient.getString("patientID");
            String fname = Home_Page_Activity.patient.getString("firstName");
            String lname = Home_Page_Activity.patient.getString("lastName");

            if (fname == null) {
                fname = "FirstName";
            }
            if (lname == null) {
                lname = "LastName";
            }
            fltmName.setText(fname + " " + lname);
            System.out.println("nameeeeeeee" + fname);
            uniqID.setText(vpuid);
        } else {
            //New code for shared preference
            SharedPreferences scanpidnew = getActivity().getSharedPreferences("NewPatID", 0);
            String scnewPid = scanpidnew.getString("NewID", null);

            SharedPreferences scanpid1 = getActivity().getSharedPreferences("ScannedPid", 0);
            String scPid = scanpid1.getString("scanpid", null);

            if (scPid != null) {
                vpuid = scPid;
                uniqID.setText(vpuid);
                SharedPreferences fnname1 = getActivity().getSharedPreferences("fnName", 0);

                System.out.println("name=======" + fnname1.getString("fNname", null));
                fltmName.setText(fnname1.getString("fNname", "FirstName" + " " + "LastName"));
                System.out.println("firstnamelastname" + fnname1.getString("fNname", null));


            }

            if (scnewPid != null) {
                vpuid = scnewPid;
                uniqID.setText(vpuid);
                SharedPreferences fnname1 = getActivity().getSharedPreferences("fnName", 0);

                System.out.println("name=======" + fnname1.getString("fNname", null));
                fltmName.setText(fnname1.getString("fNname", "FirstName" + " " + "LastName"));
                System.out.println("firstnamelastname" + fnname1.getString("fNname", null));


            }
        }
        tvStartdate.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                System.out.println("working................");
                startDatePickerDialog.show();

            }
        });


        tvStartdate1.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                System.out.println("working................");
                startDatePickerDialog1.show();

            }
        });
        tvStartdate2.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                System.out.println("working................");
                startDatePickerDialog2.show();

            }
        });
        tvStartdate3.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                System.out.println("working................");
                startDatePickerDialog3.show();

            }
        });


        addDrugs.setOnClickListener(new OnClickListener() {

                                        @Override
                                        public void onClick(View arg0) /*{

//                                            int count = lstDrug.getChildCount();
//                                            for (int i = 0; i < count; i++) {
//                                                try {
//                                                    if (drugObj.length() >= i)
//                                                        drugObj.put(i, AlDrugs.get(i).getText().toString());
//                                                    else
//                                                        drugObj.put(AlDrugs.get(i).getText().toString());
//                                                    if (dosageObj.length() >= i)
//                                                        dosageObj.put(i, AlDosage.get(i).getText().toString());
//                                                    else
//                                                        dosageObj.put(AlDosage.get(i).getText().toString());
//                                                    if (dateObj.length() >= i)
//                                                        dateObj.put(i, AlDateView.get(i).getText().toString());
//                                                    else
//                                                        dateObj.put(AlDateView.get(i).getText().toString());
//                                                    if (durationObj.length() >= i)
//                                                        durationObj.put(i, AlDuration.get(i).getText().toString());
//                                                    else
//                                                        durationObj.put(AlDuration.get(i).getText().toString());
//                                                    if (QuantityObj.length() >= i)
//                                                        QuantityObj.put(i, AlQuantity.get(i).getText().toString());
//                                                    else
//                                                        QuantityObj.put(AlQuantity.get(i).getText().toString());
//                                                } catch (JSONException e) {
//                                                    e.printStackTrace();
//                                                }
                                            listCount.add(listCount.size());
                                            adapterDrug.notifyDataSetChanged();
                                            AppUtil.justifyListViewHeightBasedOnChildren(lstDrug);
                                            Log.d("Clicked", "True");
                                        }*/ {
                                            // TODO Auto-generated method stub
                                            final ArrayList<ImageView> AlDaySelected = new ArrayList<ImageView>();

                                            i++;
                                            dateviewindex++;

                                            LayoutInflater inflater = LayoutInflater.from(getActivity());
                                            final View add_drugs = inflater.inflate(R.layout.add_more_drugs_new, null);
                                            final AutoCompleteTextView drug = (AutoCompleteTextView) add_drugs.findViewById(R.id.etDrug);
                                            final EditText dosage = (EditText) add_drugs.findViewById(R.id.etDosage);
                                            final TextView startdate = (TextView) add_drugs.findViewById(R.id.etStartdate);
                                            final EditText duration = (EditText) add_drugs.findViewById(R.id.etDuration);
                                            final TextView quantity = (TextView) add_drugs.findViewById(R.id.srQuantity);
                                            final TextView tvnumber = (TextView) add_drugs.findViewById(R.id.tvNumber);

                                            drug.setHint(Html.fromHtml("<font size=\"12\">" + "Drugs" + "</font>" + "<small>" + "</small>"));
                                            dosage.setHint(Html.fromHtml("<font size=\"12\">" + "Dosage" + "</font>" + "<small>" + "</small>"));
                                            duration.setHint(Html.fromHtml("<font size=\"12\">" + "Duration" + "</font>" + "<small>" + "</small>"));
                                            quantity.setHint(Html.fromHtml("<font size=\"12\">" + "Frequency" + "</font>" + "<small>" + "</small>"));

                                            drug.setAdapter(adapter);

                                            final RelativeLayout ivRemove = (RelativeLayout) add_drugs.findViewById(R.id.rlImagecross);

//                final LinearLayout days_container = (LinearLayout) add_drugs.findViewById(R.id.add_days_container);
//                final ImageView iv_monday = (ImageView) add_drugs.findViewById(R.id.days_monday);
//                final ImageView iv_tuesday = (ImageView) add_drugs.findViewById(R.id.days_tuesday);
//                final ImageView iv_wed = (ImageView) add_drugs.findViewById(R.id.days_wed);
//                final ImageView iv_thu = (ImageView) add_drugs.findViewById(R.id.days_thu);
//                final ImageView iv_fri = (ImageView) add_drugs.findViewById(R.id.days_fri);
//                final ImageView iv_sat = (ImageView) add_drugs.findViewById(R.id.days_sat);
//                final ImageView iv_sun = (ImageView) add_drugs.findViewById(R.id.days_sun);

                                            pos++;


//                days_container.setOnClickListener(new OnClickListener() {
//
//                    @Override
//                    public void onClick(View v) {
//                        // TODO Auto-generated method stub
//                        Popup_days_selectorwindow((Integer.valueOf(iv_monday.getTag().toString()) + 5));
//                    }
//                });
//
//                iv_monday.setOnClickListener(new OnClickListener() {
//
//                    @Override
//                    public void onClick(View arg0) {
//                        // TODO Auto-generated method stub
//                        Popup_days_selectorwindow((Integer.valueOf(iv_monday.getTag().toString()) + 5));
//                        Log.e("Clicked pos=======>>", String.valueOf(pos));
//                    }
//                });

//                iv_tuesday.setOnClickListener(new OnClickListener() {
//
//                    @Override
//                    public void onClick(View arg0) {
//                        // TODO Auto-generated method stub
//                        Popup_days_selectorwindow((Integer.valueOf(iv_tuesday.getTag().toString()) + 5));
//                        Log.e("Clicked pos=======>>", String.valueOf(pos));
//                    }
//                });
//
//                iv_wed.setOnClickListener(new OnClickListener() {
//
//                    @Override
//                    public void onClick(View arg0) {
//                        // TODO Auto-generated method stub
//                        Popup_days_selectorwindow((Integer.valueOf(iv_wed.getTag().toString()) + 5));
//                        Log.e("Clicked pos=======>>", String.valueOf(pos));
//                    }
//                });
//
//                iv_thu.setOnClickListener(new OnClickListener() {
//
//                    @Override
//                    public void onClick(View arg0) {
//                        // TODO Auto-generated method stub
//                        Popup_days_selectorwindow((Integer.valueOf(iv_thu.getTag().toString()) + 5));
//                        Log.e("Clicked pos=======>>", String.valueOf(pos));
//                    }
//                });
//
//                iv_fri.setOnClickListener(new OnClickListener() {
//
//                    @Override
//                    public void onClick(View arg0) {
//                        // TODO Auto-generated method stub
//                        Popup_days_selectorwindow((Integer.valueOf(iv_fri.getTag().toString()) + 5));
//                        Log.e("Clicked pos=======>>", String.valueOf(pos));
//                    }
//                });
//
//                iv_sat.setOnClickListener(new OnClickListener() {
//
//                    @Override
//                    public void onClick(View arg0) {
//                        // TODO Auto-generated method stub
//                        Popup_days_selectorwindow((Integer.valueOf(iv_sat.getTag().toString()) + 5));
//                        Log.e("Clicked pos=======>>", String.valueOf(pos));
//                    }
//                });
//
//                iv_sun.setOnClickListener(new OnClickListener() {
//
//                    @Override
//                    public void onClick(View arg0) {
//                        // TODO Auto-generated method stub
//                        Popup_days_selectorwindow((Integer.valueOf(iv_sun.getTag().toString()) + 5));
//                        Log.e("Clicked pos=======>>", String.valueOf(pos));
//                    }
//                });

                                            quantity.setOnClickListener(new OnClickListener() {

                                                @Override
                                                public void onClick(View v) {
                                                    // TODO Auto-generated method stub
                                                    flag = 4;
                                                    System.out.println("position tag" + Integer.parseInt(quantity.getTag().toString()));

                                                    int indexvalue = Integer.parseInt(quantity.getTag().toString()) + 1;
                                                    System.out.println("indexvalue" + indexvalue);
                                                    if (quantitySelectedArray.containsKey(indexvalue)) {
                                                        System.out.println("key contains");
                                                        if (quantitySelectedArray.get(indexvalue) != null) {
                                                            System.out.println("not null");
                                                            selectedQuantity_fifth = quantitySelectedArray.get(indexvalue);
                                                        }
                                                    } else {
                                                        System.out.println("no key contains");
                                                        selectedQuantity_fifth = null;
                                                    }

                                                    Bundle bundle = new Bundle();
                                                    bundle.putInt("row", Integer.parseInt(quantity.getTag().toString()));
                                                    bundle.putStringArray("selectedquantity", selectedQuantity_fifth);
                                                    bundle.putString("name", "Quantity");
                                                    bundle.putInt("tagvalue", Integer.parseInt(quantity.getTag().toString()));
                                                    bundle.putSerializable("quantiyname", quantitynamearraylist);
                                                    bundle.putStringArray("items", quantity_array);
                                                    //bundle.putSerializable("listener",Treatment_Create_Frag.this);
                                                    spObj = new QuantitySelector_Popup();
                                                    spObj.setSubmitListener(quantitySelectedListener);
                                                    spObj.setArguments(bundle);
                                                    spObj.show(getFragmentManager(), "tag");


                                                }
                                            });


                                            ivRemove.setOnClickListener(new OnClickListener() {

                                                @Override
                                                public void onClick(View v) {
                                                    // TODO Auto-generated method stub

                                                    if (drug.getText().length() != 0 || dosage.getText().length() != 0 || startdate.getText().length() != 0 || duration.getText().length() != 0) {
                                                        submitButtonDeactivation();
                                                    }
                                                    View removeView = (LinearLayout) add_drugs.findViewById(R.id.llhidden_layout);
                                                    ViewGroup parent = (ViewGroup) removeView.getParent();
                                                    parent.removeView(removeView);
                                                    int position = Integer.valueOf(removeView.getTag().toString());

                                                    if (position == (AlDrugs.size() + 4))
                                                        j--;

                                                    AlDrugs.remove(drug);
                                                    AlDosage.remove(dosage);
                                                    AlDateView.remove(startdate);
                                                    AlDuration.remove(duration);

                                                    AlQuantity.remove(quantity);


                                                    int a = Integer.valueOf(v.getTag().toString());

                                                    removedindex.add(a);
//                        AlDaysList.set(a, null);

//                        System.out.println("aldaylist size" + AlDaysList.size());

                                                    i--;
                                                    dateviewindex--;

                                                    positionval--;
                                                }
                                            });

                                            drug.addTextChangedListener(new TextWatcher() {

                                                @Override
                                                public void onTextChanged(CharSequence s, int start, int before, int count) {
                                                    // TODO Auto-generated method stub
                                                    if (s.length() == 0) {
                                                        submitButtonDeactivation();
                                                    } else if (start == 0 && before == 0 && count == 1) {
                                                        submitButtonActivation();
                                                    }
                                                }

                                                @Override
                                                public void beforeTextChanged(CharSequence s, int start, int count,
                                                                              int after) {
                                                    // TODO Auto-generated method stub

                                                }

                                                @Override
                                                public void afterTextChanged(Editable s) {
                                                    // TODO Auto-generated method stub

                                                }
                                            });

                                            dosage.addTextChangedListener(new TextWatcher() {

                                                @Override
                                                public void onTextChanged(CharSequence s, int start, int before, int count) {
                                                    // TODO Auto-generated method stub
                                                    if (s.length() == 0) {
                                                        submitButtonDeactivation();
                                                    } else if (start == 0 && before == 0 && count == 1) {
                                                        submitButtonActivation();
                                                    }
                                                }

                                                @Override
                                                public void beforeTextChanged(CharSequence s, int start, int count,
                                                                              int after) {
                                                    // TODO Auto-generated method stub

                                                }

                                                @Override
                                                public void afterTextChanged(Editable s) {
                                                    // TODO Auto-generated method stub
                                                }
                                            });

                                            startdate.addTextChangedListener(new TextWatcher() {

                                                @Override
                                                public void onTextChanged(CharSequence s, int start, int before, int count) {
                                                    // TODO Auto-generated method stub
                                                    if (s.length() == 0) {
                                                        submitButtonDeactivation();
                                                    } else if (start == 0 && before == 0 && count == 10) {
                                                        submitButtonActivation();
                                                    }
                                                }

                                                @Override
                                                public void beforeTextChanged(CharSequence s, int start, int count,
                                                                              int after) {
                                                    // TODO Auto-generated method stub

                                                }

                                                @Override
                                                public void afterTextChanged(Editable s) {
                                                    // TODO Auto-generated method stub
                                                }
                                            });

                                            duration.addTextChangedListener(new TextWatcher() {

                                                @Override
                                                public void onTextChanged(CharSequence s, int start, int before, int count) {
                                                    // TODO Auto-generated method stub
                                                    if (s.length() == 0) {
                                                        submitButtonDeactivation();
                                                    } else if (start == 0 && before == 0 && count == 1) {
                                                        submitButtonActivation();
                                                    }
                                                }

                                                @Override
                                                public void beforeTextChanged(CharSequence s, int start, int count,
                                                                              int after) {
                                                    // TODO Auto-generated method stub

                                                }

                                                @Override
                                                public void afterTextChanged(Editable s) {
                                                    // TODO Auto-generated method stub
                                                }
                                            });


//                System.out.println("checkboxtag log" + checkboxtag);
//                iv_monday.setTag(checkboxtag);
//                iv_tuesday.setTag(checkboxtag);
//                iv_wed.setTag(checkboxtag);
//                iv_thu.setTag(checkboxtag);
//                iv_fri.setTag(checkboxtag);
//                iv_sat.setTag(checkboxtag);
//                iv_sun.setTag(checkboxtag);
//
//                AlDaySelected.add(iv_monday);
//                AlDaySelected.add(iv_tuesday);
//                AlDaySelected.add(iv_wed);
//                AlDaySelected.add(iv_thu);
//                AlDaySelected.add(iv_fri);
//                AlDaySelected.add(iv_sat);
//                AlDaySelected.add(iv_sun);


//                AlDaysList.add(checkboxtag, AlDaySelected);


                                            quantity.setTag((int) quantitytag);
                                            //new code


                                            AlDrugs.add(drug);
                                            AlDosage.add(dosage);
                                            AlDateView.add(startdate);
                                            AlDuration.add(duration);

                                            AlQuantity.add(quantity);

                                            ivRemove.setTag((int) checkboxtag);


                                            startdate.setTag((int) index);
                                            add_drugs.setTag((int) j);
                                            tvnumber.setText(Integer.toString(j) + ". ");
                                            llMoreDrugs.addView(add_drugs);

                                            j++;
                                            index++;
                                            checkboxtag++;
                                            quantitytag++;
                                            imagedaystag++;

                                            System.out.println("size,,,,,,,,,," + AlDateView.size());
                                            System.out.println("dateviewindex,,,,,,,,,," + dateviewindex);
                                            System.out.println("ddddddddddddddddddddddddd" + i);
                                            System.out.println("jjjjjjjjjjjjjjjjjjjjjjjjj" + j);
//                for (int j = 0; j < AlDaysList.size(); j++) {
//                    System.out.println("list value" + AlDaysList.get(j));
//                }
                                            AlDateView.get(dateviewindex).setOnClickListener(new OnClickListener() {

                                                @Override
                                                public void onClick(View arg0) {
                                                    // TODO Auto-generated method stub
                                                    startDatePickerDialog4.show();
                                                    datepickerposition = (Integer) startdate.getTag();
                                                    System.out.println("date picker position" + datepickerposition);

                                                }
                                            });

                                        }
                                    }

        );

        tvFollowup.setOnClickListener(new

                                              OnClickListener() {

                                                  @Override
                                                  public void onClick(View arg0) {
                                                      // TODO Auto-generated method stub
                                                      startDatePickerDialog5.show();


                                                  }
                                              }

        );

        topcontainer.setOnClickListener(new

                                                OnClickListener() {

                                                    @Override
                                                    public void onClick(View v) {
                                                        // TODO Auto-generated method stub
                                                        hideKeyboard(v);
                                                    }
                                                }

        );
        svcontainer.setOnClickListener(new

                                               OnClickListener() {

                                                   @Override
                                                   public void onClick(View v) {
                                                       // TODO Auto-generated method stub
                                                       hideKeyboard(v);
                                                   }
                                               }

        );
        maincontainer.setOnClickListener(new

                                                 OnClickListener() {

                                                     @Override
                                                     public void onClick(View v) {
                                                         // TODO Auto-generated method stub
                                                         hideKeyboard(v);
                                                     }
                                                 }

        );


        return view;
    }

    @Override
    public void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
        CrashReporter.getInstance().trackScreenView("Patient Treatment creation");
        i = 3;
        j = 5;
        index = 5;
        checkboxtag = 0;
        quantitytag = 0;
        dateviewindex = -1;
        AlDateView.clear();
        AlDrugs.clear();
        AlDosage.clear();
        AlDuration.clear();
        AlDays.clear();
        AlQuantity.clear();
        List_Dynamic_Days.clear();
//        AlDaysList.clear();
        removedindex.clear();
        daysSelectedArray.clear();
        quantitySelectedArray.clear();

        llMoreDrugs.removeAllViews();

        //Toast.makeText(getActivity(), "Resume working", Toast.LENGTH_LONG).show();

        if (patienttreatmentparseObj != null) {

            drugObj = patienttreatmentparseObj.getJSONArray("drug");
            dosageObj = patienttreatmentparseObj.getJSONArray("dosage");
            dateObj = patienttreatmentparseObj.getJSONArray("drugStartDate");
            durationObj = patienttreatmentparseObj.getJSONArray("duration");
            QuantityObj = patienttreatmentparseObj.getJSONArray("quantity");
            daysObj = patienttreatmentparseObj.getJSONArray("days");

            adapterDrug.notifyDataSetChanged();
            AppUtil.justifyListViewHeightBasedOnChildren(lstDrug);

            System.out.println("duration array" + durationObj);

            if (patienttreatmentparseObj.getString("treatment") != null) {
                etTreatment.setText(patienttreatmentparseObj.getString("treatment"));
            }
            if (patienttreatmentparseObj.getString("followup") != null) {
                tvFollowup.setText(patienttreatmentparseObj.getString("followup"));
            }

            if (drugObj != null) {
                for (int k = 0; k < drugObj.length(); k++) {
                    if (k == 0) {
                        try {
                            etdrug1.setText(drugObj.getString(0));
                            etdosage1.setText(dosageObj.getString(0));
                            tvStartdate.setText(dateObj.getString(0));
                            etduration1.setText(durationObj.getString(0));
                            srquantity1.setText(QuantityObj.getString(0));
                            String quant = QuantityObj.getString(0).toString();
                            selectedQuantity_first = quant.split(",");

                            for (int l = 0; l < QuantityObj.getString(0).split(",").length; l++) {
                                System.out.println("@@@@@@@@@@@@@" + selectedQuantity_first[l]);
                            }


                            if (daysObj.getString(k).equals("ALL")) {
                                days_array1 = new String[7];
                                days_array1[0] = "Monday";
                                days_array1[1] = "Tuesday";
                                days_array1[2] = "Wednesday";
                                days_array1[3] = "Thursday";
                                days_array1[4] = "Friday";
                                days_array1[5] = "Saturday";
                                days_array1[6] = "Sunday";
                                System.out.println("workinggggggggggg");

                                //cbdaily1.setChecked(true);

//                                iv_mon_1.setImageResource(R.drawable.days_monday_2);
//                                iv_tue_1.setImageResource(R.drawable.days_tuesday_2);
//                                iv_wed_1.setImageResource(R.drawable.days_wed_2);
//                                iv_thu_1.setImageResource(R.drawable.days_thu_2);
//                                iv_fri_1.setImageResource(R.drawable.days_fri_2);
//                                iv_sat_1.setImageResource(R.drawable.days_sat_2);
//                                iv_sun_1.setImageResource(R.drawable.days_sun_2);
//
//                                iv_mon_1.setTag(1);
//                                iv_tue_1.setTag(1);
//                                iv_wed_1.setTag(1);
//                                iv_thu_1.setTag(1);
//                                iv_fri_1.setTag(1);
//                                iv_sat_1.setTag(1);
//                                iv_sun_1.setTag(1);

                                Selecteddays_first = "ALL";


                            } else {
                                String var = daysObj.getString(k);
                                System.out.println("variableeeee" + var);
                                System.out.println("length" + var.length());
                                days = var.split(",");


                                days_array1 = new String[days.length];
                                Selecteddays_first = var;
                                for (int l = 0; l < days.length; l++) {

                                    System.out.println("days" + days);
                                    System.out.println("days valueeeeeeeeee" + days[l]);

//                                    if (days[l].equals("Mo")) {
//                                        iv_mon_1.setImageResource(R.drawable.days_monday_2);
//                                        iv_mon_1.setTag(1);
//                                        days_array1[l] = "Monday";
//                                    }
//                                    if (days[l].equals("Tu")) {
//                                        iv_tue_1.setImageResource(R.drawable.days_tuesday_2);
//                                        iv_tue_1.setTag(1);
//                                        days_array1[l] = "Tuesday";
//                                    }
//                                    if (days[l].equals("We")) {
//                                        iv_wed_1.setImageResource(R.drawable.days_wed_2);
//                                        iv_wed_1.setTag(1);
//                                        days_array1[l] = "Wednesday";
//                                    }
//                                    if (days[l].equals("Th")) {
//                                        iv_thu_1.setImageResource(R.drawable.days_thu_2);
//                                        iv_thu_1.setTag(1);
//                                        days_array1[l] = "Thursday";
//                                    }
//                                    if (days[l].equals("Fr")) {
//                                        iv_fri_1.setImageResource(R.drawable.days_fri_2);
//                                        iv_fri_1.setTag(1);
//                                        days_array1[l] = "Friday";
//                                    }
//                                    if (days[l].equals("Sa")) {
//                                        iv_sat_1.setImageResource(R.drawable.days_sat_2);
//                                        iv_sat_1.setTag(1);
//                                        days_array1[l] = "Saturday";
//                                    }
//                                    if (days[l].equals("Su")) {
//                                        iv_sun_1.setImageResource(R.drawable.days_sun_2);
//                                        iv_sun_1.setTag(1);
//                                        days_array1[l] = "Sunday";
//                                    }
                                }
                            }

                        } catch (JSONException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }

                    } else if (k == 1) {

                        try {
                            etdrug2.setText(drugObj.getString(1));
                            etdosage2.setText(dosageObj.getString(1));
                            tvStartdate1.setText(dateObj.getString(1));
                            etduration2.setText(durationObj.getString(1));
                            srquantity2.setText(QuantityObj.getString(1));
                            String quant = QuantityObj.getString(1).toString();
                            selectedQuantity_second = quant.split(",");
                            for (int l = 0; l < QuantityObj.getString(1).split(",").length; l++) {
                                System.out.println("@@@@@@@@@@@@@" + selectedQuantity_second[l]);
                            }

                            if (daysObj.getString(k).equals("ALL")) {

                                Selecteddays_second = "ALL";
                                System.out.println("workinggggggggggg");
                                days_array2 = new String[7];
                                days_array2[0] = "Monday";
                                days_array2[1] = "Tuesday";
                                days_array2[2] = "Wednesday";
                                days_array2[3] = "Thursday";
                                days_array2[4] = "Friday";
                                days_array2[5] = "Saturday";
                                days_array2[6] = "Sunday";


//                                iv_mon_2.setImageResource(R.drawable.days_monday_2);
//                                iv_tue_2.setImageResource(R.drawable.days_tuesday_2);
//                                iv_wed_2.setImageResource(R.drawable.days_wed_2);
//                                iv_thu_2.setImageResource(R.drawable.days_thu_2);
//                                iv_fri_2.setImageResource(R.drawable.days_fri_2);
//                                iv_sat_2.setImageResource(R.drawable.days_sat_2);
//                                iv_sun_2.setImageResource(R.drawable.days_sun_2);
//
//
//                                iv_mon_2.setTag(1);
//                                iv_tue_2.setTag(1);
//                                iv_wed_2.setTag(1);
//                                iv_thu_2.setTag(1);
//                                iv_fri_2.setTag(1);
//                                iv_sat_2.setTag(1);
//                                iv_sun_2.setTag(1);

                            } else {
                                String var = daysObj.getString(k);

                                System.out.println("variableeeee" + var);
                                System.out.println("length" + var.length());
                                days1 = var.split(",");
                                Selecteddays_second = var;

                                days_array2 = new String[days1.length];
                                for (int l = 0; l < days1.length; l++) {

                                    System.out.println("days" + days);
                                    System.out.println("days valueeeeeeeeee" + days1[l]);

//                                    if (days1[l].equals("Mo")) {
//                                        iv_mon_2.setImageResource(R.drawable.days_monday_2);
//                                        iv_mon_2.setTag(1);
//                                        days_array2[l] = "Monday";
//                                    }
//                                    if (days1[l].equals("Tu")) {
//                                        iv_tue_2.setImageResource(R.drawable.days_tuesday_2);
//                                        iv_tue_2.setTag(1);
//                                        days_array2[l] = "Tuesday";
//                                    }
//                                    if (days1[l].equals("We")) {
//                                        iv_wed_2.setImageResource(R.drawable.days_wed_2);
//                                        iv_wed_2.setTag(1);
//                                        days_array2[l] = "Wednesday";
//                                    }
//                                    if (days1[l].equals("Th")) {
//                                        iv_thu_2.setImageResource(R.drawable.days_thu_2);
//                                        iv_thu_2.setTag(1);
//                                        days_array2[l] = "Thursday";
//                                    }
//                                    if (days1[l].equals("Fr")) {
//                                        iv_fri_2.setImageResource(R.drawable.days_fri_2);
//                                        iv_fri_2.setTag(1);
//                                        days_array2[l] = "Friday";
//                                    }
//                                    if (days1[l].equals("Sa")) {
//                                        iv_sat_2.setImageResource(R.drawable.days_sat_2);
//                                        iv_sat_2.setTag(1);
//                                        days_array2[l] = "Saturday";
//                                    }
//                                    if (days1[l].equals("Su")) {
//                                        iv_sun_2.setImageResource(R.drawable.days_sun_2);
//                                        iv_sun_2.setTag(1);
//                                        days_array2[l] = "Sunday";
//                                    }
                                }
                            }


                        } catch (JSONException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }

                    } else if (k == 2) {
                        try {
                            etdrug3.setText(drugObj.getString(2));
                            etdosage3.setText(dosageObj.getString(2));
                            tvStartdate2.setText(dateObj.getString(2));
                            etduration3.setText(durationObj.getString(2));
                            srquantity3.setText(QuantityObj.getString(2));
                            String quant = QuantityObj.getString(2).toString();
                            selectedQuantity_third = quant.split(",");


                            for (int l = 0; l < QuantityObj.getString(2).split(",").length; l++) {
                                System.out.println("@@@@@@@@@@@@@" + selectedQuantity_third[l]);
                            }

                            if (daysObj.getString(k).equals("ALL")) {
                                Selecteddays_third = "ALL";

                                days_array3 = new String[7];
                                days_array3[0] = "Monday";
                                days_array3[1] = "Tuesday";
                                days_array3[2] = "Wednesday";
                                days_array3[3] = "Thursday";
                                days_array3[4] = "Friday";
                                days_array3[5] = "Saturday";
                                days_array3[6] = "Sunday";
                                System.out.println("workinggggggggggg");

//
//                                iv_mon_3.setImageResource(R.drawable.days_monday_2);
//                                iv_tue_3.setImageResource(R.drawable.days_tuesday_2);
//                                iv_wed_3.setImageResource(R.drawable.days_wed_2);
//                                iv_thu_3.setImageResource(R.drawable.days_thu_2);
//                                iv_fri_3.setImageResource(R.drawable.days_fri_2);
//                                iv_sat_3.setImageResource(R.drawable.days_sat_2);
//                                iv_sun_3.setImageResource(R.drawable.days_sun_2);
//
//                                iv_mon_3.setTag(1);
//                                iv_tue_3.setTag(1);
//                                iv_wed_3.setTag(1);
//                                iv_thu_3.setTag(1);
//                                iv_fri_3.setTag(1);
//                                iv_sat_3.setTag(1);
//                                iv_sun_3.setTag(1);

                            } else {

                                String var = daysObj.getString(k);
                                System.out.println("variableeeee" + var);
                                System.out.println("length" + var.length());


                                days2 = var.split(",");
                                Selecteddays_third = var;
                                days_array3 = new String[days2.length];
                                for (int l = 0; l < days2.length; l++) {

                                    System.out.println("days" + days2);
                                    System.out.println("days valueeeeeeeeee third row" + days2[l]);

//                                    if (days2[l].equals("Mo")) {
//                                        iv_mon_3.setImageResource(R.drawable.days_monday_2);
//                                        iv_mon_3.setTag(1);
//                                        days_array3[l] = "Monday";
//
//                                    }
//                                    if (days2[l].equals("Tu")) {
//                                        iv_tue_3.setImageResource(R.drawable.days_tuesday_2);
//                                        iv_tue_3.setTag(1);
//                                        days_array3[l] = "Tuesday";
//                                    }
//                                    if (days2[l].equals("We")) {
//                                        iv_wed_3.setImageResource(R.drawable.days_wed_2);
//                                        iv_wed_3.setTag(1);
//                                        days_array3[l] = "Wednesday";
//                                    }
//                                    if (days2[l].equals("Th")) {
//                                        iv_thu_3.setImageResource(R.drawable.days_thu_2);
//                                        iv_thu_3.setTag(1);
//                                        days_array3[l] = "Thursday";
//                                    }
//                                    if (days2[l].equals("Fr")) {
//                                        iv_fri_3.setImageResource(R.drawable.days_fri_2);
//                                        iv_fri_3.setTag(1);
//                                        days_array3[l] = "Friday";
//                                    }
//                                    if (days2[l].equals("Sa")) {
//                                        iv_sat_3.setImageResource(R.drawable.days_sat_2);
//                                        iv_sat_3.setTag(1);
//                                        days_array3[l] = "Saturday";
//                                    }
//                                    if (days2[l].equals("Su")) {
//                                        iv_sun_3.setImageResource(R.drawable.days_sun_2);
//                                        iv_sun_3.setTag(1);
//                                        days_array3[l] = "Sunday";
//                                    }
                                }
                            }


                        } catch (JSONException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }

                    } else if (k == 3) {
                        try {
                            etdrug4.setText(drugObj.getString(3));
                            etdosage4.setText(dosageObj.getString(3));
                            tvStartdate3.setText(dateObj.getString(3));
                            etduration4.setText(durationObj.getString(3));
                            srquantity4.setText(QuantityObj.getString(3));

                            String quant = QuantityObj.getString(3).toString();
                            selectedQuantity_fourth = quant.split(",");

                            for (int l = 0; l < QuantityObj.getString(3).split(",").length; l++) {
                                System.out.println("@@@@@@@@@@@@@" + selectedQuantity_fourth[l]);
                            }

                            if (daysObj.getString(k).equals("ALL")) {
                                Selecteddays_fourth = "ALL";
                                days_array4 = new String[7];
                                days_array4[0] = "Monday";
                                days_array4[1] = "Tuesday";
                                days_array4[2] = "Wednesday";
                                days_array4[3] = "Thursday";
                                days_array4[4] = "Friday";
                                days_array4[5] = "Saturday";
                                days_array4[6] = "Sunday";
                                System.out.println("workinggggggggggg");
//                                iv_mon_4.setImageResource(R.drawable.days_monday_2);
//                                iv_tue_4.setImageResource(R.drawable.days_tuesday_2);
//                                iv_wed_4.setImageResource(R.drawable.days_wed_2);
//                                iv_thu_4.setImageResource(R.drawable.days_thu_2);
//                                iv_fri_4.setImageResource(R.drawable.days_fri_2);
//                                iv_sat_4.setImageResource(R.drawable.days_sat_2);
//                                iv_sun_4.setImageResource(R.drawable.days_sun_2);
//
//
//                                iv_mon_4.setTag(1);
//                                iv_tue_4.setTag(1);
//                                iv_wed_4.setTag(1);
//                                iv_thu_4.setTag(1);
//                                iv_fri_4.setTag(1);
//                                iv_sat_4.setTag(1);
//                                iv_sun_4.setTag(1);

                            } else {
                                String var = daysObj.getString(k);
                                System.out.println("variableeeee" + var);
                                System.out.println("length" + var.length());


                                days3 = var.split(",");
                                Selecteddays_fourth = var;
                                days_array4 = new String[days3.length];
                                for (int l = 0; l < days3.length; l++) {

                                    System.out.println("days" + days3);
                                    System.out.println("days valueeeeeeeeee" + days3[l]);

//                                    if (days3[l].equals("Mo")) {
//                                        iv_mon_4.setImageResource(R.drawable.days_monday_2);
//                                        iv_mon_4.setTag(1);
//                                        days_array4[l] = "Monday";
//                                    }
//                                    if (days3[l].equals("Tu")) {
//                                        iv_tue_4.setImageResource(R.drawable.days_tuesday_2);
//                                        iv_tue_4.setTag(1);
//                                        days_array4[l] = "Tuesday";
//                                    }
//                                    if (days3[l].equals("We")) {
//                                        iv_wed_4.setImageResource(R.drawable.days_wed_2);
//                                        iv_wed_4.setTag(1);
//                                        days_array4[l] = "Wednesday";
//                                    }
//                                    if (days3[l].equals("Th")) {
//                                        iv_thu_4.setImageResource(R.drawable.days_thu_2);
//                                        iv_thu_4.setTag(1);
//                                        days_array4[l] = "Thursday";
//                                    }
//                                    if (days3[l].equals("Fr")) {
//                                        iv_fri_4.setImageResource(R.drawable.days_fri_2);
//                                        iv_fri_4.setTag(1);
//                                        days_array4[l] = "Friday";
//                                    }
//                                    if (days3[l].equals("Sa")) {
//                                        iv_sat_4.setImageResource(R.drawable.days_sat_2);
//                                        iv_sat_4.setTag(1);
//                                        days_array4[l] = "Saturday";
//                                    }
//                                    if (days3[l].equals("Su")) {
//                                        iv_sun_4.setImageResource(R.drawable.days_sun_2);
//                                        iv_sun_4.setTag(1);
//                                        days_array4[l] = "Sunday";
//                                    }
                                }
                            }


                        } catch (JSONException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }

                    } else if (k > 3) {


                        System.out.println("inflating working......");

                        final ArrayList<ImageView> AlDaySelected = new ArrayList<ImageView>();
                        String days[];

                        i++;
                        dateviewindex++;

                        LayoutInflater inflater = LayoutInflater.from(getActivity());
                        final View add_drugs = inflater.inflate(R.layout.add_more_drugs_new, null);
                        final LinearLayout days_container = (LinearLayout) add_drugs.findViewById(R.id.add_days_container);
                        final AutoCompleteTextView drug = (AutoCompleteTextView) add_drugs.findViewById(R.id.etDrug);
                        final EditText dosage = (EditText) add_drugs.findViewById(R.id.etDosage);
                        final TextView startdate = (TextView) add_drugs.findViewById(R.id.etStartdate);
                        final EditText duration = (EditText) add_drugs.findViewById(R.id.etDuration);
                        final TextView quantity = (TextView) add_drugs.findViewById(R.id.srQuantity);
                        final TextView tvnumber = (TextView) add_drugs.findViewById(R.id.tvNumber);

                        drug.setHint(Html.fromHtml("<font size=\"12\">" + "Drugs" + "</font>" + "<small>" + "</small>"));
                        dosage.setHint(Html.fromHtml("<font size=\"12\">" + "Dosage" + "</font>" + "<small>" + "</small>"));
                        duration.setHint(Html.fromHtml("<font size=\"12\">" + "Duration" + "</font>" + "<small>" + "</small>"));
                        quantity.setHint(Html.fromHtml("<font size=\"12\">" + "Frequency" + "</font>" + "<small>" + "</small>"));

                        drug.setAdapter(adapter);

//                        final RelativeLayout ivRemove = (RelativeLayout) add_drugs.findViewById(R.id.rlImagecross);

//                        final ImageView iv_monday = (ImageView) add_drugs.findViewById(R.id.days_monday);
//                        final ImageView iv_tuesday = (ImageView) add_drugs.findViewById(R.id.days_tuesday);
//                        final ImageView iv_wed = (ImageView) add_drugs.findViewById(R.id.days_wed);
//                        final ImageView iv_thu = (ImageView) add_drugs.findViewById(R.id.days_thu);
//                        final ImageView iv_fri = (ImageView) add_drugs.findViewById(R.id.days_fri);
//                        final ImageView iv_sat = (ImageView) add_drugs.findViewById(R.id.days_sat);
//                        final ImageView iv_sun = (ImageView) add_drugs.findViewById(R.id.days_sun);

                        pos++;

                        try {
                            drug.setText(drugObj.getString(k));
                            dosage.setText(dosageObj.getString(k));
                            startdate.setText(dateObj.getString(k));
                            duration.setText(durationObj.getString(k));
                            quantity.setText(QuantityObj.getString(k));

                            String quant = QuantityObj.getString(k).toString();
                            selectedQuantity_fifth = quant.split(",");

                            int indexvalue = index - 4;
                            quantitySelectedArray.put(indexvalue, selectedQuantity_fifth);
                            //	int indexvalue1=(Integer) AlQuantity.get(i).getTag()+1;

                            System.out.println("indexvalue....." + indexvalue + "@@@@@@@");

                            for (int l = 0; l < QuantityObj.getString(k).split(",").length; l++) {
                                System.out.println("@@@@@@@@@@@@@" + selectedQuantity_fifth[l]);
                            }

                            if (daysObj.getString(k).equals("ALL")) {
                                days_array_dyanamic = new String[7];


                                days_array_dyanamic[0] = "Monday";
                                days_array_dyanamic[1] = "Tuesday";
                                days_array_dyanamic[2] = "Wednesday";
                                days_array_dyanamic[3] = "Thursday";
                                days_array_dyanamic[4] = "Friday";
                                days_array_dyanamic[5] = "Saturday";
                                days_array_dyanamic[6] = "Sunday";

                                int index = k + 1;
                                System.out.println("selected row exist" + k + 1);
                                daysSelectedArray.put(index, days_array_dyanamic);

                                System.out.println("daysSelectedArray size in check" + daysSelectedArray.size());

                                System.out.println("workinggggggggggg");
                                //daily.setChecked(true);
//                                iv_monday.setImageResource(R.drawable.days_monday_2);
//                                iv_tuesday.setImageResource(R.drawable.days_tuesday_2);
//                                iv_wed.setImageResource(R.drawable.days_wed_2);
//                                iv_thu.setImageResource(R.drawable.days_thu_2);
//                                iv_fri.setImageResource(R.drawable.days_fri_2);
//                                iv_sat.setImageResource(R.drawable.days_sat_2);
//                                iv_sun.setImageResource(R.drawable.days_sun_2);
//
//
//                                iv_monday.setTag(1);
//                                iv_tuesday.setTag(1);
//                                iv_wed.setTag(1);
//                                iv_thu.setTag(1);
//                                iv_fri.setTag(1);
//                                iv_sat.setTag(1);
//                                iv_sun.setTag(1);

                            } else {
                                //daily.setChecked(false);
                                String var = daysObj.getString(k);
                                System.out.println("variableeeee" + var);
                                System.out.println("length" + var.length());
                                days = var.split(",");
                                days_array_dyanamic = new String[days.length];
                                for (int l = 0; l < days.length; l++) {

                                    System.out.println("days" + days);
                                    System.out.println("days valueeeeeeeeee" + days[l]);

//                                    if (days[l].equals("Mo")) {
//                                        iv_monday.setImageResource(R.drawable.days_monday_2);
//                                        iv_monday.setTag(1);
//                                        days_array_dyanamic[l] = "Monday";
//                                    }
//                                    if (days[l].equals("Tu")) {
//                                        iv_tuesday.setImageResource(R.drawable.days_tuesday_2);
//                                        iv_tuesday.setTag(1);
//                                        days_array_dyanamic[l] = "Tuesday";
//                                    }
//                                    if (days[l].equals("We")) {
//                                        iv_wed.setImageResource(R.drawable.days_wed_2);
//                                        iv_wed.setTag(1);
//                                        days_array_dyanamic[l] = "Wednesday";
//                                    }
//                                    if (days[l].equals("Th")) {
//                                        iv_thu.setImageResource(R.drawable.days_thu_2);
//                                        iv_thu.setTag(1);
//                                        days_array_dyanamic[l] = "Thursday";
//                                    }
//                                    if (days[l].equals("Fr")) {
//                                        iv_fri.setImageResource(R.drawable.days_fri_2);
//                                        iv_fri.setTag(1);
//                                        days_array_dyanamic[l] = "Friday";
//                                    }
//                                    if (days[l].equals("Sa")) {
//                                        iv_sat.setImageResource(R.drawable.days_sat_2);
//                                        iv_sat.setTag(1);
//                                        days_array_dyanamic[l] = "Saturday";
//                                    }
//                                    if (days[l].equals("Su")) {
//                                        iv_sun.setImageResource(R.drawable.days_sun_2);
//                                        iv_sun.setTag(1);
//                                        days_array_dyanamic[l] = "Sunday";
//                                    }

                                }
                                for (int i = 0; i < days_array_dyanamic.length; i++)
                                    System.out.println("days_array_dyanamic" + days_array_dyanamic[i]);

                                int indexheap = k + 1;
                                daysSelectedArray.put(indexheap, days_array_dyanamic);


                                System.out.println("daysSelectedArray size" + daysSelectedArray.size());

                            }
                        } catch (JSONException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }


//                        days_container.setOnClickListener(new OnClickListener() {
//
//                            @Override
//                            public void onClick(View v) {
//                                // TODO Auto-generated method stub
//                                Popup_days_selectorwindow((Integer.valueOf(iv_monday.getTag().toString()) + 5));
//                            }
//                        });
//
//
//                        iv_monday.setOnClickListener(new OnClickListener() {
//
//                            @Override
//                            public void onClick(View arg0) {
//                                // TODO Auto-generated method stub
//                                Popup_days_selectorwindow((Integer.valueOf(iv_monday.getTag().toString()) + 5));
//                                Log.e("Clicked pos=======>>", String.valueOf(pos));
//                            }
//                        });
//
//                        iv_tuesday.setOnClickListener(new OnClickListener() {
//
//                            @Override
//                            public void onClick(View arg0) {
//                                // TODO Auto-generated method stub
//                                Popup_days_selectorwindow((Integer.valueOf(iv_tuesday.getTag().toString()) + 5));
//                                Log.e("Clicked pos=======>>", String.valueOf(pos));
//                            }
//                        });
//
//                        iv_wed.setOnClickListener(new OnClickListener() {
//
//                            @Override
//                            public void onClick(View arg0) {
//                                // TODO Auto-generated method stub
//                                Popup_days_selectorwindow((Integer.valueOf(iv_wed.getTag().toString()) + 5));
//                                Log.e("Clicked pos=======>>", String.valueOf(pos));
//                            }
//                        });
//
//                        iv_thu.setOnClickListener(new OnClickListener() {
//
//                            @Override
//                            public void onClick(View arg0) {
//                                // TODO Auto-generated method stub
//                                Popup_days_selectorwindow((Integer.valueOf(iv_thu.getTag().toString()) + 5));
//                                Log.e("Clicked pos=======>>", String.valueOf(pos));
//                            }
//                        });
//
//                        iv_fri.setOnClickListener(new OnClickListener() {
//
//                            @Override
//                            public void onClick(View arg0) {
//                                // TODO Auto-generated method stub
//                                Popup_days_selectorwindow((Integer.valueOf(iv_fri.getTag().toString()) + 5));
//                                Log.e("Clicked pos=======>>", String.valueOf(pos));
//                            }
//                        });
//
//                        iv_sat.setOnClickListener(new OnClickListener() {
//
//                            @Override
//                            public void onClick(View arg0) {
//                                // TODO Auto-generated method stub
//                                Popup_days_selectorwindow((Integer.valueOf(iv_sat.getTag().toString()) + 5));
//                                Log.e("Clicked pos=======>>", String.valueOf(pos));
//                            }
//                        });
//
//                        iv_sun.setOnClickListener(new OnClickListener() {
//
//                            @Override
//                            public void onClick(View arg0) {
//                                // TODO Auto-generated method stub
//                                Popup_days_selectorwindow((Integer.valueOf(iv_sun.getTag().toString()) + 5));
//                                Log.e("Clicked pos=======>>", String.valueOf(pos));
//                            }
//                        });

//                        quantity.setOnClickListener(new OnClickListener() {
//
//                            @Override
//                            public void onClick(View v) {
//                                // TODO Auto-generated method stub
//                                flag = 4;
//                                System.out.println("position tag" + Integer.parseInt(quantity.getTag().toString()));
//
//                                int indexvalue = Integer.parseInt(quantity.getTag().toString()) + 1;
//                                System.out.println("indexvalue" + indexvalue);
//                                if (quantitySelectedArray.containsKey(indexvalue)) {
//                                    System.out.println("key contains");
//                                    if (quantitySelectedArray.get(indexvalue) != null) {
//                                        System.out.println("not null");
//                                        selectedQuantity_fifth = quantitySelectedArray.get(indexvalue);
//                                    }
//                                } else {
//                                    System.out.println("no key contains");
//                                    selectedQuantity_fifth = null;
//                                }
//
//
//                                Bundle bundle = new Bundle();
//                                bundle.putInt("row", Integer.parseInt(quantity.getTag().toString()));
//                                bundle.putString("name", "Quantity");
//                                bundle.putStringArray("selectedquantity", selectedQuantity_fifth);
//                                bundle.putInt("tagvalue", Integer.parseInt(quantity.getTag().toString()));
//                                bundle.putSerializable("quantiyname", quantitynamearraylist);
//                                bundle.putStringArray("items", quantity_array);
//                                //bundle.putSerializable("listener",Treatment_Create_Frag.this);
//                                spObj = new QuantitySelector_Popup();
//                                spObj.setSubmitListener(quantitySelectedListener);
//                                spObj.setArguments(bundle);
//                                spObj.show(getFragmentManager(), "tag");
//
//
//                            }
//                        });


//                        ivRemove.setOnClickListener(new OnClickListener() {
//
//                            @Override
//                            public void onClick(View v) {
//                                // TODO Auto-generated method stub
//
//                                if (drug.getText().length() != 0 || dosage.getText().length() != 0 || startdate.getText().length() != 0 || duration.getText().length() != 0) {
//                                    submitButtonDeactivation();
//                                }
//                                View removeView = (LinearLayout) add_drugs.findViewById(R.id.llhidden_layout);
//                                ViewGroup parent = (ViewGroup) removeView.getParent();
//                                parent.removeView(removeView);
//                                int position = Integer.valueOf(removeView.getTag().toString());
//
//                                if (position == (AlDrugs.size() + 4))
//                                    j--;
//
//                                AlDrugs.remove(drug);
//                                AlDosage.remove(dosage);
//                                AlDateView.remove(startdate);
//                                AlDuration.remove(duration);
//                                AlQuantity.remove(quantity);
//
//
//                                int a = Integer.valueOf(v.getTag().toString());
//                                System.out.println("remove log" + a);
//                                removedindex.add(a);
////                                AlDaysList.set(a, null);
////                                System.out.println("aldaylist size" + AlDaysList.size());
//
//                                i--;
//                                dateviewindex--;
//
//                                positionval--;
//                            }
//                        });

                        drug.addTextChangedListener(new TextWatcher() {

                            @Override
                            public void onTextChanged(CharSequence s, int start, int before, int count) {
                                // TODO Auto-generated method stub
                                if (s.length() == 0) {
                                    submitButtonDeactivation();
                                } else if (start == 0 && before == 0 && count == 1) {
                                    submitButtonActivation();
                                }
                            }

                            @Override
                            public void beforeTextChanged(CharSequence s, int start, int count,
                                                          int after) {
                                // TODO Auto-generated method stub

                            }

                            @Override
                            public void afterTextChanged(Editable s) {
                                // TODO Auto-generated method stub

                            }
                        });

                        dosage.addTextChangedListener(new TextWatcher() {

                            @Override
                            public void onTextChanged(CharSequence s, int start, int before, int count) {
                                // TODO Auto-generated method stub
                                if (s.length() == 0) {
                                    submitButtonDeactivation();
                                } else if (start == 0 && before == 0 && count == 1) {
                                    submitButtonActivation();
                                }
                            }

                            @Override
                            public void beforeTextChanged(CharSequence s, int start, int count,
                                                          int after) {
                                // TODO Auto-generated method stub

                            }

                            @Override
                            public void afterTextChanged(Editable s) {
                                // TODO Auto-generated method stub
                            }
                        });

                        startdate.addTextChangedListener(new TextWatcher() {

                            @Override
                            public void onTextChanged(CharSequence s, int start, int before, int count) {
                                // TODO Auto-generated method stub
                                if (s.length() == 0) {
                                    submitButtonDeactivation();
                                } else if (start == 0 && before == 0 && count == 10) {
                                    submitButtonActivation();
                                }
                            }

                            @Override
                            public void beforeTextChanged(CharSequence s, int start, int count,
                                                          int after) {
                                // TODO Auto-generated method stub

                            }

                            @Override
                            public void afterTextChanged(Editable s) {
                                // TODO Auto-generated method stub
                            }
                        });

                        duration.addTextChangedListener(new TextWatcher() {

                            @Override
                            public void onTextChanged(CharSequence s, int start, int before, int count) {
                                // TODO Auto-generated method stub
                                if (s.length() == 0) {
                                    submitButtonDeactivation();
                                } else if (start == 0 && before == 0 && count == 1) {
                                    submitButtonActivation();
                                }
                            }

                            @Override
                            public void beforeTextChanged(CharSequence s, int start, int count,
                                                          int after) {
                                // TODO Auto-generated method stub

                            }

                            @Override
                            public void afterTextChanged(Editable s) {
                                // TODO Auto-generated method stub
                            }
                        });


//                        System.out.println("checkboxtag log" + checkboxtag);
//                        iv_monday.setTag(checkboxtag);
//                        iv_tuesday.setTag(checkboxtag);
//                        iv_wed.setTag(checkboxtag);
//                        iv_thu.setTag(checkboxtag);
//                        iv_fri.setTag(checkboxtag);
//                        iv_sat.setTag(checkboxtag);
//                        iv_sun.setTag(checkboxtag);
//
//                        AlDaySelected.add(iv_monday);
//                        AlDaySelected.add(iv_tuesday);
//                        AlDaySelected.add(iv_wed);
//                        AlDaySelected.add(iv_thu);
//                        AlDaySelected.add(iv_fri);
//                        AlDaySelected.add(iv_sat);
//                        AlDaySelected.add(iv_sun);


//                        AlDaysList.add(checkboxtag, AlDaySelected);


                        quantity.setTag((int) quantitytag);
                        //new code


//                        AlDrugs.add(drug);
//                        AlDosage.add(dosage);
//                        AlDateView.add(startdate);
//                        AlDuration.add(duration);
//                        AlQuantity.add(quantity);

//                        ivRemove.setTag((int) checkboxtag);


                        startdate.setTag((int) index);
                        add_drugs.setTag((int) j);
                        tvnumber.setText(Integer.toString(j) + ". ");

                        llMoreDrugs.addView(add_drugs);

                        j++;
                        index++;
                        checkboxtag++;
                        quantitytag++;
                        imagedaystag++;

                        System.out.println("size,,,,,,,,,," + AlDateView.size());
                        System.out.println("dateviewindex,,,,,,,,,," + dateviewindex);
                        System.out.println("ddddddddddddddddddddddddd" + i);
                        System.out.println("jjjjjjjjjjjjjjjjjjjjjjjjj" + j);

//                        AlDateView.get(dateviewindex).setOnClickListener(new OnClickListener() {
//
//                            @Override
//                            public void onClick(View arg0) {
//                                // TODO Auto-generated method stub
//                                startDatePickerDialog4.show();
////                                datepickerposition = (Integer) startdate.getTag();
//                                System.out.println("date picker position" + datepickerposition);
//
//                            }
//                        });
                    }
                }
            }

            FragmentActivityContainer.treatmangement_save_check = 0;
            ((FragmentActivityContainer) getActivity()).gettreatmentfrag().onTreatmentmanagementbtn(0);

        }
    }


    public void Clear() {

        etdrug1.setText("");
        etdrug2.setText("");
        etdrug3.setText("");
        etdrug4.setText("");

        etdosage1.setText("");
        etdosage2.setText("");
        etdosage3.setText("");
        etdosage4.setText("");

        etduration1.setText("");
        etduration2.setText("");
        etduration3.setText("");
        etduration4.setText("");

        tvStartdate.setText("");
        tvStartdate1.setText("");
        tvStartdate2.setText("");
        tvStartdate3.setText("");

        et_Treatment.setText("");
        tvFollowup.setText("");


//        iv_mon_1.setTag(0);
//        iv_mon_1.setImageResource(R.drawable.days_monday_1);
//        iv_tue_1.setTag(0);
//        iv_tue_1.setImageResource(R.drawable.days_tuesday_1);
//        iv_wed_1.setTag(0);
//        iv_wed_1.setImageResource(R.drawable.days_wed_1);
//        iv_thu_1.setTag(0);
//        iv_thu_1.setImageResource(R.drawable.days_thu_1);
//        iv_fri_1.setTag(0);
//        iv_fri_1.setImageResource(R.drawable.days_fri_1);
//        iv_sat_1.setTag(0);
//        iv_sat_1.setImageResource(R.drawable.days_sat_1);
//        iv_sun_1.setTag(0);
//        iv_sun_1.setImageResource(R.drawable.days_sun_1);
//
//
//        iv_mon_2.setTag(0);
//        iv_mon_2.setImageResource(R.drawable.days_monday_1);
//        iv_tue_2.setTag(0);
//        iv_tue_2.setImageResource(R.drawable.days_tuesday_1);
//        iv_wed_2.setTag(0);
//        iv_wed_2.setImageResource(R.drawable.days_wed_1);
//        iv_thu_2.setTag(0);
//        iv_thu_2.setImageResource(R.drawable.days_thu_1);
//        iv_fri_2.setTag(0);
//        iv_fri_2.setImageResource(R.drawable.days_fri_1);
//        iv_sat_2.setTag(0);
//        iv_sat_2.setImageResource(R.drawable.days_sat_1);
//        iv_sun_2.setTag(0);
//        iv_sun_2.setImageResource(R.drawable.days_sun_1);
//
//
//        iv_mon_3.setTag(0);
//        iv_mon_3.setImageResource(R.drawable.days_monday_1);
//        iv_tue_3.setTag(0);
//        iv_tue_3.setImageResource(R.drawable.days_tuesday_1);
//        iv_wed_3.setTag(0);
//        iv_wed_3.setImageResource(R.drawable.days_wed_1);
//        iv_thu_3.setTag(0);
//        iv_thu_3.setImageResource(R.drawable.days_thu_1);
//        iv_fri_3.setTag(0);
//        iv_fri_3.setImageResource(R.drawable.days_fri_1);
//        iv_sat_3.setTag(0);
//        iv_sat_3.setImageResource(R.drawable.days_sat_1);
//        iv_sun_3.setTag(0);
//        iv_sun_3.setImageResource(R.drawable.days_sun_1);
//
//
//        iv_mon_4.setTag(0);
//        iv_mon_4.setImageResource(R.drawable.days_monday_1);
//        iv_tue_4.setTag(0);
//        iv_tue_4.setImageResource(R.drawable.days_tuesday_1);
//        iv_wed_4.setTag(0);
//        iv_wed_4.setImageResource(R.drawable.days_wed_1);
//        iv_thu_4.setTag(0);
//        iv_thu_4.setImageResource(R.drawable.days_thu_1);
//        iv_fri_4.setTag(0);
//        iv_fri_4.setImageResource(R.drawable.days_fri_1);
//        iv_sat_4.setTag(0);
//        iv_sat_4.setImageResource(R.drawable.days_sat_1);
//        iv_sun_4.setTag(0);
//        iv_sun_4.setImageResource(R.drawable.days_sun_1);


        if (AlDrugs.size() > 0) {
            for (int i = 0; i < AlDrugs.size(); i++) {
                AlDrugs.get(i).setText("");
                AlDosage.get(i).setText("");
                AlDateView.get(i).setText("");

                AlDuration.get(i).setText("");


            }
        }

    }


    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        if (v.getId() == R.id.days1_container) {
            System.out.println("container working...");
            Popup_days_selectorwindow(0);
        }
        if (v.getId() == R.id.ivdays1_monday) {
            Popup_days_selectorwindow(0);

        }

        if (v.getId() == R.id.ivdays1_tuesday) {
            Popup_days_selectorwindow(0);
        }

        if (v.getId() == R.id.ivdays1_wed) {
            Popup_days_selectorwindow(0);
        }
        if (v.getId() == R.id.ivdays1_thu) {

            Popup_days_selectorwindow(0);
        }
        if (v.getId() == R.id.ivdays1_fri) {
            Popup_days_selectorwindow(0);
        }
        if (v.getId() == R.id.ivdays1_sat) {
            Popup_days_selectorwindow(0);
        }
        if (v.getId() == R.id.ivdays1_sun) {
            Popup_days_selectorwindow(0);
        }


        if (v.getId() == R.id.days2_container) {
            System.out.println("container working...");
            Popup_days_selectorwindow(1);
        }
        if (v.getId() == R.id.ivdays2_monday) {

            Popup_days_selectorwindow(1);
        }
        if (v.getId() == R.id.ivdays2_tuesday) {
            Popup_days_selectorwindow(1);
        }
        if (v.getId() == R.id.ivdays2_wed) {

            Popup_days_selectorwindow(1);
        }
        if (v.getId() == R.id.ivdays2_thu) {
            Popup_days_selectorwindow(1);
        }
        if (v.getId() == R.id.ivdays2_fri) {
            Popup_days_selectorwindow(1);


        }
        if (v.getId() == R.id.ivdays2_sat) {
            Popup_days_selectorwindow(1);

        }
        if (v.getId() == R.id.ivdays2_sun) {
            Popup_days_selectorwindow(1);
        }


        if (v.getId() == R.id.days3_container) {
            System.out.println("container working...");
            Popup_days_selectorwindow(2);
        }
        if (v.getId() == R.id.ivdays3_monday) {
            Popup_days_selectorwindow(2);
        }
        if (v.getId() == R.id.ivdays3_tuesday) {
            Popup_days_selectorwindow(2);
            ;
        }
        if (v.getId() == R.id.ivdays3_wed) {
            Popup_days_selectorwindow(2);
        }
        if (v.getId() == R.id.ivdays3_thu) {
            Popup_days_selectorwindow(2);
        }
        if (v.getId() == R.id.ivdays3_fri) {
            Popup_days_selectorwindow(2);
        }
        if (v.getId() == R.id.ivdays3_sat) {
            Popup_days_selectorwindow(2);
        }
        if (v.getId() == R.id.ivdays3_sun) {
            Popup_days_selectorwindow(2);
        }


        if (v.getId() == R.id.days4_container) {
            System.out.println("container working...");
            Popup_days_selectorwindow(3);
        }
        if (v.getId() == R.id.ivdays4_monday) {
            Popup_days_selectorwindow(3);
        }
        if (v.getId() == R.id.ivdays4_tuesday) {
            Popup_days_selectorwindow(3);
        }
        if (v.getId() == R.id.ivdays4_wed) {
            Popup_days_selectorwindow(3);
        }
        if (v.getId() == R.id.ivdays4_thu) {
            Popup_days_selectorwindow(3);
        }
        if (v.getId() == R.id.ivdays4_fri) {
            Popup_days_selectorwindow(3);
        }
        if (v.getId() == R.id.ivdays4_sat) {
            Popup_days_selectorwindow(3);
        }
        if (v.getId() == R.id.ivdays4_sun) {
            Popup_days_selectorwindow(3);
        }

    }

    protected void hideKeyboard(View view) {
        InputMethodManager in = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        in.hideSoftInputFromWindow(view.getWindowToken(),
                InputMethodManager.HIDE_NOT_ALWAYS);
    }

    public void submitButtonActivation() {
        // TODO Auto-generated method stub

        ((FragmentActivityContainer) getActivity()).gettreatmentfrag().onTreatmentmanagementbtn(1);


    }

    public void submitButtonDeactivation() {
        // TODO Auto-generated method stub
        Treatment_Frag object = ((FragmentActivityContainer) getActivity()).gettreatmentfrag();
        if (object != null)
            ((FragmentActivityContainer) getActivity()).gettreatmentfrag().onTreatmentmanagementbtn(0);

    }


    @Override
    public void onTreatment(int arg) {
        // TODO Auto-generated method stub
        System.out.println("tv follow up" + tvFollowup.getText().toString());
        save_treatment_details(arg);

    }

    private void save_treatment_details(final int arg) {
        // TODO Auto-generated method stub
        if (Validation()) {
            treatment = et_Treatment.getText().toString();
            System.out.println("etdrug1 value" + etdrug1.getText().toString());
//            System.out.println("tag value monday" + Integer.valueOf(iv_mon_1.getTag().toString()));

            // && (Integer.valueOf(iv_mon_1.getTag().toString())==1 || Integer.valueOf(iv_tue_1.getTag().toString())==1) || Integer.valueOf(iv_wed_1.getTag().toString())==1 || Integer.valueOf(iv_thu_1.getTag().toString())==1 ||Integer.valueOf(iv_fri_1.getTag().toString())==1 || Integer.valueOf(iv_sat_1.getTag().toString())==1 || Integer.valueOf(iv_sun_1.getTag().toString())==1

            if (etdrug1.getText().toString().length() != 0) {
                drug_name = etdrug1.getText().toString();
                drug_dosage = etdosage1.getText().toString();
                drug_startdate = tvStartdate.getText().toString();
                drug_duration = etduration1.getText().toString();
                drug_quantity = srquantity1.getText().toString();

                drugname.add(drug_name);
                drugdosage.add(drug_dosage);
                drugduration.add(drug_duration);
                drugstartdate.add(drug_startdate);
                drugquantity.add(drug_quantity);

                List_Dynamic_Days.add(Selecteddays_first);


                System.out.println("workingggggg1");
                System.out.println("drug2 value..." + etdrug2.getText().toString());

            }
            // &&(Integer.valueOf(iv_mon_2.getTag().toString())==1 || Integer.valueOf(iv_tue_2.getTag().toString())==1) || Integer.valueOf(iv_wed_2.getTag().toString())==1 || Integer.valueOf(iv_thu_2.getTag().toString())==1 ||Integer.valueOf(iv_fri_2.getTag().toString())==1 || Integer.valueOf(iv_sat_2.getTag().toString())==1 || Integer.valueOf(iv_sun_2.getTag().toString())==1
            if (etdrug2.getText().toString().length() != 0) {
                drug_name1 = etdrug2.getText().toString();
                drug_dosage1 = etdosage2.getText().toString();
                drug_startdate1 = tvStartdate1.getText().toString();
                drug_duration1 = etduration2.getText().toString();
                drug_quantity1 = srquantity2.getText().toString();

                System.out.println("workingggggg2 time");
                drugname.add(drug_name1);
                drugdosage.add(drug_dosage1);
                drugduration.add(drug_duration1);
                drugstartdate.add(drug_startdate1);
                drugquantity.add(drug_quantity1);

                List_Dynamic_Days.add(Selecteddays_second);

            }
            //&& (Integer.valueOf(iv_mon_3.getTag().toString())==1 || Integer.valueOf(iv_tue_3.getTag().toString())==1) || Integer.valueOf(iv_wed_3.getTag().toString())==1 || Integer.valueOf(iv_thu_3.getTag().toString())==1 ||Integer.valueOf(iv_fri_3.getTag().toString())==1 || Integer.valueOf(iv_sat_3.getTag().toString())==1 || Integer.valueOf(iv_sun_3.getTag().toString())==3 
            if (etdrug3.getText().toString().length() != 0) {
                drug_name2 = etdrug3.getText().toString();
                drug_dosage2 = etdosage3.getText().toString();
                drug_startdate2 = tvStartdate2.getText().toString();
                drug_duration2 = etduration3.getText().toString();
                drug_quantity2 = srquantity3.getText().toString();

                drugname.add(drug_name2);
                drugdosage.add(drug_dosage2);
                drugduration.add(drug_duration2);
                drugstartdate.add(drug_startdate2);
                drugquantity.add(drug_quantity2);
                System.out.println("workingggggg3");

                List_Dynamic_Days.add(Selecteddays_third);


            }
            //&& (Integer.valueOf(iv_mon_4.getTag().toString())==1 || Integer.valueOf(iv_tue_4.getTag().toString())==1) || Integer.valueOf(iv_wed_4.getTag().toString())==1 || Integer.valueOf(iv_thu_4.getTag().toString())==1 ||Integer.valueOf(iv_fri_4.getTag().toString())==1 || Integer.valueOf(iv_sat_4.getTag().toString())==1 || Integer.valueOf(iv_sun_4.getTag().toString())==1
            if (etdrug4.getText().toString().length() != 0) {
                drug_name3 = etdrug4.getText().toString();
                drug_dosage3 = etdosage4.getText().toString();
                drug_startdate3 = tvStartdate3.getText().toString();
                drug_duration3 = etduration4.getText().toString();
                drug_quantity3 = srquantity4.getText().toString();

                drugname.add(drug_name3);
                drugdosage.add(drug_dosage3);
                drugduration.add(drug_duration3);
                drugstartdate.add(drug_startdate3);
                drugquantity.add(drug_quantity3);

                List_Dynamic_Days.add(Selecteddays_fourth);

                System.out.println("workingggggg4");
            }


//            for (int i = 0; i < AlDaysList.size(); i++) {
//                if (AlDaysList.get(i) != null) {
//
//                    boolean flag = true;
//                    String Dynamically_Selecteddays = "";
//
//                    //for(int j=0;j<daysSelectedArray.size();j++){
//                    String daysselect[];
//
//                    int imageviewtag = Integer.valueOf(AlDaysList.get(i).get(0).getTag().toString());
//                    int selectrowtag = imageviewtag + 5;
//                    System.out.println("select row tag" + selectrowtag);
//
//                    System.out.println("daysSelectedArray length" + daysSelectedArray.size());
//                    if (daysSelectedArray.containsKey(selectrowtag)) {
//
//                        System.out.println("contains row" + i);
//                        daysselect = daysSelectedArray.get(selectrowtag);
//
//                        for (int k = 0; k < daysselect.length; k++) {
//                            System.out.println("daysselect" + daysselect[k]);
//                        }
//                        System.out.println("daysselect size" + daysselect.length);
//
//                        for (int k = 0; k < daysselect.length; k++) {
//
//                            //if(daysselect[k]!=null){
//                            if (daysselect[k].equals("Monday") || daysselect[k].equals("Daily")) {
//                                Dynamically_Selecteddays += "Mo,";
//                                flag = true;
//                            } else {
//                                System.out.println("else working....");
//                                flag = false;
//                            }
//                            if (daysselect[k].equals("Tuesday") || daysselect[k].equals("Daily")) {
//                                Dynamically_Selecteddays += "Tu,";
//                                flag = true;
//                            } else {
//                                System.out.println("else working....");
//                                flag = false;
//                            }
//                            if (daysselect[k].equals("Wednesday") || daysselect[k].equals("Daily")) {
//                                Dynamically_Selecteddays += "We,";
//                                flag = true;
//                            } else {
//                                System.out.println("else working....");
//                                flag = false;
//                            }
//                            if (daysselect[k].equals("Thursday") || daysselect[k].equals("Daily")) {
//                                Dynamically_Selecteddays += "Th,";
//                                flag = true;
//                            } else {
//                                System.out.println("else working....");
//                                flag = false;
//                            }
//                            if (daysselect[k].equals("Friday") || daysselect[k].equals("Daily")) {
//                                Dynamically_Selecteddays += "Fr,";
//                                flag = true;
//                            } else {
//                                System.out.println("else working....");
//                                flag = false;
//                            }
//                            if (daysselect[k].equals("Saturday") || daysselect[k].equals("Daily")) {
//                                Dynamically_Selecteddays += "Sa,";
//                                flag = true;
//                            } else {
//                                System.out.println("else working....");
//                                flag = false;
//                            }
//                            if (daysselect[k].equals("Sunday") || daysselect[k].equals("Daily")) {
//                                Dynamically_Selecteddays += "Su,";
//                                flag = true;
//                            } else {
//                                System.out.println("else working....");
//                                flag = false;
//                            }
//
//                        }
//
//
//                    }
//
//					/*if(!(Dynamically_Selecteddays.isEmpty()))*/
//                    List_Dynamic_Days.add(Dynamically_Selecteddays);
//                    Dynamically_Selecteddays = "";
//                }
//            }


            System.out.println("drug size" + AlDrugs.size());
            System.out.println("selected day string" + Selecteddays_first);

            Log.e("***Weekdays*", List_Dynamic_Days.toString());

            if (AlDrugs.size() > 0) {
                for (int j = 0; (j < AlDrugs.size()); j++) {
                    System.out.println("AlDrugs.get(j).getText().toString()" + AlDrugs.get(j).getText().toString());

                    drugname.add(AlDrugs.get(j).getText().toString());
                    drugdosage.add(AlDosage.get(j).getText().toString());
                    drugduration.add(AlDuration.get(j).getText().toString());
                    drugstartdate.add(AlDateView.get(j).getText().toString());
                    drugquantity.add(AlQuantity.get(j).getText().toString());
                    System.out.println("quanity" + AlQuantity.get(j).getTag());
                    System.out.println("size" + AlQuantity.size());
                    System.out.println("contains" + AlQuantity.get(j).getText().toString());
                    System.out.println("drug name" + drugname);
                    System.out.println("drugdosage" + drugdosage);
                    System.out.println("drugduration" + drugduration);
                    System.out.println("drugquantity" + drugquantity);

                }

                for (int h = 0; h < drugquantity.size(); h++) {
                    System.out.println("drug quantity values@@@@@" + drugquantity.get(h));
                }

            }


            System.out.println("list dynamic size" + List_Dynamic_Days.size());
            System.out.println("working.........." + AlDrugs.size() + AlDosage.size() + AlDuration.size() + AlDateView.size());

            uniId = uniqID.getText().toString();
            mProgressDialog = new ProgressDialog(getActivity());
            mProgressDialog.setCancelable(false);
            mProgressDialog.setMessage("Storing Prescription Data....");
            if (patienttreatmentparseObj == null) {
                System.out.println("creating parseObject");
                patienttreatmentparseObj = new ParseObject("Diagnosis");
            } else {

            }

            mProgressDialog.show();
            drugnamearray = new JSONArray();
            drugdosagearray = new JSONArray();
            drugstartdatearray = new JSONArray();
            drugdurationarray = new JSONArray();
            drugquantityarray = new JSONArray();
            listdaysarray = new JSONArray();
            for (int i = 0; i < drugname.size(); i++) {
                System.out.println("drugname@@@@" + drugname.get(i));
                drugnamearray.put(drugname.get(i));
            }
            for (int i = 0; i < drugdosage.size(); i++) {
                System.out.println("drugdosage@@@@" + drugdosage.get(i));
                drugdosagearray.put(drugdosage.get(i));
            }
            for (int i = 0; i < drugquantity.size(); i++) {
                System.out.println("drugquantity@@@@" + drugquantity.get(i));
                drugquantityarray.put(drugquantity.get(i));
            }
            for (int i = 0; i < drugduration.size(); i++) {
                System.out.println("drugduration@@@@" + drugduration.get(i));
                drugdurationarray.put(drugduration.get(i));
            }
            for (int i = 0; i < drugstartdate.size(); i++) {
                System.out.println("drugstartdate@@@@" + drugstartdate.get(i));
                drugstartdatearray.put(drugstartdate.get(i));
            }
            for (int i = 0; i < List_Dynamic_Days.size(); i++) {
                System.out.println("List_Dynamic_Days@@@@" + List_Dynamic_Days.get(i));
                listdaysarray.put(List_Dynamic_Days.get(i));
            }


			/*System.out.println("drug quantity"+drugquantity);
            for(int i=0;i<List_Dynamic_Days.size();i++)*/


            patienttreatmentparseObj.put("treatment", treatment);
            patienttreatmentparseObj.put("followup", tvFollowup.getText().toString());
            if (tvFollowup.getText().toString().length() != 0) {
                patienttreatmentparseObj.put("patientattend", false);
            }
            patienttreatmentparseObj.put("drug", drugnamearray);
            patienttreatmentparseObj.put("drugStartDate", drugstartdatearray);
            patienttreatmentparseObj.put("dosage", drugdosagearray);
            patienttreatmentparseObj.put("quantity", drugquantityarray);
            patienttreatmentparseObj.put("duration", drugdurationarray);
            patienttreatmentparseObj.put("typeFlag", 2);
            patienttreatmentparseObj.put("patientID", uniId);
            patienttreatmentparseObj.put("days", listdaysarray);
            patienttreatmentparseObj.put("createdDate", formattedDate);
            patienttreatmentparseObj.put("createdatetime", currentDateandTime);
            treatmentObjectId = uniId.concat(Long.toString(currentDateandTime));

            if (((FragmentActivityContainer) getActivity()).getDiagnosisobject_id() != null) {

                patienttreatmentparseObj.put("diagnosisId", ((FragmentActivityContainer) getActivity()).getDiagnosisobject_id());

            } else {

                patienttreatmentparseObj.put("diagnosisId", treatmentObjectId);

            }


            patienttreatmentparseObj.pinInBackground(new SaveCallback() {


                @Override
                public void done(ParseException e) {
                    // TODO Auto-generated method stub
                    mProgressDialog.dismiss();
                    if (e == null) {

                        savedflag = true;
                        if (((FragmentActivityContainer) getActivity()).getDiagnosisobject_id() == null) {
                            ((FragmentActivityContainer) getActivity()).setDiagnosisobject_id(treatmentObjectId);
                        }
                        myObjectSavedSuccessfully();
                        drugname.clear();
                        drugdosage.clear();
                        drugduration.clear();
                        drugstartdate.clear();
                        drugquantity.clear();
                        List_Dynamic_Days.clear();

                    } else {
                        myObjectSaveDidNotSucceed();
                    }
                }

                private void myObjectSaveDidNotSucceed() {
                    // TODO Auto-generated method stub

                    Toast msg1 = Toast.makeText(getActivity(), "Save Failed", Toast.LENGTH_LONG);
                    msg1.show();

                }

                private void myObjectSavedSuccessfully() {
                    // TODO Auto-generated method stub

                    Treatment_Frag.btnPrint.setEnabled(true);
                    Treatment_Frag.btnPrint.setBackgroundDrawable(getResources().getDrawable(R.drawable.submitbtn_background));
                    Treatment_Frag.btnPrint.setTextColor(getResources().getColor(R.color.actionbar_color));


                    if (arg == 2) {
                        FragmentManager fragmentManager = getFragmentManager();
                        Fragment prescrionmanagement;
                        prescrionmanagement = new ManagementGynacologist_View();
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.replace(R.id.fl_fragment_container, prescrionmanagement);
                        fragmentTransaction.commit();
                    } else if (arg == 4) {

                        FragmentManager fragmentManager = getFragmentManager();
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        Treatment_View_Frag viewtrat = new Treatment_View_Frag();
                        fragmentTransaction.replace(R.id.fl_fragment_container, viewtrat);
                        fragmentTransaction.commit();
                    }

                    Toast msg = Toast.makeText(getActivity(), "Saved", Toast.LENGTH_LONG);
                    msg.show();
                    ((FragmentActivityContainer) getActivity()).gettreatmentfrag().onTreatmentmanagementbtn(0);
                    ((FragmentActivityContainer) getActivity()).setdatasaved(true);
                    ((FragmentActivityContainer) getActivity()).setpatienttreatmentparseObj(patienttreatmentparseObj);
                    //FragmentActivityContainer.treatmangement_save_check=0;
                    FragmentActivityContainer.treatmangement_save_check = 0;
                    ((FragmentActivityContainer) getActivity()).gettreatmentfrag().onTreatmentmanagementbtn(0);


                }
            });

            patienttreatmentparseObj.saveEventually();
        } else {
            Toast.makeText(getActivity(), "Please enter the drug name", Toast.LENGTH_LONG).show();
        }

    }

    public void Popup_days_selectorwindow(int row) {

        daysselectObj = new DaysSelector_Popup();
        daysselectObj.setDynamicDaysListener(daysSelectedListener);

        Bundle bundle = new Bundle();
        bundle.putInt("row", row);
        if (days_array1 != null)
            System.out.println("days_array1 length" + days_array1.length);
        if (row == 0) {
            bundle.putStringArray("selecteddays", days_array1);
        } else if (row == 1) {
            bundle.putStringArray("selecteddays", days_array2);
        } else if (row == 2) {
            bundle.putStringArray("selecteddays", days_array3);
        } else if (row == 3) {
            bundle.putStringArray("selecteddays", days_array4);
        } else {
            System.out.println("row...." + row);
            if (daysSelectedArray.size() != 0) {
                for (int k = 0; k < daysSelectedArray.size(); k++) {
                    if (daysSelectedArray.containsKey(row)) {
                        bundle.putStringArray("selecteddays", daysSelectedArray.get(row));
                    }
                }
            } else {
                bundle.putStringArray("selecteddays", null);
            }
            days_array5 = null;
        }

        //		bundle.putSerializable("listener",Treatment_Create_Frag.this);

        daysselectObj.setArguments(bundle);
        daysselectObj.show(getFragmentManager(), "tag");
    }

    public void doOnDaysSelected(String[] outputStrArr, int selectrow) {
        // TODO Auto-generated method stub

        //System.out.println("select....row....string"+outputStrArr.toString());

        for (int i = 0; i < outputStrArr.length; i++) {
            System.out.println("select....row....string" + outputStrArr[i]);
        }

        if (selectrow >= 5) {


            System.out.println("selected row exist" + selectrow);
            daysSelectedArray.put(selectrow, outputStrArr);
            System.out.println("selected row size" + daysSelectedArray.size());


        }
        selected_days.clear();
        for (int i = 0; i < outputStrArr.length; i++) {
            selected_days.add(outputStrArr[i]);
        }


//        if (selectrow >= 5) {
//            days_array5 = outputStrArr;
//
//            if (selected_days.contains("Monday") || selected_days.contains("Daily")) {
//                System.out.println("select row" + selectrow);
//                AlDaysList.get(selectrow - 5).get(0).setImageResource(R.drawable.days_monday_2);
//                submitButtonActivation();
//                //if(AlDaysList.get(selectrow-5).get(0).getDrawable().getConstantState()==getResources().getDrawable(R.drawable.days_monday_2).getConstantState())
//            } else {
//                System.out.println("compairing working...........");
//                AlDaysList.get(selectrow - 5).get(0).setImageResource(R.drawable.days_monday_1);
//                ////submitButtonDeactivation();
//            }
//            if (selected_days.contains("Tuesday") || selected_days.contains("Daily")) {
//                System.out.println("select row" + selectrow);
//                AlDaysList.get(selectrow - 5).get(1).setImageResource(R.drawable.days_tuesday_2);
//                submitButtonActivation();
//                //if(AlDaysList.get(selectrow-5).get(1).getDrawable().getConstantState()==getResources().getDrawable(R.drawable.days_tuesday_2).getConstantState())
//            } else {
//                AlDaysList.get(selectrow - 5).get(1).setImageResource(R.drawable.days_tuesday_1);
//                //submitButtonDeactivation();
//            }
//
//            if (selected_days.contains("Wednesday") || selected_days.contains("Daily")) {
//                System.out.println("select row" + selectrow);
//                AlDaysList.get(selectrow - 5).get(2).setImageResource(R.drawable.days_wed_2);
//                submitButtonActivation();
//                //if(AlDaysList.get(selectrow-5).get(2).getDrawable().getConstantState()==getResources().getDrawable(R.drawable.days_wed_2).getConstantState())
//            } else {
//                AlDaysList.get(selectrow - 5).get(2).setImageResource(R.drawable.days_wed_1);
//                //	submitButtonDeactivation();
//            }
//            if (selected_days.contains("Thursday") || selected_days.contains("Daily")) {
//                System.out.println("select row" + selectrow);
//                AlDaysList.get(selectrow - 5).get(3).setImageResource(R.drawable.days_thu_2);
//                submitButtonActivation();
//                //if(AlDaysList.get(selectrow-5).get(3).getDrawable().getConstantState()==getResources().getDrawable(R.drawable.days_thu_2).getConstantState())
//            } else {
//                AlDaysList.get(selectrow - 5).get(3).setImageResource(R.drawable.days_thu_1);
//                //submitButtonDeactivation();
//            }
//
//            if (selected_days.contains("Friday") || selected_days.contains("Daily")) {
//                System.out.println("select row" + selectrow);
//                AlDaysList.get(selectrow - 5).get(4).setImageResource(R.drawable.days_fri_2);
//                submitButtonActivation();
//                // if(AlDaysList.get(selectrow-5).get(4).getDrawable().getConstantState()==getResources().getDrawable(R.drawable.days_fri_2).getConstantState())
//            } else {
//                AlDaysList.get(selectrow - 5).get(4).setImageResource(R.drawable.days_fri_1);
//                //submitButtonDeactivation();
//            }
//
//            if (selected_days.contains("Saturday") || selected_days.contains("Daily")) {
//                System.out.println("select row" + selectrow);
//                AlDaysList.get(selectrow - 5).get(5).setImageResource(R.drawable.days_sat_2);
//                submitButtonActivation();
//                //if(AlDaysList.get(selectrow-5).get(5).getDrawable().getConstantState()==getResources().getDrawable(R.drawable.days_sat_2).getConstantState())
//            } else {
//                AlDaysList.get(selectrow - 5).get(5).setImageResource(R.drawable.days_sat_1);
//                //submitButtonDeactivation();
//
//            }
//            if (selected_days.contains("Sunday") || selected_days.contains("Daily")) {
//                System.out.println("select row" + selectrow);
//                AlDaysList.get(selectrow - 5).get(6).setImageResource(R.drawable.days_sun_2);
//                submitButtonActivation();
//                //if(AlDaysList.get(selectrow-5).get(6).getDrawable().getConstantState()==getResources().getDrawable(R.drawable.days_sun_2).getConstantState())
//            } else {
//                AlDaysList.get(selectrow - 5).get(6).setImageResource(R.drawable.days_sun_1);
//                //submitButtonDeactivation();
//            }
//
//        }


        if (selectrow == 0) {
            days_array1 = outputStrArr;

            Selecteddays_first = "";

//            if (selected_days.contains("Monday") || selected_days.contains("Daily")) {
//                iv_mon_1.setTag(1);
//                iv_mon_1.setImageResource(R.drawable.days_monday_2);
//                Selecteddays_first += "Mo,";
//                submitButtonActivation();
//            } else if ((Integer.valueOf(iv_mon_1.getTag().toString()) == 1)) {
//                iv_mon_1.setTag(0);
//                iv_mon_1.setImageResource(R.drawable.days_monday_1);
//                //		submitButtonDeactivation();
//            }
//            if (selected_days.contains("Tuesday") || selected_days.contains("Daily")) {
//                iv_tue_1.setTag(1);
//                iv_tue_1.setImageResource(R.drawable.days_tuesday_2);
//                Selecteddays_first += "Tu,";
//                submitButtonActivation();
//            } else if ((Integer.valueOf(iv_tue_1.getTag().toString()) == 1)) {
//                iv_tue_1.setTag(0);
//                iv_tue_1.setImageResource(R.drawable.days_tuesday_1);
//                //	submitButtonDeactivation();
//            }
//            if (selected_days.contains("Wednesday") || selected_days.contains("Daily")) {
//                iv_wed_1.setTag(1);
//                iv_wed_1.setImageResource(R.drawable.days_wed_2);
//                Selecteddays_first += "We,";
//                submitButtonActivation();
//            } else if ((Integer.valueOf(iv_wed_1.getTag().toString()) == 1)) {
//                iv_wed_1.setTag(0);
//                iv_wed_1.setImageResource(R.drawable.days_wed_1);
//                //submitButtonDeactivation();
//            }
//            if (selected_days.contains("Thursday") || selected_days.contains("Daily")) {
//                iv_thu_1.setTag(1);
//                iv_thu_1.setImageResource(R.drawable.days_thu_2);
//                Selecteddays_first += "Th,";
//                submitButtonActivation();
//            } else if ((Integer.valueOf(iv_thu_1.getTag().toString()) == 1)) {
//                iv_thu_1.setTag(0);
//                iv_thu_1.setImageResource(R.drawable.days_thu_1);
//                //submitButtonDeactivation();
//            }
//            if (selected_days.contains("Friday") || selected_days.contains("Daily")) {
//                iv_fri_1.setTag(1);
//                iv_fri_1.setImageResource(R.drawable.days_fri_2);
//                Selecteddays_first += "Fr,";
//                submitButtonActivation();
//            } else if ((Integer.valueOf(iv_fri_1.getTag().toString()) == 1)) {
//                iv_fri_1.setTag(0);
//                iv_fri_1.setImageResource(R.drawable.days_fri_1);
//                //submitButtonDeactivation();
//            }
//
//            if (selected_days.contains("Saturday") || selected_days.contains("Daily")) {
//                iv_sat_1.setTag(1);
//                iv_sat_1.setImageResource(R.drawable.days_sat_2);
//                Selecteddays_first += "Sa,";
//
//                submitButtonActivation();
//            } else if ((Integer.valueOf(iv_sat_1.getTag().toString()) == 1)) {
//                iv_sat_1.setTag(0);
//                iv_sat_1.setImageResource(R.drawable.days_sat_1);
//                //submitButtonDeactivation();
//            }
//
//            if (selected_days.contains("Sunday") || selected_days.contains("Daily")) {
//                iv_sun_1.setTag(1);
//                iv_sun_1.setImageResource(R.drawable.days_sun_2);
//                Selecteddays_first += "Su,";
//                submitButtonActivation();
//
//            } else if ((Integer.valueOf(iv_sun_1.getTag().toString()) == 1)) {
//                iv_sun_1.setTag(0);
//                iv_sun_1.setImageResource(R.drawable.days_sun_1);
//                //submitButtonDeactivation();
//            }

        }

        if (selectrow == 1) {
            days_array2 = outputStrArr;
            /*if(days_array2.length!=7){
                if(cbdaily2.isChecked()){
					cbdaily2.setChecked(false);
				}
			}*/
            Selecteddays_second = "";

//            if (selected_days.contains("Monday") || selected_days.contains("Daily")) {
//                iv_mon_2.setTag(1);
//                iv_mon_2.setImageResource(R.drawable.days_monday_2);
//                Selecteddays_second += "Mo,";
//                submitButtonActivation();
//            } else if ((Integer.valueOf(iv_mon_2.getTag().toString()) == 1)) {
//                iv_mon_2.setTag(0);
//                iv_mon_2.setImageResource(R.drawable.days_monday_1);
//                //	submitButtonDeactivation();
//            }
//            if (selected_days.contains("Tuesday") || selected_days.contains("Daily")) {
//                iv_tue_2.setTag(1);
//                iv_tue_2.setImageResource(R.drawable.days_tuesday_2);
//                Selecteddays_second += "Tu,";
//                submitButtonActivation();
//            } else if ((Integer.valueOf(iv_tue_2.getTag().toString()) == 1)) {
//                iv_tue_2.setTag(0);
//                iv_tue_2.setImageResource(R.drawable.days_tuesday_1);
//                //	submitButtonDeactivation();
//            }
//            if (selected_days.contains("Wednesday") || selected_days.contains("Daily")) {
//                iv_wed_2.setTag(1);
//                iv_wed_2.setImageResource(R.drawable.days_wed_2);
//                Selecteddays_second += "We,";
//                submitButtonActivation();
//            } else if ((Integer.valueOf(iv_wed_2.getTag().toString()) == 1)) {
//                iv_wed_2.setTag(0);
//                iv_wed_2.setImageResource(R.drawable.days_wed_1);
//                //	submitButtonDeactivation();
//            }
//            if (selected_days.contains("Thursday") || selected_days.contains("Daily")) {
//                iv_thu_2.setTag(1);
//                iv_thu_2.setImageResource(R.drawable.days_thu_2);
//                Selecteddays_second += "Th,";
//                submitButtonActivation();
//            } else if ((Integer.valueOf(iv_thu_2.getTag().toString()) == 1)) {
//                iv_thu_2.setTag(0);
//                iv_thu_2.setImageResource(R.drawable.days_thu_1);
//                //	submitButtonDeactivation();
//            }
//            if (selected_days.contains("Friday") || selected_days.contains("Daily")) {
//                iv_fri_2.setTag(1);
//                iv_fri_2.setImageResource(R.drawable.days_fri_2);
//                Selecteddays_second += "Fr,";
//                submitButtonActivation();
//            } else if ((Integer.valueOf(iv_fri_2.getTag().toString()) == 1)) {
//                iv_fri_2.setTag(0);
//                iv_fri_2.setImageResource(R.drawable.days_fri_1);
//                //	submitButtonDeactivation();
//            }
//
//            if (selected_days.contains("Saturday") || selected_days.contains("Daily")) {
//                iv_sat_2.setTag(1);
//                iv_sat_2.setImageResource(R.drawable.days_sat_2);
//                Selecteddays_second += "Sa,";
//
//                submitButtonActivation();
//            } else if ((Integer.valueOf(iv_sat_2.getTag().toString()) == 1)) {
//                iv_sat_2.setTag(0);
//                iv_sat_2.setImageResource(R.drawable.days_sat_1);
//                //	submitButtonDeactivation();
//            }
//
//            if (selected_days.contains("Sunday") || selected_days.contains("Daily")) {
//                iv_sun_2.setTag(1);
//                iv_sun_2.setImageResource(R.drawable.days_sun_2);
//                Selecteddays_second += "Su,";
//                submitButtonActivation();
//
//            } else if ((Integer.valueOf(iv_sun_2.getTag().toString()) == 1)) {
//                iv_sun_2.setTag(0);
//                iv_sun_2.setImageResource(R.drawable.days_sun_1);
//                //	submitButtonDeactivation();
//            }

        }

        if (selectrow == 2) {
            days_array3 = outputStrArr;
            /*if(days_array3.length!=7){
                if(cbdaily3.isChecked()){
					cbdaily3.setChecked(false);
				}
			}*/
            Selecteddays_third = "";

//            if (selected_days.contains("Monday") || selected_days.contains("Daily")) {
//                iv_mon_3.setTag(1);
//                iv_mon_3.setImageResource(R.drawable.days_monday_2);
//                Selecteddays_third += "Mo,";
//                submitButtonActivation();
//            } else if ((Integer.valueOf(iv_mon_3.getTag().toString()) == 1)) {
//                iv_mon_3.setTag(0);
//                iv_mon_3.setImageResource(R.drawable.days_monday_1);
//                //	submitButtonDeactivation();
//            }
//            if (selected_days.contains("Tuesday") || selected_days.contains("Daily")) {
//                iv_tue_3.setTag(1);
//                iv_tue_3.setImageResource(R.drawable.days_tuesday_2);
//                Selecteddays_third += "Tu,";
//                submitButtonActivation();
//            } else if ((Integer.valueOf(iv_tue_3.getTag().toString()) == 1)) {
//                iv_tue_3.setTag(0);
//                iv_tue_3.setImageResource(R.drawable.days_tuesday_1);
//                //	submitButtonDeactivation();
//            }
//            if (selected_days.contains("Wednesday") || selected_days.contains("Daily")) {
//                iv_wed_3.setTag(1);
//                iv_wed_3.setImageResource(R.drawable.days_wed_2);
//                Selecteddays_third += "We,";
//                submitButtonActivation();
//            } else if ((Integer.valueOf(iv_wed_3.getTag().toString()) == 1)) {
//                iv_wed_3.setTag(0);
//                iv_wed_3.setImageResource(R.drawable.days_wed_1);
//                //	submitButtonDeactivation();
//            }
//            if (selected_days.contains("Thursday") || selected_days.contains("Daily")) {
//                iv_thu_3.setTag(1);
//                iv_thu_3.setImageResource(R.drawable.days_thu_2);
//                Selecteddays_third += "Th,";
//                submitButtonActivation();
//            } else if ((Integer.valueOf(iv_thu_3.getTag().toString()) == 1)) {
//                iv_thu_3.setTag(0);
//                iv_thu_3.setImageResource(R.drawable.days_thu_1);
//                //	submitButtonDeactivation();
//            }
//            if (selected_days.contains("Friday") || selected_days.contains("Daily")) {
//                iv_fri_3.setTag(1);
//                iv_fri_3.setImageResource(R.drawable.days_fri_2);
//                Selecteddays_third += "Fr,";
//                submitButtonActivation();
//            } else if ((Integer.valueOf(iv_fri_3.getTag().toString()) == 1)) {
//                iv_fri_3.setTag(0);
//                iv_fri_3.setImageResource(R.drawable.days_fri_1);
//                //	submitButtonDeactivation();
//            }
//
//            if (selected_days.contains("Saturday") || selected_days.contains("Daily")) {
//                iv_sat_3.setTag(1);
//                iv_sat_3.setImageResource(R.drawable.days_sat_2);
//                Selecteddays_third += "Sa,";
//
//                submitButtonActivation();
//            } else if ((Integer.valueOf(iv_sat_3.getTag().toString()) == 1)) {
//                iv_sat_3.setTag(0);
//                iv_sat_3.setImageResource(R.drawable.days_sat_1);
//                //	submitButtonDeactivation();
//            }
//
//            if (selected_days.contains("Sunday") || selected_days.contains("Daily")) {
//                iv_sun_3.setTag(1);
//                iv_sun_3.setImageResource(R.drawable.days_sun_2);
//                Selecteddays_third += "Su,";
//                submitButtonActivation();
//
//            } else if ((Integer.valueOf(iv_sun_3.getTag().toString()) == 1)) {
//                iv_sun_3.setTag(0);
//                iv_sun_3.setImageResource(R.drawable.days_sun_1);
//                //	submitButtonDeactivation();
//            }

        }
        if (selectrow == 3) {
            days_array4 = outputStrArr;
            /*if(days_array4.length!=7){
                if(cbdaily4.isChecked()){
					cbdaily4.setChecked(false);
				}
			}*/
            Selecteddays_fourth = "";

//            if (selected_days.contains("Monday") || selected_days.contains("Daily")) {
//                iv_mon_4.setTag(1);
//                iv_mon_4.setImageResource(R.drawable.days_monday_2);
//                Selecteddays_fourth += "Mo,";
//                submitButtonActivation();
//            } else if ((Integer.valueOf(iv_mon_4.getTag().toString()) == 1)) {
//                iv_mon_4.setTag(0);
//                iv_mon_4.setImageResource(R.drawable.days_monday_1);
//                //	submitButtonDeactivation();
//            }
//            if (selected_days.contains("Tuesday") || selected_days.contains("Daily")) {
//                iv_tue_4.setTag(1);
//                iv_tue_4.setImageResource(R.drawable.days_tuesday_2);
//                Selecteddays_fourth += "Tu,";
//                submitButtonActivation();
//            } else if ((Integer.valueOf(iv_tue_4.getTag().toString()) == 1)) {
//                iv_tue_4.setTag(0);
//                iv_tue_4.setImageResource(R.drawable.days_tuesday_1);
//                //	submitButtonDeactivation();
//            }
//            if (selected_days.contains("Wednesday") || selected_days.contains("Daily")) {
//                iv_wed_4.setTag(1);
//                iv_wed_4.setImageResource(R.drawable.days_wed_2);
//                Selecteddays_fourth += "We,";
//                submitButtonActivation();
//            } else if ((Integer.valueOf(iv_wed_4.getTag().toString()) == 1)) {
//                iv_wed_4.setTag(0);
//                iv_wed_4.setImageResource(R.drawable.days_wed_1);
//                //	submitButtonDeactivation();
//            }
//            if (selected_days.contains("Thursday") || selected_days.contains("Daily")) {
//                iv_thu_4.setTag(1);
//                iv_thu_4.setImageResource(R.drawable.days_thu_2);
//                Selecteddays_fourth += "Th,";
//                submitButtonActivation();
//            } else if ((Integer.valueOf(iv_thu_4.getTag().toString()) == 1)) {
//                iv_thu_4.setTag(0);
//                iv_thu_4.setImageResource(R.drawable.days_thu_1);
//                //	submitButtonDeactivation();
//            }
//            if (selected_days.contains("Friday") || selected_days.contains("Daily")) {
//                iv_fri_4.setTag(1);
//                iv_fri_4.setImageResource(R.drawable.days_fri_2);
//                Selecteddays_fourth += "Fr,";
//                submitButtonActivation();
//            } else if ((Integer.valueOf(iv_fri_4.getTag().toString()) == 1)) {
//                iv_fri_4.setTag(0);
//                iv_fri_4.setImageResource(R.drawable.days_fri_1);
//                //	submitButtonDeactivation();
//            }
//
//            if (selected_days.contains("Saturday") || selected_days.contains("Daily")) {
//                iv_sat_4.setTag(1);
//                iv_sat_4.setImageResource(R.drawable.days_sat_2);
//                Selecteddays_fourth += "Sa,";
//
//                submitButtonActivation();
//            } else if ((Integer.valueOf(iv_sat_4.getTag().toString()) == 1)) {
//                iv_sat_4.setTag(0);
//                iv_sat_4.setImageResource(R.drawable.days_sat_1);
//                //	submitButtonDeactivation();
//            }
//
//            if (selected_days.contains("Sunday") || selected_days.contains("Daily")) {
//                iv_sun_4.setTag(1);
//                iv_sun_4.setImageResource(R.drawable.days_sun_2);
//                Selecteddays_fourth += "Su,";
//                submitButtonActivation();
//
//            } else if ((Integer.valueOf(iv_sun_4.getTag().toString()) == 1)) {
//                iv_sun_4.setTag(0);
//                iv_sun_4.setImageResource(R.drawable.days_sun_1);
//                //	submitButtonDeactivation();
//            }
        }
        System.out.println("selected days1" + Selecteddays_first);
        System.out.println("selected days2" + Selecteddays_second);
        System.out.println("selected days3" + Selecteddays_third);
        System.out.println("selected days4" + Selecteddays_fourth);
        daysselectObj.dismiss();
    }


    public boolean Validation() {
        boolean flag = true;

        if ((etdosage1.getText().toString().length() != 0 || tvStartdate.getText().toString().length() != 0 || etduration1.getText().toString().length() != 0)) {

            if (etdrug1.getText().toString().length() != 0) {
                flag = true;
            } else {
                flag = false;
            }
        }

        if ((etdosage2.getText().toString().length() != 0 || tvStartdate1.getText().toString().length() != 0 || etduration2.getText().toString().length() != 0 || srquantity2.getText().toString().length() != 0)) {

            if (etdrug2.getText().toString().length() != 0) {
                flag = true;
            } else {
                flag = false;
            }
        }

        if ((etdosage3.getText().toString().length() != 0 || tvStartdate2.getText().toString().length() != 0 || etduration3.getText().toString().length() != 0 || srquantity3.getText().toString().length() != 0)) {

            if (etdrug3.getText().toString().length() != 0) {
                flag = true;
            } else {
                flag = false;
            }
        }

        if ((etdosage4.getText().toString().length() != 0 || tvStartdate3.getText().toString().length() != 0 || etduration4.getText().toString().length() != 0 || srquantity4.getText().toString().length() != 0)) {

            if (etdrug4.getText().toString().length() != 0) {
                flag = true;
            } else {
                flag = false;
            }
        }

        for (int k = 0; k < AlDrugs.size(); k++) {
            int keyvalue = k + 5;

            String days[] = daysSelectedArray.get(keyvalue);

            if (AlDosage.get(k).getText().toString().length() != 0 || AlDuration.get(k).getText().toString().length() != 0 || AlDateView.get(k).getText().toString().length() != 0 || AlQuantity.get(k).getText().toString().length() != 0 || daysSelectedArray.containsKey(keyvalue)) {
                if (AlDrugs.get(k).getText().toString().length() != 0) {
                    flag = true;
                } else {
                    flag = false;
                }
            }
        }

        return flag;

    }

    @Override
    public void onPrescription() {
        // TODO Auto-generated method stub
        String prescriptiondownload = "";

        SharedPreferences sp = getActivity().getSharedPreferences("Login", 0);
        String specialization = sp.getString("spel", null);
        if (!specialization.equalsIgnoreCase("gynecologist obstetrician")) {

            getpatientcurrentvitals();
        } else {
            getpatientpersonalinformation();
        }
        //fetechingdiseasenames_server();

    }


    @SuppressLint("NewApi")
    private void printfile(String pathname) {
        // TODO Auto-generated method stub
        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion >= android.os.Build.VERSION_CODES.KITKAT) {
            PrintManager printManager = (PrintManager) getActivity().getSystemService(Context.PRINT_SERVICE);
            String jobName = getString(R.string.app_name) + " Document";
            PrintHelper pda = new PrintHelper(getActivity(), pathname);
            printManager.print(jobName, pda, null);
        } else {
            Toast.makeText(getActivity(), "Print works only KITKAT or Above Versions", Toast.LENGTH_LONG).show();
        }
    }


    public void filewrite(ParseObject combinedobject) {
        FileOperationsNew fop = new FileOperationsNew();
        System.out.println("combinedobject" + combinedobject.getString("weight_result"));
        fop.write(getActivity(), uniqID.getText().toString(), combinedobject);
        if (fop.write(getActivity(), uniqID.getText().toString(), combinedobject)) {
            //progressdialog.dismiss();
            Toast.makeText(getActivity(),
                    uniqID.getText().toString() + ".pdf created", Toast.LENGTH_SHORT)
                    .show();

            String pathname = "/sdcard/" + uniqID.getText().toString() + ".pdf";
            printfile(pathname);

        } else {
            //progressdialog.dismiss();
            Toast.makeText(getActivity(), "pdf not created",
                    Toast.LENGTH_SHORT).show();
        }

    }

    public void filewritegynacologist(ParseObject combinedobject) {
        FileOperationsGynacologist fop = new FileOperationsGynacologist();
        System.out.println("combinedobject" + combinedobject.getString("weight_result"));
        fop.write(getActivity(), uniqID.getText().toString(), combinedobject);
        if (fop.write(getActivity(), uniqID.getText().toString(), combinedobject)) {
            //progressdialog.dismiss();
            Toast.makeText(getActivity(),
                    uniqID.getText().toString() + ".pdf created", Toast.LENGTH_SHORT)
                    .show();

            String pathname = "/sdcard/" + uniqID.getText().toString() + ".pdf";
            printfile(pathname);

        } else {
            //progressdialog.dismiss();
            Toast.makeText(getActivity(), "pdf not created",
                    Toast.LENGTH_SHORT).show();
        }

    }


    public void textfileCreation(ParseObject combinedobject) {

        try {
            File root;
            File gpxfile;
            System.out.println("directory" + Environment.getExternalStorageDirectory());

			/*root = new File(Environment.getExternalStorageDirectory(),uniqID.getText().toString());
            if (!root.exists()) {
				root.mkdirs();
			}
			gpxfile = new File(root, sFileName);*/

            gpxfile = new File(Environment.getExternalStorageDirectory(), uniqID.getText().toString() + "text" + ".txt");

            FileWriter writer = new FileWriter(gpxfile);
            String result = "";
            String symptsynd;
            String name = "\tweight" + "\t\t" + "BloodPressure" + "\t\t" + "Pulse" + "\t\t" + "RespiratoryRate" + "\t\t" + "SPO2" + "\t\t" + "Temperature";
            String prescription = "\tdrug" + "\t\t" + "dosage" + "\t\t" + "Startdate" + "\t\t" + "duration" + "\t\t" + "days" + "\t\t\t" + "Quantity";

            if (combinedobject.getString("weight_result") != null) {
                result += "\t" + combinedobject.getString("weight_result");
            } else {
                result += "\t" + "--";
            }
            if (combinedobject.getString("bloodPressure_result") != null) {
                result += "\t\t" + combinedobject.getString("bloodPressure_result");
            } else {
                result += "\t\t" + "--";
            }
            if (combinedobject.getString("pulse_result") != null) {
                result += "\t\t\t" + combinedobject.getString("pulse_result");
            } else {
                result += "\t\t\t" + "--";
            }
            if (combinedobject.getString("respRate_result") != null) {
                result += "\t\t\t" + combinedobject.getString("respRate_result");
            } else {
                result += "\t\t\t" + "--";
            }
            if (combinedobject.getString("spo2_result") != null) {
                result += "\t\t" + combinedobject.getString("spo2_result");
            } else {
                result += "\t\t" + "--";
            }
            if (combinedobject.getString("temperature") != null) {
                result += "\t\t" + combinedobject.getString("temperature");
            } else {
                result += "\t\t" + "--";
            }

            symptsynd = "\tSymptoms" + "\t\t\t\tSyndromes";
            String symptom, syndrome;
            if (combinedobject.getString("symptoms") != null) {
                symptom = combinedobject.getString("symptoms");
            } else {
                symptom = "Nil";
            }
            if (combinedobject.getString("syndromes") != null) {
                syndrome = combinedobject.getString("syndromes");
            } else {
                syndrome = "Nil";
            }

            StringBuilder sb = new StringBuilder(symptom);
            StringBuilder sb1 = new StringBuilder(syndrome);
            int i = 0, k = 0;
            while ((i = sb.indexOf(" ", i + 30)) != -1) {
                sb.replace(i, i + 1, "\n");
            }
            while ((k = sb1.indexOf(" ", k + 30)) != -1) {
                sb1.replace(k, k + 1, "\n");
            }

            String symptsyndromeresult = sb.toString() + "\t\t\t\t" + sb1.toString();


            writer.append("\n\n\n" + name);
            writer.append("\n" + result);
            writer.append("\n\n" + symptsynd);
            writer.append("\n\n" + symptsyndromeresult);
            writer.append("\n\n\n\n\n\n\n" + prescription);

            String prescriptiondata;
            if (combinedobject.getJSONArray("drug") != null) {
                for (int j = 0; j < combinedobject.getJSONArray("drug").length(); j++) {
                    try {

                        String drugname = combinedobject.getJSONArray("drug").getString(j);
                        StringBuilder sb2 = new StringBuilder(drugname);
                        while ((i = sb2.indexOf(" ", i + 10)) != -1) {
                            sb2.replace(i, i + 1, "\n");
                        }

                        prescriptiondata = "\t" + sb2.toString() + "\t\t" + combinedobject.getJSONArray("dosage").getString(j) + "\t\t" + combinedobject.getJSONArray("drugStartDate").getString(j) + "\t\t" + combinedobject.getJSONArray("duration").getString(j) + "\t\t" + combinedobject.getJSONArray("days").getString(j) + "\t" + combinedobject.getJSONArray("quantity").getString(j);
                        writer.append("\n\n" + prescriptiondata);


                    } catch (JSONException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }

                }
            }


            writer.flush();
            writer.close();


            //	printfile(uniqID.getText().toString());


        } catch (IOException e) {
            Toast.makeText(getActivity(), "error", Toast.LENGTH_LONG).show();
            e.printStackTrace();

        }


    }


    public void excelfileCreation(ParseObject combinedobject) {


        String Fnamexls = uniqID.getText().toString() + "excel";
        String fpath = "/sdcard/" + Fnamexls + ".xls";
        /*File sdCard = Environment.getExternalStorageDirectory();
        File directory = new File (sdCard.getAbsolutePath() + "/newfolder");
		directory.mkdirs();*/
        File file = new File(fpath);

        WorkbookSettings wbSettings = new WorkbookSettings();

        wbSettings.setLocale(new Locale("en", "EN"));

        WritableWorkbook workbook;
        try {
            int a = 1;
            workbook = Workbook.createWorkbook(file, wbSettings);
            //workbook.createSheet("Report", 0);
            WritableSheet sheet = workbook.createSheet("First Sheet", 0);


            Label labelweight = new Label(0, 1, "Weight");

            Label labelpressure = new Label(1, 1, "Blood Pressure");
            Label labelpulse = new Label(2, 1, "Pulse");
            Label labelrespiratory = new Label(3, 1, "Respiratory Rate");
            Label labelspo2 = new Label(4, 1, "Sp O2");
            Label labelQuantity = new Label(5, 1, "Temperature");
            Label weighttext, bloodpressuretext, pulsetext, Respiratorytext, spo2text, temptext;
            Label Symptoms = new Label(0, 4, "Symptoms");
            Label Syndromes = new Label(3, 4, "Syndromes");
            Label symptomstext, syndromestext;
            Label drug = new Label(0, 8, "Drug");
            Label dosage = new Label(1, 8, "Dosage");
            Label startdate = new Label(2, 8, "Start Date");
            Label duration = new Label(3, 8, "Duration");
            Label days = new Label(4, 8, "Days");
            Label quantity = new Label(5, 8, "Quantity");


            if (combinedobject.getString("weight_result") != null && combinedobject.getString("weight_result") != "Nil") {
                weighttext = new Label(0, 2, combinedobject.getString("weight_result") + " " + "kg");
            } else {
                weighttext = new Label(0, 2, "--" + " " + "kg");
            }

            if (combinedobject.getString("bloodPressure_result") != null && combinedobject.getString("bloodPressure_result") != "Nil") {
                bloodpressuretext = new Label(1, 2, combinedobject.getString("bloodPressure_result") + " " + "Hg mm");
            } else {
                bloodpressuretext = new Label(1, 2, "--" + " " + "Hg mm");
            }

            if (combinedobject.getString("pulse_result") != null && combinedobject.getString("pulse_result") != "Nil") {
                pulsetext = new Label(2, 2, combinedobject.getString("pulse_result") + " " + "beats/min");
            } else {
                pulsetext = new Label(2, 2, "--" + " " + "beats/min");
            }
            if (combinedobject.getString("respRate_result") != null && combinedobject.getString("respRate_result") != "Nil") {
                Respiratorytext = new Label(3, 2, combinedobject.getString("respRate_result") + " " + "breaths/min");
            } else {
                Respiratorytext = new Label(3, 2, "--" + " " + "breaths/min");
            }
            if (combinedobject.getString("spo2_result") != null && combinedobject.getString("spo2_result") != "Nil") {
                spo2text = new Label(4, 2, combinedobject.getString("spo2_result") + " " + "%");
            } else {
                spo2text = new Label(4, 2, "--" + " " + "%");
            }
            if (combinedobject.getString("temperature") != null && combinedobject.getString("temperature") != "Nil") {
                temptext = new Label(5, 2, combinedobject.getString("temperature") + " " + "c");
            } else {
                temptext = new Label(5, 2, "--" + " " + "c");
            }


            if (combinedobject.getString("symptoms") != null) {
                symptomstext = new Label(0, 6, combinedobject.getString("symptoms"));
            } else {
                symptomstext = new Label(0, 6, "Nil");
            }

            if (combinedobject.getString("symptoms") != null) {
                syndromestext = new Label(3, 6, combinedobject.getString("syndromes"));
            } else {
                syndromestext = new Label(3, 6, "Nil");
            }


            Label drugtext = null;
            Label dosagetext = null;
            Label startdatetext = null;
            Label durationtext = null;
            Label daystext = null;
            Label quantitytext = null;
            int k = 9;
            if (combinedobject.getJSONArray("drug") != null) {
                for (int j = 0; j < combinedobject.getJSONArray("drug").length(); j++) {
                    try {
                        drugtext = new Label(0, k, combinedobject.getJSONArray("drug").getString(j));
                        k++;
                    } catch (JSONException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            } else {
                drugtext = new Label(0, k, "Nil");
            }

            k = 9;
            if (combinedobject.getJSONArray("dosage") != null) {
                for (int j = 0; j < combinedobject.getJSONArray("dosage").length(); j++) {
                    try {
                        dosagetext = new Label(1, k, combinedobject.getJSONArray("dosage").getString(j));
                        k++;
                    } catch (JSONException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            } else {
                dosagetext = new Label(1, k, "Nil");
            }

            k = 9;
            if (combinedobject.getJSONArray("drugStartDate") != null) {
                for (int j = 0; j < combinedobject.getJSONArray("drugStartDate").length(); j++) {
                    try {
                        startdatetext = new Label(2, k, combinedobject.getJSONArray("drugStartDate").getString(j));
                        k++;
                    } catch (JSONException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            } else {
                startdatetext = new Label(2, k, "Nil");
            }

            k = 9;
            if (combinedobject.getJSONArray("duration") != null) {
                for (int j = 0; j < combinedobject.getJSONArray("duration").length(); j++) {
                    try {
                        durationtext = new Label(3, k, combinedobject.getJSONArray("duration").getString(j));
                        k++;
                    } catch (JSONException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            } else {
                durationtext = new Label(3, k, "Nil");
            }

            k = 9;
            if (combinedobject.getJSONArray("days") != null) {
                for (int j = 0; j < combinedobject.getJSONArray("days").length(); j++) {
                    try {
                        daystext = new Label(4, k, combinedobject.getJSONArray("days").getString(j));
                        k++;
                    } catch (JSONException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            } else {
                daystext = new Label(4, k, "Nil");
            }

            k = 9;
            if (combinedobject.getJSONArray("quantity") != null) {
                for (int j = 0; j < combinedobject.getJSONArray("quantity").length(); j++) {
                    try {
                        quantitytext = new Label(5, k, combinedobject.getJSONArray("quantity").getString(j));
                        k++;
                    } catch (JSONException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            } else {
                quantitytext = new Label(5, k, "Nil");
            }


            try {

                int heightInPoints = 50 * 400;
                sheet.setColumnView(0, heightInPoints);
                sheet.setColumnView(1, heightInPoints);
                sheet.setColumnView(2, heightInPoints);
                sheet.setColumnView(3, heightInPoints);
                sheet.setColumnView(4, heightInPoints);
                sheet.setColumnView(5, heightInPoints);

                sheet.addCell(labelweight);
                sheet.addCell(labelpressure);

                sheet.addCell(labelpulse);
                sheet.addCell(labelrespiratory);
                sheet.addCell(labelspo2);
                sheet.addCell(labelQuantity);

                sheet.addCell(weighttext);
                sheet.addCell(bloodpressuretext);
                sheet.addCell(pulsetext);
                sheet.addCell(Respiratorytext);
                sheet.addCell(spo2text);
                sheet.addCell(temptext);

                sheet.addCell(Symptoms);
                sheet.addCell(Syndromes);

                sheet.addCell(symptomstext);
                sheet.addCell(syndromestext);

                sheet.addCell(drug);
                sheet.addCell(dosage);
                sheet.addCell(startdate);
                sheet.addCell(duration);
                sheet.addCell(days);
                sheet.addCell(quantity);

                sheet.addCell(drugtext);
                sheet.addCell(dosagetext);
                sheet.addCell(startdatetext);
                sheet.addCell(durationtext);
                sheet.addCell(daystext);
                sheet.addCell(quantitytext);

            } catch (RowsExceededException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (WriteException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            workbook.write();
            try {
                workbook.close();
            } catch (WriteException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            //createExcel(excelSheet);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }


    private void fetechingdiseasenames_server() {
        // TODO Auto-generated method stub

        if (diseasenamearraylist.size() == 0) {
            mProgressDialog = new ProgressDialog(getActivity());
            mProgressDialog.setCancelable(false);
            mProgressDialog.setMessage("Retrieving Patient Data...");
            mProgressDialog.show();


            ParseQuery<ParseObject> query = ParseQuery.getQuery("DiseaseDatabase");
            query.fromLocalDatastore();
            query.setLimit(1000);
            query.whereNotEqualTo("DiseaseName", null);
            query.findInBackground(new FindCallback<ParseObject>() {

                @Override
                public void done(List<ParseObject> objects, ParseException e) {

                    // TODO Auto-generated method stub
                    if (e == null) {
                        if (objects.size() > 0) {
                            // query found a user
                            Log.e("Login", "Id exist");
                            patient = objects.get(0);
                            for (int i = 0; i < objects.size(); i++) {
                                Diseasenameclass diseasenameObj = new Diseasenameclass();
                                diseasenameObj.setDiseasename(objects.get(i).getString("DiseaseName"));
                                diseasenameObj.setIcdnumber(objects.get(i).getString("ICDNumber"));
                                diseasenamearraylist.add(diseasenameObj);
                                //System.out.println(objects.get(i).getString("DiseaseName"));
                            }


                            diseaseselect_server();
                        }
                    } else {
                        mProgressDialog.dismiss();
                        Toast.makeText(getActivity(), "Sorry, something went wrong", Toast.LENGTH_LONG).show();
                    }

                }


            });

        } else {
            getpatientcurrentvitals();

        }
    }

    private void diseaseselect_server() {
        // TODO Auto-generated method stub

        ParseQuery<ParseObject> query = ParseQuery.getQuery("DiseaseDatabase");
        query.fromLocalDatastore();
        query.setLimit(589);
        query.setSkip(1000);
        query.whereNotEqualTo("DiseaseName", null);
        query.findInBackground(new FindCallback<ParseObject>() {

            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                //	mProgressDialog.dismiss();
                // TODO Auto-generated method stub
                if (e == null) {
                    if (objects.size() > 0) {
                        // query found a user
                        Log.e("Login", "Id exist");
                        patient = objects.get(0);
                        for (int i = 0; i < objects.size(); i++) {
                            Diseasenameclass diseasenameObj = new Diseasenameclass();
                            diseasenameObj.setDiseasename(objects.get(i).getString("DiseaseName"));
                            diseasenameObj.setIcdnumber(objects.get(i).getString("ICDNumber"));
                            diseasenamearraylist.add(diseasenameObj);
                            //System.out.println("objects size"+objects.get(i).getString("DiseaseName"));
                        }

                        getpatientcurrentvitals();
                    }
                } else {
                    mProgressDialog.dismiss();
                    Toast.makeText(getActivity(), "Sorry, something went wrong", Toast.LENGTH_LONG).show();
                }

            }

        });
    }


    private void getpatientcurrentvitals() {

        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setCancelable(false);
        mProgressDialog.setMessage("Retrieving Patient Data...");
        mProgressDialog.show();

        ParseQuery<ParseObject> query = ParseQuery.getQuery("Patients");
        query.fromLocalDatastore();
        query.whereEqualTo("patientID", vpuid);
        query.findInBackground(new FindCallback<ParseObject>() {

            @Override
            public void done(List<ParseObject> object, ParseException e) {
                // TODO Auto-generated method stub
                //	mProgressDialog.dismiss();
                if (e == null) {

                    if (object.size() > 0) {

                        patientcurrentvitalsdetails = object.get(0);
                        getpatientdetails();

                    } else {
                        getpatientdetails();
                    }
                }

            }
        });
    }


    private void getpatientpersonalinformation() {
        // TODO Auto-generated method stub

		/*mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setCancelable(false);
		mProgressDialog.setMessage("Retrieving Patient Data...");
		mProgressDialog.show();*/
        ParseQuery<ParseObject> query = ParseQuery.getQuery("Patients");
        query.fromLocalDatastore();
        query.whereEqualTo("patientID", vpuid);

        query.findInBackground(new FindCallback<ParseObject>() {

            @Override
            public void done(List<ParseObject> object, ParseException e) {
                // TODO Auto-generated method stub
                if (e == null) {

                    if (object.size() > 0) {

                        patientpersonalInformation = object.get(0);
                        getpatientdetailsgynacologist();

                    } else {
                        getpatientdetailsgynacologist();
                    }

                }

            }
        });


    }


    private void getpatientdetails() {
        // TODO Auto-generated method stub

        Calendar c = Calendar.getInstance();
        System.out.println("Current time => " + c.getTime());
        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
        String dDate = df.format(c.getTime());

        final ParseQuery<ParseObject> query = ParseQuery.getQuery("Diagnosis");
        query.fromLocalDatastore();
        query.whereEqualTo("patientID", vpuid);
        query.whereEqualTo("createdDate", dDate);

        query.findInBackground(new FindCallback<ParseObject>() {
            private int prescriptionindex;
            private int diagnosisindex;

            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                // TODO Auto-generated method stub

                final List<ParseObject> objectslist = new ArrayList<ParseObject>();
                Log.e("Login", "done");
                if (e == null) {
                    if (objects.size() > 0 && FragmentActivityContainer.treatmangement_save_check == 0 && ((FragmentActivityContainer) getActivity()).getdatasaved() == true) {

                        ArrayList arrayList = new ArrayList();
                        ArrayList arrayList1 = new ArrayList();
                        for (int i = 0; i < objects.size(); i++) {
                            if (objects.get(i).getDouble("typeFlag") == 2) {
                                System.out.println("number....." + objects.get(i).getNumber("createdatetime"));

                                arrayList.add(objects.get(i).getNumber("createdatetime"));
                            } else {
                                arrayList1.add(objects.get(i).getNumber("createdatetime"));
                            }
                        }
                        Long val = (long) 0;
                        if (arrayList.size() != 0) {
                            val = Long.parseLong(Collections.max(arrayList) + "");
                        }
                        Long val1 = (long) 0;
                        if (arrayList1.size() != 0) {
                            val1 = Long.parseLong(Collections.max(arrayList1) + "");
                        }
                        for (int j = 0; j < objects.size(); j++) {
                            if (objects.get(j).getNumber("createdatetime") == val) {
                                prescriptionindex = j;
                            }
                            if (objects.get(j).getNumber("createdatetime") == val1) {
                                diagnosisindex = j;
                            }
                        }

                        System.out.println("objects size" + objects.size());
                        ArrayList<Integer> listed = new ArrayList<Integer>();
                        listed.clear();

                        final ParseObject combinedobject = new ParseObject("");
                        combinedobject.put("createdAt", formattedDate);

                        if (objects.get(prescriptionindex).getDouble("typeFlag") == 2) {

                            if (objects.get(prescriptionindex).getJSONArray("days") != null) {
                                System.out.println("days" + objects.get(prescriptionindex).getJSONArray("days"));
                                combinedobject.put("days", objects.get(prescriptionindex).getJSONArray("days"));
                            }
                            if (objects.get(prescriptionindex).getJSONArray("drugStartDate") != null) {
                                System.out.println("drugStartDate" + objects.get(prescriptionindex).getJSONArray("drugStartDate"));
                                combinedobject.put("drugStartDate", objects.get(prescriptionindex).getJSONArray("drugStartDate"));

                            }
                            if (objects.get(prescriptionindex).getJSONArray("dosage") != null) {
                                combinedobject.put("dosage", objects.get(prescriptionindex).getJSONArray("dosage"));
                            }
                            if (objects.get(prescriptionindex).getJSONArray("drug") != null) {
                                System.out.println("drug name" + objects.get(prescriptionindex).getJSONArray("drug"));
                                combinedobject.put("drug", objects.get(prescriptionindex).getJSONArray("drug"));
                            }
                            if (objects.get(prescriptionindex).getJSONArray("duration") != null) {
                                combinedobject.put("duration", objects.get(prescriptionindex).getJSONArray("duration"));
                            }
                            if (objects.get(prescriptionindex).getJSONArray("quantity") != null) {
                                combinedobject.put("quantity", objects.get(prescriptionindex).getJSONArray("quantity"));
                            }

                        }

                        if (objects.get(diagnosisindex).getDouble("typeFlag") == 1) {
                            if (objects.get(diagnosisindex).getString("additionalComments") != null) {
                                combinedobject.put("additionalComments", objects.get(diagnosisindex).getString("additionalComments"));
                            }

                            if (objects.get(diagnosisindex).getString("suspectedDisease") != null) {
                                combinedobject.put("suspectedDisease", objects.get(diagnosisindex).getString("suspectedDisease"));
                                //combinedobject.put("diagnosisObjId",objects.get(diagnosisindex).getString("diagnosisId"));


                                //combinedobject.put("diagnosisObjId",objects.get(diagnosisindex).getString("diagnosisId"));
                                ArrayList<String> diseasenamecheck = new ArrayList<String>();

                                String[] value1 = (objects.get(diagnosisindex).getString("suspectedDisease").split(","));
                                for (int value = 0; value < value1.length; value++) {
                                    diseasenamecheck.add(value1[value]);
                                }


                                if (_suspecteddiseasenamesObj.size() != 0 && _suspecteddiseasenamesObj != null && objects.get(diagnosisindex).getString("suspectedDisease") != null) {
                                    //compairing_code_taken_disease_fromparse(objects.get(diagnosisindex).getString("suspectedDisease"));

                                    String[] value = objects.get(diagnosisindex).getString("suspectedDisease").split(",");
                                    System.out.println("value size" + value.length);
                                    if (value != null)
                                        for (int k = 0; k < value.length; k++) {

                                            System.out.println("code valuessss from parse" + value[k]);
                                        }
                                    String[] myaccrArray = null;
                                    if (value != null) {
                                        System.out.println("code valuessss from parse1");
                                        myaccrArray = new String[value.length];
                                        for (int j = 0; j < value.length; j++) {
                                            for (int k = 0; k < diseasenamearraylist.size(); k++) {
                                                if ((diseasenamearraylist.get(k).getIcdnumber()) != null) {
                                                    String icd_from_list = diseasenamearraylist.get(k).getIcdnumber().toString().trim();
                                                    String icd_from_selected = value[j].toString().trim();


                                                    if (icd_from_selected.equalsIgnoreCase("other")) {

                                                        myaccrArray[j] = "Other";
                                                    } else if (icd_from_selected.equalsIgnoreCase("none")) {
                                                        myaccrArray[j] = "None";
                                                    } else if (icd_from_selected.equalsIgnoreCase("-")) {
                                                        myaccrArray[j] = "-";
                                                        System.out.println("code valuessss from parse" + icd_from_selected);
                                                    }

                                                    if (!icd_from_selected.equalsIgnoreCase("other") && !icd_from_selected.equalsIgnoreCase("none") && !icd_from_selected.equalsIgnoreCase("-")) {

                                                        if (icd_from_list.equalsIgnoreCase(icd_from_selected)) {
                                                            myaccrArray[j] = diseasenamearraylist.get(k).getDiseasename();
                                                        }
                                                    }
                                                }
                                            }
                                            String disease = "";
                                            for (int i = 0; i < myaccrArray.length; i++) {

                                                disease += myaccrArray[i] + "\n";
                                            }

                                            //ettext.setText(disease);
                                            combinedobject.put("suspectedDisease", disease);
                                        }
                                    }


                                } else {

                                    final ProgressDialog mProgress = new ProgressDialog(getActivity());
                                    mProgress.setMessage("Fetching Details.....");
                                    mProgress.show();
                                    ParseQuery<ParseObject> query = ParseQuery.getQuery("DiseaseDatabase");
                                    query.fromLocalDatastore();
                                    query.whereContainedIn("ICDNumber", diseasenamecheck);
                                    query.findInBackground(new FindCallback<ParseObject>() {

                                        @Override
                                        public void done(List<ParseObject> object, ParseException e) {
                                            // TODO Auto-generated method stub
                                            mProgress.dismiss();
                                            if (e == null) {
                                                if (object.size() > 0) {
                                                    String disease = "";
                                                    for (int diseasesize = 0; diseasesize < object.size(); diseasesize++) {
                                                        System.out.println("names disease" + object.get(diseasesize).getString("DiseaseName"));
                                                        disease += object.get(diseasesize).getString("DiseaseName") + "\n";
                                                    }


                                                    combinedobject.put("suspectedDisease", disease);
                                                }


                                            }

                                        }
                                    });
                                }


                            }

                            if (objects.get(diagnosisindex).getString("symptoms") != null) {
                                combinedobject.put("symptoms", objects.get(diagnosisindex).getString("symptoms"));
                            }
                            if (objects.get(diagnosisindex).getString("syndromes") != null) {
                                combinedobject.put("syndromes", objects.get(diagnosisindex).getString("syndromes"));
                            }
                        }

                        objectslist.add(combinedobject);


                        String treat = et_Treatment.getText().toString();
                        if (treatment != null) {
                            combinedobject.put("treatment", treat);
                        }

                        String followup = tvFollowup.getText().toString();
                        if (followup != null) {
                            combinedobject.put("followup", followup);
                        }

                        if (doctername != null) {
                            combinedobject.put("doctername", doctername);
                        }

                        String patientname = fltmName.getText().toString();
                        if (patientname != null) {
                            combinedobject.put("patientname", patientname);
                        }

                        combinedobject.put("id", uniqID.getText().toString());


                        JSONArray weight_result = new JSONArray();
                        JSONArray spo2_result = new JSONArray();
                        JSONArray pulse_result = new JSONArray();
                        JSONArray respRate_result = new JSONArray();
                        JSONArray height_result = new JSONArray();
                        JSONArray bloodGroup_result = new JSONArray();
                        JSONArray bloodPressure_result = new JSONArray();
                        JSONArray temperature = new JSONArray();

                        if (patientcurrentvitalsdetails != null) {

                            if (patientcurrentvitalsdetails.getJSONArray("weight") != null) {
                                weight_result = patientcurrentvitalsdetails.getJSONArray("weight");
                            }
                            if (patientcurrentvitalsdetails.getJSONArray("spo2") != null) {
                                spo2_result = patientcurrentvitalsdetails.getJSONArray("spo2");
                            }
                            if (patientcurrentvitalsdetails.getJSONArray("pulse") != null) {
                                pulse_result = patientcurrentvitalsdetails.getJSONArray("pulse");
                            }
                            if (patientcurrentvitalsdetails.getJSONArray("respRate") != null) {
                                respRate_result = patientcurrentvitalsdetails.getJSONArray("respRate");
                            }
                            if (patientcurrentvitalsdetails.getJSONArray("height") != null) {
                                height_result = patientcurrentvitalsdetails.getJSONArray("height");
                            }
                            if (patientcurrentvitalsdetails.getJSONArray("bloodGroup") != null) {
                                bloodGroup_result = patientcurrentvitalsdetails.getJSONArray("bloodGroup");
                            }
                            if (patientcurrentvitalsdetails.getJSONArray("bloodPressure") != null) {
                                bloodPressure_result = patientcurrentvitalsdetails.getJSONArray("bloodPressure");
                            }
                            if (patientcurrentvitalsdetails.getJSONArray("temp") != null) {
                                temperature = patientcurrentvitalsdetails.getJSONArray("temp");
                            }
                            JSONArray objectid = patientcurrentvitalsdetails.getJSONArray("diagnosisObjectid");
                            ArrayList<String> object = new ArrayList<String>();
                            /*if(patientcurrentvitalsdetails.getJSONArray("diagnosisObjectid")!=null){
                                for(int k=0;k<objectid.length();k++){
									try {

										if(combinedobject.getString("diagnosisObjId")!=null){
											if(combinedobject.getString("diagnosisObjId").equals(objectid.getString(k))){
												position=k;
											}
										}
										//object.add(objectid.getString(k));
									} catch (JSONException e1) {
										// TODO Auto-generated catch block
										e1.printStackTrace();
									}
								}
							}*/

                        }
                        /*	if(position!=-1){*/
                        /*try {

								combinedobject.put("weight_result",weight_result.getString(position));
								combinedobject.put("spo2_result",spo2_result.getString(position));
								combinedobject.put("temperature",temperature.getString(position));
								combinedobject.put("pulse_result",pulse_result.getString(position));
								combinedobject.put("respRate_result",respRate_result.getString(position));
								combinedobject.put("bloodPressure_result",bloodPressure_result.getString(position));


							} catch (JSONException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
						 */
                        /*}*/


                        try {

                            if (weight_result.length() > 0)
                                combinedobject.put("weight_result", weight_result.getString(weight_result.length() - 1));
                            if (spo2_result.length() > 0)
                                combinedobject.put("spo2_result", spo2_result.getString(spo2_result.length() - 1));
                            if (temperature.length() > 0)
                                combinedobject.put("temperature", temperature.getString(temperature.length() - 1));
                            if (pulse_result.length() > 0)
                                combinedobject.put("pulse_result", pulse_result.getString(pulse_result.length() - 1));
                            if (respRate_result.length() > 0)
                                combinedobject.put("respRate_result", respRate_result.getString(respRate_result.length() - 1));
                            if (bloodPressure_result.length() > 0)
                                combinedobject.put("bloodPressure_result", bloodPressure_result.getString(bloodPressure_result.length() - 1));


							/*combinedobject.put("weight_result",weight_result.getString(position));
                                combinedobject.put("spo2_result",spo2_result.getString(position));
								combinedobject.put("temperature",temperature.getString(position));
								combinedobject.put("pulse_result",pulse_result.getString(position));
								combinedobject.put("respRate_result",respRate_result.getString(position));
								combinedobject.put("bloodPressure_result",bloodPressure_result.getString(position));*/

                        } catch (JSONException e1) {
                            // TODO Auto-generated catch block
                            e1.printStackTrace();
                        }


                        System.out.println("combined drug name" + combinedobject.getJSONArray("drug"));


                        filewrite(combinedobject);
                        excelfileCreation(combinedobject);
                        textfileCreation(combinedobject);

                        listed.clear();
                        mProgressDialog.dismiss();
                    } else {
                        mProgressDialog.dismiss();
                        Toast.makeText(getActivity(), " Please save your prescription  before printing.......", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    e.printStackTrace();
                    mProgressDialog.dismiss();
                    Toast.makeText(getActivity(), "Invalid Patient", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }


    private void getpatientdetailsgynacologist() {
        // TODO Auto-generated method stub

        mgProgressDialog = new ProgressDialog(getActivity());
        mgProgressDialog.setCancelable(false);
        mgProgressDialog.setMessage("Retrieving Patient Data...");
        mgProgressDialog.show();


        Calendar c = Calendar.getInstance();
        System.out.println("Current time => " + c.getTime());
        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
        String dDate = df.format(c.getTime());

        final ParseQuery<ParseObject> query = ParseQuery.getQuery("Diagnosis");
        query.fromLocalDatastore();
        query.whereEqualTo("patientID", vpuid);
        query.whereEqualTo("createdDate", dDate);

        query.findInBackground(new FindCallback<ParseObject>() {
            private int prescriptionindex;
            private int diagnosisindex;

            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                // TODO Auto-generated method stub

                final List<ParseObject> objectslist = new ArrayList<ParseObject>();
                Log.e("Login", "done");
                if (e == null) {
                    if (objects.size() > 0 && FragmentActivityContainer.treatmangement_save_check == 0 && ((FragmentActivityContainer) getActivity()).getdatasaved() == true) {

                        ArrayList arrayList = new ArrayList();
                        ArrayList arrayList1 = new ArrayList();
                        for (int i = 0; i < objects.size(); i++) {
                            if (objects.get(i).getDouble("typeFlag") == 2) {
                                System.out.println("number....." + objects.get(i).getNumber("createdatetime"));

                                arrayList.add(objects.get(i).getNumber("createdatetime"));
                            } else {
                                arrayList1.add(objects.get(i).getNumber("createdatetime"));
                            }
                        }
                        Long val = (long) 0;
                        if (arrayList.size() != 0) {
                            val = Long.parseLong(Collections.max(arrayList) + "");
                        }
                        Long val1 = (long) 0;
                        if (arrayList1.size() != 0) {
                            val1 = Long.parseLong(Collections.max(arrayList1) + "");
                        }
                        for (int j = 0; j < objects.size(); j++) {
                            if (objects.get(j).getNumber("createdatetime") == val) {
                                prescriptionindex = j;
                            }
                            if (objects.get(j).getNumber("createdatetime") == val1) {
                                diagnosisindex = j;
                            }
                        }

                        System.out.println("objects size" + objects.size());
                        ArrayList<Integer> listed = new ArrayList<Integer>();
                        listed.clear();

                        final ParseObject combinedobject = new ParseObject("");
                        combinedobject.put("createdAt", formattedDate);

                        if (objects.get(prescriptionindex).getDouble("typeFlag") == 2) {

                            if (objects.get(prescriptionindex).getJSONArray("days") != null) {
                                System.out.println("days" + objects.get(prescriptionindex).getJSONArray("days"));
                                combinedobject.put("days", objects.get(prescriptionindex).getJSONArray("days"));
                            }
                            if (objects.get(prescriptionindex).getJSONArray("drugStartDate") != null) {
                                System.out.println("drugStartDate" + objects.get(prescriptionindex).getJSONArray("drugStartDate"));
                                combinedobject.put("drugStartDate", objects.get(prescriptionindex).getJSONArray("drugStartDate"));

                            }
                            if (objects.get(prescriptionindex).getJSONArray("dosage") != null) {
                                combinedobject.put("dosage", objects.get(prescriptionindex).getJSONArray("dosage"));
                            }
                            if (objects.get(prescriptionindex).getJSONArray("drug") != null) {
                                System.out.println("drug name" + objects.get(prescriptionindex).getJSONArray("drug"));
                                combinedobject.put("drug", objects.get(prescriptionindex).getJSONArray("drug"));
                            }
                            if (objects.get(prescriptionindex).getJSONArray("duration") != null) {
                                combinedobject.put("duration", objects.get(prescriptionindex).getJSONArray("duration"));
                            }
                            if (objects.get(prescriptionindex).getJSONArray("quantity") != null) {
                                combinedobject.put("quantity", objects.get(prescriptionindex).getJSONArray("quantity"));
                            }

                        }

                        if (objects.get(diagnosisindex).getDouble("typeFlag") == 1) {
                            if (objects.get(diagnosisindex).getString("additionalComments") != null) {
                                combinedobject.put("additionalComments", objects.get(diagnosisindex).getString("additionalComments"));
                            }

                            if (objects.get(diagnosisindex).getString("suspectedDisease") != null) {
                                combinedobject.put("suspectedDisease", objects.get(diagnosisindex).getString("suspectedDisease"));
                                //combinedobject.put("diagnosisObjId",objects.get(diagnosisindex).getString("diagnosisId"));


                                //combinedobject.put("diagnosisObjId",objects.get(diagnosisindex).getString("diagnosisId"));
                                ArrayList<String> diseasenamecheck = new ArrayList<String>();

                                String[] value1 = (objects.get(diagnosisindex).getString("suspectedDisease").split(","));
                                for (int value = 0; value < value1.length; value++) {
                                    diseasenamecheck.add(value1[value]);
                                }


                                if (_suspecteddiseasenamesObj.size() != 0 && _suspecteddiseasenamesObj != null && objects.get(diagnosisindex).getString("suspectedDisease") != null) {
                                    //compairing_code_taken_disease_fromparse(objects.get(diagnosisindex).getString("suspectedDisease"));

                                    String[] value = objects.get(diagnosisindex).getString("suspectedDisease").split(",");

                                    System.out.println("value size" + value.length);
                                    if (value != null)
                                        for (int k = 0; k < value.length; k++) {

                                            System.out.println("code valuessss from parse" + value[k]);
                                        }
                                    String[] myaccrArray = null;
                                    if (value != null) {
                                        System.out.println("code valuessss from parse1");
                                        myaccrArray = new String[value.length];
                                        for (int j = 0; j < value.length; j++) {
                                            for (int k = 0; k < diseasenamearraylist.size(); k++) {
                                                if ((diseasenamearraylist.get(k).getIcdnumber()) != null) {
                                                    String icd_from_list = diseasenamearraylist.get(k).getIcdnumber().toString().trim();
                                                    String icd_from_selected = value[j].toString().trim();


                                                    if (icd_from_selected.equalsIgnoreCase("other")) {
                                                        myaccrArray[j] = "Other";
                                                    } else if (icd_from_selected.equalsIgnoreCase("none")) {
                                                        myaccrArray[j] = "None";
                                                    } else if (icd_from_selected.equalsIgnoreCase("-")) {
                                                        myaccrArray[j] = "-";
                                                    }


                                                    if (!icd_from_selected.equalsIgnoreCase("other") && !icd_from_selected.equalsIgnoreCase("none") && !icd_from_selected.equalsIgnoreCase("-")) {
                                                        if (icd_from_list.equalsIgnoreCase(icd_from_selected)) {
                                                            myaccrArray[j] = diseasenamearraylist.get(k).getDiseasename();
                                                        }
                                                    }
                                                }
                                            }
                                            String disease = "";
                                            for (int i = 0; i < myaccrArray.length; i++) {
                                                disease += myaccrArray[i] + "\n";
                                            }

                                            //ettext.setText(disease);
                                            combinedobject.put("suspectedDisease", disease);
                                        }
                                    }


                                } else {

                                    final ProgressDialog mProgress = new ProgressDialog(getActivity());
                                    mProgress.setMessage("Fetching Details.....");
                                    mProgress.show();
                                    ParseQuery<ParseObject> query = ParseQuery.getQuery("DiseaseDatabase");
                                    query.fromLocalDatastore();
                                    query.whereContainedIn("ICDNumber", diseasenamecheck);
                                    query.findInBackground(new FindCallback<ParseObject>() {

                                        @Override
                                        public void done(List<ParseObject> object, ParseException e) {
                                            // TODO Auto-generated method stub
                                            mProgress.dismiss();
                                            if (e == null) {
                                                if (object.size() > 0) {
                                                    String disease = "";
                                                    for (int diseasesize = 0; diseasesize < object.size(); diseasesize++) {
                                                        System.out.println("names disease" + object.get(diseasesize).getString("DiseaseName"));
                                                        disease += object.get(diseasesize).getString("DiseaseName") + "\n";
                                                    }


                                                    combinedobject.put("suspectedDisease", disease);
                                                }


                                            }

                                        }
                                    });
                                }


                            }

                            if (objects.get(diagnosisindex).getString("symptoms") != null) {
                                combinedobject.put("symptoms", objects.get(diagnosisindex).getString("symptoms"));
                            }
                            if (objects.get(diagnosisindex).getString("syndromes") != null) {
                                combinedobject.put("syndromes", objects.get(diagnosisindex).getString("syndromes"));
                            }

                            if (objects.get(diagnosisindex).getString("weight") != null) {
                                combinedobject.put("weight", objects.get(diagnosisindex).getString("weight"));
                            }
                            if (objects.get(diagnosisindex).getString("height") != null) {
                                combinedobject.put("height", objects.get(diagnosisindex).getString("height"));
                            }
                            if (objects.get(diagnosisindex).getString("bloodgroup") != null) {
                                combinedobject.put("bloodgroup", objects.get(diagnosisindex).getString("bloodgroup"));
                            }
                            if (objects.get(diagnosisindex).getString("bloodpressure") != null) {
                                combinedobject.put("bloodpressure", objects.get(diagnosisindex).getString("bloodpressure"));
                            }
                            if (objects.get(diagnosisindex).getString("spo2") != null) {
                                combinedobject.put("spo2", objects.get(diagnosisindex).getString("spo2"));
                            }
                            if (objects.get(diagnosisindex).getString("pulse") != null) {
                                combinedobject.put("pulse", objects.get(diagnosisindex).getString("pulse"));
                            }
                            if (objects.get(diagnosisindex).getString("respiratoryrate") != null) {
                                combinedobject.put("respiratoryrate", objects.get(diagnosisindex).getString("respiratoryrate"));
                            }
                            if (objects.get(diagnosisindex).getString("temperature") != null) {
                                combinedobject.put("temperature", objects.get(diagnosisindex).getString("temperature"));
                            }


                            if (objects.get(diagnosisindex).getString("externalgenetalia") != null) {
                                combinedobject.put("externalgenetalia", objects.get(diagnosisindex).getString("externalgenetalia"));
                            }
                            if (objects.get(diagnosisindex).getString("secondarysex_welldeveloped") != null) {
                                combinedobject.put("secondarysex_welldeveloped", objects.get(diagnosisindex).getString("secondarysex_welldeveloped"));
                            }
                            if (objects.get(diagnosisindex).getString("secondarysex_hair") != null) {
                                combinedobject.put("secondarysex_hair", objects.get(diagnosisindex).getString("secondarysex_hair"));
                            }
                            if (objects.get(diagnosisindex).getString("secondarysex_acne") != null) {
                                combinedobject.put("secondarysex_acne", objects.get(diagnosisindex).getString("secondarysex_acne"));
                            }
                            if (objects.get(diagnosisindex).getString("secondarysex_other") != null) {
                                combinedobject.put("secondarysex_other", objects.get(diagnosisindex).getString("secondarysex_other"));
                            }
                            if (objects.get(diagnosisindex).getString("breast_lump") != null) {
                                combinedobject.put("breast_lump", objects.get(diagnosisindex).getString("breast_lump"));
                            }
                            if (objects.get(diagnosisindex).getString("breast_galactorrhea") != null) {
                                combinedobject.put("breast_galactorrhea", objects.get(diagnosisindex).getString("breast_galactorrhea"));
                            }
                            if (objects.get(diagnosisindex).getString("breast_other") != null) {
                                combinedobject.put("breast_other", objects.get(diagnosisindex).getString("breast_other"));
                            }
                            if (objects.get(diagnosisindex).getString("cervix_healthy") != null) {
                                combinedobject.put("cervix_healthy", objects.get(diagnosisindex).getString("cervix_healthy"));
                            }
                            if (objects.get(diagnosisindex).getString("cervix_bleeding") != null) {
                                combinedobject.put("cervix_bleeding", objects.get(diagnosisindex).getString("cervix_bleeding"));
                            }
                            if (objects.get(diagnosisindex).getString("cervix_lbc") != null) {
                                combinedobject.put("cervix_lbc", objects.get(diagnosisindex).getString("cervix_lbc"));
                            }
                            if (objects.get(diagnosisindex).getString("uterus_avaf") != null) {
                                combinedobject.put("uterus_avaf", objects.get(diagnosisindex).getString("uterus_avaf"));
                            }
                            if (objects.get(diagnosisindex).getString("uterus_rvrf") != null) {
                                combinedobject.put("uterus_rvrf", objects.get(diagnosisindex).getString("uterus_rvrf"));
                            }
                            if (objects.get(diagnosisindex).getString("uterus_others") != null) {
                                combinedobject.put("uterus_others", objects.get(diagnosisindex).getString("uterus_others"));
                            }


                        }

                        objectslist.add(combinedobject);


                        JSONArray Marriage_result = new JSONArray();
                        JSONArray Obstricus_result = new JSONArray();
                        JSONArray Lmp_result = new JSONArray();
                        JSONArray Menstrual_Reg_Irr = new JSONArray();
                        JSONArray menstural_cycle = new JSONArray();
                        JSONArray menstural_days = new JSONArray();
                        JSONArray lmp_flow = new JSONArray();
                        JSONArray lmp_dysmenorrhea = new JSONArray();
                        JSONArray prent_past_history = new JSONArray();
                        JSONArray family_history = new JSONArray();
                        JSONArray surgical_history = new JSONArray();
                        JSONArray drug_history = new JSONArray();
                        JSONArray majorIllness = new JSONArray();
                        JSONArray majorIllnessstaus = new JSONArray();
                        JSONArray majorIllnessfamily = new JSONArray();
                        JSONArray majorIllnessfamilystaus = new JSONArray();
                        JSONArray surgeries = new JSONArray();
                        JSONArray allergiesMedication = new JSONArray();
                        JSONArray allergiesReaction = new JSONArray();

                        JSONArray allergyhistory = new JSONArray();


                        JSONArray spo2_result = new JSONArray();
                        JSONArray pulse_result = new JSONArray();
                        JSONArray respRate_result = new JSONArray();
                        JSONArray height_result = new JSONArray();
                        JSONArray bloodGroup_result = new JSONArray();
                        JSONArray bloodPressure_result = new JSONArray();
                        JSONArray temperature = new JSONArray();


                        try {
                            if (patientpersonalInformation != null) {

                                if (patientpersonalInformation.getNumber("PatientAge") != null) {
                                    combinedobject.put("PatientAge", patientpersonalInformation.getNumber("PatientAge"));
                                }

                                if (patientpersonalInformation.getJSONArray("Allergy_History") != null) {
                                    allergyhistory = patientpersonalInformation.getJSONArray("Allergy_History");
                                }

                                if (patientpersonalInformation.getJSONArray("MarriageHistoryYear") != null) {
                                    Marriage_result = patientpersonalInformation.getJSONArray("MarriageHistoryYear");
                                }
                                if (patientpersonalInformation.getJSONArray("ObstricusHistory") != null) {
                                    Obstricus_result = patientpersonalInformation.getJSONArray("ObstricusHistory");
                                }
                                if (patientpersonalInformation.getJSONArray("Lmp_date") != null) {
                                    Lmp_result = patientpersonalInformation.getJSONArray("Lmp_date");
                                }
                                if (patientpersonalInformation.getJSONArray("Menstrual_Regular_Irregular") != null) {
                                    Menstrual_Reg_Irr = patientpersonalInformation.getJSONArray("Menstrual_Regular_Irregular");
                                }
                                if (patientpersonalInformation.getJSONArray("Menstrual_Cycle") != null) {
                                    menstural_cycle = patientpersonalInformation.getJSONArray("Menstrual_Cycle");
                                }
                                if (patientpersonalInformation.getJSONArray("Menstrual_Days") != null) {
                                    menstural_days = patientpersonalInformation.getJSONArray("Menstrual_Days");
                                }
                                if (patientpersonalInformation.getJSONArray("Lmp_flow") != null) {
                                    lmp_flow = patientpersonalInformation.getJSONArray("Lmp_flow");
                                }
                                if (patientpersonalInformation.getJSONArray("Lmp_dysmenorrhea") != null) {
                                    lmp_dysmenorrhea = patientpersonalInformation.getJSONArray("Lmp_dysmenorrhea");
                                }
                                if (patientpersonalInformation.getJSONArray("majorIllness") != null) {
                                    majorIllness = patientpersonalInformation.getJSONArray("majorIllness");
                                }
                                if (patientpersonalInformation.getJSONArray("majorIllnessStatus") != null) {
                                    majorIllnessstaus = patientpersonalInformation.getJSONArray("majorIllnessStatus");
                                }
                                if (patientpersonalInformation.getJSONArray("majorIllnessFamily") != null) {
                                    majorIllnessfamily = patientpersonalInformation.getJSONArray("majorIllnessFamily");
                                }
                                if (patientpersonalInformation.getJSONArray("majorIllnessFamilyStatus") != null) {
                                    majorIllnessfamilystaus = patientpersonalInformation.getJSONArray("majorIllnessFamilyStatus");
                                }
                                if (patientpersonalInformation.getJSONArray("Surgeries") != null) {
                                    surgeries = patientpersonalInformation.getJSONArray("Surgeries");
                                }
                                if (patientpersonalInformation.getJSONArray("allergiesMedication") != null) {
                                    allergiesMedication = patientpersonalInformation.getJSONArray("allergiesMedication");
                                }
                                if (patientpersonalInformation.getJSONArray("allergiesReaction") != null) {
                                    allergiesReaction = patientpersonalInformation.getJSONArray("allergiesReaction");
                                }


                                if (allergyhistory.length() > 0)
                                    combinedobject.put("AllergyHistory", allergyhistory.getString(allergyhistory.length() - 1));
                                if (Marriage_result.length() > 0)
                                    combinedobject.put("MarriageHistoryYear", Marriage_result.getString(Marriage_result.length() - 1));
                                if (Obstricus_result.length() > 0)
                                    combinedobject.put("ObstricusHistory", Obstricus_result.getString(Obstricus_result.length() - 1));
                                if (Lmp_result.length() > 0)
                                    combinedobject.put("Lmp_date", Lmp_result.getString(Lmp_result.length() - 1));
                                if (Menstrual_Reg_Irr.length() > 0)
                                    combinedobject.put("Menstrual_Regular_Irregular", Menstrual_Reg_Irr.getString(Menstrual_Reg_Irr.length() - 1));
                                if (menstural_cycle.length() > 0)
                                    combinedobject.put("Menstrual_Cycle", menstural_cycle.getString(menstural_cycle.length() - 1));
                                if (lmp_flow.length() > 0)
                                    combinedobject.put("Lmp_flow", lmp_flow.getString(lmp_flow.length() - 1));
                                if (menstural_days.length() > 0)
                                    combinedobject.put("Menstrual_Days", menstural_days.getString(menstural_days.length() - 1));
                                if (lmp_dysmenorrhea.length() > 0)
                                    combinedobject.put("Lmp_dysmenorrhea", lmp_dysmenorrhea.getString(lmp_dysmenorrhea.length() - 1));
                                if (majorIllness.length() > 0)
                                    combinedobject.put("majorIllness", majorIllness.getString(majorIllness.length() - 1));
                                if (majorIllnessstaus.length() > 0)
                                    combinedobject.put("majorIllnessStatus", majorIllnessstaus.getString(majorIllnessstaus.length() - 1));
                                if (majorIllnessfamily.length() > 0)
                                    combinedobject.put("majorIllnessFamily", majorIllnessfamily.getString(majorIllnessfamily.length() - 1));
                                if (majorIllnessfamilystaus.length() > 0)
                                    combinedobject.put("majorIllnessFamilyStatus", majorIllnessfamilystaus.getString(majorIllnessfamilystaus.length() - 1));
                                if (surgeries.length() > 0)
                                    combinedobject.put("Surgeries", surgeries.getString(surgeries.length() - 1));
                                if (allergiesMedication.length() > 0)
                                    combinedobject.put("allergiesMedication", allergiesMedication.getString(allergiesMedication.length() - 1));
                                if (allergiesReaction.length() > 0)
                                    combinedobject.put("allergiesReaction", allergiesReaction.getString(allergiesReaction.length() - 1));


                            }
                        } catch (JSONException e1) {
                            // TODO Auto-generated catch block
                            e1.printStackTrace();
                        }


                        String treat = et_Treatment.getText().toString();
                        if (treatment != null) {
                            combinedobject.put("treatment", treat);
                        }

                        String followup = tvFollowup.getText().toString();
                        if (followup != null) {
                            combinedobject.put("followup", followup);
                        }

                        if (doctername != null) {
                            combinedobject.put("doctername", doctername);
                        }

                        String patientname = fltmName.getText().toString();
                        if (patientname != null) {
                            combinedobject.put("patientname", patientname);
                        }

                        combinedobject.put("id", uniqID.getText().toString());

                        System.out.println("combined drug name" + combinedobject.getJSONArray("drug"));

                        filewritegynacologist(combinedobject);
                        excelfileCreation(combinedobject);
                        textfileCreation(combinedobject);

                        listed.clear();
                        mgProgressDialog.dismiss();
                    } else {
                        mgProgressDialog.dismiss();
                        Toast.makeText(getActivity(), " Please save your prescription  before printing.......", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    e.printStackTrace();
                    mgProgressDialog.dismiss();
                    Toast.makeText(getActivity(), "Invalid Patient", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void setFrequency(int position) {
        frequencyPosition = position;
        flag = 4;
        System.out.println("position tag" + position);

        int indexvalue = position;
        System.out.println("indexvalue" + indexvalue);
        if (quantitySelectedArray.containsKey(indexvalue)) {
            System.out.println("key contains");
            if (quantitySelectedArray.get(indexvalue) != null) {
                System.out.println("not null");
                selectedQuantity_fifth = quantitySelectedArray.get(indexvalue);
            }
        } else {
            System.out.println("no key contains");
            selectedQuantity_fifth = null;
        }

        Bundle bundle = new Bundle();
        bundle.putInt("row", position);
        bundle.putStringArray("selectedquantity", selectedQuantity_fifth);
        bundle.putString("name", "Quantity");
        bundle.putInt("tagvalue", position);
        bundle.putSerializable("quantiyname", quantitynamearraylist);
        bundle.putStringArray("items", quantity_array);
        //bundle.putSerializable("listener",Treatment_Create_Frag.this);
        spObj = new QuantitySelector_Popup();
        spObj.setSubmitListener(quantitySelectedListener);
        spObj.setArguments(bundle);
        spObj.show(getFragmentManager(), "tag");
    }

    public void removeRow(int position, ArrayList<String> drug) {

//        drug.remove(position);
//        arrDrug.clear();
//        arrDrug = new ArrayList<String>(drug);
        listCount.remove(position);
        adapterDrug.notifyDataSetChanged();
        AppUtil.justifyListViewHeightBasedOnChildren(lstDrug);

    }


    public void startDatepicker(int position) {
        datepickerposition = position;
        startDatePickerDialog4.show();
        Log.d("DatePickerPosition", datepickerposition + " fhg");
        System.out.println("date picker position" + position);
    }


}




