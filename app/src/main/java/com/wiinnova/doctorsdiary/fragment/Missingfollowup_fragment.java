package com.wiinnova.doctorsdiary.fragment;

import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.wiinnova.doctorsdiary.R;
import com.parse.FindCallback;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.wiinnova.doctorsdiary.supportclasses.SearchMissingFollowupdetailsAdapter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;

public class Missingfollowup_fragment extends Fragment {

	int flag;
	ImageView ivStartdate;
	ImageView ivEnddate;
	TextView tvStartdate;
	TextView tvEnddate;
	String current;
	Date currentdate;
	Calendar newDate;
	Calendar newDate1;
	ListView lvlistitem;
	private TextView datetextview;
	private TextView docternametextview;

	ProgressDialog mProgressdialog;
	private SimpleDateFormat dateFormatter,df;
	private SimpleDateFormat dateFormatter1;
	private DatePickerDialog startDatePickerDialog;
	private DatePickerDialog startDatePickerDialog1;


	ArrayList<Date> followupdatescontainedintable=new ArrayList<Date>();
	List<String> patientidsizecheck=new ArrayList<String>();
	Date followupdate;
	Date  createddate;
	String PatientId;
	int noOfTimesCalled = 0;
	List<ParseObject> results=new ArrayList<ParseObject>();

	List<String> followupdates = new ArrayList<String>();
	List<String> missingfollowuppatientid = new ArrayList<String>();
	ArrayList<Date> followup=new ArrayList<Date>();
	ArrayList<Date> lastvisit=new ArrayList<Date>();
	ArrayList<String> patientid=new ArrayList<String>();
	ArrayList<String> firstname=new ArrayList<String>();
	ArrayList<String> lastname=new ArrayList<String>();
	ArrayList<String> phonenumber=new ArrayList<String>();
	ArrayList<String> email=new ArrayList<String>();
	ArrayList<String> visitdate=new ArrayList<String>();
	ArrayList<String> lastvisitdate=new ArrayList<String>();
	SearchMissingFollowupdetailsAdapter adapter;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View v=inflater.inflate(R.layout.missing_followup_layout, null);

		docternametextview=(TextView)v.findViewById(R.id.tvdrname);
		datetextview=(TextView)v.findViewById(R.id.tvdate);
		lvlistitem=(ListView)v.findViewById(R.id.search_old_record_result_item_list);
		ivStartdate=(ImageView)v.findViewById(R.id.ivstartdate);
		ivEnddate=(ImageView)v.findViewById(R.id.ivenddate);
		tvStartdate=(TextView)v.findViewById(R.id.startDate);
		tvEnddate=(TextView)v.findViewById(R.id.endDate);
		dateFormatter = new SimpleDateFormat("dd-MM-yyyy");
		dateFormatter1 = new SimpleDateFormat("dd-MM-yyyy");


		SharedPreferences docter_name=getActivity().getSharedPreferences("Login", 0);
		String docter_first_name=docter_name.getString("fName", null);  
		String docter_last_name=docter_name.getString("lName", null);  

		docternametextview.setText(docter_first_name+" "+docter_last_name);



		Calendar c = Calendar.getInstance();
		System.out.println("Current time => " + c.getTime());
		df = new SimpleDateFormat("dd-MMM-yyyy");
		current = df.format(c.getTime());
		datetextview.setText(current);
		/*try {
			currentdate=(Date)dateFormatter1.parse(current);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/


		View headerView = ((LayoutInflater)getActivity().getSystemService(getActivity().LAYOUT_INFLATER_SERVICE)).inflate(R.layout.missingfollowupdetails_header, null, false);
		lvlistitem.addHeaderView(headerView);

		
		tvStartdate.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				startDatePickerDialog.show();
			}
		});
		
		tvEnddate.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				startDatePickerDialog1.show();
			}
		});
		
		ivStartdate.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub

				startDatePickerDialog.show();

			}
		});

		ivEnddate.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				startDatePickerDialog1.show();
			}


		});


		Calendar newCalendar = Calendar.getInstance();
		startDatePickerDialog = new DatePickerDialog(getActivity(), new OnDateSetListener() {
			public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
				newDate = Calendar.getInstance();
				newDate.set(year, monthOfYear, dayOfMonth);				
				tvStartdate.setText(dateFormatter.format(newDate.getTime()));
			}
		},newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
		startDatePickerDialog.getDatePicker().setMaxDate(Calendar.getInstance().getTimeInMillis());

		startDatePickerDialog1 = new DatePickerDialog(getActivity(), new OnDateSetListener() {
			public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
				newDate1 = Calendar.getInstance();
				newDate1.set(year, monthOfYear, dayOfMonth);				
				tvEnddate.setText(dateFormatter.format(newDate1.getTime()));
				/*if(noOfTimesCalled%2==0){*/

				if(view.isShown())
					finddatebwStartEnddate1(tvStartdate.getText().toString(),tvEnddate.getText().toString());

				/*}
				noOfTimesCalled++;*/
			}
		},newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
		startDatePickerDialog1.getDatePicker().setMaxDate(Calendar.getInstance().getTimeInMillis());


		return v;
	}


	public void finddatebwStartEnddate1(String str_date,String end_date){

		missingfollowuppatientid.clear();
		flag=0;

		mProgressdialog=new ProgressDialog(getActivity());
		mProgressdialog.setMessage("Please wait...");
		mProgressdialog.show();

		ParseQuery<ParseObject> query= ParseQuery.getQuery("Diagnosis");
		query.fromLocalDatastore();

		query.whereGreaterThanOrEqualTo("followup", str_date);
		query.whereLessThanOrEqualTo("followup", end_date);
		query.whereEqualTo("patientattend",false);

		query.findInBackground(new FindCallback<ParseObject>() {

			@Override
			public void done(List<ParseObject> object, com.parse.ParseException e) {
				// TODO Auto-generated method stub

				if(e==null){
					if(object.size()>0){
						for(int i=0;i<object.size();i++){

							followupdates.add(object.get(i).getString("followup"));
							missingfollowuppatientid.add(object.get(i).getString("patientID"));


							System.out.println("missingfollowuppatientid"+object.get(i).getString("patientID"));
						}

						for(int i=0;i<missingfollowuppatientid.size();i++){
							fetechmissingpatientlastvisitdate(missingfollowuppatientid.get(i));
						}


					}else{

						missingfollowuppatientid.clear();
						lastvisitdate.clear();
						firstname.clear();
						lastname.clear();
						phonenumber.clear();
						email.clear();
						followupdates.clear(); 
						lastvisitdate.clear();
						adapter=new SearchMissingFollowupdetailsAdapter(getActivity(), missingfollowuppatientid, firstname, lastname,followupdates, lastvisitdate, phonenumber, email);
						lvlistitem.setAdapter(adapter);
						Toast.makeText(getActivity(), "No Missing Follow-up between these dates", Toast.LENGTH_LONG).show();
						mProgressdialog.dismiss();
					}
				}
			}
		});
	}

	private void fetechmissingpatientlastvisitdate(String patientid) {
		// TODO Auto-generated method stub


		ParseQuery<ParseObject> query1= ParseQuery.getQuery("Diagnosis");
		query1.fromLocalDatastore();
		query1.whereEqualTo("patientID", patientid);
		query1.findInBackground(new FindCallback<ParseObject>() {

			@Override
			public void done(List<ParseObject> objects, com.parse.ParseException e) {
				// TODO Auto-generated method stub
				if(e==null){
					if(objects.size()>0){
						for(int j=0;j<objects.size();j++){
							visitdate.add(objects.get(j).getString("createdDate"));
						}

						System.out.println("visitdate"+Collections.max(visitdate));
						lastvisitdate.add(Collections.max(visitdate));
						visitdate.clear();
						
						for(int k=0;k<missingfollowuppatientid.size();k++){
							fetechmissingfollowuppatientdetailsnew(missingfollowuppatientid.get(k));
						}


					}else{
						mProgressdialog.dismiss();
						System.out.println("no element");
					}

				}else{
					e.printStackTrace();
				}
			}


		});



	}


	private void fetechmissingfollowuppatientdetailsnew(
			String string) {
		// TODO Auto-generated method stub

		ParseQuery<ParseObject> query= ParseQuery.getQuery("Patients");
		query.fromLocalDatastore();
		query.whereEqualTo("patientID", string);
		query.findInBackground(new FindCallback<ParseObject>() {

			@Override
			public void done(List<ParseObject> objects, com.parse.ParseException e) {
				// TODO Auto-generated method stub

				mProgressdialog.dismiss();
				if(e==null){
					System.out.println("working1....");
					if(objects.size()>0){
						System.out.println("objects size"+objects.size());
						System.out.println("working....");
						for(int k=0;k<objects.size();k++){

							firstname.add(objects.get(k).getString("firstName"));
							lastname.add(objects.get(k).getString("lastName"));
							phonenumber.add(objects.get(k).getString("phoneNumber"));
							email.add(objects.get(k).getString("email"));
						}

						for(int i=0;i<firstname.size();i++){
							System.out.println("firstname size"+firstname.get(i));
						}

						flag++;
						System.out.println("flag value"+flag);
						System.out.println("missingfollowuppatientid value"+missingfollowuppatientid.size());
						if(flag==missingfollowuppatientid.size()){

							adapter=new SearchMissingFollowupdetailsAdapter(getActivity(), missingfollowuppatientid, firstname, lastname,followupdates, lastvisitdate, phonenumber, email);
							lvlistitem.setAdapter(adapter);
						}




						/*patientid.clear();
						firstname.clear();
						lastname.clear();
						lastvisitdate.clear();
						phonenumber.clear();
						email.clear();*/


					}else{
						mProgressdialog.dismiss();
					}
				}
			}
		});


	}


	private void fetechmissingfollowuppatientdetails(List<String> missingpatientid) {
		// TODO Auto-generated method stub


		ParseQuery<ParseObject> query= ParseQuery.getQuery("Patients");
		query.fromLocalDatastore();
		query.whereContainedIn("patientID", missingpatientid);
		query.findInBackground(new FindCallback<ParseObject>() {



			@Override
			public void done(List<ParseObject> objects, com.parse.ParseException e) {
				// TODO Auto-generated method stub

				mProgressdialog.dismiss();
				if(e==null){
					System.out.println("working1....");
					if(objects.size()>0){
						System.out.println("objects size"+objects.size());
						System.out.println("working....");
						for(int k=0;k<objects.size();k++){

							firstname.add(objects.get(k).getString("firstName"));
							lastname.add(objects.get(k).getString("lastName"));
							phonenumber.add(objects.get(k).getString("phoneNumber"));
							email.add(objects.get(k).getString("email"));
						}

						for(int i=0;i<firstname.size();i++){
							System.out.println("firstname size"+firstname.get(i));
						}



						adapter=new SearchMissingFollowupdetailsAdapter(getActivity(), missingfollowuppatientid, firstname, lastname,followupdates, lastvisitdate, phonenumber, email);
						lvlistitem.setAdapter(adapter);


						/*patientid.clear();
						firstname.clear();
						lastname.clear();
						lastvisitdate.clear();
						phonenumber.clear();
						email.clear();*/


					}else{
						mProgressdialog.dismiss();
					}
				}


			}
		});


	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		CrashReporter.getInstance().trackScreenView("Missing Followup");
	}



}
