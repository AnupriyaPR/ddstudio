package com.wiinnova.doctorsdiary.fragment;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.wiinnova.doctorsdiary.R;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.SaveCallback;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

public class Password_Change extends Fragment implements OnClickListener{

	EditText etoldpasswd;
	EditText etnewpasswd;
	EditText etrenewpasswd;
	TextView flName,currDate;
	TextView doctername;
	Button btnsubmit;

	ParseObject usermaster;
	ProgressDialog mProgressDialog;

	String docter_username;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view=inflater.inflate(R.layout.fragment_passwordchange, container,false);
		
		Initialize_Components(view);
		setnamedate();
		Setup_Listeners();
		
		return view;
	}

	private void setnamedate() {
		// TODO Auto-generated method stub
		SharedPreferences docter_name=getActivity().getSharedPreferences("Login", 0);
		String docter_first_name=docter_name.getString("fName", null);  
		String docter_last_name=docter_name.getString("lName", null); 
		 docter_username=docter_name.getString("Unm", null); 
		
		doctername.setText(docter_first_name+" "+docter_last_name);
		

		Calendar c = Calendar.getInstance();
		System.out.println("Current time => " + c.getTime());
		SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
		String formattedDate = df.format(c.getTime());
		currDate.setText(formattedDate);
		
		
		            
		
	}

	private void Setup_Listeners() {
		// TODO Auto-generated method stub
		btnsubmit.setOnClickListener(this);
	}

	private void Initialize_Components(View view) {
		// TODO Auto-generated method stub
		etoldpasswd=(EditText)view.findViewById(R.id.etUsername);
		etnewpasswd=(EditText)view.findViewById(R.id.etOldpasswd);
		etrenewpasswd=(EditText)view.findViewById(R.id.etNewpasswd);

		btnsubmit=(Button)view.findViewById(R.id.btnSubmit);
		doctername=(TextView)view.findViewById(R.id.tvdoctername);
		currDate=(TextView)view.findViewById(R.id.tvDate);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId()){
		case R.id.btnSubmit:
			passwordchange();
			break;

		}

	}

	private void passwordchange() {
		// TODO Auto-generated method stub
		boolean flag=true;


		if(!(etnewpasswd.getText().toString().equals(etrenewpasswd.getText().toString()))){
			Toast.makeText(getActivity(), "Password mismatch", Toast.LENGTH_LONG).show();
			flag=false;
			
		}
		if(TextUtils.isEmpty(etrenewpasswd.getText().toString())){
			etrenewpasswd.setError("Please re-enter newpassword");
			etrenewpasswd.requestFocus();
			flag=false;
		}
		if(TextUtils.isEmpty(etnewpasswd.getText().toString())){
			etnewpasswd.setError("Please enter new password");
			etnewpasswd.requestFocus();
			flag=false;
		}
		if(TextUtils.isEmpty(etoldpasswd.getText().toString())){
			etoldpasswd.setError("Please enter old password");
			etoldpasswd.requestFocus();
			flag=false;
		}
	
		
		
		if(flag==true){

			getoldpassword();
			

		}
	}

	private void updatenewpassword() {
		// TODO Auto-generated method stub
		mProgressDialog = new ProgressDialog(getActivity());

		mProgressDialog.setMessage("Saving data....");
		mProgressDialog.show();
		ParseQuery<ParseObject> query = ParseQuery.getQuery("UserMaster");
		query.whereEqualTo("docRegistrationNumber",docter_username);
		query.findInBackground(new FindCallback<ParseObject>() {
			
			@Override
			public void done(List<ParseObject> newpassword, ParseException e) {
				// TODO Auto-generated method stub
				
				if(e==null){
					newpassword.get(0).put("password",etrenewpasswd.getText().toString());
					newpassword.get(0).saveInBackground(new SaveCallback() {
						
						@Override
						public void done(ParseException e) {
							// TODO Auto-generated method stub
							mProgressDialog.dismiss();
							if (e == null) {
								myObjectSavedSuccessfully();
								// Id= Student.getObjectId();
								//i=1;

							} else {
								myObjectSaveDidNotSucceed();
						}
							
						}

						private void myObjectSaveDidNotSucceed() {
							// TODO Auto-generated method stub
							Toast msg1=Toast.makeText(getActivity(), "Failed", Toast.LENGTH_LONG);
							msg1.show();

						}

						private void myObjectSavedSuccessfully() {
							// TODO Auto-generated method stub
							Toast msg=Toast.makeText(getActivity(), " Successfully Completed", Toast.LENGTH_LONG);
							msg.show();
							fieldsclear();
						}

						private void fieldsclear() {
							// TODO Auto-generated method stub
							etrenewpasswd.setText("");
							etoldpasswd.setText("");
							etnewpasswd.setText("");
						}
					});
				}
				
				
			}
		});
	}

	private void getoldpassword() {
		// TODO Auto-generated method stub
		mProgressDialog = new ProgressDialog(getActivity());

		mProgressDialog.setMessage("changing data....");
		mProgressDialog.show();

		ParseQuery<ParseObject> query = ParseQuery.getQuery("UserMaster");
		query.whereEqualTo("docRegistrationNumber",docter_username);

		query.findInBackground(new FindCallback<ParseObject>() {

			@Override
			public void done(List<ParseObject> password, ParseException e) {
				// TODO Auto-generated method stub
				mProgressDialog.dismiss();
				if(e==null)
				{
					if(password.size()>0){
						usermaster=password.get(0);
						
						if(etoldpasswd.getText().toString().equals(usermaster.get("password"))){
							System.out.println("passowrd"+usermaster.get("password"));
							updatenewpassword();
						}else{
							Toast msg=Toast.makeText(getActivity(), "Wrong Password", Toast.LENGTH_LONG);
							msg.show();
						}
					}else{
						Toast msg=Toast.makeText(getActivity(), "Invalid User", Toast.LENGTH_LONG);
						msg.show();
					}
				}


			}
		});

	}



}
