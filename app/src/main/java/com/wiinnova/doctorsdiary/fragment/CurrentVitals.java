package com.wiinnova.doctorsdiary.fragment;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.wiinnova.doctorsdiary.R;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.SaveCallback;
import com.wiinnova.doctorsdiary.activities.FragmentActivityContainer;
import com.wiinnova.doctorsdiary.popupwindow.SpinnerPopup;
import com.wiinnova.doctorsdiary.popupwindow.SpinnerPopup.onSubmitListener;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import com.wiinnova.doctorsdiary.fragment.Patient_Frag.oncurrentvitalssubmit;

public class CurrentVitals extends Fragment implements oncurrentvitalssubmit, Serializable{



	public interface oncurrentvitalsbutton{
		void onCurrentbutton(int arg);
	}
	boolean patientedit_checkflag=true;
	boolean patientflag=true;

	boolean flag=false;

	JSONArray weight_result;
	JSONArray spo2_result;
	JSONArray pulse_result;
	JSONArray respRate_result;
	JSONArray height_result;
	JSONArray bloodGroup_result;
	JSONArray bloodPressure_result;
	JSONArray temp_result;
	JSONArray update_date;
	JSONArray currentvitals_id;

	JSONArray weight_result_array;
	JSONArray spo2_result_array;
	JSONArray pulse_result_array;
	JSONArray respRate_result_array;
	JSONArray height_result_array;
	JSONArray bloodGroup_result_array;
	JSONArray bloodPressure_result_array;
	JSONArray temp_result_array;
	JSONArray update_date_array;
	JSONArray currentvitals_id_array;


	EditText etWeight;
	TextView etBloodGroup;
	EditText etBloodPressuresystolic;
	EditText etBloodPressurediastolic;
	EditText etSp;
	EditText etTemperature;
	EditText etHeight;
	EditText etPulse;
	EditText etRespiratory;

	Button btnSubmit;
	TextView tvName;
	TextView tvUniqueid;

	ImageView ivPowerd;
	ScrollView topScrollview;

	String weight,bloodgroup,bloodpressure,sp,temperature,height,pulse,respiratory,updatedate;

	String PatientId,pfirstName,pmiddleName,plastName,padhaarNo,psex,pmaritalSta,pyearBirth,pstrtName,pCitName,ppinCode,pStat,pCountry,pPhoneNum,pemailId,pHeight,pWeight,pBgroup,pBloodG;
	Boolean ok;
	String[] allmedicArray;

	static int i;
	String[] allreacArray;
	String[] majillArray;
	String[] majillStaArray;
	String[] surgArray;
	String[] surgArray_one;
	String[] surgArray_two;
	String[] majorIllnessFamily;
	String[] majorIllnessFamilyStatus;
	String[] socialHistory;
	String[] socialHistoryStatus;
	String[] drArray;
	String[] bloodgroup_array;
	Button pcurrentMinfo;
	EditText pweight,pheight,pbloodG;	
	//ArrayList<Patient_info_Store> arrayOfpatinfo;
	String patientId;
	ProgressDialog mProgressDialog;
	RelativeLayout rlPowerd;
	RelativeLayout toplayout;
	ParseObject patientobject;

	onNameListener mListener;
	SpinnerPopup spObj;
	Patient_Personal_Info_Frag patientpesonalinfofrag;
	Patient_Medical_History_First_Frag patientmedicalhistoryfrag;
	Patient_Current_Medical_Info_Frag patientcurrentmedinfpfrag;
	String currentvitalsid;
	int currentvitalscount;
	private onSubmitListener bloodGroupSpinnerListener;
	private Patient_Frag patientfragObj;
	private boolean admintab;
	private boolean 		admintabdefault;

	public interface onNameListener {  


		void onSubmit(String name);  
	} 



	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub



		View view=inflater.inflate(R.layout.current_vitals_fragment,container, false);
		Initialize_Components(view);

		Bundle bundle=getArguments();
		patientflag=bundle.getBoolean("patientflag");
		admintab=bundle.getBoolean("admintab");
		patientfragObj = new Patient_Frag();
		Bundle bundle1=new Bundle();
		bundle1.putInt("sidemenuitem", 4);
		bundle1.putBoolean("patientflag", false);
		patientfragObj.setArguments(bundle1);
		getFragmentManager().beginTransaction()
		.replace(R.id.fl_sidemenu_container, patientfragObj)
		.commit();

		if(patientfragObj!=null){
			((FragmentActivityContainer)getActivity()).setpatientfrag(patientfragObj);
		}

		Set_Hint();



		((FragmentActivityContainer)getActivity()).setcurrentvitals(CurrentVitals.this);
		admintab=((FragmentActivityContainer)getActivity()).getcheckadmintab();
		admintabdefault=((FragmentActivityContainer)getActivity()).getadmintabdefault();

		if(admintab==true){
			System.out.println("admin tab working.....");
			patientobject=((FragmentActivityContainer)getActivity()).getOldPatientobject();
		}else if(admintab==false && admintabdefault==false){
			patientobject=((FragmentActivityContainer)getActivity()).getPatientobject();
		}else if(admintab==false && admintabdefault==true){
			patientobject=((FragmentActivityContainer)getActivity()).getOldPatientobject();
		}
		
		((FragmentActivityContainer)getActivity()).getpatientfrag().menu_item=3;
		patientedit_checkflag=((FragmentActivityContainer)getActivity()).getpatientflagvalue();

		if(patientedit_checkflag==false){
			patientflag=false;
		}
		System.out.println(" patient flag medical history"+patientflag);

		bloodgroup_array=getResources().getStringArray(R.array.bloodgroup_array);


		bloodGroupSpinnerListener=new onSubmitListener() {

			@Override
			public void onSubmit(String arg, int position) {
				// TODO Auto-generated method stub

				etBloodGroup.setText(arg);
				spObj.dismiss();
			}
		};


		if(patientflag==true && patientobject!=null){

			patientId=patientobject.getString("patientID");
			tvUniqueid.setText(patientId);
			String fname=patientobject.getString("firstName");
			String lname=patientobject.getString("lastName");
			if(fname==null){
				fname="FirstName";
			}
			if(lname==null){
				lname="LastName";
			}
			tvName.setText(fname +" "+lname);
			tvUniqueid.setText(patientId);
		}

		etWeight.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				if(etWeight.length()==0 && etHeight.getText().length()==0 && etBloodGroup.getText().length()==0 && etBloodPressuresystolic.getText().length()==0&&etBloodPressurediastolic.getText().length()==0&& etSp.getText().length()==0&& etTemperature.getText().length()==0&&etHeight.getText().length()==0&&etPulse.getText().length()==0&&etRespiratory.getText().length()==0){
					submitButtonDeactivation();
				}else{
					submitButtonActivation();
				}
			}
		});


		etBloodGroup.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub

				Bundle bundle=new Bundle();
				bundle.putString("name", "Blood Group");
				bundle.putStringArray("items",bloodgroup_array);
				//bundle.putSerializable("listener",CurrentVitals.this);
				spObj=new SpinnerPopup();
				spObj.setSubmitListener(bloodGroupSpinnerListener);
				spObj.setArguments(bundle);
				spObj.show(getFragmentManager(), "tag");

			}
		});

		etBloodGroup.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				if(etWeight.length()==0 && etHeight.getText().length()==0 && etBloodGroup.getText().length()==0 && etBloodPressuresystolic.getText().length()==0&&etBloodPressurediastolic.getText().length()==0&& etSp.getText().length()==0&& etTemperature.getText().length()==0&&etHeight.getText().length()==0&&etPulse.getText().length()==0&&etRespiratory.getText().length()==0){
					submitButtonDeactivation();
				}else{
					submitButtonActivation();
				}
			}
		});

		etBloodPressurediastolic.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				System.out.println("start"+start+"before"+before+"count"+count);

				if(before==1){
					flag=true;
				}else{
					flag=false;
				}
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				if(!(s.toString().contains("/")) && flag==false){
					if(s.length()==3){
						s.append("/");
						System.out.println("text 3");
					}
				}

				if(etWeight.length()==0 && etHeight.getText().length()==0 && etBloodGroup.getText().length()==0 && etBloodPressuresystolic.getText().length()==0&&etBloodPressurediastolic.getText().length()==0&& etSp.getText().length()==0&& etTemperature.getText().length()==0&&etHeight.getText().length()==0&&etPulse.getText().length()==0&&etRespiratory.getText().length()==0){
					submitButtonDeactivation();
				}else{
					submitButtonActivation();
				}
			}
		});

		etBloodPressuresystolic.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				System.out.println("start"+start+"before"+before+"count"+count);

				if(before==1){
					flag=true;
				}else{
					flag=false;
				}
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				if(!(s.toString().contains("/")) && flag==false){
					if(s.length()==3){
						s.append("/");
						System.out.println("text 3");
					}
				}

				if(etWeight.length()==0 && etHeight.getText().length()==0 && etBloodGroup.getText().length()==0 && etBloodPressuresystolic.getText().length()==0&&etBloodPressurediastolic.getText().length()==0&& etSp.getText().length()==0&& etTemperature.getText().length()==0&&etHeight.getText().length()==0&&etPulse.getText().length()==0&&etRespiratory.getText().length()==0){
					submitButtonDeactivation();
				}else{
					submitButtonActivation();
				}
			}
		});

		etSp.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				if(etWeight.length()==0 && etHeight.getText().length()==0 && etBloodGroup.getText().length()==0 && etBloodPressuresystolic.getText().length()==0&& etSp.getText().length()==0&& etTemperature.getText().length()==0&&etHeight.getText().length()==0&&etPulse.getText().length()==0&&etRespiratory.getText().length()==0){
					submitButtonDeactivation();
				}else{
					submitButtonActivation();
				}
			}
		});

		etTemperature.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				if(etWeight.length()==0 && etHeight.getText().length()==0 && etBloodGroup.getText().length()==0 && etBloodPressuresystolic.getText().length()==0&& etSp.getText().length()==0&& etTemperature.getText().length()==0&&etHeight.getText().length()==0&&etPulse.getText().length()==0&&etRespiratory.getText().length()==0){
					submitButtonDeactivation();
				}else{
					submitButtonActivation();
				}
			}
		});

		etHeight.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				if(etWeight.length()==0 && etHeight.getText().length()==0 && etBloodGroup.getText().length()==0 && etBloodPressuresystolic.getText().length()==0&& etSp.getText().length()==0&& etTemperature.getText().length()==0&&etHeight.getText().length()==0&&etPulse.getText().length()==0&&etRespiratory.getText().length()==0){
					submitButtonDeactivation();
				}else{
					submitButtonActivation();
				}	
			}
		});

		etPulse.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				if(etWeight.length()==0 && etHeight.getText().length()==0 && etBloodGroup.getText().length()==0 && etBloodPressuresystolic.getText().length()==0&& etSp.getText().length()==0&& etTemperature.getText().length()==0&&etHeight.getText().length()==0&&etPulse.getText().length()==0&&etRespiratory.getText().length()==0){
					submitButtonDeactivation();
				}else{
					submitButtonActivation();
				}
			}
		});

		etRespiratory.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				if(etWeight.length()==0 && etHeight.getText().length()==0 && etBloodGroup.getText().length()==0 && etBloodPressuresystolic.getText().length()==0&& etSp.getText().length()==0&& etTemperature.getText().length()==0&&etHeight.getText().length()==0&&etPulse.getText().length()==0&&etRespiratory.getText().length()==0){
					submitButtonDeactivation();
				}else{
					submitButtonActivation();
				}
			}
		});
		SharedPreferences scanpidnew=getActivity().getSharedPreferences("NewPatID", 0);
		String scnewPid=scanpidnew.getString("NewID", null); 


		if(patientobject==null){
			if(scnewPid!=null){
				tvUniqueid.setText(scnewPid);
				SharedPreferences fnname1=getActivity().getSharedPreferences("fnName", 0);
				tvName.setText(fnname1.getString("fNname", "FirstName"+" "+"LastName"));  
			}
		}else{
			SharedPreferences fnname1=getActivity().getSharedPreferences("fnName", 0);
			tvName.setText(fnname1.getString("fNname", "FirstName"+" "+"LastName"));    
			tvUniqueid.setText(patientobject.getString("patientID"));
		}



		toplayout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				hideKeyboard(v);
			}
		});

		topScrollview.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				hideKeyboard(v);
			}
		});

		topScrollview.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent arg1) {
				// TODO Auto-generated method stub
				hideKeyboard(v);
				return false;
			}
		});



		return view;

	}


	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onViewCreated(view, savedInstanceState);
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

		CrashReporter.getInstance().trackScreenView("Patient Current Vitals");

		etBloodGroup.setEnabled(true);
		if(patientedit_checkflag==false){

			etWeight.setEnabled(false);
			etBloodGroup.setEnabled(false);
			etBloodPressuresystolic.setEnabled(false);
			etBloodPressurediastolic.setEnabled(false);
			etSp.setEnabled(false);
			etTemperature.setEnabled(false);
			etHeight.setEnabled(false);
			etPulse.setEnabled(false);
			etRespiratory.setEnabled(false);

		}

		if(patientobject !=null){

			String fname=patientobject.getString("firstName");
			String lname=patientobject.getString("lastName");
			if(fname==null){
				fname="FirstName";
			}
			if(lname==null){
				lname="LastName";
			}
			tvName.setText(fname +" "+lname);

			weight_result=patientobject.getJSONArray("weight");
			String[] weightAnsArray = null;
			if(weight_result!=null){
				String result="";
				try{
					result=weight_result.getString(weight_result.length()-1);
				}catch(Exception e){
					e.printStackTrace();
				}
				if(result.toString()!=null && !(result.toString().equals("Nil"))){
					etWeight.setText(result.toString());
				}
			}

			spo2_result=patientobject.getJSONArray("spo2");

			if(spo2_result!=null){
				String result="";
				try{
					result=spo2_result.getString(spo2_result.length()-1);
				}catch(Exception e){
					e.printStackTrace();
				}
				if(result.toString()!=null && !(result.toString().equals("Nil"))){
					etSp.setText(result.toString());
				}
			}


			pulse_result=patientobject.getJSONArray("pulse");

			if(pulse_result!=null){
				String result="";
				try{
					result=pulse_result.getString(pulse_result.length()-1);
				}catch(Exception e){
					e.printStackTrace();
				}
				if(result.toString()!=null && !(result.toString().equals("Nil"))){
					etPulse.setText(result.toString());
				}
			}

			respRate_result=patientobject.getJSONArray("respRate");

			if(respRate_result!=null){
				String result="";
				try{
					result=respRate_result.getString(respRate_result.length()-1);
				}catch(Exception e){
					e.printStackTrace();
				}
				if(result.toString()!=null && !(result.toString().equals("Nil"))){
					etRespiratory.setText(result.toString());
				}
			}


			height_result=patientobject.getJSONArray("height");

			if(height_result!=null){
				String result="";
				try{
					result=height_result.getString(height_result.length()-1);
				}catch(Exception e){
					e.printStackTrace();
				}
				if(result.toString()!=null && !(result.toString().equals("Nil"))){
					etHeight.setText(result.toString());
				}
			}


			bloodGroup_result=patientobject.getJSONArray("bloodGroup");

			if(bloodGroup_result!=null){
				String result="";
				try{
					result=bloodGroup_result.getString(bloodGroup_result.length()-1);
				}catch(Exception e){
					e.printStackTrace();
				}
				if(result.toString()!=null && !(result.toString().equals("Nil"))){
					etBloodGroup.setText(result.toString());
					etBloodGroup.setEnabled(false);
				}

			}

			bloodPressure_result=patientobject.getJSONArray("bloodPressure");
			String pressure[]=null;
			if(bloodPressure_result!=null){
				String result="";
				try{

					result=bloodPressure_result.getString(bloodPressure_result.length()-1);
					pressure=result.split("/");
				}catch(Exception e){
					e.printStackTrace();
				}
				if(!(result.equals("Nil"))){
					if(pressure[0]!=null){
						etBloodPressuresystolic.setText(pressure[0].toString());
					}
					if(pressure[1]!=null){
						etBloodPressurediastolic.setText(pressure[1].toString());
					}
				}
			}

			temp_result=patientobject.getJSONArray("temp");		
			if(temp_result!=null){
				String result="";
				try{
					result=temp_result.getString(temp_result.length()-1);
				}catch(Exception e){
					e.printStackTrace();
				}
				if(result.toString()!=null && !(result.toString().equals("Nil"))){
					etTemperature.setText(result.toString());
				}

			}
			update_date=patientobject.getJSONArray("testedDate");
			if(update_date!=null){
				String result="";
				try{
					result=update_date.getString(update_date.length()-1);
				}catch(Exception e){
					e.printStackTrace();
				}
			}
			currentvitals_id=patientobject.getJSONArray("currentvitalsId");
			((FragmentActivityContainer)getActivity()).getpatientfrag().onButtonactivation(0);
		}
	}


	protected void hideKeyboard(View view)
	{
		InputMethodManager in = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
		in.hideSoftInputFromWindow(view.getWindowToken(),
				InputMethodManager.HIDE_NOT_ALWAYS);

	}
	public String[] join(String [] ... parms) {
		// calculate size of target array
		int size = 0;
		for (String[] array : parms) {
			size += array.length;
		}

		String[] result = new String[size];

		int j = 0;
		for (String[] array : parms) {
			for (String s : array) {
				result[j++] = s;
			}
		}
		return result;
	}

	private void Set_Hint() {
		// TODO Auto-generated method stub
		etWeight.setHint(Html.fromHtml( "</font>" + "<small>" + "Weight" + "</small>" )); 
		etHeight.setHint(Html.fromHtml( "<small>" + "Height" + "</small>" )); 
		etBloodPressuresystolic.setHint(Html.fromHtml( "<small>" + "Systolic" + "</small>" ));
		etBloodPressurediastolic.setHint(Html.fromHtml( "<small>" + "Diastolic" + "</small>" ));
		etSp.setHint(Html.fromHtml( "<small>" + "SP O2" + "</small>" ));
		etTemperature.setHint(Html.fromHtml( "<small>" + "Temperature" + "</small>" ));
		etPulse.setHint(Html.fromHtml( "<small>" + "Pulse" + "</small>" ));
		etRespiratory.setHint(Html.fromHtml( "<small>" + "Respiratory Rate" + "</small>" ));
	}

	private void Initialize_Components(View view) {
		// TODO Auto-generated method stub
		topScrollview=(ScrollView)view.findViewById(R.id.ctml2);
		etWeight=(EditText)view.findViewById(R.id.etweight);
		etBloodGroup=(TextView)view.findViewById(R.id.etbloodgroup);
		etSp=(EditText)view.findViewById(R.id.etspo);
		etTemperature=(EditText)view.findViewById(R.id.ettemperature);
		etHeight=(EditText)view.findViewById(R.id.etheight);
		etPulse=(EditText)view.findViewById(R.id.etpulse);
		etRespiratory=(EditText)view.findViewById(R.id.etrespiratory);	
		etBloodPressuresystolic=(EditText)view.findViewById(R.id.etbloodpressure);
		etBloodPressurediastolic=(EditText)view.findViewById(R.id.etbloodpressure1);
		tvName=(TextView)view.findViewById(R.id.tvfirst_last_name);
		tvUniqueid=(TextView)view.findViewById(R.id.tvuniqueid);	
		toplayout=(RelativeLayout)view.findViewById(R.id.topContainer);
	}

	private void submitButtonActivation() {
		// TODO Auto-generated method stub
		((FragmentActivityContainer)getActivity()).getpatientfrag().onCurrentbutton(1);

	}
	private void submitButtonDeactivation() {
		// TODO Auto-generated method stub
		((FragmentActivityContainer)getActivity()).getpatientfrag().onCurrentbutton(0);
	}

	@Override
	public void onCurrent(int arg,int targetfragment) {
		// TODO Auto-generated method stub

		System.out.println("current submit workinggggggggg");
		boolean valid_flag=true;

		if(etWeight.getText().length()!=0){
			weight=etWeight.getText().toString().trim();
		}else{
			weight="Nil";
		}
		if(etHeight.getText().length()!=0){
			height=etHeight.getText().toString().trim();
		}else{
			height="Nil";
		}
		if(etBloodGroup.getText().length()!=0){
			bloodgroup=etBloodGroup.getText().toString().trim();
		}else{
			bloodgroup="Nil";
		}
		ok = bloodgroup.matches("^(A|B|AB|O)[+-]$");
		if(etBloodPressuresystolic.getText().length()!=0 && etBloodPressurediastolic.getText().length()!=0){
			bloodpressure=etBloodPressuresystolic.getText().toString().trim()+"/"+etBloodPressurediastolic.getText().toString().trim();
		}else{
			bloodpressure="Nil";
		}
		if(etPulse.getText().length()!=0){
			pulse=etPulse.getText().toString().trim();
		}else{
			pulse="Nil";
		}
		if(etRespiratory.getText().length()!=0){
			respiratory=etRespiratory.getText().toString();
		}else{
			respiratory="Nil";
		}
		if(etSp.getText().length()!=0){
			sp=etSp.getText().toString();
		}else{
			sp="Nil";
		}
		if(etTemperature.getText().length()!=0){
			temperature=etTemperature.getText().toString();
		}else{
			temperature="Nil";
		}

		Calendar c1 = Calendar.getInstance();
		SimpleDateFormat df1 = new SimpleDateFormat("dd-MMM-yyyy");
		updatedate = df1.format(c1.getTime());
		System.out.println("update date"+updatedate);




		System.out.println(etWeight.getText());
		System.out.println(etHeight.getText());
		System.out.println(etBloodGroup.getText());
		System.out.println(etPulse.getText());
		System.out.println(etRespiratory.getText());
		System.out.println(etSp.getText());
		System.out.println(etTemperature.getText());

		System.out.println(etBloodPressuresystolic.getText());



		save_patientcurrent_vitals(targetfragment);

	}

	private void save_patientcurrent_vitals( final int targetfragment) {
		// TODO Auto-generated method stub
		System.out.println("save targetfragment inside"+targetfragment);
		SharedPreferences scanpidnew=getActivity().getSharedPreferences("NewPatID", 0);
		String scnewPid=scanpidnew.getString("NewID", null); 

		SharedPreferences sp=getActivity().getSharedPreferences("Login", 0);
		String docterregId=sp.getString("docterRegnumber", null);



		SharedPreferences scanpid1=getActivity().getSharedPreferences("ScannedPid", 0);
		String scPid=scanpid1.getString("scanpid", null);
		if(patientobject==null){
			patientobject=new ParseObject("Patients");

			if(scPid!=null||scnewPid!=null){
				if(scPid!=null){
					Log.e("scanner pid111====>>", scPid);
					patientId=scPid;
				}
				if(scnewPid!=null){
					Log.e("scanner pid====>>", scnewPid);
					patientId=scnewPid;
				}
			}
			else{
				Toast.makeText(getActivity(), "Patient Id not Found", Toast.LENGTH_SHORT).show();
			}

		}else{
			patientId=patientobject.getString("patientID");
		}


		if(patientflag==false){


			weightdata();
			spo2data();
			pulsedata();
			respRatedata();
			heightdata();
			bloodGroupdata();
			bloodPressuredata();
			tempdata();
			updatedate();


		}else if(patientflag==true){
			if(((FragmentActivityContainer)getActivity()).getoldpatientcurrentvitalsupdateflag()==false){


				weightdata();
				spo2data();
				pulsedata();
				respRatedata();
				heightdata();
				bloodGroupdata();
				bloodPressuredata();
				tempdata();
				updatedate();

				((FragmentActivityContainer)getActivity()).setoldpatientcurrentvitalsupdateflag(true);
			}else{


				weightdata();
				spo2data();
				pulsedata();
				respRatedata();
				heightdata();
				bloodGroupdata();
				bloodPressuredata();
				tempdata();
				updatedate();
			}
		}

		mProgressDialog = new ProgressDialog(getActivity());

		mProgressDialog.setMessage("Storing Patient Data....");
		mProgressDialog.setCancelable(false);
		mProgressDialog.show();
		patientobject.put("patientID", patientId);
		if(weight_result!=null){
			patientobject.put("weight",weight_result);
		}else{
			patientobject.put("weight",weight_result_array);
		}
		if(height_result!=null){
			patientobject.put("height",height_result);
		}else{
			patientobject.put("height",height_result_array);
		}
		if(bloodGroup_result!=null){
			patientobject.put("bloodGroup",bloodGroup_result);
		}else{
			patientobject.put("bloodGroup",bloodGroup_result_array);
		}
		if(bloodPressure_result!=null){
			patientobject.put("bloodPressure",bloodPressure_result);
		}else{
			patientobject.put("bloodPressure",bloodPressure_result_array);
		}
		if(pulse_result!=null){
			patientobject.put("pulse",pulse_result);
		}else{
			patientobject.put("pulse",pulse_result_array);
		}
		if(respRate_result!=null){
			patientobject.put("respRate",respRate_result);
		}else{
			patientobject.put("respRate",respRate_result_array);
		}
		if(spo2_result!=null){
			patientobject.put("spo2",spo2_result);
		}else{
			patientobject.put("spo2",spo2_result_array);
		}
		if(temp_result!=null){
			patientobject.put("temp",temp_result);
		}else{
			patientobject.put("temp",temp_result_array);
		}
		if(update_date!=null){
			patientobject.put("testedDate",update_date);
		}else{
			patientobject.put("testedDate",update_date_array);
		}
		patientobject.put("docterRegistrationNumber",docterregId);

		patientobject.pinInBackground(new SaveCallback() {

			@Override
			public void done(ParseException e) {
				// TODO Auto-generated method stub
				mProgressDialog.dismiss();
				if (e == null) {
					System.out.println("not error");
					System.out.println("object id"+patientobject.getObjectId());
					((FragmentActivityContainer)getActivity()).setCurrentvitalsObjectid(patientId);

					myObjectSavedSuccessfully(targetfragment);

				} else {
					myObjectSaveDidNotSucceed();
				}
			}

			private void myObjectSaveDidNotSucceed() {
				// TODO Auto-generated method stub
				Toast msg1=Toast.makeText(getActivity(), "Failed", Toast.LENGTH_LONG);
				msg1.show();
			}

			private void myObjectSavedSuccessfully(int targetfragment) {
				// TODO Auto-generated method stub

				Toast msg=Toast.makeText(getActivity(), " Successfully Completed", Toast.LENGTH_LONG);
				msg.show();

				//	admintab=((FragmentActivityContainer)getActivity()).getcheckadmintab();

				FragmentManager fragmentManager = getFragmentManager();
				FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

				if(targetfragment==2){
					if(((FragmentActivityContainer)getActivity()).getmedicalhistory()==null){
						patientmedicalhistoryfrag=new Patient_Medical_History_First_Frag();
						Bundle bundle=new Bundle();
						bundle.putBoolean("patientflag", patientflag);
						patientmedicalhistoryfrag.setArguments(bundle);
					}else{
						patientmedicalhistoryfrag=((FragmentActivityContainer)getActivity()).getmedicalhistory();
					}

					((FragmentActivityContainer)getActivity()).setmedicalhistory(patientmedicalhistoryfrag);
					fragmentTransaction.replace(R.id.fl_fragment_container, patientmedicalhistoryfrag);
				}else if(targetfragment==3){
					patientcurrentmedinfpfrag = new Patient_Current_Medical_Info_Frag();
					fragmentTransaction.replace(R.id.fl_fragment_container, patientcurrentmedinfpfrag);
				}else if(targetfragment==1){
					System.out.println("workinggggggg698");
					if(((FragmentActivityContainer)getActivity()).getpersonalinfo()==null){
						patientpesonalinfofrag = new Patient_Personal_Info_Frag();
						Bundle bundle=new Bundle();
						bundle.putBoolean("patientflag", patientflag);
						patientpesonalinfofrag.setArguments(bundle);
					}else{
						patientpesonalinfofrag=((FragmentActivityContainer)getActivity()).getpersonalinfo();
					}

					fragmentTransaction.replace(R.id.fl_fragment_container, patientpesonalinfofrag);

				}
				if(admintab==true)
					fragmentTransaction.addToBackStack(null);
				fragmentTransaction.commit();

				((FragmentActivityContainer)getActivity()).setPatientobject(patientobject);
				FragmentActivityContainer.check_save=0;
			}
		});

		patientobject.saveEventually();
	}

	public void weightdata(){
		String result="";
		result=weight;
		if(weight_result!=null){
			if(patientflag==false){
				try {
					weight_result.put(weight_result.length()-1, result);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}else if(patientflag==true && ((FragmentActivityContainer)getActivity()).getoldpatientcurrentvitalsupdateflag()==false){
				weight_result.put(result);
			}else if(patientflag==true && ((FragmentActivityContainer)getActivity()).getoldpatientcurrentvitalsupdateflag()==true){
				try {
					weight_result.put(weight_result.length()-1, result);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		}else{
			weight_result_array=new JSONArray();
			weight_result_array.put(result);
		}
	}

	public void spo2data(){
		String result="";
		result=sp;
		if(spo2_result!=null){
			if(patientflag==false){
				try {
					spo2_result.put(spo2_result.length()-1, result);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}else if(patientflag==true && ((FragmentActivityContainer)getActivity()).getoldpatientcurrentvitalsupdateflag()==false){
				spo2_result.put(result);
			}else if(patientflag==true && ((FragmentActivityContainer)getActivity()).getoldpatientcurrentvitalsupdateflag()==true){
				try {
					spo2_result.put(spo2_result.length()-1, result);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		}else{
			spo2_result_array=new JSONArray();
			spo2_result_array.put(result);
		}
	}

	public void pulsedata(){
		String result="";
		result=pulse;
		if(pulse_result!=null){
			if(patientflag==false){
				try {
					pulse_result.put(pulse_result.length()-1, result);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}else if(patientflag==true && ((FragmentActivityContainer)getActivity()).getoldpatientcurrentvitalsupdateflag()==false){
				pulse_result.put(result);
			}else if(patientflag==true && ((FragmentActivityContainer)getActivity()).getoldpatientcurrentvitalsupdateflag()==true){
				try {
					pulse_result.put(pulse_result.length()-1, result);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		}else{
			pulse_result_array=new JSONArray();
			pulse_result_array.put(result);
		}
	}


	public void respRatedata(){
		String result="";
		result=respiratory;
		if(respRate_result!=null){
			if(patientflag==false){
				try {
					respRate_result.put(respRate_result.length()-1, result);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}else if(patientflag==true && ((FragmentActivityContainer)getActivity()).getoldpatientcurrentvitalsupdateflag()==false){
				respRate_result.put(result);
			}else if(patientflag==true && ((FragmentActivityContainer)getActivity()).getoldpatientcurrentvitalsupdateflag()==true){
				try {
					respRate_result.put(respRate_result.length()-1, result);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		}else{
			respRate_result_array=new JSONArray();
			respRate_result_array.put(result);
		}
	}


	public void heightdata(){
		String result="";
		result=height;
		if(height_result!=null){
			if(patientflag==false){
				try {
					height_result.put(height_result.length()-1, result);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}else if(patientflag==true && ((FragmentActivityContainer)getActivity()).getoldpatientcurrentvitalsupdateflag()==false){
				height_result.put(result);
			}else if(patientflag==true && ((FragmentActivityContainer)getActivity()).getoldpatientcurrentvitalsupdateflag()==true){
				try {
					height_result.put(height_result.length()-1, result);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		}else{
			height_result_array=new JSONArray();
			height_result_array.put(result);
		}
	}

	public void bloodGroupdata(){
		String result="";
		result=bloodgroup;
		if(bloodGroup_result!=null){
			if(patientflag==false){
				try {
					bloodGroup_result.put(bloodGroup_result.length()-1, result);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}else if(patientflag==true && ((FragmentActivityContainer)getActivity()).getoldpatientcurrentvitalsupdateflag()==false){
				bloodGroup_result.put(result);
			}else if(patientflag==true && ((FragmentActivityContainer)getActivity()).getoldpatientcurrentvitalsupdateflag()==true){
				try {
					bloodGroup_result.put(bloodGroup_result.length()-1, result);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		}else{
			bloodGroup_result_array=new JSONArray();
			bloodGroup_result_array.put(result);
		}
	}

	public void bloodPressuredata(){
		String result="";
		result=bloodpressure;
		if(bloodPressure_result!=null){
			if(patientflag==false){
				try {
					bloodPressure_result.put(bloodPressure_result.length()-1, result);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}else if(patientflag==true && ((FragmentActivityContainer)getActivity()).getoldpatientcurrentvitalsupdateflag()==false){
				bloodPressure_result.put(result);
			}else if(patientflag==true && ((FragmentActivityContainer)getActivity()).getoldpatientcurrentvitalsupdateflag()==true){
				try {
					bloodPressure_result.put(bloodPressure_result.length()-1, result);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		}else{
			bloodPressure_result_array=new JSONArray();
			bloodPressure_result_array.put(result);
		}
	}

	public void tempdata(){
		String result="";
		result=temperature;
		if(temp_result!=null){
			if(patientflag==false){
				try {
					temp_result.put(temp_result.length()-1, result);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}else if(patientflag==true && ((FragmentActivityContainer)getActivity()).getoldpatientcurrentvitalsupdateflag()==false){
				temp_result.put(result);
			}else if(patientflag==true && ((FragmentActivityContainer)getActivity()).getoldpatientcurrentvitalsupdateflag()==true){
				try {
					temp_result.put(temp_result.length()-1, result);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		}else{
			temp_result_array=new JSONArray();
			temp_result_array.put(result);
		}
	}

	public void updatedate(){

		String result="";
		result=updatedate;
		if(update_date!=null){
			if(patientflag==false){
				try {
					update_date.put(update_date.length()-1, result);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}else if(patientflag==true && ((FragmentActivityContainer)getActivity()).getoldpatientcurrentvitalsupdateflag()==false){
				update_date.put(result);
			}else if(patientflag==true && ((FragmentActivityContainer)getActivity()).getoldpatientcurrentvitalsupdateflag()==true){
				try {
					update_date.put(update_date.length()-1, result);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		}else{
			update_date_array=new JSONArray();
			update_date_array.put(result);
		}
	}





}
