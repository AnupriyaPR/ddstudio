package com.wiinnova.doctorsdiary.fragment;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.wiinnova.doctorsdiary.R;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.wiinnova.doctorsdiary.activities.FragmentActivityContainer;
import com.wiinnova.doctorsdiary.popupwindow.SearchPatientNamePopup;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class AdminPatients extends Fragment  implements OnClickListener,Serializable{

	TextView date;
	TextView drname;
	EditText et_editname;
	EditText etPatientName;
	RelativeLayout rlSearch;
	RelativeLayout rlEdit;
	LinearLayout container;
	ImageView iv_missimgfollowup;
	ImageView iv_appointmentdetails;
	ArrayList<String> patientname=new ArrayList<String>();
	ArrayList<ParseObject> objectid=new ArrayList<ParseObject>();

	SearchPatientNamePopup searchname;
	ProgressDialog mProgressDialog;

	List<ParseObject> patientobject;
	SearchPatientresult searchpatientObj;
	private DD_Frag ddfirst;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view=inflater.inflate(R.layout.admin_patients_fragment, container,false);


		ddfirst = new  DD_Frag();
		Bundle bundle=new Bundle();
		bundle.putInt("sidemenuitem", 3);
		ddfirst.setArguments(bundle);

		getFragmentManager().beginTransaction()
		.replace(R.id.fl_sidemenu_container, ddfirst)
		.commit();


		System.out.println("admin patients working.....");

		Initialize_Componenets(view);
		Setup_Listeners();



		SharedPreferences docter_name=getActivity().getSharedPreferences("Login", 0);
		String docter_first_name=docter_name.getString("fName", null);  
		String docter_last_name=docter_name.getString("lName", null);  

		drname.setText(docter_first_name+" "+docter_last_name);


		Calendar c = Calendar.getInstance();
		System.out.println("Current time => " + c.getTime());
		SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
		String formattedDate = df.format(c.getTime());
		date.setText(formattedDate);

		return view;
	}
	private void Setup_Listeners() {
		// TODO Auto-generated method stub
		rlSearch.setOnClickListener(this);
		container.setOnClickListener(this);
		rlEdit.setOnClickListener(this);
		iv_missimgfollowup.setOnClickListener(this);
		iv_appointmentdetails.setOnClickListener(this);
	}
	private void Initialize_Componenets(View view) {
		// TODO Auto-generated method stub
		etPatientName=(EditText)view.findViewById(R.id.etpatientname);
		rlSearch=(RelativeLayout)view.findViewById(R.id.rlsearch);
		rlEdit=(RelativeLayout)view.findViewById(R.id.rledit);

		et_editname=(EditText)view.findViewById(R.id.etedit_name);
		date=(TextView)view.findViewById(R.id.tvdate);
		drname=(TextView)view.findViewById(R.id.tvdrname);
		container=(LinearLayout)view.findViewById(R.id.llcontainer);
		iv_missimgfollowup=(ImageView)view.findViewById(R.id.ivMissingfollowup);
		iv_appointmentdetails=(ImageView)view.findViewById(R.id.ivAppointment);
	}
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.rledit:


			if(TextUtils.isEmpty(et_editname.getText().toString())){
				et_editname.setError("Enter PatientName or PatientID");

				et_editname.requestFocus();

			}else{
				et_editname.setError(null);
				fetchingpatientsnameedit();
			}

			InputMethodManager in = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
			in.hideSoftInputFromWindow(v.getWindowToken(),
					InputMethodManager.HIDE_NOT_ALWAYS);

			break;


		case R.id.rlsearch:



			if(TextUtils.isEmpty(etPatientName.getText().toString())){
				etPatientName.setError("Enter PatientName or PatientID");

				etPatientName.requestFocus();

			}else{
				etPatientName.setError(null);
				fetchingpatientsname();
			}


			InputMethodManager inputmanger = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
			inputmanger.hideSoftInputFromWindow(v.getWindowToken(),
					InputMethodManager.HIDE_NOT_ALWAYS);


			break;

		case R.id.ivMissingfollowup:
			//getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);	
			getFragmentManager().beginTransaction().replace(R.id.fl_fragment_container, new Missingfollowup_fragment()).addToBackStack(null).commit();

			break;

		case R.id.ivAppointment:
			//getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
			getFragmentManager().beginTransaction().replace(R.id.fl_fragment_container, new AppointmentSchedule()).addToBackStack(null).commit();

			break;

		case R.id.llcontainer:

			InputMethodManager inputmang = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
			inputmang.hideSoftInputFromWindow(v.getWindowToken(),
					InputMethodManager.HIDE_NOT_ALWAYS);

			break;

		default:
			break;
		}
	}
	@SuppressWarnings("unchecked")
	private void fetchingpatientsname() {
		// TODO Auto-generated method stub
		mProgressDialog = new ProgressDialog(getActivity());
		mProgressDialog.setMessage("Fetching name...");
		mProgressDialog.show();

		String name ="";
		String namelast="";
		String firstName="";
		String lastName="";
		String UserFullName=etPatientName.getText().toString();
		String lastnamefirstletteruppercase="";

		if(UserFullName.contains(" ")){
			int firstSpace = UserFullName.indexOf(" "); // detect the first space character
			firstName = UserFullName.substring(0, firstSpace);  // get everything upto the first space character
			lastName = UserFullName.substring(firstSpace).trim(); // get everything after the first space, trimming the spaces off
		}else{
			firstName=etPatientName.getText().toString();
			lastName="";
		}

		for(int i=1;i<firstName.length();i++){
			name+=firstName.charAt(i);
		}
		for(int i=1;i<lastName.length();i++){
			namelast+=lastName.charAt(i);
		}

		String firstletteruppercase=firstName.substring(0,1).toUpperCase()+name;

		if(lastName.length()!=0)
			lastnamefirstletteruppercase=lastName.substring(0,1).toUpperCase()+namelast;

		String nameedittext=etPatientName.getText().toString().trim();
		String uppercase=etPatientName.getText().toString().toUpperCase();
		String lowercase=etPatientName.getText().toString().toLowerCase();

		String fistuppercase=firstName.toUpperCase();
		String firstlowercase=firstName.toLowerCase();

		String lastuppercase=lastName.toUpperCase();
		String lastlowercase=lastName.toLowerCase();


		ArrayList<String> listnamecases=new ArrayList<String>();
		listnamecases.add(firstletteruppercase);
		listnamecases.add(nameedittext);
		listnamecases.add(uppercase);
		listnamecases.add(lowercase);
		listnamecases.add(fistuppercase);
		listnamecases.add(firstlowercase);
		listnamecases.add(lastuppercase);
		listnamecases.add(lastlowercase);
		listnamecases.add(lastName);
		listnamecases.add(firstName);
		listnamecases.add(lastnamefirstletteruppercase);


		ArrayList<String> firstnamecases=new ArrayList<String>();
		ArrayList<String> lastnamecases=new ArrayList<String>();
		firstnamecases.add(fistuppercase);
		firstnamecases.add(firstlowercase);

		lastnamecases.add(lastuppercase);
		lastnamecases.add(lastlowercase);

		ParseQuery<ParseObject> fullname= ParseQuery.getQuery("Patients");
		fullname.whereContainedIn("firstName", listnamecases);
		if(lastName.length()!=0){
			fullname.whereContainedIn("lastName", listnamecases);
		}
		ParseQuery<ParseObject> idpatient= ParseQuery.getQuery("Patients");
		idpatient.whereContainedIn("patientID", listnamecases);


		ParseQuery<ParseObject> last= ParseQuery.getQuery("Patients");
		last.whereContainedIn("lastName", listnamecases);



		List<ParseQuery<ParseObject>> queries = new ArrayList<ParseQuery<ParseObject>>();
		queries.add(fullname);
		queries.add(idpatient);
		queries.add(last);

		System.out.println("working........@@");
		
		@SuppressWarnings("rawtypes")
		ParseQuery query = ParseQuery.or(queries);
		query.fromLocalDatastore();
		query.findInBackground(new FindCallback<ParseObject>() {

			@Override
			public void done(List<ParseObject> objects, ParseException e) {
				// TODO Auto-generated method stub
				patientobject=objects;
				mProgressDialog.dismiss();
				if(e==null){
					patientname.clear();
				/*	if(objects.size()>0){*/
						for(int i=0;i<objects.size();i++){
							String name=objects.get(i).getString("firstName");
							if(name!=null){
								patientname.add(objects.get(i).getString("firstName").toString());
							}
							System.out.println("namessssssssss"+objects.get(i).getString("firstName"));
						}

						((FragmentActivityContainer)getActivity()).setPatientserachresultObj(patientobject);


						//getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);	

						getFragmentManager().beginTransaction().replace(R.id.fl_fragment_container, new SearchPatientresult()).addToBackStack(null).commit();


					/*}*/
				}else{
					e.printStackTrace();
				}
			}
		});
	}



	@SuppressWarnings("unchecked")
	private void fetchingpatientsnameedit() {
		// TODO Auto-generated method stub
		mProgressDialog = new ProgressDialog(getActivity());
		mProgressDialog.setMessage("Fetching name...");
		mProgressDialog.show();

		String name ="";
		String namelast="";
		String firstName="";
		String lastName="";
		String UserFullName=et_editname.getText().toString();
		String lastnamefirstletteruppercase="";

		if(UserFullName.contains(" ")){
			int firstSpace = UserFullName.indexOf(" "); // detect the first space character
			firstName = UserFullName.substring(0, firstSpace);  // get everything upto the first space character
			lastName = UserFullName.substring(firstSpace).trim(); // get everything after the first space, trimming the spaces off
		}else{
			firstName=et_editname.getText().toString();
			lastName="";
		}

		for(int i=1;i<firstName.length();i++){
			name+=firstName.charAt(i);
		}
		for(int i=1;i<lastName.length();i++){
			namelast+=lastName.charAt(i);
		}

		String firstletteruppercase=firstName.substring(0,1).toUpperCase()+name;

		if(lastName.length()!=0)
			lastnamefirstletteruppercase=lastName.substring(0,1).toUpperCase()+namelast;

		String nameedittext=et_editname.getText().toString().trim();
		String uppercase=et_editname.getText().toString().toUpperCase();
		String lowercase=et_editname.getText().toString().toLowerCase();

		String fistuppercase=firstName.toUpperCase();
		String firstlowercase=firstName.toLowerCase();

		String lastuppercase=lastName.toUpperCase();
		String lastlowercase=lastName.toLowerCase();


		ArrayList<String> listnamecases=new ArrayList<String>();
		listnamecases.add(firstletteruppercase);
		listnamecases.add(nameedittext);
		listnamecases.add(uppercase);
		listnamecases.add(lowercase);
		listnamecases.add(fistuppercase);
		listnamecases.add(firstlowercase);
		listnamecases.add(lastuppercase);
		listnamecases.add(lastlowercase);
		listnamecases.add(lastName);
		listnamecases.add(firstName);
		listnamecases.add(lastnamefirstletteruppercase);


		ArrayList<String> firstnamecases=new ArrayList<String>();
		ArrayList<String> lastnamecases=new ArrayList<String>();
		firstnamecases.add(fistuppercase);
		firstnamecases.add(firstlowercase);

		lastnamecases.add(lastuppercase);
		lastnamecases.add(lastlowercase);

		ParseQuery<ParseObject> fullname= ParseQuery.getQuery("Patients");
		fullname.whereContainedIn("firstName", listnamecases);
		if(lastName.length()!=0){
			fullname.whereContainedIn("lastName", listnamecases);
		}
		ParseQuery<ParseObject> idpatient= ParseQuery.getQuery("Patients");
		idpatient.whereContainedIn("patientID", listnamecases);


		ParseQuery<ParseObject> last= ParseQuery.getQuery("Patients");
		last.whereContainedIn("lastName", listnamecases);



		List<ParseQuery<ParseObject>> queries = new ArrayList<ParseQuery<ParseObject>>();
		queries.add(fullname);
		queries.add(idpatient);
		queries.add(last);


		@SuppressWarnings("rawtypes")
		ParseQuery query = ParseQuery.or(queries);
		query.fromLocalDatastore();
		query.findInBackground(new FindCallback<ParseObject>() {

			@Override
			public void done(List<ParseObject> objects, ParseException e) {
				// TODO Auto-generated method stub
				patientobject=objects;
				mProgressDialog.dismiss();
				if(e==null){
					patientname.clear();
					for(int i=0;i<objects.size();i++){
						String name=objects.get(i).getString("firstName");
						if(name!=null){
							patientname.add(objects.get(i).getString("firstName").toString());
						}
					}

					((FragmentActivityContainer)getActivity()).setPatientserachresultObj(patientobject);

					//getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);	

					getFragmentManager().beginTransaction().replace(R.id.fl_fragment_container, new Editpatientresult()).addToBackStack(null).commit();


				}
			}
		});
	}



	public List<ParseObject> getpatientsobject()
	{
		return this.patientobject;

	}
	/*@Override
	public void popupdismiss(int arg) {
		// TODO Auto-generated method stub
		searchname.dismiss();	
		((Patient_Tab_Activity)getActivity()).setviewpagerslide(true);
		if(((Patient_Tab_Activity)getActivity()).gettabcount()==1){
			((Patient_Tab_Activity)getActivity()).addactionbartabs();
		}
		((Patient_Tab_Activity)getActivity()).getviewpager().setCurrentItem(0);
	}*/

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		CrashReporter.getInstance().trackScreenView("Admin Patients");
	}

}
