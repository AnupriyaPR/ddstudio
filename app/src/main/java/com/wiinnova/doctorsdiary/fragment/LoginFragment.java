package com.wiinnova.doctorsdiary.fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.wiinnova.doctorsdiary.R;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.wiinnova.doctorsdiary.activities.Home_Page_Activity;
import com.wiinnova.doctorsdiary.supportclasses.ConnectionDetector;
import com.wiinnova.doctorsdiary.supportclasses.SuspecteddiseasenamesClass;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class LoginFragment extends Fragment implements OnClickListener{

	private TextView tv_forgotpassword;
	private EditText uEmail;
	private EditText uPass;
	private Button logIn;
	private ProgressDialog mProgressDialog;
	private String userEmail;
	private String password;

	ParseObject patient;
	int localobjectsize;
	int skip=0;
	private String docRegistartionnumber;

	ConnectionDetector networkconnection_check;


	List<ParseObject> diseaseobjects=new ArrayList<ParseObject>();

	SuspecteddiseasenamesClass suspecteddiseaseclassObj;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View v=inflater.inflate(R.layout.login_layout_fragment, null);
		Initialize_Componenets(v);
		Setup_Listeners();

		return v;
	}

	private void Setup_Listeners() {
		// TODO Auto-generated method stub

		logIn.setOnClickListener(this);
		tv_forgotpassword.setOnClickListener(this);
	}

	private void Initialize_Componenets(View v) {
		// TODO Auto-generated method stub

		tv_forgotpassword=(TextView)v.findViewById(R.id.forgot_pass);
		uEmail=(EditText)v.findViewById(R.id.uname);
		uPass=(EditText)v.findViewById(R.id.upass);
		logIn=(Button)v.findViewById(R.id.login);

		networkconnection_check=new ConnectionDetector(getActivity());
		suspecteddiseaseclassObj=new SuspecteddiseasenamesClass();
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.login:
			login_method();
			break;

		case R.id.forgot_pass:
			forgot_password();
		}


	}

	private void forgot_password() {
		// TODO Auto-generated method stub
		getFragmentManager().beginTransaction().replace(R.id.container, new ForgotPasswordFragment()).commit();

	}

	private void login_method() {
		// TODO Auto-generated method stub

		boolean validfields=true;
		//hideKeyboard(arg0);

		userEmail=uEmail.getText().toString();
		password=uPass.getText().toString().trim();

		if(TextUtils.isEmpty(uPass.getText().toString())){
			uPass.setError("Enter PassWord ");
			validfields=false;
			uPass.requestFocus();

		}else{
			uPass.setError(null);
		}

		if(!isValidEmail(uEmail.getText().toString()) || uEmail.length()==0){
			System.out.println("email workinggggggggg");
			uEmail.setError("Enter User Mail ID ");
			uEmail.requestFocus();
			validfields=false;
		}else{
			uEmail.setError(null);
		}


		if(validfields==true){
			if(networkconnection_check.isConnectedToInternet())
			{
				mProgressDialog = new ProgressDialog(getActivity());
				mProgressDialog.setMessage("Logging In....");
				mProgressDialog.show();

				ParseQuery<ParseObject> query = ParseQuery.getQuery("UserMaster");
				query.whereEqualTo("email", userEmail);
				query.whereEqualTo("password", password);
				query.findInBackground(new FindCallback<ParseObject>() {



					@Override
					public void done(List<ParseObject> objects, ParseException e) {
						mProgressDialog.dismiss();

						// TODO Auto-generated method stub
						if (e == null) {
							if (objects.size() > 0) {
								// query found a user
								Log.e("Login", "Logged innnnnnnn");

								/*fetechingdiseasenames_server();*/

								//fetechdiseasefromserver();
								ParseObject userobject=objects.get(0);

								docRegistartionnumber=userobject.getString("docRegistrationNumber");
								String drFname=userobject.getString("firstName");
								String drLname=userobject.getString("lastName");													    
								String accredition=userobject.getString("accredition");
								String cityname=userobject.getString("city");
								String country=userobject.getString("country");
								String eMail=userobject.getString("email");													    
								String phone=userobject.getString("phone");
								String pinzip=userobject.getString("pincode");
								String qualification=userobject.getString("qualification");
								String specialization=userobject.getString("specialization");													    
								String stat=userobject.getString("state");
								String strname=userobject.getString("street");
								String supSpl=userobject.getString("superSpecialization");													   
								String fLname=drFname+" "+drLname;

								fetchingdocterpatientDetails();

//								Toast.makeText(getActivity(),"Login Successful",Toast.LENGTH_SHORT).show();

								SharedPreferences sp=getActivity().getSharedPreferences("Login", 0);
								SharedPreferences.Editor Ed=sp.edit();
								Ed.putString("Unm",userEmail );              
								Ed.putString("Psw",password); 
								Ed.putString("drFLname",fLname);
								Ed.putString("fName",drFname);
								Ed.putString("lName",drLname);													
								Ed.putString("Accredi",accredition);
								Ed.putString("cityName",cityname);
								Ed.putString("Country",country);
								Ed.putString("mailId",eMail);
								Ed.putString("phNo",phone);
								Ed.putString("pinCode",pinzip);
								Ed.putString("qual",qualification);
								Ed.putString("spel",specialization);
								Ed.putString("State",stat);
								Ed.putString("Street",strname);
								Ed.putString("SuperSpl",supSpl);
								Ed.putString("docterRegnumber", docRegistartionnumber);
								
								
								Ed.putFloat("header",180f);              
								Ed.putFloat("footer",90f); 
								Ed.putFloat("left",30f);
								Ed.putFloat("right",120f);
								Ed.putFloat("title", 8f);
								Ed.putFloat("content", 8f);
								
								Ed.commit();

								Intent i=new Intent(getActivity(),Home_Page_Activity.class);
								startActivity(i);
								getActivity().finish();
								Log.e(" login page", " finish");

							}
							else {
								Toast msg=Toast.makeText(getActivity(), "User Name Or Password Incorrect", Toast.LENGTH_LONG);
								msg.show();
							}
						} 
						else {
							Toast.makeText(getActivity(), "Sorry, something went wrong", Toast.LENGTH_LONG).show();
						}

					}


				});
			}else{
				Toast.makeText(getActivity(), "Internet is not connected.Please Switch On Net Connectivity", Toast.LENGTH_LONG).show();
			}
		}

	}

	boolean isValidEmail(CharSequence target) {
		if (target == null) {
			return false;
		} else {
			return android.util.Patterns.EMAIL_ADDRESS.matcher(target)
					.matches();
		}
	}



	private void fetchingdocterpatientDetails() {
		// TODO Auto-generated method stub

		ParseQuery<ParseObject> query = ParseQuery.getQuery("Patients");
		query.fromLocalDatastore();
		query.whereEqualTo("docterRegistrationNumber", docRegistartionnumber);
		query.findInBackground(new FindCallback<ParseObject>() {

			@Override
			public void done(List<ParseObject> objects, ParseException e) {
				// TODO Auto-generated method stub
				if(e==null){
					if(objects.size()==0){
						ParseQuery<ParseObject> query = ParseQuery.getQuery("Patients");
						query.whereEqualTo("docterRegistrationNumber", docRegistartionnumber);
						query.findInBackground(new FindCallback<ParseObject>() {
							@Override
							public void done(List<ParseObject> objects, ParseException e) {
								// TODO Auto-generated method stub
								if(e==null){
									if(objects.size()>0){
										System.out.println("pin patient object size"+objects.size());
										ParseObject.pinAllInBackground(objects);

										for(int i=0;i<objects.size();i++){
											ParseQuery<ParseObject> query = ParseQuery.getQuery("Diagnosis");
											query.whereEqualTo("patientID", objects.get(i).getString("patientID"));
											query.findInBackground(new FindCallback<ParseObject>() {

												@Override
												public void done(List<ParseObject> object, ParseException e) {
													// TODO Auto-generated method stub
													if(e==null){
														if(object.size()>0){
															ParseObject.pinAllInBackground(object);
														}

													}


												}
											});

										}

									}else{

									}
								}

							}
						});
					}
				}

			}
		});

	}


	private void fetechdiseasefromserver() {
		// TODO Auto-generated method stub
		ParseQuery<ParseObject> query = ParseQuery.getQuery("DiseaseDatabase");
		query.fromLocalDatastore();
		query.findInBackground(new FindCallback<ParseObject>() {

			@Override
			public void done(List<ParseObject> objects, ParseException arg1) {
				// TODO Auto-generated method stub

				localobjectsize=objects.size();
				if(objects.size()==0){
					System.out.println("localobjectsize"+localobjectsize);
					ParseQuery<ParseObject> query = ParseQuery.getQuery("DiseaseDatabase");
					if(localobjectsize!=0){
						query.fromLocalDatastore();
					}
					query.setLimit(1000);
					query.findInBackground(getAllObjects());
				}

			}
		});
	}

	private FindCallback getAllObjects() {
		// TODO Auto-generated method stub

		return new FindCallback<ParseObject>() {

			@Override
			public void done(List<ParseObject> objects, ParseException e) {
				// TODO Auto-generated method stub

				if (e == null) {

					diseaseobjects.addAll(objects);

					int limit =1000;
					if (objects.size() == limit){
						skip = skip + limit;
						ParseQuery query = new ParseQuery("DiseaseDatabase");
						System.out.println("local"+localobjectsize);
						if(localobjectsize!=0){
							query.fromLocalDatastore();
						}
						query.setSkip(skip);
						query.setLimit(limit);
						query.findInBackground(getAllObjects());
					}
					else {

						((CrashReporter)getActivity().getApplicationContext()).setDiseasenameObjects(diseaseobjects);
						ParseObject.pinAllInBackground(diseaseobjects);
					}


				}
			}
		};
	}






	private void fetechingdiseasenames_server() {
		// TODO Auto-generated method stub
		ParseQuery<ParseObject> query = ParseQuery.getQuery("DiseaseDatabase");
		query.fromLocalDatastore();
		query.findInBackground(new FindCallback<ParseObject>() {
			@Override
			public void done(List<ParseObject> objects, ParseException e) {
				// TODO Auto-generated method stub
				if(objects.size()==0){

					System.out.println("no data avilable");
					ParseQuery<ParseObject> query = ParseQuery.getQuery("DiseaseDatabase");
					query.setLimit(1000);
					query.whereNotEqualTo("DiseaseName",null);
					query.findInBackground(new FindCallback<ParseObject>() {



						@Override
						public void done(List<ParseObject> objects, ParseException e) {

							// TODO Auto-generated method stub
							if (e == null) {
								if (objects.size() > 0) {
									// query found a user

									Log.e("Login", "Id exist");
									patient=objects.get(0);
									diseaseobjects.addAll(objects);

									diseaseselect_server();


								}
								else {

									Toast msg=Toast.makeText(getActivity(), "Some problem in your network connection.Please check and try agian... ", Toast.LENGTH_LONG);
									msg.show();

								}
							} 
							else {
								Toast.makeText(getActivity(), "Sorry, something went wrong", Toast.LENGTH_LONG).show();
							}

						}


					});



				}


			}
		});




	}

	private void diseaseselect_server() {
		// TODO Auto-generated method stub

		ParseQuery<ParseObject> query = ParseQuery.getQuery("DiseaseDatabase");
		query.setLimit(589);
		query.setSkip(1000);
		query.whereNotEqualTo("DiseaseName",null);
		query.findInBackground(new FindCallback<ParseObject>() {



			@Override
			public void done(List<ParseObject> objects, ParseException e) {
				mProgressDialog.dismiss();
				// TODO Auto-generated method stub
				if (e == null) {
					if (objects.size() > 0) {
						Log.e("Login", "Id exist");
						patient=objects.get(0);
						diseaseobjects.addAll(objects);
						Log.e("Login", "Id exist"+diseaseobjects.size());

						ParseObject.pinAllInBackground(diseaseobjects);

						((CrashReporter)getActivity().getApplicationContext()).setDiseasenameObjects(diseaseobjects);

						System.out.println("working..llll..........."+diseaseobjects.size());




					}
					else {
						Toast msg=Toast.makeText(getActivity(), "Some problem in your network connection.Please check and try agian...", Toast.LENGTH_LONG);
						msg.show();
					}
				} 
				else {
					Toast.makeText(getActivity(), "Sorry, something went wrong", Toast.LENGTH_LONG).show();
				}
			}
		});
	}




}
