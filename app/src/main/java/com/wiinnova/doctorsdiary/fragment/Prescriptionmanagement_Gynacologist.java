package com.wiinnova.doctorsdiary.fragment;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.wiinnova.doctorsdiary.R;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.SaveCallback;
import com.wiinnova.doctorsdiary.activities.FragmentActivityContainer;
import com.wiinnova.doctorsdiary.activities.Home_Page_Activity;
import com.wiinnova.doctorsdiary.fragment.Treatment_Frag.onTreatmentmanagement;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Prescriptionmanagement_Gynacologist extends Fragment implements onTreatmentmanagement {

	private CheckBox cbconservative;
	private CheckBox cbsurgical;
	private EditText etlineoftreatment;

	private CheckBox cbphysical;
	private CheckBox cbpelvic;
	private EditText etexercise;

	private CheckBox cbsalt;
	private CheckBox cbsugar;

	private CheckBox cbhighprotein;
	private CheckBox cbother;
	private EditText etDiet;

	private EditText etsometext;



	private Treatment_Frag treatementfrag;

	private ParseObject managementtreatmentparseObj;
	private ProgressDialog mProgressDialog;

	TextView dflName,duniqueID;
	String puid,filan;
	private TextView tvdate;
	private long currentDateandTime;
	private String formattedDate,formattedDate1;
	private LinearLayout mainlayout;
	private ScrollView scroll;
	private SimpleDateFormat df;


	public interface ontreatmentmanagementbtnvisible{
		void onTreatmentmanagementbtn(int arg);
	}



	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,

			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view=inflater.inflate(R.layout.prescriptionmanagement_gynecologist, null);

		dflName=(TextView)view.findViewById(R.id.tvname);
		duniqueID=(TextView)view.findViewById(R.id.cduniqueid);
		tvdate=(TextView)view.findViewById(R.id.ctmcurrdate);
		mainlayout=(LinearLayout)view.findViewById(R.id.llmaincontainer);
		scroll=(ScrollView)view.findViewById(R.id.scroll);

		treatementfrag = new  Treatment_Frag();
		getFragmentManager().beginTransaction()
		.replace(R.id.fl_sidemenu_container, treatementfrag)
		.commit();

		((FragmentActivityContainer)getActivity()).setTreatmentmanagement_gynacologist(this);
		managementtreatmentparseObj=((FragmentActivityContainer)getActivity()).getpatienttreatmentparseObj();
		//FragmentActivityContainer.treat_save_check=0;

		if(treatementfrag!=null){
			((FragmentActivityContainer)getActivity()).settreatmentfrag(treatementfrag);
		}

		((FragmentActivityContainer)getActivity()).gettreatmentfrag().onTreatmentmanagementbtn(0);



		mainlayout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				hideKeyboard(v);
			}
		});

		scroll.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				hideKeyboard(v);

			}
		});




		if(Home_Page_Activity.patient!=null){
			puid=Home_Page_Activity.patient.getString("patientID");
			String fname=Home_Page_Activity.patient.getString("firstName");
			String lname=Home_Page_Activity.patient.getString("lastName");
			System.out.println("patient id1,,,,,,"+puid);
			if(fname==null){
				fname="FirstName";
			}
			if(lname==null){
				lname="LastName";
			}
			dflName.setText(fname+" "+lname);

		}else{

			SharedPreferences scanpidnew=getActivity().getSharedPreferences("NewPatID", 0);
			String scnewPid=scanpidnew.getString("NewID", null); 
			SharedPreferences scanpid1=getActivity().getSharedPreferences("ScannedPid", 0);
			String scPid=scanpid1.getString("scanpid", null);       
			if(scPid!=null){
				puid=scPid;
				SharedPreferences fnname1=getActivity().getSharedPreferences("fnName", 0);
				dflName.setText(fnname1.getString("fNname", "FirstName"+" "+"LastName"));
				System.out.println("name diag view.."+fnname1.getString("fNname", "FirstName"+" "+"LastName"));
			}

			if(scnewPid!=null){

				puid=scnewPid;


				SharedPreferences fnname1=getActivity().getSharedPreferences("fnName", 0);
				dflName.setText(fnname1.getString("fNname", "FirstName"+" "+"LastName"));
				System.out.println("name diag view.."+fnname1.getString("fNname", "FirstName"+" "+"LastName"));
			}
		}




		duniqueID.setText(puid);

		Calendar c = Calendar.getInstance();
		System.out.println("Current time => " + c.getTime());
		SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");

		SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss");
		currentDateandTime = c.getTimeInMillis();

		//Log.e("cure Timeee====", String.valueOf(c.getTimeInMillis()));

		formattedDate = df.format(c.getTime());
		tvdate.setText(formattedDate);



		cbconservative=(CheckBox)view.findViewById(R.id.cbconservation);
		cbconservative.setOnClickListener(new OnClickListener() {	
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				if(cbconservative.isChecked()){
					cbsurgical.setChecked(false);
					submitButtonActivation();
				}

			}
		});

		cbsurgical=(CheckBox)view.findViewById(R.id.cbsurgical);
		cbsurgical.setOnClickListener(new OnClickListener() {	
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				if(cbsurgical.isChecked()){
					cbconservative.setChecked(false);
					submitButtonActivation();
				}

			}
		});



		cbphysical=(CheckBox)view.findViewById(R.id.cbphysical);
		cbphysical.setOnClickListener(new OnClickListener() {	
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				if(cbphysical.isChecked()){
					cbpelvic.setChecked(false);
					submitButtonActivation();
				}

			}
		});


		cbpelvic=(CheckBox)view.findViewById(R.id.cbpelvic);
		cbpelvic.setOnClickListener(new OnClickListener() {	
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				if(cbpelvic.isChecked()){
					cbphysical.setChecked(false);
					submitButtonActivation();
				}

			}
		});


		cbsalt=(CheckBox)view.findViewById(R.id.cbsalt);
		cbsalt.setOnClickListener(new OnClickListener() {	
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				if(cbsalt.isChecked()){
					cbsugar.setChecked(false);
					submitButtonActivation();
				}

			}
		});



		cbsugar=(CheckBox)view.findViewById(R.id.cbsugar);
		cbsugar.setOnClickListener(new OnClickListener() {	
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				if(cbsugar.isChecked()){
					cbsalt.setChecked(false);
					submitButtonActivation();
				}

			}
		});



		cbhighprotein=(CheckBox)view.findViewById(R.id.cbhighprotein);
		cbhighprotein.setOnClickListener(new OnClickListener() {	
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				if(cbhighprotein.isChecked()){
					cbother.setChecked(false);
					submitButtonActivation();
				}

			}
		});


		cbother=(CheckBox)view.findViewById(R.id.cbother);
		cbother.setOnClickListener(new OnClickListener() {	
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				if(cbother.isChecked()){
					cbhighprotein.setChecked(false);
					submitButtonActivation();
				}

			}
		});


		etlineoftreatment=(EditText)view.findViewById(R.id.etlineoftreatment);
		etDiet=(EditText)view.findViewById(R.id.etother);
		etexercise=(EditText)view.findViewById(R.id.etgeneral);
		etsometext=(EditText)view.findViewById(R.id.etexersise);


		return view;
	}

	protected void hideKeyboard(View view) {
		InputMethodManager in = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
		in.hideSoftInputFromWindow(view.getWindowToken(),
				InputMethodManager.HIDE_NOT_ALWAYS);
	}

	private void submitButtonActivation() {
		// TODO Auto-generated method stub
		((FragmentActivityContainer)getActivity()).gettreatmentfrag().onTreatmentmanagementbtn(1);
	}

	private void submitButtonDeactivation() {
		// TODO Auto-generated method stub
		((FragmentActivityContainer)getActivity()).gettreatmentfrag().onTreatmentmanagementbtn(0);
	}



	@Override
	public void onManagement(final int targetfragment) {
		// TODO Auto-generated method stub



		mProgressDialog = new ProgressDialog(getActivity());
		mProgressDialog.setCancelable(false);
		mProgressDialog.setMessage("Storing Management Data....");
		mProgressDialog.show();

		if(managementtreatmentparseObj==null)
			managementtreatmentparseObj=new ParseObject("Diagnosis");


		managementtreatmentparseObj.put("typeFlag", 2);
		managementtreatmentparseObj.put("patientID",duniqueID.getText().toString());
		Calendar c = Calendar.getInstance();
		System.out.println("Current time => " + c.getTime());
		df = new SimpleDateFormat("dd-MMM-yyyy");
		formattedDate1 = df.format(c.getTime());

		managementtreatmentparseObj.put("createdDate",formattedDate1);
		managementtreatmentparseObj.put("createdatetime",currentDateandTime);


		//////////////////////////////set a Uniqe id
		if (((FragmentActivityContainer) getActivity()).getDiagnosisobject_id() != null)  {
			managementtreatmentparseObj.put("diagnosisId", ((FragmentActivityContainer) getActivity()).getDiagnosisobject_id());
		} else {
			managementtreatmentparseObj.put("diagnosisId", Long.toString(currentDateandTime));
		}
		////////////////////////////////////////////////////////////


		String lineoftreatment,editlineoftreatment;
		if(cbconservative.isChecked()){
			lineoftreatment=cbconservative.getText().toString();
		}else if(cbsurgical.isChecked()){
			lineoftreatment=cbsurgical.getText().toString();
		}else{
			lineoftreatment="Nil";
		}

		if(etlineoftreatment.getText().toString().length()!=0){
			editlineoftreatment=etlineoftreatment.getText().toString();
		}else{
			editlineoftreatment="Nil";
		}

		managementtreatmentparseObj.put("lineoftreatment", lineoftreatment+","+editlineoftreatment);


		String general_exercise,editgeneral_exercise;
		if(cbphysical.isChecked()){
			general_exercise=cbphysical.getText().toString();
		}else if(cbpelvic.isChecked()){
			general_exercise=cbpelvic.getText().toString();
		}else{
			general_exercise="Nil";
		}			
		if(etexercise.getText().toString().length()!=0){
			editgeneral_exercise=etexercise.getText().toString();
		}else{
			editgeneral_exercise="Nil";
		}

		managementtreatmentparseObj.put("general_exercise", general_exercise+","+editgeneral_exercise);


		String general_diet,editgeneral_diet;
		if(cbsalt.isChecked()){
			general_diet=cbsalt.getText().toString();
		}else if(cbsugar.isChecked()){
			general_diet=cbsugar.getText().toString();
		}else{
			general_diet="Nil";
		}			


		managementtreatmentparseObj.put("general_diet", general_diet);


		String general_highprotein,editgeneral_protein;
		if(cbhighprotein.isChecked()){
			general_highprotein=cbhighprotein.getText().toString();
		}else if(cbother.isChecked()){
			general_highprotein=cbother.getText().toString();
		}else{
			general_highprotein="Nil";
		}			
		if(etDiet.getText().toString().length()!=0){
			editgeneral_protein=etDiet.getText().toString();
		}else{
			editgeneral_protein="Nil";
		}

		managementtreatmentparseObj.put("general_diet_highprotein", general_highprotein+","+editgeneral_protein);


		if(etsometext.getText().toString().length()!=0){
			managementtreatmentparseObj.put("exercise", etsometext.getText().toString());
		}else{
			managementtreatmentparseObj.put("exercise", "Nil");
		}

		managementtreatmentparseObj.pinInBackground(new SaveCallback() {

			@Override
			public void done(ParseException e) {
				// TODO Auto-generated method stub
				mProgressDialog.dismiss();
				if(e==null){

					if (((FragmentActivityContainer) getActivity()).getDiagnosisobject_id() == null) {
						((FragmentActivityContainer) getActivity()).setDiagnosisobject_id(Long.toString(currentDateandTime));
					}

					((FragmentActivityContainer)getActivity()).setpatienttreatmentparseObj(managementtreatmentparseObj);

					Treatment_Create_Frag  createtrat;
					
					if(targetfragment==3){
						FragmentManager fragmentManager = getFragmentManager();
						if(((FragmentActivityContainer)getActivity()).gettreatmentcreatefrag()!=null){
							 createtrat=((FragmentActivityContainer)getActivity()).gettreatmentcreatefrag();
						}else{
							createtrat= new Treatment_Create_Frag();
						}	
						FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
						fragmentTransaction.replace(R.id.fl_fragment_container, createtrat);
						fragmentTransaction.commit();
						
						
					}else if(targetfragment==2){
						
						FragmentManager fragmentManager = getFragmentManager();

						Fragment prescrionmanagement;
						prescrionmanagement= new ManagementGynacologist_View();
						FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
						fragmentTransaction.replace(R.id.fl_fragment_container, prescrionmanagement);
						fragmentTransaction.commit();
					}else if(targetfragment==4){
						
						FragmentManager fragmentManager = getFragmentManager();
						FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
						Treatment_View_Frag viewtrat = new Treatment_View_Frag();
						fragmentTransaction.replace(R.id.fl_fragment_container, viewtrat);
						fragmentTransaction.commit();
						
					}
					
					Toast.makeText(getActivity(), "Successfully Saved", Toast.LENGTH_LONG).show();				
					((FragmentActivityContainer)getActivity()).gettreatmentfrag().onTreatmentmanagementbtn(0);
					FragmentActivityContainer.treatmangement_save_check=0;
					
				}


			}
		});

		managementtreatmentparseObj.saveEventually();
	}


	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

		if(managementtreatmentparseObj!=null){

			if(managementtreatmentparseObj.getString("lineoftreatment")!=null){
				String[] lineoftreatment=managementtreatmentparseObj.getString("lineoftreatment").split(",");

				if(lineoftreatment[0].equals("Conservation")){
					cbconservative.setChecked(true);
				}else if(lineoftreatment[0].equals("Surgical")){
					cbsurgical.setChecked(true);
				}


				if(!lineoftreatment[1].equalsIgnoreCase("Nil")){
					etlineoftreatment.setText(lineoftreatment[1].toString());
				}

			}

			if(managementtreatmentparseObj.getString("general_exercise")!=null){
				String[] exercise=managementtreatmentparseObj.getString("general_exercise").split(",");

				if(exercise[0].equals("Physical")){
					cbphysical.setChecked(true);
				}else if(exercise[0].equals("Pelvic")){
					cbpelvic.setChecked(true);
				}

				if(!exercise[1].equalsIgnoreCase("Nil")){
					etexercise.setText(exercise[1].toString());
				}
			}

			if(managementtreatmentparseObj.getString("general_diet")!=null){
				String diet=managementtreatmentparseObj.getString("general_diet");

				if(diet.equalsIgnoreCase("salt")){
					cbsalt.setChecked(true);
				}else if(diet.equalsIgnoreCase("sugar")){
					cbsugar.setChecked(true);
				}
			}

			if(managementtreatmentparseObj.getString("general_diet_highprotein")!=null){
				String dietsecond[]=managementtreatmentparseObj.getString("general_diet_highprotein").split(",");

				if(dietsecond[0].equals("High Protein")){
					cbhighprotein.setChecked(true);
				}else if(dietsecond[0].equals("Other")){
					cbother.setChecked(true);
				}

				if(!dietsecond[1].equalsIgnoreCase("Nil")){
					etDiet.setText(dietsecond[1].toString());
				}
			}

			if(managementtreatmentparseObj.getString("exercise")!=null){
				if(!managementtreatmentparseObj.getString("exercise").equals("Nil")){

					etsometext.setText(managementtreatmentparseObj.getString("exercise").toString());
				}	
			}
		}		
	}
}
