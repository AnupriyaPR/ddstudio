package com.wiinnova.doctorsdiary.fragment;


import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import com.wiinnova.doctorsdiary.fragments.PatientpersonalInfo;

import com.wiinnova.doctorsdiary.R;
import com.wiinnova.doctorsdiary.activities.FragmentActivityContainer;
import com.wiinnova.doctorsdiary.activities.printTestActivity;
import com.wiinnova.doctorsdiary.fragment.Prescriptionmanagement_Gynacologist.ontreatmentmanagementbtnvisible;
import com.wiinnova.doctorsdiary.fragment.Treatment_Create_Frag.ontreatmentbtnvisible;
import com.wiinnova.doctorsdiary.popupwindow.PrintlayoutSelectorDialog;
import com.wiinnova.doctorsdiary.supportclasses.PrintHelper;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CancellationSignal;
import android.os.ParcelFileDescriptor;
import android.print.PageRange;
import android.print.PrintAttributes;
import android.print.PrintDocumentAdapter;
import android.print.PrintDocumentInfo;
import android.print.PrintManager;
import android.print.PrintDocumentAdapter.LayoutResultCallback;
import android.print.PrintDocumentAdapter.WriteResultCallback;
import android.text.StaticLayout;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;


public class Treatment_Frag extends Fragment {

    static public RelativeLayout createtm;
    //    static public RelativeLayout viewtm;
    RelativeLayout management;
    RelativeLayout rlContacts;
    Button btnsubmit;
    //private Button btnPrinttest;
    public String specialization;
    static Button btnPrint;
    int flag = 0;
//	private RelativeLayout managementview;


    //ImageView ivBack;


    SharedPreferences sp;

    interface ontreatmentfrag {
        void onTreatment(int arg);
    }

    interface onPrescriptionprint {
        void onPrescription();
    }

    interface onPriscriptionviewprint {
        void onviewPrescription();
    }

    interface onTreatmentmanagement {
        void onManagement(int val);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.treatment_frag, null);

        sp = getActivity().getSharedPreferences("Login", 0);
        specialization = sp.getString("spel", null);

        ((FragmentActivityContainer) getActivity()).settreatmentfrag(this);
        ((FragmentActivityContainer) getActivity()).prescriptiontabcolorchange();

        management = (RelativeLayout) view.findViewById(R.id.prescriptionmanagement);
//		managementview = (RelativeLayout) view.findViewById(R.id.viewmanagement);
        createtm = (RelativeLayout) view.findViewById(R.id.crtm);
//        viewtm = (RelativeLayout) view.findViewById(R.id.viewtm);
        rlContacts = (RelativeLayout) view.findViewById(R.id.rlContactus);
        btnPrint = (Button) view.findViewById(R.id.llPrint);
        btnsubmit = (Button) view.findViewById(R.id.btnSubmit);

        //btnPrinttest=(Button)view.findViewById(R.id.llPrinttest);


        if (!specialization.equalsIgnoreCase("gynecologist obstetrician")) {
            management.setVisibility(View.GONE);
//			managementview.setVisibility(View.GONE);

            createtm.setBackgroundColor(getResources().getColor(R.color.red_color));
//            viewtm.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));

        } else {
            management.setVisibility(View.VISIBLE);
//			managementview.setVisibility(View.VISIBLE);

            management.setBackgroundColor(getResources().getColor(R.color.red_color));
//			managementview.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
            createtm.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
//            viewtm.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));

        }


		/*	btnPrinttest.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent i=new Intent(getActivity(),printTestActivity.class);
				startActivity(i);


			}
		});*/


        //ivBack=(ImageView)view.findViewById(R.id.btnback);
        btnPrint.setEnabled(false);

        btnPrint.setBackgroundDrawable(getResources().getDrawable(R.drawable.submitbtn_background_beforesave));
        btnPrint.setTextColor(getResources().getColor(R.color.grey));


        btnsubmit.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub


                if (((FragmentActivityContainer) getActivity()).gettreatmentcreatefrag() != null) {

                    if (((FragmentActivityContainer) getActivity()).gettreatmentcreatefrag().isVisible()) {

                        ((FragmentActivityContainer) getActivity()).gettreatmentcreatefrag().onTreatment(1);

                    } else if (((FragmentActivityContainer) getActivity()).getTreatmentmanagement_gynacologist() != null) {

                        if (((FragmentActivityContainer) getActivity()).getTreatmentmanagement_gynacologist().isVisible()) {
                            ((FragmentActivityContainer) getActivity()).getTreatmentmanagement_gynacologist().onManagement(3);
                        }
                    }

                } else if (((FragmentActivityContainer) getActivity()).getTreatmentmanagement_gynacologist() != null) {

                    if (((FragmentActivityContainer) getActivity()).getTreatmentmanagement_gynacologist().isVisible()) {
                        ((FragmentActivityContainer) getActivity()).getTreatmentmanagement_gynacologist().onManagement(3);
                    }

                }


            }
        });


		/*ivBack.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				((FragmentActivityContainer)getActivity()).backpressedmethod();
			}
		});*/


        management.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub

                btnPrint.setEnabled(false);

                flag = 0;

                if (FragmentActivityContainer.treatmangement_save_check == 0) {
                    management.setBackgroundColor(getResources().getColor(R.color.red_color));
                    createtm.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
//                    viewtm.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
                    management.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));


                    FragmentManager fragmentManager = getFragmentManager();

                    Fragment prescrionmanagement = ((FragmentActivityContainer) getActivity()).getTreatmentmanagement_gynacologist();
                    /*if(((FragmentActivityContainer)getActivity()).gettreatmentcreatefrag()==null) {*/
                    prescrionmanagement = new Prescriptionmanagement_Gynacologist();
                    /*}*/
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.fl_fragment_container, prescrionmanagement);
                    /*fragmentTransaction.addToBackStack(null);*/
                    fragmentTransaction.commit();
                } else {

                    AlertDialog.Builder builder1 = new AlertDialog.Builder(getActivity());
                    builder1.setTitle("SAVE ITEM");
                    builder1.setMessage("Do you want save these items");
                    builder1.setCancelable(true);
                    builder1.setPositiveButton("Yes",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();

                                    btnsubmit.performClick();


                                }
                            });
                    builder1.setNegativeButton("No",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                    FragmentActivityContainer.treatmangement_save_check = 0;
                                    onTreatmentmanagementbtn(0);


                                    management.setBackgroundColor(getResources().getColor(R.color.red_color));
                                    createtm.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
//                                    viewtm.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
//									managementview.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));


                                    FragmentManager fragmentManager = getFragmentManager();

                                    Fragment prescrionmanagement = ((FragmentActivityContainer) getActivity()).getTreatmentmanagement_gynacologist();
                                    prescrionmanagement = new Prescriptionmanagement_Gynacologist();
                                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                                    fragmentTransaction.replace(R.id.fl_fragment_container, prescrionmanagement);
                                    fragmentTransaction.commit();


                                }
                            });

                    AlertDialog alert11 = builder1.create();
                    alert11.show();

                }


            }
        });
        management.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                // TODO Auto-generated method stub

                btnPrint.setEnabled(false);
                btnPrint.setBackgroundDrawable(getResources().getDrawable(R.drawable.submitbtn_background_beforesave));


                flag = 0;

                if (FragmentActivityContainer.treatmangement_save_check == 0) {
                    management.setBackgroundColor(getResources().getColor(R.color.red_color));
//                    management.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
                    createtm.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
//                    viewtm.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));


                    FragmentManager fragmentManager = getFragmentManager();

                    Fragment prescrionmanagement;
					/*if(((FragmentActivityContainer)getActivity()).gettreatmentcreatefrag()==null) {*/
                    prescrionmanagement = new ManagementGynacologist_View();
					/*}*/
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.fl_fragment_container, prescrionmanagement);
					/*fragmentTransaction.addToBackStack(null);*/
                    fragmentTransaction.commit();
                } else {

                    management.setBackgroundColor(getResources().getColor(R.color.red_color));
//                    management.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
                    createtm.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
//                    viewtm.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));


                    AlertDialog.Builder builder1 = new AlertDialog.Builder(getActivity());
                    builder1.setTitle("SAVE ITEM");
                    builder1.setMessage("Do you want save these items");
                    builder1.setCancelable(true);
                    builder1.setPositiveButton("Yes",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {


                                    if (getFragmentManager().findFragmentById(R.id.fl_fragment_container) instanceof Prescriptionmanagement_Gynacologist) {
                                        ((FragmentActivityContainer) getActivity()).getTreatmentmanagement_gynacologist().onManagement(2);
                                    } else if (getFragmentManager().findFragmentById(R.id.fl_fragment_container) instanceof Treatment_Create_Frag) {
                                        ((FragmentActivityContainer) getActivity()).gettreatmentcreatefrag().onTreatment(2);
                                    }


                                    dialog.cancel();

//							btnsubmit.performClick();


                                }
                            });
                    builder1.setNegativeButton("No",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                    FragmentActivityContainer.treatmangement_save_check = 0;
                                    onTreatmentmanagementbtn(0);

                                    management.setBackgroundColor(getResources().getColor(R.color.red_color));
//                                    management.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
                                    createtm.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
//                                    viewtm.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));


                                    FragmentManager fragmentManager = getFragmentManager();

                                    Fragment prescrionmanagement;
							/*if(((FragmentActivityContainer)getActivity()).gettreatmentcreatefrag()==null) {*/
                                    prescrionmanagement = new ManagementGynacologist_View();
							/*}*/
                                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                                    fragmentTransaction.replace(R.id.fl_fragment_container, prescrionmanagement);
							/*fragmentTransaction.addToBackStack(null);*/
                                    fragmentTransaction.commit();


                                }
                            });

                    AlertDialog alert11 = builder1.create();
                    alert11.show();

                }
                return false;

            }
        });

/*
        managementview.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub

                btnPrint.setEnabled(false);
                btnPrint.setBackgroundDrawable(getResources().getDrawable(R.drawable.submitbtn_background_beforesave));


                flag = 0;

                if (FragmentActivityContainer.treatmangement_save_check == 0) {
//					managementview.setBackgroundColor(getResources().getColor(R.color.red_color));
                    management.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
                    createtm.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
                    viewtm.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));


                    FragmentManager fragmentManager = getFragmentManager();

                    Fragment prescrionmanagement;
					*/
/*if(((FragmentActivityContainer)getActivity()).gettreatmentcreatefrag()==null) {*//*

                    prescrionmanagement = new ManagementGynacologist_View();
					*/
/*}*//*

                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.fl_fragment_container, prescrionmanagement);
					*/
/*fragmentTransaction.addToBackStack(null);*//*

                    fragmentTransaction.commit();
                } else {

//					managementview.setBackgroundColor(getResources().getColor(R.color.red_color));
                    management.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
                    createtm.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
                    viewtm.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));


                    AlertDialog.Builder builder1 = new AlertDialog.Builder(getActivity());
                    builder1.setTitle("SAVE ITEM");
                    builder1.setMessage("Do you want save these items");
                    builder1.setCancelable(true);
                    builder1.setPositiveButton("Yes",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {


                                    if (getFragmentManager().findFragmentById(R.id.fl_fragment_container) instanceof Prescriptionmanagement_Gynacologist) {
                                        ((FragmentActivityContainer) getActivity()).getTreatmentmanagement_gynacologist().onManagement(2);
                                    } else if (getFragmentManager().findFragmentById(R.id.fl_fragment_container) instanceof Treatment_Create_Frag) {
                                        ((FragmentActivityContainer) getActivity()).gettreatmentcreatefrag().onTreatment(2);
                                    }


                                    dialog.cancel();

//							btnsubmit.performClick();


                                }
                            });
                    builder1.setNegativeButton("No",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                    FragmentActivityContainer.treatmangement_save_check = 0;
                                    onTreatmentmanagementbtn(0);

//									managementview.setBackgroundColor(getResources().getColor(R.color.red_color));
                                    management.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
                                    createtm.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
                                    viewtm.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));


                                    FragmentManager fragmentManager = getFragmentManager();

                                    Fragment prescrionmanagement;
							*/
/*if(((FragmentActivityContainer)getActivity()).gettreatmentcreatefrag()==null) {*//*

                                    prescrionmanagement = new ManagementGynacologist_View();
							*/
/*}*//*

                                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                                    fragmentTransaction.replace(R.id.fl_fragment_container, prescrionmanagement);
							*/
/*fragmentTransaction.addToBackStack(null);*//*

                                    fragmentTransaction.commit();


                                }
                            });

                    AlertDialog alert11 = builder1.create();
                    alert11.show();

                }


            }
        });
*/

        createtm.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub

				/*if(FragmentActivityContainer.treat_save_check==0){*/

                //	btnPrint.setEnabled(true);
                //btnPrint.setBackgroundDrawable(getResources().getDrawable(R.drawable.submitbtn_background));

                flag = 1;


                if (FragmentActivityContainer.treatmangement_save_check == 0) {
                    if (specialization.equalsIgnoreCase("gynecologist obstetrician")) {
                        management.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
//						managementview.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));

                    }

                    createtm.setBackgroundColor(getResources().getColor(R.color.red_color));
//                    viewtm.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));


                    FragmentManager fragmentManager = getFragmentManager();

                    Fragment createtrat = ((FragmentActivityContainer) getActivity()).gettreatmentcreatefrag();
					/*if(((FragmentActivityContainer)getActivity()).gettreatmentcreatefrag()==null) {*/
                    createtrat = new Treatment_Create_Frag();
					/*}*/
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.fl_fragment_container, createtrat);
					/*fragmentTransaction.addToBackStack(null);*/
                    fragmentTransaction.commit();
                } else {


                    if (specialization.equalsIgnoreCase("gynecologist obstetrician")) {
                        management.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
//						managementview.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));

                    }

                    createtm.setBackgroundColor(getResources().getColor(R.color.red_color));
//                    viewtm.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));


                    AlertDialog.Builder builder1 = new AlertDialog.Builder(getActivity());
                    builder1.setTitle("SAVE ITEM");
                    builder1.setMessage("Do you want save these items");
                    builder1.setCancelable(true);
                    builder1.setPositiveButton("Yes",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();

                                    btnsubmit.performClick();


                                }
                            });
                    builder1.setNegativeButton("No",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                    FragmentActivityContainer.treatmangement_save_check = 0;
                                    onTreatmentmanagementbtn(0);

                                    if (specialization.equalsIgnoreCase("gynecologist obstetrician")) {
                                        management.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
//										managementview.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));

                                    }

                                    createtm.setBackgroundColor(getResources().getColor(R.color.red_color));
//                                    viewtm.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));


                                    FragmentManager fragmentManager = getFragmentManager();

                                    Fragment createtrat = ((FragmentActivityContainer) getActivity()).gettreatmentcreatefrag();
							/*if(((FragmentActivityContainer)getActivity()).gettreatmentcreatefrag()==null) {*/
                                    createtrat = new Treatment_Create_Frag();
							/*}*/
                                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                                    fragmentTransaction.replace(R.id.fl_fragment_container, createtrat);
							/*fragmentTransaction.addToBackStack(null);*/
                                    fragmentTransaction.commit();


                                }
                            });

                    AlertDialog alert11 = builder1.create();
                    alert11.show();

                }
            }
        });
        createtm.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                // TODO Auto-generated method stub

				/*if(FragmentActivityContainer.treat_save_check <=0 ){*/

                if (FragmentActivityContainer.treatmangement_save_check == 0) {

                    btnPrint.setEnabled(true);
                    btnsubmit.setEnabled(false);

                    btnPrint.setBackgroundDrawable(getResources().getDrawable(R.drawable.submitbtn_background));
                    btnPrint.setTextColor(getResources().getColor(R.color.actionbar_color));

                    createtm.setBackgroundColor(getResources().getColor(R.color.red_color));
//                    viewtm.setBackgroundColor(getResources().getColor(R.color.red_color));

                    if (specialization.equalsIgnoreCase("gynecologist obstetrician")) {
                        management.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
//						managementview.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
                    }

                    FragmentManager fragmentManager = getFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    Treatment_View_Frag viewtrat = new Treatment_View_Frag();
                    fragmentTransaction.replace(R.id.fl_fragment_container, viewtrat);
					/*fragmentTransaction.addToBackStack(null);*/
                    fragmentTransaction.commit();

                } else {


                    createtm.setBackgroundColor(getResources().getColor(R.color.red_color));
//                    viewtm.setBackgroundColor(getResources().getColor(R.color.red_color));

                    if (specialization.equalsIgnoreCase("gynecologist obstetrician")) {
                        management.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
//						managementview.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
                    }


                    AlertDialog.Builder builder1 = new AlertDialog.Builder(getActivity());
                    builder1.setTitle("SAVE ITEM");
                    builder1.setMessage("Do you want save these items");
                    builder1.setCancelable(true);
                    builder1.setPositiveButton("Yes",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                    if (getFragmentManager().findFragmentById(R.id.fl_fragment_container) instanceof Prescriptionmanagement_Gynacologist) {
                                        ((FragmentActivityContainer) getActivity()).getTreatmentmanagement_gynacologist().onManagement(4);
                                    }

                                    if (getFragmentManager().findFragmentById(R.id.fl_fragment_container) instanceof Treatment_Create_Frag) {
                                        ((FragmentActivityContainer) getActivity()).gettreatmentcreatefrag().onTreatment(4);
                                    }

                                    dialog.cancel();

                                    //btnsubmit.performClick();


                                }
                            });
                    builder1.setNegativeButton("No",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                    FragmentActivityContainer.treatmangement_save_check = 0;
                                    onTreatmentmanagementbtn(0);

                                    btnPrint.setEnabled(true);
                                    btnsubmit.setEnabled(false);


                                    btnPrint.setBackgroundDrawable(getResources().getDrawable(R.drawable.submitbtn_background));
                                    btnPrint.setTextColor(getResources().getColor(R.color.actionbar_color));

                                    createtm.setBackgroundColor(getResources().getColor(R.color.red_color));
//                                    viewtm.setBackgroundColor(getResources().getColor(R.color.red_color));

                                    if (specialization.equalsIgnoreCase("gynecologist obstetrician")) {
                                        management.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
//										managementview.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
                                    }

                                    FragmentManager fragmentManager = getFragmentManager();
                                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                                    Treatment_View_Frag viewtrat = new Treatment_View_Frag();
                                    fragmentTransaction.replace(R.id.fl_fragment_container, viewtrat);
							/*fragmentTransaction.addToBackStack(null);*/
                                    fragmentTransaction.commit();

                                }
                            });

                    AlertDialog alert11 = builder1.create();
                    alert11.show();

                }
                return false;

            }
        });

/*
        viewtm.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

				*/
/*if(FragmentActivityContainer.treat_save_check <=0 ){*//*


                if (FragmentActivityContainer.treatmangement_save_check == 0) {

                    btnPrint.setEnabled(true);
                    btnsubmit.setEnabled(false);

                    btnPrint.setBackgroundDrawable(getResources().getDrawable(R.drawable.submitbtn_background));
                    btnPrint.setTextColor(getResources().getColor(R.color.actionbar_color));

                    createtm.setBackgroundColor(getResources().getColor(R.color.red_color));
//                    viewtm.setBackgroundColor(getResources().getColor(R.color.red_color));

                    if (specialization.equalsIgnoreCase("gynecologist obstetrician")) {
                        management.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
//						managementview.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
                    }

                    FragmentManager fragmentManager = getFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    Treatment_View_Frag viewtrat = new Treatment_View_Frag();
                    fragmentTransaction.replace(R.id.fl_fragment_container, viewtrat);
					*/
/*fragmentTransaction.addToBackStack(null);*//*

                    fragmentTransaction.commit();

                } else {


                    createtm.setBackgroundColor(getResources().getColor(R.color.red_color));
//                    viewtm.setBackgroundColor(getResources().getColor(R.color.red_color));

                    if (specialization.equalsIgnoreCase("gynecologist obstetrician")) {
                        management.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
//						managementview.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
                    }


                    AlertDialog.Builder builder1 = new AlertDialog.Builder(getActivity());
                    builder1.setTitle("SAVE ITEM");
                    builder1.setMessage("Do you want save these items");
                    builder1.setCancelable(true);
                    builder1.setPositiveButton("Yes",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                    if (getFragmentManager().findFragmentById(R.id.fl_fragment_container) instanceof Prescriptionmanagement_Gynacologist) {
                                        ((FragmentActivityContainer) getActivity()).getTreatmentmanagement_gynacologist().onManagement(4);
                                    }

                                    if (getFragmentManager().findFragmentById(R.id.fl_fragment_container) instanceof Treatment_Create_Frag) {
                                        ((FragmentActivityContainer) getActivity()).gettreatmentcreatefrag().onTreatment(4);
                                    }

                                    dialog.cancel();

                                    //btnsubmit.performClick();


                                }
                            });
                    builder1.setNegativeButton("No",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                    FragmentActivityContainer.treatmangement_save_check = 0;
                                    onTreatmentmanagementbtn(0);

                                    btnPrint.setEnabled(true);
                                    btnsubmit.setEnabled(false);


                                    btnPrint.setBackgroundDrawable(getResources().getDrawable(R.drawable.submitbtn_background));
                                    btnPrint.setTextColor(getResources().getColor(R.color.actionbar_color));

                                    createtm.setBackgroundColor(getResources().getColor(R.color.red_color));
//                                    viewtm.setBackgroundColor(getResources().getColor(R.color.red_color));

                                    if (specialization.equalsIgnoreCase("gynecologist obstetrician")) {
                                        management.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
//										managementview.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
                                    }

                                    FragmentManager fragmentManager = getFragmentManager();
                                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                                    Treatment_View_Frag viewtrat = new Treatment_View_Frag();
                                    fragmentTransaction.replace(R.id.fl_fragment_container, viewtrat);
							*/
/*fragmentTransaction.addToBackStack(null);*//*

                                    fragmentTransaction.commit();

                                }
                            });

                    AlertDialog alert11 = builder1.create();
                    alert11.show();

                }


            }
        });
*/


        btnPrint.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub

                int temp = sp.getInt("template", -1);

                if (temp == -1) {
                    Bundle b = new Bundle();
                    PrintlayoutSelectorDialog psObj = new PrintlayoutSelectorDialog();
                    b.putString("from", "settings");
                    psObj.setArguments(b);
                    psObj.show(getFragmentManager(), "tag");

                } else {

                    if (((FragmentActivityContainer) getActivity()).gettreatmentcreatefrag() != null) {

                        if (((FragmentActivityContainer) getActivity()).gettreatmentcreatefrag().isVisible()) {

                            ((FragmentActivityContainer) getActivity()).gettreatmentcreatefrag().onPrescription();

                        } else if (((FragmentActivityContainer) getActivity()).gettreatmentviewcreatefrag() != null) {

                            if (((FragmentActivityContainer) getActivity()).gettreatmentviewcreatefrag().isVisible()) {
                                ((FragmentActivityContainer) getActivity()).gettreatmentviewcreatefrag().onviewPrescription();
                            }
                        }

                    } else if (((FragmentActivityContainer) getActivity()).gettreatmentviewcreatefrag() != null) {

                        if (((FragmentActivityContainer) getActivity()).gettreatmentviewcreatefrag().isVisible()) {
                            ((FragmentActivityContainer) getActivity()).gettreatmentviewcreatefrag().onviewPrescription();
                        }

                    }

                }
            }
        });


        return view;
    }

    public void setSideItemBg(int item) {

        switch (item) {
            case 1:
                management.setBackgroundColor(getResources().getColor(R.color.red_color));
//				managementview.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
                createtm.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
//                viewtm.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));


                break;
            case 2:
                management.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
//				managementview.setBackgroundColor(getResources().getColor(R.color.red_color));
                createtm.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
//                viewtm.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));

                break;
            case 3:
                if (specialization.equalsIgnoreCase("gynecologist obstetrician")) {
                    management.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
//					managementview.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
                }
                createtm.setBackgroundColor(getResources().getColor(R.color.red_color));
//                viewtm.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));

                break;

            case 4:
                if (specialization.equalsIgnoreCase("gynecologist obstetrician")) {
                    management.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
//					managementview.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
                }
                createtm.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
//                viewtm.setBackgroundColor(getResources().getColor(R.color.red_color));

                break;

            default:
                break;
        }

    }


    public void onTreatmentbtn(int arg) {
        // TODO Auto-generated method stub
        if (Treatment_Frag.this.isVisible()) {
            if (arg == 1) {

                if (FragmentActivityContainer.treat_save_check < 0) {
                    FragmentActivityContainer.treat_save_check = 0;
                }
                btnsubmit.setBackgroundDrawable(getResources().getDrawable(R.drawable.submitbtn_background));
                btnsubmit.setTextColor(getResources().getColor(R.color.actionbar_color));
                btnsubmit.setEnabled(true);


                FragmentActivityContainer.treat_save_check++;
                System.out.println("treat save check value" + FragmentActivityContainer.treat_save_check);
            }
            if (arg == 0) {
                FragmentActivityContainer.treat_save_check--;

                if (FragmentActivityContainer.treat_save_check <= 0) {
                    btnsubmit.setBackgroundDrawable(getResources().getDrawable(R.drawable.submitbtn_background_beforesave));
                    btnsubmit.setTextColor(getResources().getColor(R.color.grey));
                    btnsubmit.setEnabled(false);

                    FragmentActivityContainer.treat_save_check = 0;
                }

                System.out.println("treat save check value" + FragmentActivityContainer.treat_save_check);
            }
        }
    }

    public void onTreatmentmanagementbtn(int arg) {
        // TODO Auto-generated method stub

        if (Treatment_Frag.this.isVisible()) {
            if (arg == 1) {

                if (FragmentActivityContainer.treatmangement_save_check < 0) {
                    FragmentActivityContainer.treatmangement_save_check = 0;
                }
                btnsubmit.setBackgroundDrawable(getResources().getDrawable(R.drawable.submitbtn_background));
                btnsubmit.setTextColor(getResources().getColor(R.color.actionbar_color));
                btnsubmit.setEnabled(true);


                FragmentActivityContainer.treatmangement_save_check++;
                System.out.println("treat save check value" + FragmentActivityContainer.treatmangement_save_check);
            }
            if (arg == 0) {
                FragmentActivityContainer.treatmangement_save_check--;

                if (FragmentActivityContainer.treatmangement_save_check <= 0) {
                    btnsubmit.setBackgroundDrawable(getResources().getDrawable(R.drawable.submitbtn_background_beforesave));
                    btnsubmit.setTextColor(getResources().getColor(R.color.grey));
                    btnsubmit.setEnabled(false);

                    FragmentActivityContainer.treatmangement_save_check = 0;
                }

                System.out.println("treat save check value" + FragmentActivityContainer.treat_save_check);
            }


        }


    }


    public void medicinecolorchange() {

        SharedPreferences sp = getActivity().getSharedPreferences("Login", 0);
        String special = sp.getString("spel", null);

        if (!special.equalsIgnoreCase("gynecologist obstetrician")) {
            management.setVisibility(View.GONE);
//			managementview.setVisibility(View.GONE);

            createtm.setBackgroundColor(getResources().getColor(R.color.red_color));
//            viewtm.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));

        } else {
            management.setVisibility(View.VISIBLE);
//			managementview.setVisibility(View.VISIBLE);

            management.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
//			managementview.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
            createtm.setBackgroundColor(getResources().getColor(R.color.red_color));
//            viewtm.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));

        }


    }

}