package com.wiinnova.doctorsdiary.fragment;


import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.wiinnova.doctorsdiary.R;
import com.wiinnova.doctorsdiary.activities.FragmentActivityContainer;

public class DD_Frag extends Fragment{

	static RelativeLayout accset;
	static RelativeLayout stati;
	static RelativeLayout adminpat;
	EditText firstName,lastName,email,confEmail,createPass,confPass;
	RelativeLayout rlContacts;
	private int sideitem;
	//ImageView btnBack;

	@Override
	public View onCreateView(LayoutInflater inflater,
			ViewGroup container, Bundle savedInstanceState) {

		View myView = inflater.inflate(R.layout.dd_frag, container,false);

		System.out.println("admin frag menu working.....");
		
		((FragmentActivityContainer)getActivity()).enabletabItemvisibility();
		
		((FragmentActivityContainer)getActivity()).admintabcolorchange();
		
		
		
		Bundle bundle=getArguments();
		sideitem=bundle.getInt("sidemenuitem");

		accset=(RelativeLayout)myView.findViewById(R.id.accsett);
		stati=(RelativeLayout)myView.findViewById(R.id.stat);
		adminpat=(RelativeLayout)myView.findViewById(R.id.addpat);
		//btnBack=(ImageView)myView.findViewById(R.id.btnback);
		
		
		if(sideitem==1){
			accset.setBackgroundColor(getResources().getColor(R.color.red_color));
			stati.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
			adminpat.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
		}else if(sideitem==2){
			accset.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
			stati.setBackgroundColor(getResources().getColor(R.color.red_color));
			adminpat.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
		}else if(sideitem==3){
			accset.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
			stati.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
			adminpat.setBackgroundColor(getResources().getColor(R.color.red_color));
		}

		/*
		accset.setBackgroundColor(getResources().getColor(R.color.red_color));
		stati.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
		adminpat.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));*/
		/*
		btnBack.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				((FragmentActivityContainer)getActivity()).backpressedmethod();
			}
		});*/
		

		accset.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				accset.setBackgroundColor(getResources().getColor(R.color.red_color));
				stati.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
				adminpat.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));

				FragmentManager fragmentManager =getFragmentManager();
				FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

				getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);	
				
				DD_Account_Settings_Frag ddsecond = new DD_Account_Settings_Frag();

				fragmentTransaction.replace(R.id.fl_fragment_container, ddsecond);
				/*fragmentTransaction.addToBackStack(null);*/
				fragmentTransaction.commit();

			}
		});



		stati.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);	
				
				accset.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
				stati.setBackgroundColor(getResources().getColor(R.color.red_color));
				adminpat.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));

				FragmentManager fragmentManager =getFragmentManager();
				FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

				Statistics ddstat = new Statistics();

				fragmentTransaction.replace(R.id.fl_fragment_container, ddstat);
				/*fragmentTransaction.addToBackStack(null);*/
				fragmentTransaction.commit();

			}
		});

		adminpat.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				accset.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
				stati.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
				adminpat.setBackgroundColor(getResources().getColor(R.color.red_color));
				
				getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);	

				
				FragmentManager fragmentManager =getFragmentManager();
				FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
				AdminPatients admin_patients = new AdminPatients();
				fragmentTransaction.replace(R.id.fl_fragment_container, admin_patients);
				/*fragmentTransaction.addToBackStack(null);*/
				fragmentTransaction.commit();

			}
		});

		return myView;
	}
}
