package com.wiinnova.doctorsdiary.fragment;


import android.app.Fragment;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.Spinner;
import android.widget.TextView;

import com.wiinnova.doctorsdiary.R;
import com.wiinnova.doctorsdiary.popupwindow.SystemBrightnessPopup;
import com.wiinnova.doctorsdiary.supportclasses.CustomGrid;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class DD_Account_Settings_Frag extends Fragment{

	Button save,cancel;
	TextView flName,currDate;
	EditText fname,lname,email,pass,strname,cityname,pincode,state,phone,supspl,drregno;
	Spinner count,accr,spl,qual;
	String accrd,qualf,sply;
	//	String drFname,drLname;
	TextView doctername;

	ProgressDialog mProgressDialog;
	//	public static ParseObject drdetails;
	String drRegno,fName,lName,eMail,pas,strName,cityName,pinzip,stat,country,phoneno,accredition,specialization,supSpl,qualification,dregNo;

	private Assign_drugs assigndrugs;
	//	 private Assign_Frequent_drugs frequent_drugs;

	/*LinearLayout llabout;
	LinearLayout lladdress;
	LinearLayout llemail;
	LinearLayout llpassword;
	LinearLayout llbrightness;
	LinearLayout llfrequentdrugs;
	LinearLayout llfrequentdisease;
	LinearLayout llpurchaseanalysis;
	LinearLayout llpurchasemedicalcards;
	LinearLayout llprescriptionTemplate;*/

	GridView grid;
	String[] web = {
			"About",
			"Address",
			"E-mail",
			"Password",
			"Brightness",
			"Assign \n Frequent Drugs",
			"Assign \n Frequent Disease",
			"Purchase \n Analysis",
			"Purchase \n Medical Cards",
			"Prescription \n Template",


	} ;
	int[] imageId = {
			R.drawable.icon_account_setting_about,
			R.drawable.icon_account_setting_address,
			R.drawable.icon_account_setting_email,
			R.drawable.icon_account_setting_password,
			R.drawable.icon_account_setting_brightness,
			R.drawable.icon_account_setting_frequentdrugs,
			R.drawable.icon_account_setting_frequent_disease,
			R.drawable.icon_account_setting_purchase_analysis,
			R.drawable.icon_account_setting_purchase_analysis,
			R.drawable.icon_account_setting_prescription_template,


	};
	public DD_Frag ddfirst;
	@Override
	public View onCreateView(LayoutInflater inflater,
			ViewGroup container, Bundle savedInstanceState) {

		
		View myView = inflater.inflate(R.layout.dd_account_settings_frag_new, container,false);

		ddfirst = new  DD_Frag();
		Bundle bundle=new Bundle();
		bundle.putInt("sidemenuitem", 1);
		ddfirst.setArguments(bundle);
		getFragmentManager().beginTransaction()
		.replace(R.id.fl_sidemenu_container, ddfirst)
		.commit();
		
		



		//((Patient_Tab_Activity)getActivity()).getfragmentPagerAdapter().setadminloadflag(0);
		Initialize_components(myView);

		SharedPreferences docter_name=getActivity().getSharedPreferences("Login", 0);
		String docter_first_name=docter_name.getString("fName", null);  
		String docter_last_name=docter_name.getString("lName", null);  

		doctername.setText(docter_first_name+" "+docter_last_name);


		Calendar c = Calendar.getInstance();
		System.out.println("Current time => " + c.getTime());
		SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
		String formattedDate = df.format(c.getTime());
		currDate.setText(formattedDate);

		CustomGrid adapter = new CustomGrid(getActivity(), web, imageId);
		grid=(GridView)myView.findViewById(R.id.grid);
		grid.setAdapter(adapter);


		grid.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View v, int position, long arg3) {
				// TODO Auto-generated method stub

				if(position==9){
/*
					android.support.v4.app.FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
					FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

					Assign_drugs assign_drugs = new Assign_drugs();

					fragmentTransaction.replace(R.id.ddfragment_container1, assign_drugs);
					//fragmentTransaction.addToBackStack("assign_drugs");

					Password_Change passwordchangeObj=new Password_Change();

					fragmentTransaction.replace(R.id.ddfragment_container2, passwordchangeObj);
					fragmentTransaction.addToBackStack("passwordchange");
					fragmentTransaction.commit();
					
					PrescriptionSettings prescriptionsettings=new PrescriptionSettings();

					fragmentTransaction.replace(R.id.ddfragment_container2, prescriptionsettings);
					fragmentTransaction.addToBackStack("prescriptionsettings");
					fragmentTransaction.commit();*/
					
				getActivity().getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);	

				getActivity().getFragmentManager().beginTransaction().replace(R.id.fl_fragment_container, new PrescriptionSettings()).addToBackStack(null).commit();
					
					

				}
				if(position==4){
					SystemBrightnessPopup  sbpObj=new SystemBrightnessPopup();
					sbpObj.show(getFragmentManager(), "tag");
					/*sbpObj.setCancelable(false);
					if ( sbpObj.getDialog() != null )
						sbpObj.getDialog().setCanceledOnTouchOutside(true);*/
				}

				/*	if(position==5){
					android.support.v4.app.FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
					FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

					Assign_drugs_main drugs_main = new Assign_drugs_main();

					fragmentTransaction.replace(R.id.ddfragment_container2, drugs_main);
					fragmentTransaction.addToBackStack("drugs_main");

					Assign_drugs assign_drugs = new Assign_drugs();

					fragmentTransaction.replace(R.id.ddfragment_container1, assign_drugs);
					//fragmentTransaction.addToBackStack("assign_drugs");
					fragmentTransaction.commit();
				}
				 */
			}
		});
		/*	llabout=(LinearLayout)myView.findViewById(R.id.llAbout);
		lladdress=(LinearLayout)myView.findViewById(R.id.llAddress);
		llemail=(LinearLayout)myView.findViewById(R.id.llEmail);
		llpassword=(LinearLayout)myView.findViewById(R.id.llPassword);
		llbrightness=(LinearLayout)myView.findViewById(R.id.llBrightness);
		llfrequentdrugs=(LinearLayout)myView.findViewById(R.id.llFrequentDrugs);
		llfrequentdisease=(LinearLayout)myView.findViewById(R.id.llFrequentDiseas);
		llpurchaseanalysis=(LinearLayout)myView.findViewById(R.id.llPurchaseAnalysis);
		llpurchasemedicalcards=(LinearLayout)myView.findViewById(R.id.llPurchaseCards);
		llprescriptionTemplate=(LinearLayout)myView.findViewById(R.id.llPrescriptionTemplate);




		llabout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				llabout.setBackgroundColor(getResources().getColor(R.color.red_color));
				lladdress.setBackgroundColor(getResources().getColor(R.color.actionbar_color));
				llemail.setBackgroundColor(getResources().getColor(R.color.actionbar_color));
				llpassword.setBackgroundColor(getResources().getColor(R.color.actionbar_color));
				llbrightness.setBackgroundColor(getResources().getColor(R.color.actionbar_color));
				llfrequentdrugs.setBackgroundColor(getResources().getColor(R.color.actionbar_color));
				llfrequentdisease.setBackgroundColor(getResources().getColor(R.color.actionbar_color));
				llpurchaseanalysis.setBackgroundColor(getResources().getColor(R.color.actionbar_color));
				llpurchasemedicalcards.setBackgroundColor(getResources().getColor(R.color.actionbar_color));
				llprescriptionTemplate.setBackgroundColor(getResources().getColor(R.color.actionbar_color));



			}
		});

		lladdress.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				llabout.setBackgroundColor(getResources().getColor(R.color.actionbar_color));
				lladdress.setBackgroundColor(getResources().getColor(R.color.red_color));
				llemail.setBackgroundColor(getResources().getColor(R.color.actionbar_color));
				llpassword.setBackgroundColor(getResources().getColor(R.color.actionbar_color));
				llbrightness.setBackgroundColor(getResources().getColor(R.color.actionbar_color));
				llfrequentdrugs.setBackgroundColor(getResources().getColor(R.color.actionbar_color));
				llfrequentdisease.setBackgroundColor(getResources().getColor(R.color.actionbar_color));
				llpurchaseanalysis.setBackgroundColor(getResources().getColor(R.color.actionbar_color));
				llpurchasemedicalcards.setBackgroundColor(getResources().getColor(R.color.actionbar_color));
				llprescriptionTemplate.setBackgroundColor(getResources().getColor(R.color.actionbar_color));



			}
		});

		llemail.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				llabout.setBackgroundColor(getResources().getColor(R.color.actionbar_color));
				lladdress.setBackgroundColor(getResources().getColor(R.color.actionbar_color));
				llemail.setBackgroundColor(getResources().getColor(R.color.red_color));
				llpassword.setBackgroundColor(getResources().getColor(R.color.actionbar_color));
				llbrightness.setBackgroundColor(getResources().getColor(R.color.actionbar_color));
				llfrequentdrugs.setBackgroundColor(getResources().getColor(R.color.actionbar_color));
				llfrequentdisease.setBackgroundColor(getResources().getColor(R.color.actionbar_color));
				llpurchaseanalysis.setBackgroundColor(getResources().getColor(R.color.actionbar_color));
				llpurchasemedicalcards.setBackgroundColor(getResources().getColor(R.color.actionbar_color));
				llprescriptionTemplate.setBackgroundColor(getResources().getColor(R.color.actionbar_color));



			}
		});


		llpassword.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				llabout.setBackgroundColor(getResources().getColor(R.color.actionbar_color));
				lladdress.setBackgroundColor(getResources().getColor(R.color.actionbar_color));
				llemail.setBackgroundColor(getResources().getColor(R.color.actionbar_color));
				llpassword.setBackgroundColor(getResources().getColor(R.color.red_color));
				llbrightness.setBackgroundColor(getResources().getColor(R.color.actionbar_color));
				llfrequentdrugs.setBackgroundColor(getResources().getColor(R.color.actionbar_color));
				llfrequentdisease.setBackgroundColor(getResources().getColor(R.color.actionbar_color));
				llpurchaseanalysis.setBackgroundColor(getResources().getColor(R.color.actionbar_color));
				llpurchasemedicalcards.setBackgroundColor(getResources().getColor(R.color.actionbar_color));
				llprescriptionTemplate.setBackgroundColor(getResources().getColor(R.color.actionbar_color));



			}
		});




		llbrightness.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				llabout.setBackgroundColor(getResources().getColor(R.color.actionbar_color));
				lladdress.setBackgroundColor(getResources().getColor(R.color.actionbar_color));
				llemail.setBackgroundColor(getResources().getColor(R.color.actionbar_color));
				llpassword.setBackgroundColor(getResources().getColor(R.color.actionbar_color));
				llbrightness.setBackgroundColor(getResources().getColor(R.color.red_color));
				llfrequentdrugs.setBackgroundColor(getResources().getColor(R.color.actionbar_color));
				llfrequentdisease.setBackgroundColor(getResources().getColor(R.color.actionbar_color));
				llpurchaseanalysis.setBackgroundColor(getResources().getColor(R.color.actionbar_color));
				llpurchasemedicalcards.setBackgroundColor(getResources().getColor(R.color.actionbar_color));
				llprescriptionTemplate.setBackgroundColor(getResources().getColor(R.color.actionbar_color));



			}
		});
		llfrequentdrugs.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				llabout.setBackgroundColor(getResources().getColor(R.color.actionbar_color));
				lladdress.setBackgroundColor(getResources().getColor(R.color.actionbar_color));
				llemail.setBackgroundColor(getResources().getColor(R.color.actionbar_color));
				llpassword.setBackgroundColor(getResources().getColor(R.color.actionbar_color));
				llbrightness.setBackgroundColor(getResources().getColor(R.color.actionbar_color));
				llfrequentdrugs.setBackgroundColor(getResources().getColor(R.color.red_color));
				llfrequentdisease.setBackgroundColor(getResources().getColor(R.color.actionbar_color));
				llpurchaseanalysis.setBackgroundColor(getResources().getColor(R.color.actionbar_color));
				llpurchasemedicalcards.setBackgroundColor(getResources().getColor(R.color.actionbar_color));
				llprescriptionTemplate.setBackgroundColor(getResources().getColor(R.color.actionbar_color));



				android.support.v4.app.FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
				FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

				Assign_drugs_main drugs_main = new Assign_drugs_main();

				fragmentTransaction.replace(R.id.ddfragment_container2, drugs_main);
				fragmentTransaction.addToBackStack("drugs_main");

				Assign_drugs assign_drugs = new Assign_drugs();

				fragmentTransaction.replace(R.id.ddfragment_container1, assign_drugs);
				fragmentTransaction.addToBackStack("assign_drugs");
				fragmentTransaction.commit();




			}
		});

		llfrequentdisease.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				llabout.setBackgroundColor(getResources().getColor(R.color.actionbar_color));
				lladdress.setBackgroundColor(getResources().getColor(R.color.actionbar_color));
				llemail.setBackgroundColor(getResources().getColor(R.color.actionbar_color));
				llpassword.setBackgroundColor(getResources().getColor(R.color.actionbar_color));
				llbrightness.setBackgroundColor(getResources().getColor(R.color.actionbar_color));
				llfrequentdrugs.setBackgroundColor(getResources().getColor(R.color.actionbar_color));
				llfrequentdisease.setBackgroundColor(getResources().getColor(R.color.red_color));
				llpurchaseanalysis.setBackgroundColor(getResources().getColor(R.color.actionbar_color));
				llpurchasemedicalcards.setBackgroundColor(getResources().getColor(R.color.actionbar_color));
				llprescriptionTemplate.setBackgroundColor(getResources().getColor(R.color.actionbar_color));



			}
		});

		llpurchaseanalysis.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				llabout.setBackgroundColor(getResources().getColor(R.color.actionbar_color));
				lladdress.setBackgroundColor(getResources().getColor(R.color.actionbar_color));
				llemail.setBackgroundColor(getResources().getColor(R.color.actionbar_color));
				llpassword.setBackgroundColor(getResources().getColor(R.color.actionbar_color));
				llbrightness.setBackgroundColor(getResources().getColor(R.color.actionbar_color));
				llfrequentdrugs.setBackgroundColor(getResources().getColor(R.color.actionbar_color));
				llfrequentdisease.setBackgroundColor(getResources().getColor(R.color.actionbar_color));
				llpurchaseanalysis.setBackgroundColor(getResources().getColor(R.color.red_color));
				llpurchasemedicalcards.setBackgroundColor(getResources().getColor(R.color.actionbar_color));
				llprescriptionTemplate.setBackgroundColor(getResources().getColor(R.color.actionbar_color));



			}
		});


		llpurchasemedicalcards.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				llabout.setBackgroundColor(getResources().getColor(R.color.actionbar_color));
				lladdress.setBackgroundColor(getResources().getColor(R.color.actionbar_color));
				llemail.setBackgroundColor(getResources().getColor(R.color.actionbar_color));
				llpassword.setBackgroundColor(getResources().getColor(R.color.actionbar_color));
				llbrightness.setBackgroundColor(getResources().getColor(R.color.actionbar_color));
				llfrequentdrugs.setBackgroundColor(getResources().getColor(R.color.actionbar_color));
				llfrequentdisease.setBackgroundColor(getResources().getColor(R.color.actionbar_color));
				llpurchaseanalysis.setBackgroundColor(getResources().getColor(R.color.actionbar_color));
				llpurchasemedicalcards.setBackgroundColor(getResources().getColor(R.color.red_color));
				llprescriptionTemplate.setBackgroundColor(getResources().getColor(R.color.actionbar_color));



			}
		});

		llprescriptionTemplate.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				llabout.setBackgroundColor(getResources().getColor(R.color.actionbar_color));
				lladdress.setBackgroundColor(getResources().getColor(R.color.actionbar_color));
				llemail.setBackgroundColor(getResources().getColor(R.color.actionbar_color));
				llpassword.setBackgroundColor(getResources().getColor(R.color.actionbar_color));
				llbrightness.setBackgroundColor(getResources().getColor(R.color.actionbar_color));
				llfrequentdrugs.setBackgroundColor(getResources().getColor(R.color.actionbar_color));
				llfrequentdisease.setBackgroundColor(getResources().getColor(R.color.actionbar_color));
				llpurchaseanalysis.setBackgroundColor(getResources().getColor(R.color.actionbar_color));
				llpurchasemedicalcards.setBackgroundColor(getResources().getColor(R.color.actionbar_color));
				llprescriptionTemplate.setBackgroundColor(getResources().getColor(R.color.red_color));



			}
		});*/
		/*  flName=(TextView)myView.findViewById(R.id.ddfrtandlastname);
	   currDate=(TextView)myView.findViewById(R.id.ddcurrdate);

	   //Button

	   save=(Button)myView.findViewById(R.id.ddsave);
	   cancel=(Button)myView.findViewById(R.id.ddclear);

	   //Edittext

	   fname=(EditText)myView.findViewById(R.id.ddfnm);
	   lname=(EditText)myView.findViewById(R.id.ddlnm);
	   email=(EditText)myView.findViewById(R.id.ddml);
	   pass=(EditText)myView.findViewById(R.id.ddpss);
	   strname=(EditText)myView.findViewById(R.id.ddstrname);
	   cityname=(EditText)myView.findViewById(R.id.ddcityname);
	   pincode=(EditText)myView.findViewById(R.id.ddpincode);
	   state=(EditText)myView.findViewById(R.id.ddstate);
	   count=(Spinner)myView.findViewById(R.id.ddcountry);
	   phone=(EditText)myView.findViewById(R.id.ddphone);
	   accr=(Spinner)myView.findViewById(R.id.ddaccred);
	   spl=(Spinner)myView.findViewById(R.id.ddsplz);
	   supspl=(EditText)myView.findViewById(R.id.ddsupsplz);
	   qual=(Spinner)myView.findViewById(R.id.ddqualf);
	   drregno=(EditText)myView.findViewById(R.id.ddregno);



	   fname.setEnabled(false);
	   lname.setEnabled(false);
	   email.setEnabled(false);
	   pass.setEnabled(false);
	   strname.setEnabled(false);
	   cityname.setEnabled(false);
	   pincode.setEnabled(false);
	   state.setEnabled(false);
	   count.setEnabled(false);
	   phone.setEnabled(false);
	   accr.setEnabled(false);
	   spl.setEnabled(false);
	   supspl.setEnabled(false);
	   qual.setEnabled(false);
	   drregno.setEnabled(false);



	   SharedPreferences sp1=getActivity().getSharedPreferences("Login", 0);

	   String unm=sp1.getString("Unm", null);       
	   String drflname = sp1.getString("drFLname", null);


	   if(unm!=null){


		   drRegno=unm;


		   fname.setText(sp1.getString("fName", null));
		   lname.setText(sp1.getString("lName", null));
		   email.setText(sp1.getString("mailId", null));
		   pass.setText(sp1.getString("Psw", null));
		   strname.setText(sp1.getString("Street", null));
		   cityname.setText(sp1.getString("cityName", null));
		   pincode.setText(sp1.getString("pinCode", null));
		   state.setText(sp1.getString("State", null));
		   String country=sp1.getString("Country", null);
		   phone.setText(sp1.getString("phNo", null));
		    accrd=sp1.getString("Accredi", null);
		   sply=sp1.getString("spel", null);
		   supspl.setText(sp1.getString("SuperSpl", null));
		   qualf=sp1.getString("qual", null);
		   drregno.setText(sp1.getString("Unm", null));

		   ArrayAdapter<CharSequence> adapter= ArrayAdapter.createFromResource(getActivity(), R.array.countries_array, android.R.layout.simple_spinner_item);
		    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		    count.setAdapter(adapter);
		    if (!country.equals(null)) {
		        int spinnerPostion = adapter.getPosition(country);
		        count.setSelection(spinnerPostion);
		        spinnerPostion = 0;
		    }




		   flName.setText(drflname);

			Log.e("splash", "finish");

	   }

	   Calendar c = Calendar.getInstance();
   	System.out.println("Current time => " + c.getTime());
   	SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
   	String formattedDate = df.format(c.getTime());
   	currDate.setText(formattedDate);

    ParseQuery<ParseObject> query = new ParseQuery<ParseObject>("SystemKeys");

 		query.findInBackground(new FindCallback<ParseObject>() {


 			@Override
 			public void done(List<ParseObject> objects, ParseException e) {
 				// TODO Auto-generated method stub
 				//mProgressDialog.dismiss();
 				Log.e("Login", "done");
 				if(e==null){ 

 					for (int i = 0; i < objects.size(); i++) {
 						ParseObject object = objects.get(i);
 	                    String key =  object.getString("key");

 	                    String values=object.getString("value");
 	                    String[] value = values.split(",");
 	                    Log.e("parse value",String.valueOf(value[0]) );
 	                    if(key.equals("accredition"))
 	                    {
 	                    	String[]  myaccrArray = null;
 	                    	if(value!=null){
 		                    	myaccrArray = new String[value.length];
 	    	                    for(int j = 0; j < value.length; j++){    
 		                        	myaccrArray[j]=value[j];
 		                        	Log.v("result------value",  myaccrArray[j]);
 		                        	ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item,myaccrArray ); //selected item will look like a spinner set from XML
 		                        	spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
 		                        	accr.setAdapter(spinnerArrayAdapter);



 	    	                    }

 	                    	}


 	                    	ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item,myaccrArray );
 	           		    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
 	           		accr.setAdapter(adapter);
 	           		    if (!accrd.equals(null)) {
 	           		        int spinnerPostion = adapter.getPosition(accrd);
 	           		    accr.setSelection(spinnerPostion);
 	           		        spinnerPostion = 0;
 	           		    }
 	                    }

 	                    if(key.equals("qualifications"))
 	                    {
 	                    	String[]  myqulrArray = null;
 		                    if(value!=null){
 		                    	myqulrArray = new String[value.length];
 		                    	for(int j = 0; j < value.length; j++){    
 		                    		myqulrArray[j]=value[j];
 		                    		Log.v("result------value", myqulrArray[j]);
 		                    		ArrayAdapter<String> spinnerArrayAdapter1 = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item,myqulrArray ); //selected item will look like a spinner set from XML
 		                    		spinnerArrayAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
 		                    		qual.setAdapter(spinnerArrayAdapter1);

 		                    	}
 		                    }

 		                   ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item,myqulrArray );
 	 	           		    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
 	 	           		qual.setAdapter(adapter);
 	 	           		    if (!qualf.equals(null)) {
 	 	           		        int spinnerPostion = adapter.getPosition(qualf);
 	 	           		   qual.setSelection(spinnerPostion);
 	 	           		        spinnerPostion = 0;
 	 	           		    }

 	                    }

 	                    if(key.equals("specialization"))
 	                    {
 	                    	String[]  mysplrArray = null;
 		                    if(value!=null){
 		                    	mysplrArray = new String[value.length];
 	    	                    for(int j = 0; j < value.length; j++){    
 	    	                    	mysplrArray[j]=value[j];
 	    	                    	Log.v("result------value", mysplrArray[j]);
 	    	                    	ArrayAdapter<String> spinnerArrayAdapter2 = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item,mysplrArray ); //selected item will look like a spinner set from XML
 	    	                    	spinnerArrayAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
 	    	                    	spl.setAdapter(spinnerArrayAdapter2);
 	    	                    }
 		                    }
 		                   ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item,mysplrArray );
	 	           		    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
	 	           		spl.setAdapter(adapter);
	 	           		    if (!sply.equals(null)) {
	 	           		        int spinnerPostion = adapter.getPosition(sply);
	 	           		    spl.setSelection(spinnerPostion);
	 	           		        spinnerPostion = 0;
	 	           		    }
 	                    }

 						else{
 							Log.e("Login", "exception");
 							//e.printStackTrace();
 						}

 					}

 				}
 			}});


   	cancel.setOnClickListener(new OnClickListener() {

		@Override
		public void onClick(View arg0) {
			// TODO Auto-generated method stub

			getActivity().finish();

		}
	});


	save.setOnClickListener(new OnClickListener() {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub

			String butt=save.getText().toString();
			if(butt.equals("Edit")){

				save.setText("Save");

				   fname.setEnabled(true);
				   lname.setEnabled(true);
				   email.setEnabled(true);
				   pass.setEnabled(true);
				   strname.setEnabled(true);
				   cityname.setEnabled(true);
				   pincode.setEnabled(true);
				   state.setEnabled(true);
				   count.setEnabled(true);
				   phone.setEnabled(true);
				   accr.setEnabled(true);
				   spl.setEnabled(true);
				   supspl.setEnabled(true);
				   qual.setEnabled(true);
				   drregno.setEnabled(true);

			}

			else{


			 fName=fname.getText().toString().trim();
			 lName=lname.getText().toString().trim();
			 eMail=email.getText().toString().trim();
			 pas=pass.getText().toString().trim();
			 strName=strname.getText().toString().trim();
			 cityName=cityname.getText().toString().trim();
			 pinzip=pincode.getText().toString().trim();
			 stat=state.getText().toString().trim();
			 country=count.getSelectedItem().toString();
			 phoneno=phone.getText().toString().trim();
			 accredition=accr.getSelectedItem().toString();;
			 specialization=spl.getSelectedItem().toString();
			 supSpl=supspl.getText().toString().trim();
			 qualification=qual.getSelectedItem().toString();
			 dregNo=drregno.getText().toString().trim();


				mProgressDialog = new ProgressDialog(getActivity());

				//mProgressDialog.setTitle("Logging In....");

				mProgressDialog.setMessage("Saving...");
				mProgressDialog.show();
				ParseQuery<ParseObject> query = ParseQuery.getQuery("UserMaster");
				query.whereEqualTo("docRegistrationNumber",drRegno);

				query.findInBackground(new FindCallback<ParseObject>() {

					@Override
					public void done(List<ParseObject> objects, ParseException e) {
						//mProgressDialog.dismiss();
						// TODO Auto-generated method stub
						if (e == null) {
							if (objects.size() > 0) {
									// query found a user
									Log.e("Login", "Id exist");
									ParseObject drdetails=objects.get(0);
									//Toast.makeText(Enter_Old_Patient_Id.this,"Login Successful",Toast.LENGTH_SHORT).show();



									drdetails.put("accredition", accredition);
									drdetails.put("city", cityName);
									drdetails.put("country", country);
									drdetails.put("docRegistrationNumber", dregNo);
									drdetails.put("email", eMail);
									drdetails.put("firstName", fName);
									drdetails.put("lastName", lName);
									drdetails.put("password", pas);
									drdetails.put("phone", phoneno);
									drdetails.put("pincode", pinzip);
									drdetails.put("qualification", qualification);
									drdetails.put("specialization", specialization);
									drdetails.put("state", stat);
									drdetails.put("street", strName);
									drdetails.put("superSpecialization", supSpl);


									drdetails.saveInBackground(new SaveCallback() {
										public void done(ParseException e) {
											mProgressDialog.dismiss();
											if (e == null) {
												myObjectSavedSuccessfully();
											} else {
												myObjectSaveDidNotSucceed();
											}
										}

										private void myObjectSaveDidNotSucceed() {
											// TODO Auto-generated method stub
											Toast msg1=Toast.makeText(getActivity(), "Registration Failed", Toast.LENGTH_LONG);
											msg1.show();

										}

										private void myObjectSavedSuccessfully() {
											// TODO Auto-generated method stub
											Toast msg=Toast.makeText(getActivity(), "Successfully Completed", Toast.LENGTH_LONG);
											msg.show();

											save.setText("Edit");

											   fname.setEnabled(false);
											   lname.setEnabled(false);
											   email.setEnabled(false);
											   pass.setEnabled(false);
											   strname.setEnabled(false);
											   cityname.setEnabled(false);
											   pincode.setEnabled(false);
											   state.setEnabled(false);
											   count.setEnabled(false);
											   phone.setEnabled(false);
											   accr.setEnabled(false);
											   spl.setEnabled(false);
											   supspl.setEnabled(false);
											   qual.setEnabled(false);
											   drregno.setEnabled(false);


											   String drfnln=fName+" "+lName;

												SharedPreferences sp=getActivity().getSharedPreferences("Login", 0);
												SharedPreferences.Editor Ed=sp.edit();
												Ed.putString("Unm",dregNo );              
												Ed.putString("Psw",pas); 
												Ed.putString("drFLname",drfnln);
												Ed.putString("fName",fName);
												Ed.putString("lName",lName);
												Ed.putString("Accredi",accredition);
												Ed.putString("cityName",cityName);
												Ed.putString("Country",country);
												Ed.putString("mailId",eMail);
												Ed.putString("phNo",phoneno);
												Ed.putString("pinCode",pinzip);
												Ed.putString("qual",qualification);
												Ed.putString("spel",specialization);
												Ed.putString("State",stat);
												Ed.putString("Street",strName);
												Ed.putString("SuperSpl",supSpl);
												Ed.commit();



										}
									});


						        }
						        else {
						        	//Log.e("Login", "Logged innnnnnnn faileeeeeed");
						        	Toast msg=Toast.makeText(getActivity(), "Invalid Reg No", Toast.LENGTH_LONG);
						        	msg.show();

								}
						 } 
						 else {
							 Toast.makeText(getActivity(), "Sorry, something went wrong", Toast.LENGTH_LONG).show();
						 }

					}
				});


			}

		}
	});


		 */


		return myView;
	}
	private void Initialize_components(View myView) {
		// TODO Auto-generated method stub
		doctername=(TextView)myView.findViewById(R.id.tvdoctername);
		currDate=(TextView)myView.findViewById(R.id.tvDate);
	}
	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		CrashReporter.getInstance().trackScreenView("Account Setting");
	}
}