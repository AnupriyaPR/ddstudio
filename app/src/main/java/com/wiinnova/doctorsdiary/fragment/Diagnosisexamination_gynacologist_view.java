package com.wiinnova.doctorsdiary.fragment;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.wiinnova.doctorsdiary.R;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.wiinnova.doctorsdiary.activities.FragmentActivityContainer;
import com.wiinnova.doctorsdiary.activities.Home_Page_Activity;
import com.wiinnova.doctorsdiary.supportclasses.AppUtil;
import com.wiinnova.doctorsdiary.supportclasses.ArrayAdapterDiagnosisView;
import com.wiinnova.doctorsdiary.supportclasses.LMPAdapterDiagnosis;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

//import static com.wiinnova.doctorsdiary.fragment.Diagnosisexamination_Gynacologist.lmpdate_array;
//import static com.wiinnova.doctorsdiary.fragment.Diagnosisexamination_Gynacologist.lmpflow_array;
//import static com.wiinnova.doctorsdiary.fragment.Diagnosisexamination_Gynacologist.lmpdysmenorrhea_array;
//import static com.wiinnova.doctorsdiary.fragment.Diagnosisexamination_Gynacologist.menstrualregularlist;
//import static com.wiinnova.doctorsdiary.fragment.Diagnosisexamination_Gynacologist.menstrualdayslist;
//import static com.wiinnova.doctorsdiary.fragment.Diagnosisexamination_Gynacologist.menstrualcyclelist;


public class Diagnosisexamination_gynacologist_view extends Fragment {

    ListView lstDiagnosisView;
    TextView dflName, duniqueID;
    String puid, filan;
    ProgressDialog mProgressDialog;
    ParseObject patient;
    int skip = 0;
    List<ParseObject> object;
    ArrayAdapterDiagnosisView lmpAdapter;
    LinearLayout layoutcontainer;

    private Diagnosis_Frag_Gynacologist diagfrag;

    public void justifyListViewHeightBasedOnChildren(ListView listView) {

        ListAdapter adapter = listView.getAdapter();

        if (adapter == null) {
            return;
        }
        ViewGroup vg = listView;
        int totalHeight = 0;
        for (int i = 0; i < adapter.getCount(); i++) {
            View listItem = adapter.getView(i, null, vg);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }

        ViewGroup.LayoutParams par = listView.getLayoutParams();
        par.height = totalHeight + (listView.getDividerHeight() * (adapter.getCount() - 1));
        listView.setLayoutParams(par);
        listView.requestLayout();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        View view = inflater.inflate(R.layout.diagnosis_examination_view, null);

        dflName = (TextView) view.findViewById(R.id.tvfirstlastName);
        duniqueID = (TextView) view.findViewById(R.id.cduniqueid);
        layoutcontainer = (LinearLayout) view.findViewById(R.id.diadynamic);

        /////////////////////////Listview for result
        lstDiagnosisView = (ListView) view.findViewById(R.id.lstDiagnosisView);
        /////////////////////////////////////////////////////

        if (getFragmentManager().findFragmentById(R.id.fl_sidemenu_container) instanceof Diagnosis_Frag_Gynacologist) {

        } else {
            diagfrag = new Diagnosis_Frag_Gynacologist();
            getFragmentManager().beginTransaction()
                    .replace(R.id.fl_sidemenu_container, diagfrag)
                    .commit();
        }
        if (diagfrag != null) {
            ((FragmentActivityContainer) getActivity()).setDiagfrag_gynacologist(diagfrag);
        }


        if (Home_Page_Activity.patient != null) {
            puid = Home_Page_Activity.patient.getString("patientID");
            String fname = Home_Page_Activity.patient.getString("firstName");
            String lname = Home_Page_Activity.patient.getString("lastName");
            System.out.println("patient id1,,,,,," + puid);
            if (fname == null) {
                fname = "FirstName";
            }
            if (lname == null) {
                lname = "LastName";
            }
            dflName.setText(fname + " " + lname);

        } else {

            SharedPreferences scanpidnew = getActivity().getSharedPreferences("NewPatID", 0);
            String scnewPid = scanpidnew.getString("NewID", null);
            SharedPreferences scanpid1 = getActivity().getSharedPreferences("ScannedPid", 0);
            String scPid = scanpid1.getString("scanpid", null);
            if (scPid != null) {
                puid = scPid;
                SharedPreferences fnname1 = getActivity().getSharedPreferences("fnName", 0);
                dflName.setText(fnname1.getString("fNname", "FirstName" + " " + "LastName"));
                System.out.println("name diag view.." + fnname1.getString("fNname", "FirstName" + " " + "LastName"));
            }

            if (scnewPid != null) {

                puid = scnewPid;


                SharedPreferences fnname1 = getActivity().getSharedPreferences("fnName", 0);
                dflName.setText(fnname1.getString("fNname", "FirstName" + " " + "LastName"));
                System.out.println("name diag view.." + fnname1.getString("fNname", "FirstName" + " " + "LastName"));
            }

        }

        duniqueID.setText(puid);
        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setCancelable(false);
        mProgressDialog.setMessage("Retrieving Examination Data...");
        mProgressDialog.show();
        System.out.println("object taken from main frag not working");


        fetchingdata_from_server();

        return view;
    }

    /*//////////////////////////////////list LMP on Listview---calling from ArrayAdapterDiagnosisView
    public void viewLmp(ParseObject patientobject, ListView lstMenstrualPeriod) {
        ArrayList<Integer> listCount = new ArrayList<Integer>();
        try {
            JSONArray arrayLmp = new JSONArray();
            lmpdate_array.clear();
            arrayLmp = patientobject.getJSONArray("Lmp_date");
            for (int j = 0; j < arrayLmp.length(); j++) {
                lmpdate_array.add(arrayLmp.get(j).toString());
                Log.d("DATESLOcal", lmpdate_array.toString() + " ghf");
            }
            JSONArray arrayFlow = new JSONArray();
            arrayFlow = patientobject.getJSONArray("Lmp_flow");
            lmpflow_array.clear();
            for (int j = 0; j < arrayFlow.length(); j++) {
                lmpflow_array.add(arrayFlow.get(j).toString());
            }
            JSONArray arrayDys = new JSONArray();
            arrayDys = patientobject.getJSONArray("Lmp_dysmenorrhea");
            lmpdysmenorrhea_array.clear();
            for (int j = 0; j < arrayDys.length(); j++) {
                lmpdysmenorrhea_array.add(arrayDys.get(j).toString());
            }
            JSONArray arrayReg = new JSONArray();
            menstrualregularlist.clear();
            arrayReg = patientobject.getJSONArray("Menstrual_Regular_Irregular");
            for (int j = 0; j < arrayReg.length(); j++) {
                menstrualregularlist.add(arrayReg.get(j).toString());
            }
            JSONArray arrayDays = new JSONArray();
            arrayDays = patientobject.getJSONArray("Menstrual_Days");
            menstrualdayslist.clear();
            for (int j = 0; j < arrayDays.length(); j++) {
                menstrualdayslist.add(arrayDays.get(j).toString());
            }
            JSONArray arrayCycle = new JSONArray();
            arrayCycle = patientobject.getJSONArray("Menstrual_Cycle");
            menstrualcyclelist.clear();
            for (int j = 0; j < arrayCycle.length(); j++) {
                menstrualcyclelist.add(arrayCycle.get(j).toString());
                listCount.add(0);
            }
        } catch (JSONException ex) {
            ex.printStackTrace();
        } catch (NullPointerException ex) {
            ex.printStackTrace();
        }


        LMPAdapterDiagnosis lmp = new LMPAdapterDiagnosis(getActivity(), listCount, null, lmpdate_array);
        lstMenstrualPeriod.setAdapter(lmp);
        justifyListViewHeightBasedOnChildren(lstMenstrualPeriod);
        lstMenstrualPeriod.setEnabled(false);
    }*/

    private void fetchingdata_from_server() {
        // TODO Auto-generated method stub

        final ParseQuery<ParseObject> query = ParseQuery.getQuery("Diagnosis");
        if (!AppUtil.haveNetwokConnection(getActivity()))
            query.fromLocalDatastore();
        query.whereEqualTo("patientID", puid);
        query.whereEqualTo("typeFlag", 1);
        query.addDescendingOrder("createdDate");


        mProgressDialog.setOnCancelListener(new OnCancelListener() {

            @Override
            public void onCancel(DialogInterface dialog) {
                // TODO Auto-generated method stub
                query.cancel();
            }
        });


        query.findInBackground(new FindCallback<ParseObject>() {


            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                // TODO Auto-generated method stub
                object = objects;
                if (Diagnosisexamination_gynacologist_view.this.isVisible()) {

                    if (e == null) {

                        /////////////////////////Call arrayadapter

                        lmpAdapter = new ArrayAdapterDiagnosisView(getActivity(), objects, Diagnosisexamination_gynacologist_view.this);
                        lstDiagnosisView.setAdapter(lmpAdapter);


                    }
                }

                mProgressDialog.hide();
            }


        });
    }
}
