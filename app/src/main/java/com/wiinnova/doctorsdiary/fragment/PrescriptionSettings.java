package com.wiinnova.doctorsdiary.fragment;

import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.wiinnova.doctorsdiary.R;
import com.wiinnova.doctorsdiary.popupwindow.PrintlayoutSelectorDialog;
//import com.wiinnova.doctorsdiary.popupwindow.PrintlayoutSelectorDialog;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class PrescriptionSettings extends Fragment{

	private EditText etHeader;
	private EditText etFooter;
	private EditText etRight;
	private EditText etLeft;
	private EditText etTitlefont;
	private EditText etContentfont;
	private Button btSubmit;
	private TextView doctername;
	private TextView tvDate;
	private LinearLayout llfirst;
	private LinearLayout llthird;
	private Button btnLayout;
	private String specialization;
	
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view=inflater.inflate(R.layout.prescriptionsettings_layout, null);
		
		etTitlefont=(EditText)view.findViewById(R.id.etTitlefont);
		etContentfont=(EditText)view.findViewById(R.id.etContentfont);
		etHeader=(EditText)view.findViewById(R.id.etHeader);
		etFooter=(EditText)view.findViewById(R.id.etFooter);
		etLeft=(EditText)view.findViewById(R.id.etLeft);
		etRight=(EditText)view.findViewById(R.id.etRight);
		btSubmit=(Button)view.findViewById(R.id.btnSubmit);
		doctername=(TextView)view.findViewById(R.id.tvdoctername);
		tvDate=(TextView)view.findViewById(R.id.tvDate);
		llfirst=(LinearLayout)view.findViewById(R.id.llfirst);
		llthird=(LinearLayout)view.findViewById(R.id.llthird);
		
		btnLayout=(Button)view.findViewById(R.id.btnlayout);
		
		SharedPreferences docter_name=getActivity().getSharedPreferences("Login", 0);
		specialization=docter_name.getString("spel", null);
		
		if(specialization.equalsIgnoreCase("gynecologist obstetrician")){
			btnLayout.setVisibility(View.VISIBLE);
		}
		
		String docter_first_name=docter_name.getString("fName", null);  
		String docter_last_name=docter_name.getString("lName", null);
		
		etHeader.setText(Float.toString(docter_name.getFloat("header",0)));
		etFooter.setText(Float.toString(docter_name.getFloat("footer", 0)));
		etLeft.setText(Float.toString(docter_name.getFloat("left", 0)));
		etRight.setText(Float.toString(docter_name.getFloat("right", 0)));
		etTitlefont.setText(Float.toString(docter_name.getFloat("title",0)));
		etContentfont.setText(Float.toString(docter_name.getFloat("content",0)));
		
		
		doctername.setText(docter_first_name+" "+docter_last_name);
		
		

		Calendar c = Calendar.getInstance();
		System.out.println("Current time => " + c.getTime());
		SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
		String formattedDate = df.format(c.getTime());
		tvDate.setText(formattedDate);
		SharedPreferences sp=getActivity().getSharedPreferences("Login", 0);
		
		btnLayout.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Bundle b=new Bundle();
				PrintlayoutSelectorDialog psObj=new PrintlayoutSelectorDialog();
				b.putString("from", "settings");
				psObj.setArguments(b);
				psObj.show(getFragmentManager(), "tag");
				
				
			}
		});
		
		
		
		llfirst.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				hideKeyboard(arg0);
				
			}
		});
		
llthird.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				hideKeyboard(arg0);
				
			}
		});
		
		
		btSubmit.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				
				
				if(TextUtils.isEmpty(etHeader.getText().toString())){
					etHeader.setError("Enter Header Size");

					etHeader.requestFocus();

				}else if(TextUtils.isEmpty(etFooter.getText().toString())){
					
					etFooter.setError("Enter Footer Size");

					etFooter.requestFocus();
					
				}else if(TextUtils.isEmpty(etLeft.getText().toString())){
					
					etLeft.setError("Enter Left Margin Size");

					etLeft.requestFocus();
				}else if(TextUtils.isEmpty(etRight.getText().toString())){
					
					etRight.setError("Enter Right Margin Size");

					etRight.requestFocus();
				}else{
					float cm=0.02645833333f;
					float pixel=37.79f;
					
					float headervalue=(Float.valueOf(etHeader.getText().toString()));
					float footervalue=(Float.valueOf(etFooter.getText().toString()));
					float leftvalue=(Float.valueOf(etLeft.getText().toString()));
					float rightvalue=(Float.valueOf(etRight.getText().toString()));
					float Titlefontvalue=(Float.valueOf(etTitlefont.getText().toString()));
					float Contentfontvalue=(Float.valueOf(etContentfont.getText().toString()));
					
					System.out.println("header value"+headervalue);
					System.out.println("footervalue value"+footervalue);
					
					SharedPreferences sp=getActivity().getSharedPreferences("Login", 0);
					SharedPreferences.Editor Ed=sp.edit();
					Ed.putFloat("header",headervalue);              
					Ed.putFloat("footer",footervalue); 
					Ed.putFloat("left",leftvalue);
					Ed.putFloat("right",rightvalue);
					Ed.putFloat("title",Titlefontvalue);
					Ed.putFloat("content",Contentfontvalue);
					
					Ed.commit();
					
					Toast.makeText(getActivity(), "Successfully Saved", Toast.LENGTH_LONG).show();
					
					getActivity().getFragmentManager().popBackStack();
					
				}
				
				
				
			}
		});
		
		
		return view;
	}
	
	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		CrashReporter.getInstance().trackScreenView("Assign Drugs");
	}
	
	protected void hideKeyboard(View view) {
		InputMethodManager in = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
		in.hideSoftInputFromWindow(view.getWindowToken(),
				InputMethodManager.HIDE_NOT_ALWAYS);
	}


}
