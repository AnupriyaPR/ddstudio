package com.wiinnova.doctorsdiary.fragment;


import com.wiinnova.doctorsdiary.fragments.PatientMedicalaHistory;
import com.wiinnova.doctorsdiary.fragments.PatientPreviousTreatment;
import com.wiinnova.doctorsdiary.fragments.PatientpersonalInfo;

import com.wiinnova.doctorsdiary.R;
import com.parse.ParseObject;
import com.wiinnova.doctorsdiary.activities.FragmentActivityContainer;
import com.wiinnova.doctorsdiary.activities.Home_Page_Activity;
import com.wiinnova.doctorsdiary.fragment.Patient_Medical_History_First_Frag.onbuttonactivationmedhist;
import com.wiinnova.doctorsdiary.fragment.Patient_Personal_Info_Frag.onbuttonactivation;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;


public class Patient_Frag extends Fragment {

    boolean patientedit_checkflag = true;
    boolean patientflag = true;

    public static RelativeLayout pinfo;
    public static RelativeLayout pcurrMedInfo;
    public static RelativeLayout pmedHist;
    public static RelativeLayout pmeCurrent;
    public static RelativeLayout rlContacts;
    public static RelativeLayout pObstriticshis;


    private boolean doubleBackToExitPressedOnce;

    ParseObject patientnameobj;

    String pid;
    Patient_Personal_Info_Frag pntprsnlinfo;
    Obstetrics_gynacologist pntobstetrics;
    Patient_Medical_History_First_Frag pntmedhist;
    CurrentVitals cvObj;

    Patientpersonalinfo_Gynacoliogist pntprsnlinfo_gynacologist;

    PatientCurrentMedicalaInformation_Gynacologist pntmedhist_gynacologist;
    PatientpersonalInfo pntprsnlinfo_nephro;
    PatientMedicalaHistory pntmedhist_nephro;


    Patient_Frag patientfragment;

    Button btnsubmit;
    //ImageView btnback;
    public int menu_item = 0;
    private int sidemenu;
    private boolean admintab;
    private String specialization;


    public interface onPersonalsubmit {
        void onPersonal(int arg, int targetfragment);
    }

    public interface onMedicalhistorysubmit {
        void onMedical(int arg, int targetfragment);
    }

    public interface oncurrentvitalssubmit {
        void onCurrent(int arg, int targetfragment);
    }

    public interface onObstriticsSubmit {
        void onObstritics(int arg, int targetfragment);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.patient_frag, null);


        SharedPreferences sp = getActivity().getSharedPreferences("Login", 0);
        specialization = sp.getString("spel", null);


        ((FragmentActivityContainer) getActivity()).patienttabcolorchange();
        patientnameobj = ((FragmentActivityContainer) getActivity()).getPatientobject();


        patientfragment = ((FragmentActivityContainer) getActivity()).getpatientfrag();

        pinfo = (RelativeLayout) view.findViewById(R.id.ppersonalinfo);
        pcurrMedInfo = (RelativeLayout) view.findViewById(R.id.pcurrmedinfo);
        pmedHist = (RelativeLayout) view.findViewById(R.id.pmedhis);
        pmeCurrent = (RelativeLayout) view.findViewById(R.id.currentvitals);
        rlContacts = (RelativeLayout) view.findViewById(R.id.rlContactus);
        btnsubmit = (Button) view.findViewById(R.id.btnSubmit);
        pObstriticshis = (RelativeLayout) view.findViewById(R.id.pobshis);

        //btnback=(ImageView)view.findViewById(R.id.btnSubmit1);

        final Bundle bundle = getArguments();
        patientflag = bundle.getBoolean("patientflag");
        sidemenu = bundle.getInt("sidemenuitem");

        admintab = ((FragmentActivityContainer) getActivity()).getcheckadmintab();

        if (specialization.equalsIgnoreCase("gynecologist obstetrician")) {
            pmeCurrent.setVisibility(View.INVISIBLE);
            pObstriticshis.setVisibility(View.VISIBLE);
        }
        if (specialization.equalsIgnoreCase("Nephrologist")) {
            pmeCurrent.setVisibility(View.INVISIBLE);
            pObstriticshis.setVisibility(View.VISIBLE);
        }

        if (sidemenu == 1) {
            if (specialization.equalsIgnoreCase("gynecologist obstetrician")) {

                pObstriticshis.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
                pinfo.setBackgroundColor(getResources().getColor(R.color.red_color));
                pcurrMedInfo.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
                pmedHist.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));

            } else {

                pinfo.setBackgroundColor(getResources().getColor(R.color.red_color));
                pcurrMedInfo.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
                pmedHist.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
                pmeCurrent.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
            }
        } else if (sidemenu == 2) {
            if (specialization.equalsIgnoreCase("gynecologist obstetrician")) {

                pObstriticshis.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
                pinfo.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
                pcurrMedInfo.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
                pmedHist.setBackgroundColor(getResources().getColor(R.color.red_color));

            } else {

                pinfo.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
                pcurrMedInfo.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
                pmedHist.setBackgroundColor(getResources().getColor(R.color.red_color));
                pmeCurrent.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));

            }
        } else if (sidemenu == 3) {
            if (specialization.equalsIgnoreCase("gynecologist obstetrician")) {

                pinfo.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
                pcurrMedInfo.setBackgroundColor(getResources().getColor(R.color.red_color));
                pmedHist.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
                pObstriticshis.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));

            } else {

                pinfo.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
                pcurrMedInfo.setBackgroundColor(getResources().getColor(R.color.red_color));
                pmedHist.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
                pmeCurrent.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
            }
        } else if (sidemenu == 4) {
            if (specialization.equalsIgnoreCase("gynecologist obstetrician")) {

                pinfo.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
                pcurrMedInfo.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
                pmedHist.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
                pObstriticshis.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));

            } else {

                pinfo.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
                pcurrMedInfo.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
                pmedHist.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
                pmeCurrent.setBackgroundColor(getResources().getColor(R.color.red_color));
            }
        } else if (sidemenu == 5) {

            if (specialization.equalsIgnoreCase("gynecologist obstetrician")) {

                pObstriticshis.setBackgroundColor(getResources().getColor(R.color.red_color));
                pinfo.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
                pcurrMedInfo.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
                pmedHist.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));

            } else {

                pinfo.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
                pcurrMedInfo.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
                pmedHist.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
                pmeCurrent.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
            }
        }


        if (((FragmentActivityContainer) getActivity()).getdisablesidemenuitem() == true) {

            pcurrMedInfo.setVisibility(View.INVISIBLE);
            pmedHist.setVisibility(View.INVISIBLE);
            pmeCurrent.setVisibility(View.INVISIBLE);

        }


        System.out.println("patient flag in patient frag" + patientflag);


		/*btnback.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				((FragmentActivityContainer)getActivity()).backpressedmethod();
			}
		});
		 */

        btnsubmit.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub

                System.out.println("menu item value...." + menu_item);

                try {
                    if (menu_item == 1) {
                        if (specialization.equalsIgnoreCase("gynecologist obstetrician")) {
                            ((FragmentActivityContainer) getActivity()).getPntprsnlinfo_gynacologist().onPersonal(1, 5);
                        } else if (specialization.equalsIgnoreCase("Nephrologist")) {
                            ((FragmentActivityContainer) getActivity()).getPntprsnlinfo_nephro().onPersonal(1, 0);
                        } else {

                            ((FragmentActivityContainer) getActivity()).getpersonalinfo().onPersonal(1, 0);
                        }
                    }
                    System.out.println(" not working inside personal btn" + menu_item);
                    if (menu_item == 2) {

                        if (specialization.equalsIgnoreCase("gynecologist obstetrician")) {
                            ((FragmentActivityContainer) getActivity()).getPatientcurrentmedinfo_gynacologist().onMedical(1, 5);
                        } else if (specialization.equalsIgnoreCase("Nephrologist")) {
                            ((FragmentActivityContainer) getActivity()).getPatientMedicalHistory_nephro().onMedical(1, 0);
                        } else {
                            ((FragmentActivityContainer) getActivity()).getmedicalhistory().onMedical(1, 0);
                        }
                    }
                    if (menu_item == 3) {
                        if (specialization.equalsIgnoreCase("Nephrologist")) {
                            ((FragmentActivityContainer) getActivity()).getPatientMedicalHistory_nephro().onMedical(1, 0);
                        } else
                            ((FragmentActivityContainer) getActivity()).getcurrentvitals().onCurrent(1, 0);
                    }

                    if (menu_item == 5) {
                        if (specialization.equalsIgnoreCase("gynecologist obstetrician")) {
                            ((FragmentActivityContainer) getActivity()).getPatientObstritics().onObstritics(1, 2);
                        }
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        });


        pinfo.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub

                if (specialization.equalsIgnoreCase("Nephrologist")) {

                    pntprsnlinfo_nephro = ((FragmentActivityContainer) getActivity()).getPntprsnlinfo_nephro();
                    if (FragmentActivityContainer.check_save == 0) {
                        FragmentManager fragmentManager = getFragmentManager();
                        pntprsnlinfo_nephro = new PatientpersonalInfo();
                        if (bundle != null) {
                            Bundle bundle1 = new Bundle();
                            bundle1.putBoolean("patientflag", patientflag);
                            pntprsnlinfo_nephro.setArguments(bundle1);
                        }
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.replace(R.id.fl_fragment_container, pntprsnlinfo_nephro);
                        if (admintab == true) {
                            fragmentTransaction.addToBackStack(null);
                        }
                        fragmentTransaction.commit();

                    } else {

                        AlertDialog.Builder builder1 = new AlertDialog.Builder(getActivity());
                        builder1.setTitle("SAVE ITEM");
                        builder1.setMessage("Do you want save these items");
                        builder1.setCancelable(true);
                        builder1.setPositiveButton("Yes",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        if (getFragmentManager().findFragmentById(R.id.fl_fragment_container) instanceof PatientpersonalInfo) {
                                            ((FragmentActivityContainer) getActivity()).getPntprsnlinfo_nephro().onPersonal(1, 1);
                                        } else if (getFragmentManager().findFragmentById(R.id.fl_fragment_container) instanceof Patient_Current_Medical_Info_Frag) {
                                            ((FragmentActivityContainer) getActivity()).getPatientMedicalHistory_nephro().onMedical(1, 1);
                                        }
                                        dialog.cancel();
                                    }
                                });

                        builder1.setNegativeButton("No",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        FragmentActivityContainer.check_save = 0;
                                        FragmentManager fragmentManager = getFragmentManager();
                                        pntprsnlinfo_nephro = new PatientpersonalInfo();
                                        if (bundle != null) {
                                            Bundle bundle1 = new Bundle();
                                            bundle1.putBoolean("patientflag", patientflag);
                                            pntprsnlinfo_nephro.setArguments(bundle1);
                                        }
                                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                                        fragmentTransaction.replace(R.id.fl_fragment_container, pntprsnlinfo_nephro);
                                        if (admintab == true) {
                                            fragmentTransaction.addToBackStack(null);
                                        }
                                        fragmentTransaction.commit();
                                    }
                                });
                        AlertDialog alert11 = builder1.create();
                        alert11.show();
                    }

                } else if (specialization.equalsIgnoreCase("gynecologist obstetrician")) {

                    pntprsnlinfo_gynacologist = ((FragmentActivityContainer) getActivity()).getPntprsnlinfo_gynacologist();
                    if (FragmentActivityContainer.check_save == 0) {
                        FragmentManager fragmentManager = getFragmentManager();
                        pntprsnlinfo_gynacologist = new Patientpersonalinfo_Gynacoliogist();
                        if (bundle != null) {
                            Bundle bundle1 = new Bundle();
                            bundle1.putBoolean("patientflag", patientflag);
                            pntprsnlinfo_gynacologist.setArguments(bundle1);
                        }
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.replace(R.id.fl_fragment_container, pntprsnlinfo_gynacologist);
                        if (admintab == true) {
                            fragmentTransaction.addToBackStack(null);
                        }
                        fragmentTransaction.commit();

                    } else {

                        AlertDialog.Builder builder1 = new AlertDialog.Builder(getActivity());
                        builder1.setTitle("SAVE ITEM");
                        builder1.setMessage("Do you want save these items");
                        builder1.setCancelable(true);
                        builder1.setPositiveButton("Yes",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        if (getFragmentManager().findFragmentById(R.id.fl_fragment_container) instanceof Patientpersonalinfo_Gynacoliogist) {
                                            ((FragmentActivityContainer) getActivity()).getPntprsnlinfo_gynacologist().onPersonal(1, 1);
                                        } else if (getFragmentManager().findFragmentById(R.id.fl_fragment_container) instanceof PatientCurrentMedicalaInformation_Gynacologist) {
                                            ((FragmentActivityContainer) getActivity()).getPatientcurrentmedinfo_gynacologist().onMedical(1, 1);
                                        } else if (getFragmentManager().findFragmentById(R.id.fl_fragment_container) instanceof Obstetrics_gynacologist) {
                                            ((FragmentActivityContainer) getActivity()).getPatientObstritics().onObstritics(1, 1);
                                        }


                                        dialog.cancel();
                                    }
                                });

                        builder1.setNegativeButton("No",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        FragmentActivityContainer.check_save = 0;
                                        FragmentManager fragmentManager = getFragmentManager();
                                        pntprsnlinfo_gynacologist = new Patientpersonalinfo_Gynacoliogist();
                                        if (bundle != null) {
                                            Bundle bundle1 = new Bundle();
                                            bundle1.putBoolean("patientflag", patientflag);
                                            pntprsnlinfo_gynacologist.setArguments(bundle1);
                                        }
                                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                                        fragmentTransaction.replace(R.id.fl_fragment_container, pntprsnlinfo_gynacologist);
                                        if (admintab == true) {
                                            fragmentTransaction.addToBackStack(null);
                                        }
                                        fragmentTransaction.commit();
                                    }
                                });
                        AlertDialog alert11 = builder1.create();
                        alert11.show();
                    }

                } else {

                    pntprsnlinfo = ((FragmentActivityContainer) getActivity()).getpersonalinfo();
                    if (FragmentActivityContainer.check_save == 0) {
                        FragmentManager fragmentManager = getFragmentManager();
                        pntprsnlinfo = new Patient_Personal_Info_Frag();
                        if (bundle != null) {
                            Bundle bundle1 = new Bundle();
                            bundle1.putBoolean("patientflag", patientflag);
                            pntprsnlinfo.setArguments(bundle1);
                        }
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.replace(R.id.fl_fragment_container, pntprsnlinfo);
                        if (admintab == true) {
                            fragmentTransaction.addToBackStack(null);
                        }
                        fragmentTransaction.commit();
                    } else {
                        AlertDialog.Builder builder1 = new AlertDialog.Builder(getActivity());
                        builder1.setTitle("SAVE ITEM");
                        builder1.setMessage("Do you want save these items");
                        builder1.setCancelable(true);
                        builder1.setPositiveButton("Yes",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        if (getFragmentManager().findFragmentById(R.id.fl_fragment_container) instanceof Patient_Personal_Info_Frag) {
                                            System.out.println("yes working");
                                            ((FragmentActivityContainer) getActivity()).getpersonalinfo().onPersonal(1, 1);
                                        } else if (getFragmentManager().findFragmentById(R.id.fl_fragment_container) instanceof Patient_Medical_History_First_Frag) {
                                            ((FragmentActivityContainer) getActivity()).getmedicalhistory().onMedical(1, 1);
                                        } else if (getFragmentManager().findFragmentById(R.id.fl_fragment_container) instanceof CurrentVitals) {
                                            System.out.println("current working123");
                                            ((FragmentActivityContainer) getActivity()).getcurrentvitals().onCurrent(1, 1);
                                        }
                                        dialog.cancel();
                                    }
                                });
                        builder1.setNegativeButton("No",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        FragmentActivityContainer.check_save = 0;
                                        FragmentManager fragmentManager = getFragmentManager();
                                        pntprsnlinfo = new Patient_Personal_Info_Frag();
                                        if (bundle != null) {
                                            Bundle bundle1 = new Bundle();
                                            bundle1.putBoolean("patientflag", patientflag);
                                            pntprsnlinfo.setArguments(bundle1);
                                        }
                                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                                        fragmentTransaction.replace(R.id.fl_fragment_container, pntprsnlinfo);
                                        if (admintab == true) {
                                            System.out.println("admin tab working.........");
                                            fragmentTransaction.addToBackStack(null);
                                        }
                                        fragmentTransaction.commit();
                                    }
                                });
                        AlertDialog alert11 = builder1.create();
                        alert11.show();
                    }
                }


            }
        });

		/*pcurrMedInfo.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				if(FragmentActivityContainer.check_save == 0){

					FragmentManager fragmentManager = getFragmentManager();
					FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
					Patient_Current_Medical_Info_Frag pntcurrmedinfo = new Patient_Current_Medical_Info_Frag();

					fragmentTransaction.replace(R.id.fl_fragment_container, pntcurrmedinfo);
					if(admintab==true)
						fragmentTransaction.addToBackStack(null);
					fragmentTransaction.commit();
				}else{


					AlertDialog.Builder builder1 = new AlertDialog.Builder(getActivity());
					builder1.setTitle("SAVE ITEM");
					builder1.setMessage("Do you want save these items");
					builder1.setCancelable(true);
					builder1.setPositiveButton("Yes",
							new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id) {
							System.out.println("working med");
							System.out.println("getFragmentManager().findFragmentById(R.id.fl_fragment_container)"+getFragmentManager().findFragmentById(R.id.fl_fragment_container));
							if(getFragmentManager().findFragmentById(R.id.fl_fragment_container) instanceof Patient_Personal_Info_Frag){
								try{
									System.out.println("patient pesonal frag");

									((FragmentActivityContainer)getActivity()).getpersonalinfo().onPersonal(1,3);

								}catch(Exception e){
									Toast.makeText(getActivity(), "getfragment error7", Toast.LENGTH_LONG).show();
								}
							}else if(getFragmentManager().findFragmentById(R.id.fl_fragment_container) instanceof Patient_Medical_History_First_Frag){
								System.out.println("med history frag");

								((FragmentActivityContainer)getActivity()).getmedicalhistory().onMedical(1,3);

							}else if(getFragmentManager().findFragmentById(R.id.fl_fragment_container) instanceof CurrentVitals){

								((FragmentActivityContainer)getActivity()).getcurrentvitals().onCurrent(1,3);

							}
							dialog.cancel();
						}
					});
					builder1.setNegativeButton("No",
							new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id) {

							FragmentActivityContainer.check_save=0;

							getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);

							FragmentManager fragmentManager = getFragmentManager();
							Patient_Current_Medical_Info_Frag pntcurrmedinfo = new Patient_Current_Medical_Info_Frag();
							FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
							fragmentTransaction.replace(R.id.fl_fragment_container, pntcurrmedinfo);
							if(admintab==true){
								System.out.println("admin tab working.........");
								fragmentTransaction.addToBackStack(null);
							}
							fragmentTransaction.commit();
						}
					});

					AlertDialog alert11 = builder1.create();
					alert11.show();

				}


			}
		});
		 */


        pcurrMedInfo.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                if (specialization.equalsIgnoreCase("Nephrologist")) {


                    if (FragmentActivityContainer.check_save == 0) {

                        FragmentManager fragmentManager = getFragmentManager();
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        PatientPreviousTreatment pntcurrmedinfo = new PatientPreviousTreatment();

                        fragmentTransaction.replace(R.id.fl_fragment_container, pntcurrmedinfo);
                        if (admintab == true)
                            fragmentTransaction.addToBackStack(null);
                        fragmentTransaction.commit();
                    } else {


                        AlertDialog.Builder builder1 = new AlertDialog.Builder(getActivity());
                        builder1.setTitle("SAVE ITEM");
                        builder1.setMessage("Do you want save these items");
                        builder1.setCancelable(true);
                        builder1.setPositiveButton("Yes",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        System.out.println("working med");
                                        System.out.println("getFragmentManager().findFragmentById(R.id.fl_fragment_container)" + getFragmentManager().findFragmentById(R.id.fl_fragment_container));
                                        if (getFragmentManager().findFragmentById(R.id.fl_fragment_container) instanceof PatientpersonalInfo) {
                                            ((FragmentActivityContainer) getActivity()).getPntprsnlinfo_nephro().onPersonal(1, 3);
                                        } else if (getFragmentManager().findFragmentById(R.id.fl_fragment_container) instanceof PatientMedicalaHistory) {
                                            ((FragmentActivityContainer) getActivity()).getPatientMedicalHistory_nephro().onMedical(1, 3);
                                        }
                                        dialog.cancel();
                                    }
                                });
                        builder1.setNegativeButton("No",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {

                                        FragmentActivityContainer.check_save = 0;

								/*getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);*/

                                        FragmentManager fragmentManager = getFragmentManager();
                                        PatientPreviousTreatment pntcurrmedinfo = new PatientPreviousTreatment();
                                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                                        fragmentTransaction.replace(R.id.fl_fragment_container, pntcurrmedinfo);
                                        if (admintab == true) {
                                            System.out.println("admin tab working.........");
                                            fragmentTransaction.addToBackStack(null);
                                        }
                                        fragmentTransaction.commit();
                                    }
                                });

                        AlertDialog alert11 = builder1.create();
                        alert11.show();

                    }


                } else if (specialization.equalsIgnoreCase("gynecologist obstetrician")) {


                    if (FragmentActivityContainer.check_save == 0) {

                        FragmentManager fragmentManager = getFragmentManager();
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        Patient_Current_Medical_Info_Frag pntcurrmedinfo = new Patient_Current_Medical_Info_Frag();

                        fragmentTransaction.replace(R.id.fl_fragment_container, pntcurrmedinfo);
                        if (admintab == true)
                            fragmentTransaction.addToBackStack(null);
                        fragmentTransaction.commit();
                    } else {


                        AlertDialog.Builder builder1 = new AlertDialog.Builder(getActivity());
                        builder1.setTitle("SAVE ITEM");
                        builder1.setMessage("Do you want save these items");
                        builder1.setCancelable(true);
                        builder1.setPositiveButton("Yes",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        System.out.println("working med");
                                        System.out.println("getFragmentManager().findFragmentById(R.id.fl_fragment_container)" + getFragmentManager().findFragmentById(R.id.fl_fragment_container));
                                        if (getFragmentManager().findFragmentById(R.id.fl_fragment_container) instanceof Patientpersonalinfo_Gynacoliogist) {
                                            ((FragmentActivityContainer) getActivity()).getPntprsnlinfo_gynacologist().onPersonal(1, 3);
                                        } else if (getFragmentManager().findFragmentById(R.id.fl_fragment_container) instanceof PatientCurrentMedicalaInformation_Gynacologist) {
                                            ((FragmentActivityContainer) getActivity()).getPatientcurrentmedinfo_gynacologist().onMedical(1, 3);
                                        } else if (getFragmentManager().findFragmentById(R.id.fl_fragment_container) instanceof Obstetrics_gynacologist) {
                                            ((FragmentActivityContainer) getActivity()).getPatientObstritics().onObstritics(1, 1);
                                        }
                                        dialog.cancel();
                                    }
                                });
                        builder1.setNegativeButton("No",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {

                                        FragmentActivityContainer.check_save = 0;

								/*getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);*/

                                        FragmentManager fragmentManager = getFragmentManager();
                                        Patient_Current_Medical_Info_Frag pntcurrmedinfo = new Patient_Current_Medical_Info_Frag();
                                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                                        fragmentTransaction.replace(R.id.fl_fragment_container, pntcurrmedinfo);
                                        if (admintab == true) {
                                            System.out.println("admin tab working.........");
                                            fragmentTransaction.addToBackStack(null);
                                        }
                                        fragmentTransaction.commit();
                                    }
                                });

                        AlertDialog alert11 = builder1.create();
                        alert11.show();

                    }


                } else {


                    if (FragmentActivityContainer.check_save == 0) {

                        FragmentManager fragmentManager = getFragmentManager();
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        Patient_Current_Medical_Info_Frag pntcurrmedinfo = new Patient_Current_Medical_Info_Frag();

                        fragmentTransaction.replace(R.id.fl_fragment_container, pntcurrmedinfo);
                        if (admintab == true)
                            fragmentTransaction.addToBackStack(null);
                        fragmentTransaction.commit();
                    } else {


                        AlertDialog.Builder builder1 = new AlertDialog.Builder(getActivity());
                        builder1.setTitle("SAVE ITEM");
                        builder1.setMessage("Do you want save these items");
                        builder1.setCancelable(true);
                        builder1.setPositiveButton("Yes",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        System.out.println("working med");
                                        System.out.println("getFragmentManager().findFragmentById(R.id.fl_fragment_container)" + getFragmentManager().findFragmentById(R.id.fl_fragment_container));
                                        if (getFragmentManager().findFragmentById(R.id.fl_fragment_container) instanceof Patient_Personal_Info_Frag) {
                                            try {
                                                System.out.println("patient pesonal frag");

                                                ((FragmentActivityContainer) getActivity()).getpersonalinfo().onPersonal(1, 3);

                                            } catch (Exception e) {
                                                Toast.makeText(getActivity(), "getfragment error7", Toast.LENGTH_LONG).show();
                                            }
                                        } else if (getFragmentManager().findFragmentById(R.id.fl_fragment_container) instanceof Patient_Medical_History_First_Frag) {
                                            System.out.println("med history frag");

                                            ((FragmentActivityContainer) getActivity()).getmedicalhistory().onMedical(1, 3);

                                        } else if (getFragmentManager().findFragmentById(R.id.fl_fragment_container) instanceof CurrentVitals) {

                                            ((FragmentActivityContainer) getActivity()).getcurrentvitals().onCurrent(1, 3);

                                        }
                                        dialog.cancel();
                                    }
                                });
                        builder1.setNegativeButton("No",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {

                                        FragmentActivityContainer.check_save = 0;

								/*getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);*/

                                        FragmentManager fragmentManager = getFragmentManager();
                                        Patient_Current_Medical_Info_Frag pntcurrmedinfo = new Patient_Current_Medical_Info_Frag();
                                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                                        fragmentTransaction.replace(R.id.fl_fragment_container, pntcurrmedinfo);
                                        if (admintab == true) {
                                            System.out.println("admin tab working.........");
                                            fragmentTransaction.addToBackStack(null);
                                        }
                                        fragmentTransaction.commit();
                                    }
                                });

                        AlertDialog alert11 = builder1.create();
                        alert11.show();

                    }


                }
            }
        });


        pmedHist.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                if (specialization.equalsIgnoreCase("Nephrologist")) {


                    if (FragmentActivityContainer.check_save == 0) {

                        FragmentManager fragmentManager = getFragmentManager();
                        pntmedhist_nephro = new PatientMedicalaHistory();

                        if (bundle != null) {
                            Bundle bundle1 = new Bundle();
                            bundle1.putBoolean("patientflag", patientflag);
                            pntmedhist_nephro.setArguments(bundle);
                        }
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

                        fragmentTransaction.replace(R.id.fl_fragment_container, pntmedhist_nephro);
                        if (admintab == true) {
                            fragmentTransaction.addToBackStack(null);
                        }
                        fragmentTransaction.commit();


                    } else {

                        AlertDialog.Builder builder1 = new AlertDialog.Builder(getActivity());
                        builder1.setTitle("SAVE ITEM");
                        builder1.setMessage("Do you want save these items");
                        builder1.setCancelable(true);
                        builder1.setPositiveButton("Yes",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        try {
                                            if (getFragmentManager().findFragmentById(R.id.fl_fragment_container) instanceof PatientpersonalInfo) {
                                                ((FragmentActivityContainer) getActivity()).getPntprsnlinfo_nephro().onPersonal(1, 2);
                                            } else if (getFragmentManager().findFragmentById(R.id.fl_fragment_container) instanceof PatientMedicalaHistory) {
                                                ((FragmentActivityContainer) getActivity()).getPatientMedicalHistory_nephro().onMedical(1, 2);
                                            }
                                        } catch (Exception e) {
                                            Toast.makeText(getActivity(), "getfragment error9", Toast.LENGTH_LONG).show();
                                        }
                                        dialog.cancel();
                                    }
                                });
                        builder1.setNegativeButton("No",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        FragmentActivityContainer.check_save = 0;
                                        FragmentManager fragmentManager = getFragmentManager();
                                        pntmedhist_nephro = new PatientMedicalaHistory();
                                        if (bundle != null) {
                                            Bundle bundle1 = new Bundle();
                                            bundle1.putBoolean("patientflag", patientflag);
                                            pntmedhist_nephro.setArguments(bundle);
                                        }
                                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                                        fragmentTransaction.replace(R.id.fl_fragment_container, pntmedhist_nephro);
                                        if (admintab == true) {
                                            fragmentTransaction.addToBackStack(null);
                                        }
                                        fragmentTransaction.commit();
                                    }
                                });
                        AlertDialog alert11 = builder1.create();
                        alert11.show();
                    }


                } else if (specialization.equalsIgnoreCase("gynecologist obstetrician")) {

                    if (FragmentActivityContainer.check_save == 0) {

                        FragmentManager fragmentManager = getFragmentManager();
                        pntmedhist_gynacologist = new PatientCurrentMedicalaInformation_Gynacologist();

                        if (bundle != null) {
                            Bundle bundle1 = new Bundle();
                            bundle1.putBoolean("patientflag", patientflag);
                            pntmedhist_gynacologist.setArguments(bundle);
                        }
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

                        fragmentTransaction.replace(R.id.fl_fragment_container, pntmedhist_gynacologist);
                        if (admintab == true) {
                            fragmentTransaction.addToBackStack(null);
                        }
                        fragmentTransaction.commit();


                    } else {

                        AlertDialog.Builder builder1 = new AlertDialog.Builder(getActivity());
                        builder1.setTitle("SAVE ITEM");
                        builder1.setMessage("Do you want save these items");
                        builder1.setCancelable(true);
                        builder1.setPositiveButton("Yes",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        try {
                                            if (getFragmentManager().findFragmentById(R.id.fl_fragment_container) instanceof Patientpersonalinfo_Gynacoliogist) {
                                                ((FragmentActivityContainer) getActivity()).getPntprsnlinfo_gynacologist().onPersonal(1, 2);
                                            } else if (getFragmentManager().findFragmentById(R.id.fl_fragment_container) instanceof PatientCurrentMedicalaInformation_Gynacologist) {
                                                ((FragmentActivityContainer) getActivity()).getPatientcurrentmedinfo_gynacologist().onMedical(1, 2);
                                            } else if (getFragmentManager().findFragmentById(R.id.fl_fragment_container) instanceof Obstetrics_gynacologist) {
                                                ((FragmentActivityContainer) getActivity()).getPatientObstritics().onObstritics(1, 2);
                                            }
                                        } catch (Exception e) {
                                            Toast.makeText(getActivity(), "getfragment error9", Toast.LENGTH_LONG).show();
                                        }
                                        dialog.cancel();
                                    }
                                });
                        builder1.setNegativeButton("No",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        FragmentActivityContainer.check_save = 0;
                                        FragmentManager fragmentManager = getFragmentManager();
                                        pntmedhist_gynacologist = new PatientCurrentMedicalaInformation_Gynacologist();
                                        if (bundle != null) {
                                            Bundle bundle1 = new Bundle();
                                            bundle1.putBoolean("patientflag", patientflag);
                                            pntmedhist_gynacologist.setArguments(bundle);
                                        }
                                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                                        fragmentTransaction.replace(R.id.fl_fragment_container, pntmedhist_gynacologist);
                                        if (admintab == true) {
                                            fragmentTransaction.addToBackStack(null);
                                        }
                                        fragmentTransaction.commit();
                                    }
                                });
                        AlertDialog alert11 = builder1.create();
                        alert11.show();
                    }


                } else {

                    if (FragmentActivityContainer.check_save == 0) {
                        FragmentManager fragmentManager = getFragmentManager();

                        pntmedhist = new Patient_Medical_History_First_Frag();
                        if (bundle != null) {
                            Bundle bundle1 = new Bundle();
                            bundle1.putBoolean("patientflag", patientflag);
                            pntmedhist.setArguments(bundle);
                            System.out.println("medhist" + patientflag);
                        }
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

                        fragmentTransaction.replace(R.id.fl_fragment_container, pntmedhist);
                        System.out.println("admintab value1" + admintab);

                        if (admintab == true) {
                            System.out.println("admin tab working.........");
                            fragmentTransaction.addToBackStack(null);
                        }
                        fragmentTransaction.commit();
                    } else {
                        AlertDialog.Builder builder1 = new AlertDialog.Builder(getActivity());
                        builder1.setTitle("SAVE ITEM");
                        builder1.setMessage("Do you want save these items");
                        builder1.setCancelable(true);
                        builder1.setPositiveButton("Yes",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        try {
                                            if (getFragmentManager().findFragmentById(R.id.fl_fragment_container) instanceof Patient_Personal_Info_Frag) {
                                                System.out.println("yes working");
                                                ((FragmentActivityContainer) getActivity()).getpersonalinfo().onPersonal(1, 2);
                                            } else if (getFragmentManager().findFragmentById(R.id.fl_fragment_container) instanceof Patient_Medical_History_First_Frag) {
                                                ((FragmentActivityContainer) getActivity()).getmedicalhistory().onMedical(1, 2);
                                            } else if (getFragmentManager().findFragmentById(R.id.fl_fragment_container) instanceof CurrentVitals) {
                                                ((FragmentActivityContainer) getActivity()).getcurrentvitals().onCurrent(1, 2);
                                            }
                                        } catch (Exception e) {
                                            Toast.makeText(getActivity(), "getfragment error9", Toast.LENGTH_LONG).show();
                                        }
                                        dialog.cancel();
                                    }
                                });
                        builder1.setNegativeButton("No",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {

                                        FragmentActivityContainer.check_save = 0;
                                /*getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);*/
                                        FragmentManager fragmentManager = getFragmentManager();
                                        pntmedhist = new Patient_Medical_History_First_Frag();
                                        System.out.println("  workinggggggggggggggggggg");
                                        if (bundle != null) {
                                            Bundle bundle1 = new Bundle();
                                            bundle1.putBoolean("patientflag", patientflag);
                                            pntmedhist.setArguments(bundle);
                                        }
                                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                                        fragmentTransaction.replace(R.id.fl_fragment_container, pntmedhist);
                                        if (admintab == true) {
                                            System.out.println("admin tab working.........");
                                            fragmentTransaction.addToBackStack(null);
                                        }
                                        fragmentTransaction.commit();
                                    }
                                });

                        AlertDialog alert11 = builder1.create();
                        alert11.show();
                    }
                }
            }
        });


        pObstriticshis.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub

                if (specialization.equalsIgnoreCase("gynecologist obstetrician")) {

                    //pntobstetrics=((FragmentActivityContainer)getActivity()).getPatientObstritics();
                    if (FragmentActivityContainer.check_save == 0) {
                        FragmentManager fragmentManager = getFragmentManager();
                        pntobstetrics = new Obstetrics_gynacologist();
                        if (bundle != null) {
                            Bundle bundle1 = new Bundle();
                            bundle1.putBoolean("patientflag", patientflag);
                            pntobstetrics.setArguments(bundle1);
                        }
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.replace(R.id.fl_fragment_container, pntobstetrics);
                        if (admintab == true) {
                            fragmentTransaction.addToBackStack(null);
                        }
                        fragmentTransaction.commit();

                    } else {

                        AlertDialog.Builder builder1 = new AlertDialog.Builder(getActivity());
                        builder1.setTitle("SAVE ITEM");
                        builder1.setMessage("Do you want save these items");
                        builder1.setCancelable(true);
                        builder1.setPositiveButton("Yes",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        if (getFragmentManager().findFragmentById(R.id.fl_fragment_container) instanceof Patientpersonalinfo_Gynacoliogist) {
                                            ((FragmentActivityContainer) getActivity()).getPntprsnlinfo_gynacologist().onPersonal(1, 5);
                                        } else if (getFragmentManager().findFragmentById(R.id.fl_fragment_container) instanceof PatientCurrentMedicalaInformation_Gynacologist) {
                                            ((FragmentActivityContainer) getActivity()).getPatientcurrentmedinfo_gynacologist().onMedical(1, 5);
                                        } else if (getFragmentManager().findFragmentById(R.id.fl_fragment_container) instanceof Obstetrics_gynacologist) {
                                            ((FragmentActivityContainer) getActivity()).getPatientObstritics().onObstritics(1, 1);
                                        }
                                        dialog.cancel();
                                    }
                                });

                        builder1.setNegativeButton("No",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        FragmentActivityContainer.check_save = 0;
                                        FragmentManager fragmentManager = getFragmentManager();
                                        pntobstetrics = new Obstetrics_gynacologist();
                                        if (bundle != null) {
                                            Bundle bundle1 = new Bundle();
                                            bundle1.putBoolean("patientflag", patientflag);
                                            pntobstetrics.setArguments(bundle1);
                                        }
                                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                                        fragmentTransaction.replace(R.id.fl_fragment_container, pntobstetrics);
                                        if (admintab == true) {
                                            fragmentTransaction.addToBackStack(null);
                                        }
                                        fragmentTransaction.commit();
                                    }
                                });
                        AlertDialog alert11 = builder1.create();
                        alert11.show();
                    }

                }


            }
        });


        pmeCurrent.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                if (FragmentActivityContainer.check_save <= 0) {

                    FragmentManager fragmentmanager = getFragmentManager();

                    cvObj = new CurrentVitals();
                    if (bundle != null) {
                        Bundle bundle1 = new Bundle();
                        bundle1.putBoolean("patientflag", patientflag);
                        cvObj.setArguments(bundle);
                    }

					/*fragmentmanager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);*/

                    FragmentTransaction fragmenttransaction = fragmentmanager.beginTransaction();
                    fragmenttransaction.replace(R.id.fl_fragment_container, cvObj);

                    if (admintab == true) {
                        System.out.println("admin working....");
                        fragmenttransaction.addToBackStack(null);
                    }
                    fragmenttransaction.commit();

                } else {

                    AlertDialog.Builder builder1 = new AlertDialog.Builder(getActivity());
                    builder1.setTitle("SAVE ITEM");
                    builder1.setMessage("Do you want save these items");
                    builder1.setCancelable(true);
                    builder1.setPositiveButton("Yes",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                    if (getFragmentManager().findFragmentById(R.id.fl_fragment_container) instanceof Patient_Personal_Info_Frag) {
                                        System.out.println("yes working");
                                        ((FragmentActivityContainer) getActivity()).getpersonalinfo().onPersonal(1, 4);
                                    } else if (getFragmentManager().findFragmentById(R.id.fl_fragment_container) instanceof Patient_Medical_History_First_Frag) {
                                        ((FragmentActivityContainer) getActivity()).getmedicalhistory().onMedical(1, 4);
                                    } else if (getFragmentManager().findFragmentById(R.id.fl_fragment_container) instanceof CurrentVitals) {
                                        ((FragmentActivityContainer) getActivity()).getcurrentvitals().onCurrent(1, 4);
                                    }

                                    dialog.cancel();
                                }
                            });
                    builder1.setNegativeButton("No",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                    FragmentActivityContainer.check_save = 0;
                            /*getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);*/
                                    FragmentManager fragmentmanager = getFragmentManager();
							/*if(((FragmentActivityContainer)getActivity()).getcurrentvitals()==null){*/
                                    cvObj = new CurrentVitals();
                                    if (bundle != null) {
                                        Bundle bundle1 = new Bundle();

                                        bundle1.putBoolean("patientflag", patientflag);
                                        cvObj.setArguments(bundle);
                                    }
							/*}else{
								cvObj=((FragmentActivityContainer)getActivity()).getcurrentvitals();
							}*/
                                    FragmentTransaction fragmenttransaction = fragmentmanager.beginTransaction();
                                    fragmenttransaction.replace(R.id.fl_fragment_container, cvObj);
                                    if (admintab == true) {
                                        System.out.println("admin tab working.........");
                                        fragmenttransaction.addToBackStack(null);
                                    }

                                    fragmenttransaction.commit();
                                }
                            });

                    AlertDialog alert11 = builder1.create();
                    alert11.show();

                }

            }
        });

        return view;
    }

    public ParseObject getPatientobject() {
        return patientnameobj;
    }

    public void onButtonactivation(int arg) {
        // TODO Auto-generated method stub

        System.out.println("mmmmmmmmmmmmmmmmmmmmmmm");
        if (patientfragment != null) {
            if (patientfragment.isVisible()) {
				/*if(Patient_Frag.this.isVisible()){*/
                System.out.println("nnnnnnnnnnnnnnnnnnn");

                if (arg == 2) {
                    btnsubmit.setBackgroundDrawable(getResources().getDrawable(R.drawable.submitbtn_background));
                    btnsubmit.setTextColor(getResources().getColor(R.color.actionbar_color));
                    btnsubmit.setEnabled(true);


                    FragmentActivityContainer.check_save = 2;
                }
                if (arg == 0) {
                    btnsubmit.setBackgroundDrawable(getResources().getDrawable(R.drawable.submitbtn_background_beforesave));
                    btnsubmit.setTextColor(getResources().getColor(R.color.grey));
                    btnsubmit.setEnabled(false);

                    FragmentActivityContainer.check_save = 0;
                }
                //}
            }
        }
    }

    public void setmenuitemvalue(int value) {
        this.menu_item = value;
    }

    public int getmenuitemvalue() {
        return menu_item;

    }

    public void onbuttonmedhist(int arg) {
        // TODO Auto-generated method stub
		/*if(Patient_Frag.this.isVisible())*/
        if (patientfragment != null) {
            if (patientfragment.isVisible()) {

                System.out.println("patient frag check......." + FragmentActivityContainer.check_save);
                if (arg == 1) {
                    System.out.println("FragmentActivityContainer.check_save++ calling" + FragmentActivityContainer.check_save);
                    btnsubmit.setBackgroundDrawable(getResources().getDrawable(R.drawable.submitbtn_background));
                    btnsubmit.setTextColor(getResources().getColor(R.color.actionbar_color));
                    btnsubmit.setEnabled(true);
                    FragmentActivityContainer.check_save++;
                    System.out.println("FragmentActivityContainer.check_save++1 calling" + FragmentActivityContainer.check_save);
                }

                if (arg == 0) {

                    FragmentActivityContainer.check_save--;
                    if (FragmentActivityContainer.check_save <= 0) {
                        btnsubmit.setBackgroundDrawable(getResources().getDrawable(R.drawable.submitbtn_background_beforesave));
                        btnsubmit.setTextColor(getResources().getColor(R.color.grey));
                        btnsubmit.setEnabled(false);
                    } else {
                        btnsubmit.setBackgroundDrawable(getResources().getDrawable(R.drawable.submitbtn_background));
                        btnsubmit.setTextColor(getResources().getColor(R.color.actionbar_color));
                        btnsubmit.setEnabled(true);
                    }
                    if (FragmentActivityContainer.check_save < 0) {
                        FragmentActivityContainer.check_save = 0;
                    }
                }

                if (arg == 2) {
                    FragmentActivityContainer.check_save = 0;
                    btnsubmit.setBackgroundDrawable(getResources().getDrawable(R.drawable.submitbtn_background_beforesave));
                    btnsubmit.setTextColor(getResources().getColor(R.color.grey));
                    btnsubmit.setEnabled(false);
                }
            }
        }

    }


    @Override
    public void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
        System.out.println("on pause working........");
    }


    public void onCurrentbutton(int arg) {
        // TODO Auto-generated method stub
        if (patientfragment != null) {
            if (patientfragment.isVisible()) {
                if (arg == 1) {
                    btnsubmit.setBackgroundDrawable(getResources().getDrawable(R.drawable.submitbtn_background));
                    btnsubmit.setTextColor(getResources().getColor(R.color.actionbar_color));
                    btnsubmit.setEnabled(true);
                    FragmentActivityContainer.check_save = 2;
                }
                if (arg == 0) {
                    btnsubmit.setBackgroundDrawable(getResources().getDrawable(R.drawable.submitbtn_background_beforesave));
                    btnsubmit.setTextColor(getResources().getColor(R.color.grey));
                    btnsubmit.setEnabled(false);
                    FragmentActivityContainer.check_save = 0;

                }
            }
        }
    }


    //Patient Tab Side menu Items Color Change

    public void getpatient_personal_info_colorchange() {
        pinfo.setBackgroundColor(getResources().getColor(R.color.red_color));
        pcurrMedInfo.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
        pmedHist.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
        pmeCurrent.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
    }

    public void getmedicalhistory_colorchange() {
        pinfo.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
        pcurrMedInfo.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
        pmedHist.setBackgroundColor(getResources().getColor(R.color.red_color));
        pmeCurrent.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
    }

    public void getcurrentmedinfo_colorchange() {
        pinfo.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
        pcurrMedInfo.setBackgroundColor(getResources().getColor(R.color.red_color));
        pmedHist.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
        pmeCurrent.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));

        if (specialization.equalsIgnoreCase("gynecologist obstetrician")) {
            pObstriticshis.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
        }

    }

    public void getcurrent_vitals_colorchange() {
        pinfo.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
        pcurrMedInfo.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
        pmedHist.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
        pmeCurrent.setBackgroundColor(getResources().getColor(R.color.red_color));
    }


}