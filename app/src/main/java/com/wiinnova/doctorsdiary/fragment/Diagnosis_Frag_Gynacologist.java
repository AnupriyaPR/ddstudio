package com.wiinnova.doctorsdiary.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.wiinnova.doctorsdiary.R;
import com.parse.ParseObject;
import com.wiinnova.doctorsdiary.activities.FragmentActivityContainer;
import com.wiinnova.doctorsdiary.fragment.Diagnosis_Create_Frag.onbuttondiagnosis;


public class Diagnosis_Frag_Gynacologist extends Fragment implements onbuttondiagnosis {
    public static RelativeLayout Diagexamination;
    public static RelativeLayout DiagexaminationView;
    public static RelativeLayout createDiag;
    public static RelativeLayout labdata;
    public static RelativeLayout viewDiag;

    RelativeLayout rlContacts;
    public static Button btnsubmit;
    //ImageView btnBack;
    ParseObject patientdiagnosisparseObj;

    Fragment createDiagnosisFrag;
    Fragment diagnosisExamination;
    Diagnosislab_Gynacologist viewdiag;
    Diagnosisexamination_gynacologist_view viewexamination;


    private static int menu_item = 0;


    public interface ondiagnosiscreate {
        void ondiagnosis(int arg);
    }

    public interface onexaminationcreate {
        void onexamination(int arg);
    }
    public static Activity activity;

    public static void saveData(int frag) {

        if (menu_item == 1)
            ((FragmentActivityContainer)activity).getDiagexam_gynacologist().onexamination(frag);
        else
            ((FragmentActivityContainer) activity).getdiagnosiscreatefrag_gynacologist().ondiagnosis(frag);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.diagnosisfrag_gynacologist, null);
        activity=getActivity();

        ((FragmentActivityContainer) getActivity()).diagnosistabcolorchange();
        patientdiagnosisparseObj = ((FragmentActivityContainer) getActivity()).getpatientdiagnosisparseObj();

        Diagexamination = (RelativeLayout) view.findViewById(R.id.rldiagnosisexamination);
        createDiag = (RelativeLayout) view.findViewById(R.id.crtdia);
        viewDiag = (RelativeLayout) view.findViewById(R.id.viewdiagnosis);

        labdata = (RelativeLayout) view.findViewById(R.id.viewdia);
        DiagexaminationView = (RelativeLayout) view.findViewById(R.id.viewexamination);
        btnsubmit = (Button) view.findViewById(R.id.btnSubmit);
        //btnBack=(ImageView)view.findViewById(R.id.btnback);

        setSideMenuItembg(1);

        createDiagnosisFrag = ((FragmentActivityContainer) getActivity()).getdiagnosiscreatefrag_gynacologist();
        diagnosisExamination = ((FragmentActivityContainer) getActivity()).getDiagexam_gynacologist();

        btnsubmit.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub

                System.out.println("menu..." + menu_item);

                if (menu_item == 1) {

                    saveData(2);
                } else if (menu_item == 2) {
                    saveData(1);
                }
            }
        });

		/*	btnBack.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				((FragmentActivityContainer)getActivity()).backpressedmethod();
			}
		});*/

        Diagexamination.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub

                setSideMenuItembg(1);

                if (FragmentActivityContainer.diag_save_check == 0) {
                    setFragment(1);
                } else {

                    AlertDialog.Builder builder1 = new AlertDialog.Builder(getActivity());
                    builder1.setTitle("SAVE ITEM");
                    builder1.setMessage("Do you want save these items");
                    builder1.setCancelable(true);
                    builder1.setPositiveButton("Yes",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();

                                    btnsubmit.performClick();

                                }
                            });
                    builder1.setNegativeButton("No",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                    onDiagnosisbtn(0);
                                    setFragment(1);


                                }
                            });

                    AlertDialog alert11 = builder1.create();
                    alert11.show();

                }


            }
        });

        Diagexamination.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                // TODO Auto-generated method stub
                //if(FragmentActivityContainer.diag_save_check==0 ||FragmentActivityContainer.diag_save_check==1 ){

                setSideMenuItembg(1);

                if (FragmentActivityContainer.diag_save_check == 0) {
                    setFragment(2);
                } else {

                    AlertDialog.Builder builder1 = new AlertDialog.Builder(getActivity());
                    builder1.setTitle("SAVE ITEM");
                    builder1.setMessage("Do you want save these items");
                    builder1.setCancelable(true);
                    builder1.setPositiveButton("Yes",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();

                                    btnsubmit.performClick();

                                }
                            });
                    builder1.setNegativeButton("No",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                    onDiagnosisbtn(0);
                                    setFragment(2);


                                }
                            });

                    AlertDialog alert11 = builder1.create();
                    alert11.show();

                }

                return false;
            }
        });

       /* DiagexaminationView.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                //if(FragmentActivityContainer.diag_save_check==0 ||FragmentActivityContainer.diag_save_check==1 ){

                setSideMenuItembg(2);

                if (FragmentActivityContainer.diag_save_check == 0) {
                    setFragment(2);
                } else {

                    AlertDialog.Builder builder1 = new AlertDialog.Builder(getActivity());
                    builder1.setTitle("SAVE ITEM");
                    builder1.setMessage("Do you want save these items");
                    builder1.setCancelable(true);
                    builder1.setPositiveButton("Yes",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();

                                    btnsubmit.performClick();

                                }
                            });
                    builder1.setNegativeButton("No",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                    onDiagnosisbtn(0);
                                    setFragment(2);


                                }
                            });

                    AlertDialog alert11 = builder1.create();
                    alert11.show();

                }


            }
        });*/
        labdata.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                //if(FragmentActivityContainer.diag_save_check==0 ||FragmentActivityContainer.diag_save_check==1 ){

                setSideMenuItembg(3);

                if (FragmentActivityContainer.diag_save_check == 0) {
                    setFragment(3);
                } else {

                    AlertDialog.Builder builder1 = new AlertDialog.Builder(getActivity());
                    builder1.setTitle("SAVE ITEM");
                    builder1.setMessage("Do you want save these items");
                    builder1.setCancelable(true);
                    builder1.setPositiveButton("Yes",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();

                                    btnsubmit.performClick();

                                }
                            });
                    builder1.setNegativeButton("No",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                    onDiagnosisbtn(0);
                                    setFragment(3);


                                }
                            });

                    AlertDialog alert11 = builder1.create();
                    alert11.show();

                }


            }
        });
        createDiag.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub

                setSideMenuItembg(4);
                if (FragmentActivityContainer.diag_save_check == 0) {

                    setFragment(4);
                } else {

                    AlertDialog.Builder builder1 = new AlertDialog.Builder(getActivity());
                    builder1.setTitle("SAVE ITEM");
                    builder1.setMessage("Do you want save these items");
                    builder1.setCancelable(true);
                    builder1.setPositiveButton("Yes",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();

                                    btnsubmit.performClick();

                                }
                            });
                    builder1.setNegativeButton("No",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                    onDiagnosisbtn(0);
                                    setFragment(4);


                                }
                            });

                    AlertDialog alert11 = builder1.create();
                    alert11.show();

                }


            }
        });

        createDiag.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                // TODO Auto-generated method stub
                //if(FragmentActivityContainer.diag_save_check==0 ||FragmentActivityContainer.diag_save_check==1 ){

                setSideMenuItembg(4);

                if (FragmentActivityContainer.diag_save_check == 0) {
                    setFragment(5);
                } else {

                    AlertDialog.Builder builder1 = new AlertDialog.Builder(getActivity());
                    builder1.setTitle("SAVE ITEM");
                    builder1.setMessage("Do you want save these items");
                    builder1.setCancelable(true);
                    builder1.setPositiveButton("Yes",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();

                                    btnsubmit.performClick();

                                }
                            });
                    builder1.setNegativeButton("No",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                    onDiagnosisbtn(0);
                                    setFragment(5);

                                }
                            });

                    AlertDialog alert11 = builder1.create();
                    alert11.show();

                }

                return false;
            }
        });

      /*  viewDiag.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                //if(FragmentActivityContainer.diag_save_check==0 ||FragmentActivityContainer.diag_save_check==1 ){

                setSideMenuItembg(5);

                if (FragmentActivityContainer.diag_save_check == 0) {
                    setFragment(5);
                } else {

                    AlertDialog.Builder builder1 = new AlertDialog.Builder(getActivity());
                    builder1.setTitle("SAVE ITEM");
                    builder1.setMessage("Do you want save these items");
                    builder1.setCancelable(true);
                    builder1.setPositiveButton("Yes",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();

                                    btnsubmit.performClick();

                                }
                            });
                    builder1.setNegativeButton("No",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                    onDiagnosisbtn(0);
                                    setFragment(5);

                                }
                            });

                    AlertDialog alert11 = builder1.create();
                    alert11.show();

                }


            }
        });*/


        return view;
    }

    public void setmenuitemvalue(int value) {
        this.menu_item = value;
    }

    public int getmenuitemvalue() {
        return menu_item;

    }

    public void setFragment(int item) {

        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        switch (item) {
            case 1:

                diagnosisExamination = new Diagnosisexamination_Gynacologist();
                fragmentTransaction.replace(R.id.fl_fragment_container, diagnosisExamination);
                fragmentTransaction.commit();

                break;
            case 2:
                Diagnosisexamination_gynacologist_view viewExamFrag = new Diagnosisexamination_gynacologist_view();
                fragmentTransaction.replace(R.id.fl_fragment_container, viewExamFrag);
                fragmentTransaction.commit();

                break;
            case 3:
                Diagnosislab_Gynacologist labDatafrag = new Diagnosislab_Gynacologist();
                fragmentTransaction.replace(R.id.fl_fragment_container, labDatafrag);
                fragmentTransaction.commit();

                break;
            case 4:
                createDiagnosisFrag = new DiagnosisCreate_Gynacologist();
                fragmentTransaction.replace(R.id.fl_fragment_container, createDiagnosisFrag);
                fragmentTransaction.commit();

                break;
            case 5:
                Diagnosis_View_Frag viewdiagFrag = new Diagnosis_View_Frag();
                fragmentTransaction.replace(R.id.fl_fragment_container, viewdiagFrag);
                fragmentTransaction.commit();

                break;

            default:
                break;
        }


    }

    public void setSideMenuItembg(int item) {

        switch (item) {
            case 1:

                Diagexamination.setBackgroundColor(getResources().getColor(R.color.red_color));
                DiagexaminationView.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
                labdata.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
                createDiag.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
                viewDiag.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));

                break;
            case 2:
                Diagexamination.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
                DiagexaminationView.setBackgroundColor(getResources().getColor(R.color.red_color));
                labdata.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
                createDiag.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
                viewDiag.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));

                break;
            case 3:
                Diagexamination.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
                DiagexaminationView.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
                labdata.setBackgroundColor(getResources().getColor(R.color.red_color));
                createDiag.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
                viewDiag.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));

                break;
            case 4:
                Diagexamination.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
                DiagexaminationView.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
                labdata.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
                createDiag.setBackgroundColor(getResources().getColor(R.color.red_color));
                viewDiag.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));

                break;
            case 5:
                Diagexamination.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
                DiagexaminationView.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
                labdata.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
                createDiag.setBackgroundColor(getResources().getColor(R.color.dark_blue_color));
                viewDiag.setBackgroundColor(getResources().getColor(R.color.red_color));

                break;

            default:
                break;
        }

    }


    @Override
    public void onDiagnosisbtn(int arg) {
        // TODO Auto-generated method stub
        System.out.println("arg value" + arg);
        if (Diagnosis_Frag_Gynacologist.this.isVisible()) {
            System.out.println("arg value1" + arg);
            if (arg == 1) {
                btnsubmit.setBackgroundDrawable(getResources().getDrawable(R.drawable.submitbtn_background));
                btnsubmit.setTextColor(getResources().getColor(R.color.actionbar_color));
                btnsubmit.setEnabled(true);
                FragmentActivityContainer.diag_save_check = 2;
            }

            if (arg == 0) {
                btnsubmit.setBackgroundDrawable(getResources().getDrawable(R.drawable.submitbtn_background_beforesave));
                btnsubmit.setTextColor(getResources().getColor(R.color.grey));
                btnsubmit.setEnabled(false);
                FragmentActivityContainer.diag_save_check = 0;
            }
        }
    }
}