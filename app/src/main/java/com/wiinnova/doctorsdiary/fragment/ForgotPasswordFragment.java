package com.wiinnova.doctorsdiary.fragment;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.wiinnova.doctorsdiary.R;
import com.parse.FindCallback;
import com.parse.FunctionCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ForgotPasswordFragment extends Fragment implements OnClickListener{

	private EditText et_email;
	private Button btn_submit;
	private ProgressDialog mProgressDialog;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View v=inflater.inflate(R.layout.forgotpassword_layout_fragment,null);

		Initialize_Components(v);
		Setup_Listener();

		return v;
	}

	private void Setup_Listener() {
		// TODO Auto-generated method stub
		btn_submit.setOnClickListener(this);

	}

	private void Initialize_Components(View v) {
		// TODO Auto-generated method stub
		et_email=(EditText)v.findViewById(R.id.email_address);
		btn_submit=(Button)v.findViewById(R.id.submit);

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.submit:

			forgotpassword();

			break;

		default:
			break;
		}



	}

	private void forgotpassword() {
		// TODO Auto-generated method stub
		boolean validfields=true;
		System.out.println("valid email"+isValidEmail(et_email.getText().toString()));
		System.out.println("length email"+et_email.length());
		if(!isValidEmail(et_email.getText().toString()) || et_email.length()==0 ){
			System.out.println("email workinggggggggg");
			et_email.setError("Enter User Mail ID ");
			et_email.requestFocus();
			validfields=false;
		}else{
			et_email.setError(null);
		}
		
		if(validfields==true){
			mProgressDialog = new ProgressDialog(getActivity());
			mProgressDialog.setMessage("Sending Details....");
			mProgressDialog.show();
			
			
			ParseQuery<ParseObject> query = ParseQuery.getQuery("UserMaster");
			query.whereEqualTo("email", et_email.getText().toString());
			query.findInBackground(new FindCallback<ParseObject>() {
				
				@Override
				public void done(List<ParseObject> object, ParseException e) {
					// TODO Auto-generated method stub
					  mProgressDialog.dismiss();
					if(e==null){
						if(object.size()>0){
							ParseObject details=object.get(0);
							String username=details.getString("firstName");
							String lastname=details.getString("lastName");
							String name=username+" "+lastname;
							String password=details.getString("password");
							
							
							sendconfirmationmail(name,et_email.getText().toString(),password);
							
							Toast.makeText(getActivity(), "Mail sent.", Toast.LENGTH_SHORT).show();
				    		getFragmentManager().beginTransaction().replace(R.id.container, new LoginFragment()).commit();
							
						}else{
							
							 Toast.makeText(getActivity(),"No user registered for this email", Toast.LENGTH_SHORT).show();
						}
					}else{
						 Toast.makeText(getActivity(),"There are some problems.Please Try again later", Toast.LENGTH_SHORT).show();
					}
					
				}
			});
			
			
			
		}
		
	}

	boolean isValidEmail(CharSequence target) {
		if (target == null) {
			return false;
		} else {
			return android.util.Patterns.EMAIL_ADDRESS.matcher(target)
					.matches();
		}
	}
	
	
	
	
	private void sendconfirmationmail(String username, String email, String password)
	{
		// TODO Auto-generated method stub
		Map<String, String> params = new HashMap();
		params.put("username", username);
		params.put("email",email);
		params.put("password",password);
		ParseCloud.callFunctionInBackground("forgotPasswordDD", params, new FunctionCallback<Object>() {
			@Override
			public void done(Object arg0, ParseException arg1) {
				// TODO Auto-generated method stub
				Log.e("send mail", "response: " + arg1);
			}
		});

	}

}
