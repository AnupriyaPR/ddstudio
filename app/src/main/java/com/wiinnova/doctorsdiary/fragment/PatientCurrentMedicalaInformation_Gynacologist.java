package com.wiinnova.doctorsdiary.fragment;

import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.NumberPicker;
import android.widget.NumberPicker.OnValueChangeListener;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.wiinnova.doctorsdiary.R;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.SaveCallback;
import com.wiinnova.doctorsdiary.activities.FragmentActivityContainer;
import com.wiinnova.doctorsdiary.fragment.Patient_Frag.onMedicalhistorysubmit;
import com.wiinnova.doctorsdiary.popupwindow.SpinnerPopup;
import com.wiinnova.doctorsdiary.popupwindow.SpinnerPopup.onSubmitListener;
import com.wiinnova.doctorsdiary.supportclasses.LMPAdapter;

import org.json.JSONArray;
import org.json.JSONException;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

public class PatientCurrentMedicalaInformation_Gynacologist extends Fragment implements onMedicalhistorysubmit {


    public interface onbuttonactivationmedhist {
        void onbuttonmedhist(int arg);
    }

    ListView lstMenstrualPeriod;
    ParseObject patientobject;
    boolean patientflag = true;

    private SpinnerPopup spObj;
    private String[] flowpmp_array;
    private String[] dysmenorrhea_array;


    private TextView UniqueID;
    private TextView patientName;

    private NumberPicker marriageyear;
    private NumberPicker marriagechildren;
    private NumberPicker marriagemiscarriages;

    private EditText menarche;
    //private NumberPicker flowpmp;
    private EditText dysmenorrheapmp;

//    private LinearLayout lladdpmp;
    //private LinearLayout llAddmorepmp;

//    private TextView datelmp;
//    private TextView flowlmp;
//    private TextView dysmenorrhealmp;

//    private CheckBox menstural_regular;
//    private CheckBox menstural_irregular;
//    public static  NumberPicker menstural_days;
//    private NumberPicker menstural_cycle;

    private CheckBox nopresentpasthistory;

    private CheckBox hypercurrent;
    private CheckBox hyperpast;
    private CheckBox hyperna;
    private EditText hypermedication;

    private CheckBox diabcurrent;
    private CheckBox diabpast;
    private CheckBox diabna;
    private EditText diabmedication;

    private CheckBox thyroidcurrent;
    private CheckBox thyroidpast;
    private CheckBox thyroidna;
    private EditText thyroidmedication;

    private CheckBox cystcurrent;
    private CheckBox cystpast;
    private CheckBox cystna;
    private EditText cystmedication;

    private CheckBox endometriosiscurrent;
    private CheckBox endometriosispast;
    private CheckBox endometriosisna;
    private EditText endometriosismedication;

    private CheckBox uterinecurrent;
    private CheckBox uterinepast;
    private CheckBox uterinena;
    private EditText uterinemedication;


    private CheckBox uticurrent;
    private CheckBox utipast;
    private CheckBox utina;
    private EditText utimedication;

    private CheckBox cancercurrent;
    private CheckBox cancerpast;
    private CheckBox cancerna;
    private EditText cancermedication;

    private LinearLayout llAddpresentpast;
    private LinearLayout llAddmorebtn;

    private CheckBox fhyper;
    private CheckBox mhyper;
    private CheckBox shyper;
    private CheckBox gfhyper;
    private CheckBox gmhyper;
    private CheckBox fdia;
    private CheckBox mdia;
    private CheckBox sdia;
    private CheckBox gfdia;
    private CheckBox gmdia;
    private CheckBox fcan;
    private CheckBox mcan;
    private CheckBox scan;
    private CheckBox gfcan;
    private CheckBox gmcan;
    private CheckBox fOther;
    private CheckBox mOther;
    private CheckBox sOther;
    private CheckBox gfOther;
    private CheckBox gmOther;
    private CheckBox fna;
    private CheckBox mna;
    private CheckBox sna;
    private CheckBox gfna;
    private CheckBox gmna;
    private EditText fOedittext;
    private EditText mOedittext;
    private EditText sOedittext;
    private EditText gfOedittext;
    private EditText gmOedittext;


    private EditText medicOne;
    private EditText medicTwo;
    private EditText reactionOne;
    private EditText reactionTwo;
    private EditText surgeryOne;
    private EditText surgeryTwo;
    private CheckBox noAlerReport;
    //private CheckBox noIllReport;
    private CheckBox noSurgeryReport;
    private LinearLayout addsurgery;
    private LinearLayout addmoresurgery;

    private CheckBox casein;
    private CheckBox egg;
    private CheckBox fish;
    private CheckBox milk;
    private CheckBox nut;
    private CheckBox shellfish;
    private CheckBox sulfite;
    private CheckBox soy;
    private CheckBox wheat;
    private CheckBox other;
    private EditText etallergyOther1;
    private CheckBox spring;
    private CheckBox summer;
    private CheckBox fall;
    private CheckBox winter;
    private CheckBox bee;
    private CheckBox cat;
    private CheckBox dog;
    private CheckBox insect;
    private CheckBox other2;
    private EditText etallergyOther2;
    private CheckBox dust;
    private CheckBox mold;
    private CheckBox plant;
    private CheckBox pollen;
    private CheckBox sun;
    private LinearLayout addallergy;
    private LinearLayout addallergybtn;
    private CheckBox alcurr;
    private CheckBox alpast;
    private CheckBox alna;
    private CheckBox tobcurr;
    private CheckBox tobpast;
    private CheckBox tobna;
    private CheckBox osubcurr;
    private CheckBox osubpast;
    private CheckBox osubna;
    private CheckBox tobchewablecurr;
    private CheckBox tobchewablepast;
    private CheckBox tobchewablena;
    private EditText otherhistory;


    private boolean patientedit_checkflag;
    private boolean admintab;
    private boolean admintabdefault;


    private DatePickerDialog startDatePickerDialog;
    private DatePickerDialog startDatePickerDialoginflate;
    public SimpleDateFormat dateFormatter;
    private onSubmitListener spinnerpopupListener;
    private int flag;

    int dateposition = 0;
    private int datetextViewposition;
    private int pmpflowtextviewposition;
    private int pmpdysmenpmptextviewposition;

    private int lmpdatetextViewposition;
    private int lmpflowtextviewposition;
    private int lmpdysmenpmptextviewposition;

    ArrayList<Integer> listCount = new ArrayList<Integer>();


    ArrayList<TextView> pmpDate_Arraylist = new ArrayList<TextView>();
    ArrayList<TextView> pmpFlow_ArrayList = new ArrayList<TextView>();
    ArrayList<TextView> pmpDysmen_ArrayList = new ArrayList<TextView>();
    ArrayList<TextView> lmpDate_Arraylist = new ArrayList<TextView>();
    ArrayList<TextView> lmpFlow_ArrayList = new ArrayList<TextView>();
    ArrayList<TextView> lmpDysmen_ArrayList = new ArrayList<TextView>();

    ArrayList<String> majorillArraylist = new ArrayList<String>();
    ArrayList<String> majorillstatusArraylist = new ArrayList<String>();


    ArrayList<TextView> pmpdateArraylist = new ArrayList<TextView>();
    private ArrayList<TextView> pmpflowArraylist = new ArrayList<TextView>();
    private ArrayList<TextView> pmpdysmenpmpArraylist = new ArrayList<TextView>();
    public Calendar newCalendar;
    //    private LinearLayout lladdlmp;
    private LinearLayout llAddmorelmp;
    int lmpposition = 0;
    protected ArrayList<TextView> lmpdateArraylist = new ArrayList<TextView>();
    protected ArrayList<TextView> lmpflowArraylist = new ArrayList<TextView>();
    protected ArrayList<TextView> lmpdysmenpmpArraylist = new ArrayList<TextView>();

    int presentpastposition = 0;
    protected ArrayList<CheckBox> presentpastnaarraylist = new ArrayList<CheckBox>();
    protected ArrayList<CheckBox> currentarraylist = new ArrayList<CheckBox>();
    protected ArrayList<CheckBox> pastarraylist = new ArrayList<CheckBox>();
    protected ArrayList<EditText> medicationarraylist = new ArrayList<EditText>();
    protected ArrayList<TextView> serialnumberArraylist = new ArrayList<TextView>();
    protected ArrayList<RelativeLayout> removeArraylist = new ArrayList<RelativeLayout>();
    protected ArrayList<EditText> diseasenamearraylist = new ArrayList<EditText>();
    Integer presentpastnaposition;

    int surgeryposition = 0;
    protected ArrayList<EditText> surgeryoneArraylist = new ArrayList<EditText>();
    protected ArrayList<EditText> surgerytwoArraylist = new ArrayList<EditText>();
    protected ArrayList<TextView> serialnumberoneArraylist = new ArrayList<TextView>();
    protected ArrayList<TextView> serialnumbertwoArraylist = new ArrayList<TextView>();
    protected ArrayList<RelativeLayout> surgeryremoveArraylist = new ArrayList<RelativeLayout>();
    Integer surgeryclickposition;


    int drugposition = 0;
    protected ArrayList<EditText> medicationdrugArraylist = new ArrayList<EditText>();
    protected ArrayList<EditText> reactiondrugArraylist = new ArrayList<EditText>();
    protected ArrayList<TextView> serialnumbermedArraylist = new ArrayList<TextView>();
    protected ArrayList<TextView> serialnumberreacArraylist = new ArrayList<TextView>();
    protected ArrayList<RelativeLayout> drugremoveArraylist = new ArrayList<RelativeLayout>();
    Integer drugclickposition;

    protected ArrayList<RelativeLayout> lmpremoveArraylist = new ArrayList<RelativeLayout>();
    protected ArrayList<RelativeLayout> pmpremoveArraylist = new ArrayList<RelativeLayout>();


    JSONArray pmp_array = new JSONArray();
    JSONArray lmp_array = new JSONArray();

    ArrayList<String> menarchelist = new ArrayList<String>();
    ArrayList<String> dysmenorrheapmplist = new ArrayList<String>();

    public static ArrayList<String> menstrualregularlist = new ArrayList<String>(1);
    public static ArrayList<String> menstrualdayslist = new ArrayList<String>(1);
    public static ArrayList<String> menstrualcyclelist = new ArrayList<String>(1);


    ArrayList<String> marriageyearlist = new ArrayList<String>();
    ArrayList<String> marriagechildrenlist = new ArrayList<String>();
    ArrayList<String> marriagemiscariageslist = new ArrayList<String>();


    public static ArrayList<String> pmpdate_array = new ArrayList<String>();
    ArrayList<String> pmpflow_array = new ArrayList<String>();
    ArrayList<String> pmpdysmenorrhea_array = new ArrayList<String>();


    ArrayList<String> otherhistory_array = new ArrayList<String>();
    public static ArrayList<String> lmpdate_array = new ArrayList<String>(1);
    public static ArrayList<String> lmpflow_array = new ArrayList<String>(1);
    public static ArrayList<String> lmpdysmenorrhea_array = new ArrayList<String>(1);


    ArrayList<String> majorill_array = new ArrayList<String>();
    ArrayList<String> majorillstatus_array = new ArrayList<String>();
    JSONArray familyistory_array = null;
    ArrayList<String> familyistorystatus_array = new ArrayList<String>();
    ArrayList<String> surgery_arraylist = new ArrayList<String>();
    ArrayList<String> allergy_array = new ArrayList<String>();
    ArrayList<String> medication_array = new ArrayList<String>();
    ArrayList<String> reaction_array = new ArrayList<String>();
    ArrayList<String> socialhistorystatus_array = new ArrayList<String>();
    String[] socHistStatArray = null;
    String[] allergyArray = null;

    JSONArray socialhistory_array = null;


    private String[] millfamStatArray = null;
    private String[] millfamArray = null;
    private ProgressDialog mProgressDialog;
    private String patientId;
    private DatePickerDialog startDatePickerDialog1;


    private Patient_Frag patientfragObj;

    private JSONArray menstrualregular_resultArray = new JSONArray();
    private JSONArray menstrualdayslist_resultArray = new JSONArray();
    private JSONArray menstrualcyclelist_resultArray = new JSONArray();

    private JSONArray menarche_resultArray = new JSONArray();
    private JSONArray dysmenorrheapmp_resultArray = new JSONArray();
    private JSONArray marriageyear_resultArray = new JSONArray();
    private JSONArray marriagechildren_resultArray = new JSONArray();
    private JSONArray marriagemiscariages_resultArray = new JSONArray();

    private JSONArray pmpdate_resultArray = new JSONArray();
    private JSONArray pmpflow_resultArray = new JSONArray();
    private JSONArray pmpdysmenorrhea_resultarray = new JSONArray();
    private JSONArray lmpdate_resultArray = new JSONArray();
    private JSONArray otherhistory_resultArray = new JSONArray();
    private JSONArray lmpflow_resultArray = new JSONArray();
    private JSONArray lmpdysmenorrhea_resultarray = new JSONArray();
    private JSONArray majorillnessArray = new JSONArray();
    private JSONArray majorillnessstatusArray = new JSONArray();
    private JSONArray majorillnessfamilyArray = new JSONArray();
    private JSONArray majorillnessfamilystatusArray = new JSONArray();
    private JSONArray surgery_resultArray = new JSONArray();
    private JSONArray allergyhistory_resultArray = new JSONArray();
    private JSONArray allergymedication_resultArray = new JSONArray();
    private JSONArray allergyreaction_resultArray = new JSONArray();
    private JSONArray socialhistory_resultArray = new JSONArray();
    private JSONArray socialhistorystatus_resultArray = new JSONArray();


    ArrayList<String> pmpdatearraylist = new ArrayList<String>();
    ArrayList<String> pmpflowarraylist = new ArrayList<String>();
    ArrayList<String> pmpdysmenorrheaarraylist = new ArrayList<String>();

    ArrayList<String> lmpdatearraylist = new ArrayList<String>();
    ArrayList<String> lmpflowarraylist = new ArrayList<String>();
    ArrayList<String> lmpdysmenorrheaarraylist = new ArrayList<String>();


    /*ArrayList<String> allmedicArray =new ArrayList<String>();
    ArrayList<String> allreacArray = new ArrayList<String>();
	ArrayList<String> majillArray = new ArrayList<String>();
	ArrayList<String> majillStaArray = new ArrayList<String>();
	ArrayList<String> surgArray = new ArrayList<String>();*/
    private JSONArray ar_shistarray = new JSONArray();
    private JSONArray ar_infamarray = new JSONArray();
    private JSONArray ar_allergyarray = new JSONArray();
    private int medicindex;
    private LinearLayout mainlayout;
    private ScrollView scroll;
    private CheckBox hyperthyroidcurrent;
    private CheckBox hyperthyroidpast;
    private CheckBox hyperthyroidna;
    private EditText hyperthyroidmedication;
    LMPAdapter lmpAdapter;

    public void justifyListViewHeightBasedOnChildren(ListView listView) {

        ListAdapter adapter = listView.getAdapter();

        if (adapter == null) {
            return;
        }
        ViewGroup vg = listView;
        int totalHeight = 0;
        for (int i = 0; i < adapter.getCount(); i++) {
            View listItem = adapter.getView(i, null, vg);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }

        ViewGroup.LayoutParams par = listView.getLayoutParams();
        par.height = totalHeight + (listView.getDividerHeight() * (adapter.getCount() - 1));
        listView.setLayoutParams(par);
        listView.requestLayout();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        View v = inflater.inflate(R.layout.patientmedicalhistory_gynecologist, null);


        Bundle bundle = getArguments();
        patientflag = bundle.getBoolean("patientflag");
        admintab = bundle.getBoolean("admintab");


        patientfragObj = new Patient_Frag();
        Bundle bundle1 = new Bundle();
        bundle1.putInt("sidemenuitem", 2);
        bundle1.putBoolean("patientflag", patientflag);
        patientfragObj.setArguments(bundle1);

        getFragmentManager().beginTransaction()
                .replace(R.id.fl_sidemenu_container, patientfragObj)
                .commit();

        if (patientfragObj != null) {
            ((FragmentActivityContainer) getActivity()).setpatientfrag(patientfragObj);
        }


        flowpmp_array = getResources().getStringArray(R.array.flowpmp);
        dysmenorrhea_array = getResources().getStringArray(R.array.dysmenorrhea);

        ///////////////////////Set adapter
        lstMenstrualPeriod = (ListView) v.findViewById(R.id.lstMenstrualPeriod);
        listCount = new ArrayList<Integer>();
        listCount.add(0);

        lmpAdapter = new LMPAdapter(getActivity(), listCount, this);
        lstMenstrualPeriod.setAdapter(lmpAdapter);
        justifyListViewHeightBasedOnChildren(lstMenstrualPeriod);

        ////////////////////////////////////////////////////////
        mainlayout = (LinearLayout) v.findViewById(R.id.llmainlayout);
        scroll = (ScrollView) v.findViewById(R.id.scrollview);

        UniqueID = (TextView) v.findViewById(R.id.cduniqueid);
        patientName = (TextView) v.findViewById(R.id.tvname);

        marriageyear = (NumberPicker) v.findViewById(R.id.yearPicker);
        marriagechildren = (NumberPicker) v.findViewById(R.id.childrenicker);
        marriagemiscarriages = (NumberPicker) v.findViewById(R.id.miscarriagespicker);

        menarche = (EditText) v.findViewById(R.id.datepmp);
        //flowpmp=(TextView)v.findViewById(R.id.flowpmp);
        dysmenorrheapmp = (EditText) v.findViewById(R.id.dysmenorrheapmp);

//        lladdpmp = (LinearLayout) v.findViewById(R.id.lladdpmp);
        //llAddmorepmp=(LinearLayout)v.findViewById(R.id.llAddmorepmp);

//        lladdlmp = (LinearLayout) v.findViewById(R.id.lladdlmp);
        llAddmorelmp = (LinearLayout) v.findViewById(R.id.llAddmorelmp);

//        datelmp = (TextView) v.findViewById(R.id.datelmp);
//        flowlmp = (TextView) v.findViewById(R.id.flowlmp);
//        dysmenorrhealmp = (TextView) v.findViewById(R.id.dysmenorrhealmp);


//        menstural_regular = (CheckBox) v.findViewById(R.id.cbregular);
//        menstural_irregular = (CheckBox) v.findViewById(R.id.cbirregular);

//        menstural_days = (NumberPicker) v.findViewById(R.id.dayspicker);
//        menstural_cycle = (NumberPicker) v.findViewById(R.id.cyclepicker);

        nopresentpasthistory = (CheckBox) v.findViewById(R.id.nopresentpasthistory);

        hypercurrent = (CheckBox) v.findViewById(R.id.cbcurrent_1);
        hyperpast = (CheckBox) v.findViewById(R.id.cbpast_1);
        hyperna = (CheckBox) v.findViewById(R.id.cbna_1);
        hypermedication = (EditText) v.findViewById(R.id.etmedication_1);


        diabcurrent = (CheckBox) v.findViewById(R.id.cbcurrent_2);
        diabpast = (CheckBox) v.findViewById(R.id.cbpast_2);
        diabna = (CheckBox) v.findViewById(R.id.cbna_2);
        diabmedication = (EditText) v.findViewById(R.id.etmedication_2);

        thyroidcurrent = (CheckBox) v.findViewById(R.id.cbcurrent_3);
        thyroidpast = (CheckBox) v.findViewById(R.id.cbpast_3);
        thyroidna = (CheckBox) v.findViewById(R.id.cbna_3);
        thyroidmedication = (EditText) v.findViewById(R.id.etmedication_3);


        hyperthyroidcurrent = (CheckBox) v.findViewById(R.id.cbhcurrent_3);
        hyperthyroidpast = (CheckBox) v.findViewById(R.id.cbhpast_3);
        hyperthyroidna = (CheckBox) v.findViewById(R.id.cbhna_3);
        hyperthyroidmedication = (EditText) v.findViewById(R.id.ethmedication_3);


        cystcurrent = (CheckBox) v.findViewById(R.id.cbcurrent_4);
        cystpast = (CheckBox) v.findViewById(R.id.cbpast_4);
        cystna = (CheckBox) v.findViewById(R.id.cbna_4);
        cystmedication = (EditText) v.findViewById(R.id.etmedication_4);


        endometriosiscurrent = (CheckBox) v.findViewById(R.id.cbcurrent_5);
        endometriosispast = (CheckBox) v.findViewById(R.id.cbpast_5);
        endometriosisna = (CheckBox) v.findViewById(R.id.cbna_5);
        endometriosismedication = (EditText) v.findViewById(R.id.etmedication_5);

        uterinecurrent = (CheckBox) v.findViewById(R.id.cbcurrent_6);
        uterinepast = (CheckBox) v.findViewById(R.id.cbpast_6);
        uterinena = (CheckBox) v.findViewById(R.id.cbna_6);
        uterinemedication = (EditText) v.findViewById(R.id.etmedication_6);


        uticurrent = (CheckBox) v.findViewById(R.id.cbcurrent_7);
        utipast = (CheckBox) v.findViewById(R.id.cbpast_7);
        utina = (CheckBox) v.findViewById(R.id.cbna_7);
        utimedication = (EditText) v.findViewById(R.id.etmedication_7);

        cancercurrent = (CheckBox) v.findViewById(R.id.cbcurrent_8);
        cancerpast = (CheckBox) v.findViewById(R.id.cbpast_8);
        cancerna = (CheckBox) v.findViewById(R.id.cbna_8);
        cancermedication = (EditText) v.findViewById(R.id.etmedication_8);

        llAddpresentpast = (LinearLayout) v.findViewById(R.id.lladdmoreillness);
        llAddmorebtn = (LinearLayout) v.findViewById(R.id.llAddmorebutton);

        fhyper = (CheckBox) v.findViewById(R.id.pmhsfhyper);
        fdia = (CheckBox) v.findViewById(R.id.pmhsfdia);
        fcan = (CheckBox) v.findViewById(R.id.pmhsfcancer);
        fOther = (CheckBox) v.findViewById(R.id.pmhsfother);
        fna = (CheckBox) v.findViewById(R.id.pmhsna);

        mhyper = (CheckBox) v.findViewById(R.id.pmhsmhyper);
        mdia = (CheckBox) v.findViewById(R.id.pmhsmdia);
        mcan = (CheckBox) v.findViewById(R.id.pmhsmcancer);
        mOther = (CheckBox) v.findViewById(R.id.pmhssmother);
        mna = (CheckBox) v.findViewById(R.id.pmhsmna);

        shyper = (CheckBox) v.findViewById(R.id.pmhsshyper);
        sdia = (CheckBox) v.findViewById(R.id.pmhssdia);
        scan = (CheckBox) v.findViewById(R.id.pmhsscancer);
        sOther = (CheckBox) v.findViewById(R.id.pmhssother);
        sna = (CheckBox) v.findViewById(R.id.pmhssna);

        gfhyper = (CheckBox) v.findViewById(R.id.pmhsgfhyper);
        gfdia = (CheckBox) v.findViewById(R.id.pmhsgfdia);
        gfcan = (CheckBox) v.findViewById(R.id.pmhsgfcancer);
        gfOther = (CheckBox) v.findViewById(R.id.pmhsgfother);
        gfna = (CheckBox) v.findViewById(R.id.pmhsgfna);

        gmhyper = (CheckBox) v.findViewById(R.id.pmhsgmhyper);
        gmdia = (CheckBox) v.findViewById(R.id.pmhsgmdia);
        gmcan = (CheckBox) v.findViewById(R.id.pmhsgmcancer);
        gmOther = (CheckBox) v.findViewById(R.id.pmhsgmmother);
        gmna = (CheckBox) v.findViewById(R.id.pmhsgmna);

        fOedittext = (EditText) v.findViewById(R.id.pmhsfotheredit);
        mOedittext = (EditText) v.findViewById(R.id.pmhsmotheredit);
        sOedittext = (EditText) v.findViewById(R.id.pmhssotheredit);
        gfOedittext = (EditText) v.findViewById(R.id.pmhsgfotheredit);
        gmOedittext = (EditText) v.findViewById(R.id.pmhsgmotheredit);


        medicOne = (EditText) v.findViewById(R.id.pmhmed1);
        medicTwo = (EditText) v.findViewById(R.id.pmhmed2);
        reactionOne = (EditText) v.findViewById(R.id.pmhreac1);
        reactionTwo = (EditText) v.findViewById(R.id.pmhreac2);
        surgeryOne = (EditText) v.findViewById(R.id.pmhsur1);
        surgeryTwo = (EditText) v.findViewById(R.id.pmhsur2);

        noAlerReport = (CheckBox) v.findViewById(R.id.pmhaller);
        noSurgeryReport = (CheckBox) v.findViewById(R.id.pmhsurg);

        addsurgery = (LinearLayout) v.findViewById(R.id.lladdsurgery);
        addmoresurgery = (LinearLayout) v.findViewById(R.id.llAddmoresurgerybutton);


        casein = (CheckBox) v.findViewById(R.id.cbcasein);
        egg = (CheckBox) v.findViewById(R.id.cbegg);
        fish = (CheckBox) v.findViewById(R.id.cbfish);
        milk = (CheckBox) v.findViewById(R.id.cbmilk);
        nut = (CheckBox) v.findViewById(R.id.cbnut);
        shellfish = (CheckBox) v.findViewById(R.id.cbshellfish);
        sulfite = (CheckBox) v.findViewById(R.id.cbsulfite);
        soy = (CheckBox) v.findViewById(R.id.cbsoy);
        wheat = (CheckBox) v.findViewById(R.id.cbwheat);
        other = (CheckBox) v.findViewById(R.id.cbother);
        etallergyOther1 = (EditText) v.findViewById(R.id.otheredit);
        spring = (CheckBox) v.findViewById(R.id.cbspring);
        summer = (CheckBox) v.findViewById(R.id.cbsummer);
        fall = (CheckBox) v.findViewById(R.id.cbfall);
        winter = (CheckBox) v.findViewById(R.id.cbwinter);
        bee = (CheckBox) v.findViewById(R.id.cbbee);
        cat = (CheckBox) v.findViewById(R.id.cbcat);
        dog = (CheckBox) v.findViewById(R.id.cbdog);
        insect = (CheckBox) v.findViewById(R.id.cbinseect);
        other2 = (CheckBox) v.findViewById(R.id.cbotheranimal);
        etallergyOther2 = (EditText) v.findViewById(R.id.etotheranimal);
        dust = (CheckBox) v.findViewById(R.id.cbdust);
        mold = (CheckBox) v.findViewById(R.id.cbmold);
        plant = (CheckBox) v.findViewById(R.id.cbplant);
        pollen = (CheckBox) v.findViewById(R.id.cbpollen);
        sun = (CheckBox) v.findViewById(R.id.cbsun);

        addallergy = (LinearLayout) v.findViewById(R.id.addallergy);
        addallergybtn = (LinearLayout) v.findViewById(R.id.lladd_allergy);

        alcurr = (CheckBox) v.findViewById(R.id.pmhsalcurr);
        alpast = (CheckBox) v.findViewById(R.id.pmhsalpast);
        alna = (CheckBox) v.findViewById(R.id.pmhsalna);

        tobcurr = (CheckBox) v.findViewById(R.id.pmhstobcurr);
        tobpast = (CheckBox) v.findViewById(R.id.pmhstobpast);
        tobna = (CheckBox) v.findViewById(R.id.pmhstobna);


        tobchewablecurr = (CheckBox) v.findViewById(R.id.pmhstobchcurr);
        tobchewablepast = (CheckBox) v.findViewById(R.id.pmhstobchpast);
        tobchewablena = (CheckBox) v.findViewById(R.id.pmhstobchna);

        osubcurr = (CheckBox) v.findViewById(R.id.pmhsotrcurr);
        osubpast = (CheckBox) v.findViewById(R.id.pmhsotrpast);
        osubna = (CheckBox) v.findViewById(R.id.pmhsotrna);

        otherhistory = (EditText) v.findViewById(R.id.etotherhistory);

        dateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.US);


        otherhistory.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
                // TODO Auto-generated method stub

            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
                submitButtonActivation();
            }
        });


        try {
            ((FragmentActivityContainer) getActivity()).setPatientcurrentmedinfo_gynacologist(PatientCurrentMedicalaInformation_Gynacologist.this);

            ((FragmentActivityContainer) getActivity()).getpatientfrag().menu_item = 2;

            patientedit_checkflag = ((FragmentActivityContainer) getActivity()).getpatientflagvalue();
            admintab = ((FragmentActivityContainer) getActivity()).getcheckadmintab();
            admintabdefault = ((FragmentActivityContainer) getActivity()).getadmintabdefault();

            if (admintab == true) {
                patientobject = ((FragmentActivityContainer) getActivity()).getOldPatientobject();
            } else if (admintab == false && admintabdefault == false) {
                patientobject = ((FragmentActivityContainer) getActivity()).getPatientobject();
            } else if (admintab == false && admintabdefault == true) {
                patientobject = ((FragmentActivityContainer) getActivity()).getOldPatientobject();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (patientedit_checkflag == false) {
            patientflag = false;
        }

        if (patientflag == true && patientobject != null) {
            System.out.println("activation falg value" + FragmentActivityContainer.check_save);
            patientId = patientobject.getString("patientID");
            String fname = patientobject.getString("firstName");
            String lname = patientobject.getString("lastName");
            System.out.println("name........" + fname);
            if (fname == null) {
                fname = "FirstName";
            }
            if (lname == null) {
                lname = "LastName";
            }
            patientName.setText(fname + " " + lname);
            UniqueID.setText(patientId);
        }


        SharedPreferences scanpidnew = getActivity().getSharedPreferences("NewPatID", 0);
        String scnewPid = scanpidnew.getString("NewID", null);


        if (patientobject == null) {
            if (scnewPid != null) {
                UniqueID.setText(scnewPid);
                SharedPreferences fnname1 = getActivity().getSharedPreferences("fnName", 0);
                patientName.setText(fnname1.getString("fNname", "FirstName" + " " + "LastName"));

                patientId = scnewPid;
            }
        } else {
            SharedPreferences fnname1 = getActivity().getSharedPreferences("fnName", 0);
            patientName.setText(fnname1.getString("fNname", "FirstName" + " " + "LastName"));
            UniqueID.setText(patientobject.getString("patientID"));

            patientId = patientobject.getString("patientID");
        }


        marriageyear.setMaxValue(100);
        marriageyear.setMinValue(0);


        marriageyear.setOnValueChangedListener(new OnValueChangeListener() {

            @Override
            public void onValueChange(NumberPicker arg0, int arg1, int arg2) {
                // TODO Auto-generated method stub
                submitButtonActivation();
            }
        });


        marriagechildren.setMaxValue(10);
        marriagechildren.setMinValue(0);

        marriagechildren.setOnValueChangedListener(new OnValueChangeListener() {

            @Override
            public void onValueChange(NumberPicker arg0, int arg1, int arg2) {
                // TODO Auto-generated method stub
                submitButtonActivation();
            }
        });
        marriagemiscarriages.setMaxValue(10);
        marriagemiscarriages.setMinValue(0);


        marriagemiscarriages.setOnValueChangedListener(new OnValueChangeListener() {

            @Override
            public void onValueChange(NumberPicker arg0, int arg1, int arg2) {
                // TODO Auto-generated method stub
                submitButtonActivation();
            }
        });


//        menstural_days.setMaxValue(30);
//        menstural_days.setMinValue(0);
//
//
//        menstural_days.setOnValueChangedListener(new OnValueChangeListener() {
//
//            @Override
//            public void onValueChange(NumberPicker arg0, int arg1, int arg2) {
//                // TODO Auto-generated method stub
//                submitButtonActivation();
//            }
//        });

//        menstural_cycle.setMaxValue(30);
//        menstural_cycle.setMinValue(0);
//
//        menstural_cycle.setOnValueChangedListener(new OnValueChangeListener() {
//
//            @Override
//            public void onValueChange(NumberPicker arg0, int arg1, int arg2) {
//                // TODO Auto-generated method stub
//                submitButtonActivation();
//            }
//        });
//
//        menarche.setMaxValue(100);
//        menarche.setMinValue(0);
//        dysmenorrheapmp.setMaxValue(100);
//        dysmenorrheapmp.setMinValue(0);
//
        menarche.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                submitButtonActivation();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        dysmenorrheapmp.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                submitButtonActivation();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        mainlayout.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                hideKeyboard(v);
            }
        });

        scroll.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                hideKeyboard(v);

            }
        });


        spinnerpopupListener = new onSubmitListener() {


            @Override
            public void onSubmit(String arg, int position) {
                // TODO Auto-generated method stub

                submitButtonActivation();

				/*if(flag==0){
                    flowpmp.setText(arg);
				}*/
                /*else if(flag==1){
                    dysmenorrheapmp.setText(arg);
				}else*/
//                if (flag == 2) {
//                    flowlmp.setText(arg);
//                }
//                 if (flag == 3) {
//                    dysmenorrhealmp.setText(arg);
//                } else
                if (flag == 4) {
                    pmpflowArraylist.get(pmpflowtextviewposition).setText(arg);

                } else if (flag == 5) {
                    pmpdysmenpmpArraylist.get(pmpdysmenpmptextviewposition).setText(arg);
                } else if (flag == 6) {

                    lmpflowArraylist.get(lmpflowtextviewposition).setText(arg);

                } else if (flag == 7) {
                    lmpdysmenpmpArraylist.get(lmpdysmenpmptextviewposition).setText(arg);
                }
                spObj.dismiss();
            }
        };


        newCalendar = Calendar.getInstance();


        dysmenorrheapmp.setOnClickListener(new OnClickListener() {


            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub				
                flag = 1;
                Bundle bundle = new Bundle();
                bundle.putString("name", "Dysmenorrhea");
                bundle.putStringArray("items", dysmenorrhea_array);
                spObj = new SpinnerPopup();
                spObj.setSubmitListener(spinnerpopupListener);
                spObj.setArguments(bundle);
                spObj.show(getFragmentManager(), "tag");
            }
        });


        llAddmorelmp.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                addMoreLmp();

            }
        });


//        menstural_regular.setOnClickListener(new OnClickListener() {
//
//            @Override
//            public void onClick(View arg0) {
//                // TODO Auto-generated method stub
//
//                submitButtonActivation();
//
//                if (menstural_regular.isChecked()) {
//                    menstural_irregular.setChecked(false);
//                }
//
//
//            }
//        });
//
//        menstural_irregular.setOnClickListener(new OnClickListener() {
//
//            @Override
//            public void onClick(View arg0) {
//                // TODO Auto-generated method stub
//
//                submitButtonActivation();
//
//                if (menstural_irregular.isChecked()) {
//                    menstural_regular.setChecked(false);
//                }
//
//
//            }
//        });

        nopresentpasthistory.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub

                submitButtonActivation();

                if (nopresentpasthistory.isChecked()) {

                    hypercurrent.setChecked(false);
                    hyperpast.setChecked(false);
                    hyperna.setChecked(false);
                    hypermedication.setEnabled(false);

                    hypercurrent.setClickable(false);
                    hyperpast.setClickable(false);
                    hyperna.setClickable(false);
                    hypermedication.setText("");


                    diabcurrent.setChecked(false);
                    diabpast.setChecked(false);
                    diabna.setChecked(false);
                    diabmedication.setEnabled(false);

                    diabcurrent.setClickable(false);
                    diabpast.setClickable(false);
                    diabna.setClickable(false);
                    diabmedication.setText("");


                    thyroidcurrent.setChecked(false);
                    thyroidpast.setChecked(false);
                    thyroidna.setChecked(false);
                    thyroidmedication.setEnabled(false);


                    thyroidcurrent.setClickable(false);
                    thyroidpast.setClickable(false);
                    thyroidna.setClickable(false);
                    thyroidmedication.setText("");


                    hyperthyroidcurrent.setChecked(false);
                    hyperthyroidpast.setChecked(false);
                    hyperthyroidna.setChecked(false);
                    hyperthyroidmedication.setEnabled(false);


                    hyperthyroidcurrent.setClickable(false);
                    hyperthyroidpast.setClickable(false);
                    hyperthyroidna.setClickable(false);
                    hyperthyroidmedication.setText("");


                    cystcurrent.setChecked(false);
                    cystpast.setChecked(false);
                    cystna.setChecked(false);
                    cystmedication.setEnabled(false);

                    cystcurrent.setClickable(false);
                    cystpast.setClickable(false);
                    cystna.setClickable(false);
                    cystmedication.setText("");

                    endometriosiscurrent.setChecked(false);
                    endometriosispast.setChecked(false);
                    endometriosisna.setChecked(false);
                    endometriosismedication.setEnabled(false);


                    endometriosiscurrent.setClickable(false);
                    endometriosispast.setClickable(false);
                    endometriosisna.setClickable(false);
                    endometriosismedication.setText("");


                    uterinecurrent.setChecked(false);
                    uterinepast.setChecked(false);
                    uterinena.setChecked(false);
                    uterinemedication.setEnabled(false);


                    uterinecurrent.setClickable(false);
                    uterinepast.setClickable(false);
                    uterinena.setClickable(false);
                    uterinemedication.setText("");


                    uticurrent.setChecked(false);
                    utipast.setChecked(false);
                    utina.setChecked(false);
                    utimedication.setEnabled(false);


                    uticurrent.setClickable(false);
                    utipast.setClickable(false);
                    utina.setClickable(false);
                    utimedication.setText("");


                    cancercurrent.setChecked(false);
                    cancerpast.setChecked(false);
                    cancerna.setChecked(false);
                    cancermedication.setEnabled(false);


                    cancercurrent.setClickable(false);
                    cancerpast.setClickable(false);
                    cancerna.setClickable(false);
                    cancermedication.setText("");

                    llAddpresentpast.removeAllViews();
                    llAddmorebtn.setClickable(false);


                } else {

                    llAddmorebtn.setClickable(true);

                    hypermedication.setEnabled(true);


                    hypercurrent.setClickable(true);
                    hyperpast.setClickable(true);
                    hyperna.setClickable(true);


                    diabmedication.setEnabled(true);


                    diabcurrent.setClickable(true);
                    diabpast.setClickable(true);
                    diabna.setClickable(true);


                    thyroidmedication.setEnabled(true);


                    thyroidcurrent.setClickable(true);
                    thyroidpast.setClickable(true);
                    thyroidna.setClickable(true);


                    hyperthyroidmedication.setEnabled(true);


                    hyperthyroidcurrent.setClickable(true);
                    hyperthyroidpast.setClickable(true);
                    hyperthyroidna.setClickable(true);


                    cystmedication.setEnabled(true);

                    cystcurrent.setClickable(true);
                    cystpast.setClickable(true);
                    cystna.setClickable(true);


                    endometriosismedication.setEnabled(true);

                    endometriosiscurrent.setClickable(true);
                    endometriosispast.setClickable(true);
                    endometriosisna.setClickable(true);


                    uterinemedication.setEnabled(true);


                    uterinecurrent.setClickable(true);
                    uterinepast.setClickable(true);
                    uterinena.setClickable(true);


                    utimedication.setEnabled(true);


                    uticurrent.setClickable(true);
                    utipast.setClickable(true);
                    utina.setClickable(true);


                    cancercurrent.setClickable(true);
                    cancerpast.setClickable(true);
                    cancerna.setClickable(true);
                    cancermedication.setEnabled(true);


					/*cancercurrent.setClickable(true);
                    cancerpast.setClickable(true);
					cancerna.setClickable(true);
					 */

                }


            }
        });


        hyperna.setOnCheckedChangeListener(new MyCheckedChangeListener(1, 0));
        diabna.setOnCheckedChangeListener(new MyCheckedChangeListener(2, 0));
        thyroidna.setOnCheckedChangeListener(new MyCheckedChangeListener(3, 0));
        cystna.setOnCheckedChangeListener(new MyCheckedChangeListener(4, 0));
        endometriosisna.setOnCheckedChangeListener(new MyCheckedChangeListener(5, 0));
        uterinena.setOnCheckedChangeListener(new MyCheckedChangeListener(6, 0));
        utina.setOnCheckedChangeListener(new MyCheckedChangeListener(7, 0));
        cancerna.setOnCheckedChangeListener(new MyCheckedChangeListener(8, 0));
        hyperthyroidna.setOnCheckedChangeListener(new MyCheckedChangeListener(9, 0));


        cancerna.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                // TODO Auto-generated method stub
                if (cancerna.isChecked()) {
                    submitButtonActivation();
                } else {
                    submitButtonDeactivation();
                }


            }
        });


        utina.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                // TODO Auto-generated method stub
                if (utina.isChecked()) {
                    submitButtonActivation();
                } else {
                    submitButtonDeactivation();
                }


            }
        });


        uterinena.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                // TODO Auto-generated method stub
                if (uterinena.isChecked()) {
                    submitButtonActivation();
                } else {
                    submitButtonDeactivation();
                }


            }
        });


        endometriosisna.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                // TODO Auto-generated method stub
                if (endometriosisna.isChecked()) {
                    submitButtonActivation();
                } else {
                    submitButtonDeactivation();
                }


            }
        });


        cystna.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                // TODO Auto-generated method stub
                if (cystna.isChecked()) {
                    submitButtonActivation();
                } else {
                    submitButtonDeactivation();
                }


            }
        });


        thyroidna.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                // TODO Auto-generated method stub
                if (thyroidna.isChecked()) {
                    submitButtonActivation();
                } else {
                    submitButtonDeactivation();
                }


            }
        });


        hyperna.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                // TODO Auto-generated method stub
                if (hyperna.isChecked()) {
                    submitButtonActivation();
                } else {
                    submitButtonDeactivation();
                }


            }
        });


        diabna.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                // TODO Auto-generated method stub
                if (diabna.isChecked()) {
                    submitButtonActivation();
                } else {
                    submitButtonDeactivation();
                }


            }
        });


        cancerpast.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                // TODO Auto-generated method stub
                if (cancerpast.isChecked()) {
                    cancercurrent.setChecked(false);
                    submitButtonActivation();
                } else {
                    submitButtonDeactivation();
                }


            }
        });

        cancercurrent.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (cancercurrent.isChecked()) {
                    cancerpast.setChecked(false);
                    submitButtonActivation();
                } else {
                    submitButtonDeactivation();
                }


            }
        });


        utipast.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (utipast.isChecked()) {
                    uticurrent.setChecked(false);
                    submitButtonActivation();
                } else {
                    submitButtonDeactivation();
                }


            }
        });


        uticurrent.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (uticurrent.isChecked()) {
                    utipast.setChecked(false);
                    submitButtonActivation();
                } else {
                    submitButtonDeactivation();
                }


            }
        });


        uterinecurrent.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (uterinecurrent.isChecked()) {
                    uterinepast.setChecked(false);
                    submitButtonActivation();
                } else {
                    submitButtonDeactivation();
                }


            }
        });


        uterinepast.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (uterinepast.isChecked()) {
                    uterinecurrent.setChecked(false);
                    submitButtonActivation();
                } else {
                    submitButtonDeactivation();
                }


            }
        });


        endometriosispast.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (endometriosispast.isChecked()) {
                    endometriosiscurrent.setChecked(false);
                    submitButtonActivation();
                } else {
                    submitButtonDeactivation();
                }


            }
        });


        endometriosiscurrent.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (endometriosiscurrent.isChecked()) {
                    endometriosispast.setChecked(false);
                    submitButtonActivation();
                } else {
                    submitButtonDeactivation();
                }


            }
        });


        cystcurrent.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (cystcurrent.isChecked()) {
                    cystpast.setChecked(false);
                    submitButtonActivation();
                } else {
                    submitButtonDeactivation();
                }


            }
        });


        cystpast.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (cystpast.isChecked()) {
                    cystcurrent.setChecked(false);
                    submitButtonActivation();
                } else {
                    submitButtonDeactivation();
                }


            }
        });


        thyroidpast.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (thyroidpast.isChecked()) {
                    thyroidcurrent.setChecked(false);
                    submitButtonActivation();
                } else {
                    submitButtonDeactivation();
                }


            }
        });


        thyroidcurrent.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (thyroidcurrent.isChecked()) {
                    thyroidpast.setChecked(false);
                    submitButtonActivation();
                } else {
                    submitButtonDeactivation();
                }


            }
        });


        hyperthyroidpast.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (hyperthyroidpast.isChecked()) {
                    hyperthyroidcurrent.setChecked(false);
                    submitButtonActivation();
                } else {
                    submitButtonDeactivation();
                }


            }
        });


        hyperthyroidcurrent.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (hyperthyroidcurrent.isChecked()) {
                    hyperthyroidpast.setChecked(false);
                    submitButtonActivation();
                } else {
                    submitButtonDeactivation();
                }


            }
        });


        diabpast.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (diabpast.isChecked()) {
                    diabcurrent.setChecked(false);
                    submitButtonActivation();
                } else {
                    submitButtonDeactivation();
                }


            }
        });


        diabcurrent.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (diabcurrent.isChecked()) {
                    diabpast.setChecked(false);
                    submitButtonActivation();
                } else {
                    submitButtonDeactivation();
                }


            }
        });


        hypercurrent.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (hypercurrent.isChecked()) {
                    hyperpast.setChecked(false);
                    submitButtonActivation();
                } else {
                    submitButtonDeactivation();
                }


            }
        });

        hyperpast.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (hyperpast.isChecked()) {
                    hypercurrent.setChecked(false);
                    submitButtonActivation();
                } else {
                    submitButtonDeactivation();
                }


            }
        });


        llAddmorebtn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub

                submitButtonActivation();

                LayoutInflater inflater = LayoutInflater.from(getActivity());
                final View add_pmp = inflater.inflate(R.layout.addmorecurrentpast, null);

                TextView serialno = (TextView) add_pmp.findViewById(R.id.serialnumber);
                EditText diseasename = (EditText) add_pmp.findViewById(R.id.diseasename);
                final CheckBox current = (CheckBox) add_pmp.findViewById(R.id.cbcurrent);
                CheckBox past = (CheckBox) add_pmp.findViewById(R.id.cbpast);
                final CheckBox presentpastna = (CheckBox) add_pmp.findViewById(R.id.cbna);
                EditText medication = (EditText) add_pmp.findViewById(R.id.etmedication);
                RelativeLayout remove = (RelativeLayout) add_pmp.findViewById(R.id.rlImagecross);


                presentpastna.setTag(presentpastposition);
                medication.setTag(presentpastposition);
                past.setTag(presentpastposition);
                current.setTag(presentpastposition);
                diseasename.setTag(presentpastposition);
                serialno.setTag(presentpastposition);
                remove.setTag(presentpastposition);

                int serialnumber = presentpastposition + 9;

                serialno.setText(Integer.toString(serialnumber));


                presentpastnaarraylist.add(presentpastposition, presentpastna);
                pastarraylist.add(presentpastposition, past);
                currentarraylist.add(presentpastposition, current);
                medicationarraylist.add(presentpastposition, medication);
                diseasenamearraylist.add(presentpastposition, diseasename);
                removeArraylist.add(presentpastposition, remove);
                serialnumberArraylist.add(presentpastposition, serialno);

                presentpastposition++;


                currentarraylist.get((Integer) current.getTag()).setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        // TODO Auto-generated method stub
                        if (currentarraylist.get((Integer) current.getTag()).isChecked()) {
                            submitButtonActivation();
                            pastarraylist.get((Integer) current.getTag()).setChecked(false);
                        } else {
                            submitButtonDeactivation();
                        }


                    }
                });


                pastarraylist.get((Integer) current.getTag()).setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        // TODO Auto-generated method stub
                        if (pastarraylist.get((Integer) current.getTag()).isChecked()) {
                            submitButtonActivation();
                            currentarraylist.get((Integer) current.getTag()).setChecked(false);
                        } else {
                            submitButtonDeactivation();
                        }


                    }
                });


                presentpastnaarraylist.get((Integer) presentpastna.getTag()).setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View arg0) {
                        // TODO Auto-generated method stub				
                        presentpastnaposition = (Integer) presentpastna.getTag();

                        submitButtonActivation();

                        if (presentpastnaarraylist.get((Integer) presentpastna.getTag()).isChecked()) {
                            currentarraylist.get((Integer) presentpastna.getTag()).setChecked(false);
                            pastarraylist.get((Integer) presentpastna.getTag()).setChecked(false);
                            currentarraylist.get((Integer) presentpastna.getTag()).setClickable(false);
                            pastarraylist.get((Integer) presentpastna.getTag()).setClickable(false);
                            medicationarraylist.get((Integer) presentpastna.getTag()).setEnabled(false);
                            medicationarraylist.get((Integer) presentpastna.getTag()).setText("");
                        } else {
                            currentarraylist.get((Integer) presentpastna.getTag()).setClickable(true);
                            pastarraylist.get((Integer) presentpastna.getTag()).setClickable(true);
                            medicationarraylist.get((Integer) presentpastna.getTag()).setEnabled(true);

                        }

                    }
                });


                removeArraylist.get((Integer) presentpastna.getTag()).setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        // TODO Auto-generated method stub

                        submitButtonDeactivation();

                        View removeView = (LinearLayout) add_pmp.findViewById(R.id.presentpastcontainer);
                        ViewGroup parent = (ViewGroup) removeView.getParent();
                        parent.removeView(removeView);


                        presentpastnaarraylist.set((Integer) presentpastna.getTag(), null);
                        pastarraylist.set((Integer) presentpastna.getTag(), null);
                        currentarraylist.set((Integer) presentpastna.getTag(), null);
                        medicationarraylist.set((Integer) presentpastna.getTag(), null);
                        diseasenamearraylist.set((Integer) presentpastna.getTag(), null);
                        removeArraylist.set((Integer) presentpastna.getTag(), null);
                        serialnumberArraylist.set((Integer) presentpastna.getTag(), null);

                    }
                });


                llAddpresentpast.addView(add_pmp);

            }
        });


        fna.setOnCheckedChangeListener(new FamilyCheckedChangeListener(1));
        mna.setOnCheckedChangeListener(new FamilyCheckedChangeListener(2));
        sna.setOnCheckedChangeListener(new FamilyCheckedChangeListener(3));
        gfna.setOnCheckedChangeListener(new FamilyCheckedChangeListener(4));
        gmna.setOnCheckedChangeListener(new FamilyCheckedChangeListener(5));

        fna.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                if (fna.isChecked()) {
                    submitButtonActivation();
                } else {
                    submitButtonDeactivation();
                }


            }
        });


        mna.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                if (mna.isChecked()) {
                    submitButtonActivation();
                } else {
                    submitButtonDeactivation();
                }


            }
        });

        sna.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                if (sna.isChecked()) {
                    submitButtonActivation();
                } else {
                    submitButtonDeactivation();
                }


            }
        });


        gfna.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                if (gfna.isChecked()) {
                    submitButtonActivation();
                } else {
                    submitButtonDeactivation();
                }


            }
        });

        gmna.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                if (gmna.isChecked()) {
                    submitButtonActivation();
                } else {
                    submitButtonDeactivation();
                }


            }
        });


        fhyper.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                if (fhyper.isChecked()) {
                    submitButtonActivation();
                } else {
                    submitButtonDeactivation();
                }


            }
        });
        mhyper.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                if (mhyper.isChecked()) {
                    submitButtonActivation();
                } else {
                    submitButtonDeactivation();
                }


            }
        });
        shyper.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                if (shyper.isChecked()) {
                    submitButtonActivation();
                } else {
                    submitButtonDeactivation();
                }


            }
        });
        gfhyper.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                if (gfhyper.isChecked()) {
                    submitButtonActivation();
                } else {
                    submitButtonDeactivation();
                }


            }
        });
        gmhyper.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                if (gmhyper.isChecked()) {
                    submitButtonActivation();
                } else {
                    submitButtonDeactivation();
                }


            }
        });
        fdia.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                if (fdia.isChecked()) {
                    submitButtonActivation();
                } else {
                    submitButtonDeactivation();
                }


            }
        });
        mdia.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                if (mdia.isChecked()) {
                    submitButtonActivation();
                } else {
                    submitButtonDeactivation();
                }


            }
        });
        sdia.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                if (sdia.isChecked()) {
                    submitButtonActivation();
                } else {
                    submitButtonDeactivation();
                }


            }
        });
        gfdia.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                if (gfdia.isChecked()) {
                    submitButtonActivation();
                } else {
                    submitButtonDeactivation();
                }


            }
        });
        gmdia.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                if (gmdia.isChecked()) {
                    submitButtonActivation();
                } else {
                    submitButtonDeactivation();
                }


            }
        });
        fcan.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                if (fcan.isChecked()) {
                    submitButtonActivation();
                } else {
                    submitButtonDeactivation();
                }


            }
        });
        mcan.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                if (mcan.isChecked()) {
                    submitButtonActivation();
                } else {
                    submitButtonDeactivation();
                }


            }
        });
        scan.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                if (scan.isChecked()) {
                    submitButtonActivation();
                } else {
                    submitButtonDeactivation();
                }


            }
        });
        gfcan.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                if (gfcan.isChecked()) {
                    submitButtonActivation();
                } else {
                    submitButtonDeactivation();
                }


            }
        });
        gmcan.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                if (gmcan.isChecked()) {
                    submitButtonActivation();
                } else {
                    submitButtonDeactivation();
                }


            }
        });
        fOther.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                if (fOther.isChecked()) {
                    submitButtonActivation();
                } else {
                    submitButtonDeactivation();
                }


            }
        });
        mOther.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                if (mOther.isChecked()) {
                    submitButtonActivation();
                } else {
                    submitButtonDeactivation();
                }


            }
        });
        sOther.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                if (sOther.isChecked()) {
                    submitButtonActivation();
                } else {
                    submitButtonDeactivation();
                }


            }
        });
        gfOther.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                if (gfOther.isChecked()) {
                    submitButtonActivation();
                } else {
                    submitButtonDeactivation();
                }


            }
        });
        gmOther.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                if (gmOther.isChecked()) {
                    submitButtonActivation();
                } else {
                    submitButtonDeactivation();
                }


            }
        });
        ;


        fOedittext.setEnabled(false);
        mOedittext.setEnabled(false);
        sOedittext.setEnabled(false);
        gfOedittext.setEnabled(false);
        gmOedittext.setEnabled(false);
        fOther.setOnCheckedChangeListener(new FamilyOtherCheckedChangeListener(1));
        mOther.setOnCheckedChangeListener(new FamilyOtherCheckedChangeListener(2));
        sOther.setOnCheckedChangeListener(new FamilyOtherCheckedChangeListener(3));
        gfOther.setOnCheckedChangeListener(new FamilyOtherCheckedChangeListener(4));
        gmOther.setOnCheckedChangeListener(new FamilyOtherCheckedChangeListener(5));


        noSurgeryReport.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                submitButtonActivation();

                if (noSurgeryReport.isChecked()) {

                    surgeryOne.setEnabled(false);
                    surgeryTwo.setEnabled(false);

                    surgeryOne.setText("");
                    surgeryTwo.setText("");

                    addsurgery.removeAllViews();
                    addmoresurgery.setClickable(false);

                } else {

                    addmoresurgery.setClickable(true);
                    surgeryOne.setEnabled(true);
                    surgeryTwo.setEnabled(true);
                }


            }
        });


        addmoresurgery.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub


                submitButtonActivation();

                LayoutInflater inflater = LayoutInflater.from(getActivity());
                final View surger_inflate = inflater.inflate(R.layout.addmore_surgery_gynacologist, null);
                TextView SugNumber = (TextView) surger_inflate.findViewById(R.id.tvSugNumber);
                TextView SugNumber1 = (TextView) surger_inflate.findViewById(R.id.tvSugNumber1);
                EditText surgery = (EditText) surger_inflate.findViewById(R.id.etadd_more_surgery_one);
                EditText surgery1 = (EditText) surger_inflate.findViewById(R.id.etadd_more_surgery_two);
                final RelativeLayout remove = (RelativeLayout) surger_inflate.findViewById(R.id.rlImagecross);

                SugNumber.setTag(surgeryposition);
                SugNumber1.setTag(surgeryposition);
                surgery.setTag(surgeryposition);
                surgery1.setTag(surgeryposition);
                remove.setTag(surgeryposition);

                surgeryoneArraylist.add(surgeryposition, surgery);
                surgerytwoArraylist.add(surgeryposition, surgery1);
                serialnumberoneArraylist.add(surgeryposition, SugNumber);
                serialnumbertwoArraylist.add(surgeryposition, SugNumber1);
                removeArraylist.add(surgeryposition, remove);

                SugNumber.setText(Integer.toString(surgeryposition + 3) + ".");
                SugNumber1.setText(Integer.toString(surgeryposition + 4) + ".");

                surgeryposition++;


                removeArraylist.get((Integer) remove.getTag()).setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        // TODO Auto-generated method stub

                        submitButtonDeactivation();

                        View removeView = (LinearLayout) surger_inflate.findViewById(R.id.llAddSurgeries);
                        ViewGroup parent = (ViewGroup) removeView.getParent();
                        parent.removeView(removeView);

                        surgeryoneArraylist.add((Integer) remove.getTag(), null);
                        surgerytwoArraylist.add((Integer) remove.getTag(), null);
                        serialnumberoneArraylist.add((Integer) remove.getTag(), null);
                        serialnumbertwoArraylist.add((Integer) remove.getTag(), null);
                        removeArraylist.add((Integer) remove.getTag(), null);


                    }
                });


                addsurgery.addView(surger_inflate);

            }
        });


        etallergyOther1.setEnabled(false);
        other.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                submitButtonActivation();

                if (other.isChecked()) {
                    etallergyOther1.setEnabled(true);
                } else {
                    etallergyOther1.setEnabled(false);
                    etallergyOther1.setText("");
                }


            }
        });

        casein.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                submitButtonActivation();
            }
        });


        casein.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (casein.isChecked())
                    submitButtonActivation();
            }
        });
        egg.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (egg.isChecked())
                    submitButtonActivation();
            }
        });
        fish.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (fish.isChecked())
                    submitButtonActivation();
            }
        });
        milk.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (milk.isChecked())
                    submitButtonActivation();
            }
        });
        nut.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (nut.isChecked())
                    submitButtonActivation();
            }
        });
        shellfish.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (shellfish.isChecked())
                    submitButtonActivation();
            }
        });
        sulfite.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (sulfite.isChecked())
                    submitButtonActivation();
            }
        });
        soy.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (soy.isChecked())
                    submitButtonActivation();
            }
        });
        wheat.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (wheat.isChecked())
                    submitButtonActivation();
            }
        });


        spring.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (spring.isChecked())
                    submitButtonActivation();
            }
        });
        summer.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (summer.isChecked())
                    submitButtonActivation();
            }
        });
        fall.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (fall.isChecked())
                    submitButtonActivation();
            }
        });
        winter.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (winter.isChecked())
                    submitButtonActivation();
            }
        });
        bee.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (bee.isChecked())
                    submitButtonActivation();
            }
        });
        cat.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (cat.isChecked())
                    submitButtonActivation();
            }
        });
        dog.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (dog.isChecked())
                    submitButtonActivation();
            }
        });
        insect.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (insect.isChecked())
                    submitButtonActivation();
            }
        });


        mold.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (mold.isChecked())
                    submitButtonActivation();
            }
        });
        plant.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (plant.isChecked())
                    submitButtonActivation();
            }
        });
        pollen.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (pollen.isChecked())
                    submitButtonActivation();
            }
        });
        sun.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                if (sun.isChecked())
                    submitButtonActivation();
            }
        });


        etallergyOther2.setEnabled(false);
        other2.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                submitButtonActivation();

                if (other2.isChecked()) {
                    etallergyOther2.setEnabled(true);
                } else {
                    etallergyOther2.setEnabled(false);
                    etallergyOther2.setText("");
                }


            }
        });


        noAlerReport.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                submitButtonActivation();

                if (noAlerReport.isChecked()) {
                    medicOne.setEnabled(false);
                    medicTwo.setEnabled(false);
                    reactionOne.setEnabled(false);
                    reactionTwo.setEnabled(false);

                    medicOne.setText("");
                    medicTwo.setText("");
                    reactionOne.setText("");
                    reactionTwo.setText("");

                    addallergy.removeAllViews();
                    addallergybtn.setClickable(false);

                } else {

                    addallergybtn.setClickable(true);

                    medicOne.setEnabled(true);
                    medicTwo.setEnabled(true);
                    reactionOne.setEnabled(true);
                    reactionTwo.setEnabled(true);
                }

            }
        });


        addallergybtn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                submitButtonActivation();

                LayoutInflater inflater = LayoutInflater.from(getActivity());
                final View allergy_add = inflater.inflate(R.layout.add_more_allergies, null);
                TextView Medserialnumber = (TextView) allergy_add.findViewById(R.id.tvNumber);
                TextView Reacserialnumber = (TextView) allergy_add.findViewById(R.id.tvReacNumber);
                final EditText medication = (EditText) allergy_add.findViewById(R.id.etadd_more_allergey);
                EditText reaction = (EditText) allergy_add.findViewById(R.id.etadd_more_reaction);
                final RelativeLayout remove = (RelativeLayout) allergy_add.findViewById(R.id.rlImagecross);


                Medserialnumber.setTag(drugposition);
                Reacserialnumber.setTag(drugposition);
                medication.setTag(drugposition);
                reaction.setTag(drugposition);
                remove.setTag(drugposition);

                medicationdrugArraylist.add(drugposition, medication);
                reactiondrugArraylist.add(drugposition, reaction);
                serialnumbermedArraylist.add(drugposition, Medserialnumber);
                serialnumberreacArraylist.add(drugposition, Reacserialnumber);
                drugremoveArraylist.add(drugposition, remove);

                int val = drugposition + 3;

                Medserialnumber.setText(Integer.toString(val));
                Reacserialnumber.setText(Integer.toString(val));

                drugposition++;

                drugremoveArraylist.get((Integer) medication.getTag()).setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        // TODO Auto-generated method stub

                        submitButtonDeactivation();

                        View removeView = (LinearLayout) allergy_add.findViewById(R.id.llhidden_layout);
                        ViewGroup parent = (ViewGroup) removeView.getParent();
                        parent.removeView(removeView);

                        medicationdrugArraylist.set((Integer) medication.getTag(), null);
                        reactiondrugArraylist.set((Integer) medication.getTag(), null);
                        serialnumbermedArraylist.set((Integer) medication.getTag(), null);
                        serialnumberreacArraylist.set((Integer) medication.getTag(), null);
                        drugremoveArraylist.set((Integer) medication.getTag(), null);
                    }
                });

                addallergy.addView(allergy_add);


            }
        });


        alna.setOnCheckedChangeListener(new SocialHistoryCheckedChangeListener(1));
        tobna.setOnCheckedChangeListener(new SocialHistoryCheckedChangeListener(2));
        tobchewablena.setOnCheckedChangeListener(new SocialHistoryCheckedChangeListener(3));
        osubna.setOnCheckedChangeListener(new SocialHistoryCheckedChangeListener(4));

        alna.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                submitButtonActivation();
            }
        });

        tobna.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                submitButtonActivation();
            }
        });

        tobchewablena.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                submitButtonActivation();
            }
        });

        osubna.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                submitButtonActivation();
            }
        });


        alcurr.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (alcurr.isChecked()) {
                    submitButtonActivation();
                    alpast.setChecked(false);
                } else {
                    submitButtonDeactivation();
                }


            }
        });


        alpast.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (alpast.isChecked()) {
                    submitButtonActivation();
                    alcurr.setChecked(false);
                } else {
                    submitButtonDeactivation();
                }


            }
        });


        tobcurr.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (tobcurr.isChecked()) {
                    submitButtonActivation();
                    tobpast.setChecked(false);
                } else {
                    submitButtonDeactivation();
                }


            }
        });

        tobpast.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (tobpast.isChecked()) {
                    submitButtonActivation();
                    tobcurr.setChecked(false);
                } else {
                    submitButtonDeactivation();
                }


            }
        });


        tobchewablecurr.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (tobchewablecurr.isChecked()) {
                    submitButtonActivation();
                    tobchewablepast.setChecked(false);
                } else {
                    submitButtonDeactivation();
                }


            }
        });


        tobchewablepast.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (tobchewablepast.isChecked()) {
                    submitButtonActivation();
                    tobchewablecurr.setChecked(false);
                } else {
                    submitButtonDeactivation();
                }


            }
        });


        osubcurr.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (osubcurr.isChecked()) {
                    submitButtonActivation();
                    osubpast.setChecked(false);
                } else {
                    submitButtonDeactivation();
                }


            }
        });


        osubpast.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (osubpast.isChecked()) {
                    submitButtonActivation();
                    osubcurr.setChecked(false);
                } else {
                    submitButtonDeactivation();
                }


            }
        });

        return v;
    }


    public class MyCheckedChangeListener implements OnCheckedChangeListener {
        int position;
        int arraylistposition;

        public MyCheckedChangeListener(int position, int arraylistposition) {
            submitButtonActivation();
            this.position = position;
            this.arraylistposition = arraylistposition;
        }

        private void changeVisibility(CheckBox current, CheckBox past, EditText medication, boolean isChecked) {
            if (isChecked) {

                current.setChecked(false);
                past.setChecked(false);
                medication.setEnabled(false);
                medication.setText("");
                current.setClickable(false);
                past.setClickable(false);

            } else {

                current.setClickable(true);
                past.setClickable(true);
                medication.setEnabled(true);

            }

        }

        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            switch (position) {
                case 1:
                    changeVisibility(hypercurrent, hyperpast, hypermedication, isChecked);
                    break;
                case 2:
                    changeVisibility(diabcurrent, diabpast, diabmedication, isChecked);
                    break;
                case 3:
                    changeVisibility(thyroidcurrent, thyroidpast, thyroidmedication, isChecked);
                    break;
                case 4:
                    changeVisibility(cystcurrent, cystpast, cystmedication, isChecked);
                    break;
                case 5:
                    changeVisibility(endometriosiscurrent, endometriosispast, endometriosismedication, isChecked);
                    break;
                case 6:
                    changeVisibility(uterinecurrent, uterinepast, uterinemedication, isChecked);
                    break;
                case 7:
                    changeVisibility(uticurrent, utipast, utimedication, isChecked);
                    break;
                case 8:
                    changeVisibility(cancercurrent, cancerpast, cancermedication, isChecked);
                    break;
                case 9:
                    changeVisibility(hyperthyroidcurrent, hyperthyroidpast, hyperthyroidmedication, isChecked);
                    break;

            }
        }
    }


    public class FamilyCheckedChangeListener implements OnCheckedChangeListener {
        int position;

        public FamilyCheckedChangeListener(int position) {

            this.position = position;
        }

        private void changeVisibility(CheckBox hyper, CheckBox diabetes, CheckBox cancer, CheckBox other, EditText etother, boolean isChecked) {
            if (isChecked) {

                submitButtonActivation();

                hyper.setChecked(false);
                diabetes.setChecked(false);
                cancer.setChecked(false);
                other.setChecked(false);

                etother.setEnabled(false);
                etother.setText("");

                hyper.setClickable(false);
                diabetes.setClickable(false);
                cancer.setClickable(false);
                other.setClickable(false);

            } else {

                submitButtonDeactivation();

                hyper.setClickable(true);
                diabetes.setClickable(true);
                cancer.setClickable(true);
                other.setClickable(true);

                etother.setEnabled(true);
            }
        }

        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            switch (position) {
                case 1:
                    changeVisibility(fhyper, fdia, fcan, fOther, fOedittext, isChecked);
                    break;
                case 2:
                    changeVisibility(mhyper, mdia, mcan, mOther, mOedittext, isChecked);
                    break;
                case 3:
                    changeVisibility(shyper, sdia, scan, sOther, sOedittext, isChecked);
                    break;
                case 4:
                    changeVisibility(gfhyper, gfdia, gfcan, gfOther, gfOedittext, isChecked);
                    break;
                case 5:
                    changeVisibility(gmhyper, gmdia, gmcan, gmOther, gmOedittext, isChecked);
                    break;

            }
        }
    }


    public class FamilyOtherCheckedChangeListener implements OnCheckedChangeListener {
        int position;

        public FamilyOtherCheckedChangeListener(int position) {
            submitButtonActivation();
            this.position = position;
        }

        private void changeVisibility(EditText etother, boolean isChecked) {
            if (isChecked) {

                etother.setEnabled(true);

            } else {
                etother.setEnabled(false);
                etother.setText("");
            }
        }

        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            switch (position) {
                case 1:
                    changeVisibility(fOedittext, isChecked);
                    break;
                case 2:
                    changeVisibility(mOedittext, isChecked);
                    break;
                case 3:
                    changeVisibility(sOedittext, isChecked);
                    break;
                case 4:
                    changeVisibility(gfOedittext, isChecked);
                    break;
                case 5:
                    changeVisibility(gmOedittext, isChecked);
                    break;

            }
        }
    }


    public class SocialHistoryCheckedChangeListener implements OnCheckedChangeListener {
        int position;

        public SocialHistoryCheckedChangeListener(int position) {
            submitButtonActivation();
            this.position = position;
        }

        private void changeVisibility(CheckBox current, CheckBox past, boolean isChecked) {
            if (isChecked) {

                current.setChecked(false);
                past.setChecked(false);

                current.setClickable(false);
                past.setClickable(false);

            } else {

                current.setClickable(true);
                past.setClickable(true);

            }
        }

        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            switch (position) {
                case 1:
                    changeVisibility(alcurr, alpast, isChecked);
                    break;
                case 2:
                    changeVisibility(tobcurr, tobpast, isChecked);
                    break;
                case 3:
                    changeVisibility(tobchewablecurr, tobchewablepast, isChecked);
                    break;
                case 4:
                    changeVisibility(osubcurr, osubpast, isChecked);
                    break;

            }
        }
    }


    @Override
    public void onMedical(int arg, int targetfragment) {
        // TODO Auto-generated method stub
        send_data_nextpage(targetfragment);
    }


    private void send_data_nextpage(final int targetfragment) {
        // TODO Auto-generated method stub

        System.out.println("working......medical.....");


        SharedPreferences scanpidnew = getActivity().getSharedPreferences("NewPatID", 0);
        String scnewPid = scanpidnew.getString("NewID", null);


        SharedPreferences sp = getActivity().getSharedPreferences("Login", 0);
        String docterregId = sp.getString("docterRegnumber", null);

        SharedPreferences scanpid1 = getActivity().getSharedPreferences("ScannedPid", 0);
        String scPid = scanpid1.getString("scanpid", null);
        if (patientobject == null) {

            if (scPid != null || scnewPid != null) {
                if (scPid != null) {
                    Log.e("scanner pid111====>>", scPid);
                    patientId = scPid;
                }
                if (scnewPid != null) {
                    Log.e("scanner pid====>>", scnewPid);
                    patientId = scnewPid;
                }
            }
        } else if (patientobject.getString("patientID") == null) {
            patientId = scnewPid;
        } else {
            patientId = patientobject.getString("patientID");
        }


        if (patientobject == null) {
            patientobject = new ParseObject("Patients");
        }




		/*pmpdate_array=new JSONArray();*/
        /*pmpflow_array=new JSONArray();
        pmpdysmenorrhea_array=new JSONArray();*/

		/*lmpdate_array=new JSONArray();*/
        /*lmpflow_array=new JSONArray();
        lmpdysmenorrhea_array=new JSONArray();*/


        //majorill_array=new JSONArray();
        //majorillstatus_array=new JSONArray();
        familyistory_array = new JSONArray();
        surgery_arraylist.clear();
        //allergy_array=new JSONArray();

        socialhistory_array = new JSONArray();
        socHistStatArray = new String[4];


        menarchelist.clear();
        dysmenorrheapmplist.clear();

//        menstrualregularlist.clear();
//        menstrualdayslist.clear();
//        menstrualcyclelist.clear();


        marriageyearlist.clear();
        marriagechildrenlist.clear();
        marriagemiscariageslist.clear();


        pmpdate_array.clear();
        pmpflow_array.clear();
        pmpdysmenorrhea_array.clear();

        otherhistory_array.clear();
//        lmpdate_array.clear();
//        lmpflow_array.clear();
//        lmpdysmenorrhea_array.clear();

        majorill_array.clear();
        majorillstatus_array.clear();
        familyistorystatus_array.clear();
        surgery_arraylist.clear();
        allergy_array.clear();
        medication_array.clear();
        reaction_array.clear();
        socialhistorystatus_array.clear();













		/*if(menarche.getText().toString().length()!=0 && flowpmp.getText().toString().length()!=0 && dysmenorrheapmp.getText().toString().length()!=0)
        {

			pmpdate_array.add(menarche.getText().toString());
			pmpflow_array.add(flowpmp.getText().toString());
			pmpdysmenorrhea_array.add(dysmenorrheapmp.getText().toString());
		}*/

//        if (datelmp.getText().toString().length() != 0) {
//            lmpdate_array.add(datelmp.getText().toString());
//        } else {
//            lmpdate_array.add("Nil");
//        }
//        if (flowlmp.getText().toString().length() != 0) {
//            lmpflow_array.add(flowlmp.getText().toString());
//        } else {
//            lmpflow_array.add("Nil");
//        }
//        if (dysmenorrhealmp.getText().toString().length() != 0) {
//            lmpdysmenorrhea_array.add(dysmenorrhealmp.getText().toString());
//        } else {
//            lmpdysmenorrhea_array.add("Nil");
//        }


        if (pmpdateArraylist.size() > 0 && pmpflowArraylist.size() > 0 && pmpdysmenpmpArraylist.size() > 0) {

            System.out.println("pmpdateArraylist size" + pmpdateArraylist.size());

            for (int i = 0; i < pmpdateArraylist.size(); i++) {

                if (pmpdateArraylist.get(i) != null && pmpflowArraylist.get(i) != null && pmpdysmenpmpArraylist.get(i) != null) {

                    if (pmpdateArraylist.get(i).getText().length() != 0 && pmpflowArraylist.get(i).getText().length() != 0 && pmpdysmenpmpArraylist.get(i).getText().length() != 0) {

                        pmpdate_array.add(pmpdateArraylist.get(i).getText().toString());

                        pmpflow_array.add(pmpflowArraylist.get(i).getText().toString());
                        pmpdysmenorrhea_array.add(pmpdysmenpmpArraylist.get(i).getText().toString());
                    }

                }
            }

        }

        if (lmpdateArraylist.size() > 0) {
            for (int i = 0; i < lmpdateArraylist.size(); i++) {
                if (lmpdateArraylist.get(i) != null) {
                    if (lmpdateArraylist.get(i).getText().length() != 0) {
                        lmpdate_array.add(lmpdateArraylist.get(i).getText().toString());
//                        listCount.add(0);
                    } else {
                        lmpdate_array.add("Nil");
                    }
                }
                if (lmpflowArraylist.get(i) != null) {
                    if (lmpflowArraylist.get(i).getText().length() != 0) {
                        lmpflow_array.add(lmpflowArraylist.get(i).getText().toString());
                    } else {
                        lmpflow_array.add("Nil");
                    }
                }
                if (lmpdysmenpmpArraylist.get(i) != null) {
                    if (lmpdysmenpmpArraylist.get(i).getText().length() != 0) {
                        lmpdysmenorrhea_array.add(lmpdysmenpmpArraylist.get(i).getText().toString());
                    } else {
                        lmpdysmenorrhea_array.add("Nil");
                    }
                }
            }
//            lmpAdapter.notifyDataSetChanged();
//            justifyListViewHeightBasedOnChildren(lstMenstrualPeriod);
        }

        if (nopresentpasthistory.isChecked() == true) {

            majorill_array.add("Hypertension");
            majorill_array.add("Diabetes");
            majorill_array.add("Hypothyridism");
            majorill_array.add("Hyperthyroidism");
            majorill_array.add("Cyst");
            majorill_array.add("Endometriosis");
            majorill_array.add("Uterine Fibroids");
            majorill_array.add("Uti");
            majorill_array.add("Cancer");


            majorillstatus_array.add("NA");
            majorillstatus_array.add("NA");
            majorillstatus_array.add("NA");
            majorillstatus_array.add("NA");
            majorillstatus_array.add("NA");
            majorillstatus_array.add("NA");
            majorillstatus_array.add("NA");
            majorillstatus_array.add("NA");
            majorillstatus_array.add("NA");

        } else {

            majorill_array.add("Hypertension");
            majorill_array.add("Diabetes");
            majorill_array.add("Hypothyridism");
            majorill_array.add("Hyperthyroidism");
            majorill_array.add("Cyst");
            majorill_array.add("Endometriosis");
            majorill_array.add("Uterine Fibroids");
            majorill_array.add("Uti");
            majorill_array.add("Cancer");


            String hyperstatus = "", hypermed = "", HyperTension;


            if (hypercurrent.isChecked()) {
                hyperstatus = hypercurrent.getText().toString();
            } else if (hyperpast.isChecked()) {
                hyperstatus = hyperpast.getText().toString();
            } else if (hyperna.isChecked()) {
                hyperstatus = hyperna.getText().toString();
            } else {
                hyperstatus = "Nil";
            }
            if (hypermedication.getText().toString().length() != 0) {
                hypermed = hypermedication.getText().toString();
            } else {
                hypermed = "Nil";
            }

            HyperTension = hyperstatus + "&" + hypermed;


            String diabetesstatus = "", diabetesmed = "", Diabetes;


            if (diabcurrent.isChecked()) {
                diabetesstatus = diabcurrent.getText().toString();
            } else if (diabpast.isChecked()) {
                diabetesstatus = diabpast.getText().toString();
            } else if (diabna.isChecked()) {
                diabetesstatus = diabna.getText().toString();
            } else {
                diabetesstatus = "Nil";
            }
            if (diabmedication.getText().toString().length() != 0) {
                diabetesmed = diabmedication.getText().toString();
            } else {
                diabetesmed = "Nil";
            }

            Diabetes = diabetesstatus + "&" + diabetesmed;


            String thyroidstatus = "", thyroidmed = "", Thyroid;


            if (thyroidcurrent.isChecked()) {
                thyroidstatus = thyroidcurrent.getText().toString();
            } else if (thyroidpast.isChecked()) {
                thyroidstatus = thyroidpast.getText().toString();
            } else if (thyroidna.isChecked()) {
                thyroidstatus = thyroidna.getText().toString();
            } else {
                thyroidstatus = "Nil";
            }
            if (thyroidmedication.getText().toString().length() != 0) {
                thyroidmed = thyroidmedication.getText().toString();
            } else {
                thyroidmed = "Nil";
            }

            Thyroid = thyroidstatus + "&" + thyroidmed;


            String hyperthyroidstatus = "", hyperthyroidmed = "", hyperThyroid;


            if (hyperthyroidcurrent.isChecked()) {
                hyperthyroidstatus = hyperthyroidcurrent.getText().toString();
            } else if (hyperthyroidpast.isChecked()) {
                hyperthyroidstatus = hyperthyroidpast.getText().toString();
            } else if (hyperthyroidna.isChecked()) {
                hyperthyroidstatus = hyperthyroidna.getText().toString();
            } else {
                hyperthyroidstatus = "Nil";
            }
            if (hyperthyroidmedication.getText().toString().length() != 0) {
                hyperthyroidmed = hyperthyroidmedication.getText().toString();
            } else {
                hyperthyroidmed = "Nil";
            }

            hyperThyroid = hyperthyroidstatus + "&" + hyperthyroidmed;


            String cyststatus = "", cystmed = "", Cyst;


            if (cystcurrent.isChecked()) {
                cyststatus = cystcurrent.getText().toString();
            } else if (cystpast.isChecked()) {
                cyststatus = cystpast.getText().toString();
            } else if (cystna.isChecked()) {
                cyststatus = cystna.getText().toString();
            } else {
                cyststatus = "Nil";
            }
            if (cystmedication.getText().toString().length() != 0) {
                cystmed = cystmedication.getText().toString();
            } else {
                cystmed = "Nil";
            }
            Cyst = cyststatus + "&" + cystmed;


            String enostatus = "", endomed = "", Endometriosis;


            if (endometriosiscurrent.isChecked()) {
                enostatus = endometriosiscurrent.getText().toString();
            } else if (endometriosispast.isChecked()) {
                enostatus = endometriosispast.getText().toString();
            } else if (endometriosisna.isChecked()) {
                enostatus = endometriosisna.getText().toString();
            } else {
                enostatus = "Nil";
            }
            if (endometriosismedication.getText().toString().length() != 0) {
                endomed = endometriosismedication.getText().toString();
            } else {
                endomed = "Nil";
            }
            Endometriosis = enostatus + "&" + endomed;


            String uterinestatus = "", uterinemed = "", Uterine;


            if (uterinecurrent.isChecked()) {
                uterinestatus = uterinecurrent.getText().toString();
            } else if (uterinepast.isChecked()) {
                uterinestatus = uterinepast.getText().toString();
            } else if (uterinena.isChecked()) {
                uterinestatus = uterinena.getText().toString();
            } else {

                uterinestatus = "Nil";

            }
            if (uterinemedication.getText().toString().length() != 0) {
                uterinemed = uterinemedication.getText().toString();
            } else {
                uterinemed = "Nil";
            }
            Uterine = uterinestatus + "&" + uterinemed;


            String utistatus = "", utimed = "", Uti;


            if (uticurrent.isChecked()) {
                utistatus = uticurrent.getText().toString();
            } else if (utipast.isChecked()) {
                utistatus = utipast.getText().toString();
            } else if (utina.isChecked()) {
                utistatus = utina.getText().toString();
            } else {
                utistatus = "Nil";
            }
            if (utimedication.getText().toString().length() != 0) {
                utimed = utimedication.getText().toString();
            } else {
                utimed = "Nil";
            }
            Uti = utistatus + "&" + utimed;


            String cancerstatus = "", cancermed = "", Cancer;


            if (cancercurrent.isChecked()) {
                cancerstatus = cancercurrent.getText().toString();
            } else if (cancerpast.isChecked()) {
                cancerstatus = cancerpast.getText().toString();
            } else if (cancerna.isChecked()) {
                cancerstatus = cancerna.getText().toString();
            } else {
                cancerstatus = "Nil";
            }
            if (cancermedication.getText().toString().length() != 0) {
                cancermed = cancermedication.getText().toString();
            } else {
                cancermed = "Nil";
            }
            Cancer = cancerstatus + "&" + cancermed;


            majorillstatus_array.add(HyperTension);
            majorillstatus_array.add(Diabetes);
            majorillstatus_array.add(Thyroid);
            majorillstatus_array.add(hyperThyroid);
            majorillstatus_array.add(Cyst);
            majorillstatus_array.add(Endometriosis);
            majorillstatus_array.add(Uterine);
            majorillstatus_array.add(Uti);
            majorillstatus_array.add(Cancer);


            if (diseasenamearraylist.size() > 0) {

                for (int i = 0; i < diseasenamearraylist.size(); i++) {
                    if (diseasenamearraylist.get(i) != null && currentarraylist.get(i) != null && pastarraylist.get(i) != null && medicationarraylist.get(i) != null) {


                        String status = "", medication = "", Disease;


                        if (currentarraylist.get(i).isChecked()) {
                            status = currentarraylist.get(i).getText().toString();
                        }
                        if (pastarraylist.get(i).isChecked()) {
                            status = pastarraylist.get(i).getText().toString();
                        }
                        if (presentpastnaarraylist.get(i).isChecked()) {
                            status = presentpastnaarraylist.get(i).getText().toString();
                        }
                        if (medicationarraylist.get(i).getText().toString().length() != 0) {
                            medication = medicationarraylist.get(i).getText().toString();
                        } else {
                            medication = "Nil";
                        }
                        Disease = status + "&" + medication;

                        majorill_array.add(diseasenamearraylist.get(i).getText().toString());
                        majorillstatus_array.add(Disease);

                    }
                }

            }

        }


        millfamStatArray = new String[5];
        millfamArray = new String[5];

        millfamArray[0] = "Father";
        millfamArray[1] = "Mother";
        millfamArray[2] = "Sibling";
        millfamArray[3] = "Grandfather";
        millfamArray[4] = "Grandmother";

        familyistory_array.put("Father");
        familyistory_array.put("Mother");
        familyistory_array.put("Sibling");
        familyistory_array.put("Grandfather");
        familyistory_array.put("Grandmother");


        if (fna.isChecked()) {
            millfamStatArray[0] = "N/A";
        } else {
            String fillhyp, filldia, fillcan, filloth = null;

            if (fhyper.isChecked()) {
                fillhyp = fhyper.getText().toString();
            } else {
                fillhyp = "Nil";
            }
            if (fdia.isChecked()) {
                filldia = fdia.getText().toString();
            } else {
                filldia = "Nil";
            }
            if (fcan.isChecked()) {
                fillcan = fcan.getText().toString();
            } else {
                fillcan = "Nil";
            }
            if (fOther.isChecked()) {
                String fotherm = fOedittext.getText().toString();
                if (fotherm.matches("")) {
                    //Toast.makeText(getActivity(), "Enter Father Other ill", Toast.LENGTH_SHORT).show();
                } else {
                    filloth = fOedittext.getText().toString();
                }

            } else {
                filloth = "Nil";
            }
            String fatherills = fillhyp + "," + filldia + "," + fillcan + "," + filloth;

            millfamStatArray[0] = fatherills;
            Log.e("Valueeeee===ffff", fatherills);
        }


        if (mna.isChecked()) {
            millfamStatArray[1] = "N/A";
        } else {
            String millhyp, milldia, millcan, milloth = null;

            if (mhyper.isChecked()) {
                millhyp = mhyper.getText().toString();
            } else {
                millhyp = "Nil";
            }
            if (mdia.isChecked()) {
                milldia = mdia.getText().toString();
            } else {
                milldia = "Nil";
            }
            if (mcan.isChecked()) {
                millcan = mcan.getText().toString();
            } else {
                millcan = "Nil";
            }
            if (mOther.isChecked()) {
                String fotherm = mOedittext.getText().toString();
                if (fotherm.matches("")) {
                    //	Toast.makeText(getActivity(), "Enter Mother Other ill", Toast.LENGTH_SHORT).show();
                } else {
                    milloth = mOedittext.getText().toString();

                }
            } else {
                milloth = "Nil";
            }
            String matherills = millhyp + "," + milldia + "," + millcan + "," + milloth;

            millfamStatArray[1] = matherills;
            Log.e("Valueeeee===ffff", matherills);
        }

        if (sna.isChecked()) {
            millfamStatArray[2] = "N/A";
        } else {
            String sillhyp, silldia, sillcan, silloth = null;

            if (shyper.isChecked()) {
                sillhyp = shyper.getText().toString();
            } else {
                sillhyp = "Nil";
            }
            if (sdia.isChecked()) {
                silldia = sdia.getText().toString();
            } else {
                silldia = "Nil";
            }
            if (scan.isChecked()) {
                sillcan = scan.getText().toString();
            } else {
                sillcan = "Nil";
            }
            if (sOther.isChecked()) {
                String fotherm = sOedittext.getText().toString();
                if (fotherm.matches("")) {
                    //	Toast.makeText(getActivity(), "Enter Sibling Other ill", Toast.LENGTH_SHORT).show();
                } else {
                    silloth = sOedittext.getText().toString();
                }

            } else {
                silloth = "Nil";
            }
            String satherills = sillhyp + "," + silldia + "," + sillcan + "," + silloth;

            millfamStatArray[2] = satherills;
            Log.e("Valueeeee===ffff", satherills);
        }

        if (gfna.isChecked()) {
            millfamStatArray[3] = "N/A";
        } else {
            String gfillhyp, gfilldia, gfillcan, gfilloth = null;
            if (gfhyper.isChecked()) {
                gfillhyp = gfhyper.getText().toString();
            } else {
                gfillhyp = "Nil";
            }
            if (gfdia.isChecked()) {
                gfilldia = gfdia.getText().toString();
            } else {
                gfilldia = "Nil";
            }
            if (gfcan.isChecked()) {
                gfillcan = gfcan.getText().toString();
            } else {
                gfillcan = "Nil";
            }
            if (gfOther.isChecked()) {
                String gfotherm = gfOedittext.getText().toString();
                if (gfotherm.matches("")) {
                    //		Toast.makeText(getActivity(), "Enter Grandfather Other ill", Toast.LENGTH_SHORT).show();
                } else {
                    gfilloth = gfOedittext.getText().toString();
                }

            } else {
                gfilloth = "Nil";
            }
            String gfatherills = gfillhyp + "," + gfilldia + "," + gfillcan + "," + gfilloth;

            millfamStatArray[3] = gfatherills;
            Log.e("Valueeeee===ffff", gfatherills);
        }


        if (gmna.isChecked()) {
            millfamStatArray[4] = "N/A";
        } else {
            String gmillhyp, gmilldia, gmillcan, gmilloth = null;

            if (gmhyper.isChecked()) {
                gmillhyp = gmhyper.getText().toString();
            } else {
                gmillhyp = "Nil";
            }
            if (gmdia.isChecked()) {
                gmilldia = gmdia.getText().toString();
            } else {
                gmilldia = "Nil";
            }
            if (gmcan.isChecked()) {
                gmillcan = gmcan.getText().toString();
            } else {
                gmillcan = "Nil";
            }
            if (gmOther.isChecked()) {
                String gmotherm = gmOedittext.getText().toString();
                if (gmotherm.matches("")) {
                    //Toast.makeText(getActivity(), "Enter Grandmother Other ill", Toast.LENGTH_SHORT).show();
                } else {
                    gmilloth = gmOedittext.getText().toString();
                }

            } else {
                gmilloth = "Nil";
            }
            String gmatherills = gmillhyp + "," + gmilldia + "," + gmillcan + "," + gmilloth;

            millfamStatArray[4] = gmatherills;
            Log.e("Valueeeee===ffff", String.valueOf(millfamStatArray));
        }

        familyistorystatus_array.add(millfamStatArray[0]);
        familyistorystatus_array.add(millfamStatArray[1]);
        familyistorystatus_array.add(millfamStatArray[2]);
        familyistorystatus_array.add(millfamStatArray[3]);
        familyistorystatus_array.add(millfamStatArray[4]);


        if (noSurgeryReport.isChecked()) {
            surgery_arraylist.add("NA");
            surgery_arraylist.add("NA");
        } else {

            if (surgeryOne.getText().toString().length() != 0) {
                surgery_arraylist.add(surgeryOne.getText().toString());
            } else {
                surgery_arraylist.add("Nil");
            }
            if (surgeryTwo.getText().toString().length() != 0) {
                surgery_arraylist.add(surgeryTwo.getText().toString());
            } else {
                surgery_arraylist.add("Nil");
            }
        }

        if (surgeryoneArraylist.size() > 0) {
            for (int i = 0; i < surgeryoneArraylist.size(); i++) {
                if (surgeryoneArraylist.get(i) != null) {
                    if (surgeryoneArraylist.get(i).getText().toString().length() != 0) {
                        surgery_arraylist.add(surgeryoneArraylist.get(i).getText().toString());
                    }
                }
            }
        }


        if (surgerytwoArraylist.size() > 0) {
            for (int i = 0; i < surgerytwoArraylist.size(); i++) {
                if (surgerytwoArraylist.get(i) != null) {
                    if (surgerytwoArraylist.get(i).getText().toString().length() != 0) {
                        surgery_arraylist.add(surgerytwoArraylist.get(i).getText().toString());
                    }
                }
            }
        }


        String allergy_first, allergy_second, allergy_third, allergy_fourth, allergy_fifth;
        String allergy_status = "", allergy_status_second = "", allergy_status_third = "", allergy_status_fourth = "", allergy_status_fifth = "";

        if (casein.isChecked()) {
            allergy_status += casein.getText().toString() + ",";
        } else {
            allergy_status += "Nil" + ",";
        }
        if (egg.isChecked()) {
            allergy_status += egg.getText().toString() + ",";
        } else {
            allergy_status += "Nil" + ",";
        }
        if (fish.isChecked()) {
            allergy_status += fish.getText().toString() + ",";
        } else {
            allergy_status += "Nil" + ",";
        }
        if (milk.isChecked()) {
            allergy_status += milk.getText().toString() + ",";
        } else {
            allergy_status += "Nil" + ",";
        }
        if (nut.isChecked()) {
            allergy_status += nut.getText().toString();
        } else {
            allergy_status += "Nil";
        }


        if (shellfish.isChecked()) {
            allergy_status_second += shellfish.getText().toString() + ",";
        } else {
            allergy_status_second += "Nil" + ",";
        }

        if (sulfite.isChecked()) {
            allergy_status_second += sulfite.getText().toString() + ",";
        } else {
            allergy_status_second += "Nil" + ",";
        }


        if (soy.isChecked()) {
            allergy_status_second += soy.getText().toString() + ",";
        } else {
            allergy_status_second += "Nil" + ",";
        }

        if (wheat.isChecked()) {
            allergy_status_second += wheat.getText().toString() + ",";
        } else {
            allergy_status_second += "Nil" + ",";
        }

        if (other.isChecked() && etallergyOther1.getText().toString().length() != 0) {
            allergy_status_second += etallergyOther1.getText().toString();
        } else {
            allergy_status_second += "Nil";
        }


        if (spring.isChecked()) {
            allergy_status_third += spring.getText().toString() + ",";
        } else {
            allergy_status_third += "Nil" + ",";
        }


        if (summer.isChecked()) {
            allergy_status_third += summer.getText().toString() + ",";
        } else {
            allergy_status_third += "Nil" + ",";
        }


        if (fall.isChecked()) {
            allergy_status_third += fall.getText().toString() + ",";
        } else {
            allergy_status_third += "Nil" + ",";
        }


        if (winter.isChecked()) {
            allergy_status_third += winter.getText().toString() + ",";
        } else {
            allergy_status_third += "Nil" + ",";
        }


        if (bee.isChecked()) {
            allergy_status_fourth += bee.getText().toString() + ",";
        } else {
            allergy_status_fourth += "Nil" + ",";
        }

        if (cat.isChecked()) {
            allergy_status_fourth += cat.getText().toString() + ",";
        } else {
            allergy_status_fourth += "Nil" + ",";
        }

        if (dog.isChecked()) {
            allergy_status_fourth += dog.getText().toString() + ",";
        } else {
            allergy_status_fourth += "Nil" + ",";
        }

        if (insect.isChecked()) {
            allergy_status_fourth += insect.getText().toString() + ",";
        } else {
            allergy_status_fourth += "Nil" + ",";
        }

        if (other2.isChecked() && etallergyOther2.getText().toString().length() != 0) {

            allergy_status_fourth += etallergyOther2.getText().toString();

        } else {
            allergy_status_fourth += "Nil";
        }

        if (dust.isChecked()) {
            allergy_status_fifth += dust.getText().toString() + ",";
        } else {
            allergy_status_fifth += "Nil" + ",";
        }

        if (mold.isChecked()) {
            allergy_status_fifth += mold.getText().toString() + ",";
        } else {
            allergy_status_fifth += "Nil" + ",";
        }

        if (plant.isChecked()) {
            allergy_status_fifth += plant.getText().toString() + ",";
        } else {
            allergy_status_fifth += "Nil" + ",";
        }

        if (pollen.isChecked()) {
            allergy_status_fifth += pollen.getText().toString() + ",";
        } else {
            allergy_status_fifth += "Nil" + ",";
        }

        if (sun.isChecked()) {
            allergy_status_fifth += sun.getText().toString();
        } else {
            allergy_status_fifth += "Nil";
        }


        String allergy = allergy_status + "&" + allergy_status_second + "&" + allergy_status_third + "&" + allergy_status_fourth + "&" + allergy_status_fifth;
        allergy_array.add(allergy_status);
        allergy_array.add(allergy_status_second);
        allergy_array.add(allergy_status_third);
        allergy_array.add(allergy_status_fourth);
        allergy_array.add(allergy_status_fifth);


        if (noAlerReport.isChecked()) {
            medication_array.add("NA");
            medication_array.add("NA");

            reaction_array.add("NA");
            reaction_array.add("NA");

        } else {

            if (medicOne.getText().toString().length() != 0) {
                medication_array.add(medicOne.getText().toString());
            } else {
                medication_array.add("Nil");
            }
            if (medicTwo.getText().toString().length() != 0) {
                medication_array.add(medicTwo.getText().toString());
            } else {
                medication_array.add("Nil");
            }

            if (reactionOne.getText().toString().length() != 0) {
                reaction_array.add(reactionOne.getText().toString());
            } else {
                reaction_array.add("Nil");
            }
            if (reactionOne.getText().toString().length() != 0) {
                reaction_array.add(reactionOne.getText().toString());
            } else {
                reaction_array.add("Nil");
            }


            if (medicationdrugArraylist.size() > 0) {
                for (int i = 0; i < medicationdrugArraylist.size(); i++) {
                    if (medicationdrugArraylist.get(i) != null) {
                        if (medicationdrugArraylist.get(i).getText().toString().length() != 0)
                            medication_array.add(medicationdrugArraylist.get(i).getText().toString());
                    }
                }
            }
            if (reactiondrugArraylist.size() > 0) {
                for (int i = 0; i < reactiondrugArraylist.size(); i++) {
                    if (reactiondrugArraylist.get(i) != null) {
                        if (reactiondrugArraylist.get(i).getText().toString().length() != 0)
                            reaction_array.add(reactiondrugArraylist.get(i).getText().toString());
                    }
                }
            }
        }


        socialhistory_array.put("Alcohol");
        socialhistory_array.put("Tobacco(Smoke)");
        socialhistory_array.put("Tobacco(Chewable)");
        socialhistory_array.put("Other Substance");


        if (alcurr.isChecked()) {
            socialhistorystatus_array.add(alcurr.getText().toString());

        } else if (alpast.isChecked()) {
            socialhistorystatus_array.add(alpast.getText().toString());
        } else if (alna.isChecked()) {
            socialhistorystatus_array.add(alna.getText().toString());
        } else {
            socialhistorystatus_array.add("Nil");
        }

        if (tobcurr.isChecked()) {
            socialhistorystatus_array.add(tobcurr.getText().toString());
        } else if (tobpast.isChecked()) {
            socialhistorystatus_array.add(tobpast.getText().toString());
        } else if (tobna.isChecked()) {
            socialhistorystatus_array.add(tobna.getText().toString());
        } else {
            socialhistorystatus_array.add("Nil");
        }

        if (tobchewablecurr.isChecked()) {
            socialhistorystatus_array.add(tobchewablecurr.getText().toString());
        } else if (tobchewablepast.isChecked()) {
            socialhistorystatus_array.add(tobchewablepast.getText().toString());
        } else if (tobchewablena.isChecked()) {
            socialhistorystatus_array.add(tobchewablena.getText().toString());
        } else {
            socialhistorystatus_array.add("Nil");
        }

        if (osubcurr.isChecked()) {
            socialhistorystatus_array.add(osubcurr.getText().toString());
        } else if (osubpast.isChecked()) {
            socialhistorystatus_array.add(osubpast.getText().toString());
        } else if (osubna.isChecked()) {
            socialhistorystatus_array.add(osubna.getText().toString());
        } else {
            socialhistorystatus_array.add("Nil");
        }

        for (int i = 0; i < 4; i++) {
            socHistStatArray[i] = socialhistorystatus_array.get(i);
        }


        if (otherhistory.getText().toString().length() != 0) {

            otherhistory_array.add(otherhistory.getText().toString());

        } else {
            otherhistory_array.add("Nil");
        }

//        if (menstural_regular.isChecked()) {
//            menstrualregularlist.add(menstural_regular.getText().toString());
//
//        } else if (menstural_irregular.isChecked()) {
//            menstrualregularlist.add(menstural_irregular.getText().toString());
//
//        }

//        menstrualdayslist.add(Integer.toString(menstural_days.getValue()));
//        menstrualcyclelist.add(Integer.toString(menstural_cycle.getValue()));
        menarchelist.add(menarche.getText().toString());
        dysmenorrheapmplist.add(dysmenorrheapmp.getText().toString());

        marriagemiscariageslist.add(Integer.toString(marriagemiscarriages.getValue()));
        marriagechildrenlist.add(Integer.toString(marriagechildren.getValue()));
        marriageyearlist.add(Integer.toString(marriageyear.getValue()));


        System.out.println("pmp_array" + pmpdate_array);
        System.out.println("pmp_array" + pmpflow_array);
        System.out.println("pmp_array" + pmpdysmenorrhea_array);

        System.out.println("lmp_array" + pmpdate_array);
        System.out.println("lmp_array" + pmpflow_array);
        System.out.println("lmp_array" + pmpdysmenorrhea_array);


        System.out.println("majorill_array" + majorill_array);
        System.out.println("majorillstatus_array" + majorillstatus_array);
        System.out.println("familyistory_array" + familyistory_array);
        System.out.println("surgery_array" + surgery_arraylist);
        System.out.println("allergy_array" + allergy_array);
        System.out.println("medication_array" + medication_array);
        System.out.println("reaction_array" + reaction_array);
        System.out.println("socialhistorystatus_array" + socialhistorystatus_array);


//        menstrualdaysata();
//        menstrualregulardata();
//        menstrualcycledata();
        menarchedata();
        dysendata();

        marriagechildrendata();
        marriageyeardata();
        marriagemiscariagesdata();

        pmp_data();
//        lmp_date();
        pmp_flowdata();
//        lmp_flowdata();
        pmp_synddata();
//        lmp_synddata();

        medication_data();
        reaction_data();

        surgery_data();

        majorillness_data();
        majorillness_status();

        familyillnessstatusdata();
        socialhistorystatusdata();
        allergystatusdata();

        otherhistory_data();


        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setMessage("Storing Patient Data ....");
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();


        patientobject.put("patientID", patientId);


        patientobject.put("OtherMedicalHistory", otherhistory_array);


        patientobject.put("MarriageHistoryYear", marriageyearlist);


        patientobject.put("MarriageHistoryChildren", marriagechildrenlist);


        patientobject.put("MarriageHistoryMiscarriages", marriagemiscariageslist);

        patientobject.put("Pmp_date", pmpdate_array);
        patientobject.put("Pmp_flow", pmpflow_array);
        patientobject.put("Pmp_dysmenorrhea", pmpdysmenorrhea_array);

        Log.d("lmpdate_array", lmpdate_array.toString() + " ff");
        patientobject.put("Lmp_date", lmpdate_array);
        patientobject.put("Lmp_flow", lmpflow_array);
        patientobject.put("Lmp_dysmenorrhea", lmpdysmenorrhea_array);


        patientobject.put("Menstrual_Regular_Irregular", menstrualregularlist);

        patientobject.put("Menstrual_Days", menstrualdayslist);
        Log.d("lmpdate_array", menstrualdayslist + " gfh");


        patientobject.put("Menstrual_Cycle", menstrualcyclelist);


        patientobject.put("menarche_number", menarchelist);


        patientobject.put("menopause_number", dysmenorrheapmplist);

        patientobject.put("majorIllness", majorill_array);
        patientobject.put("majorIllnessStatus", majorillstatus_array);

        patientobject.put("majorIllnessFamily", familyistory_array);

        if (majorillnessfamilystatusArray != null) {
            patientobject.put("majorIllnessFamilyStatus", majorillnessfamilystatusArray);
        } else {
            patientobject.put("majorIllnessFamilyStatus", ar_infamarray);
        }

        //patientobject.put("majorIllnessFamilyStatus", familyistorystatus_array);
        patientobject.put("Surgeries", surgery_arraylist);
        //patientobject.put("Allergy_History", allergy_array);
        patientobject.put("allergiesMedication", medication_array);
        patientobject.put("allergiesReaction", reaction_array);
        patientobject.put("socialHistory", socialhistory_array);

        if (socialhistorystatus_resultArray != null) {
            patientobject.put("socialHistoryStatus", socialhistorystatus_resultArray);
        } else {
            patientobject.put("socialHistoryStatus", ar_shistarray);
        }

        if (allergyhistory_resultArray != null) {
            patientobject.put("Allergy_History", allergyhistory_resultArray);
        } else {
            patientobject.put("Allergy_History", ar_allergyarray);
        }


        //patientobject.put("socialHistoryStatus", socialhistorystatus_array);

        patientobject.pinInBackground(new SaveCallback() {

//			private Obstetrics_gynacologist patientObstriticsObj;

            @Override
            public void done(ParseException e) {
                // TODO Auto-generated method stub
                if (e == null) {

                    mProgressDialog.dismiss();
                    Toast.makeText(getActivity(), "successfully saved", Toast.LENGTH_LONG).show();
                    ((FragmentActivityContainer) getActivity()).setPatientobject(patientobject);
                    ((FragmentActivityContainer) getActivity()).check_save = 0;
                    submitButtonDeactivation();

                    if (targetfragment == 5) {

//						FragmentManager fragmentManager = getFragmentManager();
//						FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
//						if(((FragmentActivityContainer)getActivity()).getPatientObstritics()==null){
//							patientObstriticsObj=new Obstetrics_gynacologist();
//							Bundle bundle=new Bundle();
//							bundle.putBoolean("patientflag", patientflag);
//							patientObstriticsObj.setArguments(bundle);
//						}else{
//							patientObstriticsObj=((FragmentActivityContainer)getActivity()).getPatientObstritics();
//						}
//						((FragmentActivityContainer)getActivity()).setPatientObstritics(patientObstriticsObj);
//						fragmentTransaction.replace(R.id.fl_fragment_container, patientObstriticsObj);
//						fragmentTransaction.commit();
                    } else if (targetfragment == 6) {

                        FragmentManager fragmentManager = getFragmentManager();
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        Diagnosisexamination_Gynacologist diagnosis = new Diagnosisexamination_Gynacologist();
                        fragmentTransaction.replace(R.id.fl_fragment_container, diagnosis);
                        fragmentTransaction.commit();
                    } else if (targetfragment == 3) {

                        FragmentManager fragmentManager = getFragmentManager();
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        Patient_Current_Medical_Info_Frag pntcurrmedinfo = new Patient_Current_Medical_Info_Frag();

                        fragmentTransaction.replace(R.id.fl_fragment_container, pntcurrmedinfo);
                        fragmentTransaction.commit();
                    }else if (targetfragment == 7) {

                        FragmentManager fragmentManager = getFragmentManager();
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        Prescriptionmanagement_Gynacologist diagnosis = new Prescriptionmanagement_Gynacologist();
                        fragmentTransaction.replace(R.id.fl_fragment_container, diagnosis);
                        fragmentTransaction.commit();
                    }


                } else {
                    e.printStackTrace();
                }
            }
        });

        patientobject.saveEventually();

    }


    private void menstrualdaysata() {
        // TODO Auto-generated method stub

        String result = "";
        for (int i = 0; i < menstrualdayslist.size(); i++) {
            result = menstrualdayslist.get(i);
        }
        try {
            if (menstrualdayslist_resultArray != null) {
                menstrualdayslist.clear();
                if (patientflag == false) {
                    for (int i = 0; i < menstrualdayslist_resultArray.length() - 1; i++) {
                        menstrualdayslist.add(menstrualdayslist_resultArray.getString(i));
                    }
                } else if (patientflag == true && ((FragmentActivityContainer) getActivity()).getoldpatientupdateflag() == false) {
                    for (int i = 0; i < menstrualdayslist_resultArray.length(); i++) {
                        menstrualdayslist.add(menstrualdayslist_resultArray.getString(i));
                    }
                } else if (patientflag == true && ((FragmentActivityContainer) getActivity()).getoldpatientupdateflag() == true) {
                    for (int i = 0; i < menstrualdayslist_resultArray.length() - 1; i++) {
                        menstrualdayslist.add(menstrualdayslist_resultArray.getString(i));
                    }
                }
                menstrualdayslist.add(result);
            } else {
                menstrualdayslist.add(result);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    private void menstrualcycledata() {
        // TODO Auto-generated method stub

        String result = "";
        for (int i = 0; i < menstrualcyclelist.size(); i++) {
            result = menstrualcyclelist.get(i);
        }
        try {
            if (menstrualcyclelist_resultArray != null) {
                menstrualcyclelist.clear();
                if (patientflag == false) {
                    for (int i = 0; i < menstrualcyclelist_resultArray.length() - 1; i++) {
                        menstrualcyclelist.add(menstrualcyclelist_resultArray.getString(i));
                    }
                } else if (patientflag == true && ((FragmentActivityContainer) getActivity()).getoldpatientupdateflag() == false) {
                    for (int i = 0; i < menstrualcyclelist_resultArray.length(); i++) {
                        menstrualcyclelist.add(menstrualcyclelist_resultArray.getString(i));
                    }
                } else if (patientflag == true && ((FragmentActivityContainer) getActivity()).getoldpatientupdateflag() == true) {
                    for (int i = 0; i < menstrualcyclelist_resultArray.length() - 1; i++) {
                        menstrualcyclelist.add(menstrualcyclelist_resultArray.getString(i));
                    }
                }
                menstrualcyclelist.add(result);
            } else {
                menstrualcyclelist.add(result);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    private void menstrualregulardata() {
        // TODO Auto-generated method stub

        String result = "";
        for (int i = 0; i < menstrualregularlist.size(); i++) {
            result = menstrualregularlist.get(i);
        }
        try {
            if (menstrualregular_resultArray != null) {
                menstrualregularlist.clear();
                if (patientflag == false) {
                    for (int i = 0; i < menstrualregular_resultArray.length() - 1; i++) {
                        menstrualregularlist.add(menstrualregular_resultArray.getString(i));
                    }
                } else if (patientflag == true && ((FragmentActivityContainer) getActivity()).getoldpatientupdateflag() == false) {
                    for (int i = 0; i < menstrualregular_resultArray.length(); i++) {
                        menstrualregularlist.add(menstrualregular_resultArray.getString(i));
                    }
                } else if (patientflag == true && ((FragmentActivityContainer) getActivity()).getoldpatientupdateflag() == true) {
                    for (int i = 0; i < menstrualregular_resultArray.length() - 1; i++) {
                        menstrualregularlist.add(menstrualregular_resultArray.getString(i));
                    }
                }
                menstrualregularlist.add(result);
            } else {
                menstrualregularlist.add(result);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    private void dysendata() {
        // TODO Auto-generated method stub

        String result = "";
        for (int i = 0; i < dysmenorrheapmplist.size(); i++) {
            result = dysmenorrheapmplist.get(i);
        }
        try {
            if (dysmenorrheapmp_resultArray != null) {
                dysmenorrheapmplist.clear();
                if (patientflag == false) {
                    for (int i = 0; i < dysmenorrheapmp_resultArray.length() - 1; i++) {
                        dysmenorrheapmplist.add(dysmenorrheapmp_resultArray.getString(i));
                    }
                } else if (patientflag == true && ((FragmentActivityContainer) getActivity()).getoldpatientupdateflag() == false) {
                    for (int i = 0; i < dysmenorrheapmp_resultArray.length(); i++) {
                        dysmenorrheapmplist.add(dysmenorrheapmp_resultArray.getString(i));
                    }
                } else if (patientflag == true && ((FragmentActivityContainer) getActivity()).getoldpatientupdateflag() == true) {
                    for (int i = 0; i < dysmenorrheapmp_resultArray.length() - 1; i++) {
                        dysmenorrheapmplist.add(dysmenorrheapmp_resultArray.getString(i));
                    }
                }
                dysmenorrheapmplist.add(result);
            } else {
                dysmenorrheapmplist.add(result);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    private void menarchedata() {
        // TODO Auto-generated method stub

        String result = "";
        for (int i = 0; i < menarchelist.size(); i++) {
            result = menarchelist.get(i);
        }
        try {
            if (menarche_resultArray != null) {
                menarchelist.clear();
                if (patientflag == false) {
                    for (int i = 0; i < menarche_resultArray.length() - 1; i++) {
                        menarchelist.add(menarche_resultArray.getString(i));
                    }
                } else if (patientflag == true && ((FragmentActivityContainer) getActivity()).getoldpatientupdateflag() == false) {
                    for (int i = 0; i < menarche_resultArray.length(); i++) {
                        menarchelist.add(menarche_resultArray.getString(i));
                    }
                } else if (patientflag == true && ((FragmentActivityContainer) getActivity()).getoldpatientupdateflag() == true) {
                    for (int i = 0; i < menarche_resultArray.length() - 1; i++) {
                        menarchelist.add(menarche_resultArray.getString(i));
                    }
                }
                menarchelist.add(result);
            } else {
                menarchelist.add(result);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    private void marriageyeardata() {
        // TODO Auto-generated method stub

        String result = "";
        for (int i = 0; i < marriageyearlist.size(); i++) {
            result = marriageyearlist.get(i);
        }
        try {
            marriageyearlist.clear();
            if (marriageyear_resultArray != null) {
                if (patientflag == false) {
                    for (int i = 0; i < marriageyear_resultArray.length() - 1; i++) {
                        marriageyearlist.add(marriageyear_resultArray.getString(i));
                    }
                } else if (patientflag == true && ((FragmentActivityContainer) getActivity()).getoldpatientupdateflag() == false) {
                    for (int i = 0; i < marriageyear_resultArray.length(); i++) {
                        marriageyearlist.add(marriageyear_resultArray.getString(i));
                    }
                } else if (patientflag == true && ((FragmentActivityContainer) getActivity()).getoldpatientupdateflag() == true) {
                    for (int i = 0; i < marriageyear_resultArray.length() - 1; i++) {
                        marriageyearlist.add(marriageyear_resultArray.getString(i));
                    }
                }
                marriageyearlist.add(result);
            } else {
                marriageyearlist.add(result);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    private void marriagechildrendata() {
        // TODO Auto-generated method stub

        String result = "";
        for (int i = 0; i < marriagechildrenlist.size(); i++) {
            result = marriagechildrenlist.get(i);
        }
        try {
            marriagechildrenlist.clear();
            if (marriagechildren_resultArray != null) {
                if (patientflag == false) {
                    for (int i = 0; i < marriagechildren_resultArray.length() - 1; i++) {
                        marriagechildrenlist.add(marriagechildren_resultArray.getString(i));
                    }
                } else if (patientflag == true && ((FragmentActivityContainer) getActivity()).getoldpatientupdateflag() == false) {
                    for (int i = 0; i < marriagechildren_resultArray.length(); i++) {
                        marriagechildrenlist.add(marriagechildren_resultArray.getString(i));
                    }
                } else if (patientflag == true && ((FragmentActivityContainer) getActivity()).getoldpatientupdateflag() == true) {
                    for (int i = 0; i < marriagechildren_resultArray.length() - 1; i++) {
                        marriagechildrenlist.add(marriagechildren_resultArray.getString(i));
                    }
                }
                marriagechildrenlist.add(result);
            } else {
                marriagechildrenlist.add(result);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    private void marriagemiscariagesdata() {
        // TODO Auto-generated method stub

        String result = "";
        for (int i = 0; i < marriagemiscariageslist.size(); i++) {
            result = marriagemiscariageslist.get(i);
        }
        try {
            marriagemiscariageslist.clear();
            if (marriagemiscariages_resultArray != null) {
                if (patientflag == false) {
                    for (int i = 0; i < marriagemiscariages_resultArray.length() - 1; i++) {
                        marriagemiscariageslist.add(marriagemiscariages_resultArray.getString(i));
                    }
                } else if (patientflag == true && ((FragmentActivityContainer) getActivity()).getoldpatientupdateflag() == false) {
                    for (int i = 0; i < marriagemiscariages_resultArray.length(); i++) {
                        marriagemiscariageslist.add(marriagemiscariages_resultArray.getString(i));
                    }
                } else if (patientflag == true && ((FragmentActivityContainer) getActivity()).getoldpatientupdateflag() == true) {
                    for (int i = 0; i < marriagemiscariages_resultArray.length() - 1; i++) {
                        marriagemiscariageslist.add(marriagemiscariages_resultArray.getString(i));
                    }
                }
                marriagemiscariageslist.add(result);
            } else {
                marriagemiscariageslist.add(result);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    private void lmp_synddata() {
        // TODO Auto-generated method stub

        String result = "";
        for (int i = 0; i < lmpdysmenorrhea_array.size(); i++) {
            result += lmpdysmenorrhea_array.get(i) + ",";
        }
        try {
            if (lmpdysmenorrhea_resultarray != null) {
                lmpdysmenorrhea_array.clear();
                if (patientflag == false) {
                    for (int i = 0; i < lmpdysmenorrhea_resultarray.length() - 1; i++) {
                        lmpdysmenorrhea_array.add(lmpdysmenorrhea_resultarray.getString(i));
                    }
                } else if (patientflag == true && ((FragmentActivityContainer) getActivity()).getoldpatientupdateflag() == false) {
                    for (int i = 0; i < lmpdysmenorrhea_resultarray.length(); i++) {
                        lmpdysmenorrhea_array.add(lmpdysmenorrhea_resultarray.getString(i));
                    }
                } else if (patientflag == true && ((FragmentActivityContainer) getActivity()).getoldpatientupdateflag() == true) {
                    for (int i = 0; i < lmpdysmenorrhea_resultarray.length() - 1; i++) {
                        lmpdysmenorrhea_array.add(lmpdysmenorrhea_resultarray.getString(i));
                    }
                }
                lmpdysmenorrhea_array.add(result);
            } else {
                lmpdysmenorrhea_array.add(result);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    private void pmp_synddata() {
        // TODO Auto-generated method stub

        String result = "";
        for (int i = 0; i < pmpdysmenorrhea_array.size(); i++) {
            result += pmpdysmenorrhea_array.get(i) + ",";
        }
        try {
            if (pmpdysmenorrhea_resultarray != null) {
                pmpdysmenorrhea_array.clear();
                if (patientflag == false) {
                    for (int i = 0; i < pmpdysmenorrhea_resultarray.length() - 1; i++) {
                        pmpdysmenorrhea_array.add(pmpdysmenorrhea_resultarray.getString(i));
                    }
                } else if (patientflag == true && ((FragmentActivityContainer) getActivity()).getoldpatientupdateflag() == false) {
                    for (int i = 0; i < pmpdysmenorrhea_resultarray.length(); i++) {
                        pmpdysmenorrhea_array.add(pmpdysmenorrhea_resultarray.getString(i));
                    }
                } else if (patientflag == true && ((FragmentActivityContainer) getActivity()).getoldpatientupdateflag() == true) {
                    for (int i = 0; i < pmpdysmenorrhea_resultarray.length() - 1; i++) {
                        pmpdysmenorrhea_array.add(pmpdysmenorrhea_resultarray.getString(i));
                    }
                }
                pmpdysmenorrhea_array.add(result);
            } else {
                pmpdysmenorrhea_array.add(result);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    private void lmp_flowdata() {
        // TODO Auto-generated method stub


        String result = "";
        for (int i = 0; i < lmpflow_array.size(); i++) {
            result += lmpflow_array.get(i) + ",";
        }
        try {
            if (lmpflow_resultArray != null) {
                lmpflow_array.clear();
                if (patientflag == false) {
                    for (int i = 0; i < lmpflow_resultArray.length() - 1; i++) {
                        lmpflow_array.add(lmpflow_resultArray.getString(i));
                    }
                } else if (patientflag == true && ((FragmentActivityContainer) getActivity()).getoldpatientupdateflag() == false) {
                    for (int i = 0; i < lmpflow_resultArray.length(); i++) {
                        lmpflow_array.add(lmpflow_resultArray.getString(i));
                    }
                } else if (patientflag == true && ((FragmentActivityContainer) getActivity()).getoldpatientupdateflag() == true) {
                    for (int i = 0; i < lmpflow_resultArray.length() - 1; i++) {
                        lmpflow_array.add(lmpflow_resultArray.getString(i));
                    }
                }
                lmpflow_array.add(result);
            } else {
                lmpflow_array.add(result);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    private void pmp_flowdata() {
        // TODO Auto-generated method stub


        String result = "";
        for (int i = 0; i < pmpflow_array.size(); i++) {
            result += pmpflow_array.get(i) + ",";
        }
        try {
            if (pmpflow_resultArray != null) {
                pmpflow_array.clear();
                if (patientflag == false) {
                    for (int i = 0; i < pmpflow_resultArray.length() - 1; i++) {
                        pmpflow_array.add(pmpflow_resultArray.getString(i));
                    }
                } else if (patientflag == true && ((FragmentActivityContainer) getActivity()).getoldpatientupdateflag() == false) {
                    for (int i = 0; i < pmpflow_resultArray.length(); i++) {
                        pmpflow_array.add(pmpflow_resultArray.getString(i));
                    }
                } else if (patientflag == true && ((FragmentActivityContainer) getActivity()).getoldpatientupdateflag() == true) {
                    for (int i = 0; i < pmpflow_resultArray.length() - 1; i++) {
                        pmpflow_array.add(pmpflow_resultArray.getString(i));
                    }
                }
                pmpflow_array.add(result);
            } else {
                pmpflow_array.add(result);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    private void lmp_date() {
        // TODO Auto-generated method stub
        Log.d("lmpdate_array", lmpdate_array + " dsd");
        ArrayList<String> arrNew = new ArrayList<String>(lmpdate_array);
//        String result = "";
//        for (int i = 0; i < lmpdate_array.size(); i++) {
//            result += lmpdate_array.get(i) + ",";
//        }
        try {
            if (lmpdate_resultArray != null) {
                lmpdate_array.clear();
                if (patientflag == false) {
                    for (int i = 0; i < lmpdate_resultArray.length() - 1; i++) {
                        lmpdate_array.add(lmpdate_resultArray.getString(i));
                    }
                } else if (patientflag == true && ((FragmentActivityContainer) getActivity()).getoldpatientupdateflag() == false) {
                    for (int i = 0; i < lmpdate_resultArray.length(); i++) {
                        lmpdate_array.add(lmpdate_resultArray.getString(i));
                    }
                } else if (patientflag == true && ((FragmentActivityContainer) getActivity()).getoldpatientupdateflag() == true) {
                    for (int i = 0; i < lmpdate_resultArray.length() - 1; i++) {
                        lmpdate_array.add(lmpdate_resultArray.getString(i));
                    }
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        for (int i = 0; i < arrNew.size(); i++) {
            lmpdate_array.add(arrNew.get(i));
        }

    }


    private void otherhistory_data() {
        // TODO Auto-generated method stub

        String result = "";
        for (int i = 0; i < otherhistory_array.size(); i++) {
            result = otherhistory_array.get(i);
        }
        try {
            if (otherhistory_resultArray != null) {
                otherhistory_array.clear();
                if (patientflag == false) {
                    for (int i = 0; i < otherhistory_resultArray.length() - 1; i++) {
                        otherhistory_array.add(otherhistory_resultArray.getString(i));
                    }
                } else if (patientflag == true && ((FragmentActivityContainer) getActivity()).getoldpatientupdateflag() == false) {
                    for (int i = 0; i < otherhistory_resultArray.length(); i++) {
                        otherhistory_array.add(otherhistory_resultArray.getString(i));
                    }
                } else if (patientflag == true && ((FragmentActivityContainer) getActivity()).getoldpatientupdateflag() == true) {
                    for (int i = 0; i < otherhistory_resultArray.length() - 1; i++) {
                        otherhistory_array.add(otherhistory_resultArray.getString(i));
                    }
                }
                otherhistory_array.add(result);
            } else {
                otherhistory_array.add(result);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    private void pmp_data() {
        // TODO Auto-generated method stub

        String result = "";
        for (int i = 0; i < pmpdate_array.size(); i++) {
            result += pmpdate_array.get(i) + ",";
        }
        try {
            if (pmpdate_resultArray != null) {
                pmpdate_array.clear();
                if (patientflag == false) {
                    for (int i = 0; i < pmpdate_resultArray.length() - 1; i++) {
                        pmpdate_array.add(pmpdate_resultArray.getString(i));
                    }
                } else if (patientflag == true && ((FragmentActivityContainer) getActivity()).getoldpatientupdateflag() == false) {
                    for (int i = 0; i < pmpdate_resultArray.length(); i++) {
                        pmpdate_array.add(pmpdate_resultArray.getString(i));
                    }
                } else if (patientflag == true && ((FragmentActivityContainer) getActivity()).getoldpatientupdateflag() == true) {
                    for (int i = 0; i < pmpdate_resultArray.length() - 1; i++) {
                        pmpdate_array.add(pmpdate_resultArray.getString(i));
                    }
                }
                pmpdate_array.add(result);
            } else {
                pmpdate_array.add(result);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    private void allergystatusdata() {
        // TODO Auto-generated method stub

        String result = "";
        for (int i = 0; i < allergy_array.size(); i++) {
            result += allergy_array.get(i) + "&";
        }

        if (allergyhistory_resultArray != null) {
            allergy_array.clear();
            if (patientflag == false) {
                try {
                    allergyhistory_resultArray.put(allergyhistory_resultArray.length() - 1, result);
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            } else if (patientflag == true && ((FragmentActivityContainer) getActivity()).getoldpatientupdateflag() == false) {
                allergyhistory_resultArray.put(result);
            } else if (patientflag == true && ((FragmentActivityContainer) getActivity()).getoldpatientupdateflag() == true) {
                try {
                    allergyhistory_resultArray.put(allergyhistory_resultArray.length() - 1, result);
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            }
        } else {
            ar_allergyarray = new JSONArray();
            ar_allergyarray.put(result);
        }


    }

    private void socialhistorystatusdata() {
        // TODO Auto-generated method stub

        String result = "";
        for (int i = 0; i < socHistStatArray.length; i++) {
            result += socHistStatArray[i] + ",";
        }
        if (socialhistorystatus_resultArray != null) {
            if (patientflag == false) {
                try {
                    socialhistorystatus_resultArray.put(socialhistorystatus_resultArray.length() - 1, result);
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            } else if (patientflag == true && ((FragmentActivityContainer) getActivity()).getoldpatientupdateflag() == false) {
                socialhistorystatus_resultArray.put(result);
            } else if (patientflag == true && ((FragmentActivityContainer) getActivity()).getoldpatientupdateflag() == true) {
                try {
                    socialhistorystatus_resultArray.put(socialhistorystatus_resultArray.length() - 1, result);
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            }
        } else {
            ar_shistarray = new JSONArray();
            ar_shistarray.put(result);
        }


    }


    private void familyillnessstatusdata() {
        // TODO Auto-generated method stub

        String result = "";
        for (int i = 0; i < familyistorystatus_array.size(); i++) {
            result += familyistorystatus_array.get(i) + ";";
        }
        //result="[" +result+ "]";
        System.out.println("resultttt" + result);
        if (majorillnessfamilystatusArray != null) {
            if (patientflag == false) {
                try {
                    majorillnessfamilystatusArray.put(majorillnessfamilystatusArray.length() - 1, result);
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            } else if (patientflag == true && ((FragmentActivityContainer) getActivity()).getoldpatientupdateflag() == false) {
                majorillnessfamilystatusArray.put(result);
            } else if (patientflag == true && ((FragmentActivityContainer) getActivity()).getoldpatientupdateflag() == true) {
                try {
                    majorillnessfamilystatusArray.put(majorillnessfamilystatusArray.length() - 1, result);
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
            System.out.println("ar_infam array" + majorillnessfamilystatusArray);
        } else {

            ar_infamarray = new JSONArray();
            ar_infamarray.put(result);

        }


    }


    private void majorillness_status() {
        // TODO Auto-generated method stub

        String allmajorillnessstatus = "";
        for (int i = 0; i < majorillstatus_array.size(); i++) {
            allmajorillnessstatus += majorillstatus_array.get(i) + ",";
        }
        try {
            majorillstatus_array.clear();
            if (majorillnessstatusArray != null) {
                if (patientflag == false) {
                    for (int i = 0; i < majorillnessstatusArray.length() - 1; i++) {
                        majorillstatus_array.add(majorillnessstatusArray.getString(i));
                    }
                } else if (patientflag == true && ((FragmentActivityContainer) getActivity()).getoldpatientupdateflag() == false) {
                    for (int i = 0; i < majorillnessstatusArray.length(); i++) {
                        majorillstatus_array.add(majorillnessstatusArray.getString(i));
                    }
                } else if (patientflag == true && ((FragmentActivityContainer) getActivity()).getoldpatientupdateflag() == true) {
                    for (int i = 0; i < majorillnessstatusArray.length() - 1; i++) {
                        majorillstatus_array.add(majorillnessstatusArray.getString(i));
                    }
                }
                majorillstatus_array.add(allmajorillnessstatus);
            } else {
                majorillstatus_array.add(allmajorillnessstatus);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    private void majorillness_data() {
        // TODO Auto-generated method stub

        String allmajorillness = "";
        for (int i = 0; i < majorill_array.size(); i++) {
            allmajorillness += majorill_array.get(i) + ",";
        }
        try {
            majorill_array.clear();
            if (majorillnessArray != null) {
                if (patientflag == false) {
                    for (int i = 0; i < majorillnessArray.length() - 1; i++) {
                        majorill_array.add(majorillnessArray.getString(i));
                    }
                } else if (patientflag == true && ((FragmentActivityContainer) getActivity()).getoldpatientupdateflag() == false) {
                    for (int i = 0; i < majorillnessArray.length(); i++) {
                        majorill_array.add(majorillnessArray.getString(i));
                    }
                } else if (patientflag == true && ((FragmentActivityContainer) getActivity()).getoldpatientupdateflag() == true) {
                    for (int i = 0; i < majorillnessArray.length() - 1; i++) {
                        majorill_array.add(majorillnessArray.getString(i));
                    }
                }
                majorill_array.add(allmajorillness);
            } else {
                majorill_array.add(allmajorillness);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    private void surgery_data() {
        // TODO Auto-generated method stub

        String allsurgery = "";
        for (int i = 0; i < surgery_arraylist.size(); i++) {
            allsurgery += surgery_arraylist.get(i) + ",";
        }
        try {
            surgery_arraylist.clear();
            if (surgery_resultArray != null) {
                if (patientflag == false) {
                    for (int i = 0; i < surgery_resultArray.length() - 1; i++) {
                        surgery_arraylist.add(surgery_resultArray.getString(i));
                    }
                } else if (patientflag == true && ((FragmentActivityContainer) getActivity()).getoldpatientupdateflag() == false) {
                    for (int i = 0; i < surgery_resultArray.length(); i++) {
                        surgery_arraylist.add(surgery_resultArray.getString(i));
                    }
                } else if (patientflag == true && ((FragmentActivityContainer) getActivity()).getoldpatientupdateflag() == true) {
                    for (int i = 0; i < surgery_resultArray.length() - 1; i++) {
                        surgery_arraylist.add(surgery_resultArray.getString(i));
                    }
                }
                surgery_arraylist.add(allsurgery);
            } else {
                surgery_arraylist.add(allsurgery);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    private void reaction_data() {
        // TODO Auto-generated method stub

        String allreaction = "";
        for (int i = 0; i < reaction_array.size(); i++) {
            allreaction += reaction_array.get(i) + ",";
        }
        try {
            reaction_array.clear();
            if (allergyreaction_resultArray != null) {
                if (patientflag == false) {
                    for (int i = 0; i < allergyreaction_resultArray.length() - 1; i++) {
                        reaction_array.add(allergyreaction_resultArray.getString(i));
                    }
                } else if (patientflag == true && ((FragmentActivityContainer) getActivity()).getoldpatientupdateflag() == false) {
                    for (int i = 0; i < allergyreaction_resultArray.length(); i++) {
                        reaction_array.add(allergyreaction_resultArray.getString(i));
                    }
                } else if (patientflag == true && ((FragmentActivityContainer) getActivity()).getoldpatientupdateflag() == true) {
                    for (int i = 0; i < allergyreaction_resultArray.length() - 1; i++) {
                        reaction_array.add(allergyreaction_resultArray.getString(i));
                    }
                }
                reaction_array.add(allreaction);
            } else {
                reaction_array.add(allreaction);
            }

        } catch (JSONException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }


    }


    private void medication_data() {
        // TODO Auto-generated method stub

        String allmedic = "";
        for (int i = 0; i < medication_array.size(); i++) {
            allmedic += medication_array.get(i) + ",";
        }
        try {
            medication_array.clear();
            if (allergymedication_resultArray != null) {
                if (patientflag == false) {
                    for (int i = 0; i < allergymedication_resultArray.length() - 1; i++) {
                        medication_array.add(allergymedication_resultArray.getString(i));
                    }
                } else if (patientflag == true && ((FragmentActivityContainer) getActivity()).getoldpatientupdateflag() == false) {
                    for (int i = 0; i < allergymedication_resultArray.length(); i++) {
                        medication_array.add(allergymedication_resultArray.getString(i));
                    }
                } else if (patientflag == true && ((FragmentActivityContainer) getActivity()).getoldpatientupdateflag() == true) {
                    for (int i = 0; i < allergymedication_resultArray.length() - 1; i++) {
                        medication_array.add(allergymedication_resultArray.getString(i));
                    }
                }
                medication_array.add(allmedic);
            } else {
                medication_array.add(allmedic);
            }

        } catch (JSONException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }


    }


    public void submitButtonActivation() {
        // TODO Auto-generated method stub
        ((FragmentActivityContainer) getActivity()).getpatientfrag().onbuttonmedhist(1);
    }

    private void submitButtonDeactivation() {
        // TODO Auto-generated method stub
        ((FragmentActivityContainer) getActivity()).getpatientfrag().onbuttonmedhist(0);
    }


    @Override
    public void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
        Log.d("lmpdate_array", "onResume");
        CrashReporter.getInstance().trackScreenView("Patient Current Medical Information Gynaecologist");

        if (patientedit_checkflag == false) {

            marriageyear.setEnabled(false);
            marriagechildren.setEnabled(false);
            marriagemiscarriages.setEnabled(false);

            menarche.setEnabled(false);
            //flowpmp.setEnabled(false);
            dysmenorrheapmp.setEnabled(false);

//            lladdpmp.setEnabled(false);
            //llAddmorepmp.setEnabled(false);

//            datelmp.setEnabled(false);
//            flowlmp.setEnabled(false);
//            dysmenorrhealmp.setEnabled(false);

//            menstural_regular.setEnabled(false);
//            menstural_irregular.setEnabled(false);
//            menstural_days.setEnabled(false);
//            menstural_cycle.setEnabled(false);

            nopresentpasthistory.setEnabled(false);

            hypercurrent.setEnabled(false);
            hyperpast.setEnabled(false);
            hyperna.setEnabled(false);
            hypermedication.setEnabled(false);

            diabcurrent.setEnabled(false);
            diabpast.setEnabled(false);
            diabna.setEnabled(false);
            diabmedication.setEnabled(false);

            thyroidcurrent.setEnabled(false);
            thyroidpast.setEnabled(false);
            thyroidna.setEnabled(false);
            thyroidmedication.setEnabled(false);

            hyperthyroidcurrent.setEnabled(false);
            hyperthyroidpast.setEnabled(false);
            hyperthyroidna.setEnabled(false);
            hyperthyroidmedication.setEnabled(false);

            cystcurrent.setEnabled(false);
            cystpast.setEnabled(false);
            cystna.setEnabled(false);
            cystmedication.setEnabled(false);

            endometriosiscurrent.setEnabled(false);
            endometriosispast.setEnabled(false);
            endometriosisna.setEnabled(false);
            endometriosismedication.setEnabled(false);

            uterinecurrent.setEnabled(false);
            uterinepast.setEnabled(false);
            uterinena.setEnabled(false);
            uterinemedication.setEnabled(false);


            uticurrent.setEnabled(false);
            utipast.setEnabled(false);
            utina.setEnabled(false);
            utimedication.setEnabled(false);

            cancercurrent.setEnabled(false);
            cancerpast.setEnabled(false);
            cancerna.setEnabled(false);
            cancermedication.setEnabled(false);

            llAddpresentpast.setEnabled(false);
            llAddmorebtn.setEnabled(false);

            fhyper.setEnabled(false);
            mhyper.setEnabled(false);
            shyper.setEnabled(false);
            gfhyper.setEnabled(false);
            gmhyper.setEnabled(false);
            fdia.setEnabled(false);
            mdia.setEnabled(false);
            sdia.setEnabled(false);
            gfdia.setEnabled(false);
            gmdia.setEnabled(false);
            fcan.setEnabled(false);
            mcan.setEnabled(false);
            scan.setEnabled(false);
            gfcan.setEnabled(false);
            gmcan.setEnabled(false);
            fOther.setEnabled(false);
            mOther.setEnabled(false);
            sOther.setEnabled(false);
            gfOther.setEnabled(false);
            gmOther.setEnabled(false);
            fna.setEnabled(false);
            mna.setEnabled(false);
            sna.setEnabled(false);
            gfna.setEnabled(false);
            gmna.setEnabled(false);
            fOedittext.setEnabled(false);
            mOedittext.setEnabled(false);
            sOedittext.setEnabled(false);
            gfOedittext.setEnabled(false);
            gmOedittext.setEnabled(false);


            medicOne.setEnabled(false);
            medicTwo.setEnabled(false);
            reactionOne.setEnabled(false);
            reactionTwo.setEnabled(false);
            surgeryOne.setEnabled(false);
            surgeryTwo.setEnabled(false);
            noAlerReport.setEnabled(false);
            //noIllReport.setEnabled(false);
            noSurgeryReport.setEnabled(false);
            addsurgery.setEnabled(false);
            addmoresurgery.setEnabled(false);

            casein.setEnabled(false);
            egg.setEnabled(false);
            fish.setEnabled(false);
            milk.setEnabled(false);
            nut.setEnabled(false);
            shellfish.setEnabled(false);
            sulfite.setEnabled(false);
            soy.setEnabled(false);
            wheat.setEnabled(false);
            other.setEnabled(false);
            etallergyOther1.setEnabled(false);
            spring.setEnabled(false);
            summer.setEnabled(false);
            fall.setEnabled(false);
            winter.setEnabled(false);
            bee.setEnabled(false);
            cat.setEnabled(false);
            dog.setEnabled(false);
            insect.setEnabled(false);
            other2.setEnabled(false);
            etallergyOther2.setEnabled(false);
            dust.setEnabled(false);
            mold.setEnabled(false);
            plant.setEnabled(false);
            pollen.setEnabled(false);
            sun.setEnabled(false);
            addallergy.setEnabled(false);
            addallergybtn.setEnabled(false);
            alcurr.setEnabled(false);
            alpast.setEnabled(false);
            alna.setEnabled(false);
            tobcurr.setEnabled(false);
            tobpast.setEnabled(false);
            tobna.setEnabled(false);
            osubcurr.setEnabled(false);
            osubpast.setEnabled(false);
            osubna.setEnabled(false);
            tobchewablecurr.setEnabled(false);
            tobchewablepast.setEnabled(false);
            tobchewablena.setEnabled(false);
            otherhistory.setEnabled(false);
        }


        if (patientobject != null) {

            dateposition = 0;
            pmpdateArraylist.clear();
            pmpflowArraylist.clear();
            pmpdysmenpmpArraylist.clear();
            pmpremoveArraylist.clear();

            lmpposition = 0;
            lmpdateArraylist.clear();
            lmpflowArraylist.clear();
            lmpdysmenpmpArraylist.clear();
            lmpremoveArraylist.clear();


            surgeryposition = 0;
            surgeryoneArraylist.clear();
            surgerytwoArraylist.clear();
            serialnumberoneArraylist.clear();
            serialnumbertwoArraylist.clear();
            removeArraylist.clear();

            int drugposition = 0;
            medicationdrugArraylist.clear();
            reactiondrugArraylist.clear();
            serialnumbermedArraylist.clear();
            serialnumberreacArraylist.clear();
            drugremoveArraylist.clear();
            //drugclickposition;


            String fname = patientobject.getString("firstName");
            String lname = patientobject.getString("lastName");
            if (fname == null) {
                fname = "FirstName";
            }
            if (lname == null) {
                lname = "LastName";
            }
            patientName.setText(fname + " " + lname);


            if (patientobject.getJSONArray("MarriageHistoryYear") != null) {
                marriageyear_resultArray = patientobject.getJSONArray("MarriageHistoryYear");
                try {
                    marriageyear.setValue(Integer.parseInt(marriageyear_resultArray.getString(marriageyear_resultArray.length() - 1)));
                } catch (NumberFormatException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            }


            if (patientobject.getJSONArray("MarriageHistoryChildren") != null) {
                marriagechildren_resultArray = patientobject.getJSONArray("MarriageHistoryChildren");
                try {
                    marriagechildren.setValue(Integer.parseInt(marriagechildren_resultArray.getString(marriagechildren_resultArray.length() - 1)));
                } catch (NumberFormatException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            }


            if (patientobject.getJSONArray("MarriageHistoryMiscarriages") != null) {
                marriagemiscariages_resultArray = patientobject.getJSONArray("MarriageHistoryMiscarriages");
                try {
                    marriagemiscarriages.setValue(Integer.parseInt(marriagemiscariages_resultArray.getString(marriagemiscariages_resultArray.length() - 1)));
                } catch (NumberFormatException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            }


            //marriageyear.setValue(patientobject.getInt("MarriageHistoryYear"));
            //marriagechildren.setValue(patientobject.getInt("MarriageHistoryChildren"));
            //marriagemiscarriages.setValue(patientobject.getInt("MarriageHistoryMiscarriages"));


            if (patientobject.getJSONArray("menarche_number") != null) {
                menarche_resultArray = patientobject.getJSONArray("menarche_number");
                Log.d("DateGet", menarche_resultArray.toString() + " f");
                try {
                    menarche.setText(menarche_resultArray.getString(menarche_resultArray.length() - 1));
                } catch (NumberFormatException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            }


            if (patientobject.getJSONArray("menopause_number") != null) {
                dysmenorrheapmp_resultArray = patientobject.getJSONArray("menopause_number");
                try {
                    dysmenorrheapmp.setText(dysmenorrheapmp_resultArray.getString(dysmenorrheapmp_resultArray.length() - 1));
                } catch (NumberFormatException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            }




			/*menarche.setValue(patientobject.getInt("menarche_number"));
            dysmenorrheapmp.setValue(patientobject.getInt("menopause_number"));*/

            //pmpdate_resultArray=patientobject.getJSONArray("Pmp_date");
            //pmpflow_resultArray=patientobject.getJSONArray("Pmp_flow");
            //pmpdysmenorrhea_resultarray=patientobject.getJSONArray("Pmp_dysmenorrhea");

            lmpdate_resultArray = patientobject.getJSONArray("Lmp_date");
            // Log.d("DateGet", lmpdate_resultArray.toString() + " f");
            lmpflow_resultArray = patientobject.getJSONArray("Lmp_flow");
            lmpdysmenorrhea_resultarray = patientobject.getJSONArray("Lmp_dysmenorrhea");

            if (patientobject.getJSONArray("Menstrual_Regular_Irregular") != null) {

                menstrualregular_resultArray = patientobject.getJSONArray("Menstrual_Regular_Irregular");
//
//                try {
//                    if (patientobject.getJSONArray("Menstrual_Regular_Irregular").getString(menstrualregular_resultArray.length() - 1).equalsIgnoreCase("Regular")) {
//                        menstural_regular.setChecked(true);
//                    } else if (patientobject.getJSONArray("Menstrual_Regular_Irregular").getString(menstrualregular_resultArray.length() - 1).equalsIgnoreCase("Irregular")) {
//                        menstural_irregular.setChecked(true);
//                    }
//                } catch (JSONException e) {
//                    // TODO Auto-generated catch block
//                    e.printStackTrace();
//                }
            }




			/*menstural_days.setValue(patientobject.getInt("Menstrual_Days"));
            menstural_cycle.setValue(patientobject.getInt("Menstrual_Cycle"));*/


            if (patientobject.getJSONArray("Menstrual_Days") != null) {
                menstrualdayslist_resultArray = patientobject.getJSONArray("Menstrual_Days");
//                try {
//                    menstrualdayslist.setValue(Integer.parseInt(menstrualdayslist_resultArray.getString(menstrualdayslist_resultArray.length() - 1)));
//                } catch (NumberFormatException e) {
//                    // TODO Auto-generated catch block
//                    e.printStackTrace();
//                } catch (JSONException e) {
//                    // TODO Auto-generated catch block
//                    e.printStackTrace();
//                }

            }


            if (patientobject.getJSONArray("Menstrual_Cycle") != null) {
                menstrualcyclelist_resultArray = patientobject.getJSONArray("Menstrual_Cycle");
//                try {
//                    menstural_cycle.setValue(Integer.parseInt(menstrualcyclelist_resultArray.getString(menstrualcyclelist_resultArray.length() - 1)));
//                } catch (NumberFormatException e) {
//                    // TODO Auto-generated catch block
//                    e.printStackTrace();
//                } catch (JSONException e) {
//                    // TODO Auto-generated catch block
//                    e.printStackTrace();
//                }

            }


            majorillnessArray = patientobject.getJSONArray("majorIllness");
            majorillnessstatusArray = patientobject.getJSONArray("majorIllnessStatus");

            majorillnessfamilyArray = patientobject.getJSONArray("majorIllnessFamily");
            majorillnessfamilystatusArray = patientobject.getJSONArray("majorIllnessFamilyStatus");

            surgery_resultArray = patientobject.getJSONArray("Surgeries");
            allergyhistory_resultArray = patientobject.getJSONArray("Allergy_History");

            allergymedication_resultArray = patientobject.getJSONArray("allergiesMedication");
            allergyreaction_resultArray = patientobject.getJSONArray("allergiesReaction");

            socialhistory_resultArray = patientobject.getJSONArray("socialHistory");
            //socialhistorystatus_array
            socialhistorystatus_resultArray = patientobject.getJSONArray("socialHistoryStatus");

			/*if(patientobject.getString("OtherMedicalHistory")!=null){
                if(!patientobject.getString("OtherMedicalHistory").equals("Nil")){
					otherhistory.setText(patientobject.getString("OtherMedicalHistory"));
				}
			}*/
            otherhistory_resultArray = patientobject.getJSONArray("OtherMedicalHistory");

            if (otherhistory_resultArray != null) {

                try {
                    otherhistory.setText(otherhistory_resultArray.get(otherhistory_resultArray.length() - 1).toString());
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            }

//            if (lmpdate_resultArray != null) {

//            try {
//
////                String date[] = lmpdate_resultArray.getString(lmpdate_resultArray.length() - 1).split(",");
////                String flow[] = lmpflow_resultArray.getString(lmpflow_resultArray.length() - 1).split(",");
////                String dysmenorrhea[] = lmpdysmenorrhea_resultarray.getString(lmpdysmenorrhea_resultarray.length() - 1).split(",");
////                String dayes[] = menstrualdayslist_resultArray.getString(menstrualdayslist_resultArray.length() - 1).split(",");
////                String cycle[] = menstrualcyclelist_resultArray.getString(menstrualcyclelist_resultArray.length() - 1).split(",");
//
//                lmpdate_array.clear();
//                for (int i = 0; i < lmpdate_resultArray.length(); i++) {
//                    lmpdate_array.add(lmpdate_resultArray.get(i).toString());
//                }
//                lmpflow_array.clear();
//                for (int i = 0; i < lmpflow_resultArray.length(); i++) {
//                    lmpflow_array.add(lmpflow_resultArray.get(i).toString());
//                }
//                lmpdysmenorrhea_array.clear();
//                for (int i = 0; i < lmpdysmenorrhea_resultarray.length(); i++) {
//                    lmpdysmenorrhea_array.add(lmpdysmenorrhea_resultarray.get(i).toString());
//                }
//                menstrualdayslist.clear();
//                for (int i = 0; i < menstrualdayslist_resultArray.length(); i++) {
//                    menstrualdayslist.add(menstrualdayslist_resultArray.get(i).toString());
//                }
//                menstrualcyclelist.clear();
//                for (int i = 0; i < menstrualcyclelist_resultArray.length(); i++) {
//                    menstrualcyclelist.add(menstrualcyclelist_resultArray.get(i).toString());
//                }
//                menstrualregularlist.clear();
//                for (int i = 0; i < menstrualregular_resultArray.length(); i++) {
//                    menstrualregularlist.add(menstrualregular_resultArray.get(i).toString());
//                }
//
//            } catch (JSONException e1) {
//                // TODO Auto-generated catch block
//                e1.printStackTrace();
//            }
//
//            listCount.clear();
//            for (int i = 1; i < lmpdate_array.size(); i++) {
//                Log.d("DateGet", lmpdate_array.get(i) + " k");
//                listCount.add(0);
//            }
//            lmpAdapter.notifyDataSetChanged();
//            justifyListViewHeightBasedOnChildren(lstMenstrualPeriod);
////            }
            String Illnessstatus[];
            ArrayList<String> illness = new ArrayList<String>();

            if (majorillnessstatusArray != null) {


                try {

                    String val[] = majorillnessstatusArray.getString(majorillnessstatusArray.length() - 1).split(",");
                    for (int i = 0; i < val.length; i++) {
                        illness.add(val[i]);
                    }

                    boolean flag = true;
                    for (int j = 0; j < illness.size(); j++) {

                        if (!illness.get(j).equalsIgnoreCase("NA")) {
                            flag = false;
                        }

                    }


                    if (flag == true) {


                        nopresentpasthistory.setChecked(true);


                        hypercurrent.setChecked(false);
                        hyperpast.setChecked(false);
                        hyperna.setChecked(false);
                        hypermedication.setEnabled(false);

                        hypercurrent.setClickable(false);
                        hyperpast.setClickable(false);
                        hyperna.setClickable(false);
                        hypermedication.setText("");


                        diabcurrent.setChecked(false);
                        diabpast.setChecked(false);
                        diabna.setChecked(false);
                        diabmedication.setEnabled(false);


                        diabcurrent.setClickable(false);
                        diabpast.setClickable(false);
                        diabna.setClickable(false);
                        diabmedication.setText("");


                        thyroidcurrent.setChecked(false);
                        thyroidpast.setChecked(false);
                        thyroidna.setChecked(false);
                        thyroidmedication.setEnabled(false);


                        thyroidcurrent.setClickable(false);
                        thyroidpast.setClickable(false);
                        thyroidna.setClickable(false);
                        thyroidmedication.setText("");


                        hyperthyroidcurrent.setChecked(false);
                        hyperthyroidpast.setChecked(false);
                        hyperthyroidna.setChecked(false);
                        hyperthyroidmedication.setEnabled(false);


                        hyperthyroidcurrent.setClickable(false);
                        hyperthyroidpast.setClickable(false);
                        hyperthyroidna.setClickable(false);
                        hyperthyroidmedication.setText("");


                        cystcurrent.setChecked(false);
                        cystpast.setChecked(false);
                        cystna.setChecked(false);
                        cystmedication.setEnabled(false);


                        cystcurrent.setClickable(false);
                        cystpast.setClickable(false);
                        cystna.setClickable(false);
                        cystmedication.setText("");

                        endometriosiscurrent.setChecked(false);
                        endometriosispast.setChecked(false);
                        endometriosisna.setChecked(false);
                        endometriosismedication.setEnabled(false);


                        endometriosiscurrent.setClickable(false);
                        endometriosispast.setClickable(false);
                        endometriosisna.setClickable(false);
                        endometriosismedication.setText("");


                        uterinecurrent.setChecked(false);
                        uterinepast.setChecked(false);
                        uterinena.setChecked(false);
                        uterinemedication.setEnabled(false);


                        uterinecurrent.setClickable(false);
                        uterinepast.setClickable(false);
                        uterinena.setClickable(false);
                        uterinemedication.setText("");


                        uticurrent.setChecked(false);
                        utipast.setChecked(false);
                        utina.setChecked(false);
                        utimedication.setEnabled(false);


                        uticurrent.setClickable(false);
                        utipast.setClickable(false);
                        utina.setClickable(false);
                        utimedication.setText("");


                        cancercurrent.setChecked(false);
                        cancerpast.setChecked(false);
                        cancerna.setChecked(false);
                        cancermedication.setEnabled(false);


                        cancercurrent.setClickable(false);
                        cancerpast.setClickable(false);
                        cancerna.setClickable(false);
                        cancermedication.setText("");

                        llAddpresentpast.removeAllViews();
                        llAddmorebtn.setClickable(false);


                    } else {


                        hypercurrent.setClickable(true);
                        hyperpast.setClickable(true);
                        hyperna.setClickable(true);
                        diabcurrent.setClickable(true);
                        diabpast.setClickable(true);
                        diabna.setClickable(true);
                        thyroidcurrent.setClickable(true);
                        thyroidpast.setClickable(true);
                        thyroidna.setClickable(true);
                        hyperthyroidcurrent.setClickable(true);
                        hyperthyroidpast.setClickable(true);
                        hyperthyroidna.setClickable(true);
                        cystcurrent.setClickable(true);
                        cystpast.setClickable(true);
                        cystna.setClickable(true);
                        endometriosiscurrent.setClickable(true);
                        endometriosispast.setClickable(true);
                        endometriosisna.setClickable(true);
                        uterinecurrent.setClickable(true);
                        uterinepast.setClickable(true);
                        uterinena.setClickable(true);
                        uticurrent.setClickable(true);
                        utipast.setClickable(true);
                        utina.setClickable(true);
                        cancercurrent.setClickable(true);
                        cancerpast.setClickable(true);
                        cancerna.setClickable(true);
                        llAddmorebtn.setClickable(true);


                        if (illness.get(0).contains("&")) {

                            Illnessstatus = illness.get(0).split("&");
                            if (Illnessstatus[0].equalsIgnoreCase("current")) {
                                hypercurrent.setChecked(true);
                            } else if (Illnessstatus[0].equalsIgnoreCase("past")) {
                                hyperpast.setChecked(true);
                            } else if (Illnessstatus[0].equalsIgnoreCase("N/A")) {
                                hyperna.setChecked(true);
                            }
                            if (!Illnessstatus[1].equalsIgnoreCase("Nil"))
                                hypermedication.setText(Illnessstatus[1]);


                            Illnessstatus = illness.get(1).split("&");
                            if (Illnessstatus[0].equalsIgnoreCase("current")) {
                                diabcurrent.setChecked(true);
                            } else if (Illnessstatus[0].equalsIgnoreCase("past")) {
                                diabpast.setChecked(true);
                            } else if (Illnessstatus[0].equalsIgnoreCase("N/A")) {
                                diabna.setChecked(true);
                            }
                            if (!Illnessstatus[1].equalsIgnoreCase("Nil"))
                                diabmedication.setText(Illnessstatus[1]);


                            Illnessstatus = illness.get(2).split("&");
                            ;
                            if (Illnessstatus[0].equalsIgnoreCase("current")) {
                                thyroidcurrent.setChecked(true);
                            } else if (Illnessstatus[0].equalsIgnoreCase("past")) {
                                thyroidpast.setChecked(true);
                            } else if (Illnessstatus[0].equalsIgnoreCase("N/A")) {
                                thyroidna.setChecked(true);
                            }
                            if (!Illnessstatus[1].equalsIgnoreCase("Nil"))
                                thyroidmedication.setText(Illnessstatus[1]);

                            Illnessstatus = illness.get(3).split("&");
                            ;
                            if (Illnessstatus[0].equalsIgnoreCase("current")) {
                                hyperthyroidcurrent.setChecked(true);
                            } else if (Illnessstatus[0].equalsIgnoreCase("past")) {
                                hyperthyroidpast.setChecked(true);
                            } else if (Illnessstatus[0].equalsIgnoreCase("N/A")) {
                                hyperthyroidna.setChecked(true);
                            }
                            if (!Illnessstatus[1].equalsIgnoreCase("Nil"))
                                hyperthyroidmedication.setText(Illnessstatus[1]);


                            Illnessstatus = illness.get(4).split("&");
                            if (Illnessstatus[0].equalsIgnoreCase("current")) {
                                cystcurrent.setChecked(true);
                            } else if (Illnessstatus[0].equalsIgnoreCase("past")) {
                                cystpast.setChecked(true);
                            } else if (Illnessstatus[0].equalsIgnoreCase("N/A")) {
                                cystna.setChecked(true);
                            }
                            if (!Illnessstatus[1].equalsIgnoreCase("Nil"))
                                cystmedication.setText(Illnessstatus[1]);

                            Illnessstatus = illness.get(5).split("&");
                            if (Illnessstatus[0].equalsIgnoreCase("current")) {
                                endometriosiscurrent.setChecked(true);
                            } else if (Illnessstatus[0].equalsIgnoreCase("past")) {
                                endometriosispast.setChecked(true);
                            } else if (Illnessstatus[0].equalsIgnoreCase("N/A")) {
                                endometriosisna.setChecked(true);
                            }
                            if (!Illnessstatus[1].equalsIgnoreCase("Nil"))
                                endometriosismedication.setText(Illnessstatus[1]);


                            Illnessstatus = illness.get(6).split("&");
                            if (Illnessstatus[0].equalsIgnoreCase("current")) {
                                uterinecurrent.setChecked(true);
                            } else if (Illnessstatus[0].equalsIgnoreCase("past")) {
                                uterinepast.setChecked(true);
                            } else if (Illnessstatus[0].equalsIgnoreCase("N/A")) {
                                uterinena.setChecked(true);
                            }
                            if (!Illnessstatus[1].equalsIgnoreCase("Nil"))
                                uterinemedication.setText(Illnessstatus[1]);


                            Illnessstatus = illness.get(7).split("&");
                            if (Illnessstatus[0].equalsIgnoreCase("current")) {
                                uticurrent.setChecked(true);
                            } else if (Illnessstatus[0].equalsIgnoreCase("past")) {
                                utipast.setChecked(true);
                            } else if (Illnessstatus[0].equalsIgnoreCase("N/A")) {
                                utina.setChecked(true);
                            }
                            if (!Illnessstatus[1].equalsIgnoreCase("Nil"))
                                utimedication.setText(Illnessstatus[1]);


                            Illnessstatus = illness.get(8).split("&");
                            if (Illnessstatus[0].equalsIgnoreCase("current")) {
                                cancercurrent.setChecked(true);
                            } else if (Illnessstatus[0].equalsIgnoreCase("past")) {
                                cancerpast.setChecked(true);
                            } else if (Illnessstatus[0].equalsIgnoreCase("N/A")) {
                                cancerna.setChecked(true);
                            }
                            if (!Illnessstatus[1].equalsIgnoreCase("Nil"))
                                cancermedication.setText(Illnessstatus[1]);

                        } else {


                            if (illness.get(0).equalsIgnoreCase("current")) {
                                hypercurrent.setChecked(true);
                            } else if (illness.get(0).equalsIgnoreCase("past")) {
                                hyperpast.setChecked(true);
                            } else if (illness.get(0).equalsIgnoreCase("N/A")) {
                                hyperna.setChecked(true);
                            }


                            if (illness.get(1).equalsIgnoreCase("current")) {
                                diabcurrent.setChecked(true);
                            } else if (illness.get(1).equalsIgnoreCase("past")) {
                                diabpast.setChecked(true);
                            } else if (illness.get(1).equalsIgnoreCase("N/A")) {
                                diabna.setChecked(true);
                            }

                            if (illness.get(2).equalsIgnoreCase("current")) {
                                cancercurrent.setChecked(true);
                            } else if (illness.get(2).equalsIgnoreCase("past")) {
                                cancerpast.setChecked(true);
                            } else if (illness.get(2).equalsIgnoreCase("N/A")) {
                                cancerna.setChecked(true);
                            }


                        }
                    }

                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }


                if (illness.size() > 9) {
                    for (int i = 9; i < illness.size(); i++) {


                        LayoutInflater inflater = LayoutInflater.from(getActivity());
                        final View add_pmp = inflater.inflate(R.layout.addmorecurrentpast, null);

                        TextView serialno = (TextView) add_pmp.findViewById(R.id.serialnumber);
                        EditText diseasename = (EditText) add_pmp.findViewById(R.id.diseasename);
                        CheckBox current = (CheckBox) add_pmp.findViewById(R.id.cbcurrent);
                        CheckBox past = (CheckBox) add_pmp.findViewById(R.id.cbpast);
                        final CheckBox presentpastna = (CheckBox) add_pmp.findViewById(R.id.cbna);
                        EditText medication = (EditText) add_pmp.findViewById(R.id.etmedication);
                        RelativeLayout remove = (RelativeLayout) add_pmp.findViewById(R.id.rlImagecross);

                        if (patientedit_checkflag == false) {

                            diseasename.setEnabled(false);
                            current.setEnabled(false);
                            past.setEnabled(false);
                            presentpastna.setEnabled(false);
                            medication.setEnabled(false);
                            remove.setEnabled(false);

                        }


                        serialno.setText(Integer.toString(i));

                        try {

                            String[] disease = majorillnessArray.getString(majorillnessArray.length() - 1).split(",");
                            diseasename.setText(disease[i].toString());

                            String status[] = illness.get(i).split("&");

                            if (status[0].equalsIgnoreCase("current")) {
                                current.setChecked(true);
                            } else if (status[0].equalsIgnoreCase("past")) {
                                past.setChecked(true);
                            } else if (status[0].equalsIgnoreCase("N/A")) {
                                presentpastna.setChecked(true);
                            }

                            medication.setText(status[1]);

                        } catch (JSONException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }

                        llAddpresentpast.addView(add_pmp);

                    }
                }
            }

            if (majorillnessfamilystatusArray != null) {


                String[] myAnsArrayfam = null;
                if (majorillnessfamilystatusArray != null) {

                    try {
                        myAnsArrayfam = majorillnessfamilystatusArray.getString(majorillnessfamilystatusArray.length() - 1).split(";");
                        //System.out.println("family illness history"+ar_infam.getString(ar_infam.length()-1));
                    } catch (JSONException medicationlist) {
                        // TODO Auto-generated catch block
                        medicationlist.printStackTrace();
                    }
                    for (int j = 0; j < myAnsArrayfam.length; j++) {
                        String values = myAnsArrayfam[j].toString();
                        String[] value = values.split(",");

                        if (value != null) {

                            if (value[0].toString().equals("N/A")) {

                                if (j == 0) {
                                    System.out.println("NA working........");
                                    fna.setChecked(true);
                                    fhyper.setChecked(false);
                                    fdia.setChecked(false);
                                    fcan.setChecked(false);
                                    fOther.setChecked(false);

									/*fhyper.setEnabled(false);
                                    fdia.setEnabled(false);
									fcan.setEnabled(false);
									fOther.setEnabled(false);*/
                                }
                                if (j == 1) {
                                    mna.setChecked(true);
                                    mhyper.setChecked(false);
                                    mdia.setChecked(false);
                                    mcan.setChecked(false);
                                    mOther.setChecked(false);

									/*mhyper.setEnabled(false);
                                    mdia.setEnabled(false);
									mcan.setEnabled(false);
									mOther.setEnabled(false);*/
                                }
                                if (j == 2) {
                                    sna.setChecked(true);
                                    shyper.setChecked(false);
                                    sdia.setChecked(false);
                                    scan.setChecked(false);
                                    sOther.setChecked(false);

									/*shyper.setEnabled(false);
                                    sdia.setEnabled(false);
									scan.setEnabled(false);
									sOther.setEnabled(false);*/
                                }
                                if (j == 3) {
                                    gfna.setChecked(true);
                                    gfhyper.setChecked(false);
                                    gfdia.setChecked(false);
                                    gfcan.setChecked(false);
                                    gfOther.setChecked(false);

									/*gfhyper.setEnabled(false);
                                    gfdia.setEnabled(false);
									gfcan.setEnabled(false);
									gfOther.setEnabled(false);*/
                                }
                                if (j == 4) {
                                    gmna.setChecked(true);
                                    gmhyper.setChecked(false);
                                    gmdia.setChecked(false);
                                    gmcan.setChecked(false);
                                    gmOther.setChecked(false);

									/*	gmhyper.setEnabled(false);
                                    gmdia.setEnabled(false);
									gmcan.setEnabled(false);
									gmOther.setEnabled(false);
									 */
                                }


                            } else {


                                if (value[0].toString().equals("Hypertension")) {
                                    if (j == 0) {

                                        System.out.println("father hyper workinggggggggg");
                                        fhyper.setChecked(true);
                                    }
                                    if (j == 1) {
                                        mhyper.setChecked(true);
                                    }
                                    if (j == 2) {
                                        shyper.setChecked(true);
                                    }
                                    if (j == 3) {
                                        gfhyper.setChecked(true);
                                    }
                                    if (j == 4) {
                                        gmhyper.setChecked(true);
                                    }
                                }

                                if (value[1].toString().equals("Diabetes")) {
                                    if (j == 0) {

                                        System.out.println("father diabeties working.....");
                                        fdia.setChecked(true);
                                    }

                                    if (j == 1) {
                                        mdia.setChecked(true);
                                    }
                                    if (j == 2) {
                                        sdia.setChecked(true);

                                    }
                                    if (j == 3) {
                                        gfdia.setChecked(true);

                                    }
                                    if (j == 4) {
                                        gmdia.setChecked(true);

                                    }
                                }
                                if (value[2].toString().equals("Cancer")) {
                                    if (j == 0) {
                                        System.out.println("father cancer working........");
                                        fcan.setChecked(true);

                                    }
                                    if (j == 1) {
                                        mcan.setChecked(true);

                                    }
                                    if (j == 2) {
                                        scan.setChecked(true);

                                    }
                                    if (j == 3) {
                                        gfcan.setChecked(true);

                                    }
                                    if (j == 4) {
                                        gmcan.setChecked(true);

                                    }
                                }

                                if (value[3].toString().equals("Nil")) {

                                    System.out.println("Nil working.....");
                                } else {
                                    if (j == 0) {

                                        System.out.println("father other working............");
                                        if (!(value[3].toString().equalsIgnoreCase("null")))
                                            fOedittext.setText(value[3].toString());

                                        fOther.setChecked(true);
                                    }
                                    if (j == 1) {
                                        if (!(value[3].toString().equalsIgnoreCase("null")))
                                            mOedittext.setText(value[3].toString());

                                        mOther.setChecked(true);
                                    }
                                    if (j == 2) {
                                        if (!(value[3].toString().equalsIgnoreCase("null")))
                                            sOedittext.setText(value[3].toString());

                                        sOther.setChecked(true);
                                    }
                                    if (j == 3) {
                                        if (!(value[3].toString().equalsIgnoreCase("null")))
                                            gfOedittext.setText(value[3].toString());

                                        gfOther.setChecked(true);
                                    }
                                    if (j == 4) {
                                        if (!(value[3].toString().equalsIgnoreCase("null")))
                                            gmOedittext.setText(value[3].toString());

                                        gmOther.setChecked(true);
                                    }
                                }
                            }
                        }
                    }
                    if (patientflag == true) {
                        if (fOther.isChecked()) {
                            fOedittext.setEnabled(true);
                        }
                        if (mOther.isChecked()) {
                            mOedittext.setEnabled(true);
                        }
                        if (sOther.isChecked()) {
                            sOedittext.setEnabled(true);
                        }
                        if (gfOther.isChecked()) {
                            gfOedittext.setEnabled(true);
                        }
                        if (gmOther.isChecked()) {
                            gmOedittext.setEnabled(true);
                        }
                    }
                }














				/*			if(majorillnessfamilystatusArray.length()>0){

					try {


						if(majorillnessfamilystatusArray.getString(0).equalsIgnoreCase("N/A")){
							fna.setChecked(true);
						}else{						
							String[] fatherstatus=majorillnessfamilystatusArray.getString(0).split(",");
							if(fatherstatus[0].equalsIgnoreCase("Hypertension")){
								fhyper.setChecked(true);
							}
							if(fatherstatus[1].equalsIgnoreCase("Diabetes")){
								fdia.setChecked(true);
							}
							if(fatherstatus[2].equalsIgnoreCase("Cancer")){
								fcan.setChecked(true);
							}
							if(fatherstatus[3].equalsIgnoreCase("Nil")){
								fOther.setChecked(false);
							}else{
								fOther.setChecked(true);
								fOedittext.setText(fatherstatus[3]);
							}		
						}
						if(majorillnessfamilystatusArray.getString(1).equalsIgnoreCase("N/A")){
							mna.setChecked(true);
						}else{

							String[] motherstatus=majorillnessfamilystatusArray.getString(1).split(",");
							if(motherstatus[0].equalsIgnoreCase("Hypertension")){
								mhyper.setChecked(true);
							}
							if(motherstatus[1].equalsIgnoreCase("Diabetes")){
								mdia.setChecked(true);
							}
							if(motherstatus[2].equalsIgnoreCase("Cancer")){
								mcan.setChecked(true);
							}
							if(motherstatus[3].equalsIgnoreCase("Nil")){
								mOther.setChecked(false);
							}else{
								mOther.setChecked(true);
								mOedittext.setText(motherstatus[3]);
							}		


						}
						if(majorillnessfamilystatusArray.getString(2).equalsIgnoreCase("N/A")){
							sna.setChecked(true);
						}else{

							String[] siblingstatus=majorillnessfamilystatusArray.getString(2).split(",");
							if(siblingstatus[0].equalsIgnoreCase("Hypertension")){
								shyper.setChecked(true);
							}
							if(siblingstatus[1].equalsIgnoreCase("Diabetes")){
								sdia.setChecked(true);
							}
							if(siblingstatus[2].equalsIgnoreCase("Cancer")){
								scan.setChecked(true);
							}
							if(siblingstatus[3].equalsIgnoreCase("Nil")){
								sOther.setChecked(false);
							}else{
								sOther.setChecked(true);
								sOedittext.setText(siblingstatus[3]);
							}		


						}
						if(majorillnessfamilystatusArray.getString(3).equalsIgnoreCase("N/A")){
							gfna.setChecked(true);
						}else{

							String[] grandfatherstatus=majorillnessfamilystatusArray.getString(3).split(",");
							if(grandfatherstatus[0].equalsIgnoreCase("Hypertension")){
								gfhyper.setChecked(true);
							}
							if(grandfatherstatus[1].equalsIgnoreCase("Diabetes")){
								gfdia.setChecked(true);
							}
							if(grandfatherstatus[2].equalsIgnoreCase("Cancer")){
								gfcan.setChecked(true);
							}
							if(grandfatherstatus[3].equalsIgnoreCase("Nil")){
								gfOther.setChecked(false);
							}else{
								gfOther.setChecked(true);
								gfOedittext.setText(grandfatherstatus[3]);
							}		


						}
						if(majorillnessfamilystatusArray.getString(4).equalsIgnoreCase("N/A")){
							gmna.setChecked(true);
						}else{

							String[] grandmotherstatus=majorillnessfamilystatusArray.getString(4).split(",");
							if(grandmotherstatus[0].equalsIgnoreCase("Hypertension")){
								gmhyper.setChecked(true);
							}
							if(grandmotherstatus[1].equalsIgnoreCase("Diabetes")){
								gmdia.setChecked(true);
							}
							if(grandmotherstatus[2].equalsIgnoreCase("Cancer")){
								gmcan.setChecked(true);
							}
							if(grandmotherstatus[3].equalsIgnoreCase("Nil")){
								gmOther.setChecked(false);
							}else{
								gmOther.setChecked(true);
								gmOedittext.setText(grandmotherstatus[3]);
							}														
						}					
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}			
				}*/
            }

            if (surgery_resultArray != null) {
                String[] myAnsArraysur = null;
                if (surgery_resultArray.length() > 0) {
                    String surgresult = "";
                    myAnsArraysur = new String[surgery_resultArray.length()];
                    try {
                        surgresult = surgery_resultArray.getString(surgery_resultArray.length() - 1);
                        myAnsArraysur = surgresult.split(",");
                    } catch (JSONException medicationlist) {
                        // TODO Auto-generated catch block
                        medicationlist.printStackTrace();
                    }
                    if ((myAnsArraysur[0].toString()).matches("NA")) {

                        noSurgeryReport.setChecked(true);
                        /*surgeryOne.setEnabled(false);
                        surgeryTwo.setEnabled(false);
						addSurge.setEnabled(false);*/

                    } else {


                        if (!(myAnsArraysur[0].toString().equals("Nil"))) {
                            surgeryOne.setText(myAnsArraysur[0].toString());
                        }
                        if (!(myAnsArraysur[1].toString().equals("Nil"))) {
                            surgeryTwo.setText(myAnsArraysur[1].toString());
                        }

                        if (myAnsArraysur.length > 2) {

                            for (int i = 0; i < (myAnsArraysur.length) - 2; i += 2) {

                                LayoutInflater inflater = LayoutInflater.from(getActivity());
                                final View surger_inflate = inflater.inflate(R.layout.addmore_surgery_gynacologist, null);
                                TextView SugNumber = (TextView) surger_inflate.findViewById(R.id.tvSugNumber);
                                TextView SugNumber1 = (TextView) surger_inflate.findViewById(R.id.tvSugNumber1);
                                EditText surgery = (EditText) surger_inflate.findViewById(R.id.etadd_more_surgery_one);
                                EditText surgery1 = (EditText) surger_inflate.findViewById(R.id.etadd_more_surgery_two);
                                final RelativeLayout remove = (RelativeLayout) surger_inflate.findViewById(R.id.rlImagecross);


                                if (patientedit_checkflag == false) {

                                    surgery.setEnabled(false);
                                    surgery1.setEnabled(false);
                                }


                                if (!myAnsArraysur[i + 2].toString().equalsIgnoreCase("Nil"))
                                    surgery.setText(myAnsArraysur[i + 2].toString());
                                if (!myAnsArraysur[i + 2].toString().equalsIgnoreCase("Nil"))
                                    surgery1.setText(myAnsArraysur[i + 2].toString());


                                SugNumber.setTag(surgeryposition);
                                SugNumber1.setTag(surgeryposition);
                                surgery.setTag(surgeryposition);
                                surgery1.setTag(surgeryposition);
                                remove.setTag(surgeryposition);


                                surgeryoneArraylist.add(surgeryposition, surgery);
                                surgerytwoArraylist.add(surgeryposition, surgery1);
                                serialnumberoneArraylist.add(surgeryposition, SugNumber);
                                serialnumbertwoArraylist.add(surgeryposition, SugNumber1);
                                removeArraylist.add(surgeryposition, remove);

                                surgeryposition++;

                                removeArraylist.get((Integer) remove.getTag()).setOnClickListener(new OnClickListener() {

                                    @Override
                                    public void onClick(View v) {
                                        // TODO Auto-generated method stub

                                        submitButtonDeactivation();

                                        View removeView = (LinearLayout) surger_inflate.findViewById(R.id.llAddSurgeries);
                                        ViewGroup parent = (ViewGroup) removeView.getParent();
                                        parent.removeView(removeView);

                                        surgeryoneArraylist.add((Integer) remove.getTag(), null);
                                        surgerytwoArraylist.add((Integer) remove.getTag(), null);
                                        serialnumberoneArraylist.add((Integer) remove.getTag(), null);
                                        serialnumbertwoArraylist.add((Integer) remove.getTag(), null);
                                        removeArraylist.add((Integer) remove.getTag(), null);


                                    }
                                });


                                SugNumber.setText(Integer.toString(i + 3) + ".");
                                SugNumber1.setText(Integer.toString(i + 4) + ".");


                                addsurgery.addView(surger_inflate);
                            }
                        }
                    }
                }
            }

            if (allergyhistory_resultArray != null) {
                if (allergyhistory_resultArray.length() > 0) {

                    String allergy[];
                    String allergy_total[];

                    try {

                        allergy_total = allergyhistory_resultArray.getString(allergyhistory_resultArray.length() - 1).split("&");

                        allergy = allergy_total[0].split(",");

                        if (allergy_total[0].equalsIgnoreCase("Nil")) {

                        } else {

                            if (allergy[0].equalsIgnoreCase("casein")) {
                                casein.setChecked(true);
                            }
                            if (allergy[1].equalsIgnoreCase("egg")) {
                                egg.setChecked(true);
                            }
                            if (allergy[2].equalsIgnoreCase("fish")) {
                                fish.setChecked(true);
                            }
                            if (allergy[3].equalsIgnoreCase("milk")) {
                                milk.setChecked(true);
                            }
                            if (allergy[4].equalsIgnoreCase("nut")) {
                                nut.setChecked(true);
                            }


                        }


                        allergy = allergy_total[1].split(",");

                        if (allergy_total[1].equalsIgnoreCase("Nil")) {

                        } else {

                            if (allergy[0].equalsIgnoreCase("shellfish")) {
                                shellfish.setChecked(true);
                            }
                            if (allergy[1].equalsIgnoreCase("sulfite")) {
                                sulfite.setChecked(true);
                            }
                            if (allergy[2].equalsIgnoreCase("soy")) {
                                soy.setChecked(true);
                            }
                            if (allergy[3].equalsIgnoreCase("wheat")) {
                                wheat.setChecked(true);
                            }
                            if (!allergy[4].equalsIgnoreCase("Nil")) {
                                other.setChecked(true);
                                etallergyOther1.setText(allergy[4]);
                            }


                        }


                        allergy = allergy_total[2].split(",");

                        if (allergy_total[2].equalsIgnoreCase("Nil")) {

                        } else {

                            if (allergy[0].equalsIgnoreCase("spring")) {
                                spring.setChecked(true);
                            }
                            if (allergy[1].equalsIgnoreCase("summer")) {
                                summer.setChecked(true);
                            }
                            if (allergy[2].equalsIgnoreCase("fall")) {
                                fall.setChecked(true);
                            }
                            if (allergy[3].equalsIgnoreCase("winter")) {
                                winter.setChecked(true);
                            }

                        }


                        allergy = allergy_total[3].split(",");

                        if (allergy_total[3].equalsIgnoreCase("Nil")) {

                        } else {

                            if (allergy[0].equalsIgnoreCase("Bee")) {
                                bee.setChecked(true);
                            }
                            if (allergy[1].equalsIgnoreCase("cat")) {
                                cat.setChecked(true);
                            }
                            if (allergy[2].equalsIgnoreCase("dog")) {
                                dog.setChecked(true);
                            }
                            if (allergy[3].equalsIgnoreCase("insect")) {
                                insect.setChecked(true);
                            }
                            if (!allergy[4].equalsIgnoreCase("Nil")) {
                                other2.setChecked(true);
                                etallergyOther2.setText(allergy[4]);
                            }


                        }


                        allergy = allergy_total[4].split(",");
                        if (allergy_total[4].equalsIgnoreCase("Nil")) {

                        } else {

                            if (allergy[0].equalsIgnoreCase("dust")) {
                                dust.setChecked(true);
                            }
                            if (allergy[1].equalsIgnoreCase("mold")) {
                                mold.setChecked(true);
                            }
                            if (allergy[2].equalsIgnoreCase("plant")) {
                                plant.setChecked(true);
                            }
                            if (allergy[3].equalsIgnoreCase("pollen")) {
                                pollen.setChecked(true);
                            }
                            if (allergy[4].equalsIgnoreCase("sun")) {
                                sun.setChecked(true);
                            }

                        }
                    } catch (JSONException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            }

            //allergyreaction_resultArray

            String[] myAnsArray = null;
            if (allergymedication_resultArray != null) {
                String result = "";
                myAnsArray = new String[allergymedication_resultArray.length()];
                try {
                    result = allergymedication_resultArray.getString(allergymedication_resultArray.length() - 1);
                    myAnsArray = result.split(",");
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                for (int j = 0; j < myAnsArray.length; j++) {
                    Log.v("result--", myAnsArray[j]);
                }

                if (myAnsArray[0].toString().matches("NA")) {
                    noAlerReport.setChecked(true);
                } else {
                    noAlerReport.setChecked(false);
                }
            }


            if (allergyreaction_resultArray != null) {
                String[] myAnsArrayreac = null;
                if (allergyreaction_resultArray.length() > 0) {

                    String result = "";
                    myAnsArrayreac = new String[allergymedication_resultArray.length()];
                    try {
                        result = allergyreaction_resultArray.getString(allergyreaction_resultArray.length() - 1);
                        myAnsArrayreac = result.split(",");

                    } catch (JSONException medicationlist) {
                        // TODO Auto-generated catch block
                        medicationlist.printStackTrace();
                    }

                    if ((myAnsArrayreac[0].toString()).matches("NA")) {

                        noAlerReport.setChecked(true);
                        /*medicOne.setEnabled(false);
                    medicTwo.setEnabled(false);
					reactionOne.setEnabled(false);
					reactionTwo.setEnabled(false);
					addAlle.setEnabled(false);*/
                    } else {

                        noAlerReport.setChecked(false);
                        medicOne.setEnabled(true);
                        medicTwo.setEnabled(true);
                        reactionOne.setEnabled(true);
                        reactionTwo.setEnabled(true);

                        if (myAnsArrayreac.length == 2) {
                            if (!(myAnsArray[0].toString().equals("Nil"))) {
                                medicOne.setText(myAnsArray[0].toString());
                            }
                            if (!(myAnsArray[1].toString().equals("Nil"))) {
                                medicTwo.setText(myAnsArray[1].toString());
                            }
                            if (!(myAnsArrayreac[0].toString().equals("Nil"))) {
                                reactionOne.setText(myAnsArrayreac[0].toString());
                            }
                            if (!(myAnsArrayreac[1].toString().equals("Nil"))) {
                                reactionTwo.setText(myAnsArrayreac[1].toString());
                            }
                        }
                        if (myAnsArrayreac.length == 1) {
                            if (!(myAnsArray[0].toString().equals("Nil"))) {
                                medicOne.setText(myAnsArray[0].toString());
                            }
                            if (!(myAnsArrayreac[0].toString().equals("Nil"))) {
                                reactionOne.setText(myAnsArrayreac[0].toString());
                            }
                        }
                        if ((myAnsArrayreac.length) > 2) {

                            if (!(myAnsArray[0].toString().equals("Nil"))) {
                                medicOne.setText(myAnsArray[0].toString());
                            }
                            if (!(myAnsArray[1].toString().equals("Nil"))) {
                                medicTwo.setText(myAnsArray[1].toString());
                            }
                            if (!(myAnsArrayreac[0].toString().equals("Nil"))) {
                                reactionOne.setText(myAnsArrayreac[0].toString());
                            }
                            if (!(myAnsArrayreac[1].toString().equals("Nil"))) {
                                reactionTwo.setText(myAnsArrayreac[1].toString());
                            }
                            medicindex = 3;

							/*medicationlist.clear();
                        reactionlist.clear();*/
                            for (int k = 2; k < myAnsArrayreac.length; k++) {

                                System.out.println("for loop workinggggggg");

                                //	rl =(LinearLayout)view.findViewById(R.id.pmhl33);
                                LinearLayout layout2 = new LinearLayout(getActivity());
                                LayoutInflater inflater_allergy = LayoutInflater.from(getActivity());
                                final View add_allergies = inflater_allergy.inflate(R.layout.add_more_allergies, null);
                                final EditText et_allergy = (EditText) add_allergies.findViewById(R.id.etadd_more_allergey);
                                EditText et_reaction = (EditText) add_allergies.findViewById(R.id.etadd_more_reaction);
                                RelativeLayout iv_remove = (RelativeLayout) add_allergies.findViewById(R.id.rlImagecross);
                                TextView tvnumber = (TextView) add_allergies.findViewById(R.id.tvNumber);
                                TextView tvReacnumber = (TextView) add_allergies.findViewById(R.id.tvReacNumber);


                                if (patientedit_checkflag == false) {
                                    et_allergy.setEnabled(false);
                                    et_reaction.setEnabled(false);
                                    iv_remove.setEnabled(false);

                                }


                                if (!(myAnsArray[k].toString().equals("Nil"))) {
                                    et_allergy.setText(myAnsArray[k].toString());
                                }


                                if (!(myAnsArrayreac[k].toString().toString().equals("Nil"))) {
                                    et_reaction.setText(myAnsArrayreac[k].toString().toString());
                                }


								/*Medserialnumber.setTag(drugposition);
                                Reacserialnumber.setTag(drugposition);*/
                                et_allergy.setTag(drugposition);
                                et_reaction.setTag(drugposition);
                                iv_remove.setTag(drugposition);

                                medicationdrugArraylist.add(drugposition, et_allergy);
                                reactiondrugArraylist.add(drugposition, et_reaction);
                                /*serialnumbermedArraylist.add(drugposition, Medserialnumber);
                                serialnumberreacArraylist.add(drugposition, Reacserialnumber);*/
                                drugremoveArraylist.add(drugposition, iv_remove);

                                int val = drugposition + 3;
                                tvnumber.setText(Integer.toString(val));
                                tvReacnumber.setText(Integer.toString(val));

                                drugposition++;

                                drugremoveArraylist.get((Integer) et_allergy.getTag()).setOnClickListener(new OnClickListener() {

                                    @Override
                                    public void onClick(View v) {
                                        // TODO Auto-generated method stub

                                        submitButtonDeactivation();

                                        View removeView = (LinearLayout) add_allergies.findViewById(R.id.llhidden_layout);
                                        ViewGroup parent = (ViewGroup) removeView.getParent();
                                        parent.removeView(removeView);

                                        medicationdrugArraylist.set((Integer) et_allergy.getTag(), null);
                                        reactiondrugArraylist.set((Integer) et_allergy.getTag(), null);
                                        //	serialnumbermedArraylist.set((Integer)et_allergy.getTag(), null);
                                        //serialnumberreacArraylist.set((Integer)et_allergy.getTag(), null);
                                        drugremoveArraylist.set((Integer) et_allergy.getTag(), null);
                                    }
                                });


                                addallergy.addView(add_allergies);
                            }
                        }
                    }
                }
            }


            String[] myAnsArraysh = null;
            if (socialhistorystatus_resultArray != null) {
                try {
                    myAnsArraysh = socialhistorystatus_resultArray.getString(socialhistorystatus_resultArray.length() - 1).split(",");
                } catch (JSONException medicationlist) {
                    // TODO Auto-generated catch block
                    medicationlist.printStackTrace();
                }

                if (myAnsArraysh[0].toString().equals("Current")) {

                    alcurr.setChecked(true);
                    alpast.setChecked(false);
                    alna.setChecked(false);
                    //	alpast.setEnabled(false);
                    //alna.setEnabled(false);
                }
                if (myAnsArraysh[0].toString().equals("Past")) {

                    alcurr.setChecked(false);
                    alpast.setChecked(true);
                    alna.setChecked(false);
                    //alcurr.setEnabled(false);
                    //alna.setEnabled(false);
                }
                if (myAnsArraysh[0].toString().equals("N/A")) {

                    alcurr.setChecked(false);
                    alpast.setChecked(false);
                    alna.setChecked(true);

                    //alcurr.setEnabled(false);
                    //alpast.setEnabled(false);
                }

                if (myAnsArraysh[1].toString().equals("Current")) {

                    tobcurr.setChecked(true);
                    tobpast.setChecked(false);
                    tobna.setChecked(false);

                    //tobpast.setEnabled(false);
                    //tobna.setEnabled(false);
                }
                if (myAnsArraysh[1].toString().equals("Past")) {
                    tobpast.setChecked(true);
                    tobcurr.setChecked(false);
                    tobna.setChecked(false);

                    //tobcurr.setEnabled(false);				
                    //tobna.setEnabled(false);
                }
                if (myAnsArraysh[1].toString().equals("N/A")) {

                    tobcurr.setChecked(false);
                    tobpast.setChecked(false);
                    tobna.setChecked(true);

                    //tobcurr.setEnabled(false);
                    //tobpast.setEnabled(false);
                }

                if (myAnsArraysh.length > 3) {

                    if (myAnsArraysh[2].toString().equals("Current")) {

                        tobchewablecurr.setChecked(true);
                        tobchewablepast.setChecked(false);
                        tobchewablena.setChecked(false);

                        //tobchewablepast.setEnabled(false);
                        //tobchewablena.setEnabled(false);
                    }
                    if (myAnsArraysh[2].toString().equals("Past")) {
                        tobchewablepast.setChecked(true);
                        tobchewablecurr.setChecked(false);
                        tobchewablena.setChecked(false);

						/*tobcurr.setEnabled(false);				
                tobna.setEnabled(false);*/
                    }
                    if (myAnsArraysh[2].toString().equals("N/A")) {

                        tobchewablecurr.setChecked(false);
                        tobchewablepast.setChecked(false);
                        tobchewablena.setChecked(true);

						/*tobcurr.setEnabled(false);
                tobpast.setEnabled(false);*/
                    }


                    if (myAnsArraysh[3].toString().equals("Current")) {

                        osubcurr.setChecked(true);
                        osubpast.setChecked(false);
                        osubna.setChecked(false);

                        //osubpast.setEnabled(false);
                        //osubna.setEnabled(false);
                    }
                    if (myAnsArraysh[3].toString().equals("Past")) {
                        osubpast.setChecked(true);
                        osubcurr.setChecked(false);
                        osubna.setChecked(false);
                        //osubcurr.setEnabled(false);
                        //osubna.setEnabled(false);
                    }
                    if (myAnsArraysh[3].toString().equals("N/A")) {

                        osubcurr.setChecked(false);
                        osubpast.setChecked(false);
                        osubna.setChecked(true);

                        //osubcurr.setEnabled(false);
                        //osubpast.setEnabled(false);
                    }
                } else {

                    if (myAnsArraysh[2].toString().equals("Current")) {

                        osubcurr.setChecked(true);
                        osubpast.setChecked(false);
                        osubna.setChecked(false);

                        //osubpast.setEnabled(false);
                        //osubna.setEnabled(false);
                    }
                    if (myAnsArraysh[2].toString().equals("Past")) {
                        osubpast.setChecked(true);
                        osubcurr.setChecked(false);
                        osubna.setChecked(false);
                        //osubcurr.setEnabled(false);
                        //osubna.setEnabled(false);
                    }
                    if (myAnsArraysh[2].toString().equals("N/A")) {

                        osubcurr.setChecked(false);
                        osubpast.setChecked(false);
                        osubna.setChecked(true);

                        //osubcurr.setEnabled(false);
                        //osubpast.setEnabled(false);


                    }
                }
            }
        }

    }

    protected void hideKeyboard(View view) {
        InputMethodManager in = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        in.hideSoftInputFromWindow(view.getWindowToken(),
                InputMethodManager.HIDE_NOT_ALWAYS);
    }


    ///////////////////////////////////////CheckBox
    public void setCheckBox(int position, String item) {
        View view = lstMenstrualPeriod.getChildAt(position);
        CheckBox chkRegular = (CheckBox) view.findViewById(R.id.cbregular);
        CheckBox chkIrregular = (CheckBox) view.findViewById(R.id.cbirregular);
        if (item.equals("regular")) {

            chkRegular.setChecked(true);
            chkIrregular.setChecked(false);
            try {
                menstrualregularlist.remove(position);
                menstrualregularlist.add(position, chkRegular.getText().toString());
            } catch (IndexOutOfBoundsException e) {

                menstrualregularlist.add(position, chkRegular.getText().toString());
            }
        } else {

            chkRegular.setChecked(false);
            chkIrregular.setChecked(true);
            try {
                menstrualregularlist.remove(position);
                menstrualregularlist.add(position, chkIrregular.getText().toString());
            } catch (IndexOutOfBoundsException e) {
                menstrualregularlist.add(position, chkIrregular.getText().toString());
            }
        }
        submitButtonActivation();
    }

    ////////////////////////////////////set DatePicker
    public void callDatePicker(final int position) {

        startDatePickerDialog1 = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                submitButtonActivation();
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                View v = lstMenstrualPeriod.getChildAt(position);
                TextView txtDate = (TextView) v.findViewById(R.id.date_menstural);
                txtDate.setText(dateFormatter.format(newDate.getTime()));

                ////////////////////store to array
                try {

                    Log.d("lmpdate_array", "DatePicker");
                    lmpdate_array.remove(position);
                    lmpdate_array.add(position, dateFormatter.format(newDate.getTime()));
                } catch (IndexOutOfBoundsException e) {
                    e.printStackTrace();
                    lmpdate_array.add(position, dateFormatter.format(newDate.getTime()));
                }
                Log.d("lmpdate_array", lmpdate_array + " fdsf");
            }

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

        startDatePickerDialog1.getDatePicker().setMaxDate(Calendar.getInstance().getTimeInMillis());


        startDatePickerDialog1.show();
    }

    //////////////////////////////set flow
    public void showFlow(final int pos) {
        spinnerpopupListener = new onSubmitListener() {


            @Override
            public void onSubmit(String arg, int position) {
                // TODO Auto-generated method stub

                submitButtonActivation();

                View v = lstMenstrualPeriod.getChildAt(pos);
                TextView txtFlow = (TextView) v.findViewById(R.id.flow_menstural);
                txtFlow.setText(arg);
                //////////////////////////store to array
                try {
                    lmpflow_array.remove(pos);
                    lmpflow_array.add(pos, arg);
                } catch (IndexOutOfBoundsException e) {
                    e.printStackTrace();
                    lmpflow_array.add(pos, arg);
                }
                spObj.dismiss();
            }
        }

        ;
        Bundle bundle = new Bundle();
        bundle.putString("name", "Flow");
        bundle.putStringArray("items", flowpmp_array);
        spObj = new

                SpinnerPopup();

        spObj.setSubmitListener(spinnerpopupListener);
        spObj.setArguments(bundle);
        spObj.show(

                getFragmentManager(),

                "tag");

    }

    ///////////////////////////////////show Dysmenorrhea

    public void showDysmenorrhea(final int pos) {
        spinnerpopupListener = new onSubmitListener() {


            @Override
            public void onSubmit(String arg, int position) {
                // TODO Auto-generated method stub

                submitButtonActivation();

                View v = lstMenstrualPeriod.getChildAt(pos);
                TextView txtFlow = (TextView) v.findViewById(R.id.dysmenorrhea_menstural);
                txtFlow.setText(arg);

                ///////////////store to arraylist
                try {
                    lmpdysmenorrhea_array.remove(pos);
                    lmpdysmenorrhea_array.add(pos, arg);
                } catch (IndexOutOfBoundsException e) {
                    e.printStackTrace();
                    lmpdysmenorrhea_array.add(pos, arg);
                }
                spObj.dismiss();
            }
        };
        Bundle bundle = new Bundle();
        bundle.putString("name", "Dysmenorrhea");
        bundle.putStringArray("items", dysmenorrhea_array);
        spObj = new SpinnerPopup();
        spObj.setSubmitListener(spinnerpopupListener);
        spObj.setArguments(bundle);
        spObj.show(getFragmentManager(), "tag");
    }

    //////////////////////set menstrual days
    public void setMenstrualDays(int position) {
        String value;
        submitButtonActivation();

        View v = lstMenstrualPeriod.getChildAt(position);
        EditText txtDays = (EditText) v.findViewById(R.id.dayspicker);
        value = txtDays.getText().toString();

        try {
            menstrualdayslist.remove(position);
            menstrualdayslist.add(position, value);
        } catch (Exception e) {
            e.printStackTrace();
            menstrualdayslist.add(position, value);
        }
    }

    //////////////////////set menstrual days
    public void setMenstrualCycle(int position) {
        String value;
        submitButtonActivation();

        View v = lstMenstrualPeriod.getChildAt(position);
        EditText txtDays = (EditText) v.findViewById(R.id.cyclepicker);
        value = txtDays.getText().toString();
        try {
            menstrualcyclelist.remove(position);
            menstrualcyclelist.add(position, value);
        } catch (Exception e) {
            e.printStackTrace();
            menstrualcyclelist.add(position, value);
        }
    }

    ///////////////////////////////Close one row
    public void closeRow(int position) {
        listCount.remove(position);
        try {
            lmpdate_array.remove(position);
            lmpflow_array.remove(position);
            lmpdysmenorrhea_array.remove(position);
            menstrualregularlist.remove(position);
            menstrualcyclelist.remove(position);
            menstrualdayslist.remove(position);
        } catch (IndexOutOfBoundsException e) {
            e.printStackTrace();
        }

        Log.d("lmpdate_array", lmpdate_array + " j");
        lmpAdapter.notifyDataSetChanged();
        justifyListViewHeightBasedOnChildren(lstMenstrualPeriod);

    }

    public void addMoreLmp() {
        lmpdate_array.add("");
        lmpflow_array.add("");
        lmpdysmenorrhea_array.add("");
        menstrualregularlist.add("");
        menstrualcyclelist.add("");
        menstrualdayslist.add("");

        listCount.add(0);
        lmpAdapter.notifyDataSetChanged();
        justifyListViewHeightBasedOnChildren(lstMenstrualPeriod);
        submitButtonActivation();
    }
}




