package com.wiinnova.doctorsdiary.fragment;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.NumberPicker;
import android.widget.NumberPicker.OnValueChangeListener;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.SaveCallback;
import com.wiinnova.doctorsdiary.R;
import com.wiinnova.doctorsdiary.activities.FragmentActivityContainer;
import com.wiinnova.doctorsdiary.fragment.Patient_Frag.onObstriticsSubmit;
import com.wiinnova.doctorsdiary.popupwindow.SpinnerPopup;
import com.wiinnova.doctorsdiary.popupwindow.SpinnerPopup.onSubmitListener;
import com.wiinnova.doctorsdiary.supportclasses.AppUtil;
import com.wiinnova.doctorsdiary.supportclasses.ExpandableHeightGridView;
import com.wiinnova.doctorsdiary.supportclasses.LMPAdapterDiagnosis;
import com.wiinnova.doctorsdiary.supportclasses.ParanumberpickerObject;

import org.json.JSONArray;
import org.json.JSONException;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class Obstetrics_gynacologist extends Fragment implements onObstriticsSubmit {
//    String where;
//
//    public Obstetrics_gynacologist(String where) {
//        this.where = where;
//    }

    public static ExpandableHeightGridView paragrid;

	/*paraelement adapter;*/


    TextView patientUniqueId;
    TextView patientname;
    TextWatcher textWatcher;

    EditText marriedLife;
    EditText bloodGroup;
    EditText gravida;
    EditText para;
    EditText living;
    EditText edtAbortion;
    TextView tvterm;
    EditText abortion;
    CheckBox vaginal;
    CheckBox cesarean;
    CheckBox normal;
    CheckBox forceps;
    CheckBox vaccum;
    TextView helthy;
    CheckBox male;
    CheckBox female;
    EditText year;
    EditText week;
    TextView dateldd;
    TextView dateedd;
    TextView datedog;
    TextView txtAutoPregnancy;

    EditText etPara;
    LinearLayout lladdbtn;
    LinearLayout preg_container;
    public ArrayList<ParanumberpickerObject> ObjectPicker;
    private SpinnerPopup spObj;

    private Integer termviewposition;
    private Integer healthviewposition;


    private int flag;
    int pregposition = 0;
    private String[] term_array;
    private String[] health_array;
    private String[] bloodgroup_array;

    private onSubmitListener spinnerpopupListener;


    ArrayList<TextView> tvtermList = new ArrayList<TextView>();
    ArrayList<EditText> abortionList = new ArrayList<EditText>();
    ArrayList<CheckBox> vaginalList = new ArrayList<CheckBox>();
    ArrayList<CheckBox> cesareanList = new ArrayList<CheckBox>();
    ArrayList<CheckBox> normalList = new ArrayList<CheckBox>();
    ArrayList<CheckBox> forcepsList = new ArrayList<CheckBox>();
    ArrayList<CheckBox> vaccumList = new ArrayList<CheckBox>();
    ArrayList<TextView> helthyList = new ArrayList<TextView>();
    ArrayList<CheckBox> maleList = new ArrayList<CheckBox>();
    ArrayList<CheckBox> femaleList = new ArrayList<CheckBox>();
    ArrayList<EditText> yearList = new ArrayList<EditText>();
    ArrayList<EditText> weekList = new ArrayList<EditText>();
    ArrayList<RelativeLayout> removeArraylist = new ArrayList<RelativeLayout>();

    private boolean patientedit_checkflag;

    private boolean admintab;

    private boolean admintabdefault;

    private ParseObject patientobject;
    private ParseObject diagnosisObject;

    private String patientId;

    private ProgressDialog mProgressDialog;

    private String marriedlife_result;

    private String husbandd_bloodresult;

    private String gravida_result;

    private String para_result;

    private String living_result;

    private JSONArray term_result;

    private JSONArray typeabortion_result;

    private JSONArray typepregnency_result;

    private JSONArray pregnencynormal_result;

    private JSONArray pregnencylive_result;

    private JSONArray pregnencygender_result;

    private JSONArray pregnencyyear_resullt;

    private JSONArray pregnency_week;

    private String pregnencyldd_result;

    private String pregnencyedd_result;

    private String pregnencydog_result;

    private boolean patientflag;

    private String marriedlife_result_array;

    private String husbandd_bloodresult_array;

    private String gravida_result_array;

    private String para_result_array;

    private String living_result_array;

    private JSONArray term_result_array;

    private JSONArray typeabortion_result_array;

    private ArrayList<String> pregtype = new ArrayList<String>();

    private JSONArray typepregnency_result_array;

    private JSONArray pregnencynormal_result_array;

    private ArrayList<String> pregnormal = new ArrayList<String>();

    private ArrayList<String> preghealthy = new ArrayList<String>();

    private JSONArray pregnencylive_result_array;

    private ArrayList<String> preggender = new ArrayList<String>();

    private JSONArray pregnencygender_result_array;

    private JSONArray pregnencyyear_resullt_array;

    private JSONArray pregnency_week_resullt_array;

    private String ldddata;

    private String pregnencyldd_resullt_array;

    private String pregnencyedd_result_array;

    private String edddata;

    private String dogdata;

    private String pregnencydog_result_array;

    private String husbandblood;

    private String pararesult;

    private ArrayList<String> termresult = new ArrayList<String>();

    private ArrayList<String> abortionresult = new ArrayList<String>();

    private DatePickerDialog startDatePickerDialog;
    private DatePickerDialog startDatePickerDialog2;

    private Patient_Frag patientfragObj;

    private ArrayList<String> yearvalue = new ArrayList<String>();

    private ArrayList<String> weekvalue = new ArrayList<String>();
    private PatientCurrentMedicalaInformation_Gynacologist patientmedicalhistoryfragObj;
    ////////////////////////////////////////////////////////////LMP
    ListView lstMenstrualPeriod;
    ArrayList<Integer> listCount = new ArrayList<Integer>();
    LMPAdapterDiagnosis lmpAdapter;
    private LinearLayout llAddmorelmp;
    public Calendar newCalendar;
    public static ArrayList<String> lmpdate_array = new ArrayList<String>(1);
    public static ArrayList<String> lmpflow_array = new ArrayList<String>(1);
    public static ArrayList<String> lmpdysmenorrhea_array = new ArrayList<String>(1);
    public static ArrayList<String> menstrualregularlist = new ArrayList<String>(1);
    public static ArrayList<String> menstrualdayslist = new ArrayList<String>(1);
    public static ArrayList<String> menstrualcyclelist = new ArrayList<String>(1);
    private String[] dysmenorrhea_array;
    private DatePickerDialog startDatePickerDialog1;
    public SimpleDateFormat dateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
    private String[] flowpmp_array;


    public void LMP(Activity activity, View v) {

        lmpdate_array = new ArrayList<String>(1);
        lmpflow_array = new ArrayList<String>(1);
        lmpdysmenorrhea_array = new ArrayList<String>(1);
        menstrualregularlist = new ArrayList<String>(1);
        menstrualdayslist = new ArrayList<String>(1);
        menstrualcyclelist = new ArrayList<String>(1);
        dysmenorrhea_array = getResources().getStringArray(R.array.dysmenorrhea);
        flowpmp_array = getResources().getStringArray(R.array.flowpmp);

        newCalendar = Calendar.getInstance();
        lstMenstrualPeriod = (ListView) v.findViewById(R.id.lstMenstrualPeriod);
        llAddmorelmp = (LinearLayout) v.findViewById(R.id.llAddmorelmp);

        listCount = new ArrayList<Integer>();
        listCount.add(0);
        lmpAdapter = new LMPAdapterDiagnosis(activity, listCount, this, null);
        lstMenstrualPeriod.setAdapter(lmpAdapter);
        AppUtil.justifyListViewHeightBasedOnChildren(lstMenstrualPeriod);
        llAddmorelmp.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                addMoreLmp();

            }
        });
    }

    public void addMoreLmp() {
        lmpdate_array.add("");
        lmpflow_array.add("");
        lmpdysmenorrhea_array.add("");
        menstrualregularlist.add("");
        menstrualcyclelist.add("");
        menstrualdayslist.add("");

        listCount.add(0);
        lmpAdapter.notifyDataSetChanged();
        AppUtil.justifyListViewHeightBasedOnChildren(lstMenstrualPeriod);
        submitButtonActivation();
    }

    ///////////////////////////////////////CheckBox
    public void setCheckBox(int position, String item) {
        View view = lstMenstrualPeriod.getChildAt(position);
        CheckBox chkRegular = (CheckBox) view.findViewById(R.id.cbregular);
        CheckBox chkIrregular = (CheckBox) view.findViewById(R.id.cbirregular);
        if (item.equals("regular")) {

            chkRegular.setChecked(true);
            chkIrregular.setChecked(false);
            try {
                menstrualregularlist.remove(position);
                menstrualregularlist.add(position, chkRegular.getText().toString());
            } catch (IndexOutOfBoundsException e) {

                menstrualregularlist.add(position, chkRegular.getText().toString());
            }
        } else {

            chkRegular.setChecked(false);
            chkIrregular.setChecked(true);
            try {
                menstrualregularlist.remove(position);
                menstrualregularlist.add(position, chkIrregular.getText().toString());
            } catch (IndexOutOfBoundsException e) {
                menstrualregularlist.add(position, chkIrregular.getText().toString());
            }
        }
        submitButtonActivation();
    }

    ////////////////////////////////////set DatePicker
    public void callDatePicker(final int position) {

        startDatePickerDialog1 = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                submitButtonActivation();
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                View v = lstMenstrualPeriod.getChildAt(position);
                TextView txtDate = (TextView) v.findViewById(R.id.date_menstural);
                txtDate.setText(dateFormatter.format(newDate.getTime()));

                ////////////////////////////////////////calculate Expected Delivery date

//                if (position == lstMenstrualPeriod.getChildCount() - 1)/*if position eqals last entry*/ {
//                    Date date = newDate.getTime();
//                    Calendar c = Calendar.getInstance();
//                    c.setTime(date);
//                    c.add(Calendar.DATE, 280);
//                    dateedd.setText(dateFormatter.format(c.getTime()));
//                }
                ////////////////////store to array
                try {

                    Log.d("lmpdate_array", "DatePicker");
                    lmpdate_array.remove(position);
                    lmpdate_array.add(position, dateFormatter.format(newDate.getTime()));
                } catch (IndexOutOfBoundsException e) {
                    e.printStackTrace();
                    lmpdate_array.add(position, dateFormatter.format(newDate.getTime()));
                }
                Log.d("lmpdate_array", lmpdate_array + " fdsf");
            }

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

        startDatePickerDialog1.getDatePicker().setMaxDate(Calendar.getInstance().getTimeInMillis());


        startDatePickerDialog1.show();
    }

    //////////////////////////////set flow
    public void showFlow(final int pos) {
        onSubmitListener popupListener = new onSubmitListener() {


            @Override
            public void onSubmit(String arg, int position) {
                // TODO Auto-generated method stub

                submitButtonActivation();

                View v = lstMenstrualPeriod.getChildAt(pos);
                TextView txtFlow = (TextView) v.findViewById(R.id.flow_menstural);
                txtFlow.setText(arg);
                //////////////////////////store to array
                try {
                    lmpflow_array.remove(pos);
                    lmpflow_array.add(pos, arg);
                } catch (IndexOutOfBoundsException e) {
                    e.printStackTrace();
                    lmpflow_array.add(pos, arg);
                }
                spObj.dismiss();
            }
        };
        Bundle bundle = new Bundle();
        bundle.putString("name", "Flow");
        bundle.putStringArray("items", flowpmp_array);
        spObj = new

                SpinnerPopup();

        spObj.setSubmitListener(popupListener);
        spObj.setArguments(bundle);
        spObj.show(

                getFragmentManager(),

                "tag");

    }

    ///////////////////////////////////show Dysmenorrhea

    public void showDysmenorrhea(final int pos) {
        onSubmitListener spinnerpopupListener = new onSubmitListener() {


            @Override
            public void onSubmit(String arg, int position) {
                // TODO Auto-generated method stub

                submitButtonActivation();

                View v = lstMenstrualPeriod.getChildAt(pos);
                TextView txtFlow = (TextView) v.findViewById(R.id.dysmenorrhea_menstural);
                txtFlow.setText(arg);

                ///////////////store to arraylist
                try {
                    lmpdysmenorrhea_array.remove(pos);
                    lmpdysmenorrhea_array.add(pos, arg);
                } catch (IndexOutOfBoundsException e) {
                    e.printStackTrace();
                    lmpdysmenorrhea_array.add(pos, arg);
                }
                spObj.dismiss();
            }
        };
        Bundle bundle = new Bundle();
        bundle.putString("name", "Dysmenorrhea");
        bundle.putStringArray("items", dysmenorrhea_array);
        spObj = new SpinnerPopup();
        spObj.setSubmitListener(spinnerpopupListener);
        spObj.setArguments(bundle);
        spObj.show(getFragmentManager(), "tag");
    }

    //////////////////////set menstrual days
    public void setMenstrualDays(int position) {
        String value;
        submitButtonActivation();

        View v = lstMenstrualPeriod.getChildAt(position);
        EditText txtDays = (EditText) v.findViewById(R.id.dayspicker);
        value = txtDays.getText().toString();

        try {
            menstrualdayslist.remove(position);
            menstrualdayslist.add(position, value);
        } catch (Exception e) {
            e.printStackTrace();
            menstrualdayslist.add(position, value);
        }
    }

    //////////////////////set menstrual cycle
    public void setMenstrualCycle(int position) {
        String value;
        submitButtonActivation();

        View v = lstMenstrualPeriod.getChildAt(position);
        EditText txtDays = (EditText) v.findViewById(R.id.cyclepicker);
        value = txtDays.getText().toString();
        try {
            menstrualcyclelist.remove(position);
            menstrualcyclelist.add(position, value);
        } catch (Exception e) {
            e.printStackTrace();
            menstrualcyclelist.add(position, value);
        }
    }

    ///////////////////////////////Close one row
    public void closeRow(int position) {
        listCount.remove(position);
        try {
            lmpdate_array.remove(position);
            lmpflow_array.remove(position);
            lmpdysmenorrhea_array.remove(position);
            menstrualregularlist.remove(position);
            menstrualcyclelist.remove(position);
            menstrualdayslist.remove(position);
        } catch (IndexOutOfBoundsException e) {
            e.printStackTrace();
        }

        Log.d("lmpdate_array", lmpdate_array + " j");
        lmpAdapter.notifyDataSetChanged();
        AppUtil.justifyListViewHeightBasedOnChildren(lstMenstrualPeriod);

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        View view = inflater.inflate(R.layout.obstetricshistory_gynacologist, null);


        term_array = getResources().getStringArray(R.array.termarray);
        health_array = getResources().getStringArray(R.array.healtharray);
        bloodgroup_array = getResources().getStringArray(R.array.bloodgroup_array);


        patientUniqueId = (TextView) view.findViewById(R.id.cduniqueid);
        patientname = (TextView) view.findViewById(R.id.tvname);

        etPara = (EditText) view.findViewById(R.id.editpara);
        lladdbtn = (LinearLayout) view.findViewById(R.id.addpregnancy);
        preg_container = (LinearLayout) view.findViewById(R.id.llpregnancycontainer);

        marriedLife = (EditText) view.findViewById(R.id.marriedlife_numpicker);
        bloodGroup = (EditText) view.findViewById(R.id.etbloodgroup);
        gravida = (EditText) view.findViewById(R.id.gravida);
        para = (EditText) view.findViewById(R.id.editpara);
        living = (EditText) view.findViewById(R.id.living);
        edtAbortion = (EditText) view.findViewById(R.id.edtAbortion);
        tvterm = (TextView) view.findViewById(R.id.tvterm);
        abortion = (EditText) view.findViewById(R.id.etabortion);
        vaginal = (CheckBox) view.findViewById(R.id.cbvaginal);
        cesarean = (CheckBox) view.findViewById(R.id.cbcesarean);
        normal = (CheckBox) view.findViewById(R.id.cbnormal);
        forceps = (CheckBox) view.findViewById(R.id.cbforceps);
        vaccum = (CheckBox) view.findViewById(R.id.cbvaccum);
        helthy = (TextView) view.findViewById(R.id.tvhelthy);
        male = (CheckBox) view.findViewById(R.id.cbmale);
        female = (CheckBox) view.findViewById(R.id.cbfemale);
        year = (EditText) view.findViewById(R.id.years);
        week = (EditText) view.findViewById(R.id.weeks);
        dateldd = (TextView) view.findViewById(R.id.lddpicker);
        dateedd = (TextView) view.findViewById(R.id.eddpicker);
        datedog = (TextView) view.findViewById(R.id.dogpicker);
        txtAutoPregnancy = (TextView) view.findViewById(R.id.txtAutoPregnancy);

        textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                submitButtonActivation();
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        };

        txtAutoPregnancy.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                View view = lstMenstrualPeriod.getChildAt(lstMenstrualPeriod.getChildCount() - 1);
                TextView txtdate = (TextView) view.findViewById(R.id.date_menstural);
                try {
                    Calendar newDate = Calendar.getInstance();
                    newDate.setTime(dateFormatter.parse(txtdate.getText().toString()));// all done
                    Date date = newDate.getTime();
                    Calendar c = Calendar.getInstance();
                    c.setTime(date);
                    c.add(Calendar.DATE, 280);
                    dateedd.setText(dateFormatter.format(c.getTime()));
                } catch (java.text.ParseException e) {
                    e.printStackTrace();
                }
            }
        });


        ///////////////call LMP

        LMP(getActivity(), view);
        //////////////////////////////

//		marriedLife.setMaxValue(100);
//		marriedLife.setMinValue(0);


//        year.setMaxValue(100);
//        year.setMinValue(0);
//
//        week.setMaxValue(100);
//        week.setMinValue(0);


        Bundle bundle = getArguments();
        patientflag = bundle.getBoolean("patientflag");
        admintab = bundle.getBoolean("admintab");


        patientfragObj = new Patient_Frag();
        Bundle bundle1 = new Bundle();
        bundle1.putInt("sidemenuitem", 5);
        bundle1.putBoolean("patientflag", patientflag);
        patientfragObj.setArguments(bundle1);

        getFragmentManager().beginTransaction()
                .replace(R.id.fl_sidemenu_container, patientfragObj)
                .commit();

        if (patientfragObj != null) {
            ((FragmentActivityContainer) getActivity()).setpatientfrag(patientfragObj);
        }

        try {
            ((FragmentActivityContainer) getActivity()).setPatientObstritics(this);

            ((FragmentActivityContainer) getActivity()).getpatientfrag().menu_item = 5;

            patientedit_checkflag = ((FragmentActivityContainer) getActivity()).getpatientflagvalue();
            admintab = ((FragmentActivityContainer) getActivity()).getcheckadmintab();
            admintabdefault = ((FragmentActivityContainer) getActivity()).getadmintabdefault();

            if (admintab == true) {
                patientobject = ((FragmentActivityContainer) getActivity()).getOldPatientobject();
            } else if (admintab == false && admintabdefault == false) {
                patientobject = ((FragmentActivityContainer) getActivity()).getPatientobject();
            } else if (admintab == false && admintabdefault == true) {
                patientobject = ((FragmentActivityContainer) getActivity()).getOldPatientobject();
            }
//            if (where.equals("obstetrics"))
            diagnosisObject = ((FragmentActivityContainer) getActivity()).getpatientdiagnosisparseObj();
//            else if(where.equals("previous")){
//                diagnosisObject=
//            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        if (patientflag == true && patientobject != null) {

            patientId = patientobject.getString("patientID");
            patientUniqueId.setText(patientId);
            String fname = patientobject.getString("firstName");
            String lname = patientobject.getString("lastName");
            if (fname == null) {
                fname = "FirstName";
            }
            if (lname == null) {
                lname = "LastName";
            }
            patientname.setText(fname + " " + lname);
            patientUniqueId.setText(patientId);
        }


        SharedPreferences scanpidnew = getActivity().getSharedPreferences("NewPatID", 0);
        String scnewPid = scanpidnew.getString("NewID", null);


        if (diagnosisObject == null) {
            if (scnewPid != null) {
                patientUniqueId.setText(scnewPid);
                SharedPreferences fnname1 = getActivity().getSharedPreferences("fnName", 0);
                patientname.setText(fnname1.getString("fNname", "FirstName" + " " + "LastName"));
            }
        } else {
            SharedPreferences fnname1 = getActivity().getSharedPreferences("fnName", 0);
            patientname.setText(fnname1.getString("fNname", "FirstName" + " " + "LastName"));
            patientUniqueId.setText(diagnosisObject.getString("patientID"));
        }


        marriedLife.addTextChangedListener(textWatcher);


        gravida.addTextChangedListener(textWatcher);
        para.addTextChangedListener(textWatcher);
        living.addTextChangedListener(textWatcher);
        abortion.addTextChangedListener(textWatcher);
        edtAbortion.addTextChangedListener(textWatcher);


        vaginal.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub

                if (vaginal.isChecked()) {
                    cesarean.setChecked(false);
                    submitButtonActivation();
                }
            }
        });

        cesarean.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                if (cesarean.isChecked()) {
                    vaginal.setChecked(false);
                    submitButtonActivation();
                }
            }
        });


        normal.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                if (normal.isChecked()) {

                    forceps.setChecked(false);
                    vaccum.setChecked(false);
                    submitButtonActivation();
                }

            }
        });


        forceps.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                if (forceps.isChecked()) {

                    normal.setChecked(false);
                    vaccum.setChecked(false);
                    submitButtonActivation();
                }

            }
        });


        vaccum.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                if (vaccum.isChecked()) {

                    normal.setChecked(false);
                    forceps.setChecked(false);
                    submitButtonActivation();
                }

            }
        });


        male.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                if (male.isChecked()) {
                    female.setChecked(false);
                    submitButtonActivation();
                }
            }
        });


        female.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                if (female.isChecked()) {
                    male.setChecked(false);
                    submitButtonActivation();
                }
            }
        });

        year.addTextChangedListener(textWatcher);

        week.addTextChangedListener(textWatcher);

        Calendar newCalendar = Calendar.getInstance();
        Calendar cal = Calendar.getInstance();

        dateFormatter = new SimpleDateFormat("dd-MMM-yyyy");

        startDatePickerDialog = new DatePickerDialog(getActivity(), new OnDateSetListener() {
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                dateldd.setText(dateFormatter.format(newDate.getTime()));
            }
        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
        startDatePickerDialog.getDatePicker().setCalendarViewShown(false);


        startDatePickerDialog1 = new DatePickerDialog(getActivity(), new OnDateSetListener() {
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                dateedd.setText(dateFormatter.format(newDate.getTime()));
            }
        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

        startDatePickerDialog1.getDatePicker().setCalendarViewShown(false);


        startDatePickerDialog2 = new DatePickerDialog(getActivity(), new OnDateSetListener() {
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                dateedd.setText(dateFormatter.format(newDate.getTime()));
            }
        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

        startDatePickerDialog2.getDatePicker().setCalendarViewShown(false);


        dateldd.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                System.out.println("working................");
                startDatePickerDialog.show();
                submitButtonActivation();

            }
        });


        dateedd.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                System.out.println("working................");
                startDatePickerDialog2.show();
                submitButtonActivation();

            }
        });


        datedog.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                System.out.println("working................");
                startDatePickerDialog2.show();
                submitButtonActivation();

            }
        });


        bloodGroup.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub

                flag = 3;
                Bundle bundle = new Bundle();
                bundle.putString("name", "Blood Group");
                bundle.putStringArray("items", bloodgroup_array);
                //bundle.putSerializable("listener",CurrentVitals.this);
                spObj = new SpinnerPopup();
                spObj.setSubmitListener(spinnerpopupListener);
                spObj.setArguments(bundle);
                spObj.show(getFragmentManager(), "tag");


            }
        });


        bloodGroup.setOnFocusChangeListener(new OnFocusChangeListener() {

            @Override
            public void onFocusChange(View arg0, boolean b) {
                // TODO Auto-generated method stub

                if (b == true) {

                    flag = 3;
                    Bundle bundle = new Bundle();
                    bundle.putString("name", "Blood Group");
                    bundle.putStringArray("items", bloodgroup_array);
                    //bundle.putSerializable("listener",CurrentVitals.this);
                    spObj = new SpinnerPopup();
                    spObj.setSubmitListener(spinnerpopupListener);
                    spObj.setArguments(bundle);
                    spObj.show(getFragmentManager(), "tag");

                }


            }
        });


        spinnerpopupListener = new onSubmitListener() {

            @Override
            public void onSubmit(String arg, int position) {
                // TODO Auto-generated method stub

                //submitButtonActivation();

                if (flag == 1) {
                    tvtermList.get(termviewposition).setText(arg);
                }

                if (flag == 2) {
                    helthyList.get(healthviewposition).setText(arg);
                }

                if (flag == 3) {

                    bloodGroup.setText(arg);

                }
                if (flag == 4) {
                    tvterm.setText(arg);
                }

                if (flag == 5) {
                    helthy.setText(arg);
                }

                submitButtonActivation();
                spObj.dismiss();


            }
        };


        tvterm.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                flag = 4;
                Bundle bundle = new Bundle();
                bundle.putString("name", "Term");
                bundle.putStringArray("items", term_array);
                spObj = new SpinnerPopup();
                spObj.setSubmitListener(spinnerpopupListener);
                spObj.setArguments(bundle);
                spObj.show(getFragmentManager(), "tag");

            }
        });


        helthy.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub

                flag = 5;
                Bundle bundle = new Bundle();
                bundle.putString("name", "health");
                bundle.putStringArray("items", health_array);
                spObj = new SpinnerPopup();
                spObj.setSubmitListener(spinnerpopupListener);
                spObj.setArguments(bundle);
                spObj.show(getFragmentManager(), "tag");


            }
        });


        lladdbtn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub

                LayoutInflater inflater = LayoutInflater.from(getActivity());
                final View add_preg = inflater.inflate(R.layout.addpregnancy_rowitem, null);

                final TextView tvterm = (TextView) add_preg.findViewById(R.id.tvterm);
                final EditText abortion = (EditText) add_preg.findViewById(R.id.etabortion);
                final CheckBox vaginal = (CheckBox) add_preg.findViewById(R.id.cbvaginal);
                final CheckBox cesarean = (CheckBox) add_preg.findViewById(R.id.cbcesarean);
                final CheckBox normal = (CheckBox) add_preg.findViewById(R.id.cbnormal);
                final CheckBox forceps = (CheckBox) add_preg.findViewById(R.id.cbforceps);
                final CheckBox vaccum = (CheckBox) add_preg.findViewById(R.id.cbvaccum);

                final TextView helthy = (TextView) add_preg.findViewById(R.id.tvhelthy);
                final CheckBox male = (CheckBox) add_preg.findViewById(R.id.cbmale);
                final CheckBox female = (CheckBox) add_preg.findViewById(R.id.cbfemale);
                final EditText year = (EditText) add_preg.findViewById(R.id.years);
                final EditText week = (EditText) add_preg.findViewById(R.id.weeks);

                final RelativeLayout remove = (RelativeLayout) add_preg.findViewById(R.id.rlImagecross);


//                week.setMaxValue(100);
//                week.setMinValue(0);
//
//                year.setMaxValue(100);
//                year.setMinValue(0);

                tvterm.setTag(pregposition);
                abortion.setTag(pregposition);
                vaginal.setTag(pregposition);
                cesarean.setTag(pregposition);
                normal.setTag(pregposition);
                forceps.setTag(pregposition);
                vaccum.setTag(pregposition);

                helthy.setTag(pregposition);
                male.setTag(pregposition);
                female.setTag(pregposition);
                year.setTag(pregposition);
                week.setTag(pregposition);
                remove.setTag(pregposition);


                removeArraylist.add(pregposition, remove);
                tvtermList.add(pregposition, tvterm);
                abortionList.add(pregposition, abortion);
                vaginalList.add(pregposition, vaginal);
                cesareanList.add(pregposition, cesarean);
                normalList.add(pregposition, normal);
                forcepsList.add(pregposition, forceps);
                vaccumList.add(pregposition, vaccum);
                helthyList.add(pregposition, helthy);
                maleList.add(pregposition, male);
                femaleList.add(pregposition, female);
                yearList.add(pregposition, year);
                weekList.add(pregposition, week);

                pregposition++;

                abortionList.get((Integer) (year.getTag())).addTextChangedListener(new TextWatcher() {

                    @Override
                    public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
                        // TODO Auto-generated method stub

                    }

                    @Override
                    public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                                  int arg3) {
                        // TODO Auto-generated method stub

                    }

                    @Override
                    public void afterTextChanged(Editable arg0) {
                        // TODO Auto-generated method stub
                        submitButtonActivation();
                    }
                });


                yearList.get((Integer) (year.getTag())).addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {

                        submitButtonActivation();
                    }

                    @Override
                    public void afterTextChanged(Editable s) {

                    }
                });

                weekList.get((Integer) (year.getTag())).addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {

                        submitButtonActivation();
                    }

                    @Override
                    public void afterTextChanged(Editable s) {

                    }
                });


                normalList.get((Integer) (normal.getTag())).setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View arg0) {
                        // TODO Auto-generated method stub

                        if (normalList.get((Integer) (normal.getTag())).isChecked()) {
                            forcepsList.get((Integer) (forceps.getTag())).setChecked(false);
                            vaccumList.get((Integer) (vaccum.getTag())).setChecked(false);
                            submitButtonActivation();
                        }


                    }
                });


                forcepsList.get((Integer) (forceps.getTag())).setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View arg0) {
                        // TODO Auto-generated method stub

                        if (forcepsList.get((Integer) (forceps.getTag())).isChecked()) {
                            normalList.get((Integer) (normal.getTag())).setChecked(false);
                            vaccumList.get((Integer) (vaccum.getTag())).setChecked(false);
                            submitButtonActivation();
                        }


                    }
                });


                vaccumList.get((Integer) (forceps.getTag())).setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View arg0) {
                        // TODO Auto-generated method stub

                        if (vaccumList.get((Integer) (vaccum.getTag())).isChecked()) {
                            normalList.get((Integer) (normal.getTag())).setChecked(false);
                            forcepsList.get((Integer) (forceps.getTag())).setChecked(false);
                            submitButtonActivation();
                        }


                    }
                });


                maleList.get((Integer) (male.getTag())).setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View arg0) {
                        // TODO Auto-generated method stub

                        if (maleList.get((Integer) (male.getTag())).isChecked()) {
                            femaleList.get((Integer) (female.getTag())).setChecked(false);
                            submitButtonActivation();
                        }


                    }
                });


                femaleList.get((Integer) (female.getTag())).setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View arg0) {
                        // TODO Auto-generated method stub

                        if (femaleList.get((Integer) (female.getTag())).isChecked()) {
                            maleList.get((Integer) (male.getTag())).setChecked(false);
                            submitButtonActivation();
                        }


                    }
                });


                vaginalList.get((Integer) (vaginal.getTag())).setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View arg0) {
                        // TODO Auto-generated method stub

                        if (vaginalList.get((Integer) (vaginal.getTag())).isChecked()) {
                            cesareanList.get((Integer) (cesarean.getTag())).setChecked(false);
                            submitButtonActivation();
                        }


                    }
                });


                cesareanList.get((Integer) (cesarean.getTag())).setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View arg0) {
                        // TODO Auto-generated method stub

                        if (cesareanList.get((Integer) (cesarean.getTag())).isChecked()) {
                            vaginalList.get((Integer) (vaginal.getTag())).setChecked(false);
                            submitButtonActivation();
                        }


                    }
                });


                removeArraylist.get((Integer) (remove.getTag())).setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        // TODO Auto-generated method stub

                        View removeView = (LinearLayout) add_preg.findViewById(R.id.pregcontainer);
                        ViewGroup parent = (ViewGroup) removeView.getParent();
                        parent.removeView(removeView);

                        System.out.println("tag value....." + (Integer) (remove.getTag()));

                        tvtermList.set((Integer) (remove.getTag()), null);
                        abortionList.set((Integer) (remove.getTag()), null);
                        vaginalList.set((Integer) (remove.getTag()), null);
                        cesareanList.set((Integer) (remove.getTag()), null);
                        normalList.set((Integer) (remove.getTag()), null);
                        forcepsList.set((Integer) (remove.getTag()), null);
                        vaccumList.set((Integer) (remove.getTag()), null);
                        helthyList.set((Integer) (remove.getTag()), null);
                        maleList.set((Integer) (remove.getTag()), null);
                        femaleList.set((Integer) (remove.getTag()), null);
                        yearList.set((Integer) (remove.getTag()), null);
                        weekList.set((Integer) (remove.getTag()), null);


                        removeArraylist.set((Integer) (remove.getTag()), null);

                        submitButtonActivation();


                    }
                });


                tvtermList.get((Integer) (tvterm.getTag())).setOnClickListener(new OnClickListener() {


                    @Override
                    public void onClick(View arg0) {
                        // TODO Auto-generated method stub

                        termviewposition = (Integer) (tvterm.getTag());
                        flag = 1;
                        Bundle bundle = new Bundle();
                        bundle.putString("name", "Term");
                        bundle.putStringArray("items", term_array);
                        spObj = new SpinnerPopup();
                        spObj.setSubmitListener(spinnerpopupListener);
                        spObj.setArguments(bundle);
                        spObj.show(getFragmentManager(), "tag");


                    }
                });


                helthyList.get((Integer) (tvterm.getTag())).setOnClickListener(new OnClickListener() {


                    @Override
                    public void onClick(View arg0) {
                        // TODO Auto-generated method stub

                        healthviewposition = (Integer) (tvterm.getTag());
                        flag = 2;
                        Bundle bundle = new Bundle();
                        bundle.putString("name", "health");
                        bundle.putStringArray("items", health_array);
                        spObj = new SpinnerPopup();
                        spObj.setSubmitListener(spinnerpopupListener);
                        spObj.setArguments(bundle);
                        spObj.show(getFragmentManager(), "tag");


                    }
                });


                preg_container.addView(add_preg);

            }
        });



		/*	paragrid=(ExpandableHeightGridView)view.findViewById(R.id.grid);
        paragrid.setExpanded(true);
		ObjectPicker=new ArrayList<ParanumberpickerObject>();*/

		/*	ParanumberpickerObject obj=new ParanumberpickerObject();
        obj.setPickervalue(0);
		ParanumberpickerObject obj1=new ParanumberpickerObject();
		obj1.setPickervalue(0);
		for(int i=0;i<1;i++){
			ObjectPicker.add(obj);
		}*/
        /*	adapter=new paraelement(getActivity(), ObjectPicker);
        paragrid.setAdapter(adapter);
		 */



		/*	paragrid.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long arg3) {
				// TODO Auto-generated method stub

				new ParanumberpickerObject().setPickervalue(position);

			}
		});*/


        return view;
    }

    private void submitButtonActivation() {
        // TODO Auto-generated method stub
        try {
            ((FragmentActivityContainer) getActivity()).getpatientfrag().onButtonactivation(2);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void submitButtonDeactivation() {
        // TODO Auto-generated method stub
        try {
            ((FragmentActivityContainer) getActivity()).getpatientfrag().onButtonactivation(0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static float convertDpToPixel(float dp) {
        DisplayMetrics metrics = Resources.getSystem().getDisplayMetrics();
        float px = dp * (metrics.densityDpi / 160f);
        return Math.round(px);
    }

    @Override
    public void onObstritics(int arg, final int targetfragment) {
        // TODO Auto-generated method stub


        if (year.getText().toString().length() == 0) {
            yearvalue.add("Nil");
        } else {
            yearvalue.add(year.getText().toString());
        }
        Log.d("WeekValue", week.getText() + " week.getText()");
        if (week.getText().toString().length() > 0) {
            weekvalue.add(week.getText().toString());
        } else {
            weekvalue.add("Nil");
        }

        if (abortion.getText().toString() != null && abortion.getText().toString().length() != 0) {
            abortionresult.add(abortion.getText().toString());
        } else {
            abortionresult.add("Nil");
        }

        if (bloodGroup.getText().toString() != null && bloodGroup.getText().length() != 0) {
            husbandblood = bloodGroup.getText().toString();
        } else {
            husbandblood = "Nil";
        }


        if (para.getText().toString() != null && para.getText().toString().length() != 0) {
            pararesult = para.getText().toString();
        } else {
            pararesult = "Nil";
        }


        if (tvterm.getText().toString() != null && tvterm.getText().toString().length() != 0) {
            termresult.add(tvterm.getText().toString());
        } else {
            termresult.add("Nil");
        }


        if (vaginal.isChecked()) {
            pregtype.add(vaginal.getText().toString());
        } else if (cesarean.isChecked()) {
            pregtype.add(cesarean.getText().toString());
        } else {
            pregtype.add("Nil");
        }


        if (normal.isChecked()) {
            pregnormal.add(normal.getText().toString());
        } else if (forceps.isChecked()) {
            pregnormal.add(forceps.getText().toString());
        } else if (vaccum.isChecked()) {
            pregnormal.add(vaccum.getText().toString());
        } else {
            pregnormal.add("Nil");
        }


        if (helthy.getText().toString() != null && helthy.getText().toString().length() != 0) {
            preghealthy.add(helthy.getText().toString());
        } else {
            preghealthy.add("Nil");
        }

        if (male.isChecked()) {
            Log.d("GENDerCheck", male.getText().toString());
            preggender.add(male.getText().toString());
        } else if (female.isChecked()) {
            Log.d("GENDerCheck", female.getText().toString());
            preggender.add(female.getText().toString());
        } else {
            Log.d("GENDerCheck", "Nil");
            preggender.add("Nil");
        }


        if (dateldd.getText().toString() != null && dateldd.getText().toString().length() != 0) {
            ldddata = dateldd.getText().toString();
        } else {
            ldddata = "Nil";
        }


        if (dateedd.getText().toString() != null && dateedd.getText().toString().length() != 0) {
            edddata = dateedd.getText().toString();
        } else {
            edddata = "Nil";
        }


        if (datedog.getText().toString() != null && datedog.getText().toString().length() != 0) {
            dogdata = datedog.getText().toString();
        } else {
            dogdata = "Nil";
        }

        if (tvtermList.size() > 0) {

            System.out.println("tvtermlist size" + tvtermList.size());

            for (int i = 0; i < tvtermList.size(); i++) {
                if (tvtermList.get(i).getText().toString() != null && tvtermList.get(i).getText().toString().length() != 0)
                    termresult.add(tvtermList.get(i).getText().toString());
                else
                    termresult.add("Nil");
            }
        }

        if (abortionList.size() > 0) {

            for (int i = 0; i < abortionList.size(); i++) {
                if (abortionList.get(i).toString() != null && abortionList.get(i).toString().length() != 0)
                    abortionresult.add(abortionList.get(i).getText().toString());
                else
                    abortionresult.add("Nil");
            }
        }


        if (vaginalList.size() > 0) {

            for (int i = 0; i < vaginalList.size(); i++) {
                if (vaginalList.get(i).isChecked())
                    pregtype.add(vaginalList.get(i).getText().toString());
                else if (cesareanList.get(i).isChecked())
                    pregtype.add(cesareanList.get(i).getText().toString());
                else
                    pregtype.add("Nil");
            }
        }


        if (normalList.size() > 0) {
            for (int i = 0; i < normalList.size(); i++) {
                if (normalList.get(i).isChecked())
                    pregnormal.add(normalList.get(i).getText().toString());
                else if (forcepsList.get(i).isChecked())
                    pregnormal.add(forcepsList.get(i).getText().toString());
                else if (vaccumList.get(i).isChecked())
                    pregnormal.add(vaccumList.get(i).getText().toString());
                else
                    pregnormal.add("Nil");
            }
        }


        if (helthyList.size() > 0) {
            for (int i = 0; i < helthyList.size(); i++) {
                if (helthyList.get(i).toString() != null && helthyList.get(i).toString().length() != 0)
                    preghealthy.add(helthyList.get(i).getText().toString());
                else
                    preghealthy.add("Nil");
            }
        }

        if (maleList.size() > 0) {
            for (int i = 0; i < maleList.size(); i++) {
                if (maleList.get(i).isChecked()) {
                    Log.d("GENDerCheck", maleList.get(i).getText().toString() + " dg");
                    preggender.add(maleList.get(i).getText().toString());
                } else if (femaleList.get(i).isChecked()) {
                    Log.d("GENDerCheck", femaleList.get(i).getText().toString() + " dg");
                    preggender.add(femaleList.get(i).getText().toString());
                } else {
                    Log.d("GENDerCheck", "Nil" + " dg");
                    preggender.add("Nil");
                }
            }
        }

        if (yearList.size() > 0) {
            for (int i = 0; i < yearList.size(); i++) {
                if (yearList.get(i).getText().toString().length() == 0) {

                    yearvalue.add("Nil");

                } else {

                    yearvalue.add(yearList.get(i).getText().toString());
                }

            }
        }

        if (weekList.size() > 0) {
            Log.d("WeekValue", weekList.size() + " weekList.size()");
            for (int i = 0; i < weekList.size(); i++) {
                if (weekList.get(i).getText().toString().length() == 0) {
                    weekvalue.add("Nil");
                } else {
                    weekvalue.add(weekList.get(i).getText().toString());
                }


            }
        }

        marriedlifedata();
        bloodGroupdata();
        gravidadata();
        paradata();
        livingdata();
        termdata();
        abortiontypedata();
        pregnencytypedata();
        pregnencynormaldata();
        pregnencyhelthydata();
        pregnencygenderdata();
        pregnencyyeardata();
        pregnencyweekdata();
        pregnencyldddata();
        pregnencyedddata();
        pregnencydogdata();


        SharedPreferences scanpidnew = getActivity().getSharedPreferences("NewPatID", 0);
        String scnewPid = scanpidnew.getString("NewID", null);
        SharedPreferences scanpid1 = getActivity().getSharedPreferences("ScannedPid", 0);
        String scPid = scanpid1.getString("scanpid", null);

        if (patientobject == null) {
            if (scPid != null || scnewPid != null) {

                if (scPid != null) {
                    patientId = scPid;
                }
                if (scnewPid != null) {
                    patientId = scnewPid;
                }

            }
        } else if (patientobject.getString("patientID") == null) {
            patientId = scnewPid;
        } else {
            patientId = patientobject.getString("patientID");
        }

        if (diagnosisObject == null) {
            diagnosisObject = new ParseObject("Diagnosis");

        }

        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setMessage("Storing Patient Data....");
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();


        diagnosisObject.put("patientID", patientId);

//        if (marriedlife_result != null) {
        diagnosisObject.put("Married_Life", marriedLife.getText().toString());
//        } else {
//            diagnosisObject.put("Married_Life", marriedlife_result_array);
//        }


//        if (husbandd_bloodresult != null) {
        diagnosisObject.put("Husband_Blood_Group", bloodGroup.getText().toString());
//        } else {
//            diagnosisObject.put("Husband_Blood_Group", husbandd_bloodresult_array);
//        }


//        if (gravida_result != null) {
        diagnosisObject.put("Gravida", gravida.getText().toString());
//        } else {
//            diagnosisObject.put("Gravida", gravida_result_array);
//        }


//        if (para_result != null) {
        diagnosisObject.put("Para", para.getText().toString());
//        } else {
//            diagnosisObject.put("Para", para_result_array);
//        }


//        if (living_result != null) {
        diagnosisObject.put("Living", living.getText().toString());
        diagnosisObject.put("Abortion", edtAbortion.getText().toString());
//        } else {
//            diagnosisObject.put("Living", living_result_array);
//        }


        if (term_result != null) {

            System.out.println("term_result not null");
            diagnosisObject.put("Pregnancy_Term", term_result);
        } else {

            System.out.println("term_result  null");
            diagnosisObject.put("Pregnancy_Term", term_result_array);
        }


        if (typeabortion_result != null) {
            diagnosisObject.put("Pregnency_Abortion", typeabortion_result);
        } else {
            diagnosisObject.put("Pregnency_Abortion", typeabortion_result_array);
        }


        if (typepregnency_result != null) {
            diagnosisObject.put("Pregnency_Type", typepregnency_result);
        } else {
            diagnosisObject.put("Pregnency_Type", typepregnency_result_array);
        }


        if (pregnencynormal_result != null) {
            diagnosisObject.put("pregnency_normal", pregnencynormal_result);
        } else {
            diagnosisObject.put("pregnency_normal", pregnencynormal_result_array);
        }


        if (pregnencylive_result != null) {
            diagnosisObject.put("Pregnency_live", pregnencylive_result);
        } else {
            diagnosisObject.put("Pregnency_live", pregnencylive_result_array);
        }


        if (pregnencygender_result != null) {
            diagnosisObject.put("Pregnency_Gender", pregnencygender_result);
        } else {
            diagnosisObject.put("Pregnency_Gender", pregnencygender_result_array);
        }


        if (pregnencyyear_resullt != null) {
            diagnosisObject.put("Pregnency_Year", pregnencyyear_resullt);
        } else {
            diagnosisObject.put("Pregnency_Year", pregnencyyear_resullt_array);
        }


        if (pregnency_week != null) {
            diagnosisObject.put("Pregnency_Week", pregnency_week);
        } else {
            diagnosisObject.put("Pregnency_Week", pregnency_week_resullt_array);
        }


//        if (pregnencyldd_result != null) {
        diagnosisObject.put("Pregnency_LDD", dateldd.getText().toString());
//        } else {
//            diagnosisObject.put("Pregnency_LDD", pregnencyldd_resullt_array);
//        }


//        if (pregnencyedd_result != null) {
        diagnosisObject.put("Pregnency_EDD", dateedd.getText().toString());
//        } else {
//            diagnosisObject.put("Pregnency_EDD", pregnencyedd_result_array);
//        }


//        if (pregnencydog_result != null) {
        diagnosisObject.put("Pregnency_DOG", datedog.getText().toString());
//        } else {
//            diagnosisObject.put("Pregnency_DOG", pregnencydog_result_array);
//        }

        ///////////////////////////for LMP
        diagnosisObject.put("Lmp_date", lmpdate_array);
        diagnosisObject.put("Lmp_flow", lmpflow_array);
        diagnosisObject.put("Lmp_dysmenorrhea", lmpdysmenorrhea_array);
        diagnosisObject.put("Menstrual_Regular_Irregular", menstrualregularlist);
        diagnosisObject.put("Menstrual_Days", menstrualdayslist);
        diagnosisObject.put("Menstrual_Cycle", menstrualcyclelist);

        diagnosisObject.put("typeFlag", 1);
        Calendar c = Calendar.getInstance();
        System.out.println("Current time => " + c.getTime());
        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
        String formattedDate = df.format(c.getTime());
        diagnosisObject.put("createdDate", formattedDate);

        diagnosisObject.pinInBackground(new SaveCallback() {

            @Override
            public void done(ParseException e) {
                // TODO Auto-generated method stub

                mProgressDialog.dismiss();
                if (e == null) {
                    submitButtonDeactivation();

                    System.out.println("target fragment" + targetfragment);

                    if (targetfragment == 2) {
                        FragmentManager fragmentManager = getFragmentManager();
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

                        if (((FragmentActivityContainer) getActivity()).getmedicalhistory() == null) {
                            patientmedicalhistoryfragObj = new PatientCurrentMedicalaInformation_Gynacologist();
                            Bundle bundle = new Bundle();
                            bundle.putBoolean("patientflag", patientflag);
                            patientmedicalhistoryfragObj.setArguments(bundle);

                        } else {
                            patientmedicalhistoryfragObj = ((FragmentActivityContainer) getActivity()).getPatientcurrentmedinfo_gynacologist();
                        }

                        ((FragmentActivityContainer) getActivity()).setPatientcurrentmedinfo_gynacologist(patientmedicalhistoryfragObj);
                        fragmentTransaction.replace(R.id.fl_fragment_container, patientmedicalhistoryfragObj);
                        fragmentTransaction.commit();

                    } else if (targetfragment == 6) {

                        FragmentManager fragmentManager = getFragmentManager();
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        Diagnosisexamination_Gynacologist diagnosis = new Diagnosisexamination_Gynacologist();
                        fragmentTransaction.replace(R.id.fl_fragment_container, diagnosis);
                        fragmentTransaction.commit();
                    } else if (targetfragment == 7) {

                        FragmentManager fragmentManager = getFragmentManager();
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        Prescriptionmanagement_Gynacologist diagnosis = new Prescriptionmanagement_Gynacologist();
                        fragmentTransaction.replace(R.id.fl_fragment_container, diagnosis);
                        fragmentTransaction.commit();
                    }


                    Toast.makeText(getActivity(), "Successfully Saved", Toast.LENGTH_LONG).show();
                }

                /////////////////////////attach diagnosis data with main container.
                ((FragmentActivityContainer) getActivity()).setpatientdiagnosisparseObj(diagnosisObject);
                //////////////////////////////////////////////////////////////////
            }
        });

        diagnosisObject.saveEventually();
    }

    @Override
    public void onResume() {
        // TODO Auto-generated method stub
        super.onResume();

        pregposition = 0;

        if (patientedit_checkflag == false) {

            tvterm.setEnabled(false);
            abortion.setEnabled(false);

            vaginal.setEnabled(false);
            cesarean.setEnabled(false);
            normal.setEnabled(false);
            forceps.setEnabled(false);
            vaccum.setEnabled(false);

            helthy.setEnabled(false);
            male.setEnabled(false);
            female.setEnabled(false);
            year.setEnabled(false);
            week.setEnabled(false);

        }

        if (diagnosisObject != null) {

            marriedlife_result = diagnosisObject.getString("Married_Life");
            husbandd_bloodresult = diagnosisObject.getString("Husband_Blood_Group");
            gravida_result = diagnosisObject.getString("Gravida");
            para_result = diagnosisObject.getString("Para");
            living_result = diagnosisObject.getString("Living");
            term_result = diagnosisObject.getJSONArray("Pregnancy_Term");
            typeabortion_result = diagnosisObject.getJSONArray("Pregnency_Abortion");
            typepregnency_result = diagnosisObject.getJSONArray("Pregnency_Type");
            pregnencynormal_result = diagnosisObject.getJSONArray("pregnency_normal");
            pregnencylive_result = diagnosisObject.getJSONArray("Pregnency_live");
            pregnencygender_result = diagnosisObject.getJSONArray("Pregnency_Gender");
            pregnencyyear_resullt = diagnosisObject.getJSONArray("Pregnency_Year");
            pregnency_week = diagnosisObject.getJSONArray("Pregnency_Week");
            pregnencyldd_result = diagnosisObject.getString("Pregnency_LDD");
            pregnencyedd_result = diagnosisObject.getString("Pregnency_EDD");
            pregnencydog_result = diagnosisObject.getString("Pregnency_DOG");


            if (marriedlife_result != null) {
                String result = "";
                try {
//                    result = marriedlife_result.getString(marriedlife_result.length() - 1);
                    result = marriedlife_result;
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (result.toString() != null && !(result.toString().equals("Nil"))) {
                    marriedLife.setText(result);
                }
            }


            if (husbandd_bloodresult != null) {
                String result = "";
                try {
//                    result = husbandd_bloodresult.getString(husbandd_bloodresult.length() - 1);
                    result = husbandd_bloodresult;
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (result.toString() != null && !(result.toString().equals("Nil"))) {
                    bloodGroup.setText(result.toString());
                }
            }

            if (gravida_result != null) {
                String result = "";
                try {
//                    result = gravida_result.getString(gravida_result.length() - 1);
                    result = gravida_result;
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (result.toString() != null && !(result.toString().equals("Nil"))) {
                    gravida.setText(result);
                }
            }


            if (para_result != null) {
                String result = "";
                try {
//                    result = para_result.getString(para_result.length() - 1);
                    result = para_result;
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (result.toString() != null && !(result.toString().equals("Nil"))) {
                    para.setText(result.toString());
                }
            }


            if (living_result != null) {
                String result = "";
                try {
//                    result = living_result.getString(living_result.length() - 1);
                    result = living_result;
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (result.toString() != null && !(result.toString().equals("Nil"))) {
                    living.setText(result);
                }
                if (diagnosisObject.getString("Abortion") != null)
                    edtAbortion.setText(diagnosisObject.getString("Abortion"));
            }

            ArrayList<String> termresultArraylist = new ArrayList<String>();
            ArrayList<String> typeabortionArraylist = new ArrayList<String>();
            ArrayList<String> typepregnencyArraylist = new ArrayList<String>();
            ArrayList<String> pregnencynormalArraylist = new ArrayList<String>();
            ArrayList<String> pregnencyliveArraylist = new ArrayList<String>();
            ArrayList<String> pregnencygenderArraylist = new ArrayList<String>();
            ArrayList<String> pregnencyyearArraylist = new ArrayList<String>();
            ArrayList<String> pregnencyweekArraylist = new ArrayList<String>();

            if (term_result != null) {
                String result = "";
                try {
                    result = term_result.getString(term_result.length() - 1);
                    String[] termAnsArray = result.split(",");
                    for (int i = 0; i < termAnsArray.length; i++) {
                        termresultArraylist.add(termAnsArray[i]);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (!(termresultArraylist.get(0).equals("Nil"))) {
                    tvterm.setText(termresultArraylist.get(0));
                }
            }


            if (typeabortion_result != null) {
                String result = "";
                try {
                    result = typeabortion_result.getString(typeabortion_result.length() - 1);
                    String[] termAnsArray = result.split(",");
                    for (int i = 0; i < termAnsArray.length; i++) {
                        typeabortionArraylist.add(termAnsArray[i]);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (!(typeabortionArraylist.get(0).equals("Nil"))) {
                    abortion.setText(typeabortionArraylist.get(0));
                }
            }


            if (typepregnency_result != null) {
                String result = "";
                try {
                    result = typepregnency_result.getString(typepregnency_result.length() - 1);
                    String[] termAnsArray = result.split(",");
                    for (int i = 0; i < termAnsArray.length; i++) {
                        typepregnencyArraylist.add(termAnsArray[i]);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (!(typepregnencyArraylist.get(0).equals("Nil"))) {

                    if (typepregnencyArraylist.get(0).equalsIgnoreCase("vaginal"))
                        vaginal.setChecked(true);
                    else if (typepregnencyArraylist.get(0).equalsIgnoreCase("cesarean"))
                        cesarean.setChecked(true);
                }
            }


            if (pregnencynormal_result != null) {
                String result = "";
                try {
                    result = pregnencynormal_result.getString(pregnencynormal_result.length() - 1);
                    String[] termAnsArray = result.split(",");
                    for (int i = 0; i < termAnsArray.length; i++) {
                        pregnencynormalArraylist.add(termAnsArray[i]);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (!(pregnencynormalArraylist.get(0).equals("Nil"))) {

                    if (pregnencynormalArraylist.get(0).equalsIgnoreCase("normal"))
                        normal.setChecked(true);
                    else if (pregnencynormalArraylist.get(0).equalsIgnoreCase("forceps"))
                        forceps.setChecked(true);
                    else if (pregnencynormalArraylist.get(0).equalsIgnoreCase("vaccum"))
                        vaccum.setChecked(true);
                }
            }


            if (pregnencylive_result != null) {
                String result = "";
                try {
                    result = pregnencylive_result.getString(pregnencylive_result.length() - 1);
                    String[] termAnsArray = result.split(",");
                    for (int i = 0; i < termAnsArray.length; i++) {
                        pregnencyliveArraylist.add(termAnsArray[i]);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (!(pregnencyliveArraylist.get(0).equals("Nil"))) {
                    helthy.setText(pregnencyliveArraylist.get(0));
                }
            }


            if (pregnencygender_result != null) {
                String result = "";
                try {
                    result = pregnencygender_result.getString(pregnencygender_result.length() - 1);
                    String[] termAnsArray = result.split(",");
                    for (int i = 0; i < termAnsArray.length; i++) {
                        pregnencygenderArraylist.add(termAnsArray[i]);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
//                if (!(pregnencygenderArraylist.get(0).toString().equals("Nil"))) {
                if (pregnencygenderArraylist.get(0).equalsIgnoreCase("male"))
                    male.setChecked(true);
                else if (pregnencygenderArraylist.get(0).equalsIgnoreCase("female"))
                    female.setChecked(true);
//                }
            }


            if (pregnencyyear_resullt != null) {
                String result = "";
                try {
                    result = pregnencyyear_resullt.getString(pregnencyyear_resullt.length() - 1);
                    String[] termAnsArray = result.split(",");
                    for (int i = 0; i < termAnsArray.length; i++) {
                        pregnencyyearArraylist.add(termAnsArray[i]);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (!(pregnencyyearArraylist.get(0).equals("Nil"))) {

                    year.setText(pregnencyyearArraylist.get(0));
                }
            }


            if (pregnency_week != null) {
                String result = "";
                try {
                    result = pregnency_week.getString(pregnency_week.length() - 1);
                    String[] termAnsArray = result.split(",");
                    for (int i = 0; i < termAnsArray.length; i++) {
                        pregnencyweekArraylist.add(termAnsArray[i].toString());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (!(pregnencyweekArraylist.get(0).contains("Nil"))) {

                    week.setText(pregnencyweekArraylist.get(0).replace(",", ""));
                }
            }


            if (termresultArraylist.size() > 1) {

                for (int j = 1; j < termresultArraylist.size(); j++) {

                    LayoutInflater inflater = LayoutInflater.from(getActivity());
                    final View add_preg = inflater.inflate(R.layout.addpregnancy_rowitem, null);

                    final TextView tvterm = (TextView) add_preg.findViewById(R.id.tvterm);
                    final EditText abortion = (EditText) add_preg.findViewById(R.id.etabortion);
                    final CheckBox vaginal = (CheckBox) add_preg.findViewById(R.id.cbvaginal);
                    final CheckBox cesarean = (CheckBox) add_preg.findViewById(R.id.cbcesarean);
                    final CheckBox normal = (CheckBox) add_preg.findViewById(R.id.cbnormal);
                    final CheckBox forceps = (CheckBox) add_preg.findViewById(R.id.cbforceps);
                    final CheckBox vaccum = (CheckBox) add_preg.findViewById(R.id.cbvaccum);

                    final TextView helthy = (TextView) add_preg.findViewById(R.id.tvhelthy);
                    final CheckBox male = (CheckBox) add_preg.findViewById(R.id.cbmale);
                    final CheckBox female = (CheckBox) add_preg.findViewById(R.id.cbfemale);
                    final EditText year = (EditText) add_preg.findViewById(R.id.years);
                    final EditText week = (EditText) add_preg.findViewById(R.id.weeks);

                    final RelativeLayout remove = (RelativeLayout) add_preg.findViewById(R.id.rlImagecross);


//                    week.setMaxValue(100);
//                    week.setMinValue(0);
//
//                    year.setMaxValue(100);
//                    year.setMinValue(0);


                    if (patientedit_checkflag == false) {

                        tvterm.setEnabled(false);
                        abortion.setEnabled(false);

                        vaginal.setEnabled(false);
                        cesarean.setEnabled(false);
                        normal.setEnabled(false);
                        forceps.setEnabled(false);
                        vaccum.setEnabled(false);

                        helthy.setEnabled(false);
                        male.setEnabled(false);
                        female.setEnabled(false);
                        year.setEnabled(false);
                        week.setEnabled(false);
                        remove.setEnabled(false);
                    }


                    if (!termresultArraylist.get(j).equalsIgnoreCase("Nil"))
                        tvterm.setText(termresultArraylist.get(j).toString());
                    try {
                        if (!typeabortionArraylist.get(j).equalsIgnoreCase("Nil"))
                            abortion.setText(typeabortionArraylist.get(j).toString());
                    } catch (IndexOutOfBoundsException ex) {
                        ex.printStackTrace();
                    }

                    if (!(typepregnencyArraylist.get(j).equals("Nil"))) {

                        if (typepregnencyArraylist.get(j).equalsIgnoreCase("vaginal"))
                            vaginal.setChecked(true);
                        else if (typepregnencyArraylist.get(j).equalsIgnoreCase("cesarean"))
                            cesarean.setChecked(true);
                    }

                    if (!(pregnencynormalArraylist.get(j).equals("Nil"))) {
                        if (pregnencynormalArraylist.get(j).equalsIgnoreCase("normal"))
                            normal.setChecked(true);
                        else if (pregnencynormalArraylist.get(j).equalsIgnoreCase("forceps"))
                            forceps.setChecked(true);
                        else if (pregnencynormalArraylist.get(j).equalsIgnoreCase("vaccum"))
                            vaccum.setChecked(true);
                    }
                    try {
                        if (!(pregnencyliveArraylist.get(j).toString().equals("Nil"))) {

                            helthy.setText(pregnencyliveArraylist.get(j));
                        }
                    } catch (IndexOutOfBoundsException e) {
                        e.printStackTrace();
                    }

                    if (!(pregnencygenderArraylist.get(j).toString().equals("Nil"))) {
                        if (pregnencygenderArraylist.get(j).equalsIgnoreCase("Male"))
                            male.setChecked(true);
                        else if (pregnencygenderArraylist.get(j).equalsIgnoreCase("Female"))
                            female.setChecked(true);
                    }
                    try {
                        if (!(pregnencyyearArraylist.get(j).equals("Nil"))) {
                            year.setText(pregnencyyearArraylist.get(j));
                        }

                        if (!(pregnencyweekArraylist.get(j).contains("Nil"))) {
                            week.setText(pregnencyweekArraylist.get(j).replace(",", ""));
                        }

                    } catch (IndexOutOfBoundsException e) {
                        e.printStackTrace();
                    }
                    tvterm.setTag(pregposition);
                    abortion.setTag(pregposition);
                    vaginal.setTag(pregposition);
                    cesarean.setTag(pregposition);
                    normal.setTag(pregposition);
                    forceps.setTag(pregposition);
                    vaccum.setTag(pregposition);

                    helthy.setTag(pregposition);
                    male.setTag(pregposition);
                    female.setTag(pregposition);
                    year.setTag(pregposition);
                    week.setTag(pregposition);
                    remove.setTag(pregposition);


                    removeArraylist.add(pregposition, remove);
                    tvtermList.add(pregposition, tvterm);
                    abortionList.add(pregposition, abortion);
                    vaginalList.add(pregposition, vaginal);
                    cesareanList.add(pregposition, cesarean);
                    normalList.add(pregposition, normal);
                    forcepsList.add(pregposition, forceps);
                    vaccumList.add(pregposition, vaccum);
                    helthyList.add(pregposition, helthy);
                    maleList.add(pregposition, male);
                    femaleList.add(pregposition, female);
                    yearList.add(pregposition, year);
                    weekList.add(pregposition, week);

                    pregposition++;


                    abortionList.get((Integer) (year.getTag())).addTextChangedListener(new TextWatcher() {

                        @Override
                        public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
                            // TODO Auto-generated method stub

                        }

                        @Override
                        public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                                      int arg3) {
                            // TODO Auto-generated method stub

                        }

                        @Override
                        public void afterTextChanged(Editable arg0) {
                            // TODO Auto-generated method stub
                            submitButtonActivation();
                        }
                    });


                    yearList.get((Integer) (year.getTag())).addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                        }

                        @Override
                        public void onTextChanged(CharSequence s, int start, int before, int count) {

                            submitButtonActivation();
                        }

                        @Override
                        public void afterTextChanged(Editable s) {

                        }
                    });

                    weekList.get((Integer) (year.getTag())).addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                        }

                        @Override
                        public void onTextChanged(CharSequence s, int start, int before, int count) {

                            submitButtonActivation();
                        }

                        @Override
                        public void afterTextChanged(Editable s) {

                        }
                    });


                    normalList.get((Integer) (normal.getTag())).setOnClickListener(new OnClickListener() {

                        @Override
                        public void onClick(View arg0) {
                            // TODO Auto-generated method stub

                            if (normalList.get((Integer) (normal.getTag())).isChecked()) {
                                forcepsList.get((Integer) (forceps.getTag())).setChecked(false);
                                vaccumList.get((Integer) (vaccum.getTag())).setChecked(false);
                                submitButtonActivation();
                            }


                        }
                    });


                    forcepsList.get((Integer) (forceps.getTag())).setOnClickListener(new OnClickListener() {

                        @Override
                        public void onClick(View arg0) {
                            // TODO Auto-generated method stub

                            if (forcepsList.get((Integer) (forceps.getTag())).isChecked()) {
                                normalList.get((Integer) (normal.getTag())).setChecked(false);
                                vaccumList.get((Integer) (vaccum.getTag())).setChecked(false);
                                submitButtonActivation();
                            }


                        }
                    });


                    vaccumList.get((Integer) (forceps.getTag())).setOnClickListener(new OnClickListener() {

                        @Override
                        public void onClick(View arg0) {
                            // TODO Auto-generated method stub

                            if (vaccumList.get((Integer) (vaccum.getTag())).isChecked()) {
                                normalList.get((Integer) (normal.getTag())).setChecked(false);
                                forcepsList.get((Integer) (forceps.getTag())).setChecked(false);
                                submitButtonActivation();
                            }


                        }
                    });


                    maleList.get((Integer) (male.getTag())).setOnClickListener(new OnClickListener() {

                        @Override
                        public void onClick(View arg0) {
                            // TODO Auto-generated method stub

                            if (maleList.get((Integer) (male.getTag())).isChecked()) {
                                femaleList.get((Integer) (female.getTag())).setChecked(false);
                                submitButtonActivation();
                            }


                        }
                    });


                    femaleList.get((Integer) (female.getTag())).setOnClickListener(new OnClickListener() {

                        @Override
                        public void onClick(View arg0) {
                            // TODO Auto-generated method stub

                            if (femaleList.get((Integer) (female.getTag())).isChecked()) {
                                maleList.get((Integer) (male.getTag())).setChecked(false);
                                submitButtonActivation();
                            }


                        }
                    });

                    vaginalList.get((Integer) (vaginal.getTag())).setOnClickListener(new OnClickListener() {

                        @Override
                        public void onClick(View arg0) {
                            // TODO Auto-generated method stub

                            if (vaginalList.get((Integer) (vaginal.getTag())).isChecked()) {
                                cesareanList.get((Integer) (vaginal.getTag())).setChecked(false);
                                submitButtonActivation();
                            }


                        }
                    });


                    cesareanList.get((Integer) (vaginal.getTag())).setOnClickListener(new OnClickListener() {

                        @Override
                        public void onClick(View arg0) {
                            // TODO Auto-generated method stub

                            if (cesareanList.get((Integer) (vaginal.getTag())).isChecked()) {
                                vaginalList.get((Integer) (vaginal.getTag())).setChecked(false);
                                submitButtonActivation();
                            }


                        }
                    });


                    removeArraylist.get((Integer) (remove.getTag())).setOnClickListener(new OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            // TODO Auto-generated method stub

                            View removeView = (LinearLayout) add_preg.findViewById(R.id.pregcontainer);
                            ViewGroup parent = (ViewGroup) removeView.getParent();
                            parent.removeView(removeView);

                            System.out.println("tag value....." + (Integer) (remove.getTag()));

                            tvtermList.set((Integer) (remove.getTag()), null);
                            abortionList.set((Integer) (remove.getTag()), null);
                            vaginalList.set((Integer) (remove.getTag()), null);
                            cesareanList.set((Integer) (remove.getTag()), null);
                            normalList.set((Integer) (remove.getTag()), null);
                            forcepsList.set((Integer) (remove.getTag()), null);
                            vaccumList.set((Integer) (remove.getTag()), null);
                            helthyList.set((Integer) (remove.getTag()), null);
                            maleList.set((Integer) (remove.getTag()), null);
                            femaleList.set((Integer) (remove.getTag()), null);
                            yearList.set((Integer) (remove.getTag()), null);
                            weekList.set((Integer) (remove.getTag()), null);


                            removeArraylist.set((Integer) (remove.getTag()), null);

                            submitButtonActivation();


                        }
                    });


                    tvtermList.get((Integer) (tvterm.getTag())).setOnClickListener(new OnClickListener() {


                        @Override
                        public void onClick(View arg0) {
                            // TODO Auto-generated method stub

                            termviewposition = (Integer) (tvterm.getTag());
                            flag = 1;
                            Bundle bundle = new Bundle();
                            bundle.putString("name", "Term");
                            bundle.putStringArray("items", term_array);
                            spObj = new SpinnerPopup();
                            spObj.setSubmitListener(spinnerpopupListener);
                            spObj.setArguments(bundle);
                            spObj.show(getFragmentManager(), "tag");


                        }
                    });


                    helthyList.get((Integer) (tvterm.getTag())).setOnClickListener(new OnClickListener() {


                        @Override
                        public void onClick(View arg0) {
                            // TODO Auto-generated method stub

                            healthviewposition = (Integer) (tvterm.getTag());
                            flag = 2;
                            Bundle bundle = new Bundle();
                            bundle.putString("name", "health");
                            bundle.putStringArray("items", health_array);
                            spObj = new SpinnerPopup();
                            spObj.setSubmitListener(spinnerpopupListener);
                            spObj.setArguments(bundle);
                            spObj.show(getFragmentManager(), "tag");
                        }
                    });
                    preg_container.addView(add_preg);

                }

            }

            if (pregnencyldd_result != null) {
                String result = "";
                try {
//                    result = pregnencyldd_result.getString(pregnencyldd_result.length() - 1);
                    result = pregnencyldd_result;
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (result.toString() != null && !(result.toString().equals("Nil"))) {

                    dateldd.setText(result);
                }
            }

            if (pregnencyedd_result != null) {
                String result = "";
                try {
//                    result = pregnencyedd_result.getString(pregnencyedd_result.length() - 1);
                    result = pregnencyedd_result;
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (result.toString() != null && !(result.toString().equals("Nil"))) {

                    dateedd.setText(result);
                }
            }

            if (pregnencydog_result != null) {
                String result = "";
                try {
//                    result = pregnencydog_result.getString(pregnencydog_result.length() - 1);
                    result = pregnencydog_result;
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (result.toString() != null && !(result.toString().equals("Nil"))) {

                    datedog.setText(result);
                }
            }

            ///////////////////////////for LMP
            List[] arrayArryayList = {lmpdate_array, lmpflow_array, lmpdysmenorrhea_array, menstrualregularlist, menstrualdayslist, menstrualcyclelist};
            String[] fildNames = {"Lmp_date", "Lmp_flow", "Lmp_dysmenorrhea", "Menstrual_Regular_Irregular", "Menstrual_Days", "Menstrual_Cycle"};
            fillLMPArrays(diagnosisObject, arrayArryayList, fildNames);

        }

    }

    public void fillLMPArrays(ParseObject patientobject, List[] array, String[] fieldName) {
        try {
            for (int k = 0; k < array.length; k++) {
                listCount.clear();
                JSONArray jsonArray = patientobject.getJSONArray(fieldName[k]);
                for (int i = 0; i < jsonArray.length(); i++) {
                    ArrayList<String> arrayList = (ArrayList<String>) array[k];
                    arrayList.add(jsonArray.get(i).toString());
                    listCount.add(0);

                }

            }
            ///////////////////////////set adapter
            if (listCount.size() == 0)
                listCount.add(0);
            lmpAdapter = new LMPAdapterDiagnosis(getActivity(), listCount, this, null);
            lstMenstrualPeriod.setAdapter(lmpAdapter);
            AppUtil.justifyListViewHeightBasedOnChildren(lstMenstrualPeriod);
            //////////////////////////////////////////////////////////
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            e.printStackTrace();
            listCount.clear();
            listCount.add(0);
        }
    }

	/*if(medicationlist.size()>0){
        for(int j=0;j<(medicationlist.size());j++){
			Object item=medicationlist.get(j);
			Object reactionitem=reactionlist.get(j);

			if(item!=null){
				if(medicationlist.get(j).getText().length()!=0){
					allmedicArray.add(medicationlist.get(j).getText().toString());
				}else{
					allmedicArray.add("Nil");
				}
				if(reactionlist.get(j).getText().length()!=0 ){
					allreacArray.add(reactionlist.get(j).getText().toString());
				}else{
					allreacArray.add("Nil");
				}
			}

			System.out.println("medicationlist details"+medicationlist.get(j).getText().toString());
			System.out.println("medication list size"+medicationlist.size());
		}


	}*/

    public void marriedlifedata() {
        String result = "";
        result = marriedLife.getText().toString();
        if (marriedlife_result != null) {
            if (patientflag == false) {
//                try {
//                    marriedlife_result.put(marriedlife_result.length() - 1, result);
                marriedlife_result = marriedlife_result;
//                } catch (JSONException e) {
//                    // TODO Auto-generated catch block
//                    e.printStackTrace();
//                }
            } else if (patientflag == true && ((FragmentActivityContainer) getActivity()).getoldpatientupdateflag() == false) {
//                marriedlife_result.put(result);
                marriedlife_result = result;
            } else if (patientflag == true && ((FragmentActivityContainer) getActivity()).getoldpatientupdateflag() == true) {
//                try {
//                    marriedlife_result.put(marriedlife_result.length() - 1, result);
                marriedlife_result = marriedlife_result;
//                } catch (JSONException e) {
//                    // TODO Auto-generated catch block
//                    e.printStackTrace();
//                }

            }
        } else {
//            marriedlife_result_array = new JSONArray();
//            marriedlife_result_array.put(result);
            marriedlife_result_array = result;
        }
    }


    public void bloodGroupdata() {
        String result = "";
        result = husbandblood;
        if (husbandd_bloodresult != null) {
            if (patientflag == false) {
//                try {
//                    husbandd_bloodresult.put(husbandd_bloodresult.length() - 1, result);
                husbandd_bloodresult = husbandd_bloodresult;
//                } catch (JSONException e) {
//                    // TODO Auto-generated catch block
//                    e.printStackTrace();
//                }
            } else if (patientflag == true && ((FragmentActivityContainer) getActivity()).getoldpatientupdateflag() == false) {
//                husbandd_bloodresult.put(result);
                husbandd_bloodresult = result;
            } else if (patientflag == true && ((FragmentActivityContainer) getActivity()).getoldpatientupdateflag() == true) {
//                try {
//                    husbandd_bloodresult.put(husbandd_bloodresult.length() - 1, result);
                husbandd_bloodresult = husbandd_bloodresult;
//                } catch (JSONException e) {
//                    // TODO Auto-generated catch block
//                    e.printStackTrace();
//                }

            }
        } else {
//            husbandd_bloodresult_array = new JSONArray();
//            husbandd_bloodresult_array.put(result);
            husbandd_bloodresult_array = result;
        }
    }


    public void gravidadata() {
        String result = "";
        result = gravida.getText().toString();
        if (gravida_result != null) {
            if (patientflag == false) {
//                try {
//                    gravida_result.put(gravida_result.length() - 1, result);
                gravida_result = gravida_result;
//                } catch (JSONException e) {
//                    // TODO Auto-generated catch block
//                    e.printStackTrace();
//                }
            } else if (patientflag == true && ((FragmentActivityContainer) getActivity()).getoldpatientupdateflag() == false) {
//                gravida_result.put(result);
                gravida_result = result;
            } else if (patientflag == true && ((FragmentActivityContainer) getActivity()).getoldpatientupdateflag() == true) {
//                try {
//                    gravida_result.put(gravida_result.length() - 1, result);
                gravida_result = gravida_result;
//                } catch (JSONException e) {
//                    // TODO Auto-generated catch block
//                    e.printStackTrace();
//                }

            }
        } else {
//            gravida_result_array = new JSONArray();
//            gravida_result_array.put(result);
            gravida_result_array = result;
        }
    }


    public void paradata() {
        String result = "";
        result = pararesult;
        if (para_result != null) {
            if (patientflag == false) {
//                try {
//                    para_result.put(para_result.length() - 1, result);
                para_result = para_result;
//                } catch (JSONException e) {
//                    // TODO Auto-generated catch block
//                    e.printStackTrace();
//                }
            } else if (patientflag == true && ((FragmentActivityContainer) getActivity()).getoldpatientupdateflag() == false) {
//                para_result.put(result);
                para_result = result;
            } else if (patientflag == true && ((FragmentActivityContainer) getActivity()).getoldpatientupdateflag() == true) {
//                try {
//                    para_result.put(para_result.length() - 1, result);
                para_result = para_result;
//                } catch (JSONException e) {
//                    // TODO Auto-generated catch block
//                    e.printStackTrace();
//                }

            }
        } else {
//            para_result_array = new JSONArray();
//            para_result_array.put(result);
            para_result_array = result;
        }
    }


    public void livingdata() {
        String result = "";
        result = living.getText().toString();
        if (living_result != null) {
            if (patientflag == false) {
//                try {
//                    living_result.put(living_result.length() - 1, result);
                living_result = living_result;
//                } catch (JSONException e) {
//                    // TODO Auto-generated catch block
//                    e.printStackTrace();
//                }
            } else if (patientflag == true && ((FragmentActivityContainer) getActivity()).getoldpatientupdateflag() == false) {
//                living_result.put(result);
                living_result = result;
            } else if (patientflag == true && ((FragmentActivityContainer) getActivity()).getoldpatientupdateflag() == true) {
//                try {
//                    living_result.put(living_result.length() - 1, result);
                living_result = living_result;
//                } catch (JSONException e) {
//                    // TODO Auto-generated catch block
//                    e.printStackTrace();
//                }

            }
        } else {
//            living_result_array = new JSONArray();
//            living_result_array.put(result);
            living_result_array = result;
        }
    }


    public void termdata() {
        String result = "";
        for (int i = 0; i < termresult.size(); i++) {

            System.out.println("term result working.." + termresult.get(i));
            result += termresult.get(i) + ",";
        }
        termresult.clear();

        if (term_result != null) {
            if (patientflag == false) {

                try {
                    term_result.put(term_result.length() - 1, result);
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            } else if (patientflag == true && ((FragmentActivityContainer) getActivity()).getoldpatientupdateflag() == false) {
                term_result.put(result);
            } else if (patientflag == true && ((FragmentActivityContainer) getActivity()).getoldpatientupdateflag() == true) {
                try {
                    term_result.put(term_result.length() - 1, result);
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            }
        } else {

            System.out.println("term_result term_getdata null");

            term_result_array = new JSONArray();
            term_result_array.put(result);
        }
    }


    public void abortiontypedata() {
        String result = "";
        for (int i = 0; i < abortionresult.size(); i++) {
            result += abortionresult.get(i) + ",";

        }
        abortionresult.clear();
        if (typeabortion_result != null) {
            if (patientflag == false) {
                try {
                    typeabortion_result.put(typeabortion_result.length() - 1, result);
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            } else if (patientflag == true && ((FragmentActivityContainer) getActivity()).getoldpatientupdateflag() == false) {
                typeabortion_result.put(result);
            } else if (patientflag == true && ((FragmentActivityContainer) getActivity()).getoldpatientupdateflag() == true) {
                try {
                    typeabortion_result.put(typeabortion_result.length() - 1, result);
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            }
        } else {
            typeabortion_result_array = new JSONArray();
            typeabortion_result_array.put(result);
        }
    }


    public void pregnencytypedata() {
        String result = "";
        for (int i = 0; i < pregtype.size(); i++) {
            result += pregtype.get(i) + ",";
        }
        pregtype.clear();
        if (typepregnency_result != null) {
            if (patientflag == false) {
                try {
                    typepregnency_result.put(typepregnency_result.length() - 1, result);
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            } else if (patientflag == true && ((FragmentActivityContainer) getActivity()).getoldpatientupdateflag() == false) {
                typepregnency_result.put(result);
            } else if (patientflag == true && ((FragmentActivityContainer) getActivity()).getoldpatientupdateflag() == true) {
                try {
                    typepregnency_result.put(typepregnency_result.length() - 1, result);
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            }
        } else {
            typepregnency_result_array = new JSONArray();
            typepregnency_result_array.put(result);
        }
    }


    public void pregnencynormaldata() {
        String result = "";
        for (int i = 0; i < pregnormal.size(); i++) {
            result += pregnormal.get(i) + ",";
        }
        pregnormal.clear();
        if (pregnencynormal_result != null) {
            if (patientflag == false) {
                try {
                    pregnencynormal_result.put(pregnencynormal_result.length() - 1, result);
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            } else if (patientflag == true && ((FragmentActivityContainer) getActivity()).getoldpatientupdateflag() == false) {
                pregnencynormal_result.put(result);
            } else if (patientflag == true && ((FragmentActivityContainer) getActivity()).getoldpatientupdateflag() == true) {
                try {
                    pregnencynormal_result.put(pregnencynormal_result.length() - 1, result);
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            }
        } else {
            pregnencynormal_result_array = new JSONArray();
            pregnencynormal_result_array.put(result);
        }
    }


    public void pregnencyhelthydata() {
        String result = "";
        for (int i = 0; i < preghealthy.size(); i++) {
            result += preghealthy.get(i) + ",";
        }
        preghealthy.clear();
        if (pregnencylive_result != null) {
            if (patientflag == false) {
                try {
                    pregnencylive_result.put(pregnencylive_result.length() - 1, result);
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            } else if (patientflag == true && ((FragmentActivityContainer) getActivity()).getoldpatientupdateflag() == false) {
                pregnencylive_result.put(result);
            } else if (patientflag == true && ((FragmentActivityContainer) getActivity()).getoldpatientupdateflag() == true) {
                try {
                    pregnencylive_result.put(pregnencylive_result.length() - 1, result);
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            }
        } else {
            pregnencylive_result_array = new JSONArray();
            pregnencylive_result_array.put(result);
        }
    }


    public void pregnencygenderdata() {
        String result = "";
        for (int i = 0; i < preggender.size(); i++) {
            result += preggender.get(i) + ",";
        }
        preggender.clear();
        if (pregnencygender_result != null) {
            if (patientflag == false) {
                try {
                    pregnencygender_result.put(pregnencygender_result.length() - 1, result);
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            } else if (patientflag == true && ((FragmentActivityContainer) getActivity()).getoldpatientupdateflag() == false) {
                pregnencygender_result.put(result);
            } else if (patientflag == true && ((FragmentActivityContainer) getActivity()).getoldpatientupdateflag() == true) {
                try {
                    pregnencygender_result.put(pregnencygender_result.length() - 1, result);
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            }
        } else {
            pregnencygender_result_array = new JSONArray();
            pregnencygender_result_array.put(result);
        }
    }


    public void pregnencyyeardata() {
        String result = "";
        for (int i = 0; i < yearvalue.size(); i++) {
            result += yearvalue.get(i) + ",";
        }
        yearvalue.clear();
        if (pregnencyyear_resullt != null) {
            if (patientflag == false) {
                try {
                    pregnencyyear_resullt.put(pregnencyyear_resullt.length() - 1, result);
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            } else if (patientflag == true && ((FragmentActivityContainer) getActivity()).getoldpatientupdateflag() == false) {
                pregnencyyear_resullt.put(result);
            } else if (patientflag == true && ((FragmentActivityContainer) getActivity()).getoldpatientupdateflag() == true) {
                try {
                    pregnencyyear_resullt.put(pregnencyyear_resullt.length() - 1, result);
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            }
        } else {
            pregnencyyear_resullt_array = new JSONArray();
            pregnencyyear_resullt_array.put(result);
        }
    }

    public void pregnencyweekdata() {
        String result = "";
        Log.d("WeekValue", weekvalue.size() + "  weekvalue.size()");

        for (int i = 0; i < weekvalue.size(); i++) {
            result += weekvalue.get(i) + ",";
        }
        weekvalue.clear();
        if (pregnency_week != null) {
            if (patientflag == false) {
                try {
                    pregnency_week.put(pregnency_week.length() - 1, result);
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            } else if (patientflag == true && ((FragmentActivityContainer) getActivity()).getoldpatientupdateflag() == false) {
                pregnency_week.put(result);
            } else if (patientflag == true && ((FragmentActivityContainer) getActivity()).getoldpatientupdateflag() == true) {
                try {
                    pregnency_week.put(pregnency_week.length() - 1, result);
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            }
        } else {
            pregnency_week_resullt_array = new JSONArray();
            pregnency_week_resullt_array.put(result);
        }
    }


    public void pregnencyldddata() {
        String result = "";
        result = ldddata;
        if (pregnencyldd_result != null) {
            if (patientflag == false) {
//                try {
//                    pregnencyldd_result.put(pregnencyldd_result.length() - 1, result);
                pregnencyldd_result = pregnencyldd_result;
//                } catch (JSONException e) {
//                    // TODO Auto-generated catch block
//                    e.printStackTrace();
//                }
            } else if (patientflag == true && ((FragmentActivityContainer) getActivity()).getoldpatientupdateflag() == false) {
//                pregnencyldd_result.put(result);
                pregnencyldd_result = result;
            } else if (patientflag == true && ((FragmentActivityContainer) getActivity()).getoldpatientupdateflag() == true) {
//                try {
//                    pregnencyldd_result.put(pregnencyldd_result.length() - 1, result);
                pregnencyldd_result = pregnencyldd_result;
//                } catch (JSONException e) {
//                    // TODO Auto-generated catch block
//                    e.printStackTrace();
//                }

            }
        } else {
//            pregnencyldd_resullt_array = new JSONArray();
//            pregnencyldd_resullt_array.put(result);
            pregnencyldd_resullt_array = result;
        }
    }


    public void pregnencyedddata() {
        String result = "";
        result = edddata;
        if (pregnencyedd_result != null) {
            if (patientflag == false) {
//                try {
//                    pregnencyedd_result.put(pregnencyedd_result.length() - 1, result);
                pregnencyedd_result = pregnencyedd_result;
//                } catch (JSONException e) {
//                    // TODO Auto-generated catch block
//                    e.printStackTrace();
//                }
            } else if (patientflag == true && ((FragmentActivityContainer) getActivity()).getoldpatientupdateflag() == false) {
//                pregnencyedd_result.put(result);
                pregnencyedd_result = result;
            } else if (patientflag == true && ((FragmentActivityContainer) getActivity()).getoldpatientupdateflag() == true) {
//                try {
//                    pregnencyedd_result.put(pregnencyedd_result.length() - 1, result);
                pregnencyedd_result = pregnencyedd_result;
//                } catch (JSONException e) {
//                    // TODO Auto-generated catch block
//                    e.printStackTrace();
//                }

            }
        } else {
//            pregnencyedd_result_array = new JSONArray();
//            pregnencyedd_result_array.put(result);
            pregnencyedd_result_array = result;
        }
    }


    public void pregnencydogdata() {
        String result = "";
        result = dogdata;
        if (pregnencydog_result != null) {
            if (patientflag == false) {
//                try {
//                    pregnencydog_result.put(pregnencydog_result.length() - 1, result);
                pregnencydog_result = pregnencydog_result;
//                } catch (JSONException e) {
//                    // TODO Auto-generated catch block
//                    e.printStackTrace();
//                }
            } else if (patientflag == true && ((FragmentActivityContainer) getActivity()).getoldpatientupdateflag() == false) {
//                pregnencydog_result.put(result);
                pregnencydog_result = result;
            } else if (patientflag == true && ((FragmentActivityContainer) getActivity()).getoldpatientupdateflag() == true) {
//                try {
//                    pregnencydog_result.put(pregnencydog_result.length() - 1, result);
                pregnencydog_result = pregnencydog_result;
//                } catch (JSONException e) {
//                    // TODO Auto-generated catch block
//                    e.printStackTrace();
//                }
            }
        } else {
//            pregnencydog_result_array = new JSONArray();
//            pregnencydog_result_array.put(result);
            pregnencydog_result_array = result;
        }
    }
}

class paraelement extends BaseAdapter {

    private Context context;
    ArrayList<ParanumberpickerObject> pickerObject;


    public paraelement(Context mContext, ArrayList<ParanumberpickerObject> obj) {
        // TODO Auto-generated constructor stub
        this.context = mContext;
        this.pickerObject = obj;
    }


    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return pickerObject.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return pickerObject.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup arg2) {
        // TODO Auto-generated method stub
        View grid;
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		/*  if (convertView == null) {*/

        grid = new View(context);
        grid = inflater.inflate(R.layout.paragrid_container, null);

        NumberPicker numberpicker = (NumberPicker) grid.findViewById(R.id.picker_row);
        ImageView addimage = (ImageView) grid.findViewById(R.id.iv_addbtn);

        System.out.println("picker object size" + pickerObject.size());
        System.out.println("position value.." + position);

        if (position == pickerObject.size() - 1) {
            System.out.println("Visible working...");
            addimage.setVisibility(View.VISIBLE);
        }


        addimage.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub

                ParanumberpickerObject obj = new ParanumberpickerObject();
                obj.setPickervalue(0);
                pickerObject.add(obj);

                notifyDataSetChanged();

            }
        });


        return grid;
    }

    public void calculateEDD(String date) {
    }

}


