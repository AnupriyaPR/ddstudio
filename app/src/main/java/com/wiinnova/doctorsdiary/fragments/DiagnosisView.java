package com.wiinnova.doctorsdiary.fragments;


import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.wiinnova.doctorsdiary.R;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.wiinnova.doctorsdiary.activities.FragmentActivityContainer;
import com.wiinnova.doctorsdiary.activities.Home_Page_Activity;
import com.wiinnova.doctorsdiary.fragment.CrashReporter;
import com.wiinnova.doctorsdiary.fragment.Diagnosis_Frag;
import com.wiinnova.doctorsdiary.fragment.Diagnosis_Frag_Gynacologist;
import com.wiinnova.doctorsdiary.supportclasses.Diseasenameclass;

import java.util.ArrayList;
import java.util.List;

public class  DiagnosisView extends Fragment {
	TextView dflName,duniqueID;
	String puid,filan;
	TextView symptoms,syndromes,additionalcumm,date,llist;
	ProgressDialog mProgressDialog;
	private LinearLayout layoutcontainer;
	ParseObject patient2;
	ParseObject patient;
	int skip=0;

	ArrayList<Diseasenameclass> diseasenamearraylist=new ArrayList<Diseasenameclass>();
	private  List<ParseObject>  diseaseobjects = new ArrayList<ParseObject>();
	private Diagnosis_Frag diagfrag;
	List<ParseObject> _suspecteddiseasenamesObj;
	private String specialization;
	private Diagnosis_Frag_Gynacologist diagfraggyn;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {


		final View view=inflater.inflate(R.layout.diagnosis_view_frag, null);

		dflName=(TextView)view.findViewById(R.id.tvfirstlastName);
		duniqueID=(TextView)view.findViewById(R.id.cduniqueid);
		layoutcontainer=(LinearLayout)view.findViewById(R.id.diadynamic);

		_suspecteddiseasenamesObj=((CrashReporter)getActivity().getApplicationContext()).getDiseasenameObjects();
		System.out.println("diseasenamesObj size"+_suspecteddiseasenamesObj.size());

		SharedPreferences sp=getActivity().getSharedPreferences("Login", 0);
		specialization=sp.getString("spel", null);

		if(specialization.equalsIgnoreCase("gynecologist obstetrician")){
			
		}
		else if(specialization.equalsIgnoreCase("Nephrologist")){
			
		}
		else{

			if(getFragmentManager().findFragmentById(R.id.fl_sidemenu_container) instanceof Diagnosis_Frag){

			}else{
				diagfrag = new Diagnosis_Frag();
				getFragmentManager().beginTransaction()
				.replace(R.id.fl_sidemenu_container, diagfrag)
				.commit();
			}
			if(diagfrag!=null){
				((FragmentActivityContainer)getActivity()).setdiagnosisfrag(diagfrag);
			}
		}
		/*else{

			if(getFragmentManager().findFragmentById(R.id.fl_sidemenu_container) instanceof Diagnosis_Frag_Gynacologist){

			}else{
				diagfraggyn = new Diagnosis_Frag_Gynacologist();
				getFragmentManager().beginTransaction()
				.replace(R.id.fl_sidemenu_container, diagfrag)
				.commit();
			}
			if(diagfrag!=null){
				((FragmentActivityContainer)getActivity()).setDiagfrag_gynacologist(diagfraggyn);
			}

		}*/




		if(Home_Page_Activity.patient!=null){
			puid=Home_Page_Activity.patient.getString("patientID");
			String fname=Home_Page_Activity.patient.getString("firstName");
			String lname=Home_Page_Activity.patient.getString("lastName");
			System.out.println("patient id1,,,,,,"+puid);
			if(fname==null){
				fname="FirstName";
			}
			if(lname==null){
				lname="LastName";
			}
			dflName.setText(fname+" "+lname);

		}else{

			SharedPreferences scanpidnew=getActivity().getSharedPreferences("NewPatID", 0);
			String scnewPid=scanpidnew.getString("NewID", null); 
			SharedPreferences scanpid1=getActivity().getSharedPreferences("ScannedPid", 0);
			String scPid=scanpid1.getString("scanpid", null);       
			if(scPid!=null){
				puid=scPid;
				SharedPreferences fnname1=getActivity().getSharedPreferences("fnName", 0);
				dflName.setText(fnname1.getString("fNname", "FirstName"+" "+"LastName"));
				System.out.println("name diag view.."+fnname1.getString("fNname", "FirstName"+" "+"LastName"));
			}

			if(scnewPid!=null){

				puid=scnewPid;


				SharedPreferences fnname1=getActivity().getSharedPreferences("fnName", 0);
				dflName.setText(fnname1.getString("fNname", "FirstName"+" "+"LastName"));
				System.out.println("name diag view.."+fnname1.getString("fNname", "FirstName"+" "+"LastName"));
			}

		}

		duniqueID.setText(puid);

		mProgressDialog = new ProgressDialog(getActivity());
		mProgressDialog.setCancelable(false);
		mProgressDialog.setMessage("Retrieving Diagnosis Data...");
		mProgressDialog.show();
		System.out.println("object taken from main frag not working");

		if(_suspecteddiseasenamesObj==null || _suspecteddiseasenamesObj.size()==0){

			diseaseobjects.clear();
			diseasenamearraylist.clear();

			fetechdiseasefromserver();


			System.out.println("first working......");
		}else{

			System.out.println("working,,.....");
			diseasenamearraylist.clear();

			for(int i=0;i<_suspecteddiseasenamesObj.size();i++){

				Diseasenameclass diseasenameObj=new Diseasenameclass();
				diseasenameObj.setDiseasename(_suspecteddiseasenamesObj.get(i).getString("DiseaseName"));
				diseasenameObj.setIcdnumber(_suspecteddiseasenamesObj.get(i).getString("ICDNumber"));
				diseasenamearraylist.add(diseasenameObj);
			}
			compairing_code_taken_disease_fromparse();
		}

		return view;
	}


	private void fetechdiseasefromserver() {
		// TODO Auto-generated method stub
		ParseQuery<ParseObject> query = ParseQuery.getQuery("DiseaseDatabase");
		query.fromLocalDatastore();
		query.findInBackground(new FindCallback<ParseObject>() {

			@Override
			public void done(List<ParseObject> objects, ParseException arg1) {
				// TODO Auto-generated method stub


				ParseQuery<ParseObject> query = ParseQuery.getQuery("DiseaseDatabase");

				query.fromLocalDatastore();

				query.setLimit(1000);
				query.findInBackground(getAllObjects());

			}
		});
	}

	private FindCallback getAllObjects() {
		// c

		return new FindCallback<ParseObject>() {

			@Override
			public void done(List<ParseObject> objects, ParseException e) {
				// TODO Auto-generated method stub

				if (e == null) {

					diseaseobjects.addAll(objects);

					int limit =1000;
					if (objects.size() == limit){
						skip = skip + limit;
						ParseQuery query = new ParseQuery("DiseaseDatabase");
						query.fromLocalDatastore();
						query.setSkip(skip);
						query.setLimit(limit);
						query.findInBackground(getAllObjects());
					}
					else {
						if(ParseObject.pinAllInBackground(diseaseobjects) != null){
							System.out.println("pin successs"+diseaseobjects.size());
						}

						ParseObject.pinAllInBackground(diseaseobjects);


						for(int i=0;i<diseaseobjects.size();i++){

							Diseasenameclass diseasenameObj=new Diseasenameclass();
							diseasenameObj.setDiseasename(diseaseobjects.get(i).getString("DiseaseName"));
							diseasenameObj.setIcdnumber(diseaseobjects.get(i).getString("ICDNumber"));
							diseasenamearraylist.add(diseasenameObj);
						}

						compairing_code_taken_disease_fromparse();

					}
				}
			}
		};
	}

	private void compairing_code_taken_disease_fromparse() {
		// TODO Auto-generated method stub
		System.out.println("compairing code working.....");
		System.out.println("patient id"+puid);

		final ParseQuery<ParseObject> query = ParseQuery.getQuery("Diagnosis");
		query.fromLocalDatastore();
		query.whereEqualTo("patientID",puid );
		query.addDescendingOrder("createdDate");

		mProgressDialog.setOnCancelListener(new OnCancelListener() {

			@Override
			public void onCancel(DialogInterface dialog) {
				// TODO Auto-generated method stub
				query.cancel();
			}
		});

		query.findInBackground(new FindCallback<ParseObject>() {
			@Override
			public void done(List<ParseObject> objects, ParseException e) {
				// TODO Auto-generated method stub
				mProgressDialog.dismiss();

				if(DiagnosisView.this.isVisible()){
					Log.e("Login", "done");
					if(e==null){ 
						for (int i = 0; i < objects.size(); i++) {
							System.out.println("object size"+objects.size());
							ParseObject object = objects.get(i);
							LayoutInflater inflater = LayoutInflater.from(getActivity());
							if(inflater!=null){
								View patient_diagnosis_details = inflater.inflate(R.layout.diagnosis_view_dynamic_new, null); 
								double type=object.getDouble("typeFlag");
								System.out.println("type flag"+type);
								if(type==1){
									String syndro =object.getString("syndromes");
									String add=object.getString("additionalComments");
									String sympt=object.getString("symptoms");
									String formattedDate=object.getString("createdDate");
									System.out.println("create date"+formattedDate);

									symptoms=(TextView)patient_diagnosis_details.findViewById(R.id.crtsymedit);
									syndromes=(TextView)patient_diagnosis_details.findViewById(R.id.crtsynedit);
									additionalcumm=(TextView)patient_diagnosis_details.findViewById(R.id.crtadcmtdit);
									date=(TextView)patient_diagnosis_details.findViewById(R.id.cdcurrdate);

									if(sympt!=null){
										if(sympt.length()!=0){
											symptoms.setText(sympt);
										}else{
											symptoms.setText("N/A");	
										}
									}

									if(syndro!=null){
										if(syndro.length()!=0){
											syndromes.setText(syndro);
										}else{
											syndromes.setText("N/A");	
										}
									}

									if(add!=null){
										if(add.length()!=0){
											additionalcumm.setText(add);
										}else{
											additionalcumm.setText("N/A");
										}
									}
									date.setText(formattedDate);

									System.out.println("additional comments"+add);

									//dflName.setText(filan);

									llist=(TextView)patient_diagnosis_details.findViewById(R.id.vmylist1);
									String susp=object.getString("suspectedDisease");
									String[] value = null;
									if(susp!=null)
										value = susp.split(",");
									if(value!=null)
										for(int k=0;k<value.length;k++){

											System.out.println("code valuessss from parse"+value[k]);
										}
									String[]  myaccrArray = null;
									if(value!=null){
										myaccrArray = new String[value.length];
										System.out.println("diseasenamearraylist size"+diseasenamearraylist.size());
										for(int j = 0; j < value.length; j++){    
											for(int k=0;k<diseasenamearraylist.size();k++)
											{

												if((diseasenamearraylist.get(k).getIcdnumber())!=null){
													String icd_from_list=diseasenamearraylist.get(k).getIcdnumber().toString().trim();
													String icd_from_selected=value[j].toString().trim();

													if(icd_from_list.equalsIgnoreCase(icd_from_selected)){
														myaccrArray[j]=diseasenamearraylist.get(k).getDiseasename();
													}else{
														if(icd_from_selected.equalsIgnoreCase("Other")){
															myaccrArray[j]="Other";
														}else if(icd_from_selected.equalsIgnoreCase("None")){
															myaccrArray[j]="None";
														}
														else if(icd_from_selected.equalsIgnoreCase("-")){
															myaccrArray[j]="-";
														}
												

													}
												}
											}
											TextView tlm1 = new TextView(getActivity()); 
											String disease="";
											for(int k=0;k<myaccrArray.length;k++){
												disease+=myaccrArray[k]+"\n";
											}

											llist.setText(disease);
										}
									}
									if(susp!=null)
										layoutcontainer.addView(patient_diagnosis_details);
								}
							}}
					}
					else{
						e.printStackTrace();
						Toast.makeText(getActivity(), "Invalid Patient", Toast.LENGTH_SHORT).show();
					}
				}
			}});
	}
	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		CrashReporter.getInstance().trackScreenView("Diagnosis View");
	}
}
