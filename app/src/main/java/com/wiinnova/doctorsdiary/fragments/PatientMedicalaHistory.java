package com.wiinnova.doctorsdiary.fragments;


import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.wiinnova.doctorsdiary.R;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.SaveCallback;
import com.wiinnova.doctorsdiary.activities.FragmentActivityContainer;
import com.wiinnova.doctorsdiary.fragment.CrashReporter;
import com.wiinnova.doctorsdiary.fragment.CurrentVitals;
import com.wiinnova.doctorsdiary.fragment.Patient_Frag;
import com.wiinnova.doctorsdiary.fragment.Patient_Personal_Info_Frag;
import com.wiinnova.doctorsdiary.fragment.Patient_Frag.onMedicalhistorysubmit;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.Arrays;

public class PatientMedicalaHistory extends Fragment implements onMedicalhistorysubmit {


	public interface onbuttonactivationmedhist{
		void onbuttonmedhist(int arg);
	}
	boolean patientedit_checkflag=true;
	boolean patientflag=true;
	ParseObject patientobject;

	TextView tv_flname;
	TextView tv_patientid;
	Button phNext;
	Button btnSubmit;
	LinearLayout addAlle;
	LinearLayout addSurge;
	LinearLayout addMajor;
	LinearLayout llMajorillness;
	LinearLayout llRemoveallergy;

	JSONArray ar_in;
	JSONArray ar_inreac;
	JSONArray ar_insur;
	JSONArray ar_in1;
	JSONArray ar_in2;
	JSONArray ar_infam;
	JSONArray ar_infamarray;
	JSONArray ar_shistarray;
	JSONArray ar_shist;
	int a = 0;
	int b=0;
	int activationflagvalue;

	RelativeLayout rlPowerd;
	//ArrayList<Patient_info_Store> arrayOfpatinfo;
	CheckBox noAlerReport,noIllReport,noSurgeryReport,hyperCurr,hyperPast,hyperNa,diaCurr,diaPast,diaNa,canCurr,canPast,canNa;
	EditText medicOne,medicTwo,reactionOne,reactionTwo,surgeryOne,surgeryTwo,Ed1,Ed2;
	TextView Tv1,Tv2;
	LinearLayout rl,majill;
	LinearLayout llSurgery;


	ArrayList<String> allmedicArray =new ArrayList<String>();
	ArrayList<String> allreacArray = new ArrayList<String>();
	ArrayList<String> majillArray = new ArrayList<String>();
	ArrayList<String> majillStaArray = new ArrayList<String>();
	ArrayList<String> surgArray = new ArrayList<String>();

	ArrayList<String> surgArray_one = new ArrayList<String>();
	ArrayList<String> surgArray_two = new ArrayList<String>();

	ArrayList<ImageView> cross = new ArrayList<ImageView>();
	//ArrayList<ImageView> reactionlist = new ArrayList<ImageView>();  

	ArrayList<EditText> medicationlist = new ArrayList<EditText>();
	ArrayList<EditText> reactionlist = new ArrayList<EditText>();   

	ArrayList<EditText> surgeryfirstlist = new ArrayList<EditText>();
	ArrayList<EditText> surgerysecondlist = new ArrayList<EditText>();   

	ArrayList<EditText> majorillnesslist=new ArrayList<EditText>();
	ArrayList<CheckBox> currentcblist=new ArrayList<CheckBox>();
	ArrayList<CheckBox> pastcblist=new ArrayList<CheckBox>();
	ArrayList<CheckBox> nacblist=new ArrayList<CheckBox>();

	ArrayList<String> selectedcheckboxlist=new ArrayList<String>();




	ProgressDialog mProgressDialog;

	EditText fother,mother,sother,gfother,gmother;
	CheckBox fhyper,mhyper,shyper,gfhyper,gmhyper,fdia,mdia,sdia,gfdia,gmdia,fcan,mcan,scan,gfcan,gmcan,fOther,mOther,sOther,gfOther,gmOther,fna,mna,sna,gfna,gmna,alcurr,alpast,alna,tobcurr,tobpast,tobna,osubcurr,osubpast,osubna;
	String[] millfamArray = null;
	String[] millfamStatArray = null;
	String[] socHistArray = null;
	String[] socHistStatArray = null;
	String[] drArray = null;

	String patientId,pfirstName,pmiddleName,plastName,padhaarNo,psex,pmaritalSta,pyearBirth,pstrtName,pCitName,ppinCode,pStat,pCountry,pPhoneNum,pemailId,pHeight,pWeight,pBgroup;
	String[] pallmedicArray,pallreacArray,pmajillArray,pmajillStaArray,psurgArray;	

	int j=3,i=2,k=4,medicindex,surgindex,majorindex,surgindex1=3,surgindex2=4;

	LinearLayout container;
	LinearLayout llContainer;
	CurrentVitals patientcurrentvitalsfragObj;
	Patient_Personal_Info_Frag patientpesonalinfofrag;
	private Patient_Frag patientfragObj;
	private boolean admintab;
	private boolean admintabdefault;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {


		final View view=inflater.inflate(R.layout.patient_medical_history_first_frag, null);

		Bundle bundle=getArguments();
		patientflag=bundle.getBoolean("patientflag");
		admintab=bundle.getBoolean("admintab");


		patientfragObj = new Patient_Frag();
		Bundle bundle1=new Bundle();
		bundle1.putInt("sidemenuitem", 2);
		bundle1.putBoolean("patientflag", patientflag);
		patientfragObj.setArguments(bundle1);


		getFragmentManager().beginTransaction()
		.replace(R.id.fl_sidemenu_container, patientfragObj)
		.commit();

		if(patientfragObj!=null){
			((FragmentActivityContainer)getActivity()).setpatientfrag(patientfragObj);
		}




		btnSubmit=(Button)view.findViewById(R.id.btnSubmit);
		llContainer=(LinearLayout)view.findViewById(R.id.llscroll);
		container=(LinearLayout)view.findViewById(R.id.topContainer);
		//	rlPowerd=(RelativeLayout)view.findViewById(R.id.rlpowerd);
		//Button
		phNext=(Button)view.findViewById(R.id.pmhnext);
		addAlle=(LinearLayout)view.findViewById(R.id.lladd_allergy);
		addSurge=(LinearLayout)view.findViewById(R.id.pmhl54);
		addMajor=(LinearLayout)view.findViewById(R.id.ll_add_more_mi);
		llRemoveallergy=(LinearLayout)view.findViewById(R.id.llremove_allergy);

		tv_flname=(TextView)view.findViewById(R.id.tvname);
		tv_patientid=(TextView)view.findViewById(R.id.cduniqueid);
		//EditText
		medicOne=(EditText)view.findViewById(R.id.pmhmed1);
		medicTwo=(EditText)view.findViewById(R.id.pmhmed2);
		reactionOne=(EditText)view.findViewById(R.id.pmhreac1);
		reactionTwo=(EditText)view.findViewById(R.id.pmhreac2);
		surgeryOne=(EditText)view.findViewById(R.id.pmhsur1);
		surgeryTwo=(EditText)view.findViewById(R.id.pmhsur2);

		//EditText
		fother=(EditText)view.findViewById(R.id.pmhsfotheredit);
		mother=(EditText)view.findViewById(R.id.pmhsmotheredit);
		sother=(EditText)view.findViewById(R.id.pmhssotheredit);
		gfother=(EditText)view.findViewById(R.id.pmhsgfotheredit);
		gmother=(EditText)view.findViewById(R.id.pmhsgmotheredit);

		//CheckBox
		noAlerReport=(CheckBox)view.findViewById(R.id.pmhaller);
		noIllReport=(CheckBox)view.findViewById(R.id.pmhmill);
		noSurgeryReport=(CheckBox)view.findViewById(R.id.pmhsurg);
		hyperCurr=(CheckBox)view.findViewById(R.id.pmhhypcurr);
		hyperPast=(CheckBox)view.findViewById(R.id.pmhhyppast);
		hyperNa=(CheckBox)view.findViewById(R.id.pmhhypna);
		diaCurr=(CheckBox)view.findViewById(R.id.pmhdiacurr);
		diaPast=(CheckBox)view.findViewById(R.id.pmhdiapast);
		diaNa=(CheckBox)view.findViewById(R.id.pmhdiana);
		canCurr=(CheckBox)view.findViewById(R.id.pmhcancurr);
		canPast=(CheckBox)view.findViewById(R.id.pmhcanpast);
		canNa=(CheckBox)view.findViewById(R.id.pmhcanna);
		/*othCurr=(CheckBox)view.findViewById(R.id.pmhothercurr);
		otherPast=(CheckBox)view.findViewById(R.id.pmhotherpast);
		otherNa=(CheckBox)view.findViewById(R.id.pmhotherna);*/

		//CheckBox
		fhyper=(CheckBox)view.findViewById(R.id.pmhsfhyper);
		fdia=(CheckBox)view.findViewById(R.id.pmhsfdia);
		fcan=(CheckBox)view.findViewById(R.id.pmhsfcancer);
		fOther=(CheckBox)view.findViewById(R.id.pmhsfother);
		fna=(CheckBox)view.findViewById(R.id.pmhsna);

		mhyper=(CheckBox)view.findViewById(R.id.pmhsmhyper);
		mdia=(CheckBox)view.findViewById(R.id.pmhsmdia);
		mcan=(CheckBox)view.findViewById(R.id.pmhsmcancer);
		mOther=(CheckBox)view.findViewById(R.id.pmhsmaother);
		mna=(CheckBox)view.findViewById(R.id.pmhsmna);

		shyper=(CheckBox)view.findViewById(R.id.pmhsshyper);
		sdia=(CheckBox)view.findViewById(R.id.pmhssdia);
		scan=(CheckBox)view.findViewById(R.id.pmhsscancer);
		sOther=(CheckBox)view.findViewById(R.id.pmhssother);
		sna=(CheckBox)view.findViewById(R.id.pmhssna);

		gfhyper=(CheckBox)view.findViewById(R.id.pmhsgfhyper);
		gfdia=(CheckBox)view.findViewById(R.id.pmhsgfdia);
		gfcan=(CheckBox)view.findViewById(R.id.pmhsgfcancer);
		gfOther=(CheckBox)view.findViewById(R.id.pmhsgfother);
		gfna=(CheckBox)view.findViewById(R.id.pmhsgfna);

		gmhyper=(CheckBox)view.findViewById(R.id.pmhsgmhyper);
		gmdia=(CheckBox)view.findViewById(R.id.pmhsgmdia);
		gmcan=(CheckBox)view.findViewById(R.id.pmhsgmcancer);
		gmOther=(CheckBox)view.findViewById(R.id.pmhsgmtother);
		gmna=(CheckBox)view.findViewById(R.id.pmhsgmna);

		alcurr=(CheckBox)view.findViewById(R.id.pmhsalcurr);
		alpast=(CheckBox)view.findViewById(R.id.pmhsalpast);
		alna=(CheckBox)view.findViewById(R.id.pmhsalna);

		tobcurr=(CheckBox)view.findViewById(R.id.pmhstobcurr);
		tobpast=(CheckBox)view.findViewById(R.id.pmhstobpast);
		tobna=(CheckBox)view.findViewById(R.id.pmhstobna);

		osubcurr=(CheckBox)view.findViewById(R.id.pmhsotrcurr);
		osubpast=(CheckBox)view.findViewById(R.id.pmhsotrpast);
		osubna=(CheckBox)view.findViewById(R.id.pmhsotrna);

		rl =(LinearLayout)view.findViewById(R.id.pmhl33);
		llSurgery=(LinearLayout)view.findViewById(R.id.pmhl53);
		llMajorillness=(LinearLayout)view.findViewById(R.id.ll_add_mi);

		//second

		//set_medicalhistory_details();

		fother.setEnabled(false);
		mother.setEnabled(false);
		sother.setEnabled(false);
		gfother.setEnabled(false);
		gmother.setEnabled(false);



		fOther.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				//is chkIos checked?
				if (((CheckBox) v).isChecked()) {
					//Case 1
					fother.setEnabled(true);

				}
				else {
					//case 2
					fother.setEnabled(false);
					fother.setText("");

				}


			}
		});

		mOther.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				//is chkIos checked?
				if (((CheckBox) v).isChecked()) {
					//Case 1
					mother.setEnabled(true);

				}
				else {
					//case 2
					mother.setEnabled(false);
					mother.setText("");

				}


			}
		});
		sOther.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				//is chkIos checked?
				if (((CheckBox) v).isChecked()) {
					//Case 1
					sother.setEnabled(true);

				}
				else {
					//case 2
					sother.setEnabled(false);
					sother.setText("");

				}


			}
		});

		gfOther.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				//is chkIos checked?
				if (((CheckBox) v).isChecked()) {
					//Case 1
					gfother.setEnabled(true);


				}
				else {
					//case 2
					gfother.setEnabled(false);
					gfother.setText("");
				}


			}
		});
		gmOther.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				//is chkIos checked?
				if (((CheckBox) v).isChecked()) {
					//Case 1
					gmother.setEnabled(true);
				}
				else {
					//case 2
					gmother.setEnabled(false);
					gmother.setText("");
				}
			}
		});

		try{
			((FragmentActivityContainer)getActivity()).setPatientMedicalHistory_nephro(this);

			((FragmentActivityContainer)getActivity()).getpatientfrag().menu_item=2;

			patientedit_checkflag=((FragmentActivityContainer)getActivity()).getpatientflagvalue();
			admintab=((FragmentActivityContainer)getActivity()).getcheckadmintab();
			admintabdefault=((FragmentActivityContainer)getActivity()).getadmintabdefault();

			if(admintab==true){
				System.out.println("admin tab working.....");
				patientobject=((FragmentActivityContainer)getActivity()).getOldPatientobject();
			}else if(admintab==false && admintabdefault==false){
				patientobject=((FragmentActivityContainer)getActivity()).getPatientobject();
			}else if(admintab==false && admintabdefault==true){
				patientobject=((FragmentActivityContainer)getActivity()).getOldPatientobject();
			}


		}catch(Exception e){
			e.printStackTrace();
		}
		if(patientedit_checkflag==false){
			patientflag=false;
		}
		System.out.println(" patient flag medical history"+patientflag);

		if(patientflag==true && patientobject!=null){
			System.out.println("activation falg value"+FragmentActivityContainer.check_save);
			phNext.setVisibility(View.GONE);
			patientId=patientobject.getString("patientID");
			String fname=patientobject.getString("firstName");
			String lname=patientobject.getString("lastName");
			System.out.println("name........"+fname);
			if(fname==null){
				fname="FirstName";
			}
			if(lname==null){
				lname="LastName";
			}

			tv_flname.setText(fname +" "+lname);
			tv_patientid.setText(patientId);


		}


		hyperNa.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (((CheckBox) v).isChecked()){
					hyperCurr.setEnabled(false);
					hyperPast.setEnabled(false);
					hyperCurr.setChecked(false);
					hyperPast.setChecked(false);
					noIllReport.setEnabled(false);


					//submitButtonActivation();
				}else{
					hyperCurr.setEnabled(true);
					hyperPast.setEnabled(true);
					noIllReport.setEnabled(true);
					//submitButtonDeactivation();
					
				}

			}
		});

		hyperCurr.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (((CheckBox) v).isChecked()){
					hyperPast.setChecked(false);
					hyperNa.setChecked(false);
					hyperPast.setEnabled(false);
					hyperNa.setEnabled(false);
					noIllReport.setEnabled(false);
				}else{
					hyperPast.setEnabled(true);
					hyperNa.setEnabled(true);
					noIllReport.setEnabled(true);
				}

			}
		});

		hyperPast.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (((CheckBox) v).isChecked()){
					hyperCurr.setChecked(false);
					hyperNa.setChecked(false);
					hyperCurr.setEnabled(false);
					hyperNa.setEnabled(false);
					noIllReport.setEnabled(false);
				}else{
					hyperCurr.setEnabled(true);
					hyperNa.setEnabled(true);
					noIllReport.setEnabled(true);
				}

			}
		});


		diaNa.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (((CheckBox) v).isChecked()){
					diaCurr.setEnabled(false);
					diaPast.setEnabled(false);
					diaCurr.setChecked(false);
					diaPast.setChecked(false);
					noIllReport.setEnabled(false);
					//submitButtonActivation();
				}else{
					diaCurr.setEnabled(true);
					diaPast.setEnabled(true);
					noIllReport.setEnabled(true);
					//submitButtonDeactivation();
				}
			}
		});


		diaCurr.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (((CheckBox) v).isChecked()){
					diaPast.setChecked(false);
					diaNa.setChecked(false);
					diaPast.setEnabled(false);
					diaNa.setEnabled(false);
					noIllReport.setEnabled(false);
				}else{
					diaPast.setEnabled(true);
					diaNa.setEnabled(true);
					noIllReport.setEnabled(true);
				}

			}
		});

		diaPast.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (((CheckBox) v).isChecked()){
					diaCurr.setChecked(false);
					diaNa.setChecked(false);
					diaCurr.setEnabled(false);
					diaNa.setEnabled(false);
					noIllReport.setEnabled(false);
				}else{
					diaCurr.setEnabled(true);
					diaNa.setEnabled(true);
					noIllReport.setEnabled(true);
				}

			}
		});



		canNa.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (((CheckBox) v).isChecked()){
					canCurr.setEnabled(false);
					canPast.setEnabled(false);
					canCurr.setChecked(false);
					canPast.setChecked(false);
					noIllReport.setEnabled(false);
					//submitButtonActivation();
				}else{
					canCurr.setEnabled(true);
					canPast.setEnabled(true);
					noIllReport.setEnabled(true);
					//submitButtonDeactivation();
				}
			}
		});


		canCurr.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (((CheckBox) v).isChecked()){
					canPast.setChecked(false);
					canNa.setChecked(false);
					canPast.setEnabled(false);
					canNa.setEnabled(false);
					noIllReport.setEnabled(false);
				}else{
					canPast.setEnabled(true);
					canNa.setEnabled(true);
					noIllReport.setEnabled(true);
				}

			}
		});

		canPast.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (((CheckBox) v).isChecked()){
					canCurr.setChecked(false);
					canNa.setChecked(false);
					canCurr.setEnabled(false);
					canNa.setEnabled(false);
					noIllReport.setEnabled(false);
				}else{
					canCurr.setEnabled(true);
					canNa.setEnabled(true);
					noIllReport.setEnabled(true);
				}

			}
		});


		alna.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (((CheckBox) v).isChecked()){
					alcurr.setEnabled(false);
					alpast.setEnabled(false);
					alcurr.setChecked(false);
					alpast.setChecked(false);

				}else{
					alcurr.setEnabled(true);
					alpast.setEnabled(true);

				}
			}
		});


		alpast.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (((CheckBox) v).isChecked()){
					alcurr.setEnabled(false);
					alna.setEnabled(false);
					alcurr.setChecked(false);
					alna.setChecked(false);

				}else{
					alcurr.setEnabled(true);
					alna.setEnabled(true);

				}
			}
		});
		alcurr.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (((CheckBox) v).isChecked()){
					alpast.setEnabled(false);
					alna.setEnabled(false);
					alpast.setChecked(false);
					alna.setChecked(false);

				}else{
					alpast.setEnabled(true);
					alna.setEnabled(true);

				}
			}
		});

		tobna.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (((CheckBox) v).isChecked()){
					tobcurr.setEnabled(false);
					tobpast.setEnabled(false);			
					tobcurr.setChecked(false);
					tobpast.setChecked(false);

				}else{
					tobcurr.setEnabled(true);
					tobpast.setEnabled(true);

				}
			}
		});

		tobpast.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (((CheckBox) v).isChecked()){
					tobcurr.setEnabled(false);
					tobna.setEnabled(false);			
					tobcurr.setChecked(false);
					tobna.setChecked(false);

				}else{
					tobcurr.setEnabled(true);
					tobna.setEnabled(true);

				}
			}
		});

		tobcurr.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (((CheckBox) v).isChecked()){
					tobna.setEnabled(false);
					tobpast.setEnabled(false);			
					tobna.setChecked(false);
					tobpast.setChecked(false);

				}else{
					tobna.setEnabled(true);
					tobpast.setEnabled(true);

				}
			}
		});

		osubna.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if (((CheckBox) v).isChecked()){
					osubcurr.setEnabled(false);
					osubpast.setEnabled(false);		
					osubcurr.setChecked(false);
					osubpast.setChecked(false);

				}else{
					osubcurr.setEnabled(true);
					osubpast.setEnabled(true);

				}

			}
		});

		osubcurr.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if (((CheckBox) v).isChecked()){
					osubna.setEnabled(false);
					osubpast.setEnabled(false);		
					osubna.setChecked(false);
					osubpast.setChecked(false);

				}else{
					osubna.setEnabled(true);
					osubpast.setEnabled(true);

				}

			}
		});

		osubpast.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if (((CheckBox) v).isChecked()){
					osubna.setEnabled(false);
					osubcurr.setEnabled(false);		
					osubna.setChecked(false);
					osubcurr.setChecked(false);

				}else{
					osubna.setEnabled(true);
					osubcurr.setEnabled(true);

				}

			}
		});


		alna.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// TODO Auto-generated method stub
				if((  isChecked) && !(selectedcheckboxlist.contains(Integer.toString(alna.getId())))){
					submitButtonActivation();
					selectedcheckboxlist.add(Integer.toString(alna.getId()));
				}else if(!isChecked && (selectedcheckboxlist.contains(Integer.toString(alna.getId())))) 
				{
					submitButtonDeactivation();
					selectedcheckboxlist.remove(Integer.toString(alna.getId()));


				}	
			}
		});

		tobna.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton arg0, boolean isChecked) {
				// TODO Auto-generated method stub

				if((  isChecked) && !(selectedcheckboxlist.contains(Integer.toString(tobna.getId())))){
					submitButtonActivation();
					selectedcheckboxlist.add(Integer.toString(tobna.getId()));
				}else if(!isChecked && (selectedcheckboxlist.contains(Integer.toString(tobna.getId())))) 
				{
					submitButtonDeactivation();
					selectedcheckboxlist.remove(Integer.toString(tobna.getId()));


				}	

			}
		});

		osubna.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// TODO Auto-generated method stub
				if((  isChecked) && !(selectedcheckboxlist.contains(Integer.toString(osubna.getId())))){
					submitButtonActivation();
					selectedcheckboxlist.add(Integer.toString(osubna.getId()));
				}else if(!isChecked && (selectedcheckboxlist.contains(Integer.toString(osubna.getId())))) 
				{
					submitButtonDeactivation();
					selectedcheckboxlist.remove(Integer.toString(osubna.getId()));


				}	


			}
		});



		//MajorIllness Listener
		hyperNa.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton arg0, boolean isChecked) {
				// TODO Auto-generated method stub
				if((  isChecked) && !(selectedcheckboxlist.contains(Integer.toString(hyperNa.getId())))){
					submitButtonActivation();
					selectedcheckboxlist.add(Integer.toString(hyperNa.getId()));
				}else if(!isChecked && (selectedcheckboxlist.contains(Integer.toString(hyperNa.getId())))) 
				{
					submitButtonDeactivation();
					selectedcheckboxlist.remove(Integer.toString(hyperNa.getId()));


				}	
			}
		});


		diaNa.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton arg0, boolean isChecked) {
				// TODO Auto-generated method stub
				if((  isChecked) && !(selectedcheckboxlist.contains(Integer.toString(diaNa.getId())))){
					submitButtonActivation();
					selectedcheckboxlist.add(Integer.toString(diaNa.getId()));
				}else if(!isChecked && (selectedcheckboxlist.contains(Integer.toString(diaNa.getId())))) 
				{
					submitButtonDeactivation();
					selectedcheckboxlist.remove(Integer.toString(diaNa.getId()));


				}	
			}
		});


		canNa.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton arg0, boolean isChecked) {
				// TODO Auto-generated method stub
				if((  isChecked) && !(selectedcheckboxlist.contains(Integer.toString(canNa.getId())))){
					submitButtonActivation();
					selectedcheckboxlist.add(Integer.toString(canNa.getId()));
				}else if(!isChecked && (selectedcheckboxlist.contains(Integer.toString(canNa.getId())))) 
				{
					submitButtonDeactivation();
					selectedcheckboxlist.remove(Integer.toString(canNa.getId()));


				}	
			}
		});

		fhyper.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton arg0, boolean isChecked) {
				// TODO Auto-generated method stub

				if(activationflagvalue!=0 && FragmentActivityContainer.check_save==0)
				{
					FragmentActivityContainer.check_save=activationflagvalue;
					System.out.println("diabeties past checkbox working...");
				}
				if((  isChecked) && !(selectedcheckboxlist.contains(Integer.toString(fhyper.getId())))){
					submitButtonActivation();
					selectedcheckboxlist.add(Integer.toString(fhyper.getId()));
				}else if(!isChecked && (selectedcheckboxlist.contains(Integer.toString(fhyper.getId())))) 
				{
					submitButtonDeactivation();
					selectedcheckboxlist.remove(Integer.toString(fhyper.getId()));


				}	
			}
		});

		fdia.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// TODO Auto-generated method stub
				if(activationflagvalue!=0 && FragmentActivityContainer.check_save==0)
				{
					FragmentActivityContainer.check_save=activationflagvalue;
					System.out.println("diabeties past checkbox working...");
				}
				if((  isChecked) && !(selectedcheckboxlist.contains(Integer.toString(fdia.getId())))){
					submitButtonActivation();
					selectedcheckboxlist.add(Integer.toString(fdia.getId()));
				}else if(!isChecked && (selectedcheckboxlist.contains(Integer.toString(fdia.getId())))) 
				{
					submitButtonDeactivation();
					selectedcheckboxlist.remove(Integer.toString(fdia.getId()));


				}	
			}
		});

		fcan.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton arg0, boolean isChecked) {
				// TODO Auto-generated method stub
				if(activationflagvalue!=0 && FragmentActivityContainer.check_save==0)
				{
					FragmentActivityContainer.check_save=activationflagvalue;
					System.out.println("diabeties past checkbox working...");
				}
				if((  isChecked) && !(selectedcheckboxlist.contains(Integer.toString(fcan.getId())))){
					submitButtonActivation();
					selectedcheckboxlist.add(Integer.toString(fcan.getId()));
				}else if(!isChecked && (selectedcheckboxlist.contains(Integer.toString(fcan.getId())))) 
				{
					submitButtonDeactivation();
					selectedcheckboxlist.remove(Integer.toString(fcan.getId()));


				}	
			}
		});

		fOther.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// TODO Auto-generated method stub
				if(activationflagvalue!=0 && FragmentActivityContainer.check_save==0)
				{
					FragmentActivityContainer.check_save=activationflagvalue;
					System.out.println("diabeties past checkbox working...");
				}
				if((  isChecked) && !(selectedcheckboxlist.contains(Integer.toString(fOther.getId())))){
					submitButtonActivation();
					selectedcheckboxlist.add(Integer.toString(fOther.getId()));
				}else if(!isChecked && (selectedcheckboxlist.contains(Integer.toString(fOther.getId())))) 
				{
					submitButtonDeactivation();
					selectedcheckboxlist.remove(Integer.toString(fOther.getId()));


				}	
			}
		});

		//Mother Major Illness Listener

		mhyper.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// TODO Auto-generated method stub
				if(activationflagvalue!=0 && FragmentActivityContainer.check_save==0)
				{
					FragmentActivityContainer.check_save=activationflagvalue;
					System.out.println("diabeties past checkbox working...");
				}
				if((  isChecked) && !(selectedcheckboxlist.contains(Integer.toString(mhyper.getId())))){
					submitButtonActivation();
					selectedcheckboxlist.add(Integer.toString(mhyper.getId()));
				}else if(!isChecked && (selectedcheckboxlist.contains(Integer.toString(mhyper.getId())))) 
				{
					submitButtonDeactivation();
					selectedcheckboxlist.remove(Integer.toString(mhyper.getId()));


				}	
			}
		});

		mdia.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// TODO Auto-generated method stub
				if(activationflagvalue!=0 && FragmentActivityContainer.check_save==0)
				{
					FragmentActivityContainer.check_save=activationflagvalue;
					System.out.println("diabeties past checkbox working...");
				}
				if((  isChecked) && !(selectedcheckboxlist.contains(Integer.toString(mdia.getId())))){
					submitButtonActivation();
					selectedcheckboxlist.add(Integer.toString(mdia.getId()));
				}else if(!isChecked && (selectedcheckboxlist.contains(Integer.toString(mdia.getId())))) 
				{
					submitButtonDeactivation();
					selectedcheckboxlist.remove(Integer.toString(mdia.getId()));


				}	
			}
		});

		mcan.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// TODO Auto-generated method stub
				if(activationflagvalue!=0 && FragmentActivityContainer.check_save==0)
				{
					FragmentActivityContainer.check_save=activationflagvalue;
					System.out.println("diabeties past checkbox working...");
				}
				if((  isChecked) && !(selectedcheckboxlist.contains(Integer.toString(mcan.getId())))){
					submitButtonActivation();
					selectedcheckboxlist.add(Integer.toString(mcan.getId()));
				}else if(!isChecked && (selectedcheckboxlist.contains(Integer.toString(mcan.getId())))) 
				{
					submitButtonDeactivation();
					selectedcheckboxlist.remove(Integer.toString(mcan.getId()));


				}	
			}
		});

		mOther.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// TODO Auto-generated method stub
				if(activationflagvalue!=0 && FragmentActivityContainer.check_save==0)
				{
					FragmentActivityContainer.check_save=activationflagvalue;
					System.out.println("diabeties past checkbox working...");
				}
				if((  isChecked) && !(selectedcheckboxlist.contains(Integer.toString(mOther.getId())))){
					submitButtonActivation();
					selectedcheckboxlist.add(Integer.toString(mOther.getId()));
				}else if(!isChecked && (selectedcheckboxlist.contains(Integer.toString(mOther.getId())))) 
				{
					submitButtonDeactivation();
					selectedcheckboxlist.remove(Integer.toString(mOther.getId()));


				}	
			}
		});


		//Sister Major Illness Listener

		shyper.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// TODO Auto-generated method stub
				if(activationflagvalue!=0 && FragmentActivityContainer.check_save==0)
				{
					FragmentActivityContainer.check_save=activationflagvalue;
					System.out.println("diabeties past checkbox working...");
				}
				if((  isChecked) && !(selectedcheckboxlist.contains(Integer.toString(shyper.getId())))){
					submitButtonActivation();
					selectedcheckboxlist.add(Integer.toString(shyper.getId()));
				}else if(!isChecked && (selectedcheckboxlist.contains(Integer.toString(shyper.getId())))) 
				{
					submitButtonDeactivation();
					selectedcheckboxlist.remove(Integer.toString(shyper.getId()));


				}	
			}
		});

		sdia.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// TODO Auto-generated method stub
				if(activationflagvalue!=0 && FragmentActivityContainer.check_save==0)
				{
					FragmentActivityContainer.check_save=activationflagvalue;
					System.out.println("diabeties past checkbox working...");
				}
				if((  isChecked) && !(selectedcheckboxlist.contains(Integer.toString(sdia.getId())))){
					submitButtonActivation();
					selectedcheckboxlist.add(Integer.toString(sdia.getId()));
				}else if(!isChecked && (selectedcheckboxlist.contains(Integer.toString(sdia.getId())))) 
				{
					submitButtonDeactivation();
					selectedcheckboxlist.remove(Integer.toString(sdia.getId()));


				}	
			}
		});

		scan.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// TODO Auto-generated method stub
				if(activationflagvalue!=0 && FragmentActivityContainer.check_save==0)
				{
					FragmentActivityContainer.check_save=activationflagvalue;
					System.out.println("diabeties past checkbox working...");
				}
				if((  isChecked) && !(selectedcheckboxlist.contains(Integer.toString(scan.getId())))){
					submitButtonActivation();
					selectedcheckboxlist.add(Integer.toString(scan.getId()));
				}else if(!isChecked && (selectedcheckboxlist.contains(Integer.toString(scan.getId())))) 
				{
					submitButtonDeactivation();
					selectedcheckboxlist.remove(Integer.toString(scan.getId()));


				}	
			}
		});

		sOther.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// TODO Auto-generated method stub
				if(activationflagvalue!=0 && FragmentActivityContainer.check_save==0)
				{
					FragmentActivityContainer.check_save=activationflagvalue;
					System.out.println("diabeties past checkbox working...");
				}
				if((  isChecked) && !(selectedcheckboxlist.contains(Integer.toString(sOther.getId())))){
					submitButtonActivation();
					selectedcheckboxlist.add(Integer.toString(sOther.getId()));
				}else if(!isChecked && (selectedcheckboxlist.contains(Integer.toString(sOther.getId())))) 
				{
					submitButtonDeactivation();
					selectedcheckboxlist.remove(Integer.toString(sOther.getId()));


				}	
			}
		});



		//Grandfather Major illness listener	

		gfhyper.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// TODO Auto-generated method stub
				if(activationflagvalue!=0 && FragmentActivityContainer.check_save==0)
				{
					FragmentActivityContainer.check_save=activationflagvalue;
					System.out.println("diabeties past checkbox working...");
				}
				if((  isChecked) && !(selectedcheckboxlist.contains(Integer.toString(gfhyper.getId())))){
					submitButtonActivation();
					selectedcheckboxlist.add(Integer.toString(gfhyper.getId()));
				}else if(!isChecked && (selectedcheckboxlist.contains(Integer.toString(gfhyper.getId())))) 
				{
					submitButtonDeactivation();
					selectedcheckboxlist.remove(Integer.toString(gfhyper.getId()));


				}	
			}
		});

		gfdia.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// TODO Auto-generated method stub
				if(activationflagvalue!=0 && FragmentActivityContainer.check_save==0)
				{
					FragmentActivityContainer.check_save=activationflagvalue;
					System.out.println("diabeties past checkbox working...");
				}
				if((  isChecked) && !(selectedcheckboxlist.contains(Integer.toString(gfdia.getId())))){
					submitButtonActivation();
					selectedcheckboxlist.add(Integer.toString(gfdia.getId()));
				}else if(!isChecked && (selectedcheckboxlist.contains(Integer.toString(gfdia.getId())))) 
				{
					submitButtonDeactivation();
					selectedcheckboxlist.remove(Integer.toString(gfdia.getId()));


				}	
			}
		});

		gfcan.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// TODO Auto-generated method stub
				if(activationflagvalue!=0 && FragmentActivityContainer.check_save==0)
				{
					FragmentActivityContainer.check_save=activationflagvalue;
					System.out.println("diabeties past checkbox working...");
				}
				if((  isChecked) && !(selectedcheckboxlist.contains(Integer.toString(gfcan.getId())))){
					submitButtonActivation();
					selectedcheckboxlist.add(Integer.toString(gfcan.getId()));
				}else if(!isChecked && (selectedcheckboxlist.contains(Integer.toString(gfcan.getId())))) 
				{
					submitButtonDeactivation();
					selectedcheckboxlist.remove(Integer.toString(gfcan.getId()));


				}		
			}
		});

		gfOther.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// TODO Auto-generated method stub
				if(activationflagvalue!=0 && FragmentActivityContainer.check_save==0)
				{
					FragmentActivityContainer.check_save=activationflagvalue;
					System.out.println("diabeties past checkbox working...");
				}
				if((  isChecked) && !(selectedcheckboxlist.contains(Integer.toString(gfOther.getId())))){
					submitButtonActivation();
					selectedcheckboxlist.add(Integer.toString(gfOther.getId()));
				}else if(!isChecked && (selectedcheckboxlist.contains(Integer.toString(gfOther.getId())))) 
				{
					submitButtonDeactivation();
					selectedcheckboxlist.remove(Integer.toString(gfOther.getId()));


				}		
			}
		});	

		//GrandMother Major Illness Listener

		gmhyper.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// TODO Auto-generated method stub
				if(activationflagvalue!=0 && FragmentActivityContainer.check_save==0)
				{
					FragmentActivityContainer.check_save=activationflagvalue;
					System.out.println("diabeties past checkbox working...");
				}
				if((  isChecked) && !(selectedcheckboxlist.contains(Integer.toString(gmhyper.getId())))){
					submitButtonActivation();
					selectedcheckboxlist.add(Integer.toString(gmhyper.getId()));
				}else if(!isChecked && (selectedcheckboxlist.contains(Integer.toString(gmhyper.getId())))) 
				{
					submitButtonDeactivation();
					selectedcheckboxlist.remove(Integer.toString(gmhyper.getId()));


				}		
			}
		});

		gmdia.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// TODO Auto-generated method stub
				if(activationflagvalue!=0 && FragmentActivityContainer.check_save==0)
				{
					FragmentActivityContainer.check_save=activationflagvalue;
					System.out.println("diabeties past checkbox working...");
				}
				if((  isChecked) && !(selectedcheckboxlist.contains(Integer.toString(gmdia.getId())))){
					submitButtonActivation();
					selectedcheckboxlist.add(Integer.toString(gmdia.getId()));
				}else if(!isChecked && (selectedcheckboxlist.contains(Integer.toString(gmdia.getId())))) 
				{
					submitButtonDeactivation();
					selectedcheckboxlist.remove(Integer.toString(gmdia.getId()));


				}		
			}
		});

		gmcan.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// TODO Auto-generated method stub
				if(activationflagvalue!=0 && FragmentActivityContainer.check_save==0)
				{
					FragmentActivityContainer.check_save=activationflagvalue;
					System.out.println("diabeties past checkbox working...");
				}
				if((  isChecked) && !(selectedcheckboxlist.contains(Integer.toString(gmcan.getId())))){
					submitButtonActivation();
					selectedcheckboxlist.add(Integer.toString(gmcan.getId()));
				}else if(!isChecked && (selectedcheckboxlist.contains(Integer.toString(gmcan.getId())))) 
				{
					submitButtonDeactivation();
					selectedcheckboxlist.remove(Integer.toString(gmcan.getId()));


				}	
			}
		});

		gmOther.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// TODO Auto-generated method stub
				if(activationflagvalue!=0 && FragmentActivityContainer.check_save==0)
				{
					FragmentActivityContainer.check_save=activationflagvalue;
					System.out.println("diabeties past checkbox working...");
				}
				if((  isChecked) && !(selectedcheckboxlist.contains(Integer.toString(gmOther.getId())))){
					submitButtonActivation();
					selectedcheckboxlist.add(Integer.toString(gmOther.getId()));
				}else if(!isChecked && (selectedcheckboxlist.contains(Integer.toString(gmOther.getId())))) 
				{
					submitButtonDeactivation();
					selectedcheckboxlist.remove(Integer.toString(gmOther.getId()));


				}	
			}
		});	


		//Social History Listener
		alcurr.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// TODO Auto-generated method stub
				if(activationflagvalue!=0 && FragmentActivityContainer.check_save==0)
				{
					FragmentActivityContainer.check_save=activationflagvalue;
					System.out.println("diabeties past checkbox working...");
				}
				if((  isChecked) && !(selectedcheckboxlist.contains(Integer.toString(alcurr.getId())))){
					submitButtonActivation();
					selectedcheckboxlist.add(Integer.toString(alcurr.getId()));
				}else if(!isChecked && (selectedcheckboxlist.contains(Integer.toString(alcurr.getId())))) 
				{
					submitButtonDeactivation();
					selectedcheckboxlist.remove(Integer.toString(alcurr.getId()));


				}
			}
		});		

		alpast.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// TODO Auto-generated method stub
				if(activationflagvalue!=0 && FragmentActivityContainer.check_save==0)
				{
					FragmentActivityContainer.check_save=activationflagvalue;
					System.out.println("diabeties past checkbox working...");
				}
				if((  isChecked) && !(selectedcheckboxlist.contains(Integer.toString(alpast.getId())))){
					submitButtonActivation();
					selectedcheckboxlist.add(Integer.toString(alpast.getId()));
				}else if(!isChecked && (selectedcheckboxlist.contains(Integer.toString(alpast.getId())))) 
				{
					submitButtonDeactivation();
					selectedcheckboxlist.remove(Integer.toString(alpast.getId()));


				}
			}
		});	


		tobcurr.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// TODO Auto-generated method stub
				if(activationflagvalue!=0 && FragmentActivityContainer.check_save==0)
				{
					FragmentActivityContainer.check_save=activationflagvalue;
					System.out.println("diabeties past checkbox working...");
				}
				if((  isChecked) && !(selectedcheckboxlist.contains(Integer.toString(tobcurr.getId())))){
					submitButtonActivation();
					selectedcheckboxlist.add(Integer.toString(tobcurr.getId()));
				}else if(!isChecked && (selectedcheckboxlist.contains(Integer.toString(tobcurr.getId())))) 
				{
					submitButtonDeactivation();
					selectedcheckboxlist.remove(Integer.toString(tobcurr.getId()));


				}
			}
		});	

		tobpast.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// TODO Auto-generated method stub
				if(activationflagvalue!=0 && FragmentActivityContainer.check_save==0)
				{
					FragmentActivityContainer.check_save=activationflagvalue;
					System.out.println("diabeties past checkbox working...");
				}
				if((  isChecked) && !(selectedcheckboxlist.contains(Integer.toString(tobpast.getId())))){
					submitButtonActivation();
					selectedcheckboxlist.add(Integer.toString(tobpast.getId()));
				}else if(!isChecked && (selectedcheckboxlist.contains(Integer.toString(tobpast.getId())))) 
				{
					submitButtonDeactivation();
					selectedcheckboxlist.remove(Integer.toString(tobpast.getId()));


				}
			}
		});	

		osubcurr.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// TODO Auto-generated method stub
				if(activationflagvalue!=0 && FragmentActivityContainer.check_save==0)
				{
					FragmentActivityContainer.check_save=activationflagvalue;
					System.out.println("diabeties past checkbox working...");
				}
				if((  isChecked) && !(selectedcheckboxlist.contains(Integer.toString(osubcurr.getId())))){
					submitButtonActivation();
					selectedcheckboxlist.add(Integer.toString(osubcurr.getId()));
				}else if(!isChecked && (selectedcheckboxlist.contains(Integer.toString(osubcurr.getId())))) 
				{
					submitButtonDeactivation();
					selectedcheckboxlist.remove(Integer.toString(osubcurr.getId()));


				}
			}
		});	

		osubpast.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// TODO Auto-generated method stub
				if(activationflagvalue!=0 && FragmentActivityContainer.check_save==0)
				{
					FragmentActivityContainer.check_save=activationflagvalue;
					System.out.println("diabeties past checkbox working...");
				}
				if((  isChecked) && !(selectedcheckboxlist.contains(Integer.toString(osubpast.getId())))){
					submitButtonActivation();
					selectedcheckboxlist.add(Integer.toString(osubpast.getId()));
				}else if(!isChecked && (selectedcheckboxlist.contains(Integer.toString(osubpast.getId())))) 
				{
					submitButtonDeactivation();
					selectedcheckboxlist.remove(Integer.toString(osubpast.getId()));


				}
			}
		});	




		/**New code.............................................*/

		addAlle.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub

				LayoutInflater inflater=LayoutInflater.from(getActivity());
				final View add_allergies=inflater.inflate(R.layout.add_more_allergies, null);
				final EditText et_allergy=(EditText)add_allergies.findViewById(R.id.etadd_more_allergey);
				final EditText et_reaction=(EditText)add_allergies.findViewById(R.id.etadd_more_reaction);
				TextView tvnumber=(TextView)add_allergies.findViewById(R.id.tvNumber);
				TextView tvReacnumber=(TextView)add_allergies.findViewById(R.id.tvReacNumber);
				RelativeLayout ivRemove=(RelativeLayout)add_allergies.findViewById(R.id.rlImagecross);


				ivRemove.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View arg0) {
						// TODO Auto-generated method stub
						if(et_allergy.getText().length()!=0 || et_reaction.getText().length()!=0){
							submitButtonDeactivation();
						}
						System.out.println("workinggggggggggg");
						View removeView=(LinearLayout)add_allergies.findViewById(R.id.llhidden_layout);
						ViewGroup parent = (ViewGroup) removeView.getParent();
						parent.removeView(removeView);
						int position = Integer.valueOf(removeView.getTag().toString()); 			     
						Log.e("delete position ", ""+ position);
						if(position==(medicationlist.size()+2))
							j--;
						medicationlist.remove(et_allergy);
						reactionlist.remove(et_reaction);	


					}
				});

				System.out.println("medicationlist size............."+medicationlist.size());



				et_allergy.addTextChangedListener(new TextWatcher() {

					@Override
					public void onTextChanged(CharSequence s, int start, int before, int count) {
						// TODO Auto-generated method stub



						if(s.length()==0){
							submitButtonActivation();
						}else if(start==0 && before==0 && count==1){
							submitButtonActivation();
						}
					}

					@Override
					public void beforeTextChanged(CharSequence s, int start, int count,
							int after) {
						// TODO Auto-generated method stub

					}

					@Override
					public void afterTextChanged(Editable s) {
						// TODO Auto-generated method stub
						/*if(s.length()==0){
								submitButtonDeactivation();
							}else{
								submitButtonActivation();
							}*/
					}
				});
				et_reaction.addTextChangedListener(new TextWatcher() {

					@Override
					public void onTextChanged(CharSequence s, int start, int before, int count) {
						// TODO Auto-generated method stub
						System.out.println("reaction working..."+start+before+count+s.length());
						if(s.length()==0){

							submitButtonActivation();
						}else if(start==0 && before==0 && count==1){
							submitButtonActivation();
						}
					}

					@Override
					public void beforeTextChanged(CharSequence s, int start, int count,
							int after) {
						// TODO Auto-generated method stub

					}

					@Override
					public void afterTextChanged(Editable s) {
						// TODO Auto-generated method stub
						/*if(s.length()==0){
								submitButtonDeactivation();
							}else{
								submitButtonActivation();
							}*/
					}
				});

				medicationlist.add(et_allergy);
				reactionlist.add(et_reaction);
				//cross.add(ivRemove);
				add_allergies.setTag((int)j);
				rl.addView(add_allergies);
				tvnumber.setText(Integer.toString(j)+". ");
				tvReacnumber.setText(Integer.toString(j)+". ");
				j++;




			}
		});



		/**New code.............................................*/

		addSurge.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub


				LayoutInflater inflater=LayoutInflater.from(getActivity());
				final View add_allsurgeries=inflater.inflate(R.layout.add_more_surgeries, null);

				final EditText et_surgery1=(EditText)add_allsurgeries.findViewById(R.id.etadd_more_surgery_one);
				final EditText et_surgery2=(EditText)add_allsurgeries.findViewById(R.id.etadd_more_surgery_two);
				TextView tv_sugnumber=(TextView)add_allsurgeries.findViewById(R.id.tvSugNumber);
				TextView tv_sugnumber1=(TextView)add_allsurgeries.findViewById(R.id.tvSugNumber1);
				RelativeLayout ivRemove=(RelativeLayout)add_allsurgeries.findViewById(R.id.rlImagecross);


				ivRemove.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View arg0) {
						// TODO Auto-generated method stub
						System.out.println("workinggggggggggg");
						if(et_surgery1.getText().length()!=0 || et_surgery2.getText().length()!=0){
							submitButtonDeactivation();
							System.out.println("deactivation in add sug working.........");
						}
						View removeView=(LinearLayout)add_allsurgeries.findViewById(R.id.llAddSurgeries);
						ViewGroup parent = (ViewGroup) removeView.getParent();
						parent.removeView(removeView);
						int position = Integer.valueOf(removeView.getTag().toString()); 			     
						Log.e("delete position ", ""+ position);
						System.out.println("surgery firstlist size"+surgeryfirstlist.size());
						if(position==(surgeryfirstlist.size()+1)){
							i--;
							surgindex1-=2;
							surgindex2-=2;

						}
						surgeryfirstlist.remove(et_surgery1);
						surgerysecondlist.remove(et_surgery2);	 


					}
				});

				et_surgery1.addTextChangedListener(new TextWatcher() {

					@Override
					public void onTextChanged(CharSequence s, int start, int before, int count) {
						// TODO Auto-generated method stub
						if(s.length()==0){
							submitButtonActivation();
						}else if(start==0 && before==0 && count==1){
							submitButtonActivation();
						}
					}

					@Override
					public void beforeTextChanged(CharSequence s, int start, int count,
							int after) {
						// TODO Auto-generated method stub

					}

					@Override
					public void afterTextChanged(Editable s) {
						// TODO Auto-generated method stub
						/*if(s.length()==0){
								submitButtonDeactivation();
							}else{
								submitButtonActivation();
							}*/
					}
				});

				et_surgery2.addTextChangedListener(new TextWatcher() {

					@Override
					public void onTextChanged(CharSequence s, int start, int before, int count) {
						// TODO Auto-generated method stub

						if(s.length()==0){
							submitButtonActivation();
						}else if(start==0 && before==0 && count==1){
							submitButtonActivation();
						}
					}

					@Override
					public void beforeTextChanged(CharSequence s, int start, int count,
							int after) {
						// TODO Auto-generated method stub

					}

					@Override
					public void afterTextChanged(Editable s) {
						// TODO Auto-generated method stub
						/*if(s.length()==0){
								submitButtonDeactivation();
							}else{
								submitButtonActivation();
							}*/
					}
				});

				surgeryfirstlist.add(et_surgery1);
				surgerysecondlist.add(et_surgery2);
				/*tv_sugnumber.setText(i+".");
					tv_sugnumber1.setText(i+".");*/

				tv_sugnumber.setText(surgindex1+".");
				tv_sugnumber1.setText(surgindex2+".");
				add_allsurgeries.setTag((int)i);
				//add_allsurgeries.setTag((int)j);
				llSurgery.addView(add_allsurgeries);
				i++;
				surgindex1+=2;
				surgindex2+=2;



			}
		});

		addMajor.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub

				LayoutInflater inflater=LayoutInflater.from(getActivity());
				final View add_allsurgeries=inflater.inflate(R.layout.add_more_majorillnesses, null);
				final EditText et_majorillness=(EditText)add_allsurgeries.findViewById(R.id.pmhcan);
				final CheckBox cb_current=(CheckBox)add_allsurgeries.findViewById(R.id.pmhcancurr);
				final CheckBox cb_past=(CheckBox)add_allsurgeries.findViewById(R.id.pmhcanpast);
				final CheckBox cb_na=(CheckBox)add_allsurgeries.findViewById(R.id.pmhcanna);
				TextView tv_number=(TextView)add_allsurgeries.findViewById(R.id.tvMajornumber);

				RelativeLayout ivRemove=(RelativeLayout)add_allsurgeries.findViewById(R.id.rlImagecross);

				ivRemove.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View arg0) {
						// TODO Auto-generated method stub
						if(et_majorillness.getText().length()!=0){
							submitButtonDeactivation();
						}
						System.out.println("workinggggggggggg");
						View removeView=(LinearLayout)add_allsurgeries.findViewById(R.id.llAddmajorillness);
						ViewGroup parent = (ViewGroup) removeView.getParent();
						parent.removeView(removeView);

						int position = Integer.valueOf(removeView.getTag().toString()); 			     
						Log.e("delete position ", ""+ position);
						if(position==(majorillnesslist.size()+3))
							k--;
						majorillnesslist.remove(et_majorillness);
						currentcblist.remove(cb_current);	
						pastcblist.remove(cb_past);
						nacblist.remove(cb_na);

					}
				});

				et_majorillness.addTextChangedListener(new TextWatcher() {

					@Override
					public void onTextChanged(CharSequence s, int start, int before, int count) {
						// TODO Auto-generated method stub

						if(s.length()==0){
							submitButtonActivation();
						}else if(start==0 && before==0 && count==1){
							submitButtonActivation();
						}
					}

					@Override
					public void beforeTextChanged(CharSequence s, int start, int count,
							int after) {
						// TODO Auto-generated method stub

					}

					@Override
					public void afterTextChanged(Editable s) {
						// TODO Auto-generated method stub
					}
				});

				cb_current.setOnCheckedChangeListener(new  OnCheckedChangeListener() {

					@Override
					public void onCheckedChanged(CompoundButton arg0, boolean isChecked) {
						// TODO Auto-generated method stub
						if(activationflagvalue!=0 && FragmentActivityContainer.check_save==0)
						{
							FragmentActivityContainer.check_save=activationflagvalue;
							System.out.println("diabeties past checkbox working...");
						}
						if(isChecked){
							cb_past.setChecked(false);
							cb_na.setChecked(false);
							cb_past.setEnabled(false);
							cb_na.setEnabled(false);
							noIllReport.setEnabled(false);
						}else{
							cb_past.setEnabled(true);
							cb_na.setEnabled(true);
							noIllReport.setEnabled(true);
						}

						if((  isChecked) && !(selectedcheckboxlist.contains(Integer.toString(cb_current.getId())))){
							submitButtonActivation();
							selectedcheckboxlist.add(Integer.toString(cb_current.getId()));
						}else if(!isChecked && (selectedcheckboxlist.contains(Integer.toString(cb_current.getId())))) 
						{
							submitButtonDeactivation();
							selectedcheckboxlist.remove(Integer.toString(cb_current.getId()));


						}


					}
				});


				cb_past.setOnCheckedChangeListener(new OnCheckedChangeListener() {

					@Override
					public void onCheckedChanged(CompoundButton arg0, boolean isChecked) {
						// TODO Auto-generated method stub
						if(activationflagvalue!=0 && FragmentActivityContainer.check_save==0)
						{
							FragmentActivityContainer.check_save=activationflagvalue;
							System.out.println("diabeties past checkbox working...");
						}
						if(isChecked){
							cb_current.setChecked(false);
							cb_na.setChecked(false);
							cb_current.setEnabled(false);
							cb_na.setEnabled(false);
							noIllReport.setEnabled(false);

						}else{
							cb_current.setEnabled(true);
							cb_na.setEnabled(true);

							noIllReport.setEnabled(true);
						}
						if((  isChecked) && !(selectedcheckboxlist.contains(Integer.toString(cb_past.getId())))){
							submitButtonActivation();
							selectedcheckboxlist.add(Integer.toString(cb_past.getId()));
						}else if(!isChecked && (selectedcheckboxlist.contains(Integer.toString(cb_past.getId())))) 
						{
							submitButtonDeactivation();
							selectedcheckboxlist.remove(Integer.toString(cb_past.getId()));


						}

					}
				});

				cb_na.setOnCheckedChangeListener(new OnCheckedChangeListener() {

					@Override
					public void onCheckedChanged(CompoundButton arg0, boolean isChecked) {
						// TODO Auto-generated method stub
						if(activationflagvalue!=0 && FragmentActivityContainer.check_save==0)
						{
							FragmentActivityContainer.check_save=activationflagvalue;
							System.out.println("diabeties past checkbox working...");
						}
						if(isChecked){
							submitButtonActivation();
							cb_current.setEnabled(false);
							cb_past.setEnabled(false);
							cb_current.setChecked(false);
							cb_past.setChecked(false);
							noIllReport.setEnabled(false);

						}else{
							submitButtonDeactivation();
							cb_current.setEnabled(true);
							cb_past.setEnabled(true);
							noIllReport.setEnabled(true);

						}
						if((  isChecked) && !(selectedcheckboxlist.contains(Integer.toString(cb_na.getId())))){
							submitButtonActivation();
							selectedcheckboxlist.add(Integer.toString(cb_na.getId()));
						}else if(!isChecked && (selectedcheckboxlist.contains(Integer.toString(cb_na.getId())))) 
						{
							submitButtonDeactivation();
							selectedcheckboxlist.remove(Integer.toString(cb_na.getId()));


						}

					}
				});

				majorillnesslist.add(et_majorillness);

				currentcblist.add(cb_current);

				pastcblist.add(cb_past);

				nacblist.add(cb_na);

				//add_allsurgeries.setTag((int)j);
				add_allsurgeries.setTag((int)k);
				tv_number.setText(k+".");
				llMajorillness.addView(add_allsurgeries);
				k++;



			}
		});

		noAlerReport.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				//is chkIos checked?


				if (((CheckBox) v).isChecked()) {
					//Case 1
					medicOne.setEnabled(false);
					medicTwo.setEnabled(false);
					reactionOne.setEnabled(false);
					reactionTwo.setEnabled(false);
					addAlle.setEnabled(false);
					medicOne.setText("");
					medicTwo.setText("");
					reactionOne.setText("");
					reactionTwo.setText("");
					if(rl.isEnabled()){
						rl.removeAllViews();
					}
					System.out.println("noalerreport workingggggggggg");
					submitButtonActivation();
				}
				else {
					//case 2
					medicOne.setEnabled(true);
					medicTwo.setEnabled(true);
					reactionOne.setEnabled(true);
					reactionTwo.setEnabled(true);
					addAlle.setEnabled(true);

					submitButtonDeactivation();
				}


			}
		});


		noAlerReport.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// TODO Auto-generated method stub
				if(activationflagvalue!=0 && FragmentActivityContainer.check_save==0)
				{
					FragmentActivityContainer.check_save=activationflagvalue;
					System.out.println("diabeties past checkbox working...");
				}
				if((  isChecked) && !(selectedcheckboxlist.contains(Integer.toString(noAlerReport.getId())))){
					submitButtonActivation();
					selectedcheckboxlist.add(Integer.toString(noAlerReport.getId()));
				}else if(!isChecked && (selectedcheckboxlist.contains(Integer.toString(noAlerReport.getId())))) 
				{
					submitButtonDeactivation();
					selectedcheckboxlist.remove(Integer.toString(noAlerReport.getId()));


				}
			}
		});



		noIllReport.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				//is chkIos checked?
				if (((CheckBox) v).isChecked()) {
					//Case 1
					hyperCurr.setEnabled(false);
					hyperNa.setEnabled(false);
					hyperPast.setEnabled(false);

					diaCurr.setEnabled(false);
					diaNa.setEnabled(false);
					diaPast.setEnabled(false);

					canCurr.setEnabled(false);
					canNa.setEnabled(false);
					canPast.setEnabled(false);
					addMajor.setEnabled(false);

					diaCurr.setChecked(false);
					diaNa.setChecked(false);
					diaPast.setChecked(false);

					hyperCurr.setChecked(false);
					hyperNa.setChecked(false);
					hyperPast.setChecked(false);

					canCurr.setChecked(false);
					canNa.setChecked(false);
					canPast.setChecked(false);

					llMajorillness.removeAllViews();

					submitButtonActivation();

				}
				else {
					hyperCurr.setEnabled(true);
					hyperNa.setEnabled(true);
					hyperPast.setEnabled(true);
					diaCurr.setEnabled(true);
					diaNa.setEnabled(true);
					diaPast.setEnabled(true);
					canCurr.setEnabled(true);
					canNa.setEnabled(true);
					canPast.setEnabled(true);
					addMajor.setEnabled(true);

					submitButtonDeactivation();

				}


			}
		});


		noIllReport.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// TODO Auto-generated method stub
				if(activationflagvalue!=0 && FragmentActivityContainer.check_save==0)
				{
					FragmentActivityContainer.check_save=activationflagvalue;
					System.out.println("diabeties past checkbox working...");
				}
				if((  isChecked) && !(selectedcheckboxlist.contains(Integer.toString(noIllReport.getId())))){
					submitButtonActivation();
					selectedcheckboxlist.add(Integer.toString(noIllReport.getId()));
				}else if(!isChecked && (selectedcheckboxlist.contains(Integer.toString(noIllReport.getId())))) 
				{
					submitButtonDeactivation();
					selectedcheckboxlist.remove(Integer.toString(noIllReport.getId()));


				}
			}
		});


		noSurgeryReport.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				//is chkIos checked?
				if (((CheckBox) v).isChecked()) {
					//Case 1
					surgeryOne.setEnabled(false);
					surgeryTwo.setEnabled(false);
					addSurge.setEnabled(false);
					llSurgery.removeAllViews();
					surgeryOne.setText("");
					surgeryTwo.setText("");
					b=1;

					submitButtonActivation();

				}
				else {
					//case 2
					surgeryOne.setEnabled(true);
					surgeryTwo.setEnabled(true);
					addSurge.setEnabled(true);

					submitButtonDeactivation();
				}


			}
		});



		noSurgeryReport.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// TODO Auto-generated method stub
				if(activationflagvalue!=0 && FragmentActivityContainer.check_save==0)
				{
					FragmentActivityContainer.check_save=activationflagvalue;
					System.out.println("diabeties past checkbox working...");
				}
				if((  isChecked) && !(selectedcheckboxlist.contains(Integer.toString(noSurgeryReport.getId())))){
					submitButtonActivation();
					selectedcheckboxlist.add(Integer.toString(noSurgeryReport.getId()));
				}else if(!isChecked && (selectedcheckboxlist.contains(Integer.toString(noSurgeryReport.getId())))) 
				{
					submitButtonDeactivation();
					selectedcheckboxlist.remove(Integer.toString(noSurgeryReport.getId()));


				}
			}
		});

		//second
		fna.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				//is chkIos checked?
				if (((CheckBox) v).isChecked()) {
					//Case 1
					fhyper.setEnabled(false);
					fdia.setEnabled(false);
					fcan.setEnabled(false);
					fOther.setEnabled(false);

					if(fhyper.isChecked())
						fhyper.toggle();
					if(fdia.isChecked())
						fdia.toggle();
					if(fcan.isChecked())
						fcan.toggle();
					if(fOther.isChecked())
						fOther.toggle();
					fother.setText("");



				}
				else {
					//case 2
					fhyper.setEnabled(true);
					fdia.setEnabled(true);
					fcan.setEnabled(true);
					fOther.setEnabled(true);


				}


			}
		});

		fna.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// TODO Auto-generated method stub
				if((  isChecked) && !(selectedcheckboxlist.contains(Integer.toString(fna.getId())))){
					submitButtonActivation();
					selectedcheckboxlist.add(Integer.toString(fna.getId()));
				}else if(!isChecked && (selectedcheckboxlist.contains(Integer.toString(fna.getId())))) 
				{
					submitButtonDeactivation();
					selectedcheckboxlist.remove(Integer.toString(fna.getId()));


				}
			}
		});

		mna.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				//is chkIos checked?
				if (((CheckBox) v).isChecked()) {
					//Case 1
					mhyper.setEnabled(false);
					mdia.setEnabled(false);
					mcan.setEnabled(false);
					mOther.setEnabled(false);

					if(mhyper.isChecked())
						mhyper.toggle();
					if(mdia.isChecked())
						mdia.toggle();
					if(mcan.isChecked())
						mcan.toggle();
					if(mOther.isChecked())
						mOther.toggle();
					mother.setText("");



				}
				else {
					//case 2
					mhyper.setEnabled(true);
					mdia.setEnabled(true);
					mcan.setEnabled(true);
					mOther.setEnabled(true);


				}


			}
		});


		mna.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// TODO Auto-generated method stub
				if((  isChecked) && !(selectedcheckboxlist.contains(Integer.toString(mna.getId())))){
					submitButtonActivation();
					selectedcheckboxlist.add(Integer.toString(mna.getId()));
				}else if(!isChecked && (selectedcheckboxlist.contains(Integer.toString(mna.getId())))) 
				{
					submitButtonDeactivation();
					selectedcheckboxlist.remove(Integer.toString(mna.getId()));


				}
			}
		});


		sna.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				//is chkIos checked?
				if (((CheckBox) v).isChecked()) {
					//Case 1
					shyper.setEnabled(false);
					sdia.setEnabled(false);
					scan.setEnabled(false);
					sOther.setEnabled(false);


					if(shyper.isChecked())
						shyper.toggle();
					if(sdia.isChecked())
						sdia.toggle();
					if(scan.isChecked())
						scan.toggle();
					if(sOther.isChecked())
						sOther.toggle();
					sother.setText("");


				}
				else {
					//case 2
					shyper.setEnabled(true);
					sdia.setEnabled(true);
					scan.setEnabled(true);
					sOther.setEnabled(true);


				}


			}
		});


		sna.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// TODO Auto-generated method stub
				if((  isChecked) && !(selectedcheckboxlist.contains(Integer.toString(sna.getId())))){
					submitButtonActivation();
					selectedcheckboxlist.add(Integer.toString(sna.getId()));
				}else if(!isChecked && (selectedcheckboxlist.contains(Integer.toString(sna.getId())))) 
				{
					submitButtonDeactivation();
					selectedcheckboxlist.remove(Integer.toString(sna.getId()));


				}
			}
		});





		gfna.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				//is chkIos checked?
				if (((CheckBox) v).isChecked()) {
					//Case 1
					gfhyper.setEnabled(false);
					gfdia.setEnabled(false);
					gfcan.setEnabled(false);
					gfOther.setEnabled(false);

					if(gfhyper.isChecked())
						gfhyper.toggle();
					if(gfdia.isChecked())
						gfdia.toggle();
					if(gfcan.isChecked())
						gfcan.toggle();
					if(gfOther.isChecked())
						gfOther.toggle();
					gfother.setText("");


				}
				else {
					//case 2
					gfhyper.setEnabled(true);
					gfdia.setEnabled(true);
					gfcan.setEnabled(true);
					gfOther.setEnabled(true);


				}


			}
		});


		gfna.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// TODO Auto-generated method stub
				if((  isChecked) && !(selectedcheckboxlist.contains(Integer.toString(gfna.getId())))){
					submitButtonActivation();
					selectedcheckboxlist.add(Integer.toString(gfna.getId()));
				}else if(!isChecked && (selectedcheckboxlist.contains(Integer.toString(gfna.getId())))) 
				{
					submitButtonDeactivation();
					selectedcheckboxlist.remove(Integer.toString(gfna.getId()));


				}
			}
		});




		gmna.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				//is chkIos checked?
				if (((CheckBox) v).isChecked()) {
					//Case 1
					gmhyper.setEnabled(false);
					gmdia.setEnabled(false);
					gmcan.setEnabled(false);
					gmOther.setEnabled(false);


					if(gmhyper.isChecked())
						gmhyper.toggle();
					if(gmdia.isChecked())
						gmdia.toggle();
					if(gmcan.isChecked())
						gmcan.toggle();
					if(gmOther.isChecked())
						gmOther.toggle();
					gmother.setText("");



				}
				else {
					//case 2
					gmhyper.setEnabled(true);
					gmdia.setEnabled(true);
					gmcan.setEnabled(true);
					gmOther.setEnabled(true);


				}


			}
		});



		gmna.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// TODO Auto-generated method stub
				if((  isChecked) && !(selectedcheckboxlist.contains(Integer.toString(gmna.getId())))){
					submitButtonActivation();
					selectedcheckboxlist.add(Integer.toString(gmna.getId()));
				}else if(!isChecked && (selectedcheckboxlist.contains(Integer.toString(gmna.getId())))) 
				{
					submitButtonDeactivation();
					selectedcheckboxlist.remove(Integer.toString(gmna.getId()));


				}
			}
		});

		medicOne.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				if(s.length()==0){
					submitButtonActivation();
				}else if(start==0 && before==0 && count==1){
					submitButtonActivation();
				}
			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
					int arg3) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				/*if(s.length()==0){
						submitButtonDeactivation();
					}else{
						submitButtonActivation();
					}*/
			}
		});

		medicTwo.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub

				if(s.length()==0){
					submitButtonActivation();
				}else if(start==0 && before==0 && count==1){
					submitButtonActivation();
				}
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});

		reactionOne.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				if(s.length()==0){
					submitButtonActivation();
				}else if(start==0 && before==0 && count==1){
					submitButtonActivation();
				}
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});

		reactionTwo.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				if(s.length()==0){
					submitButtonActivation();
				}else if(start==0 && before==0 && count==1){
					submitButtonActivation();
				}
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				/*if(s.length()==0){
						submitButtonDeactivation();
					}else{
						submitButtonActivation();
					}*/
			}
		});

		surgeryOne.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub

				System.out.println("s.length"+s.length());
				if(s.length()==0){
					submitButtonActivation();
				}else if(start==0 && before==0 && count==1){
					System.out.println("hjjjjjjjjjj");
					submitButtonActivation();
				}
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				/*if(s.length()==0){
						submitButtonDeactivation();
					}else{
						submitButtonActivation();
					}*/
			}
		});

		surgeryTwo.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				if(s.length()==0){
					submitButtonActivation();
				}else if(start==0 && before==0 && count==1){
					submitButtonActivation();
				}
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				/*if(s.length()==0){
						submitButtonDeactivation();
					}else{
						submitButtonActivation();
					}*/
			}
		});

		SharedPreferences scanpidnew=getActivity().getSharedPreferences("NewPatID", 0);
		String scnewPid=scanpidnew.getString("NewID", null); 
		/*SharedPreferences scanpid1=getActivity().getSharedPreferences("ScannedPid", 0);
		String scPid=scanpid1.getString("scanpid", null);       
		System.out.println("scanpersonal id"+scnewPid);*/

		if(patientobject==null){
			if(scnewPid!=null){
				tv_patientid.setText(scnewPid);
				SharedPreferences fnname1=getActivity().getSharedPreferences("fnName", 0);
				tv_flname.setText(fnname1.getString("fNname", "FirstName"+" "+"LastName"));  
			}
		}else{


			SharedPreferences fnname1=getActivity().getSharedPreferences("fnName", 0);
			tv_flname.setText(fnname1.getString("fNname","FirstName"+" "+"LastName"));  
			tv_patientid.setText(patientobject.getString("patientID"));
		}

		container.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				hideKeyboard(v);
			}
		});
		llContainer.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				hideKeyboard(arg0);
			}
		});

		hyperCurr.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton arg0, boolean isChecked) {
				// TODO Auto-generated method stub
				if(activationflagvalue!=0 && FragmentActivityContainer.check_save==0)
				{
					FragmentActivityContainer.check_save=activationflagvalue;
					System.out.println("hyper checkbox working");
				}
				if((isChecked) && !(selectedcheckboxlist.contains(Integer.toString(hyperCurr.getId())))){

					submitButtonActivation();
					selectedcheckboxlist.add(Integer.toString(hyperCurr.getId()));
				}else if(!isChecked && (selectedcheckboxlist.contains(Integer.toString(hyperCurr.getId())))) 
				{
					submitButtonDeactivation();
					selectedcheckboxlist.remove(Integer.toString(hyperCurr.getId()));
				}
			}
		});

		hyperPast.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// TODO Auto-generated method stub
				if(activationflagvalue!=0 && FragmentActivityContainer.check_save==0)
				{
					FragmentActivityContainer.check_save=activationflagvalue;
					System.out.println("hyper past checkbox working...");
				}
				if((  isChecked) && !(selectedcheckboxlist.contains(Integer.toString(hyperPast.getId())))){
					submitButtonActivation();
					selectedcheckboxlist.add(Integer.toString(hyperPast.getId()));
				}else if(!isChecked && (selectedcheckboxlist.contains(Integer.toString(hyperPast.getId())))) 
				{
					submitButtonDeactivation();
					selectedcheckboxlist.remove(Integer.toString(hyperPast.getId()));


				}
			}
		});
		diaCurr.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// TODO Auto-generated method stub
				if(activationflagvalue!=0 && FragmentActivityContainer.check_save==0)
				{
					FragmentActivityContainer.check_save=activationflagvalue;
					System.out.println("diabeties checkbox working");
				}

				if((  isChecked) && !(selectedcheckboxlist.contains(Integer.toString(diaCurr.getId())))){
					submitButtonActivation();
					selectedcheckboxlist.add(Integer.toString(diaCurr.getId()));
				}else if(!isChecked && (selectedcheckboxlist.contains(Integer.toString(diaCurr.getId())))) 
				{
					submitButtonDeactivation();
					selectedcheckboxlist.remove(Integer.toString(diaCurr.getId()));


				}
			}
		});


		diaPast.setOnCheckedChangeListener(new OnCheckedChangeListener() {



			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if(activationflagvalue!=0 && FragmentActivityContainer.check_save==0)
				{
					FragmentActivityContainer.check_save=activationflagvalue;
					System.out.println("diabeties past checkbox working...");
				}
				// TODO Auto-generated method stub
				if((  isChecked) && !(selectedcheckboxlist.contains(Integer.toString(diaPast.getId())))){
					submitButtonActivation();
					selectedcheckboxlist.add(Integer.toString(diaPast.getId()));
				}else if(!isChecked && (selectedcheckboxlist.contains(Integer.toString(diaPast.getId())))) 
				{
					submitButtonDeactivation();
					selectedcheckboxlist.remove(Integer.toString(diaPast.getId()));


				}
			}
		});

		canCurr.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// TODO Auto-generated method stub
				if(activationflagvalue!=0 && FragmentActivityContainer.check_save==0)
				{
					FragmentActivityContainer.check_save=activationflagvalue;
					System.out.println("cancer check box working.");
				}

				if((  isChecked) && !(selectedcheckboxlist.contains(Integer.toString(canCurr.getId())))){
					submitButtonActivation();
					selectedcheckboxlist.add(Integer.toString(canCurr.getId()));
				}else if(!isChecked && (selectedcheckboxlist.contains(Integer.toString(canCurr.getId())))) 
				{
					submitButtonDeactivation();
					selectedcheckboxlist.remove(Integer.toString(canCurr.getId()));


				}
			}
		});

		canPast.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// TODO Auto-generated method stub
				if(activationflagvalue!=0 && FragmentActivityContainer.check_save==0)
				{
					FragmentActivityContainer.check_save=activationflagvalue;
					System.out.println("cancer past check box working");
				}
				if((isChecked) && !(selectedcheckboxlist.contains(Integer.toString(canPast.getId())))){
					submitButtonActivation();
					selectedcheckboxlist.add(Integer.toString(canPast.getId()));
					System.out.println("can past workinggggg");
				}else if(!isChecked && (selectedcheckboxlist.contains(Integer.toString(canPast.getId())))) 
				{
					submitButtonDeactivation();
					selectedcheckboxlist.remove(Integer.toString(canPast.getId()));


				}
			}
		});


		return view; 
	}

	@Override
	public void onResume() 
	{
		// TODO Auto-generated method stub
		super.onResume();
		CrashReporter.getInstance().trackScreenView("Patient Medical History");
		System.out.println("majorill array size"+majillArray.size());


		if(patientobject!=null )
		{

			String fname=patientobject.getString("firstName");
			String lname=patientobject.getString("lastName");
			if(fname==null){
				fname="FirstName";
			}
			if(lname==null){
				lname="LastName";
			}
			tv_flname.setText(fname +" "+lname);

			ar_in =  patientobject.getJSONArray("allergiesMedication");

			String[] myAnsArray = null;
			if(ar_in!=null){
				String result="";
				myAnsArray = new String[ar_in.length()];
				try {
					result=ar_in.getString(ar_in.length()-1);
					myAnsArray=result.split(",");
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				for(int j = 0; j < myAnsArray.length; j++){    
					Log.v("result--",  myAnsArray[j]);
				}

				if(myAnsArray[0].toString().matches("NA")){
					noAlerReport.setChecked(true);
				}else{
					noAlerReport.setChecked(false);
				}
			}

			ar_inreac =  patientobject.getJSONArray("allergiesReaction");
			System.out.println("allergiesreaction"+ar_inreac);
			String[] myAnsArrayreac = null;
			if(ar_inreac!=null){
				String result="";
				myAnsArrayreac = new String[ar_inreac.length()];
				try {
					result=ar_inreac.getString(ar_inreac.length()-1);
					myAnsArrayreac=result.split(",");

				} 
				catch (JSONException medicationlist) {
					// TODO Auto-generated catch block
					medicationlist.printStackTrace();
				}
				if((myAnsArrayreac[0].toString()).matches("NA")){

					noAlerReport.setChecked(true);
					medicOne.setEnabled(false);
					medicTwo.setEnabled(false);
					reactionOne.setEnabled(false);
					reactionTwo.setEnabled(false);
					addAlle.setEnabled(false);

				}
				else{

					noAlerReport.setChecked(false);
					medicOne.setEnabled(true);
					medicTwo.setEnabled(true);
					reactionOne.setEnabled(true);
					reactionTwo.setEnabled(true);

					if(myAnsArrayreac.length==2)
					{
						if(!(myAnsArray[0].toString().equals("Nil"))){
							medicOne.setText(myAnsArray[0].toString());	
						}
						if(!(myAnsArray[1].toString().equals("Nil"))){
							medicTwo.setText(myAnsArray[1].toString());
						}
						if(!(myAnsArrayreac[0].toString().equals("Nil"))){
							reactionOne.setText(myAnsArrayreac[0].toString());
						}
						if(!(myAnsArrayreac[1].toString().equals("Nil"))){
							reactionTwo.setText(myAnsArrayreac[1].toString());
						}
					}
					if(myAnsArrayreac.length==1){
						if(!(myAnsArray[0].toString().equals("Nil"))){
							medicOne.setText(myAnsArray[0].toString());
						}
						if(!(myAnsArrayreac[0].toString().equals("Nil"))){
							reactionOne.setText(myAnsArrayreac[0].toString());
						}
					}
					if((myAnsArrayreac.length)>2){

						if(!(myAnsArray[0].toString().equals("Nil"))){
							medicOne.setText(myAnsArray[0].toString());
						}
						if(!(myAnsArray[1].toString().equals("Nil"))){
							medicTwo.setText(myAnsArray[1].toString());
						}
						if(!(myAnsArrayreac[0].toString().equals("Nil"))){
							reactionOne.setText(myAnsArrayreac[0].toString());
						}
						if(!(myAnsArrayreac[1].toString().equals("Nil"))){
							reactionTwo.setText(myAnsArrayreac[1].toString());
						}
						medicindex=3;

						medicationlist.clear();
						reactionlist.clear();
						for(int k = 0; k < ((myAnsArrayreac.length)-2); k++){  

							System.out.println("for loop workinggggggg");

							//	rl =(LinearLayout)view.findViewById(R.id.pmhl33);
							LinearLayout layout2 = new LinearLayout(getActivity());
							LayoutInflater inflater_allergy=LayoutInflater.from(getActivity());
							final View add_allergies=inflater_allergy.inflate(R.layout.add_more_allergies, null);
							EditText et_allergy=(EditText)add_allergies.findViewById(R.id.etadd_more_allergey);
							EditText et_reaction=(EditText)add_allergies.findViewById(R.id.etadd_more_reaction);
							RelativeLayout iv_remove=(RelativeLayout)add_allergies.findViewById(R.id.rlImagecross);
							TextView tvnumber=(TextView)add_allergies.findViewById(R.id.tvNumber);
							TextView tvReacnumber=(TextView)add_allergies.findViewById(R.id.tvReacNumber);


							iv_remove.setOnClickListener(new OnClickListener() {

								@Override
								public void onClick(View arg0) {
									// TODO Auto-generated method stub
									View removeView=(LinearLayout)add_allergies.findViewById(R.id.llhidden_layout);
									ViewGroup parent = (ViewGroup) removeView.getParent();
									parent.removeView(removeView);
									int position = Integer.valueOf(removeView.getTag().toString()); 			     
									Log.e("delete position ", ""+ position);

									if(position==(medicationlist.size()+2))				
										medicindex--;
									/*medicationlist.remove(position-3);
									reactionlist.remove(position-3);*/

									medicationlist.set(position-3,null);
									reactionlist.set(position-3,null);
									submitButtonActivation();



								}
							});


							et_allergy.addTextChangedListener(new TextWatcher() {

								@Override
								public void onTextChanged(CharSequence s, int start, int before, int count) {
									// TODO Auto-generated method stub
									if(s.length()==0){
										submitButtonActivation();
									}else if(start==0 && before==0 && count==1){
										submitButtonActivation();
									}
								}

								@Override
								public void beforeTextChanged(CharSequence s, int start, int count,
										int after) {
									// TODO Auto-generated method stub

								}

								@Override
								public void afterTextChanged(Editable s) {
									// TODO Auto-generated method stub
									/*if(s.length()==0){
											submitButtonDeactivation();
										}else{
											submitButtonActivation();
										}*/
								}
							});
							et_reaction.addTextChangedListener(new TextWatcher() {

								@Override
								public void onTextChanged(CharSequence s, int start, int before, int count) {
									// TODO Auto-generated method stub
									if(s.length()==0){
										submitButtonActivation();
									}else if(start==0 && before==0 && count==1){
										submitButtonActivation();
									}
								}

								@Override
								public void beforeTextChanged(CharSequence s, int start, int count,
										int after) {
									// TODO Auto-generated method stub

								}

								@Override
								public void afterTextChanged(Editable s) {
									// TODO Auto-generated method stub
									/*if(s.length()==0){
											submitButtonDeactivation();
										}else{
											submitButtonActivation();
										}*/
								}
							});







							medicationlist.add(et_allergy);
							reactionlist.add(et_reaction);

							if(patientedit_checkflag==false){
								et_allergy.setEnabled(false);
								et_reaction.setEnabled(false);
								iv_remove.setEnabled(false);
							}
							System.out.println("medicationlist size"+medicationlist.size()+"medicindex3 value"+medicindex);
							System.out.println("reactionlist size"+reactionlist.size());

							if(!(myAnsArray[k+2].toString().equals("Nil"))){
								et_allergy.setText(myAnsArray[k+2].toString());
							}
							if(!(myAnsArrayreac[k+2].toString().equals("Nil"))){
								et_reaction.setText(myAnsArrayreac[k+2].toString());
							}

							add_allergies.setTag((int)medicindex);

							rl.addView(add_allergies);
							tvnumber.setText(Integer.toString(medicindex)+". ");
							tvReacnumber.setText(Integer.toString(medicindex)+". ");
							medicindex++;
						}


					}

				}



			}

			ar_in1 = patientobject.getJSONArray("majorIllnessStatus");
			ar_in2=patientobject.getJSONArray("majorIllness");
			String[] myAnsArray1 = null;
			String[] major_illness_Array=null;
			String result;
			String statusresult;
			System.out.println("majorIllnessStatus"+ar_in1);
			System.out.println("majorIllness"+ar_in2);
			if(ar_in1!=null){
				myAnsArray1 = new String[ar_in1.length()];
				major_illness_Array=new String[ar_in2.length()];
				try {
					result=ar_in1.getString(ar_in1.length()-1);
					statusresult=ar_in2.getString(ar_in2.length()-1);
					myAnsArray1=result.split(",");
					major_illness_Array=statusresult.split(",");
				} 
				catch (JSONException medicationlist) {
					// TODO Auto-generated catch block
					medicationlist.printStackTrace();
				}

				boolean flag=true;
				for(int i=0;i<myAnsArray1.length;i++){
					System.out.println("myansarray flag"+myAnsArray1[i]);
					if(myAnsArray1[i].toString().equals("Current")||myAnsArray1[i].toString().equals("Past")){
						flag=false;
					}

					if(myAnsArray1[i].contains("&")){
						flag=false;
					}
				}


				if(flag==true){
					System.out.println("flag true workingggggggg");
					noIllReport.setChecked(true);
					noIllReport.setEnabled(true);

					hyperCurr.setChecked(false);
					hyperPast.setChecked(false);
					hyperNa.setChecked(false);

					diaCurr.setChecked(false);
					diaPast.setChecked(false);
					diaNa.setChecked(false);

					canCurr.setChecked(false);
					canPast.setChecked(false);
					canNa.setChecked(false);

					hyperCurr.setEnabled(false);
					hyperPast.setEnabled(false);
					hyperNa.setEnabled(false);
					diaCurr.setEnabled(false);
					diaPast.setEnabled(false);
					diaNa.setEnabled(false);
					canCurr.setEnabled(false);
					canPast.setEnabled(false);
					canNa.setEnabled(false);
					addMajor.setEnabled(false);
				}
				else
				{

					hyperCurr.setEnabled(true);
					hyperPast.setEnabled(true);
					hyperNa.setEnabled(true);
					diaCurr.setEnabled(true);
					diaPast.setEnabled(true);
					diaNa.setEnabled(true);
					canCurr.setEnabled(true);
					canPast.setEnabled(true);
					canNa.setEnabled(true);

					noIllReport.setChecked(false);

					if(!myAnsArray1[0].contains("&")){

						if(myAnsArray1[0].toString().equals("Current")){

							hyperCurr.setChecked(true);
							hyperPast.setChecked(false);
							hyperNa.setChecked(false);
							hyperPast.setEnabled(false);
							hyperNa.setEnabled(false);

						}
						if(myAnsArray1[0].toString().equals("Past")){

							hyperPast.setChecked(true);
							hyperCurr.setChecked(false);
							hyperNa.setChecked(false);
							hyperCurr.setEnabled(false);
							hyperNa.setEnabled(false);
						}
						if(myAnsArray1[0].toString().equals("N/A")){

							hyperNa.setChecked(true);
							hyperPast.setChecked(false);
							hyperCurr.setChecked(false);
							hyperPast.setEnabled(false);
							hyperCurr.setEnabled(false);
						}
					}else{

						String status[]=myAnsArray1[0].split("&");

						if(status[0].toString().equals("Current")){

							hyperCurr.setChecked(true);
							hyperPast.setChecked(false);
							hyperNa.setChecked(false);
							hyperPast.setEnabled(false);
							hyperNa.setEnabled(false);

						}
						if(status[0].toString().equals("Past")){

							hyperPast.setChecked(true);
							hyperCurr.setChecked(false);
							hyperNa.setChecked(false);
							hyperCurr.setEnabled(false);
							hyperNa.setEnabled(false);
						}
						if(status[0].toString().equals("N/A")){

							hyperNa.setChecked(true);
							hyperPast.setChecked(false);
							hyperCurr.setChecked(false);
							hyperPast.setEnabled(false);
							hyperCurr.setEnabled(false);
						}					
					}


					if(!myAnsArray1[1].contains("&")){

						if(myAnsArray1[1].toString().equals("Current")){

							diaCurr.setChecked(true);
							diaPast.setChecked(false);
							diaNa.setChecked(false);
							diaPast.setEnabled(false);
							diaNa.setEnabled(false);
						}
						if(myAnsArray1[1].toString().equals("Past")){

							diaPast.setChecked(true);
							diaCurr.setChecked(false);
							diaNa.setChecked(false);
							diaCurr.setEnabled(false);
							diaNa.setEnabled(false);
						}
						if(myAnsArray1[1].toString().equals("N/A")){

							diaNa.setChecked(true);
							diaCurr.setChecked(false);
							diaPast.setChecked(false);
							diaCurr.setEnabled(false);
							diaPast.setEnabled(false);
						}
					}else{

						String status[]=myAnsArray1[1].split("&");

						if(status[0].toString().equals("Current")){

							diaCurr.setChecked(true);
							diaPast.setChecked(false);
							diaNa.setChecked(false);
							diaPast.setEnabled(false);
							diaNa.setEnabled(false);
						}
						if(status[0].toString().equals("Past")){

							diaPast.setChecked(true);
							diaCurr.setChecked(false);
							diaNa.setChecked(false);
							diaCurr.setEnabled(false);
							diaNa.setEnabled(false);
						}
						if(status[0].toString().equals("N/A")){

							diaNa.setChecked(true);
							diaCurr.setChecked(false);
							diaPast.setChecked(false);
							diaCurr.setEnabled(false);
							diaPast.setEnabled(false);
						}

					}



					if(myAnsArray1[2].toString().equals("Current")){

						canCurr.setChecked(true);
						canPast.setChecked(false);
						canNa.setChecked(false);
						canPast.setEnabled(false);
						canNa.setEnabled(false);
					}
					if(myAnsArray1[2].toString().equals("Past")){

						canPast.setChecked(true);
						canCurr.setChecked(false);
						canNa.setChecked(false);
						canCurr.setEnabled(false);
						canNa.setEnabled(false);
					}
					if(myAnsArray1[2].toString().equals("N/A")){

						canNa.setChecked(true);
						canCurr.setChecked(false);
						canPast.setChecked(false);
						canCurr.setEnabled(false);
						canPast.setEnabled(false);
					}


					if(myAnsArray1.length>3)
					{

						majorindex=4;

						majorillnesslist.clear();
						currentcblist.clear();
						pastcblist.clear();
						nacblist.clear();

						for(int k = 0; k < ((myAnsArray1.length)-3); k++)
						{  

							LayoutInflater inflater_illness=LayoutInflater.from(getActivity());
							final View add_allmajor=inflater_illness.inflate(R.layout.add_more_majorillnesses, null);
							EditText et_majorillness=(EditText)add_allmajor.findViewById(R.id.pmhcan);
							final CheckBox cb_current=(CheckBox)add_allmajor.findViewById(R.id.pmhcancurr);
							final CheckBox cb_past=(CheckBox)add_allmajor.findViewById(R.id.pmhcanpast);
							final CheckBox cb_na=(CheckBox)add_allmajor.findViewById(R.id.pmhcanna);
							TextView tv_number=(TextView)add_allmajor.findViewById(R.id.tvMajornumber);
							RelativeLayout ivRemove=(RelativeLayout)add_allmajor.findViewById(R.id.rlImagecross);


							ivRemove.setOnClickListener(new OnClickListener() {

								@Override
								public void onClick(View arg0) {
									// TODO Auto-generated method stub
									System.out.println("workinggggggggggg");
									View removeView=(LinearLayout)add_allmajor.findViewById(R.id.llAddmajorillness);
									ViewGroup parent = (ViewGroup) removeView.getParent();
									parent.removeView(removeView);

									int position = Integer.valueOf(removeView.getTag().toString()); 			     
									Log.e("delete position ", ""+ position);
									if(position==(majorillnesslist.size()+3))
										majorindex--;
									/*majorillnesslist.remove(position-4);
									currentcblist.remove(position-4);	
									pastcblist.remove(position-4);
									nacblist.remove(position-4);*/

									majorillnesslist.set(position-4,null);
									currentcblist.set(position-4,null);	
									pastcblist.set(position-4,null);
									nacblist.set(position-4,null);



								}
							});





							selectedcheckboxlist.remove(Integer.toString(cb_current.getId()));
							selectedcheckboxlist.remove(Integer.toString(cb_past.getId()));
							selectedcheckboxlist.remove(Integer.toString(cb_na.getId()));

							et_majorillness.addTextChangedListener(new TextWatcher() {

								@Override
								public void onTextChanged(CharSequence s, int start, int before, int count) {
									// TODO Auto-generated method stub

									if(s.length()==0){
										submitButtonActivation();
									}else if(start==0 && before==0 && count==1){
										submitButtonActivation();
									}
								}

								@Override
								public void beforeTextChanged(CharSequence s, int start, int count,
										int after) {
									// TODO Auto-generated method stub

								}

								@Override
								public void afterTextChanged(Editable s) {
									// TODO Auto-generated method stub
								}
							});




							cb_current.setOnCheckedChangeListener(new  OnCheckedChangeListener() {

								@Override
								public void onCheckedChanged(CompoundButton arg0, boolean isChecked) {
									// TODO Auto-generated method stub
									if(isChecked){

										cb_past.setChecked(false);
										cb_na.setChecked(false);
										cb_past.setEnabled(false);
										cb_na.setEnabled(false);
										noIllReport.setEnabled(false);
									}else{
										cb_past.setEnabled(true);
										cb_na.setEnabled(true);
										noIllReport.setEnabled(true);
									}

									if((  isChecked) && !(selectedcheckboxlist.contains(Integer.toString(cb_current.getId())))){
										submitButtonActivation();
										selectedcheckboxlist.add(Integer.toString(cb_current.getId()));
									}else if(!isChecked && (selectedcheckboxlist.contains(Integer.toString(cb_current.getId())))) 
									{
										submitButtonDeactivation();
										selectedcheckboxlist.remove(Integer.toString(cb_current.getId()));


									}


								}
							});


							cb_past.setOnCheckedChangeListener(new OnCheckedChangeListener() {

								@Override
								public void onCheckedChanged(CompoundButton arg0, boolean isChecked) {
									// TODO Auto-generated method stub

									if(isChecked){

										cb_current.setChecked(false);
										cb_na.setChecked(false);
										cb_current.setEnabled(false);
										cb_na.setEnabled(false);
										noIllReport.setEnabled(false);

									}else{
										cb_current.setEnabled(true);
										cb_na.setEnabled(true);

										noIllReport.setEnabled(true);
									}
									if((  isChecked) && !(selectedcheckboxlist.contains(Integer.toString(cb_past.getId())))){
										submitButtonActivation();
										selectedcheckboxlist.add(Integer.toString(cb_past.getId()));
									}else if(!isChecked && (selectedcheckboxlist.contains(Integer.toString(cb_past.getId())))) 
									{
										submitButtonDeactivation();
										selectedcheckboxlist.remove(Integer.toString(cb_past.getId()));


									}

								}
							});

							cb_na.setOnClickListener(new OnClickListener() {

								@Override
								public void onClick(View v) {
									// TODO Auto-generated method stub

								}
							});

							cb_na.setOnCheckedChangeListener(new OnCheckedChangeListener() {

								@Override
								public void onCheckedChanged(CompoundButton arg0, boolean isChecked) {
									// TODO Auto-generated method stub

									if(isChecked){
										submitButtonActivation();
										cb_current.setEnabled(false);
										cb_past.setEnabled(false);
										cb_current.setChecked(false);
										cb_past.setChecked(false);
										noIllReport.setEnabled(false);

									}else{
										submitButtonDeactivation();
										cb_current.setEnabled(true);
										cb_past.setEnabled(true);
										noIllReport.setEnabled(true);

									}
									if((  isChecked) && !(selectedcheckboxlist.contains(Integer.toString(cb_na.getId())))){
										submitButtonActivation();
										selectedcheckboxlist.add(Integer.toString(cb_na.getId()));
									}else if(!isChecked && (selectedcheckboxlist.contains(Integer.toString(cb_na.getId())))) 
									{
										submitButtonDeactivation();
										selectedcheckboxlist.remove(Integer.toString(cb_na.getId()));


									}

								}
							});








							et_majorillness.setText(major_illness_Array[k+3].toString());


							majorillnesslist.add(et_majorillness);
							currentcblist.add(cb_current);
							pastcblist.add(cb_past);
							nacblist.add(cb_na);


							if(patientedit_checkflag==false){
								et_majorillness.setEnabled(false);
								cb_current.setEnabled(false);
								cb_past.setEnabled(false);
								cb_na.setEnabled(false);
								ivRemove.setEnabled(false);
							}


							if(!(myAnsArray1[k+3].toString().contains("&"))){

								if(myAnsArray1[k+3].toString().equals("Current")){

									cb_current.setChecked(true);
									cb_past.setChecked(false);
									cb_na.setChecked(false);
									cb_past.setEnabled(false);
									cb_na.setEnabled(false);
								}
							}else{
								String status[]=myAnsArray1[k+3].split("&");


								if(status[0].toString().equals("Current")){

									cb_current.setChecked(true);
									cb_past.setChecked(false);
									cb_na.setChecked(false);
									cb_past.setEnabled(false);
									cb_na.setEnabled(false);
								}


							}

							if(!(myAnsArray1[k+3].toString().contains("&"))){

								if(myAnsArray1[k+3].toString().equals("Past")){
									cb_past.setChecked(true);
									cb_current.setChecked(false);
									cb_na.setChecked(false);
									cb_current.setEnabled(false);
									cb_na.setEnabled(false);
								}
							}else{


								String status[]=myAnsArray1[k+3].split("&");


								if(status[0].toString().equals("Past")){

									cb_past.setChecked(true);
									cb_current.setChecked(false);
									cb_na.setChecked(false);
									cb_current.setEnabled(false);
									cb_na.setEnabled(false);
								}

							}

							if(!(myAnsArray1[k+3].toString().contains("&"))){
								if(myAnsArray1[k+3].toString().equals("N/A")){

									cb_na.setChecked(true);
									cb_current.setChecked(false);
									cb_past.setChecked(false);
									cb_current.setEnabled(false);
									cb_past.setEnabled(false);
								}	
							}else{

								String status[]=myAnsArray1[k+3].split("&");


								if(status[0].toString().equals("N/A")){

									cb_na.setChecked(true);
									cb_current.setChecked(false);
									cb_past.setChecked(false);
									cb_current.setEnabled(false);
									cb_past.setEnabled(false);
								}


							}
							add_allmajor.setTag((int)majorindex);
							tv_number.setText(majorindex+".");
							llMajorillness.addView(add_allmajor);
							majorindex++;

						}
					}


				}

			}

			ar_insur =  patientobject.getJSONArray("Surgeries");
			String[] myAnsArraysur = null;
			if(ar_insur!=null){
				String surgresult="";
				myAnsArraysur = new String[ar_insur.length()];
				try {
					surgresult=ar_insur.getString(ar_insur.length()-1);
					myAnsArraysur=surgresult.split(",");
				} 
				catch (JSONException medicationlist) {         
					// TODO Auto-generated catch block
					medicationlist.printStackTrace();
				}
				if((myAnsArraysur[0].toString()).matches("NA")){

					noSurgeryReport.setChecked(true);
					surgeryOne.setEnabled(false);
					surgeryTwo.setEnabled(false);
					addSurge.setEnabled(false);

				}
				else{
					noSurgeryReport.setChecked(false);
					if(!(myAnsArraysur[0].toString().equals("Nil"))){
						surgeryOne.setText(myAnsArraysur[0].toString());
					}
					if(!(myAnsArraysur[1].toString().equals("Nil"))){
						surgeryTwo.setText(myAnsArraysur[1].toString());
					}

					if(myAnsArraysur.length>2){
						surgindex=2;
						surgeryfirstlist.clear();
						surgerysecondlist.clear();

						for(int k = 0; k < ((myAnsArraysur.length)-2); k+=2)
						{  

							LayoutInflater inflater_surgery=LayoutInflater.from(getActivity());
							final View add_allsurgeries=inflater_surgery.inflate(R.layout.add_more_surgeries, null);

							EditText et_surgery1=(EditText)add_allsurgeries.findViewById(R.id.etadd_more_surgery_one);
							EditText et_surgery2=(EditText)add_allsurgeries.findViewById(R.id.etadd_more_surgery_two);
							TextView tv_sugnumber=(TextView)add_allsurgeries.findViewById(R.id.tvSugNumber);
							TextView tv_sugnumber1=(TextView)add_allsurgeries.findViewById(R.id.tvSugNumber1);

							RelativeLayout ivRemove=(RelativeLayout)add_allsurgeries.findViewById(R.id.rlImagecross);
							ivRemove.setOnClickListener(new OnClickListener() {

								@Override
								public void onClick(View arg0) {
									// TODO Auto-generated method stub
									System.out.println("workinggggggggggg");
									View removeView=(LinearLayout)add_allsurgeries.findViewById(R.id.llAddSurgeries);
									ViewGroup parent = (ViewGroup) removeView.getParent();
									parent.removeView(removeView);
									int position = Integer.valueOf(removeView.getTag().toString()); 			     
									Log.e("delete position ", ""+ position);
									if(position==(surgeryfirstlist.size()+1))
										surgindex--;
									/*surgeryfirstlist.remove(position-2);
									surgerysecondlist.remove(position-2);	*/ 

									surgeryfirstlist.set(position-2, null);
									surgerysecondlist.set(position-2, null);
									submitButtonActivation();

								}
							});


							et_surgery1.addTextChangedListener(new TextWatcher() {

								@Override
								public void onTextChanged(CharSequence s, int start, int before, int count) {
									// TODO Auto-generated method stub
									if(s.length()==0){
										submitButtonActivation();
									}else if(start==0 && before==0 && count==1){
										submitButtonActivation();
									}
								}

								@Override
								public void beforeTextChanged(CharSequence s, int start, int count,
										int after) {
									// TODO Auto-generated method stub

								}

								@Override
								public void afterTextChanged(Editable s) {
									// TODO Auto-generated method stub
									/*if(s.length()==0){
											submitButtonDeactivation();
										}else{
											submitButtonActivation();
										}*/
								}
							});

							et_surgery2.addTextChangedListener(new TextWatcher() {

								@Override
								public void onTextChanged(CharSequence s, int start, int before, int count) {
									// TODO Auto-generated method stub

									if(s.length()==0){
										submitButtonActivation();
									}else if(start==0 && before==0 && count==1){
										submitButtonActivation();
									}
								}

								@Override
								public void beforeTextChanged(CharSequence s, int start, int count,
										int after) {
									// TODO Auto-generated method stub

								}

								@Override
								public void afterTextChanged(Editable s) {
									// TODO Auto-generated method stub
									/*if(s.length()==0){
											submitButtonDeactivation();
										}else{
											submitButtonActivation();
										}*/
								}
							});



							surgeryfirstlist.add(et_surgery1);
							surgerysecondlist.add(et_surgery2);

							if(patientedit_checkflag==false){
								ivRemove.setEnabled(false);
								et_surgery1.setEnabled(false);
								et_surgery2.setEnabled(false);
							}

							if(!(myAnsArraysur[k+2].toString().equals("Nil"))){
								et_surgery1.setText(myAnsArraysur[k+2].toString());
							}
							if(!(myAnsArraysur[k+2].toString().equals("Nil"))){
								et_surgery2.setText(myAnsArraysur[k+3].toString());
							}

							tv_sugnumber.setText(surgindex+".");
							tv_sugnumber1.setText(surgindex+".");
							add_allsurgeries.setTag((int)surgindex);
							llSurgery.addView(add_allsurgeries);
							surgindex++;





						}
					}
				}
			}


			ar_infam =  patientobject.getJSONArray("majorIllnessFamilyStatus");
			String[] myAnsArrayfam = null;
			if(ar_infam!=null){

				try {
					myAnsArrayfam=ar_infam.getString(ar_infam.length()-1).split(";");
					System.out.println("family illness history"+ar_infam.getString(ar_infam.length()-1));
				} 
				catch (JSONException medicationlist) {
					// TODO Auto-generated catch block
					medicationlist.printStackTrace();
				}
				for(int j = 0; j < myAnsArrayfam.length; j++){ 
					String values=myAnsArrayfam[j].toString();
					String[] value = values.split(",");

					if(value!=null){

						if(value[0].toString().equals("N/A")){

							if(j==0){
								System.out.println("NA working........");
								fna.setChecked(true);
								fhyper.setChecked(false);
								fdia.setChecked(false);
								fcan.setChecked(false);
								fOther.setChecked(false);

								fhyper.setEnabled(false);
								fdia.setEnabled(false);
								fcan.setEnabled(false);
								fOther.setEnabled(false);
							}
							if(j==1){
								mna.setChecked(true);
								mhyper.setChecked(false);
								mdia.setChecked(false);
								mcan.setChecked(false);
								mOther.setChecked(false);

								mhyper.setEnabled(false);
								mdia.setEnabled(false);
								mcan.setEnabled(false);
								mOther.setEnabled(false);
							}
							if(j==2){
								sna.setChecked(true);
								shyper.setChecked(false);
								sdia.setChecked(false);
								scan.setChecked(false);
								sOther.setChecked(false);

								shyper.setEnabled(false);
								sdia.setEnabled(false);
								scan.setEnabled(false);
								sOther.setEnabled(false);
							}
							if(j==3){
								gfna.setChecked(true);
								gfhyper.setChecked(false);
								gfdia.setChecked(false);
								gfcan.setChecked(false);
								gfOther.setChecked(false);

								gfhyper.setEnabled(false);
								gfdia.setEnabled(false);
								gfcan.setEnabled(false);
								gfOther.setEnabled(false);
							}
							if(j==4){
								gmna.setChecked(true);
								gmhyper.setChecked(false);
								gmdia.setChecked(false);
								gmcan.setChecked(false);
								gmOther.setChecked(false);

								gmhyper.setEnabled(false);
								gmdia.setEnabled(false);
								gmcan.setEnabled(false);
								gmOther.setEnabled(false);
							}


						}
						else{


							if(value[0].toString().equals("Hypertension")){
								if(j==0){

									System.out.println("father hyper workinggggggggg");
									fhyper.setChecked(true);
								}
								if(j==1){
									mhyper.setChecked(true);
								}
								if(j==2){
									shyper.setChecked(true);
								}
								if(j==3){
									gfhyper.setChecked(true);
								}
								if(j==4){
									gmhyper.setChecked(true);
								}
							}

							if(value[1].toString().equals("Diabetes")){
								if(j==0){

									System.out.println("father diabeties working.....");
									fdia.setChecked(true);
								}

								if(j==1){
									mdia.setChecked(true);
								}
								if(j==2){
									sdia.setChecked(true);

								}
								if(j==3){
									gfdia.setChecked(true);

								}
								if(j==4){
									gmdia.setChecked(true);

								}
							}
							if(value[2].toString().equals("Cancer")){
								if(j==0){
									System.out.println("father cancer working........");
									fcan.setChecked(true);

								}
								if(j==1){
									mcan.setChecked(true);

								}
								if(j==2){
									scan.setChecked(true);

								}
								if(j==3){
									gfcan.setChecked(true);

								}
								if(j==4){
									gmcan.setChecked(true);

								}
							}

							if(value[3].toString().equals("Nil")){

								System.out.println("Nil working.....");
							}
							else{
								if(j==0){

									System.out.println("father other working............");
									if(!(value[3].toString().equalsIgnoreCase("null")))
										fother.setText(value[3].toString());						    	                    	

									fOther.setChecked(true);
								}
								if(j==1){
									if(!(value[3].toString().equalsIgnoreCase("null")))
										mother.setText(value[3].toString());

									mOther.setChecked(true);
								}
								if(j==2){
									if(!(value[3].toString().equalsIgnoreCase("null")))
										sother.setText(value[3].toString());

									sOther.setChecked(true);
								}
								if(j==3){
									if(!(value[3].toString().equalsIgnoreCase("null")))
										gfother.setText(value[3].toString());

									gfOther.setChecked(true);
								}
								if(j==4){
									if(!(value[3].toString().equalsIgnoreCase("null")))
										gmother.setText(value[3].toString());

									gmOther.setChecked(true);
								}
							}
						}
					}
				}
				if(patientflag==true){
					if(fOther.isChecked()){
						fother.setEnabled(true);
					}
					if(mOther.isChecked()){
						mother.setEnabled(true);
					}
					if(sOther.isChecked()){
						sother.setEnabled(true);
					}
					if(gfOther.isChecked()){
						gfother.setEnabled(true);
					}
					if(gmOther.isChecked()){
						gmother.setEnabled(true);
					}
				}
			}
			ar_shist =  patientobject.getJSONArray("socialHistoryStatus");

			String[] myAnsArraysh = null;
			if(ar_shist!=null){
				try {
					myAnsArraysh=ar_shist.getString(ar_shist.length()-1).split(",");
				} 
				catch (JSONException medicationlist) {
					// TODO Auto-generated catch block
					medicationlist.printStackTrace();
				}

				if(myAnsArraysh[0].toString().equals("Current")){

					alcurr.setChecked(true);
					alpast.setChecked(false);
					alna.setChecked(false);
					alpast.setEnabled(false);
					alna.setEnabled(false);
				}
				if(myAnsArraysh[0].toString().equals("Past")){

					alcurr.setChecked(false);
					alpast.setChecked(true);
					alna.setChecked(false);
					alcurr.setEnabled(false);
					alna.setEnabled(false);
				}
				if(myAnsArraysh[0].toString().equals("N/A")){

					alcurr.setChecked(false);
					alpast.setChecked(false);
					alna.setChecked(true);

					alcurr.setEnabled(false);
					alpast.setEnabled(false);
				}

				if(myAnsArraysh[1].toString().equals("Current")){

					tobcurr.setChecked(true);
					tobpast.setChecked(false);
					tobna.setChecked(false);

					tobpast.setEnabled(false);
					tobna.setEnabled(false);
				}
				if(myAnsArraysh[1].toString().equals("Past")){
					tobpast.setChecked(true);
					tobcurr.setChecked(false);				
					tobna.setChecked(false);

					tobcurr.setEnabled(false);				
					tobna.setEnabled(false);
				}
				if(myAnsArraysh[1].toString().equals("N/A")){

					tobcurr.setChecked(false);
					tobpast.setChecked(false);
					tobna.setChecked(true);

					tobcurr.setEnabled(false);
					tobpast.setEnabled(false);
				}
				if(myAnsArraysh[2].toString().equals("Current")){

					osubcurr.setChecked(true);
					osubpast.setChecked(false);
					osubna.setChecked(false);

					osubpast.setEnabled(false);
					osubna.setEnabled(false);
				}
				if(myAnsArraysh[2].toString().equals("Past")){
					osubpast.setChecked(true);
					osubcurr.setChecked(false);
					osubna.setChecked(false);
					osubcurr.setEnabled(false);
					osubna.setEnabled(false);
				}
				if(myAnsArraysh[2].toString().equals("N/A")){

					osubcurr.setChecked(false);
					osubpast.setChecked(false);
					osubna.setChecked(true);

					osubcurr.setEnabled(false);
					osubpast.setEnabled(false);
				}

				if(myAnsArraysh.length>3){


					if(myAnsArraysh[3].toString().equals("Current")){

						osubcurr.setChecked(true);
						osubpast.setChecked(false);
						osubna.setChecked(false);

						osubpast.setEnabled(false);
						osubna.setEnabled(false);
					}
					if(myAnsArraysh[3].toString().equals("Past")){
						osubpast.setChecked(true);
						osubcurr.setChecked(false);
						osubna.setChecked(false);
						osubcurr.setEnabled(false);
						osubna.setEnabled(false);
					}
					if(myAnsArraysh[3].toString().equals("N/A")){

						osubcurr.setChecked(false);
						osubpast.setChecked(false);
						osubna.setChecked(true);

						osubcurr.setEnabled(false);
						osubpast.setEnabled(false);
					}



				}


			}


		}

		if(patientedit_checkflag==false){

			noAlerReport.setEnabled(false);
			medicOne.setEnabled(false);
			medicTwo.setEnabled(false);
			reactionOne.setEnabled(false);
			reactionTwo.setEnabled(false);
			addAlle.setEnabled(false);


			noIllReport.setEnabled(false);
			hyperCurr.setChecked(false);
			hyperPast.setChecked(false);
			hyperNa.setChecked(false);
			diaCurr.setChecked(false);
			diaPast.setChecked(false);
			diaNa.setChecked(false);
			canCurr.setChecked(false);
			canPast.setChecked(false);
			canNa.setChecked(false);
			hyperCurr.setEnabled(false);
			hyperPast.setEnabled(false);
			hyperNa.setEnabled(false);
			diaCurr.setEnabled(false);
			diaPast.setEnabled(false);
			diaNa.setEnabled(false);
			canCurr.setEnabled(false);
			canPast.setEnabled(false);
			canNa.setEnabled(false);
			addMajor.setEnabled(false);

			noSurgeryReport.setEnabled(false);
			surgeryOne.setEnabled(false);
			surgeryTwo.setEnabled(false);
			addSurge.setEnabled(false);




			fna.setEnabled(false);
			fhyper.setEnabled(false);
			fdia.setEnabled(false);
			fcan.setEnabled(false);
			fOther.setEnabled(false);


			mna.setEnabled(false);
			mhyper.setEnabled(false);
			mdia.setEnabled(false);
			mcan.setEnabled(false);
			mOther.setEnabled(false);


			sna.setEnabled(false);
			shyper.setEnabled(false);
			sdia.setEnabled(false);
			scan.setEnabled(false);
			sOther.setEnabled(false);


			gfna.setEnabled(false);
			gfhyper.setEnabled(false);
			gfdia.setEnabled(false);
			gfcan.setEnabled(false);
			gfOther.setEnabled(false);


			gmna.setEnabled(false);
			gmhyper.setEnabled(false);
			gmdia.setEnabled(false);
			gmcan.setEnabled(false);
			gmOther.setEnabled(false);

			alpast.setEnabled(false);
			alna.setEnabled(false);
			alcurr.setEnabled(false);
			tobna.setEnabled(false);
			tobcurr.setEnabled(false);
			tobpast.setEnabled(false);
			osubpast.setEnabled(false);
			osubna.setEnabled(false);
			osubcurr.setEnabled(false);
		}



		activationflagvalue=FragmentActivityContainer.check_save;
		((FragmentActivityContainer)getActivity()).getpatientfrag().onbuttonmedhist(2);
		System.out.println("activationflagvalue"+activationflagvalue);
	}





	protected void hideKeyboard(View view) {
		InputMethodManager in = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
		in.hideSoftInputFromWindow(view.getWindowToken(),
				InputMethodManager.HIDE_NOT_ALWAYS);
	}

	public void send_data_nextpage(int targetfragment)
	{

		millfamArray= new String[5];
		millfamStatArray= new String[5];
		socHistStatArray= new String[3];
		socHistArray= new String[3];
		drArray=new String[1];

		SharedPreferences sp1=getActivity().getSharedPreferences("Login", 0);

		String rdRegNum=sp1.getString("Unm", null);
		System.out.println("rdRegNum"+rdRegNum);

		if(rdRegNum!=null)
		{

			drArray[0]=rdRegNum;
		}

		millfamArray[0]="Father";
		millfamArray[1]="Mother";
		millfamArray[2]="Sibling";
		millfamArray[3]="Grandfather";
		millfamArray[4]="Grandmother";

		socHistArray[0]="Alcohol";
		socHistArray[1]="Tobacco";
		socHistArray[2]="Other Substance";

		if(fna.isChecked()){
			millfamStatArray[0]="N/A";
			Log.e("Valueeeee===ffff",String.valueOf(millfamStatArray[0]) );
		}
		else{
			String fillhyp,filldia,fillcan,filloth = null;

			if(fhyper.isChecked()){
				fillhyp=fhyper.getText().toString();
			}
			else{
				fillhyp="Nil";
			}
			if(fdia.isChecked()){
				filldia=fdia.getText().toString();
			}
			else{
				filldia="Nil";
			}
			if(fcan.isChecked()){
				fillcan=fcan.getText().toString();
			}
			else{
				fillcan="Nil";
			}
			if(fOther.isChecked()){
				String fotherm=fother.getText().toString();
				if(fotherm.matches("")){
					//Toast.makeText(getActivity(), "Enter Father Other ill", Toast.LENGTH_SHORT).show();
				}
				else{
					filloth=fother.getText().toString();
				}

			}
			else{
				filloth="Nil";
			}
			String fatherills=fillhyp+","+filldia+","+fillcan+","+filloth;

			millfamStatArray[0]=fatherills;
			Log.e("Valueeeee===ffff",fatherills );
		}


		if(mna.isChecked()){
			millfamStatArray[1]="N/A";
		}
		else{
			String millhyp,milldia,millcan,milloth = null;

			if(mhyper.isChecked()){
				millhyp=mhyper.getText().toString();
			}
			else{
				millhyp="Nil";
			}
			if(mdia.isChecked()){
				milldia=mdia.getText().toString();
			}
			else{
				milldia="Nil";
			}
			if(mcan.isChecked()){
				millcan=mcan.getText().toString();
			}
			else{
				millcan="Nil";
			}
			if(mOther.isChecked()){
				String fotherm=mother.getText().toString();
				if(fotherm.matches("")){
					//	Toast.makeText(getActivity(), "Enter Mother Other ill", Toast.LENGTH_SHORT).show();
				}
				else{
					milloth=mother.getText().toString();

				}
			}
			else{
				milloth="Nil";
			}
			String matherills=millhyp+","+milldia+","+millcan+","+milloth;

			millfamStatArray[1]=matherills;
			Log.e("Valueeeee===ffff",matherills );
		}

		if(sna.isChecked()){
			millfamStatArray[2]="N/A";
		}
		else{
			String sillhyp,silldia,sillcan,silloth = null;

			if(shyper.isChecked()){
				sillhyp=shyper.getText().toString();
			}
			else{
				sillhyp="Nil";
			}
			if(sdia.isChecked()){
				silldia=sdia.getText().toString();
			}
			else{
				silldia="Nil";
			}
			if(scan.isChecked()){
				sillcan=scan.getText().toString();
			}
			else{
				sillcan="Nil";
			}
			if(sOther.isChecked()){
				String fotherm=sother.getText().toString();
				if(fotherm.matches("")){
					//	Toast.makeText(getActivity(), "Enter Sibling Other ill", Toast.LENGTH_SHORT).show();
				}
				else{
					silloth=sother.getText().toString();
				}

			}
			else{
				silloth="Nil";
			}
			String satherills=sillhyp+","+silldia+","+sillcan+","+silloth;

			millfamStatArray[2]=satherills;
			Log.e("Valueeeee===ffff",satherills );
		}

		if(gfna.isChecked()){
			millfamStatArray[3]="N/A";
		}
		else{
			String gfillhyp,gfilldia,gfillcan,gfilloth = null;
			if(gfhyper.isChecked()){
				gfillhyp=gfhyper.getText().toString();
			}
			else{
				gfillhyp="Nil";
			}
			if(gfdia.isChecked()){
				gfilldia=gfdia.getText().toString();
			}
			else{
				gfilldia="Nil";
			}
			if(gfcan.isChecked()){
				gfillcan=gfcan.getText().toString();
			}
			else{
				gfillcan="Nil";
			}
			if(gfOther.isChecked()){
				String gfotherm=gfother.getText().toString();
				if(gfotherm.matches("")){
					//		Toast.makeText(getActivity(), "Enter Grandfather Other ill", Toast.LENGTH_SHORT).show();
				}
				else{
					gfilloth=gfother.getText().toString();
				}

			}
			else{
				gfilloth="Nil";
			}
			String gfatherills=gfillhyp+","+gfilldia+","+gfillcan+","+gfilloth;

			millfamStatArray[3]=gfatherills;
			Log.e("Valueeeee===ffff",gfatherills );
		}


		if(gmna.isChecked()){
			millfamStatArray[4]="N/A";
		}
		else{
			String gmillhyp,gmilldia,gmillcan,gmilloth = null;

			if(gmhyper.isChecked()){
				gmillhyp=gmhyper.getText().toString();
			}
			else{
				gmillhyp="Nil";
			}
			if(gmdia.isChecked()){
				gmilldia=gmdia.getText().toString();
			}
			else{
				gmilldia="Nil";
			}
			if(gmcan.isChecked()){
				gmillcan=gmcan.getText().toString();
			}
			else{
				gmillcan="Nil";
			}
			if(gmOther.isChecked()){
				String gmotherm=gmother.getText().toString();
				if(gmotherm.matches("")){
					//Toast.makeText(getActivity(), "Enter Grandmother Other ill", Toast.LENGTH_SHORT).show();
				}
				else{
					gmilloth=gmother.getText().toString();
				}

			}
			else{
				gmilloth="Nil";
			}
			String gmatherills=gmillhyp+","+gmilldia+","+gmillcan+","+gmilloth;

			millfamStatArray[4]=gmatherills;
			Log.e("Valueeeee===ffff",String.valueOf(millfamStatArray) );
		}


		if(alcurr.isChecked()){
			socHistStatArray[0]=alcurr.getText().toString();
		}
		if(alpast.isChecked()){
			socHistStatArray[0]=alpast.getText().toString();
		}
		if(alna.isChecked()){
			socHistStatArray[0]=alna.getText().toString();
		}

		if(tobcurr.isChecked()){
			socHistStatArray[1]=tobcurr.getText().toString();
		}
		if(tobpast.isChecked()){
			socHistStatArray[1]=tobpast.getText().toString();
		}
		if(tobna.isChecked()){
			socHistStatArray[1]=tobna.getText().toString();
		}

		if(osubcurr.isChecked()){
			socHistStatArray[2]=osubcurr.getText().toString();
		}
		if(osubpast.isChecked()){
			socHistStatArray[2]=osubpast.getText().toString();
		}
		if(osubna.isChecked()){
			socHistStatArray[2]=osubna.getText().toString();
		}
		Log.e("Valueeeee===sssss",String.valueOf(socHistStatArray) );

		save_PatientMedical_history(targetfragment);
	}

	private void save_PatientMedical_history(final int targetfragment)
	{
		// TODO Auto-generated method stub
		SharedPreferences scanpidnew=getActivity().getSharedPreferences("NewPatID", 0);
		String scnewPid=scanpidnew.getString("NewID", null); 


		SharedPreferences sp=getActivity().getSharedPreferences("Login", 0);
		String docterregId=sp.getString("docterRegnumber", null);

		SharedPreferences scanpid1=getActivity().getSharedPreferences("ScannedPid", 0);
		String scPid=scanpid1.getString("scanpid", null);     
		if(patientobject==null){

			if(scPid!=null||scnewPid!=null){
				if(scPid!=null){
					Log.e("scanner pid111====>>", scPid);
					patientId=scPid;
				}
				if(scnewPid!=null){
					Log.e("scanner pid====>>", scnewPid);
					patientId=scnewPid;
				}
			}
		}else if(patientobject.getString("patientID")==null){
			patientId=scnewPid;
		}else{
			patientId=patientobject.getString("patientID");
		}
		if(patientobject==null){
			patientobject=new ParseObject("Patients");
		}

		if(patientflag==false){
			medication_data();
			reaction_data();
			surgery_data();
			majorillness_data();
			majorillness_status();
			familyillnessstatusdata();
			socialhistorystatusdata();

		}else if(patientflag==true){

			if(((FragmentActivityContainer)getActivity()).getoldpatientupdateflag()==false){
				medication_data();
				reaction_data();
				surgery_data();
				majorillness_data();
				majorillness_status();
				familyillnessstatusdata();
				socialhistorystatusdata();

				((FragmentActivityContainer)getActivity()).setoldpatientupdateflag(true);
			}else{
				medication_data();
				reaction_data();
				surgery_data();
				majorillness_data();
				majorillness_status();
				familyillnessstatusdata();
				socialhistorystatusdata();
			}


		}



		mProgressDialog = new ProgressDialog(getActivity());
		mProgressDialog.setMessage("Storing Patient Data ....");
		mProgressDialog.setCancelable(false);
		mProgressDialog.show();

		patientobject.put("patientID", patientId);
		patientobject.put("allergiesMedication", allmedicArray);
		patientobject.put("allergiesReaction",  allreacArray);
		patientobject.put("majorIllness",  majillArray);
		patientobject.put("majorIllnessStatus", majillStaArray);
		patientobject.put("Surgeries",surgArray);
		patientobject.put("majorIllnessFamily",  Arrays.asList( millfamArray));
		if(ar_infam!=null){
			patientobject.put("majorIllnessFamilyStatus",ar_infam);
		}else{
			patientobject.put("majorIllnessFamilyStatus",ar_infamarray);
		}
		if(ar_shist!=null){
			patientobject.put("socialHistoryStatus",ar_shist);
		}else{
			patientobject.put("socialHistoryStatus",ar_shistarray);
		}
		patientobject.put("socialHistory",  Arrays.asList(socHistArray));
		patientobject.put("docterRegistrationNumber", docterregId);

		patientobject.put("doctors", Arrays.asList( drArray));


		patientobject.pinInBackground(new SaveCallback() {

			public void done(ParseException e) {
				mProgressDialog.dismiss();
				if (e == null) {
					myObjectSavedSuccessfully(targetfragment);
					// Id= Student.getObjectId();
					//i=1;

				} else {
					e.printStackTrace();
					myObjectSaveDidNotSucceed();
					//i=0;
				}
			}

			private void myObjectSaveDidNotSucceed() {
				// TODO Auto-generated method stub
				Toast msg1=Toast.makeText(getActivity(), "Failed", Toast.LENGTH_LONG);
				msg1.show();


			}

			private void myObjectSavedSuccessfully(int targetfragment) {
				// TODO Auto-generated method stub
				FragmentActivityContainer.check_save=0;
				Toast msg=Toast.makeText(getActivity(), " Successfully Completed", Toast.LENGTH_LONG);
				msg.show();

				medicationlist.clear();
				reactionlist.clear();
				surgeryfirstlist.clear();
				surgerysecondlist.clear();

				((FragmentActivityContainer)getActivity()).setPatientobject(patientobject);



				FragmentManager fragmentManager = getFragmentManager();
				FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

				/*if(targetfragment==4 || targetfragment==0){
					if(((FragmentActivityContainer)getActivity()).getcurrentvitals()==null){
						patientcurrentvitalsfragObj = new CurrentVitals();
						Bundle bundle=new Bundle();
						bundle.putBoolean("patientflag", patientflag);
						patientcurrentvitalsfragObj.setArguments(bundle);
					}else{
						patientcurrentvitalsfragObj=((FragmentActivityContainer)getActivity()).getcurrentvitals();
					}
					((FragmentActivityContainer)getActivity()).setcurrentvitals(patientcurrentvitalsfragObj);
					fragmentTransaction.replace(R.id.fl_fragment_container, patientcurrentvitalsfragObj);
				}else if(targetfragment==3){
					Patient_Current_Medical_Info_Frag pntcurrmedinfo = new Patient_Current_Medical_Info_Frag();
					fragmentTransaction.replace(R.id.fl_fragment_container, pntcurrmedinfo);
					//	fragmentTransaction.addToBackStack("Frag4");
				}else if(targetfragment==1){
					if(((FragmentActivityContainer)getActivity()).getpersonalinfo()==null){
						patientpesonalinfofrag = new Patient_Personal_Info_Frag();
						Bundle bundle=new Bundle();
						bundle.putBoolean("patientflag", patientflag);
						patientpesonalinfofrag.setArguments(bundle);
					}else{
						patientpesonalinfofrag=((FragmentActivityContainer)getActivity()).getpersonalinfo();
					}
					fragmentTransaction.replace(R.id.fl_fragment_container, patientpesonalinfofrag);
				}*/
				submitButtonDeactivation();
				((FragmentActivityContainer)getActivity()).setPatientobject(patientobject);		

				if(admintab==true)
					fragmentTransaction.addToBackStack(null);
				fragmentTransaction.commit();
			}
		});

		patientobject.saveEventually();
	}


	private void submitButtonActivation() {
		// TODO Auto-generated method stub
		((FragmentActivityContainer)getActivity()).getpatientfrag().onbuttonmedhist(1);
	}
	private void submitButtonDeactivation() {
		// TODO Auto-generated method stub
		((FragmentActivityContainer)getActivity()).getpatientfrag().onbuttonmedhist(0);
	}

	public String[] join(String [] ... parms) {
		// calculate size of target array
		int size = 0;
		for (String[] array : parms) {
			size += array.length;
		}

		String[] result = new String[size];

		int j = 0;
		for (String[] array : parms) {
			for (String s : array) {
				result[j++] = s;
			}
		}
		return result;
	}

	@Override
	public void onMedical(int arg,int targetfragment) {
		// TODO Auto-generated method stub

		System.out.println("medical history working..........");
		allmedicArray.clear();
		allreacArray.clear();
		majillArray.clear();
		majillStaArray.clear();
		surgArray.clear();

		System.out.println("illstat array size"+majillStaArray.size());

		String medone=medicOne.getText().toString();
		String reactone=reactionOne.getText().toString();
		String medtwo=medicTwo.getText().toString();
		String reacttwo=reactionTwo.getText().toString();
		String surOne=surgeryOne.getText().toString();
		String surTwo=surgeryTwo.getText().toString();


		Log.e("valueee eee",String.valueOf(medicationlist.size()) );
		Log.e("valueee aaa",String.valueOf(a));



		if(noAlerReport.isChecked()&&noIllReport.isChecked()&&noSurgeryReport.isChecked()){
			allmedicArray.add("NA");
			allreacArray.add("NA");

			majillArray.add("Hypertension");
			majillArray.add("Diabetes");
			majillArray.add("Cancer");

			majillStaArray.add("NA");
			majillStaArray.add("NA");
			majillStaArray.add("NA");


			surgArray.add("NA");
			surgArray.add("NA");

			send_data_nextpage(targetfragment);


		}
		else{
			if(noAlerReport.isChecked()){
				String reportAller=noAlerReport.getText().toString();
				Log.e("Checked====>>", reportAller);
				allmedicArray.add("NA");
				allreacArray.add("NA");
			}
			else{
				if(medicOne.getText().length()!=0)
				{
					System.out.println("working1");
					allmedicArray.add(medone);

				}else{
					allmedicArray.add("Nil");
				}

				if(reactionOne.getText().length()!=0){
					allreacArray.add(reactone);
				}else{
					allreacArray.add("Nil");
				}

				if(medicTwo.getText().length()!=0){
					System.out.println("working2");
					allmedicArray.add(medtwo);
				}else{
					allmedicArray.add("Nil");
				}

				if(reactionTwo.getText().length()!=0){
					allreacArray.add(reacttwo);
				}else{
					allreacArray.add("Nil");
				}



				System.out.println("medication array result show"+allmedicArray);
				System.out.println("reaction array result show"+allreacArray);

			}

			if(noIllReport.isChecked()){
				String reportIll=noIllReport.getText().toString();
				Log.e("Checked====>>", reportIll);
				majillArray.add("Hypertension");
				majillArray.add("Diabetes");
				majillArray.add("Cancer");

				majillStaArray.add("NA");
				majillStaArray.add("NA");
				majillStaArray.add("NA");
			}
			else{
				majillArray.add("Hypertension");
				majillArray.add("Diabetes");
				majillArray.add("Cancer");

				if(hyperCurr.isChecked()){
					String hypc=hyperCurr.getText().toString();
					majillStaArray.add(hypc);
				}
				if(hyperPast.isChecked()){
					String hypp=hyperPast.getText().toString();
					majillStaArray.add(hypp);
				}
				if(hyperNa.isChecked()){
					String hypn=hyperNa.getText().toString();
					majillStaArray.add(hypn);
				}
				if(!(hyperCurr.isChecked())&&!(hyperPast.isChecked())&&!(hyperNa.isChecked())){
					majillStaArray.add("N/A");
				}

				if(diaCurr.isChecked()){
					String diac=diaCurr.getText().toString();
					majillStaArray.add(diac);
				}
				if(diaPast.isChecked()){
					String diap=diaPast.getText().toString();
					majillStaArray.add(diap);
				}
				if(diaNa.isChecked()){
					String dian=diaNa.getText().toString();
					majillStaArray.add(dian);
				}

				if(!(diaCurr.isChecked())&&!(diaPast.isChecked())&&!(diaNa.isChecked())){
					majillStaArray.add("N/A");
				}


				if(canCurr.isChecked()){
					String canc=canCurr.getText().toString();
					majillStaArray.add(canc);
				}
				if(canPast.isChecked()){
					String canp=canPast.getText().toString();
					majillStaArray.add(canp);
				}
				if(canNa.isChecked()){
					String cann=canNa.getText().toString();
					majillStaArray.add(cann);
				}
				if(!(canCurr.isChecked())&&!(canPast.isChecked())&&!(canNa.isChecked())){
					majillStaArray.add("N/A");
				}
			}

			if(noSurgeryReport.isChecked()){
				surgArray.add("NA");
				surgArray.add("NA");
			}
			else{

				if(surgeryOne.getText().length()!=0){
					surgArray.add(surOne);			
				}else{
					surgArray.add("Nil");
				}
				if(surgeryTwo.getText().length()!=0){
					surgArray.add(surTwo);
				}
				else{
					surgArray.add("Nil");
				}

			}
			if(medicationlist.size()>0){
				for(int j=0;j<(medicationlist.size());j++){
					Object item=medicationlist.get(j);
					Object reactionitem=reactionlist.get(j);

					if(item!=null){
						if(medicationlist.get(j).getText().length()!=0){
							allmedicArray.add(medicationlist.get(j).getText().toString());
						}else{
							allmedicArray.add("Nil");
						}
						if(reactionlist.get(j).getText().length()!=0 ){
							allreacArray.add(reactionlist.get(j).getText().toString());
						}else{
							allreacArray.add("Nil");
						}
					}

					/*System.out.println("medicationlist details"+medicationlist.get(j).getText().toString());*/
					System.out.println("medication list size"+medicationlist.size());
				}


			}
			if(surgeryfirstlist.size()>0){
				for(int j=0;j<(surgeryfirstlist.size());j++)
				{
					Object list=surgeryfirstlist.get(j);
					if(list!=null){
						if(surgeryfirstlist.get(j).getText().length()!=0){
							surgArray.add(surgeryfirstlist.get(j).getText().toString());
						}else{
							surgArray.add("Nil");	
						}
						if(surgerysecondlist.get(j).getText().length()!=0){
							surgArray.add(surgerysecondlist.get(j).getText().toString());
						}else{
							surgArray.add("Nil");
						}
					}
				}
			}
			if(majorillnesslist.size()>0){
				System.out.println("edittext size"+majorillnesslist.size());




				for(int j=0;j<(majorillnesslist.size());j++)
				{
					Object item=majorillnesslist.get(j);
					if(item!=null){

						if(majorillnesslist.get(j).getText().length()!=0){
							majillArray.add(majorillnesslist.get(j).getText().toString());
						}else{
							majillArray.add("Nil");
						}
						if(currentcblist.size()>0){
							if(currentcblist.get(j).isChecked())
								majillStaArray.add(currentcblist.get(j).getText().toString());
						}
						if(pastcblist.size()>0){
							if(pastcblist.get(j).isChecked())
								majillStaArray.add(pastcblist.get(j).getText().toString());
						}
						if(nacblist.size()>0){

							if(nacblist.get(j).isChecked())
								majillStaArray.add(nacblist.get(j).getText().toString());
						}
						if(!(currentcblist.get(j).isChecked()) &&!(pastcblist.get(j).isChecked()) &&!(nacblist.get(j).isChecked()) ){
							majillStaArray.add("Nil");
						}
					}
				}
			}
			boolean checksizereaction=true;
			boolean checksizemedication=true;
			System.out.println("medication array size"+allmedicArray.size());
			send_data_nextpage(targetfragment);
		}
	}

	public void medication_data(){
		String allmedic = "";
		for(int i=0;i<allmedicArray.size();i++){
			allmedic+=allmedicArray.get(i)+",";
		}
		try {
			allmedicArray.clear();
			if(ar_in!=null){
				if(patientflag==false){
					for(int i=0;i<ar_in.length()-1;i++){
						allmedicArray.add(ar_in.getString(i));
					}
				}else if(patientflag==true && ((FragmentActivityContainer)getActivity()).getoldpatientupdateflag()==false){
					for(int i=0;i<ar_in.length();i++){
						allmedicArray.add(ar_in.getString(i));
					}
				}else if(patientflag==true && ((FragmentActivityContainer)getActivity()).getoldpatientupdateflag()==true){
					for(int i=0;i<ar_in.length()-1;i++){
						allmedicArray.add(ar_in.getString(i));
					}
				}
				allmedicArray.add(allmedic);
			}else{
				allmedicArray.add(allmedic);
			}

		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

	}

	public void reaction_data(){
		String allreaction = "";
		for(int i=0;i<allreacArray.size();i++){
			allreaction+=allreacArray.get(i)+",";
		}
		try {
			allreacArray.clear();
			if(ar_inreac!=null){
				if(patientflag==false){
					for(int i=0;i<ar_inreac.length()-1;i++){
						allreacArray.add(ar_inreac.getString(i));
					}
				}else if(patientflag==true && ((FragmentActivityContainer)getActivity()).getoldpatientupdateflag()==false){
					for(int i=0;i<ar_inreac.length();i++){
						allreacArray.add(ar_inreac.getString(i));
					}
				}else if(patientflag==true && ((FragmentActivityContainer)getActivity()).getoldpatientupdateflag()==true){
					for(int i=0;i<ar_inreac.length()-1;i++){
						allreacArray.add(ar_inreac.getString(i));
					}
				}
				allreacArray.add(allreaction);
			}else{
				allreacArray.add(allreaction);
			}

		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

	public void surgery_data(){
		String allsurgery="";
		for(int i=0;i<surgArray.size();i++){
			allsurgery+=surgArray.get(i)+",";
		}
		try{
			surgArray.clear();
			if(ar_insur!=null){
				if(patientflag==false){
					for(int i=0;i<ar_insur.length()-1;i++){
						surgArray.add(ar_insur.getString(i));
					}
				}else if(patientflag==true && ((FragmentActivityContainer)getActivity()).getoldpatientupdateflag()==false){
					for(int i=0;i<ar_insur.length();i++){
						surgArray.add(ar_insur.getString(i));
					}
				}else if(patientflag==true && ((FragmentActivityContainer)getActivity()).getoldpatientupdateflag()==true){
					for(int i=0;i<ar_insur.length()-1;i++){
						surgArray.add(ar_insur.getString(i));
					}
				}
				surgArray.add(allsurgery);
			}else{
				surgArray.add(allsurgery);
			}
		}catch(Exception e){
			e.printStackTrace();
		}


	}
	public void majorillness_data(){
		String allmajorillness="";
		for(int i=0;i<majillArray.size();i++){
			allmajorillness+=majillArray.get(i)+",";
		}
		try{
			majillArray.clear();
			if(ar_in2!=null){
				if(patientflag==false){
					for(int i=0;i<ar_in2.length()-1;i++){
						majillArray.add(ar_in2.getString(i));
					}
				}else if(patientflag==true && ((FragmentActivityContainer)getActivity()).getoldpatientupdateflag()==false){
					for(int i=0;i<ar_in2.length();i++){
						majillArray.add(ar_in2.getString(i));
					}
				}else if(patientflag==true && ((FragmentActivityContainer)getActivity()).getoldpatientupdateflag()==true){
					for(int i=0;i<ar_in2.length()-1;i++){
						majillArray.add(ar_in2.getString(i));
					}
				}
				majillArray.add(allmajorillness);
			}else{
				majillArray.add(allmajorillness);	
			}

		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public void majorillness_status(){
		String allmajorillnessstatus="";
		for(int i=0;i<majillStaArray.size();i++){
			allmajorillnessstatus+=majillStaArray.get(i)+",";
		}
		try{
			majillStaArray.clear();
			if(ar_in1!=null){
				if(patientflag==false){
					for(int i=0;i<ar_in1.length()-1;i++){
						majillStaArray.add(ar_in1.getString(i));
					}
				}else if(patientflag==true && ((FragmentActivityContainer)getActivity()).getoldpatientupdateflag()==false){
					for(int i=0;i<ar_in1.length();i++){
						majillStaArray.add(ar_in1.getString(i));
					}
				}else if(patientflag==true && ((FragmentActivityContainer)getActivity()).getoldpatientupdateflag()==true){
					for(int i=0;i<ar_in1.length()-1;i++){
						majillStaArray.add(ar_in1.getString(i));
					}
				}
				majillStaArray.add(allmajorillnessstatus);
			}else{
				majillStaArray.add(allmajorillnessstatus);	
			}

		}catch(Exception e){
			e.printStackTrace();
		}
		System.out.println("major illness status"+majillStaArray);
	}


	public void familyillnessstatusdata(){
		String result ="";
		for(int i=0;i<millfamStatArray.length;i++){
			result+=millfamStatArray[i]+";";
		}
		//result="[" +result+ "]";
		System.out.println("resultttt"+result);
		if(ar_infam!=null){
			if(patientflag==false){
				try {
					ar_infam.put(ar_infam.length()-1, result);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}else if(patientflag==true && ((FragmentActivityContainer)getActivity()).getoldpatientupdateflag()==false){
				ar_infam.put(result);
			}else if(patientflag==true && ((FragmentActivityContainer)getActivity()).getoldpatientupdateflag()==true){
				try {
					ar_infam.put(ar_infam.length()-1, result);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			System.out.println("ar_infam array"+ar_infam);
		}else{

			ar_infamarray=new JSONArray();
			ar_infamarray.put(result);

		}

	}
	public void socialhistorystatusdata(){
		String result="";
		for(int i=0;i<socHistStatArray.length;i++){
			result+=socHistStatArray[i]+",";
		}
		if(ar_shist!=null){
			if(patientflag==false){
				try {
					ar_shist.put(ar_shist.length()-1, result);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}else if(patientflag==true && ((FragmentActivityContainer)getActivity()).getoldpatientupdateflag()==false){
				ar_shist.put(result);
			}else if(patientflag==true && ((FragmentActivityContainer)getActivity()).getoldpatientupdateflag()==true){
				try {
					ar_shist.put(ar_shist.length()-1, result);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		}else{
			ar_shistarray=new JSONArray();
			ar_shistarray.put(result);
		}
	}

}