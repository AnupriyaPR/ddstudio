package com.wiinnova.doctorsdiary.fragments;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wiinnova.doctorsdiary.R;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.wiinnova.doctorsdiary.activities.FragmentActivityContainer;
import com.wiinnova.doctorsdiary.activities.Home_Page_Activity;
import com.wiinnova.doctorsdiary.fragment.Diagnosis_Frag_Gynacologist;
import com.wiinnova.doctorsdiary.popupwindow.SpinnerPopup;
import com.wiinnova.doctorsdiary.popupwindow.SpinnerPopup.onSubmitListener;
import com.wiinnova.doctorsdiary.supportclasses.Diseasenameclass;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

public class DiagnosisExaminationView extends Fragment{

	TextView dflName,duniqueID;
	String puid,filan;
	TextView symptoms,syndromes,additionalcumm,date,llist;
	ProgressDialog mProgressDialog;
	private LinearLayout layoutcontainer;
	ParseObject patient2;
	ParseObject patient;
	int skip=0;

	ArrayList<Diseasenameclass> diseasenamearraylist=new ArrayList<Diseasenameclass>();
	private  List<ParseObject>  diseaseobjects = new ArrayList<ParseObject>();
	private DiagnosisSideMenu diagfrag;


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view=inflater.inflate(R.layout.diagnosis_examination_view, null);

		dflName=(TextView)view.findViewById(R.id.tvfirstlastName);
		duniqueID=(TextView)view.findViewById(R.id.cduniqueid);
		layoutcontainer=(LinearLayout)view.findViewById(R.id.diadynamic);

		if(getFragmentManager().findFragmentById(R.id.fl_sidemenu_container) instanceof DiagnosisSideMenu){

		}else{
			diagfrag = new DiagnosisSideMenu();
			getFragmentManager().beginTransaction()
			.replace(R.id.fl_sidemenu_container, diagfrag)
			.commit();
		}
		if(diagfrag!=null){
			((FragmentActivityContainer)getActivity()).setDiagfrag_nephro(diagfrag);
		}


		if(Home_Page_Activity.patient!=null){
			puid=Home_Page_Activity.patient.getString("patientID");
			String fname=Home_Page_Activity.patient.getString("firstName");
			String lname=Home_Page_Activity.patient.getString("lastName");
			System.out.println("patient id1,,,,,,"+puid);
			if(fname==null){
				fname="FirstName";
			}
			if(lname==null){
				lname="LastName";
			}
			dflName.setText(fname+" "+lname);

		}else{

			SharedPreferences scanpidnew=getActivity().getSharedPreferences("NewPatID", 0);
			String scnewPid=scanpidnew.getString("NewID", null); 
			SharedPreferences scanpid1=getActivity().getSharedPreferences("ScannedPid", 0);
			String scPid=scanpid1.getString("scanpid", null);       
			if(scPid!=null){
				puid=scPid;
				SharedPreferences fnname1=getActivity().getSharedPreferences("fnName", 0);
				dflName.setText(fnname1.getString("fNname", "FirstName"+" "+"LastName"));
				System.out.println("name diag view.."+fnname1.getString("fNname", "FirstName"+" "+"LastName"));
			}

			if(scnewPid!=null){

				puid=scnewPid;


				SharedPreferences fnname1=getActivity().getSharedPreferences("fnName", 0);
				dflName.setText(fnname1.getString("fNname", "FirstName"+" "+"LastName"));
				System.out.println("name diag view.."+fnname1.getString("fNname", "FirstName"+" "+"LastName"));
			}

		}

		duniqueID.setText(puid);
		mProgressDialog = new ProgressDialog(getActivity());
		mProgressDialog.setCancelable(false);
		mProgressDialog.setMessage("Retrieving Examination Data...");
		mProgressDialog.show();
		System.out.println("object taken from main frag not working");


		fetchingdata_from_server();

		return view;	
	}

	private void fetchingdata_from_server() {
		// TODO Auto-generated method stub

		final ParseQuery<ParseObject> query = ParseQuery.getQuery("Diagnosis");
		query.fromLocalDatastore();
		query.whereEqualTo("patientID",puid );
		query.addDescendingOrder("createdDate");


		mProgressDialog.setOnCancelListener(new OnCancelListener() {

			@Override
			public void onCancel(DialogInterface dialog) {
				// TODO Auto-generated method stub
				query.cancel();
			}
		});


		query.findInBackground(new FindCallback<ParseObject>() {



			@Override
			public void done(List<ParseObject> objects, ParseException e) {
				// TODO Auto-generated method stub
				mProgressDialog.dismiss();

				if(DiagnosisExaminationView.this.isVisible()){

					if(e==null){ 
						System.out.println("object size"+objects.size());
						for (int i = 0; i < objects.size(); i++) {
							System.out.println("object size"+objects.size());
							ParseObject patientobject = objects.get(i);
							LayoutInflater inflater = LayoutInflater.from(getActivity());
							if(inflater!=null){
								View v = inflater.inflate(R.layout.diagnosisexaminationviewgynacologist, null); 
								double type=patientobject.getDouble("typeFlag");

								//LinearLayout main=(LinearLayout)v.findViewById(R.id.pmhl1);
								final LinearLayout container=(LinearLayout)v.findViewById(R.id.scroll);
								container.setVisibility(View.GONE);


								TextView flName,uniqueID;
								EditText etweight;
								EditText etheight;
								EditText etspo;
								EditText etpulse;
								EditText etrespiratory;
								EditText ettemperature;
								EditText etpressure;

								CheckBox cbnormal;
								CheckBox cbabnormal;
								EditText etexternalgenetalia;

								CheckBox cblumpno;
								CheckBox cblumpyes;
								EditText etlump;

								CheckBox cbgalactorrheano;
								CheckBox cbgalactorrheayes;
								EditText etgalactorrhea;

								CheckBox cbbreastotherno;
								CheckBox cbbreastotheryes;
								EditText etbreastother;

								CheckBox cbwelldevelopedno;
								CheckBox cbwelldevelopedyes;
								EditText etwelldeveloped;

								CheckBox cbhairno;
								CheckBox cbhairyes;
								EditText ethair;

								CheckBox cbacneno;
								CheckBox cbacneyes;
								EditText etacne;

								CheckBox cbsecondarysexotherno;
								CheckBox cbsecondarysexotheryes;
								EditText etsecondarysexother;

								CheckBox cbpreabdomennormal;
								CheckBox cbpreabdomenabnormal;
								EditText etpreabdomen;

								CheckBox cbhealthyno;
								CheckBox cbhealthyyes;
								EditText ethealthy;

								CheckBox cbbleedingno;
								CheckBox cbbleedingyes;
								EditText etbleeding;

								CheckBox cblbcno;
								CheckBox cblbcyes;
								EditText etlbc;

								TextView tvavaf;
								TextView tvrvrf;
								EditText etuterusothers;


								SpinnerPopup spObj;
								onSubmitListener spinnerpopupListener;
								int flag;

								LinearLayout datecontainer=(LinearLayout)v.findViewById(R.id.cdl2);
								TextView date=(TextView)v.findViewById(R.id.cdcurrdate);





								String bmi;
								String weight_result;
								String spo2_result;
								String pulse_result;
								String respRate_result;
								String height_result;
								String bloodGroup_result;
								String bloodPressure_result;
								String temp_result;
								JSONArray update_date;
								JSONArray currentvitals_id;

								JSONArray weight_result_array;
								JSONArray spo2_result_array;
								JSONArray pulse_result_array;
								JSONArray respRate_result_array;
								JSONArray height_result_array;
								JSONArray bloodGroup_result_array;
								JSONArray bloodPressure_result_array;
								JSONArray temp_result_array;
								JSONArray update_date_array;
								JSONArray currentvitals_id_array;



								EditText etBloodGroup;
								TextView tvName;
								String[] avaf_array;
								Diagnosis_Frag_Gynacologist diagfrag;
								ProgressDialog mProgressDialog;
								EditText etbmi;
								double heightInt;
								double weightInt;
								String[] bloodgroup_array;
								onSubmitListener bloodGroupSpinnerListener;
								EditText etdiapressure;
								EditText etsyspressure;
								CharSequence pid;


								/*flName=(TextView)v.findViewById(R.id.tvfirstlastName);
								uniqueID=(TextView)v.findViewById(R.id.cduniqueid);*/
								etweight=(EditText)v.findViewById(R.id.etweight);
								etheight=(EditText)v.findViewById(R.id.etheight);
								etbmi=(EditText)v.findViewById(R.id.etbmi);
								etspo=(EditText)v.findViewById(R.id.etspo);
								etpulse=(EditText)v.findViewById(R.id.etpulse);
								etrespiratory=(EditText)v.findViewById(R.id.etrespiratory);
								ettemperature=(EditText)v.findViewById(R.id.ettemperature);
								etBloodGroup=(EditText)v.findViewById(R.id.etbloodgroup);
								etdiapressure=(EditText)v.findViewById(R.id.etbloodpressure);
								etsyspressure=(EditText)v.findViewById(R.id.etbloodpressure1);


								cbnormal=(CheckBox)v.findViewById(R.id.cbnormal);
								cbabnormal=(CheckBox)v.findViewById(R.id.cbabnormal);
								etexternalgenetalia=(EditText)v.findViewById(R.id.etexternalgenetalia);

								cblumpno=(CheckBox)v.findViewById(R.id.cblumpno);
								cblumpyes=(CheckBox)v.findViewById(R.id.cblumpyes);
								etlump=(EditText)v.findViewById(R.id.etlump);

								cbgalactorrheano=(CheckBox)v.findViewById(R.id.cbgalactorrheano);
								cbgalactorrheayes=(CheckBox)v.findViewById(R.id.cbgalactorrheayes);
								etgalactorrhea=(EditText)v.findViewById(R.id.etgalactorrhea);

								cbbreastotherno=(CheckBox)v.findViewById(R.id.cbotherno);
								cbbreastotheryes=(CheckBox)v.findViewById(R.id.cbotheryes);
								etbreastother=(EditText)v.findViewById(R.id.etother);

								cbwelldevelopedno=(CheckBox)v.findViewById(R.id.cbwelldevelopedno);
								cbwelldevelopedyes=(CheckBox)v.findViewById(R.id.cbwelldevelopedyes);
								etwelldeveloped=(EditText)v.findViewById(R.id.etwelldeveloped);

								cbhairno=(CheckBox)v.findViewById(R.id.cbhairno);
								cbhairyes=(CheckBox)v.findViewById(R.id.cbhairyes);
								ethair=(EditText)v.findViewById(R.id.ethair);

								cbacneno=(CheckBox)v.findViewById(R.id.cbacneno);
								cbacneyes=(CheckBox)v.findViewById(R.id.cbacneyes);
								etacne=(EditText)v.findViewById(R.id.etacne);

								cbsecondarysexotherno=(CheckBox)v.findViewById(R.id.cbotherno1);
								cbsecondarysexotheryes=(CheckBox)v.findViewById(R.id.cbotheryes1);
								etsecondarysexother=(EditText)v.findViewById(R.id.etother1);

								cbpreabdomennormal=(CheckBox)v.findViewById(R.id.cbabdomenno);
								cbpreabdomenabnormal=(CheckBox)v.findViewById(R.id.cbabdomenyes);
								etpreabdomen=(EditText)v.findViewById(R.id.etAbdomen);

								cbhealthyno=(CheckBox)v.findViewById(R.id.cbhealthyno);
								cbhealthyyes=(CheckBox)v.findViewById(R.id.cbhealthyyes);
								ethealthy=(EditText)v.findViewById(R.id.ethealthy);

								cbbleedingno=(CheckBox)v.findViewById(R.id.cbbleedingno);
								cbbleedingyes=(CheckBox)v.findViewById(R.id.cbbleedingyes);
								etbleeding=(EditText)v.findViewById(R.id.etbleeding);

								cblbcno=(CheckBox)v.findViewById(R.id.cblbcno);
								cblbcyes=(CheckBox)v.findViewById(R.id.cblbcyes);;
								etlbc=(EditText)v.findViewById(R.id.etlbc);;

								tvavaf=(TextView)v.findViewById(R.id.avafsize);
								tvrvrf=(TextView)v.findViewById(R.id.rvrfsize);
								etuterusothers=(EditText)v.findViewById(R.id.etvaginal);





								etweight.setEnabled(false);
								etheight.setEnabled(false);
								etspo.setEnabled(false);
								etpulse.setEnabled(false);
								etrespiratory.setEnabled(false);
								ettemperature.setEnabled(false);
								etdiapressure.setEnabled(false);
								etsyspressure.setEnabled(false);

								cbnormal.setEnabled(false);
								cbabnormal.setEnabled(false);
								etexternalgenetalia.setEnabled(false);

								cblumpno.setEnabled(false);
								cblumpyes.setEnabled(false);
								etlump.setEnabled(false);

								cbgalactorrheano.setEnabled(false);;
								cbgalactorrheayes.setEnabled(false);;
								etgalactorrhea.setEnabled(false);;

								cbbreastotherno.setEnabled(false);;
								cbbreastotheryes.setEnabled(false);;
								etbreastother.setEnabled(false);;

								cbwelldevelopedno.setEnabled(false);;
								cbwelldevelopedyes.setEnabled(false);;
								etwelldeveloped.setEnabled(false);;

								cbhairno.setEnabled(false);;
								cbhairyes.setEnabled(false);;
								ethair.setEnabled(false);;

								cbacneno.setEnabled(false);;
								cbacneyes.setEnabled(false);;
								etacne.setEnabled(false);;

								cbsecondarysexotherno.setEnabled(false);;
								cbsecondarysexotheryes.setEnabled(false);;
								etsecondarysexother.setEnabled(false);;

								cbpreabdomennormal.setEnabled(false);;
								cbpreabdomenabnormal.setEnabled(false);;
								etpreabdomen.setEnabled(false);;

								cbhealthyno.setEnabled(false);;
								cbhealthyyes.setEnabled(false);;
								ethealthy.setEnabled(false);;

								cbbleedingno.setEnabled(false);;
								cbbleedingyes.setEnabled(false);;
								etbleeding.setEnabled(false);;

								cblbcno.setEnabled(false);;
								cblbcyes.setEnabled(false);;
								etlbc.setEnabled(false);;

								tvavaf.setEnabled(false);;
								tvrvrf.setEnabled(false);;
								etuterusothers.setEnabled(false);;

								final ImageView arrow=(ImageView)v.findViewById(R.id.ivarrow);
								// spObj;



								arrow.setBackgroundResource(R.drawable.triangle_arrow);

								datecontainer.setOnClickListener(new OnClickListener() {

									@Override
									public void onClick(View arg0) {
										// TODO Auto-generated method stub
										if(container.getVisibility()==View.VISIBLE){
											container.setVisibility(View.GONE);
											arrow.setBackgroundResource(R.drawable.triangle_arrow);

										}else{
											arrow.setBackgroundResource(R.drawable.triangle_up);
											container.setVisibility(View.VISIBLE);
										}
									}
								});












								if(type==1){


									/*String fname=patientobject.getString("firstName");
									String lname=patientobject.getString("lastName");
									if(fname==null){
										fname="FirstName";
									}
									if(lname==null){
										lname="LastName";
									}*/

									if(patientobject.getString("createdDate")!=null){
										date.setText(patientobject.getString("createdDate"));
									}

									weight_result=patientobject.getString("weight");
									if(weight_result!=null){

										if(weight_result.toString()!=null && !(weight_result.toString().equals("Nil"))){
											etweight.setText(weight_result.toString());
										}
									}

									spo2_result=patientobject.getString("spo2");

									if(spo2_result!=null){

										if(spo2_result.toString()!=null && !(spo2_result.toString().equals("Nil"))){
											etspo.setText(spo2_result.toString());
										}
									}

									bmi=patientobject.getString("bmi");


									if(bmi!=null){

										if(bmi.toString()!=null && !(bmi.toString().equals("Nil"))){
											etbmi.setText(bmi.toString());
										}
									}


									pulse_result=patientobject.getString("pulse");


									if(pulse_result!=null){

										if(pulse_result.toString()!=null && !(pulse_result.toString().equals("Nil"))){
											etpulse.setText(pulse_result.toString());
										}
									}

									respRate_result=patientobject.getString("respiratoryrate");

									if(respRate_result!=null){

										if(respRate_result.toString()!=null && !(respRate_result.toString().equals("Nil"))){
											etrespiratory.setText(respRate_result.toString());
										}
									}


									height_result=patientobject.getString("height");


									if(height_result!=null){

										if(height_result.toString()!=null && !(height_result.toString().equals("Nil"))){
											etheight.setText(height_result.toString());
										}
									}


									bloodGroup_result=patientobject.getString("bloodgroup");


									if(bloodGroup_result!=null){

										if(bloodGroup_result.toString()!=null && !(bloodGroup_result.toString().equals("Nil"))){
											etBloodGroup.setText(bloodGroup_result.toString());
											etBloodGroup.setEnabled(false);
										}

									}

									bloodPressure_result=patientobject.getString("bloodpressure");
									String pressure[]=null;

									if(bloodPressure_result!=null){
										pressure=patientobject.getString("bloodpressure").split("/");
										if(!(bloodPressure_result.equals("Nil"))){
											if(pressure[0]!=null){
												etsyspressure.setText(pressure[0].toString());
											}
											if(pressure[1]!=null){
												etdiapressure.setText(pressure[1].toString());
											}
										}
									}

									temp_result=patientobject.getString("temperature");	


									if(temp_result!=null){
										if(temp_result.toString()!=null && !(temp_result.toString().equals("Nil"))){
											ettemperature.setText(temp_result.toString());
										}

									}
									/*update_date=patientobject.getJSONArray("testedDate");
									if(update_date!=null){
										String result="";
										try{
											result=update_date.getString(update_date.length()-1);
										}catch(Exception e){
											e.printStackTrace();
										}
									}
									currentvitals_id=patientobject.getJSONArray("currentvitalsId");
									((FragmentActivityContainer)getActivity()).getpatientfrag().onButtonactivation(0);*/




									if(patientobject.getString("externalgenetalia")!=null){
										String externalgenetalia[]=patientobject.getString("externalgenetalia").split(",");
										if(externalgenetalia[0].equalsIgnoreCase("Normal")){
											cbnormal.setChecked(true);
										}else if(externalgenetalia[0].equalsIgnoreCase("Abnormal")){
											cbabnormal.setChecked(true);
										}if(!externalgenetalia[1].equalsIgnoreCase("Nil")){
											etexternalgenetalia.setText(externalgenetalia[1]);
										}
									}


									if(patientobject.getString("breast_lump")!=null){
										String breastlump[]=patientobject.getString("breast_lump").split(",");
										if(breastlump[0].equalsIgnoreCase("No")){
											cblumpno.setChecked(true);
										}else if(breastlump[0].equalsIgnoreCase("Yes")){
											cblumpyes.setChecked(true);
										}if(!breastlump[1].equalsIgnoreCase("Nil")){
											etlump.setText(breastlump[1]);
										}
									}



									if(patientobject.getString("breast_galactorrhea")!=null){
										String breastgalactorrhea[]=patientobject.getString("breast_galactorrhea").split(",");
										if(breastgalactorrhea[0].equalsIgnoreCase("No")){
											cbgalactorrheano.setChecked(true);
										}else if(breastgalactorrhea[0].equalsIgnoreCase("Yes")){
											cbgalactorrheayes.setChecked(true);
										}if(!breastgalactorrhea[1].equalsIgnoreCase("Nil")){
											etgalactorrhea.setText(breastgalactorrhea[1]);
										}
									}





									if(patientobject.getString("breast_other")!=null){
										String breastother[]=patientobject.getString("breast_other").split(",");
										if(breastother[0].equalsIgnoreCase("No")){
											cbbreastotherno.setChecked(true);
										}else if(breastother[0].equalsIgnoreCase("Yes")){
											cbbreastotheryes.setChecked(true);
										}if(!breastother[1].equalsIgnoreCase("Nil")){
											etbreastother.setText(breastother[1]);
										}
									}






									if(patientobject.getString("secondarysex_welldeveloped")!=null){
										String secondarysex_welldeveloped[]=patientobject.getString("secondarysex_welldeveloped").split(",");
										if(secondarysex_welldeveloped[0].equalsIgnoreCase("No")){
											cbwelldevelopedno.setChecked(true);
										}else if(secondarysex_welldeveloped[0].equalsIgnoreCase("Yes")){
											cbwelldevelopedyes.setChecked(true);
										}if(!secondarysex_welldeveloped[1].equalsIgnoreCase("Nil")){
											etwelldeveloped.setText(secondarysex_welldeveloped[1]);
										}
									}




									if(patientobject.getString("secondarysex_hair")!=null){
										String secondarysex_hair[]=patientobject.getString("secondarysex_hair").split(",");
										if(secondarysex_hair[0].equalsIgnoreCase("No")){
											cbhairno.setChecked(true);
										}else if(secondarysex_hair[0].equalsIgnoreCase("Yes")){
											cbhairyes.setChecked(true);
										}if(!secondarysex_hair[1].equalsIgnoreCase("Nil")){
											ethair.setText(secondarysex_hair[1]);
										}
									}



									if(patientobject.getString("secondarysex_acne")!=null){
										String secondarysex_acne[]=patientobject.getString("secondarysex_acne").split(",");
										if(secondarysex_acne[0].equalsIgnoreCase("No")){
											cbacneno.setChecked(true);
										}else if(secondarysex_acne[0].equalsIgnoreCase("Yes")){
											cbacneyes.setChecked(true);
										}if(!secondarysex_acne[1].equalsIgnoreCase("Nil")){
											etacne.setText(secondarysex_acne[1]);
										}
									}





									if(patientobject.getString("secondarysex_other")!=null){
										String secondarysex_other[]=patientobject.getString("secondarysex_other").split(",");
										if(secondarysex_other[0].equalsIgnoreCase("No")){
											cbsecondarysexotherno.setChecked(true);
										}else if(secondarysex_other[0].equalsIgnoreCase("Yes")){
											cbsecondarysexotheryes.setChecked(true);
										}if(!secondarysex_other[1].equalsIgnoreCase("Nil")){
											etsecondarysexother.setText(secondarysex_other[1]);
										}
									}





									if(patientobject.getString("preabdomenexamination")!=null){
										String preabdomenexamination[]=patientobject.getString("preabdomenexamination").split(",");
										if(preabdomenexamination[0].equalsIgnoreCase("Normal")){
											cbpreabdomenabnormal.setChecked(true);
										}else if(preabdomenexamination[0].equalsIgnoreCase("Abnormal")){
											cbpreabdomenabnormal.setChecked(true);
										}if(!preabdomenexamination[1].equalsIgnoreCase("Nil")){
											etpreabdomen.setText(preabdomenexamination[1]);
										}
									}






									if(patientobject.getString("cervix_healthy")!=null){
										String cervix_healthy[]=patientobject.getString("cervix_healthy").split(",");
										if(cervix_healthy[0].equalsIgnoreCase("No")){
											cbhealthyno.setChecked(true);
										}else if(cervix_healthy[0].equalsIgnoreCase("Yes")){
											cbhealthyyes.setChecked(true);
										}if(!cervix_healthy[1].equalsIgnoreCase("Nil")){
											ethealthy.setText(cervix_healthy[1]);
										}
									}







									if(patientobject.getString("cervix_bleeding")!=null){
										String cervix_bleeding[]=patientobject.getString("cervix_bleeding").split(",");
										if(cervix_bleeding[0].equalsIgnoreCase("No")){
											cbbleedingno.setChecked(true);
										}else if(cervix_bleeding[0].equalsIgnoreCase("Yes")){
											cbbleedingyes.setChecked(true);
										}if(!cervix_bleeding[1].equalsIgnoreCase("Nil")){
											etbleeding.setText(cervix_bleeding[1]);
										}
									}








									if(patientobject.getString("cervix_lbc")!=null){
										String cervix_lbc[]=patientobject.getString("cervix_lbc").split(",");
										if(cervix_lbc[0].equalsIgnoreCase("No")){
											cblbcno.setChecked(true);
										}else if(cervix_lbc[0].equalsIgnoreCase("Yes")){
											cblbcyes.setChecked(true);
										}if(!cervix_lbc[1].equalsIgnoreCase("Nil")){
											etlbc.setText(cervix_lbc[1]);
										}
									}






									if(patientobject.getString("uterus_avaf")!=null){
										String uterus_avaf=patientobject.getString("uterus_avaf");
										if(!uterus_avaf.equalsIgnoreCase("Nil")){
											tvavaf.setText(uterus_avaf);
										}
									}






									if(patientobject.getString("uterus_rvrf")!=null){
										String uterus_rvrf=patientobject.getString("uterus_rvrf");
										if(!uterus_rvrf.equalsIgnoreCase("Nil")){
											tvrvrf.setText(uterus_rvrf);
										}
									}






									if(patientobject.getString("uterus_others")!=null){
										String uterus_others=patientobject.getString("uterus_others");
										if(!uterus_others.equalsIgnoreCase("Nil")){
											etuterusothers.setText(uterus_others);
										}	
									}


									if(weight_result!=null ||
											spo2_result!=null || 
											bmi!=null ||
											pulse_result!=null||
											respRate_result!=null ||
											height_result!=null ||
											bloodGroup_result!=null ||
											bloodPressure_result!=null ||
											temp_result!=null ||
											patientobject.getString("externalgenetalia")!=null||
											patientobject.getString("breast_lump")!=null||
											patientobject.getString("breast_galactorrhea")!=null||
											patientobject.getString("breast_other")!=null||
											patientobject.getString("secondarysex_welldeveloped")!=null||
											patientobject.getString("secondarysex_hair")!=null||
											patientobject.getString("secondarysex_acne")!=null||
											patientobject.getString("secondarysex_other")!=null||
											patientobject.getString("preabdomenexamination")!=null||
											patientobject.getString("cervix_healthy")!=null||
											patientobject.getString("cervix_bleeding")!=null||
											patientobject.getString("cervix_lbc")!=null||
											patientobject.getString("uterus_avaf")!=null||
											patientobject.getString("uterus_rvrf")!=null||
											patientobject.getString("uterus_others")!=null){
										layoutcontainer.addView(v);
									}









								}



							}



						}
					}


				}


			}
		});



	}




}
