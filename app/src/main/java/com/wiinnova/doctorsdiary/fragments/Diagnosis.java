package com.wiinnova.doctorsdiary.fragments;


import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.MultiAutoCompleteTextView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.wiinnova.doctorsdiary.R;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.SaveCallback;
import com.wiinnova.doctorsdiary.activities.FragmentActivityContainer;
import com.wiinnova.doctorsdiary.activities.Home_Page_Activity;
import com.wiinnova.doctorsdiary.fragment.CrashReporter;
import com.wiinnova.doctorsdiary.fragment.CurrentVitals.onNameListener;
import com.wiinnova.doctorsdiary.fragment.Diagnosis_Frag_Gynacologist;
import com.wiinnova.doctorsdiary.fragment.Diagnosis_Frag_Gynacologist.ondiagnosiscreate;
import com.wiinnova.doctorsdiary.fragment.Patient_Current_Medical_Info_Frag;
import com.wiinnova.doctorsdiary.popupwindow.SuspectedDiseaseSelectorPopup;
import com.wiinnova.doctorsdiary.popupwindow.SuspectedDiseaseSelectorPopup.onSubmitListenerSds;
import com.wiinnova.doctorsdiary.supportclasses.Diseasenameclass;

import org.json.JSONArray;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class Diagnosis extends Fragment implements Serializable,ondiagnosiscreate{

	public interface onbuttondiagnosis{
		void onDiagnosisbtn(int arg);
	}

	private ScrollView topScrollview;
	String formattedDate;
	private long currentDateandTime;
	JSONArray diagnosis_id;
	int diagnosisidcount;
	String diagnosisid;
	String diagnosisObjectid;
	ParseObject diagnosisId_patientObject;
	Button btnSubmit;
	Patient_Current_Medical_Info_Frag obj;
	ImageView rlPowerd;
	Button cclear,csave;
	RelativeLayout rlSpinnerBtn;
	TextView flName,uniqueID;

	String pid;
	TextView currDate;
	EditText synd,addCmt;
	MultiAutoCompleteTextView symp;
	Spinner suspDis;
	//	ArrayList<Patient_info_Store> arrayOfpatinfo = new ArrayList<Patient_info_Store>();
	//Diagnosis_SusDisease_Spinner spinner;
	TextView spinner;
	String s,symptoms,syndromes,addiCom,uniId;
	ProgressDialog mProgressDialog;
	SimpleDateFormat dateformat;
	SimpleDateFormat df;
	public static onNameListener submitListener;
	List<ParseObject> result=new ArrayList<ParseObject>();


	RelativeLayout topcontainer;

	SuspectedDiseaseSelectorPopup sdObj;
	String disease="";
	String diseaseicd="";
	ArrayList<Diseasenameclass> disease_array=new ArrayList<Diseasenameclass>();
	ArrayList<Diseasenameclass> selected_disease_array=new ArrayList<Diseasenameclass>();

	ParseObject patient;
	ParseObject patientdiagnosisparseObj;
	ParseObject patientattended;
	ArrayList<String> diseasenames=new ArrayList<String>();
	ArrayList<String> diseaseicdnumber=new ArrayList<String>();
	ArrayList<Diseasenameclass> diseasenamearraylist=new ArrayList<Diseasenameclass>();
	ArrayList<Diseasenameclass> setdiseasenamearraylist=new ArrayList<Diseasenameclass>();
	private onSubmitListenerSds diagnosisListener;
	private Diagnosis_Frag_Gynacologist diagfrag;

	List<ParseObject> _suspecteddiseasenamesObj;
	List<ParseObject> _symptomsnamesObj;

	String symptomsname[];
	private String specialization;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {


		View view=inflater.inflate(R.layout.diagnosis_nephro, null);


		_suspecteddiseasenamesObj=((CrashReporter)getActivity().getApplicationContext()).getDiseasenameObjects();
		_symptomsnamesObj=((CrashReporter)getActivity().getApplicationContext()).getSymptomsnameObjects();

		symptomsname=new String[_symptomsnamesObj.size()];

		System.out.println("symptomsname size"+_symptomsnamesObj.size());
		for(int k=0;k<_symptomsnamesObj.size();k++){
			symptomsname[k]=_symptomsnamesObj.get(k).getString("Symptom");
		}

		ArrayAdapter adapter = new ArrayAdapter(getActivity(),android.R.layout.simple_list_item_1,symptomsname);

		System.out.println("diseasenamesObj size"+_suspecteddiseasenamesObj.size());



		/*if(getFragmentManager().findFragmentById(R.id.fl_sidemenu_container) instanceof Diagnosis_Frag_Gynacologist){

			//	((FragmentActivityContainer)getActivity()).getdiagnosis_create_colorchange();

		}else{
			diagfrag = new Diagnosis_Frag_Gynacologist();
			getFragmentManager().beginTransaction()
			.replace(R.id.fl_sidemenu_container, diagfrag)
			.commit();

			if(diagfrag!=null){
				((FragmentActivityContainer)getActivity()).setDiagfrag_gynacologist(diagfrag);
			}
		}*/


		((FragmentActivityContainer)getActivity()).setdiagnosiscreatefrag_nephro(this);

		((FragmentActivityContainer)getActivity()).getDiagfrag_nephro().onDiagnosisbtn(0);
		patientdiagnosisparseObj=((FragmentActivityContainer)getActivity()).getpatientdiagnosisparseObj();
		patientattended=((FragmentActivityContainer)getActivity()).getpatientdiagnosisparseObj();


		SharedPreferences sp=getActivity().getSharedPreferences("Login", 0);
		specialization=sp.getString("spel", null);

		if(specialization.equalsIgnoreCase("gynecologist obstetrician")){
			((FragmentActivityContainer)getActivity()).getDiagfrag_gynacologist().setmenuitemvalue(2);
		}

		submitListener = new onNameListener() {

			@Override
			public void onSubmit(String arg) {
				// TODO Auto-generated method stub
				flName.setText(arg);

			}
		};

		String[] array = {"Select Disease","Arthritis", "Asthma", "Chlamydia","Flu","Heart Disease","Meningitis","Tuberculosis(TB)" };  

		topScrollview=(ScrollView)view.findViewById(R.id.srTopscrollview);
		btnSubmit=(Button)view.findViewById(R.id.btnSubmit);
		spinner=(TextView)view.findViewById(R.id.mySpinner2);
		spinner.setMovementMethod(new ScrollingMovementMethod());
		topcontainer=(RelativeLayout)view.findViewById(R.id.topContainer);
		//Button
		cclear=(Button)view.findViewById(R.id.crtclear);
		csave=(Button)view.findViewById(R.id.crtsave);

		//EditText
		symp=(MultiAutoCompleteTextView)view.findViewById(R.id.crtsymedit);
		synd=(EditText)view.findViewById(R.id.crtsynedit);
		addCmt=(EditText)view.findViewById(R.id.crtadcmtdit);

		//TextView
		currDate=(TextView)view.findViewById(R.id.cdcurrdate);
		flName=(TextView)view.findViewById(R.id.tvfirstlastName);
		uniqueID=(TextView)view.findViewById(R.id.cduniqueid);

		rlSpinnerBtn=(RelativeLayout)view.findViewById(R.id.rlSpinner);

		//Current Date
		Calendar c = Calendar.getInstance();
		System.out.println("Current time => " + c.getTime());
		df = new SimpleDateFormat("dd-MMM-yyyy");
		formattedDate = df.format(c.getTime());
		currentDateandTime=c.getTimeInMillis();
		currDate.setText(formattedDate);

		dateformat = new SimpleDateFormat("dd-MM-yyyy", Locale.US);

		diagnosisListener=new onSubmitListenerSds() {

			@Override
			public void onSubmit(ArrayList<Diseasenameclass> selectedItems) {
				// TODO Auto-generated method stub
				doOnSubmit(selectedItems);
			}
		};


		symp.setAdapter(adapter);
		symp.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());

		symp.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
				// TODO Auto-generated method stub

			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
					int arg3) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				if(symp.getText().length()==0 && synd.getText().length()==0 && spinner.getText().length()==0 && addCmt.getText().length()==0){
					submitButtonDeactivation();
				}else{
					submitButtonActivation();
				}

			}
		});

		synd.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				if(symp.getText().length()==0 && synd.getText().length()==0 && addCmt.getText().length()==0){
					submitButtonDeactivation();
				}else{
					submitButtonActivation();
				}
			}
		});


		addCmt.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

				if(symp.getText().length()==0 && synd.getText().length()==0  && addCmt.getText().length()==0){
					submitButtonDeactivation();
				}else{
					submitButtonActivation();
				}
			}
		});

		spinner.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

				if(symp.getText().length()==0 && synd.getText().length()==0  && addCmt.getText().length()==0){
					submitButtonDeactivation();
				}else{
					submitButtonActivation();
				}

				if(spinner.getText().length()!=0){
					submitButtonActivation();
				}
			}
		});




		rlSpinnerBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				spinner.setText("");
				disease="";
				diseaseicd="";
				diseasenamearraylist.clear();

				if(_suspecteddiseasenamesObj==null || _suspecteddiseasenamesObj.size()==0){

					fetechingdiseasenames_server();	

				}else{
					
					Diseasenameclass diseasenameObjhyphen=new Diseasenameclass();
					diseasenameObjhyphen.setDiseasename("-");
					diseasenameObjhyphen.setIcdnumber("-");
					diseasenamearraylist.add(diseasenameObjhyphen);
					
					Diseasenameclass diseasenameObjnone=new Diseasenameclass();
					diseasenameObjnone.setDiseasename("None");
					diseasenameObjnone.setIcdnumber("none");
					diseasenamearraylist.add(diseasenameObjnone);

					Diseasenameclass diseasenameObjother=new Diseasenameclass();
					diseasenameObjother.setDiseasename("Other");
					diseasenameObjother.setIcdnumber("Other");
					diseasenamearraylist.add(diseasenameObjother);



					for(int i=0;i<_suspecteddiseasenamesObj.size();i++){
						Diseasenameclass diseasenameObj=new Diseasenameclass();
						System.out.println("working..."+_suspecteddiseasenamesObj.get(i).getString("DiseaseName"));
						diseasenameObj.setDiseasename(_suspecteddiseasenamesObj.get(i).getString("DiseaseName"));
						diseasenameObj.setIcdnumber(_suspecteddiseasenamesObj.get(i).getString("ICDNumber"));
						diseasenamearraylist.add(diseasenameObj);

					}

					sdObj=new SuspectedDiseaseSelectorPopup();
					sdObj.setSubmitListenerSds(diagnosisListener);

					Bundle bundle=new Bundle();

					bundle.putSerializable("diseasenamearray",diseasenamearraylist);
					bundle.putSerializable("selecteddisease", disease_array);
					sdObj.setArguments(bundle);
					sdObj.show(getFragmentManager(), "tag");

				}




			}
		});


		if(Home_Page_Activity.patient!=null){

			System.out.println("patient not null");
			pid=Home_Page_Activity.patient.getString("patientID");

			String fname=Home_Page_Activity.patient.getString("firstName");
			String lname=Home_Page_Activity.patient.getString("lastName");
			if(fname==null){
				fname="FirstName";
			}
			if(lname==null){
				lname="LastName";
			}

			flName.setText(fname+" "+lname);
			uniqueID.setText(pid);


		}else{

			SharedPreferences scanpidnew=getActivity().getSharedPreferences("NewPatID", 0);
			String scnewPid=scanpidnew.getString("NewID", null); 

			SharedPreferences scanpid1=getActivity().getSharedPreferences("ScannedPid", 0);

			String scPid=scanpid1.getString("scanpid", null);       
			System.out.println("scanpersonal id"+scnewPid);

			if(scPid!=null){

				pid=scPid;
				uniqueID.setText(pid);


				SharedPreferences fnname1=getActivity().getSharedPreferences("fnName", 0);
				flName.setText(fnname1.getString("fNname","FirstName"+" "+"LastName"));  



			}

			if(scnewPid!=null){
				pid=scnewPid;
				uniqueID.setText(pid);


				SharedPreferences fnname1=getActivity().getSharedPreferences("fnName", 0);

				flName.setText(fnname1.getString("fNname","FirstName"+" "+"LastName"));  

			}
		}

		topcontainer.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				hideKeyboard(v);
			}
		});

		topScrollview.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				hideKeyboard(v);
			}
		});


		topScrollview.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent arg1) {
				// TODO Auto-generated method stub
				hideKeyboard(v);
				return false;
			}
		});
		cclear.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				Clear();

			}
		});

		return view;

	}














	private void fetechingdiseasenames_server() {
		// TODO Auto-generated method stub


		mProgressDialog = new ProgressDialog(getActivity());
		mProgressDialog.setMessage("Getting Disease Details...");
		mProgressDialog.setCancelable(false);
		mProgressDialog.show();


		ParseQuery<ParseObject> query = ParseQuery.getQuery("DiseaseDatabase");
		query.fromLocalDatastore();

		query.setLimit(1000);
		query.whereNotEqualTo("DiseaseName",null);
		query.findInBackground(new FindCallback<ParseObject>() {

			@Override
			public void done(List<ParseObject> objects, ParseException e) {

				// TODO Auto-generated method stub
				if (e == null) {
					if (objects.size() > 0) {
						// query found a user
						Log.e("Login", "Id exist");
						patient=objects.get(0);


						for(int i=0;i<objects.size();i++){
							Diseasenameclass diseasenameObj=new Diseasenameclass();
							diseasenameObj.setDiseasename(objects.get(i).getString("DiseaseName"));
							diseasenameObj.setIcdnumber(objects.get(i).getString("ICDNumber"));
							diseasenamearraylist.add(diseasenameObj);

						}
						System.out.println("objects size"+objects.size());
						diseaseselect_server();


					}
					else {

						mProgressDialog.dismiss();
						Toast msg=Toast.makeText(getActivity(), "Disease Data not Avilable.please try again.....", Toast.LENGTH_LONG);
						msg.show();

					}
				} 
				else {
					Toast.makeText(getActivity(), "Sorry, something went wrong", Toast.LENGTH_LONG).show();
				}

			}


		});


	}

	private void diseaseselect_server() {
		// TODO Auto-generated method stub

		ParseQuery<ParseObject> query = ParseQuery.getQuery("DiseaseDatabase");
		query.fromLocalDatastore();
		query.setLimit(589);
		query.setSkip(1000);
		query.whereNotEqualTo("DiseaseName",null);
		query.findInBackground(new FindCallback<ParseObject>() {

			@Override
			public void done(List<ParseObject> objects, ParseException e) {
				mProgressDialog.dismiss();
				// TODO Auto-generated method stub
				if (e == null) {
					if (objects.size() > 0) {
						Log.e("Login", "Id exist");
						patient=objects.get(0);


						for(int i=0;i<objects.size();i++){
							Diseasenameclass diseasenameObj=new Diseasenameclass();
							diseasenameObj.setDiseasename(objects.get(i).getString("DiseaseName"));
							diseasenameObj.setIcdnumber(objects.get(i).getString("ICDNumber"));
							diseasenamearraylist.add(diseasenameObj);
						}


						sdObj=new SuspectedDiseaseSelectorPopup();
						sdObj.setSubmitListenerSds(diagnosisListener);
						if(diseasenamearraylist!=null)
						{
							((FragmentActivityContainer)getActivity()).setdiseasenamefromparse(diseasenamearraylist);

						}
						Bundle bundle=new Bundle();

						System.out.println("disease array size....sssssize.."+diseasenamearraylist.size());

						bundle.putSerializable("diseasenamearray",diseasenamearraylist);
						bundle.putSerializable("selecteddisease", disease_array);
						sdObj.setArguments(bundle);
						sdObj.show(getFragmentManager(), "tag");
					}
					else {
						mProgressDialog.dismiss();
						Toast msg=Toast.makeText(getActivity(), "Disease Data not Avilable.please try again.....", Toast.LENGTH_LONG);
						msg.show();
					}
				} 
				else {
					Toast.makeText(getActivity(), "Sorry, something went wrong", Toast.LENGTH_LONG).show();
				}
			}
		});
	}




	protected void hideKeyboard(View view) {
		InputMethodManager in = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
		in.hideSoftInputFromWindow(view.getWindowToken(),
				InputMethodManager.HIDE_NOT_ALWAYS);
	}

	public void Clear() {
		diseaseicd="";
	}

	private void submitButtonActivation() {
		// TODO Auto-generated method stub
		Diagnosis_Frag_Gynacologist diagnosissidefrag=((FragmentActivityContainer)getActivity()).getDiagfrag_gynacologist();
		if(diagnosissidefrag!=null)
			((FragmentActivityContainer)getActivity()).getDiagfrag_gynacologist().onDiagnosisbtn(1);
	}
	private void submitButtonDeactivation() {
		// TODO Auto-generated method stub
		Diagnosis_Frag_Gynacologist diagnosissidefrag=((FragmentActivityContainer)getActivity()).getDiagfrag_gynacologist();
		if(diagnosissidefrag!=null)
			((FragmentActivityContainer)getActivity()).getDiagfrag_gynacologist().onDiagnosisbtn(0);
	}


	public void doOnSubmit(ArrayList<Diseasenameclass> selectedItems) {
		// TODO Auto-generated method stub
		disease_array=selectedItems;


		for(int i=0;i<selectedItems.size();i++){
			System.out.println("selected items disease"+selectedItems.get(i).getDiseasename());
			System.out.println("selected items diseaseicd"+selectedItems.get(i).getIcdnumber());
			disease+=selectedItems.get(i).getDiseasename()+"\n";
			diseaseicd+=selectedItems.get(i).getIcdnumber()+",";


		}
		System.out.println("disease icd string"+diseaseicd);
		spinner.setText(disease);
		sdObj.dismiss();
	}

	@Override
	public void ondiagnosis(int arg) {
		// TODO Auto-generated method stub
		System.out.println("diagonosis workingggggggggg");
		symptoms=symp.getText().toString();
		syndromes=synd.getText().toString();
		addiCom=addCmt.getText().toString();

		uniId=uniqueID.getText().toString();

		if(spinner.getText().length()==0){
			Toast.makeText(getActivity(), "Select Suspected Disease", Toast.LENGTH_SHORT).show();
		}

		if(spinner.getText().length()!=0){
			System.out.println("disease icd"+diseaseicd);
			if(diseaseicd.length()==0){
				if(patientdiagnosisparseObj.getString("suspectedDisease")!=null){
					diseaseicd=patientdiagnosisparseObj.getString("suspectedDisease");
					System.out.println("check working"+diseaseicd);
				}
			}
			System.out.println("disease icd"+diseaseicd);
			mProgressDialog = new ProgressDialog(getActivity());
			mProgressDialog.setCancelable(false);
			mProgressDialog.setMessage("Storing Diagnosis Data....");
			mProgressDialog.show();


			if(patientdiagnosisparseObj==null){
				System.out.println("object nullllllllllllllllllllllllllllllll");

				patientdiagnosisparseObj=new ParseObject("Diagnosis");
				patientattended=new ParseObject("Diagnosis");
			}

			ParseQuery<ParseObject> query = ParseQuery.getQuery("Diagnosis");
			query.fromLocalDatastore();
			query.whereEqualTo("patientID",uniId);
			query.findInBackground(new FindCallback<ParseObject>() {



				@Override
				public void done(List<ParseObject> objects, ParseException e) {
					// TODO Auto-generated method stub
					if(e==null){

						System.out.println("id......."+uniId);
						Date followupdate;
						result.clear();
						result=objects;
						for(int i=0;i<result.size();i++){
							System.out.println("followup result"+result.get(i).getString("followup"));

						}


						int objectsize=objects.size()+1;
						System.out.println("objectssize"+objectsize);
						diagnosisObjectid=uniId.concat(Integer.toString(objectsize));
						patientdiagnosisparseObj.put("patientID", uniId);
						patientdiagnosisparseObj.put("typeFlag", 1);
						patientdiagnosisparseObj.put("symptoms", symptoms);
						patientdiagnosisparseObj.put("syndromes", syndromes);
						patientdiagnosisparseObj.put("suspectedDisease", diseaseicd);
						patientdiagnosisparseObj.put("additionalComments", addiCom);
						patientdiagnosisparseObj.put("createdDate",formattedDate);
						patientdiagnosisparseObj.put("createdatetime",currentDateandTime);

						if(patientdiagnosisparseObj.getString("diagnosisId")==null){
							patientdiagnosisparseObj.put("diagnosisId",diagnosisObjectid);
						}else{
							diagnosisObjectid=patientdiagnosisparseObj.getString("diagnosisId");
						}

						patientdiagnosisparseObj.pinInBackground(new SaveCallback() {

							public void done(ParseException e) {
								mProgressDialog.dismiss();
								if (e == null) {
									myObjectSavedSuccessfully();

									((FragmentActivityContainer)getActivity()).setDiagnosisobject_id(diagnosisObjectid);
									if(((FragmentActivityContainer)getActivity()).getCurrentvitalsObjectid()!=null){
										ParseQuery<ParseObject> query = ParseQuery.getQuery("Patients");
										query.fromLocalDatastore();
										query.whereEqualTo("patientID",((FragmentActivityContainer)getActivity()).getCurrentvitalsObjectid());
										query.findInBackground(new FindCallback<ParseObject>() {
											@Override
											public void done(List<ParseObject> object, ParseException e) {
												// TODO Auto-generated method stub
												if(e==null){
													diagnosisId_patientObject=object.get(0);
													diagnosisId_patientObject.addUnique("diagnosisObjectid", diagnosisObjectid);
													diagnosisId_patientObject.pinInBackground(new SaveCallback() {
														@Override
														public void done(ParseException e) {
															// TODO Auto-generated method stub
															if(e==null){
																System.out
																.println("successfully objectid saved");
															}
														}
													});
													diagnosisId_patientObject.saveEventually();


												}
											}
										});
									}
								} else {
									myObjectSaveDidNotSucceed();
								}
							}

							private void myObjectSaveDidNotSucceed() {
								// TODO Auto-generated method stub
								Toast msg1=Toast.makeText(getActivity(), "Save Failed", Toast.LENGTH_LONG);
								msg1.show();

							}

							private void myObjectSavedSuccessfully() {
								// TODO Auto-generated method stub

								Date currentdate;
								Date followupdate;

								ArrayList<Date> appointeddate=new ArrayList<Date>();
								for(int i=0;i<result.size();i++){
									System.out.println("reult.size"+result.size());
									try {
										System.out.println("followup result1"+result.get(i).getString("patientID"));
										System.out.println("followup result"+result.get(i).getString("followup"));
										if(result.get(i).getString("followup")!=""  && result.get(i).getString("followup")!=null){
											appointeddate.add((Date)dateformat.parse(result.get(i).getString("followup")));
											currentdate=(Date)df.parse(formattedDate);
											followupdate=(Date)dateformat.parse(result.get(i).getString("followup"));

											System.out.println("current date"+currentdate);
											System.out.println("followup date"+followupdate);
											if(currentdate.compareTo(followupdate)==0){

												result.get(i).get("patientID");
												patientattended=result.get(i);
												result.get(i).put("patientattend",true);
												result.get(i).pinInBackground(new SaveCallback() {
													@Override
													public void done(ParseException e) {
														// TODO Auto-generated method stub
														if(e==null){
															
															System.out.println("suuccessfully saved");
														}

													}
												});

												result.get(i).saveEventually();
											}
										}
									} catch (java.text.ParseException e1) {
										// TODO Auto-generated catch block
										e1.printStackTrace();
									}
								}




//								FragmentActivityContainer.diag_save_check=0;
								submitButtonDeactivation();
								((FragmentActivityContainer)getActivity()).setpatientdiagnosisparseObj(patientdiagnosisparseObj);
								Toast msg=Toast.makeText(getActivity(), "Saved", Toast.LENGTH_LONG);
								msg.show();
								Clear();

							}
						});

						patientdiagnosisparseObj.saveEventually();
					}

				}
			});
		}
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

		CrashReporter.getInstance().trackScreenView("Diagnosis Creation");
		if(patientdiagnosisparseObj!=null ){
			if(patientdiagnosisparseObj.getString("suspectedDisease")!=null){
				symp.setText(patientdiagnosisparseObj.getString("symptoms"));
				synd.setText(patientdiagnosisparseObj.getString("syndromes"));
				addCmt.setText(patientdiagnosisparseObj.getString("additionalComments"));

				setdiseasenamearraylist=((FragmentActivityContainer)getActivity()).getdiseasenamefromparse();


				//	setdiseasedetails();

				if(_suspecteddiseasenamesObj==null || _suspecteddiseasenamesObj.size()==0){

					setdiseasedetails();

				}else{

					/*	mProgressDialog = new ProgressDialog(getActivity());
					mProgressDialog.setMessage("Retrieving Diagnosis Data....");
					mProgressDialog.setCancelable(false);
					mProgressDialog.show();
					 */
					diseasenamearraylist.clear();
					for(int i=0;i<_suspecteddiseasenamesObj.size();i++){
						Diseasenameclass diseasenameObj=new Diseasenameclass();
						System.out.println("working..."+_suspecteddiseasenamesObj.get(i).getString("DiseaseName"));
						diseasenameObj.setDiseasename(_suspecteddiseasenamesObj.get(i).getString("DiseaseName"));
						diseasenameObj.setIcdnumber(_suspecteddiseasenamesObj.get(i).getString("ICDNumber"));
						diseasenamearraylist.add(diseasenameObj);

					}
					if(patientdiagnosisparseObj.getString("suspectedDisease")!=null){
						if(patientdiagnosisparseObj.getString("suspectedDisease").length()!=0){
							String[] value = (patientdiagnosisparseObj.getString("suspectedDisease").split(","));
							if(value!=null){
								String[]  myaccrArray = null;
								if(value!=null){
									myaccrArray = new String[value.length];
									System.out.println("diseasenamearraylist size"+diseasenamearraylist.size());
									for(int j = 0; j < value.length; j++){
										String disease="";
										if(value[0].equalsIgnoreCase("-")){
											disease+="-";
										}
										else if(value[0].equalsIgnoreCase("None")){
											disease+="None";
										}else{
											
											
											for(int k=0;k<diseasenamearraylist.size();k++)
											{
												if((diseasenamearraylist.get(k).getIcdnumber())!=null){
													String icd_from_list=diseasenamearraylist.get(k).getIcdnumber().toString().trim();
													String icd_from_selected=value[j].toString().trim();

													if(icd_from_list.equalsIgnoreCase(icd_from_selected)){
														myaccrArray[j]=diseasenamearraylist.get(k).getDiseasename();

														Diseasenameclass diseaseclassObj=new Diseasenameclass();
														diseaseclassObj.setIcdnumber(icd_from_selected);
														diseaseclassObj.setDiseasename(diseasenamearraylist.get(k).getDiseasename());
														disease_array.add(diseaseclassObj);
														
														
													}else if(icd_from_selected.equalsIgnoreCase("Other")){
														myaccrArray[j]="Other";
													}
													
												}
											}

											for(int k=0;k<myaccrArray.length;k++){
												disease+=myaccrArray[k]+"\n";
											}
											
											//mProgressDialog.dismiss();
										}
										spinner.setText(disease);
									}
								}
							}
						}
					}
				}
			}
		}
	}

	public void setdiseasedetails(){

		if(patientdiagnosisparseObj.getString("suspectedDisease")!=null){
			if(patientdiagnosisparseObj.getString("suspectedDisease").length()!=0){
				String[] value = (patientdiagnosisparseObj.getString("suspectedDisease").split(","));
				final ArrayList<String> diseasenamecheck=new ArrayList<String>();
				for(int i=0;i<value.length;i++){
					diseasenamecheck.add(value[i]);
				}

				ParseQuery<ParseObject> query = ParseQuery.getQuery("DiseaseDatabase");
				query.fromLocalDatastore();
				query.whereContainedIn("ICDNumber",diseasenamecheck);
				query.findInBackground(new FindCallback<ParseObject>() {

					@Override
					public void done(List<ParseObject> object, ParseException e) {
						// TODO Auto-generated method stub
						mProgressDialog.dismiss();
						if(e==null){
							if(object.size()>0){
								String disease="";
								for(int diseasesize=0;diseasesize<object.size();diseasesize++){
									System.out.println("names disease"+object.get(diseasesize).getString("DiseaseName"));
									disease+=object.get(diseasesize).getString("DiseaseName")+"\n";
									Diseasenameclass diseaseclassObj=new Diseasenameclass();
									diseaseclassObj.setIcdnumber(diseasenamecheck.get(diseasesize));
									diseaseclassObj.setDiseasename(object.get(diseasesize).getString("DiseaseName"));
									disease_array.add(diseaseclassObj);

								}
								spinner.setText(disease);
							}
						}


					}
				});
			}
		}
	}
}
