package com.wiinnova.doctorsdiary.fragments;

import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.TextView;

import com.wiinnova.doctorsdiary.R;
import com.wiinnova.doctorsdiary.activities.FragmentActivityContainer;
import com.wiinnova.doctorsdiary.activities.Home_Page_Activity;

public class DiagnosisLabData extends Fragment implements OnClickListener{
	
	private CheckBox cbHb;
	private CheckBox cbTLC;
	private CheckBox cbDLC;
	private CheckBox cbPlatelets;
	private CheckBox cbBloodg;
	private CheckBox cbRhType;
	private CheckBox cbSCreatinine;
	private CheckBox cbSBilirubin;
	private CheckBox cbRbs;
	private CheckBox cbfbs;
	private CheckBox cb2Hrpp;
	private CheckBox cbHbaic;
	private CheckBox cbLft;
	private CheckBox cbRft;
	private CheckBox cbNaPlus;
	private CheckBox cbKPlus;
	private CheckBox cbBt;
	private CheckBox cbCt;
	private CheckBox cbPtPc;
	private CheckBox cbAptt;
	private CheckBox cbFibrinogen;
	private CheckBox cbMiniLipidProfile;
	private CheckBox cbFreeT3;
	private CheckBox cbFreeT34;
	private CheckBox cbThs;
	private CheckBox cbProlactin;
	private CheckBox cbHIV;
	private CheckBox cbHbsag;
	private CheckBox cbHcv;
	private CheckBox cbLsh;
	private CheckBox cbLh;
	private CheckBox cbE2;
	private CheckBox cbUrineRoutineMe;
	private CheckBox cbUrineC;
	private CheckBox cbStoolRoutineMe;
	private CheckBox cbStoolC;
	private CheckBox cbVitBLevel;
	private CheckBox cbVitDLevel;
	private CheckBox cbSIron;
	private CheckBox cbSProtein;
	private CheckBox cbTiBc;
	private CheckBox cbSAlbumin;
	private CheckBox cbSGlobulin;
	private CheckBox cbSFerritin;
	private CheckBox cbAGRatio;
	private CheckBox cbCXRAYPa;
	private CheckBox cbUsKub;
	private TextView flName;
	private TextView uniqueID;
	private String pid;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view=inflater.inflate(R.layout.labdata_nephro, null);
		
		flName=(TextView)view.findViewById(R.id.tvfirstlastName);
		uniqueID=(TextView)view.findViewById(R.id.cduniqueid);
		
		cbHb = (CheckBox) view.findViewById(R.id.cb_hb);
		cbTLC = (CheckBox) view.findViewById(R.id.cb_tlc);
		cbDLC = (CheckBox) view.findViewById(R.id.cb_dlc);
		cbPlatelets = (CheckBox) view.findViewById(R.id.cb_platelets);
		cbBloodg = (CheckBox) view.findViewById(R.id.cb_bgroup);
		cbRhType = (CheckBox) view.findViewById(R.id.cb_rhtype);
		cbSCreatinine = (CheckBox) view.findViewById(R.id.cb_screatinine);
		cbSBilirubin = (CheckBox) view.findViewById(R.id.cb_sbilirubin);
		cbRbs = (CheckBox) view.findViewById(R.id.cb_rbs);
		cbfbs = (CheckBox) view.findViewById(R.id.cb_fbs);
		cb2Hrpp = (CheckBox) view.findViewById(R.id.cb_2hrpp);
		cbHbaic = (CheckBox) view.findViewById(R.id.cb_hbaic);
		cbLft = (CheckBox) view.findViewById(R.id.cb_lft);
		cbRft = (CheckBox) view.findViewById(R.id.cb_rft);
		cbNaPlus = (CheckBox) view.findViewById(R.id.cb_naplus);
		cbKPlus = (CheckBox) view.findViewById(R.id.cb_kplus);
		cbBt = (CheckBox) view.findViewById(R.id.cb_bt);
		cbCt = (CheckBox) view.findViewById(R.id.cb_ct);
		cbPtPc = (CheckBox) view.findViewById(R.id.cb_ptpc);
		cbAptt = (CheckBox) view.findViewById(R.id.cb_aptt);
		cbFibrinogen = (CheckBox) view.findViewById(R.id.cb_fibrinogen);
		cbMiniLipidProfile = (CheckBox) view.findViewById(R.id.cb_mini_lipidp);
		cbFreeT3 = (CheckBox) view.findViewById(R.id.cb_freet3);
		cbFreeT34 = (CheckBox) view.findViewById(R.id.cb_freet4);
		cbThs = (CheckBox) view.findViewById(R.id.cb_ths);
		cbProlactin = (CheckBox) view.findViewById(R.id.cb_prolactin);
		cbHIV = (CheckBox) view.findViewById(R.id.cb_hiv);
		cbHbsag = (CheckBox) view.findViewById(R.id.cb_hbsag);
		cbHcv = (CheckBox) view.findViewById(R.id.cb_hcv);
		cbLsh = (CheckBox) view.findViewById(R.id.cb_lsh);
		cbLh = (CheckBox) view.findViewById(R.id.cb_lh);
		cbE2 = (CheckBox) view.findViewById(R.id.cb_e2);
		cbUrineRoutineMe = (CheckBox) view.findViewById(R.id.cb_urinerme);
		cbUrineC = (CheckBox) view.findViewById(R.id.cb_urinec);
		cbStoolRoutineMe = (CheckBox) view.findViewById(R.id.cb_stoolrme);
		cbStoolC = (CheckBox) view.findViewById(R.id.cb_stoolc);
		cbVitBLevel = (CheckBox) view.findViewById(R.id.cb_vitblevel);
		cbVitDLevel = (CheckBox) view.findViewById(R.id.cb_vitdlevel);
		cbSIron = (CheckBox) view.findViewById(R.id.cb_siron);
		cbSProtein = (CheckBox) view.findViewById(R.id.cb_sprotein);
		cbTiBc = (CheckBox) view.findViewById(R.id.cb_tibc);
		cbSAlbumin = (CheckBox) view.findViewById(R.id.cb_salbumin);
		cbSGlobulin = (CheckBox) view.findViewById(R.id.cb_sglobulin);
		cbSFerritin = (CheckBox) view.findViewById(R.id.cb_sferritin);
		cbAGRatio = (CheckBox) view.findViewById(R.id.cb_agratio);
		cbCXRAYPa = (CheckBox) view.findViewById(R.id.cb_cxraypa);
		cbUsKub = (CheckBox) view.findViewById(R.id.cb_uskub);
		
		
		if(Home_Page_Activity.patient!=null){

			System.out.println("patient not null");
			pid=Home_Page_Activity.patient.getString("patientID");

			String fname=Home_Page_Activity.patient.getString("firstName");
			String lname=Home_Page_Activity.patient.getString("lastName");
			if(fname==null){
				fname="FirstName";
			}
			if(lname==null){
				lname="LastName";
			}

			flName.setText(fname+" "+lname);
			uniqueID.setText(pid);


		}else{

			SharedPreferences scanpidnew=getActivity().getSharedPreferences("NewPatID", 0);
			String scnewPid=scanpidnew.getString("NewID", null); 
			SharedPreferences scanpid1=getActivity().getSharedPreferences("ScannedPid", 0);
			String scPid=scanpid1.getString("scanpid", null);       
			System.out.println("scanpersonal id"+scnewPid);

			if(scPid!=null){
				pid=scPid;
				uniqueID.setText(pid);
				SharedPreferences fnname1=getActivity().getSharedPreferences("fnName", 0);
				flName.setText(fnname1.getString("fNname","FirstName"+" "+"LastName"));  
			}

			if(scnewPid!=null){
				pid=scnewPid;
				uniqueID.setText(pid);
				SharedPreferences fnname1=getActivity().getSharedPreferences("fnName", 0);
				flName.setText(fnname1.getString("fNname","FirstName"+" "+"LastName"));  
			}
		}
		
		
		cbHb.setOnClickListener(this);
		cbTLC.setOnClickListener(this); 
		cbDLC.setOnClickListener(this); 
		cbPlatelets.setOnClickListener(this); 
		cbBloodg.setOnClickListener(this); 
		cbRhType.setOnClickListener(this); 
		cbSCreatinine.setOnClickListener(this); 
		cbSBilirubin.setOnClickListener(this);
		cbRbs.setOnClickListener(this);
		cbfbs.setOnClickListener(this);
		cb2Hrpp.setOnClickListener(this);
		cbHbaic.setOnClickListener(this);
		cbLft.setOnClickListener(this); 
		cbRft.setOnClickListener(this); 
		cbNaPlus.setOnClickListener(this); 
		cbKPlus.setOnClickListener(this);
		cbBt.setOnClickListener(this);
		cbCt.setOnClickListener(this);
		cbPtPc.setOnClickListener(this); 
		cbAptt.setOnClickListener(this); 
		cbFibrinogen .setOnClickListener(this);
		cbMiniLipidProfile.setOnClickListener(this);
		cbFreeT3.setOnClickListener(this);
		cbFreeT34.setOnClickListener(this); 
		cbThs.setOnClickListener(this); 
		cbProlactin.setOnClickListener(this);
		cbHIV.setOnClickListener(this);
		cbHbsag.setOnClickListener(this);
		cbHcv.setOnClickListener(this);
		cbLsh.setOnClickListener(this);
		cbLh.setOnClickListener(this);
		cbE2.setOnClickListener(this);
		cbUrineRoutineMe.setOnClickListener(this);
		cbUrineC.setOnClickListener(this);
		cbStoolRoutineMe.setOnClickListener(this);
		cbStoolC.setOnClickListener(this);
		cbVitBLevel.setOnClickListener(this);
		cbVitDLevel.setOnClickListener(this);
		cbSIron.setOnClickListener(this);
		cbSProtein.setOnClickListener(this);
		cbTiBc.setOnClickListener(this);
		cbSAlbumin.setOnClickListener(this); 
		cbSGlobulin.setOnClickListener(this);
		cbSFerritin.setOnClickListener(this);
		cbAGRatio.setOnClickListener(this);
		cbCXRAYPa.setOnClickListener(this);
		cbUsKub.setOnClickListener(this);
		
		
		
		return view;
	}
	@Override
	public void onClick(View view) {
		// TODO Auto-generated method stub
		
		switch (view.getId()) {
		case R.id.cb_hb:
			submitButtonActivation();
			break;
		case R.id.cb_tlc:
			submitButtonActivation();
			break;
		case R.id.cb_dlc:
			submitButtonActivation();
			break;
		case R.id.cb_platelets:
			submitButtonActivation();
			break;
		case R.id.cb_bgroup:
			submitButtonActivation();
			break;
		case R.id.cb_rhtype:
			submitButtonActivation();
			break;
		case R.id.cb_screatinine:
			submitButtonActivation();
			break;
		case R.id.cb_sbilirubin:
			submitButtonActivation();
			break;
		case R.id.cb_rbs:
			submitButtonActivation();
			break;
		case R.id.cb_fbs:
			submitButtonActivation();
			break;
		case R.id.cb_2hrpp:
			submitButtonActivation();
			break;
		case R.id.cb_hbaic:
			submitButtonActivation();
			break;
		case R.id.cb_lft:
			submitButtonActivation();
			break;
		case R.id.cb_rft:
			submitButtonActivation();
			break;
		case R.id.cb_naplus:
			submitButtonActivation();
			break;
		case R.id.cb_kplus:
			submitButtonActivation();
			break;
		case R.id.cb_bt:
			submitButtonActivation();
			break;
		case R.id.cb_ct:
			submitButtonActivation();
			break;
		case R.id.cb_ptpc:
			submitButtonActivation();
			break;
		case R.id.cb_aptt:
			submitButtonActivation();
			break;
		case R.id.cb_fibrinogen:
			submitButtonActivation();
			break;
		case R.id.cb_mini_lipidp:
			submitButtonActivation();
			break;
		case R.id.cb_freet3:
			submitButtonActivation();
			break;
		case R.id.cb_freet4:
			submitButtonActivation();
			break;
		case R.id.cb_ths:
			submitButtonActivation();
			break;
		case R.id.cb_prolactin:
			submitButtonActivation();
			break;
		case R.id.cb_hiv:
			submitButtonActivation();
			break;
		case R.id.cb_hbsag:
			submitButtonActivation();
			break;
		case R.id.cb_hcv:
			submitButtonActivation();
			break;
		case R.id.cb_lsh:
			submitButtonActivation();
			break;
		case R.id.cb_lh:
			submitButtonActivation();
			break;
		case R.id.cb_e2:
			submitButtonActivation();
			break;
		case R.id.cb_urinerme:
			submitButtonActivation();
			break;
		case R.id.cb_urinec:
			submitButtonActivation();
			break;
		case R.id.cb_stoolrme:
			submitButtonActivation();
			break;
		case R.id.cb_stoolc:
			submitButtonActivation();
			break;
		case R.id.cb_vitblevel:
			submitButtonActivation();
			break;
		case R.id.cb_vitdlevel:
			submitButtonActivation();
			break;
		case R.id.cb_siron:
			submitButtonActivation();
			break;
		case R.id.cb_sprotein:
			submitButtonActivation();
			break;
		case R.id.cb_tibc:
			submitButtonActivation();
			break;
		case R.id.cb_salbumin:
			submitButtonActivation();
			break;
		case R.id.cb_sglobulin:
			submitButtonActivation();
			break;
		case R.id.cb_sferritin:
			submitButtonActivation();
			break;
		case R.id.cb_agratio:
			submitButtonActivation();
			break;
		case R.id.cb_cxraypa:
			submitButtonActivation();
			break;
		case R.id.cb_uskub:
			submitButtonActivation();
			break;

		default:
			break;
		}
		
	}

		
	protected void hideKeyboard(View view) {
		InputMethodManager in = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
		in.hideSoftInputFromWindow(view.getWindowToken(),
				InputMethodManager.HIDE_NOT_ALWAYS);
	}
	private void submitButtonActivation() {
		// TODO Auto-generated method stub
		DiagnosisSideMenu diagnosissidefrag=((FragmentActivityContainer)getActivity()).getDiagfrag_nephro();
		if(diagnosissidefrag!=null)
			((FragmentActivityContainer)getActivity()).getDiagfrag_nephro().onDiagnosisbtn(1);
	}
	private void submitButtonDeactivation() {
		// TODO Auto-generated method stub
		DiagnosisSideMenu diagnosissidefrag=((FragmentActivityContainer)getActivity()).getDiagfrag_nephro();
		if(diagnosissidefrag!=null)
			((FragmentActivityContainer)getActivity()).getDiagfrag_nephro().onDiagnosisbtn(0);
	}


	


}
