package com.wiinnova.doctorsdiary.fragments;

import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.NumberPicker;
import android.widget.NumberPicker.OnValueChangeListener;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.wiinnova.doctorsdiary.R;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.SaveCallback;
import com.wiinnova.doctorsdiary.activities.FragmentActivityContainer;
import com.wiinnova.doctorsdiary.fragment.CrashReporter;
import com.wiinnova.doctorsdiary.fragment.CurrentVitals;
import com.wiinnova.doctorsdiary.fragment.NavigationDrawerFragment;
import com.wiinnova.doctorsdiary.fragment.Patient_Current_Medical_Info_Frag;
import com.wiinnova.doctorsdiary.fragment.Patient_Frag;
import com.wiinnova.doctorsdiary.fragment.Patient_Frag.onPersonalsubmit;
import com.wiinnova.doctorsdiary.popupwindow.SpinnerPopup;
import com.wiinnova.doctorsdiary.popupwindow.SpinnerPopup.onSubmitListener;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class PatientpersonalInfo extends Fragment implements Serializable, onPersonalsubmit, OnValueChangeListener {
    int flag;


    public interface onbuttonactivation {
        void onButtonactivation(int arg);
    }


    PatientMedicalaHistory patientmedicalhistoryfragObj;
    CurrentVitals pntcurrentvitals;

    SpinnerPopup spObj;
    Button personalNext;
    EditText pfName, pmName, plName, pstreetName, pcityName, ppinCode, pphone, pemail, pid, pfAdhaar;
    TextView psex, pstutus, pcountry, pstate;

    static TextView pyearB;
    //ArrayList<Patient_info_Store> arrayOfpatinfo = new ArrayList<Patient_info_Store>();
    String patientId;
    ProgressDialog mProgressDialog;
    ImageView ivPowerd;

    TextView tv_flname;
    TextView tv_id;
    TextView tv_lastname;
    NavigationDrawerFragment navfragment;
    ListView listview;
    String[] year_array;
    String[] sex_array;
    String[] mstatus_array;
    String[] birth_array;
    String[] country_array;
    String[] state_array;
    String[] careOf_array;

    LinearLayout containertop;
    private boolean doubleBackToExitPressedOnce;
    boolean patientflag = true;
    boolean patientedit_checkflag = true;

    Button btnSubmit;
    String pfirstname;

    String pmiddlename;
    String plastname;
    String padhaarno;
    String pstrtName;
    String pCity;
    String pPin;
    String pState;
    String pPhone;
    String peMail;
    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    String pSex;
    String pStatus;
    String pYearB;
    String pCountry;
    String pAge;


    String fln;
    CurrentVitals pnt_currmedinfo;

    ParseObject patientnameobj;
    Patient_Frag patientfrag;

    private SimpleDateFormat dateFormatter;
    private DatePickerDialog startDatePickerDialog;
    private onSubmitListener spinnerpopupListener;
    private Patient_Frag patientfragObj;
    private boolean admintab;
    private boolean admintabdefault;

    private EditText tvAge;
    private int year;

    int thisYear;
    private EditText reffName;
    private EditText reflName;
    private TextView careOf;
    private EditText careOfFname;
    private EditText careOfMname;
    private EditText careOfLname;
    private TextView occupation;
    private EditText attendantName;
    private String[] occupation_array;
    private String pReffFname;
    private String pReffLname;
    private String pCareof;
    private String pCareoffirstname;
    private String pCareofmiddlename;
    private String pCareoflastname;
    private String pOccupation;
    private String pAttendantName;
    private ScrollView svContainer;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        sex_array = getResources().getStringArray(R.array.sex_array);
        mstatus_array = getResources().getStringArray(R.array.mstatus_array);
        birth_array = getResources().getStringArray(R.array.ybirth);
        country_array = getResources().getStringArray(R.array.countries_array);
        state_array = getResources().getStringArray(R.array.state_array);
        careOf_array = getResources().getStringArray(R.array.careof_array);
        occupation_array = getResources().getStringArray(R.array.occupation_array);

        View view = inflater.inflate(R.layout.patient_personalinfo_nephro, null);

        Calendar calendar = Calendar.getInstance();
        thisYear = calendar.get(Calendar.YEAR);

		/*int size=thisYear-1950;
        year_array=new String[size+1];
		for(int i=1950;i<=thisYear;i++){
			year_array[i-1950]=Integer.toString(i);
		}
		*/


        year_array = ((FragmentActivityContainer) getActivity()).getyeararray();

        patientfragObj = new Patient_Frag();

        Bundle bundle = getArguments();
        patientflag = bundle.getBoolean("patientflag");
        //admintab=bundle.getBoolean("admintab");

        Bundle bundle1 = new Bundle();
        bundle1.putInt("sidemenuitem", 1);
        bundle1.putBoolean("patientflag", patientflag);
        patientfragObj.setArguments(bundle1);
        getFragmentManager().beginTransaction()
                .replace(R.id.fl_sidemenu_container, patientfragObj)
                .commit();

        if (patientfragObj != null) {
            System.out.println("set working..........");
            ((FragmentActivityContainer) getActivity()).setpatientfrag(patientfragObj);
        } else {
            System.out.println("null..................");
        }

        ((FragmentActivityContainer) getActivity()).setPntprsnlinfo_nephro(this);

        //Button
        btnSubmit = (Button) view.findViewById(R.id.btnSubmit);
        //EditText
        dateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.US);

        containertop = (LinearLayout) view.findViewById(R.id.topcontainer);
        svContainer = (ScrollView) view.findViewById(R.id.sv_container);
        reffName = (EditText) view.findViewById(R.id.et_ref_fname);
        reflName = (EditText) view.findViewById(R.id.et_ref_lname);
        pfName = (EditText) view.findViewById(R.id.pfname);
        pmName = (EditText) view.findViewById(R.id.pmname);
        plName = (EditText) view.findViewById(R.id.plname);
        pstreetName = (EditText) view.findViewById(R.id.psrtname);
        pcityName = (EditText) view.findViewById(R.id.pcname);
        ppinCode = (EditText) view.findViewById(R.id.ppin);
        pphone = (EditText) view.findViewById(R.id.pphone);
        pemail = (EditText) view.findViewById(R.id.et_email);
        pfAdhaar = (EditText) view.findViewById(R.id.pfidno);
        tvAge = (EditText) view.findViewById(R.id.pAge);
        careOfFname = (EditText) view.findViewById(R.id.et_pcareof_fname);
        careOfMname = (EditText) view.findViewById(R.id.et_pcareof_mname);
        careOfLname = (EditText) view.findViewById(R.id.et_pcareof_lname);
        attendantName = (EditText) view.findViewById(R.id.et_attendant_name);


        tv_flname = (TextView) view.findViewById(R.id.pheadfirstname);
        tv_id = (TextView) view.findViewById(R.id.cduniqueid);
        tv_lastname = (TextView) view.findViewById(R.id.pheadlastname);


        //Spinner
        psex = (TextView) view.findViewById(R.id.psex);
        pstutus = (TextView) view.findViewById(R.id.pmstatus);
        pyearB = (TextView) view.findViewById(R.id.pybirth);
        pcountry = (TextView) view.findViewById(R.id.pcountry);
        pstate = (TextView) view.findViewById(R.id.pstate);
        occupation = (TextView) view.findViewById(R.id.tv_occupation);
        careOf = (TextView) view.findViewById(R.id.pcareof);


        ((FragmentActivityContainer) getActivity()).setsubmitbuttonActivation(0);

        try {

            ((FragmentActivityContainer) getActivity()).getpatientfrag().menu_item = 1;

            patientedit_checkflag = ((FragmentActivityContainer) getActivity()).getpatientflagvalue();
            admintab = ((FragmentActivityContainer) getActivity()).getcheckadmintab();
            admintabdefault = ((FragmentActivityContainer) getActivity()).getadmintabdefault();

            if (admintab == true) {
                System.out.println("admin tab working.....");
                patientnameobj = ((FragmentActivityContainer) getActivity()).getOldPatientobject();
            } else if (admintab == false && admintabdefault == false) {
                patientnameobj = ((FragmentActivityContainer) getActivity()).getPatientobject();
            } else if (admintab == false && admintabdefault == true) {
                patientnameobj = ((FragmentActivityContainer) getActivity()).getOldPatientobject();
            }

        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(getActivity(), "getfragment error1", Toast.LENGTH_LONG).show();
        }


        if (patientedit_checkflag == false) {
            patientflag = false;
        }
        System.out.println("patient flag value" + patientflag);

        if (patientflag == true) {

            if (patientnameobj != null) {
                FragmentActivityContainer.check_save = 0;
                patientId = patientnameobj.getString("patientID");
                tv_id.setText(patientId);
                System.out.println("scan patientid0" + patientId);
                String fname = patientnameobj.getString("firstName");
                String lname = patientnameobj.getString("lastName");
                System.out.println("name........" + fname);

                tv_flname.setText(fname + " ");
                tv_lastname.setText(lname);
            }

        }


        spinnerpopupListener = new onSubmitListener() {

            @Override
            public void onSubmit(String arg, int position) {
                // TODO Auto-generated method stub
                if (flag == 0) {
                    psex.setText(arg);
                } else if (flag == 1) {
                    pstutus.setText(arg);
                } else if (flag == 2) {
                    pcountry.setText(arg);
                } else if (flag == 3) {
                    pstate.setText(arg);
                } else if (flag == 4) {
                    pyearB.setText(arg);
                    int age = thisYear - Integer.parseInt(arg);
                    tvAge.setText(String.valueOf(age));
                } else if (flag == 5) {
                    careOf.setText(arg);

                } else if (flag == 6) {
                    occupation.setText(arg);

                }
                spObj.dismiss();
            }
        };

        tvAge.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
                // TODO Auto-generated method stub

//				 String age =(String) arg0;
                if (arg0.toString().length() > 0) {
                    int year = thisYear - Integer.parseInt(arg0.toString());
                    pyearB.setText(year + "");
                } else {
                    pyearB.setText(thisYear + "");
                }


            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

                ((FragmentActivityContainer) getActivity()).addsubmitbuttonActivation();
            }
        });


        psex.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
                // TODO Auto-generated method stub

            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

                ((FragmentActivityContainer) getActivity()).addsubmitbuttonActivation();
            }
        });

        careOf.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
                // TODO Auto-generated method stub

            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

                ((FragmentActivityContainer) getActivity()).addsubmitbuttonActivation();
            }
        });
        occupation.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
                // TODO Auto-generated method stub

            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

                ((FragmentActivityContainer) getActivity()).addsubmitbuttonActivation();
            }
        });

        pstutus.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
                // TODO Auto-generated method stub

            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
                if (reffName.getText().length() == 0 && reflName.getText().length() == 0 && pfName.getText().length() == 0 && pmName.getText().length() == 0 && plName.getText().length() == 0 && pfAdhaar.getText().length() == 0 && psex.getText().length() == 0 && pyearB.getText().length() == 0 && pstutus.getText().length() == 0 && pstreetName.getText().length() == 0 && pcityName.getText().length() == 0 && pstate.getText().length() == 0 && ppinCode.getText().length() == 0 && pcountry.getText().length() == 0 && pphone.getText().length() == 0 && pemail.getText().length() == 0) {
                    submitButtonDeactivation();
                } else {
                    submitButtonActivation();
                }
            }
        });

        pyearB.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
                if (reffName.getText().length() == 0 && reflName.getText().length() == 0 && pfName.getText().length() == 0 && pmName.getText().length() == 0 && plName.getText().length() == 0 && pfAdhaar.getText().length() == 0 && psex.getText().length() == 0 && pyearB.getText().length() == 0 && pstutus.getText().length() == 0 && pstreetName.getText().length() == 0 && pcityName.getText().length() == 0 && pstate.getText().length() == 0 && ppinCode.getText().length() == 0 && pcountry.getText().length() == 0 && pphone.getText().length() == 0 && pemail.getText().length() == 0) {
                    submitButtonDeactivation();
                } else {
                    submitButtonActivation();
                }
            }
        });

        pcountry.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

                if (reffName.getText().length() == 0 && reflName.getText().length() == 0 && pfName.getText().length() == 0 && pmName.getText().length() == 0 && plName.getText().length() == 0 && pfAdhaar.getText().length() == 0 && psex.getText().length() == 0 && pyearB.getText().length() == 0 && pstutus.getText().length() == 0 && pstreetName.getText().length() == 0 && pcityName.getText().length() == 0 && pstate.getText().length() == 0 && ppinCode.getText().length() == 0 && pcountry.getText().length() == 0 && pphone.getText().length() == 0 && pemail.getText().length() == 0) {
                    submitButtonDeactivation();
                } else {
                    submitButtonActivation();
                }
            }
        });

        reffName.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

                if (reffName.getText().length() == 0 && reflName.getText().length() == 0 && pfName.getText().length() == 0 && pmName.getText().length() == 0 && plName.getText().length() == 0 && pfAdhaar.getText().length() == 0 && psex.getText().length() == 0 && pyearB.getText().length() == 0 && pstutus.getText().length() == 0 && pstreetName.getText().length() == 0 && pcityName.getText().length() == 0 && pstate.getText().length() == 0 && ppinCode.getText().length() == 0 && pcountry.getText().length() == 0 && pphone.getText().length() == 0 && pemail.getText().length() == 0) {
                    submitButtonDeactivation();
                } else {
                    submitButtonActivation();
                }

            }
        });

        //Edittext text changed Listener
        pfName.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub

                if (count == 0) {
                    submitButtonDeactivation();
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

                tv_flname.setText(s + " ");
                if (reffName.getText().length() == 0 && reflName.getText().length() == 0 && pfName.getText().length() == 0 && pmName.getText().length() == 0 && plName.getText().length() == 0 && pfAdhaar.getText().length() == 0 && psex.getText().length() == 0 && pyearB.getText().length() == 0 && pstutus.getText().length() == 0 && pstreetName.getText().length() == 0 && pcityName.getText().length() == 0 && pstate.getText().length() == 0 && ppinCode.getText().length() == 0 && pcountry.getText().length() == 0 && pphone.getText().length() == 0 && pemail.getText().length() == 0) {
                    submitButtonDeactivation();
                } else {
                    submitButtonActivation();
                }
            }
        });

        plName.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2, int count) {
                // TODO Auto-generated method stub
                if (count == 0) {
                    submitButtonDeactivation();
                }

            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
                tv_lastname.setText(s);

                if (reffName.getText().length() == 0 && reflName.getText().length() == 0 && pfName.getText().length() == 0 && pmName.getText().length() == 0 && plName.getText().length() == 0 && pfAdhaar.getText().length() == 0 && psex.getText().length() == 0 && pyearB.getText().length() == 0 && pstutus.getText().length() == 0 && pstreetName.getText().length() == 0 && pcityName.getText().length() == 0 && pstate.getText().length() == 0 && ppinCode.getText().length() == 0 && pcountry.getText().length() == 0 && pphone.getText().length() == 0 && pemail.getText().length() == 0) {
                    submitButtonDeactivation();
                } else {
                    submitButtonActivation();
                }
            }


        });

        pmName.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2, int count) {
                // TODO Auto-generated method stub

                if (count == 0) {
                    submitButtonDeactivation();
                }

            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
                if (reffName.getText().length() == 0 && reflName.getText().length() == 0 && pfName.getText().length() == 0 && pmName.getText().length() == 0 && plName.getText().length() == 0 && pfAdhaar.getText().length() == 0 && psex.getText().length() == 0 && pyearB.getText().length() == 0 && pstutus.getText().length() == 0 && pstreetName.getText().length() == 0 && pcityName.getText().length() == 0 && pstate.getText().length() == 0 && ppinCode.getText().length() == 0 && pcountry.getText().length() == 0 && pphone.getText().length() == 0 && pemail.getText().length() == 0) {
                    submitButtonDeactivation();
                } else {
                    submitButtonActivation();
                }
            }
        });

        pfAdhaar.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                if (count == 0) {
                    submitButtonDeactivation();
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
                if (reffName.getText().length() == 0 && reflName.getText().length() == 0 && pfName.getText().length() == 0 && pmName.getText().length() == 0 && plName.getText().length() == 0 && pfAdhaar.getText().length() == 0 && psex.getText().length() == 0 && pyearB.getText().length() == 0 && pstutus.getText().length() == 0 && pstreetName.getText().length() == 0 && pcityName.getText().length() == 0 && pstate.getText().length() == 0 && ppinCode.getText().length() == 0 && pcountry.getText().length() == 0 && pphone.getText().length() == 0 && pemail.getText().length() == 0) {
                    submitButtonDeactivation();
                } else {
                    submitButtonActivation();
                }
            }
        });

        pstreetName.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                if (count == 0) {
                    submitButtonDeactivation();
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
                if (reffName.getText().length() == 0 && reflName.getText().length() == 0 && pfName.getText().length() == 0 && pmName.getText().length() == 0 && plName.getText().length() == 0 && pfAdhaar.getText().length() == 0 && psex.getText().length() == 0 && pyearB.getText().length() == 0 && pstutus.getText().length() == 0 && pstreetName.getText().length() == 0 && pcityName.getText().length() == 0 && pstate.getText().length() == 0 && ppinCode.getText().length() == 0 && pcountry.getText().length() == 0 && pphone.getText().length() == 0 && pemail.getText().length() == 0) {
                    submitButtonDeactivation();
                } else {
                    submitButtonActivation();
                }
            }
        });

        pcityName.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                if (count == 0) {
                    submitButtonDeactivation();
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
                if (reffName.getText().length() == 0 && reflName.getText().length() == 0 && pfName.getText().length() == 0 && pmName.getText().length() == 0 && plName.getText().length() == 0 && pfAdhaar.getText().length() == 0 && psex.getText().length() == 0 && pyearB.getText().length() == 0 && pstutus.getText().length() == 0 && pstreetName.getText().length() == 0 && pcityName.getText().length() == 0 && pstate.getText().length() == 0 && ppinCode.getText().length() == 0 && pcountry.getText().length() == 0 && pphone.getText().length() == 0 && pemail.getText().length() == 0) {
                    submitButtonDeactivation();
                } else {
                    submitButtonActivation();
                }
            }
        });

        ppinCode.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                if (count == 0) {
                    submitButtonDeactivation();
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
                if (reffName.getText().length() == 0 && reflName.getText().length() == 0 && pfName.getText().length() == 0 && pmName.getText().length() == 0 && plName.getText().length() == 0 && pfAdhaar.getText().length() == 0 && psex.getText().length() == 0 && pyearB.getText().length() == 0 && pstutus.getText().length() == 0 && pstreetName.getText().length() == 0 && pcityName.getText().length() == 0 && pstate.getText().length() == 0 && ppinCode.getText().length() == 0 && pcountry.getText().length() == 0 && pphone.getText().length() == 0 && pemail.getText().length() == 0) {
                    submitButtonDeactivation();
                } else {
                    submitButtonActivation();
                }
            }
        });

        pphone.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                if (count == 0) {
                    submitButtonDeactivation();
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
                if (reffName.getText().length() == 0 && reflName.getText().length() == 0 && pfName.getText().length() == 0 && pmName.getText().length() == 0 && plName.getText().length() == 0 && pfAdhaar.getText().length() == 0 && psex.getText().length() == 0 && pyearB.getText().length() == 0 && pstutus.getText().length() == 0 && pstreetName.getText().length() == 0 && pcityName.getText().length() == 0 && pstate.getText().length() == 0 && ppinCode.getText().length() == 0 && pcountry.getText().length() == 0 && pphone.getText().length() == 0 && pemail.getText().length() == 0) {
                    submitButtonDeactivation();
                } else {
                    submitButtonActivation();
                }
            }
        });

        pemail.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                if (count == 0) {
                    submitButtonDeactivation();
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

                if (reffName.getText().length() == 0 && reflName.getText().length() == 0 && pfName.getText().length() == 0 && pmName.getText().length() == 0 && plName.getText().length() == 0 && pfAdhaar.getText().length() == 0 && psex.getText().length() == 0 && pyearB.getText().length() == 0 && pstutus.getText().length() == 0 && pstreetName.getText().length() == 0 && pcityName.getText().length() == 0 && pstate.getText().length() == 0 && ppinCode.getText().length() == 0 && pcountry.getText().length() == 0 && pphone.getText().length() == 0 && pemail.getText().length() == 0) {
                    submitButtonDeactivation();
                } else {
                    submitButtonActivation();
                }
            }
        });

        pstate.addTextChangedListener(new PublicTextWatcher(pstate));


        SharedPreferences scanpidnew = getActivity().getSharedPreferences("NewPatID", 0);
        String scnewPid = scanpidnew.getString("NewID", null);


        careOf.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub

                flag = 5;

                Bundle bundle = new Bundle();
                bundle.putString("name", "Care of");
                bundle.putStringArray("items", careOf_array);
                spObj = new SpinnerPopup();
                spObj.setSubmitListener(spinnerpopupListener);
                spObj.setArguments(bundle);
                spObj.show(getFragmentManager(), "tag");
            }
        });
        occupation.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub

                flag = 6;

                Bundle bundle = new Bundle();
                bundle.putString("name", "Occupation");
                bundle.putStringArray("items", occupation_array);
                spObj = new SpinnerPopup();
                spObj.setSubmitListener(spinnerpopupListener);
                spObj.setArguments(bundle);
                spObj.show(getFragmentManager(), "tag");
            }
        });
        psex.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub

                flag = 0;

                Bundle bundle = new Bundle();
                bundle.putString("name", "Gender");
                bundle.putStringArray("items", sex_array);
                spObj = new SpinnerPopup();
                spObj.setSubmitListener(spinnerpopupListener);
                spObj.setArguments(bundle);
                spObj.show(getFragmentManager(), "tag");


            }
        });

        pyearB.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                /*System.out.println("working................");

				startDatePickerDialog.show();*/
                flag = 4;

                Bundle bundle = new Bundle();
                bundle.putString("name", "Year of Birth");
                bundle.putStringArray("items", year_array);
                spObj = new SpinnerPopup();
                spObj.setSubmitListener(spinnerpopupListener);
                spObj.setArguments(bundle);
                spObj.show(getFragmentManager(), "tag");

				
				/*show();*/

            }
        });


        Calendar newCalendar = Calendar.getInstance();


        startDatePickerDialog = new DatePickerDialog(getActivity(), new OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {


                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                pyearB.setText(dateFormatter.format(newDate.getTime()));

            }

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

        startDatePickerDialog.getDatePicker().setMaxDate(Calendar.getInstance().getTimeInMillis());


        pstutus.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub

                flag = 1;
                Bundle bundle = new Bundle();
                bundle.putString("name", "Marital Status");
                bundle.putStringArray("items", mstatus_array);
                spObj = new SpinnerPopup();
                spObj.setSubmitListener(spinnerpopupListener);
                spObj.setArguments(bundle);
                spObj.show(getFragmentManager(), "tag");


            }
        });


        pcountry.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub

                flag = 2;
                Bundle bundle = new Bundle();
                bundle.putString("name", "Country");
                bundle.putStringArray("items", country_array);
                spObj = new SpinnerPopup();
                spObj.setSubmitListener(spinnerpopupListener);
                spObj.setArguments(bundle);
                spObj.show(getFragmentManager(), "tag");

            }
        });

        pstate.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub

                flag = 3;
                Bundle bundle = new Bundle();
                bundle.putString("name", "State");
                bundle.putStringArray("items", state_array);
                spObj = new SpinnerPopup();
                spObj.setSubmitListener(spinnerpopupListener);
                spObj.setArguments(bundle);
                spObj.show(getFragmentManager(), "tag");

            }
        });


        if (patientnameobj == null) {
            if (scnewPid != null) {
                tv_id.setText(scnewPid);
                System.out.println("scan patid2" + scnewPid);
            }
        } else {
            tv_id.setText(patientnameobj.getString("patientID"));
            System.out.println("scan patid3" + patientnameobj.getString("patientID"));
        }

        containertop.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                hideKeyboard(v);
            }
        });

        svContainer.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub

                hideKeyboard(arg0);
            }
        });
        return view;
    }

    public class PublicTextWatcher implements TextWatcher {
        View view;

        public PublicTextWatcher(View view) {
            this.view = view;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            switch (view.getId()) {
                case R.id.pstate:
                    break;
            }

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            switch (view.getId()) {
                case R.id.pstate:
                    Log.d("TextChnged", "Nephrofgdgdfg");
                    if (reffName.getText().length() == 0 && reflName.getText().length() == 0 && pfName.getText().length() == 0 && pmName.getText().length() == 0 && plName.getText().length() == 0 && pfAdhaar.getText().length() == 0 && psex.getText().length() == 0 && pyearB.getText().length() == 0 && pstutus.getText().length() == 0 && pstreetName.getText().length() == 0 && pcityName.getText().length() == 0 && pstate.getText().length() == 0 && ppinCode.getText().length() == 0 && pcountry.getText().length() == 0 && pphone.getText().length() == 0 && pemail.getText().length() == 0) {
                        submitButtonDeactivation();
                    } else {
                        submitButtonActivation();
                    }
                    break;
            }
        }
    }

    @Override
    public void onResume() {
        // TODO Auto-generated method stub
        super.onResume();

        CrashReporter.getInstance().trackScreenView("Patient personal Information");

        if (patientedit_checkflag == false) {
            pfName.setEnabled(false);
            pmName.setEnabled(false);
            plName.setEnabled(false);
            pstreetName.setEnabled(false);
            pcityName.setEnabled(false);
            ppinCode.setEnabled(false);
            pstate.setEnabled(false);
            pphone.setEnabled(false);
            pemail.setEnabled(false);
            pfAdhaar.setEnabled(false);
            psex.setEnabled(false);
            pstutus.setEnabled(false);
            pyearB.setEnabled(false);
            pcountry.setEnabled(false);
            reffName.setEnabled(false);
            reflName.setEnabled(false);
            careOf.setEnabled(false);
            careOfFname.setEnabled(false);
            careOfMname.setEnabled(false);
            careOfLname.setEnabled(false);
            occupation.setEnabled(false);
            attendantName.setEnabled(false);
        }

        if (patientnameobj != null) {
            System.out.println("nameeeeeeeeeeeeeeeeeeeeee" + patientnameobj.getString("firstName"));
            pfName.setText(patientnameobj.getString("firstName"));

            pmName.setText(patientnameobj.getString("middleName"));
            plName.setText(patientnameobj.getString("lastName"));
            pstreetName.setText(patientnameobj.getString("streetName"));
            pcityName.setText(patientnameobj.getString("cityName"));
            ppinCode.setText(patientnameobj.getString("pinCode"));
            pstate.setText(patientnameobj.getString("state"));
            pphone.setText(patientnameobj.getString("phoneNumber"));
            pemail.setText(patientnameobj.getString("email"));
            pfAdhaar.setText(patientnameobj.getString("aadharnumber"));
            tvAge.setText(Integer.toString(patientnameobj.getInt("PatientAge")));
            reffName.setText(patientnameobj.getString("ReferredbyFname"));
            reflName.setText(patientnameobj.getString("ReferredbyLname"));
            careOfFname.setText(patientnameobj.getString("CareofFname"));
            careOfMname.setText(patientnameobj.getString("CareofMname"));
            careOfLname.setText(patientnameobj.getString("CareofLname"));
            attendantName.setText(patientnameobj.getString("AttendantName"));

            psex.setText(patientnameobj.getString("sex"));
            pstutus.setText(patientnameobj.getString("maritalStatus"));
            pyearB.setText(patientnameobj.getString("dob"));
            pcountry.setText(patientnameobj.getString("country"));
            careOf.setText(patientnameobj.getString("Careof"));
            occupation.setText(patientnameobj.getString("Occupation"));
            try {
                ((FragmentActivityContainer) getActivity()).getpatientfrag().onButtonactivation(0);
            } catch (Exception e) {
                e.printStackTrace();
                //	Toast.makeText(getActivity(), "getframent error", Toast.LENGTH_LONG).show();
            }
        }
    }


    private void set_textfield_values() {
        // TODO Auto-generated method stub
        System.out.println("set text working......." + pfirstname);
        if (pfirstname != null) {
            pfName.setText(pfirstname);
            pmName.setText(pmiddlename);
            plName.setText(plastname);
            pstreetName.setText(pstrtName);
            pcityName.setText(pCity);
            ppinCode.setText(pPin);
            pstate.setText(pState);
            pphone.setText(pPhone);
            pemail.setText(peMail);
            pfAdhaar.setText(padhaarno);
            psex.setText(pSex);
            pstutus.setText(pStatus);
            pyearB.setText(pYearB);
            pcountry.setText(pCountry);
        }
    }


    protected void hideKeyboard(View view) {
        InputMethodManager in = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        in.hideSoftInputFromWindow(view.getWindowToken(),
                InputMethodManager.HIDE_NOT_ALWAYS);
    }

    private void Save_patientPersonal_Information(final int targetfragment) {
        // TODO Auto-generated method stub

        SharedPreferences sp = getActivity().getSharedPreferences("Login", 0);
        String docterregId = sp.getString("docterRegnumber", null);


        SharedPreferences scanpidnew = getActivity().getSharedPreferences("NewPatID", 0);
        String scnewPid = scanpidnew.getString("NewID", null);
        SharedPreferences scanpid1 = getActivity().getSharedPreferences("ScannedPid", 0);
        String scPid = scanpid1.getString("scanpid", null);

        if (patientnameobj == null) {


            System.out.println("scanner id" + scPid);


            if (scPid != null || scnewPid != null) {

                if (scPid != null) {
                    //Log.e("scanner pid====>>", scPid);

                    patientId = scPid;
                }
                if (scnewPid != null) {
                    //Log.e("scanner pid====>>", scPid);

                    patientId = scnewPid;
                }

            }
        } else if (patientnameobj.getString("patientID") == null) {
            patientId = scnewPid;
        } else {
            patientId = patientnameobj.getString("patientID");
        }


        if (patientnameobj == null) {
            patientnameobj = new ParseObject("Patients");

        }

        mProgressDialog = new ProgressDialog(getActivity());

        mProgressDialog.setMessage("Storing Patient Data....");
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();


        patientnameobj.put("PatientAge", Integer.parseInt(pAge));
        patientnameobj.put("patientID", patientId);
        patientnameobj.put("firstName", pfirstname);
        patientnameobj.put("middleName", pmiddlename);
        patientnameobj.put("lastName", plastname);
        patientnameobj.put("aadharnumber", padhaarno);
        patientnameobj.put("sex", pSex);
        patientnameobj.put("maritalStatus", pStatus);
        patientnameobj.put("dob", pYearB);
        patientnameobj.put("streetName", pstrtName);
        patientnameobj.put("cityName", pCity);
        patientnameobj.put("pinCode", pPin);
        patientnameobj.put("state", pState);
        patientnameobj.put("country", pCountry);
        patientnameobj.put("phoneNumber", pPhone);
        patientnameobj.put("email", peMail);
        patientnameobj.put("docterRegistrationNumber", docterregId);
        patientnameobj.put("ReferredbyFname", pReffFname);
        patientnameobj.put("ReferredbyLname", pReffLname);
        patientnameobj.put("Careof", pCareof);
        patientnameobj.put("CareofFname", pCareoffirstname);
        patientnameobj.put("CareofMname", pCareofmiddlename);
        patientnameobj.put("CareofLname", pCareoflastname);
        patientnameobj.put("Occupation", pOccupation);
        patientnameobj.put("AttendantName", pAttendantName);


        patientnameobj.pinInBackground(new SaveCallback() {
            public void done(ParseException e) {
                mProgressDialog.dismiss();
                if (e == null) {
                    myObjectSavedSuccessfully();

                    // Id= Student.getObjectId();
                    //i=1;

                } else {
                    myObjectSaveDidNotSucceed();
                    //i=0;
                }
            }

            private void myObjectSaveDidNotSucceed() {
                // TODO Auto-generated method stub
                Toast msg1 = Toast.makeText(getActivity(), "Failed", Toast.LENGTH_LONG);
                msg1.show();


            }

            private void myObjectSavedSuccessfully() {
                // TODO Auto-generated method stub


                FragmentActivityContainer.check_save = 0;
                Toast msg = Toast.makeText(getActivity(), " Successfully Completed", Toast.LENGTH_LONG);
                msg.show();
                submitButtonDeactivation();

                flag = 1;

                SharedPreferences fnname = getActivity().getSharedPreferences("fnName", 0);
                SharedPreferences.Editor spObj = fnname.edit();
                spObj.putString("fNname", pfName.getText().toString() + " " + plName.getText().toString().trim());


                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                if (((FragmentActivityContainer) getActivity()).getdoctereditPatientflag() == false) {
                    if (targetfragment == 3) {
                        Patient_Current_Medical_Info_Frag pntcurrmedinfo = new Patient_Current_Medical_Info_Frag();
                        fragmentTransaction.replace(R.id.fl_fragment_container, pntcurrmedinfo);
                    } else if (targetfragment == 2 || targetfragment == 0) {
                        if (((FragmentActivityContainer) getActivity()).getmedicalhistory() == null) {
                            patientmedicalhistoryfragObj = new PatientMedicalaHistory();
                            Bundle bundle = new Bundle();
                            bundle.putBoolean("patientflag", patientflag);
                            patientmedicalhistoryfragObj.setArguments(bundle);
                        } else {
                            patientmedicalhistoryfragObj = ((FragmentActivityContainer) getActivity()).getPatientMedicalHistory_nephro();
                        }
                        ((FragmentActivityContainer) getActivity()).setPatientMedicalHistory_nephro(patientmedicalhistoryfragObj);
                        fragmentTransaction.replace(R.id.fl_fragment_container, patientmedicalhistoryfragObj);
                    } else if (targetfragment == 4) {
                        if (((FragmentActivityContainer) getActivity()).getcurrentvitals() == null) {
                            pntcurrentvitals = new CurrentVitals();
                            Bundle bundle = new Bundle();
                            bundle.putBoolean("patientflag", patientflag);
                            pntcurrentvitals.setArguments(bundle);
                        } else {
                            pntcurrentvitals = ((FragmentActivityContainer) getActivity()).getcurrentvitals();
                        }
                        ((FragmentActivityContainer) getActivity()).setcurrentvitals(pntcurrentvitals);
                        fragmentTransaction.replace(R.id.fl_fragment_container, pntcurrentvitals);
                    }
                    ((FragmentActivityContainer) getActivity()).setPatientobject(patientnameobj);
                    if (admintab == true)
                        fragmentTransaction.addToBackStack(null);
                    fragmentTransaction.commit();


                    SharedPreferences fnname1 = getActivity().getSharedPreferences("fnName", 0);
                    SharedPreferences.Editor spObj1 = fnname.edit();
                    spObj1.putString("fNname", pfName.getText().toString() + " " + plName.getText().toString().trim());


                }
            }
        });
        patientnameobj.saveEventually();

    }


    private void submitButtonActivation() {
        // TODO Auto-generated method stub
        try {
            ((FragmentActivityContainer) getActivity()).getpatientfrag().onButtonactivation(2);
        } catch (Exception e) {
            e.printStackTrace();
            //Toast.makeText(getActivity(), "getfragment error3", Toast.LENGTH_LONG).show();
        }
    }

    private void submitButtonDeactivation() {
        // TODO Auto-generated method stub
        try {
            ((FragmentActivityContainer) getActivity()).getpatientfrag().onButtonactivation(0);
        } catch (Exception e) {
            //Toast.makeText(getActivity(), "getfragment error4", Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }

    boolean isValidEmail(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target)
                    .matches();
        }
    }

    @Override
    public void onPersonal(int arg, int targetfragment) {
        // TODO Auto-generated method stub


        boolean validfields = true;
        pReffFname = reffName.getText().toString().trim();
        pReffLname = reflName.getText().toString().trim();
        pfirstname = pfName.getText().toString().trim();
        pmiddlename = pmName.getText().toString().trim();
        plastname = plName.getText().toString().trim();
        pCareoffirstname = careOfFname.getText().toString().trim();
        pCareofmiddlename = careOfMname.getText().toString().trim();
        pCareoflastname = careOfLname.getText().toString().trim();
        padhaarno = pfAdhaar.getText().toString().trim();
        pstrtName = pstreetName.getText().toString().trim();
        pCity = pcityName.getText().toString().trim();
        pPin = ppinCode.getText().toString().trim();
        pState = pstate.getText().toString().trim();
        pPhone = pphone.getText().toString().trim();
        peMail = pemail.getText().toString().trim();
        pAttendantName = attendantName.getText().toString().trim();
        emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

        pSex = psex.getText().toString();
        pStatus = pstutus.getText().toString();
        pYearB = pyearB.getText().toString();
        pCountry = pcountry.getText().toString();
        pAge = tvAge.getText().toString();
        pCareof = careOf.getText().toString();
        pOccupation = occupation.getText().toString();


        fln = (pfName.getText().toString() + " " + plName.getText().toString().trim());

        SharedPreferences fnname = getActivity().getSharedPreferences("fnName", 0);
        SharedPreferences.Editor Ed = fnname.edit();
        Ed.clear();
        Ed.putString("fNname", fln);
        Ed.commit();

        if (TextUtils.isEmpty(pcityName.getText().toString())) {
            pcityName.setError("Enter Patient City Name ");
            pcityName.requestFocus();
            validfields = false;

        } else {
            pcityName.setError(null);
        }

        if (TextUtils.isEmpty(careOfFname.getText().toString())) {
            careOfFname.setError("Enter Care of First Name ");
            careOfFname.requestFocus();
            validfields = false;

        } else {
            careOfFname.setError(null);
        }
        if (TextUtils.isEmpty(careOfLname.getText().toString())) {
            careOfLname.setError("Enter Care of Last Name ");
            careOfLname.requestFocus();
            validfields = false;

        } else {
            careOfLname.setError(null);
        }

        if (TextUtils.isEmpty(plName.getText().toString())) {
            plName.setError("Enter Patient Last Name ");
            plName.requestFocus();
            validfields = false;

        } else {
            plName.setError(null);
        }

        if (TextUtils.isEmpty(pfName.getText().toString())) {
            pfName.setError("Enter Patient Name ");
            pfName.requestFocus();
            validfields = false;

        } else {
            pfName.setError(null);
        }

        System.out.println("sex " + psex.getText());
        if (psex.getText().length() == 0) {
            System.out.println("sex workinggggggggggggg");
            Toast.makeText(getActivity(), "Select Gender", Toast.LENGTH_SHORT).show();
            validfields = false;
        } else if (careOf.getText().length() == 0) {
            Toast.makeText(getActivity(), "Select Care of", Toast.LENGTH_SHORT).show();
            validfields = false;
        } else if (occupation.getText().length() == 0) {
            Toast.makeText(getActivity(), "Select Occupation", Toast.LENGTH_SHORT).show();
            validfields = false;
        } else if (pyearB.getText().length() == 0) {
            Toast.makeText(getActivity(), "Select YOB", Toast.LENGTH_SHORT).show();
            validfields = false;
        } else if (pcountry.getText().length() == 0) {
            Toast.makeText(getActivity(), "Select Country", Toast.LENGTH_SHORT).show();
            validfields = false;
            System.out.println("country workinggggggggg");
        } else if (pfAdhaar.getText().length() != 0 && pfAdhaar.getText().length() != 12) {

            Toast.makeText(getActivity(), "Please enter 12 digit ID number", Toast.LENGTH_SHORT).show();
            validfields = false;

        } else if (pPin.length() != 6 && pPin.length() != 0) {
            System.out.println("pin workingggggggggg");
            Toast.makeText(getActivity(), "Invalid Pin number", Toast.LENGTH_SHORT).show();
            validfields = false;
        } else if (pPhone.length() < 6 && pPhone.length() != 0 || pPhone.length() > 14 && pPhone.length() != 0) {
            System.out.println("phoneee workinggggggggggg");
            Toast.makeText(getActivity(), "Invalid Phone number", Toast.LENGTH_SHORT).show();
            validfields = false;

        } else if (!isValidEmail(peMail) && peMail.length() != 0) {
            System.out.println("email workinggggggggg");
            Toast.makeText(getActivity(), "Invalid E-Mail", Toast.LENGTH_SHORT).show();
            validfields = false;
        } else {
            System.out.println("else workinggggggggggggg");
        }

        System.out.println("emailmstches" + peMail.matches(emailPattern));
        System.out.println("validfields value" + validfields);

        System.out.println("validfields" + validfields);
        if (validfields) {

            System.out.println("workingvalid");
            Save_patientPersonal_Information(targetfragment);
        } else {
        }
    }

    public void show() {

        final Dialog d = new Dialog(getActivity());
        d.setTitle("NumberPicker");
        d.setContentView(R.layout.dialog);
        Button b1 = (Button) d.findViewById(R.id.button1);
        Button b2 = (Button) d.findViewById(R.id.button2);
        final NumberPicker np = (NumberPicker) d.findViewById(R.id.numberPicker1);

        year = Calendar.getInstance().get(Calendar.YEAR);
        np.setMaxValue(year);
        np.setMinValue(1900);
        np.setWrapSelectorWheel(false);
        np.setOnValueChangedListener(PatientpersonalInfo.this);
        b1.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                Toast.makeText(getActivity(), String.valueOf(np.getValue()), Toast.LENGTH_LONG).show();
                pyearB.setText(String.valueOf(np.getValue()));

                int age = year - np.getValue();
                tvAge.setText(String.valueOf(age));


                d.dismiss();
            }
        });
        b2.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                d.dismiss();
            }
        });
        d.show();


    }

    @Override
    public void onValueChange(NumberPicker arg0, int arg1, int arg2) {
        // TODO Auto-generated method stub


    }


}