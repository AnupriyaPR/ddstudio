package com.wiinnova.doctorsdiary.fragments;


import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.wiinnova.doctorsdiary.R;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.wiinnova.doctorsdiary.activities.FragmentActivityContainer;
import com.wiinnova.doctorsdiary.fragment.CrashReporter;
import com.wiinnova.doctorsdiary.fragment.Patient_Frag;
import com.wiinnova.doctorsdiary.supportclasses.AppUtil;
import com.wiinnova.doctorsdiary.supportclasses.Diseasenameclass;
import com.wiinnova.doctorsdiary.supportclasses.LMPAdapterDiagnosis;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;


public class PatientPreviousTreatment extends Fragment {


    ArrayList<Diseasenameclass> diseasenamearraylist = new ArrayList<Diseasenameclass>();
    ParseObject patient;
    ParseObject patientcurrentvitalsdetails;
    ParseObject patientobject;
    List<ParseObject> _suspecteddiseasenamesObj;

    String formattedDate;
    String formattedDate1;
    private TextView mtvPatientid;
    private TextView mtvPatientname;
    LinearLayout llmedhistcontainer;
    ProgressDialog mProgressDialog;
    ProgressDialog mProgress;
    String[] days;
    String patientIdvalue;
    ExpandableListView lvpatientdetails;
    private Patient_Frag patientfragObj;
    private boolean admintab;
    private boolean admintabdefault;
    private String specialization;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View view = inflater.inflate(R.layout.patientcurrentmedhistorycontainer, null);
        Initialize_Components(view);

        Log.d("GYNEC", "No");
        _suspecteddiseasenamesObj = ((CrashReporter) getActivity().getApplicationContext()).getDiseasenameObjects();

        if (getFragmentManager().findFragmentById(R.id.fl_sidemenu_container) instanceof Patient_Frag) {

            ((FragmentActivityContainer) getActivity()).getpatientfrag().getcurrentmedinfo_colorchange();

        } else {

            patientfragObj = new Patient_Frag();
            Bundle bundle1 = new Bundle();
            bundle1.putInt("sidemenuitem", 3);
            bundle1.putBoolean("patientflag", false);
            patientfragObj.setArguments(bundle1);
            getFragmentManager().beginTransaction()
                    .replace(R.id.fl_sidemenu_container, patientfragObj)
                    .commit();

            if (patientfragObj != null) {
                ((FragmentActivityContainer) getActivity()).setpatientfrag(patientfragObj);
            }
        }
        admintab = ((FragmentActivityContainer) getActivity()).getcheckadmintab();
        admintabdefault = ((FragmentActivityContainer) getActivity()).getadmintabdefault();

        if (admintab == true) {
            System.out.println("admin tab working.....");
            patientobject = ((FragmentActivityContainer) getActivity()).getOldPatientobject();
        } else if (admintab == false && admintabdefault == false) {
            patientobject = ((FragmentActivityContainer) getActivity()).getPatientobject();
        } else if (admintab == false && admintabdefault == true) {
            patientobject = ((FragmentActivityContainer) getActivity()).getOldPatientobject();
        }
        if (patientobject == null) {

            SharedPreferences scanpid = getActivity().getSharedPreferences("NewPatID", 0);
            patientIdvalue = scanpid.getString("NewID", null);

        } else {
            patientIdvalue = patientobject.getString("patientID");
        }

        if (_suspecteddiseasenamesObj != null) {

            diseasenamearraylist.clear();

            for (int i = 0; i < _suspecteddiseasenamesObj.size(); i++) {
                Diseasenameclass diseasenameObj = new Diseasenameclass();
                System.out.println("working..." + _suspecteddiseasenamesObj.get(i).getString("DiseaseName"));
                diseasenameObj.setDiseasename(_suspecteddiseasenamesObj.get(i).getString("DiseaseName"));
                diseasenameObj.setIcdnumber(_suspecteddiseasenamesObj.get(i).getString("ICDNumber"));
                diseasenamearraylist.add(diseasenameObj);
            }
        }


        SharedPreferences sp = getActivity().getSharedPreferences("Login", 0);
        specialization = sp.getString("spel", null);

        if (!specialization.equalsIgnoreCase("gynecologist obstetrician")) {
            Log.d("GYNEC", "No");
            getpatientcurrentvitals();
        } else {
            Log.d("GYNEC", "Yes");
            getpatientdetailsgynacologist();
        }


        Setpatientidpatientname();

        return view;
    }


    private void getpatientcurrentvitals() {

        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setMessage("Fetching Details....");
        mProgressDialog.show();
        mProgressDialog.setCancelable(false);
        final ParseQuery<ParseObject> query = ParseQuery.getQuery("Patients");
        if (!AppUtil.haveNetwokConnection(getActivity()))
            query.fromLocalDatastore();
        query.whereEqualTo("patientID", patientIdvalue);

        query.findInBackground(new FindCallback<ParseObject>() {

            @Override
            public void done(List<ParseObject> object, ParseException e) {
                // TODO Auto-generated method stub
                //	mProgressDialog.dismiss();

                if (PatientPreviousTreatment.this.isVisible()) {
                    if (e == null) {

                        System.out.println("fragment visible.....");
                        if (object.size() > 0) {
                            patientcurrentvitalsdetails = object.get(0);
                            getpatientdetails();
                        } else {
                            getpatientdetails();
                        }
                    }
                } else {
                    mProgressDialog.dismiss();
                }

            }
        });
    }


	/*	private void getdetails(){

		final ParseQuery<ParseObject> query = ParseQuery.getQuery("Diagnosis");
		query.fromLocalDatastore();
		query.whereEqualTo("patientID",patientIdvalue);
		query.addDescendingOrder("createdDate");
		mProgressDialog.setOnCancelListener(new OnCancelListener() {

			@Override
			public void onCancel(DialogInterface dialog) {
				// TODO Auto-generated method stub
				query.cancel();
			}
		});

		query.findInBackground(new FindCallback<ParseObject>() {

			@Override
			public void done(List<ParseObject> objects, ParseException e) {
				// TODO Auto-generated method stub

				if(e==null){

					if(objects.size()>0){

						for(int i=0;i<objects.size();i++){
							for(int j=i;j<objects.size();j++){
								if(i!=j){
									if(objects.get(i).getString("diagnosisId").equals(objects.get(j).getString("diagnosisId")))
								}
							}
						}

					}

				}

			}
		});

	}*/


    private void getpatientdetails() {
        // TODO Auto-generated method stub

        final ParseQuery<ParseObject> query = ParseQuery.getQuery("Diagnosis");

        if (!AppUtil.haveNetwokConnection(getActivity()))
            query.fromLocalDatastore();
        query.whereEqualTo("patientID", patientIdvalue);
        query.addDescendingOrder("createdDate");
        mProgressDialog.setOnCancelListener(new OnCancelListener() {

            @Override
            public void onCancel(DialogInterface dialog) {
                // TODO Auto-generated method stub
                query.cancel();
            }
        });
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                // TODO Auto-generated method stub
                mProgressDialog.dismiss();
                final List<ParseObject> objectslist = new ArrayList<ParseObject>();
                Log.e("Login", "done");
                if (e == null) {
                    if (objects.size() > 0) {
                        System.out.println("objects size" + objects.size());

                        ArrayList<Integer> listed = new ArrayList<Integer>();
                        listed.clear();

                        for (int i = 0; i < objects.size(); i++) {
                            String formattedDate = objects.get(i).getString("createdDate");
                            for (int j = i; j < objects.size(); j++) {
                                System.out.println("working count" + j);
                                String formattedDate1 = objects.get(j).getString("createdDate");
                                if ((objects.get(i).getDouble("typeFlag") == 1 && objects.get(j).getDouble("typeFlag") == 2) ||
                                        (objects.get(i).getDouble("typeFlag") == 2 && objects.get(j).getDouble("typeFlag") == 1)) {
                                    System.out.println("formattedDate" + formattedDate);
                                    System.out.println("formattedDate1" + formattedDate1);

                                    System.out.println("diagnosiid" + objects.get(i).getString("diagnosisId"));
                                    System.out.println("diagnosiid" + objects.get(j).getString("diagnosisId"));

                                    String diagnosisId_diagnosis = objects.get(i).getString("diagnosisId");
                                    String diagnosisId_prescription = objects.get(j).getString("diagnosisId");

                                    //&& objects.get(i).getString("diagnosisId").equals(objects.get(j).getString("diagnosisId"))

                                    if (diagnosisId_diagnosis != null && diagnosisId_diagnosis != "" && diagnosisId_prescription != null && diagnosisId_prescription != "") {
                                        if (formattedDate.equals(formattedDate1) && diagnosisId_diagnosis.equals(diagnosisId_prescription)) {
                                            System.out.println();
                                            ParseObject combinedobject = new ParseObject("");
                                            System.out.println("iiiiii" + i + "jjjjj" + j);
                                            System.out.println("workinggggggggg1");
                                            System.out.println("formated date" + formattedDate);
                                            String days;
                                            if (objects.get(i).getJSONArray("days") != null) {
                                                combinedobject.put("days", objects.get(i).getJSONArray("days"));
                                            } else if (objects.get(j).getJSONArray("days") != null) {
                                                combinedobject.put("days", objects.get(j).getJSONArray("days"));
                                            }
                                            if (objects.get(i).getJSONArray("drugStartDate") != null) {
                                                combinedobject.put("drugStartDate", objects.get(i).getJSONArray("drugStartDate"));
                                            } else if (objects.get(j).getJSONArray("drugStartDate") != null) {
                                                combinedobject.put("drugStartDate", objects.get(j).getJSONArray("drugStartDate"));
                                            }
                                            if (objects.get(i).getJSONArray("dosage") != null) {
                                                combinedobject.put("dosage", objects.get(i).getJSONArray("dosage"));
                                            } else if (objects.get(j).getJSONArray("dosage") != null) {
                                                combinedobject.put("dosage", objects.get(j).getJSONArray("dosage"));
                                            }
                                            if (objects.get(i).getJSONArray("drug") != null) {
                                                combinedobject.put("drug", objects.get(i).getJSONArray("drug"));
                                            } else if (objects.get(j).getJSONArray("drug") != null) {
                                                combinedobject.put("drug", objects.get(j).getJSONArray("drug"));
                                            }
                                            if (objects.get(i).getJSONArray("duration") != null) {
                                                combinedobject.put("duration", objects.get(i).getJSONArray("duration"));
                                            } else if (objects.get(j).getJSONArray("duration") != null) {
                                                combinedobject.put("duration", objects.get(j).getJSONArray("duration"));
                                            }
                                            if (objects.get(i).getJSONArray("quantity") != null) {
                                                combinedobject.put("quantity", objects.get(i).getJSONArray("quantity"));
                                            } else if (objects.get(j).getJSONArray("quantity") != null) {
                                                combinedobject.put("quantity", objects.get(j).getJSONArray("quantity"));
                                            }
                                            combinedobject.put("createdAt", formattedDate);
                                            if (objects.get(j).getString("additionalComments") != null) {
                                                combinedobject.put("additionalComments", objects.get(j).getString("additionalComments"));
                                            } else if (objects.get(i).getString("additionalComments") != null) {
                                                combinedobject.put("additionalComments", objects.get(i).getString("additionalComments"));
                                            }
                                            if (objects.get(j).getString("suspectedDisease") != null) {
                                                combinedobject.put("suspectedDisease", objects.get(j).getString("suspectedDisease"));
                                                combinedobject.put("diagnosisObjId", objects.get(j).getString("diagnosisId"));
                                            } else if (objects.get(i).getString("suspectedDisease") != null) {
                                                combinedobject.put("suspectedDisease", objects.get(i).getString("suspectedDisease"));
                                                combinedobject.put("diagnosisObjId", objects.get(i).getString("diagnosisId"));
                                            }
                                            if (objects.get(j).getString("symptoms") != null) {
                                                combinedobject.put("symptoms", objects.get(j).getString("symptoms"));
                                            } else if (objects.get(i).getString("symptoms") != null) {
                                                combinedobject.put("symptoms", objects.get(i).getString("symptoms"));
                                            }
                                            if (objects.get(j).getString("syndromes") != null) {
                                                combinedobject.put("syndromes", objects.get(j).getString("syndromes"));
                                            } else if (objects.get(i).getString("syndromes") != null) {
                                                combinedobject.put("syndromes", objects.get(i).getString("syndromes"));
                                            }


                                            System.out.println("combied project" + combinedobject);
                                            objectslist.add(combinedobject);

                                            listed.add(i);
                                            listed.add(j);
                                            System.out.println("listed size" + listed.size());
                                        }
                                    }
                                }
                            }


                            if (!(listed.contains(i))) {
                                if (objects.get(i).getDouble("typeFlag") == 1) {
                                    ParseObject combinedobject = new ParseObject("");
                                    System.out.println("i valueeeeeee" + i);

                                    if (objects.get(i).getString("additionalComments") != null) {
                                        combinedobject.put("additionalComments", objects.get(i).getString("additionalComments"));
                                    } else {
                                        combinedobject.put("additionalComments", "N/A");
                                    }
                                    if (objects.get(i).getString("suspectedDisease") != null) {
                                        combinedobject.put("diagnosisObjId", objects.get(i).getString("diagnosisId"));
                                        combinedobject.put("suspectedDisease", objects.get(i).getString("suspectedDisease"));
                                    } else {
                                        combinedobject.put("suspectedDisease", "N/A");
                                    }
                                    if (objects.get(i).getString("symptoms") != null) {
                                        combinedobject.put("symptoms", objects.get(i).getString("symptoms"));
                                    } else {
                                        combinedobject.put("symptoms", "N/A");
                                    }
                                    if (objects.get(i).getString("syndromes") != null) {
                                        combinedobject.put("syndromes", objects.get(i).getString("syndromes"));
                                    } else {
                                        combinedobject.put("syndromes", "N/A");
                                    }
                                    combinedobject.put("createdAt", formattedDate);

                                    objectslist.add(combinedobject);
                                }
                            }
                        }
                        listed.clear();

                        //mProgressDialog.dismiss();
                        //new code
                        for (int i = 0; i < objectslist.size(); i++) {
                            /*if(i==0){
                                mProgressDialog.dismiss();
							}*/
                            if (PatientPreviousTreatment.this.isVisible()) {

                                LayoutInflater inflater = LayoutInflater.from(getActivity());
                                View patient_info = inflater.inflate(R.layout.patientmedifolistitems, null);

                                LinearLayout datecontaier = (LinearLayout) patient_info.findViewById(R.id.lldatecontainer);
                                final LinearLayout detailscontainer = (LinearLayout) patient_info.findViewById(R.id.llpatient_details_container);

                                final ImageView arrow = (ImageView) patient_info.findViewById(R.id.ivarrow);
                                arrow.setBackgroundResource(R.drawable.triangle_arrow);
                                TextView tvdate = (TextView) patient_info.findViewById(R.id.cdcurrdate);

                                System.out.println("position" + i);
                                formattedDate = objectslist.get(i).getString("createdAt");
                                System.out.println("dateeeeeeeee" + objectslist.get(i).getString("createdAt"));
                                tvdate.setText(formattedDate);
                                /*if(i==objectslist.size()-1){
                                    mProgressDialog.dismiss();
								}*/



                                final LinearLayout prescription = (LinearLayout) patient_info.findViewById(R.id.ctml3);
                                final EditText metSymptoms = (EditText) patient_info.findViewById(R.id.etsymptoms);
                                final EditText metSyndromes = (EditText) patient_info.findViewById(R.id.etsyndromes);
                                final EditText metComments = (EditText) patient_info.findViewById(R.id.etcomments);
                                final EditText metSuspected = (EditText) patient_info.findViewById(R.id.etsuspecteddisease);


                                final TextView metWeight = (TextView) patient_info.findViewById(R.id.etweight);
                                final TextView metBloodPresure = (TextView) patient_info.findViewById(R.id.etbloodpressure);
                                final TextView metPulse = (TextView) patient_info.findViewById(R.id.etpulse);
                                final TextView metRespiratory = (TextView) patient_info.findViewById(R.id.etrespiratory);
                                final TextView metSpo2 = (TextView) patient_info.findViewById(R.id.etsp);
                                final TextView metTemperature = (TextView) patient_info.findViewById(R.id.ettemperature);
                                final LinearLayout lldrugs = (LinearLayout) patient_info.findViewById(R.id.lldrugscontainer);
                                final int position = i;
                                llmedhistcontainer.addView(patient_info);
                                datecontaier.setOnClickListener(new OnClickListener() {

                                    @Override
                                    public void onClick(View arg0) {
                                        // TODO Auto-generated method stub
                                        if (detailscontainer.getVisibility() == View.VISIBLE) {
                                            detailscontainer.setVisibility(View.GONE);
                                            arrow.setBackgroundResource(R.drawable.triangle_arrow);

                                        } else {
                                            arrow.setBackgroundResource(R.drawable.triangle_up);
                                            detailscontainer.setVisibility(View.VISIBLE);


                                            if (objectslist.get(position).getString("suspectedDisease").length() != 0) {

                                                String[] value = (objectslist.get(position).getString("suspectedDisease").split(","));
                                                ArrayList<String> diseasenamecheck = new ArrayList<String>();

                                                for (int val = 0; val < value.length; val++) {

                                                    diseasenamecheck.add(value[val]);

                                                }

                                                if (_suspecteddiseasenamesObj.size() != 0 && _suspecteddiseasenamesObj != null) {

                                                    compairing_code_taken_disease_fromparse(objectslist.get(position).getString("suspectedDisease"), metSuspected);


                                                    String[] valuedisease = objectslist.get(position).getString("suspectedDisease").split(",");
                                                    if (valuedisease != null)
                                                        for (int k = 0; k < valuedisease.length; k++) {

                                                            System.out.println("code valuessss from parse" + valuedisease[k]);
                                                        }
                                                    String[] myaccrArray = null;
                                                    if (valuedisease != null) {
                                                        myaccrArray = new String[valuedisease.length];
                                                        for (int j = 0; j < valuedisease.length; j++) {
                                                            for (int k = 0; k < diseasenamearraylist.size(); k++) {
                                                                if ((diseasenamearraylist.get(k).getIcdnumber()) != null) {
                                                                    String icd_from_list = diseasenamearraylist.get(k).getIcdnumber().toString().trim();
                                                                    String icd_from_selected = valuedisease[j].toString().trim();


                                                                    if (icd_from_selected.equalsIgnoreCase("other")) {
                                                                        myaccrArray[j] = "Other";
                                                                    } else if (icd_from_selected.equalsIgnoreCase("none")) {
                                                                        myaccrArray[j] = "None";
                                                                    }

                                                                    if (!icd_from_selected.equalsIgnoreCase("other") && !icd_from_selected.equalsIgnoreCase("none")) {
                                                                        if (icd_from_list.equalsIgnoreCase(icd_from_selected)) {
                                                                            myaccrArray[j] = diseasenamearraylist.get(k).getDiseasename();
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                            String disease = "";
                                                            for (int i = 0; i < myaccrArray.length; i++) {

                                                                disease += myaccrArray[i] + "\n";
                                                            }

                                                            metSuspected.setText(disease);
                                                            //metSuspected.put("suspectedDisease",disease);
                                                        }
                                                    }


                                                } else {

                                                    mProgress = new ProgressDialog(getActivity());
                                                    mProgress.setMessage("Fetching Details.....");
                                                    mProgress.show();
                                                    ParseQuery<ParseObject> query = ParseQuery.getQuery("DiseaseDatabase");

                                                    if (!AppUtil.haveNetwokConnection(getActivity()))
                                                        query.fromLocalDatastore();
                                                    query.whereContainedIn("ICDNumber", diseasenamecheck);
                                                    query.findInBackground(new FindCallback<ParseObject>() {

                                                        @Override
                                                        public void done(List<ParseObject> object, ParseException e) {
                                                            // TODO Auto-generated method stub
                                                            mProgress.dismiss();
                                                            if (e == null) {
                                                                if (object.size() > 0) {
                                                                    String disease = "";
                                                                    for (int diseasesize = 0; diseasesize < object.size(); diseasesize++) {
                                                                        System.out.println("names disease" + object.get(diseasesize).getString("DiseaseName"));
                                                                        disease += object.get(diseasesize).getString("DiseaseName") + "\n";
                                                                    }
                                                                    metSuspected.setText(disease);
                                                                }
                                                            }

                                                        }
                                                    });
                                                }

                                            } else {
                                                metSuspected.setText("Nil");
                                            }


                                        }
                                    }
                                });

                                if (objectslist.get(position).getString("symptoms").length() != 0) {
                                    metSymptoms.setText(objectslist.get(position).getString("symptoms"));
                                } else {
                                    metSymptoms.setText("Nil");
                                }
                                if (objectslist.get(position).getString("syndromes").length() != 0) {
                                    metSyndromes.setText(objectslist.get(position).getString("syndromes"));
                                } else {
                                    metSyndromes.setText("Nil");
                                }


                                if (objectslist.get(position).getString("additionalComments").length() != 0) {
                                    metComments.setText(objectslist.get(position).getString("additionalComments"));
                                } else {
                                    metComments.setText("Nil");
                                }


                                if (patientobject != null) {
                                    JSONArray weight_result = null;
                                    JSONArray spo2_result = null;
                                    JSONArray pulse_result = null;
                                    JSONArray respRate_result = null;
                                    JSONArray height_result = null;
                                    JSONArray bloodGroup_result = null;
                                    JSONArray bloodPressure_result = null;
                                    JSONArray temperature = null;
                                    if (patientcurrentvitalsdetails.getJSONArray("weight") != null) {
                                        weight_result = patientcurrentvitalsdetails.getJSONArray("weight");
                                    }
                                    if (patientcurrentvitalsdetails.getJSONArray("spo2") != null) {
                                        spo2_result = patientcurrentvitalsdetails.getJSONArray("spo2");
                                    }
                                    if (patientcurrentvitalsdetails.getJSONArray("pulse") != null) {
                                        pulse_result = patientcurrentvitalsdetails.getJSONArray("pulse");
                                    }
                                    if (patientcurrentvitalsdetails.getJSONArray("respRate") != null) {
                                        respRate_result = patientcurrentvitalsdetails.getJSONArray("respRate");
                                    }
                                    if (patientcurrentvitalsdetails.getJSONArray("height") != null) {
                                        height_result = patientcurrentvitalsdetails.getJSONArray("height");
                                    }
                                    if (patientcurrentvitalsdetails.getJSONArray("bloodGroup") != null) {
                                        bloodGroup_result = patientcurrentvitalsdetails.getJSONArray("bloodGroup");
                                    }
                                    if (patientcurrentvitalsdetails.getJSONArray("bloodPressure") != null) {
                                        bloodPressure_result = patientcurrentvitalsdetails.getJSONArray("bloodPressure");
                                    }
                                    if (patientcurrentvitalsdetails.getJSONArray("temp") != null) {
                                        temperature = patientcurrentvitalsdetails.getJSONArray("temp");
                                    }
                                    JSONArray objectid = patientcurrentvitalsdetails.getJSONArray("diagnosisObjectid");
                                    ArrayList<String> object = new ArrayList<String>();
                                    if (patientcurrentvitalsdetails.getJSONArray("diagnosisObjectid") != null) {
                                        for (int k = 0; k < objectid.length(); k++) {
                                            try {
                                                object.add(objectid.getString(k));
                                            } catch (JSONException e1) {
                                                // TODO Auto-generated catch block
                                                e1.printStackTrace();
                                            }
                                        }
                                    }




									/*if(object.contains(objectslist.get(position).getString("diagnosisObjId"))){*/
                                    System.out.println("object position" + object.indexOf(objectslist.get(position).getString("diagnosisObjId")));
                                    int index = object.indexOf(objectslist.get(position).getString("diagnosisObjId"));
                                    try {
                                        //&&!(weight_result.getString(index).equals("Nil"))
                                        if (weight_result != null) {
                                            metWeight.setText(weight_result.getString(weight_result.length() - 1) + " " + "kg");
                                        } else {
                                            metWeight.setText("-" + " " + "kg");
                                        }
                                        //&&  !(bloodPressure_result.getString(index).equals("Nil"))
                                        if (bloodPressure_result != null) {
                                            metBloodPresure.setText(bloodPressure_result.getString(bloodPressure_result.length() - 1) + " " + "Hg mm");
                                        } else {
                                            metBloodPresure.setText("-" + " " + "Hg mm");
                                        }
                                        // && !(spo2_result.getString(index).equals("Nil"))
                                        if (spo2_result != null) {
                                            metSpo2.setText(spo2_result.getString(spo2_result.length() - 1) + " " + "%");
                                        } else {
                                            metSpo2.setText("-" + " " + "%");
                                        }
                                        // && !(temperature.getString(index).equals("Nil"))
                                        if (temperature != null) {
                                            metTemperature.setText(temperature.getString(temperature.length() - 1) + " " + "c");
                                        } else {
                                            metTemperature.setText("-" + " " + "c");
                                        }
                                        //&& !(pulse_result.getString(index).equals("Nil"))
                                        if (pulse_result != null) {
                                            metPulse.setText(pulse_result.getString(pulse_result.length() - 1) + " " + "beats/min");
                                        } else {
                                            metPulse.setText("-" + " " + "beats/min");
                                        }
                                        //&& !(respRate_result.getString(index).equals("Nil"))
                                        if (respRate_result != null) {
                                            metRespiratory.setText(respRate_result.getString(respRate_result.length() - 1) + " " + "breaths/min");
                                        } else {
                                            metRespiratory.setText("-" + " " + "breaths/min");
                                        }
                                    } catch (JSONException e1) {
                                        // TODO Auto-generated catch block
                                        e1.printStackTrace();
                                    }

									/*	}else{
                                        metWeight.setText("-"+" "+"kg");
										metBloodPresure.setText("-"+" "+"Hg mm");
										metSpo2.setText("-"+" "+"%");
										metTemperature.setText("-"+" "+"c");
										metPulse.setText("-"+" "+"beats/min");
										metRespiratory.setText("-"+" "+"breaths/min");
									}*/

                                    metSymptoms.setEnabled(false);
                                    metSyndromes.setEnabled(false);
                                    metSymptoms.setEnabled(false);
                                    metSuspected.setEnabled(false);
                                    metComments.setEnabled(false);
                                    metWeight.setEnabled(false);
                                    metBloodPresure.setEnabled(false);
                                    metSpo2.setEnabled(false);
                                    metTemperature.setEnabled(false);
                                    metPulse.setEnabled(false);
                                    metRespiratory.setEnabled(false);

                                    metSuspected.measure(0, 0);
                                    metComments.measure(0, 0);

                                    int susheight = metSuspected.getMeasuredHeight();
                                    int comheight = metComments.getMeasuredHeight();
                                    int symptheight = metSymptoms.getMeasuredHeight();
                                    int syndheight = metSyndromes.getMeasuredHeight();
                                    System.out.println("metSuspected height" + metSuspected.getMeasuredHeight());
                                    System.out.println("metComments height" + metComments.getMeasuredHeight());

                                    if (symptheight > syndheight) {
                                        metSyndromes.setHeight(symptheight);
                                    }
                                    if (symptheight < syndheight) {
                                        metSymptoms.setHeight(syndheight);
                                    }
                                    if (susheight > comheight) {
                                        metComments.setHeight(susheight);
                                    }
                                    if (susheight < comheight) {
                                        metSuspected.setHeight(comheight);
                                    }
                                } else {
                                    metWeight.setText("-" + " " + "kg");
                                    metBloodPresure.setText("-" + " " + "Hg mm");
                                    metSpo2.setText("-" + " " + "%");
                                    metTemperature.setText("-" + " " + "c");
                                    metPulse.setText("-" + " " + "beats/min");
                                    metRespiratory.setText("-" + " " + "breaths/min");
                                }
                                System.out.println("successssssssssss" + position);
                                JSONArray drugObj = null;
                                JSONArray dosageObj = null;
                                JSONArray dateObj = null;
                                JSONArray durationObj = null;
                                JSONArray QuantityObj = null;
                                JSONArray daysObj = null;
                                if (objectslist.get(position).getJSONArray("drug") != null) {
                                    drugObj = objectslist.get(position).getJSONArray("drug");
                                }
                                if (objectslist.get(position).getJSONArray("dosage") != null) {
                                    dosageObj = objectslist.get(position).getJSONArray("dosage");
                                }
                                if (objectslist.get(position).getJSONArray("drugStartDate") != null) {
                                    dateObj = objectslist.get(position).getJSONArray("drugStartDate");
                                }
                                if (objectslist.get(position).getJSONArray("duration") != null) {
                                    durationObj = objectslist.get(position).getJSONArray("duration");
                                }

                                if (objectslist.get(position).getJSONArray("quantity") != null) {
                                    QuantityObj = objectslist.get(position).getJSONArray("quantity");
                                }
                                if (objectslist.get(position).getJSONArray("days") != null) {
                                    daysObj = objectslist.get(position).getJSONArray("days");
                                }

                                if (drugObj != null) {
                                    System.out.println("drugObj length" + drugObj.length());

                                    for (int j = 0; j < drugObj.length(); j++) {

                                        LayoutInflater inflater1 = LayoutInflater.from(getActivity());
                                        View patient_treatment_details1 = inflater1.inflate(R.layout.patientmedinfodrugsinflate, null);

                                        EditText drug = (EditText) patient_treatment_details1.findViewById(R.id.ctmdrugedit);
                                        EditText dosage = (EditText) patient_treatment_details1.findViewById(R.id.ctmdrugdosage);
                                        TextView drugdate = (TextView) patient_treatment_details1.findViewById(R.id.ctmdrugdate);
                                        EditText duration = (EditText) patient_treatment_details1.findViewById(R.id.ctmdrugduration);
                                        EditText quantity = (EditText) patient_treatment_details1.findViewById(R.id.ctmdrugquantity);
                                        final ImageView iv_monday = (ImageView) patient_treatment_details1.findViewById(R.id.ivdays1_monday);
                                        final ImageView iv_tuesday = (ImageView) patient_treatment_details1.findViewById(R.id.ivdays1_tuesday);
                                        final ImageView iv_wed = (ImageView) patient_treatment_details1.findViewById(R.id.ivdays1_wed);
                                        final ImageView iv_thu = (ImageView) patient_treatment_details1.findViewById(R.id.ivdays1_thu);
                                        final ImageView iv_fri = (ImageView) patient_treatment_details1.findViewById(R.id.ivdays1_fri);
                                        final ImageView iv_sat = (ImageView) patient_treatment_details1.findViewById(R.id.ivdays1_sat);
                                        final ImageView iv_sun = (ImageView) patient_treatment_details1.findViewById(R.id.ivdays1_sun);
                                        //final CheckBox	daily=(CheckBox)patient_treatment_details1.findViewById(R.id.ctmdrugdaily);


                                        lldrugs.addView(patient_treatment_details1);

                                        try {
                                            drug.setText(drugObj.getString(j));
                                            dosage.setText(dosageObj.getString(j));
                                            duration.setText(durationObj.getString(j));

                                            String drug_Quantity = QuantityObj.getString(j).replaceAll("\\(.*?\\)", "");

                                            quantity.setText(drug_Quantity);
                                            drugdate.setText(dateObj.getString(j));

                                            if (daysObj != null) {
                                                if (daysObj.length() != 0) {

                                                    if (daysObj.getString(j).equals("ALL")) {
                                                        System.out.println("workinggggggggggg");
                                                        //daily.setChecked(true);
                                                        iv_monday.setImageResource(R.drawable.days_monday_2);
                                                        iv_tuesday.setImageResource(R.drawable.days_tuesday_2);
                                                        iv_wed.setImageResource(R.drawable.days_wed_2);
                                                        iv_thu.setImageResource(R.drawable.days_thu_2);
                                                        iv_fri.setImageResource(R.drawable.days_fri_2);
                                                        iv_sat.setImageResource(R.drawable.days_sat_2);
                                                        iv_sun.setImageResource(R.drawable.days_sun_2);

                                                    } else {
                                                        String var = daysObj.getString(j);
                                                        System.out.println("variableeeee" + var);
                                                        System.out.println("length" + var.length());
                                                        //days=new String[var.length()];
                                                        days = var.split(",");
                                                        for (int k = 0; k < days.length; k++) {

                                                            System.out.println("days" + days);
                                                            System.out.println("days valueeeeeeeeee" + days[k]);

                                                            if (days[k].equals("Mo")) {
                                                                iv_monday.setImageResource(R.drawable.days_monday_2);
                                                            }
                                                            if (days[k].equals("Tu")) {
                                                                iv_tuesday.setImageResource(R.drawable.days_tuesday_2);
                                                            }
                                                            if (days[k].equals("We")) {
                                                                iv_wed.setImageResource(R.drawable.days_wed_2);
                                                            }
                                                            if (days[k].equals("Th")) {
                                                                iv_thu.setImageResource(R.drawable.days_thu_2);
                                                            }
                                                            if (days[k].equals("Fr")) {
                                                                iv_fri.setImageResource(R.drawable.days_fri_2);
                                                            }
                                                            if (days[k].equals("Sa")) {
                                                                iv_sat.setImageResource(R.drawable.days_sat_2);
                                                            }
                                                            if (days[k].equals("Su")) {
                                                                iv_sun.setImageResource(R.drawable.days_sun_2);
                                                            }
                                                        }


                                                    }

                                                    drug.setEnabled(false);
                                                    dosage.setEnabled(false);
                                                    drugdate.setEnabled(false);
                                                    duration.setEnabled(false);
                                                    //daily.setEnabled(false);
                                                    quantity.setEnabled(false);
                                                } else {

                                                    drug.setVisibility(View.GONE);
                                                    dosage.setVisibility(View.GONE);
                                                    drugdate.setVisibility(View.GONE);
                                                    duration.setVisibility(View.GONE);
                                                    //	daily.setVisibility(View.GONE);
                                                    quantity.setVisibility(View.GONE);

                                                }
                                            }


                                        } catch (JSONException ex) {
                                            // TODO Auto-generated catch block
                                            ex.printStackTrace();
                                        }

                                    }
                                } else {
                                    prescription.setVisibility(patient_info.INVISIBLE);
                                }
                            }
                        }
                    } else {
                        mProgressDialog.dismiss();
                        Toast.makeText(getActivity(), "Please Save Current Vitals,Diagnosis and Prescription Details", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    mProgressDialog.dismiss();
                    Toast.makeText(getActivity(), "Invalid Patient", Toast.LENGTH_SHORT).show();
                }

            }
        });
    }


    private void getpatientdetailsgynacologist() {
        // TODO Auto-generated method stub

        final ProgressDialog ProgressDialog = new ProgressDialog(getActivity());
        ProgressDialog.setMessage("Fetching Details....");
        ProgressDialog.show();
        ProgressDialog.setCancelable(false);

        final ParseQuery<ParseObject> query = ParseQuery.getQuery("Diagnosis");
        if (!AppUtil.haveNetwokConnection(getActivity()))
            query.fromLocalDatastore();
        query.whereEqualTo("patientID", patientIdvalue);
        query.addDescendingOrder("createdDate");
        ProgressDialog.setOnCancelListener(new OnCancelListener() {

            @Override
            public void onCancel(DialogInterface dialog) {
                // TODO Auto-generated method stub
                query.cancel();
            }
        });
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                // TODO Auto-generated method stub
                ProgressDialog.dismiss();
                final List<ParseObject> objectslist = new ArrayList<ParseObject>();
                Log.d("GYNEC", "Yes" + objects.size());
                Log.e("Login", "done");
                if (e == null) {
                    if (objects.size() > 0) {
                        System.out.println("objects size" + objects.size());

                        ArrayList<Integer> listed = new ArrayList<Integer>();
                        listed.clear();

                        for (int i = 0; i < objects.size(); i++) {
                            String formattedDate = objects.get(i).getString("createdDate");
                            for (int j = i; j < objects.size(); j++) {
                                System.out.println("working count" + j);
                                String formattedDate1 = objects.get(j).getString("createdDate");
                                if ((objects.get(i).getDouble("typeFlag") == 1 && objects.get(j).getDouble("typeFlag") == 2) ||
                                        (objects.get(i).getDouble("typeFlag") == 2 && objects.get(j).getDouble("typeFlag") == 1)) {
                                    System.out.println("formattedDate" + formattedDate);
                                    System.out.println("formattedDate1" + formattedDate1);

                                    System.out.println("diagnosiid" + objects.get(i).getString("diagnosisId"));
                                    System.out.println("diagnosiid" + objects.get(j).getString("diagnosisId"));

                                    String diagnosisId_diagnosis = objects.get(i).getString("diagnosisId");
                                    String diagnosisId_prescription = objects.get(j).getString("diagnosisId");

                                    //&& objects.get(i).getString("diagnosisId").equals(objects.get(j).getString("diagnosisId"))

                                    if (diagnosisId_diagnosis != null && diagnosisId_diagnosis != "" && diagnosisId_prescription != null && diagnosisId_prescription != "") {
                                        if (formattedDate.equals(formattedDate1) && diagnosisId_diagnosis.equals(diagnosisId_prescription)) {
                                            System.out.println();
                                            ParseObject combinedobject = new ParseObject("");
                                            System.out.println("iiiiii" + i + "jjjjj" + j);
                                            System.out.println("workinggggggggg1");
                                            System.out.println("formated date" + formattedDate);
                                            String days;
                                            if (objects.get(i).getJSONArray("days") != null) {
                                                combinedobject.put("days", objects.get(i).getJSONArray("days"));
                                            } else if (objects.get(j).getJSONArray("days") != null) {
                                                combinedobject.put("days", objects.get(j).getJSONArray("days"));
                                            }
                                            if (objects.get(i).getJSONArray("drugStartDate") != null) {
                                                combinedobject.put("drugStartDate", objects.get(i).getJSONArray("drugStartDate"));
                                            } else if (objects.get(j).getJSONArray("drugStartDate") != null) {
                                                combinedobject.put("drugStartDate", objects.get(j).getJSONArray("drugStartDate"));
                                            }
                                            if (objects.get(i).getJSONArray("dosage") != null) {
                                                combinedobject.put("dosage", objects.get(i).getJSONArray("dosage"));
                                            } else if (objects.get(j).getJSONArray("dosage") != null) {
                                                combinedobject.put("dosage", objects.get(j).getJSONArray("dosage"));
                                            }
                                            if (objects.get(i).getJSONArray("drug") != null) {
                                                combinedobject.put("drug", objects.get(i).getJSONArray("drug"));
                                            } else if (objects.get(j).getJSONArray("drug") != null) {
                                                combinedobject.put("drug", objects.get(j).getJSONArray("drug"));
                                            }
                                            if (objects.get(i).getJSONArray("duration") != null) {
                                                combinedobject.put("duration", objects.get(i).getJSONArray("duration"));
                                            } else if (objects.get(j).getJSONArray("duration") != null) {
                                                combinedobject.put("duration", objects.get(j).getJSONArray("duration"));
                                            }
                                            if (objects.get(i).getJSONArray("quantity") != null) {
                                                combinedobject.put("quantity", objects.get(i).getJSONArray("quantity"));
                                            } else if (objects.get(j).getJSONArray("quantity") != null) {
                                                combinedobject.put("quantity", objects.get(j).getJSONArray("quantity"));
                                            }
                                            combinedobject.put("createdAt", formattedDate);
                                            if (objects.get(j).getString("additionalComments") != null) {
                                                combinedobject.put("additionalComments", objects.get(j).getString("additionalComments"));
                                            } else if (objects.get(i).getString("additionalComments") != null) {
                                                combinedobject.put("additionalComments", objects.get(i).getString("additionalComments"));
                                            }
                                            if (objects.get(j).getString("suspectedDisease") != null) {
                                                combinedobject.put("suspectedDisease", objects.get(j).getString("suspectedDisease"));
                                                combinedobject.put("diagnosisObjId", objects.get(j).getString("diagnosisId"));
                                            } else if (objects.get(i).getString("suspectedDisease") != null) {
                                                combinedobject.put("suspectedDisease", objects.get(i).getString("suspectedDisease"));
                                                combinedobject.put("diagnosisObjId", objects.get(i).getString("diagnosisId"));
                                            }
                                            if (objects.get(j).getString("symptoms") != null) {
                                                combinedobject.put("symptoms", objects.get(j).getString("symptoms"));
                                            } else if (objects.get(i).getString("symptoms") != null) {
                                                combinedobject.put("symptoms", objects.get(i).getString("symptoms"));
                                            }
                                            if (objects.get(j).getString("syndromes") != null) {
                                                combinedobject.put("syndromes", objects.get(j).getString("syndromes"));
                                            } else if (objects.get(i).getString("syndromes") != null) {
                                                combinedobject.put("syndromes", objects.get(i).getString("syndromes"));
                                            }

                                            if (objects.get(j).getString("weight") != null) {
                                                combinedobject.put("weight", objects.get(j).getString("weight"));
                                            } else if (objects.get(i).getString("weight") != null) {
                                                combinedobject.put("weight", objects.get(i).getString("weight"));
                                            }

                                            if (objects.get(j).getString("spo2") != null) {
                                                combinedobject.put("spo2", objects.get(j).getString("spo2"));
                                            } else if (objects.get(i).getString("spo2") != null) {
                                                combinedobject.put("spo2", objects.get(i).getString("spo2"));
                                            }

                                            if (objects.get(j).getString("pulse") != null) {
                                                combinedobject.put("pulse", objects.get(j).getString("pulse"));
                                            } else if (objects.get(i).getString("pulse") != null) {
                                                combinedobject.put("pulse", objects.get(i).getString("pulse"));
                                            }

                                            if (objects.get(j).getString("respiratoryrate") != null) {
                                                combinedobject.put("respiratoryrate", objects.get(j).getString("respiratoryrate"));
                                            } else if (objects.get(i).getString("respiratoryrate") != null) {
                                                combinedobject.put("respiratoryrate", objects.get(i).getString("respiratoryrate"));
                                            }

                                            if (objects.get(j).getString("bloodgroup") != null) {
                                                combinedobject.put("bloodgroup", objects.get(j).getString("bloodgroup"));
                                            } else if (objects.get(i).getString("bloodgroup") != null) {
                                                combinedobject.put("bloodgroup", objects.get(i).getString("bloodgroup"));
                                            }

                                            if (objects.get(j).getString("bloodpressure") != null) {
                                                combinedobject.put("bloodpressure", objects.get(j).getString("bloodpressure"));
                                            } else if (objects.get(i).getString("bloodpressure") != null) {
                                                combinedobject.put("bloodpressure", objects.get(i).getString("bloodpressure"));
                                            }

                                            if (objects.get(j).getString("temperature") != null) {
                                                combinedobject.put("temperature", objects.get(j).getString("temperature"));
                                            } else if (objects.get(i).getString("temperature") != null) {
                                                combinedobject.put("temperature", objects.get(i).getString("temperature"));
                                            }


                                            if (objects.get(j).getString("height") != null) {
                                                combinedobject.put("height", objects.get(j).getString("height"));
                                            } else if (objects.get(i).getString("height") != null) {
                                                combinedobject.put("height", objects.get(i).getString("height"));
                                            }


                                            System.out.println("combied project" + combinedobject);
                                            objectslist.add(combinedobject);

                                            listed.add(i);
                                            listed.add(j);
                                            System.out.println("listed size" + listed.size());
                                        }
                                    }
                                }
                            }


                            if (!(listed.contains(i))) {
                                if (objects.get(i).getDouble("typeFlag") == 1) {
                                    ParseObject combinedobject = new ParseObject("");
                                    System.out.println("i valueeeeeee" + i);

                                    if (objects.get(i).getString("additionalComments") != null) {
                                        combinedobject.put("additionalComments", objects.get(i).getString("additionalComments"));
                                    } else {
                                        combinedobject.put("additionalComments", "N/A");
                                    }
                                    if (objects.get(i).getString("suspectedDisease") != null) {
                                        combinedobject.put("diagnosisObjId", objects.get(i).getString("diagnosisId"));
                                        combinedobject.put("suspectedDisease", objects.get(i).getString("suspectedDisease"));
                                    } else {
                                        combinedobject.put("suspectedDisease", "N/A");
                                    }
                                    if (objects.get(i).getString("symptoms") != null) {
                                        combinedobject.put("symptoms", objects.get(i).getString("symptoms"));
                                    } else {
                                        combinedobject.put("symptoms", "N/A");
                                    }
                                    if (objects.get(i).getString("syndromes") != null) {
                                        combinedobject.put("syndromes", objects.get(i).getString("syndromes"));
                                    } else {
                                        combinedobject.put("syndromes", "N/A");
                                    }
                                    combinedobject.put("createdAt", formattedDate);


                                    if (objects.get(i).getString("weight") != null) {
                                        combinedobject.put("weight", objects.get(i).getString("weight"));
                                    }
                                    if (objects.get(i).getString("spo2") != null) {
                                        combinedobject.put("spo2", objects.get(i).getString("spo2"));
                                    }
                                    if (objects.get(i).getString("pulse") != null) {
                                        combinedobject.put("pulse", objects.get(i).getString("pulse"));
                                    }

                                    if (objects.get(i).getString("respiratoryrate") != null) {
                                        combinedobject.put("respiratoryrate", objects.get(i).getString("respiratoryrate"));
                                    }

                                    if (objects.get(i).getString("bloodgroup") != null) {
                                        combinedobject.put("bloodgroup", objects.get(i).getString("bloodgroup"));
                                    }

                                    if (objects.get(i).getString("bloodpressure") != null) {
                                        combinedobject.put("bloodpressure", objects.get(i).getString("bloodpressure"));
                                    }

                                    if (objects.get(i).getString("temperature") != null) {
                                        combinedobject.put("temperature", objects.get(i).getString("temperature"));
                                    }

                                    if (objects.get(i).getString("height") != null) {
                                        combinedobject.put("height", objects.get(i).getString("height"));
                                    }


                                    objectslist.add(combinedobject);
                                }
                            }
                        }
                        listed.clear();

                        //mProgressDialog.dismiss();
                        //new code
                        for (int i = 0; i < objectslist.size(); i++) {
                            /*if(i==0){
                                mProgressDialog.dismiss();
							}*/
                            if (PatientPreviousTreatment.this.isVisible()) {

                                LayoutInflater inflater = LayoutInflater.from(getActivity());
                                View patient_info = inflater.inflate(R.layout.patientmedifolistitems, null);

                                LinearLayout datecontaier = (LinearLayout) patient_info.findViewById(R.id.lldatecontainer);
                                final LinearLayout detailscontainer = (LinearLayout) patient_info.findViewById(R.id.llpatient_details_container);

                                final ImageView arrow = (ImageView) patient_info.findViewById(R.id.ivarrow);
                                arrow.setBackgroundResource(R.drawable.triangle_arrow);
                                TextView tvdate = (TextView) patient_info.findViewById(R.id.cdcurrdate);

                                System.out.println("position" + i);
                                formattedDate = objectslist.get(i).getString("createdAt");
                                System.out.println("dateeeeeeeee" + objectslist.get(i).getString("createdAt"));
                                tvdate.setText(formattedDate);
                                /*if(i==objectslist.size()-1){
									mProgressDialog.dismiss();
								}*/


                                final LinearLayout prescription = (LinearLayout) patient_info.findViewById(R.id.ctml3);
                                final EditText metSymptoms = (EditText) patient_info.findViewById(R.id.etsymptoms);
                                final EditText metSyndromes = (EditText) patient_info.findViewById(R.id.etsyndromes);
                                final EditText metComments = (EditText) patient_info.findViewById(R.id.etcomments);
                                final EditText metSuspected = (EditText) patient_info.findViewById(R.id.etsuspecteddisease);


                                final TextView metWeight = (TextView) patient_info.findViewById(R.id.etweight);
                                final TextView metBloodPresure = (TextView) patient_info.findViewById(R.id.etbloodpressure);
                                final TextView metPulse = (TextView) patient_info.findViewById(R.id.etpulse);
                                final TextView metRespiratory = (TextView) patient_info.findViewById(R.id.etrespiratory);
                                final TextView metSpo2 = (TextView) patient_info.findViewById(R.id.etsp);
                                final TextView metTemperature = (TextView) patient_info.findViewById(R.id.ettemperature);
                                final LinearLayout lldrugs = (LinearLayout) patient_info.findViewById(R.id.lldrugscontainer);
                                final int position = i;
                                llmedhistcontainer.addView(patient_info);
                                datecontaier.setOnClickListener(new OnClickListener() {

                                    @Override
                                    public void onClick(View arg0) {
                                        // TODO Auto-generated method stub
                                        if (detailscontainer.getVisibility() == View.VISIBLE) {
                                            detailscontainer.setVisibility(View.GONE);
                                            arrow.setBackgroundResource(R.drawable.triangle_arrow);

                                        } else {
                                            arrow.setBackgroundResource(R.drawable.triangle_up);
                                            detailscontainer.setVisibility(View.VISIBLE);


                                            if (objectslist.get(position).getString("suspectedDisease").length() != 0) {

                                                String[] value = (objectslist.get(position).getString("suspectedDisease").split(","));
                                                ArrayList<String> diseasenamecheck = new ArrayList<String>();

                                                for (int val = 0; val < value.length; val++) {

                                                    diseasenamecheck.add(value[val]);

                                                }

                                                if (_suspecteddiseasenamesObj.size() != 0 && _suspecteddiseasenamesObj != null) {

                                                    compairing_code_taken_disease_fromparse(objectslist.get(position).getString("suspectedDisease"), metSuspected);


                                                    String[] valuedisease = objectslist.get(position).getString("suspectedDisease").split(",");
                                                    if (valuedisease != null)
                                                        for (int k = 0; k < valuedisease.length; k++) {

                                                            System.out.println("code valuessss from parse" + valuedisease[k]);
                                                        }
                                                    String[] myaccrArray = null;
                                                    if (valuedisease != null) {
                                                        myaccrArray = new String[valuedisease.length];
                                                        for (int j = 0; j < valuedisease.length; j++) {
                                                            for (int k = 0; k < diseasenamearraylist.size(); k++) {
                                                                if ((diseasenamearraylist.get(k).getIcdnumber()) != null) {
                                                                    String icd_from_list = diseasenamearraylist.get(k).getIcdnumber().toString().trim();
                                                                    String icd_from_selected = valuedisease[j].toString().trim();


                                                                    if (icd_from_selected.equalsIgnoreCase("other")) {
                                                                        myaccrArray[j] = "Other";
                                                                    } else if (icd_from_selected.equalsIgnoreCase("none")) {
                                                                        myaccrArray[j] = "None";
                                                                    }

                                                                    if (!icd_from_selected.equalsIgnoreCase("other") && !icd_from_selected.equalsIgnoreCase("none")) {
                                                                        if (icd_from_list.equalsIgnoreCase(icd_from_selected)) {
                                                                            myaccrArray[j] = diseasenamearraylist.get(k).getDiseasename();
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                            String disease = "";
                                                            for (int i = 0; i < myaccrArray.length; i++) {

                                                                disease += myaccrArray[i] + "\n";
                                                            }

                                                            metSuspected.setText(disease);
                                                            //metSuspected.put("suspectedDisease",disease);
                                                        }
                                                    }


                                                } else {

                                                    mProgress = new ProgressDialog(getActivity());
                                                    mProgress.setMessage("Fetching Details.....");
                                                    mProgress.show();
                                                    ParseQuery<ParseObject> query = ParseQuery.getQuery("DiseaseDatabase");
                                                    if (!AppUtil.haveNetwokConnection(getActivity()))
                                                        query.fromLocalDatastore();
                                                    query.whereContainedIn("ICDNumber", diseasenamecheck);
                                                    query.findInBackground(new FindCallback<ParseObject>() {

                                                        @Override
                                                        public void done(List<ParseObject> object, ParseException e) {
                                                            // TODO Auto-generated method stub
                                                            mProgress.dismiss();
                                                            if (e == null) {
                                                                if (object.size() > 0) {
                                                                    String disease = "";
                                                                    for (int diseasesize = 0; diseasesize < object.size(); diseasesize++) {
                                                                        System.out.println("names disease" + object.get(diseasesize).getString("DiseaseName"));
                                                                        disease += object.get(diseasesize).getString("DiseaseName") + "\n";
                                                                    }
                                                                    metSuspected.setText(disease);
                                                                }
                                                            }

                                                        }
                                                    });
                                                }

                                            } else {
                                                metSuspected.setText("Nil");
                                            }


                                        }
                                    }
                                });

                                if (objectslist.get(position).getString("symptoms").length() != 0) {
                                    metSymptoms.setText(objectslist.get(position).getString("symptoms"));
                                } else {
                                    metSymptoms.setText("Nil");
                                }
                                if (objectslist.get(position).getString("syndromes").length() != 0) {
                                    metSyndromes.setText(objectslist.get(position).getString("syndromes"));
                                } else {
                                    metSyndromes.setText("Nil");
                                }


                                if (objectslist.get(position).getString("additionalComments").length() != 0) {
                                    metComments.setText(objectslist.get(position).getString("additionalComments"));
                                } else {
                                    metComments.setText("Nil");
                                }





								/*if(patientobject!=null){*/
                                String weight_result = null;
                                String spo2_result = null;
                                String pulse_result = null;
                                String respRate_result = null;
                                String height_result = null;
                                String bloodGroup_result = null;
                                String bloodPressure_result = null;
                                String temperature = null;
                                if (objectslist.get(position).getString("weight") != null) {
                                    weight_result = objectslist.get(position).getString("weight");
                                }
                                if (objectslist.get(position).getString("spo2") != null) {
                                    spo2_result = objectslist.get(position).getString("spo2");
                                }
                                if (objectslist.get(position).getString("pulse") != null) {
                                    pulse_result = objectslist.get(position).getString("pulse");
                                }
                                if (objectslist.get(position).getString("respiratoryrate") != null) {
                                    respRate_result = objectslist.get(position).getString("respiratoryrate");
                                }
                                if (objectslist.get(position).getString("height") != null) {
                                    height_result = objectslist.get(position).getString("height");
                                }
                                if (objectslist.get(position).getString("bloodGroup") != null) {
                                    bloodGroup_result = objectslist.get(position).getString("bloodGroup");
                                }
                                if (objectslist.get(position).getString("bloodpressure") != null) {
                                    bloodPressure_result = objectslist.get(position).getString("bloodpressure");
                                }
                                if (objectslist.get(position).getString("temperature") != null) {
                                    temperature = objectslist.get(position).getString("temperature");
                                }
								/*JSONArray objectid=patientcurrentvitalsdetails.getString("diagnosisObjectid");
									ArrayList<String> object=new ArrayList<String>();
									if(patientcurrentvitalsdetails.getJSONArray("diagnosisObjectid")!=null){
										for(int k=0;k<objectid.length();k++){
											try {
												object.add(objectid.getString(k));
											} catch (JSONException e1) {
												// TODO Auto-generated catch block
												e1.printStackTrace();
											}
										}
									}*/




								/*if(object.contains(objectslist.get(position).getString("diagnosisObjId"))){*/
                                //System.out.println("object position"+object.indexOf(objectslist.get(position).getString("diagnosisObjId")));
                                //int index=object.indexOf(objectslist.get(position).getString("diagnosisObjId"));
								/*try {*/
                                //&&!(weight_result.getString(index).equals("Nil"))
                                if (weight_result != null) {
                                    metWeight.setText(weight_result + " " + "kg");
                                } else {
                                    metWeight.setText("-" + " " + "kg");
                                }
                                //&&  !(bloodPressure_result.getString(index).equals("Nil"))
                                if (bloodPressure_result != null) {
                                    metBloodPresure.setText(bloodPressure_result + " " + "Hg mm");
                                } else {
                                    metBloodPresure.setText("-" + " " + "Hg mm");
                                }
                                // && !(spo2_result.getString(index).equals("Nil"))
                                if (spo2_result != null) {
                                    metSpo2.setText(spo2_result + " " + "%");
                                } else {
                                    metSpo2.setText("-" + " " + "%");
                                }
                                // && !(temperature.getString(index).equals("Nil"))
                                if (temperature != null) {
                                    metTemperature.setText(temperature + " " + "c");
                                } else {
                                    metTemperature.setText("-" + " " + "c");
                                }
                                //&& !(pulse_result.getString(index).equals("Nil"))
                                if (pulse_result != null) {
                                    metPulse.setText(pulse_result + " " + "beats/min");
                                } else {
                                    metPulse.setText("-" + " " + "beats/min");
                                }
                                //&& !(respRate_result.getString(index).equals("Nil"))
                                if (respRate_result != null) {
                                    metRespiratory.setText(respRate_result + " " + "breaths/min");
                                } else {
                                    metRespiratory.setText("-" + " " + "breaths/min");
                                }
								/*} catch (JSONException e1) {
											// TODO Auto-generated catch block
											e1.printStackTrace();
										}*/

								/*	}else{
										metWeight.setText("-"+" "+"kg");
										metBloodPresure.setText("-"+" "+"Hg mm");
										metSpo2.setText("-"+" "+"%");
										metTemperature.setText("-"+" "+"c");
										metPulse.setText("-"+" "+"beats/min");
										metRespiratory.setText("-"+" "+"breaths/min");
									}*/

                                metSymptoms.setEnabled(false);
                                metSyndromes.setEnabled(false);
                                metSymptoms.setEnabled(false);
                                metSuspected.setEnabled(false);
                                metComments.setEnabled(false);
                                metWeight.setEnabled(false);
                                metBloodPresure.setEnabled(false);
                                metSpo2.setEnabled(false);
                                metTemperature.setEnabled(false);
                                metPulse.setEnabled(false);
                                metRespiratory.setEnabled(false);

                                metSuspected.measure(0, 0);
                                metComments.measure(0, 0);

                                int susheight = metSuspected.getMeasuredHeight();
                                int comheight = metComments.getMeasuredHeight();
                                int symptheight = metSymptoms.getMeasuredHeight();
                                int syndheight = metSyndromes.getMeasuredHeight();
                                System.out.println("metSuspected height" + metSuspected.getMeasuredHeight());
                                System.out.println("metComments height" + metComments.getMeasuredHeight());

                                if (symptheight > syndheight) {
                                    metSyndromes.setHeight(symptheight);
                                }
                                if (symptheight < syndheight) {
                                    metSymptoms.setHeight(syndheight);
                                }
                                if (susheight > comheight) {
                                    metComments.setHeight(susheight);
                                }
                                if (susheight < comheight) {
                                    metSuspected.setHeight(comheight);
                                }
								/*		}else{
									metWeight.setText("-"+" "+"kg");
									metBloodPresure.setText("-"+" "+"Hg mm");
									metSpo2.setText("-"+" "+"%");
									metTemperature.setText("-"+" "+"c");
									metPulse.setText("-"+" "+"beats/min");
									metRespiratory.setText("-"+" "+"breaths/min");
								}*/
                                System.out.println("successssssssssss" + position);
                                JSONArray drugObj = null;
                                JSONArray dosageObj = null;
                                JSONArray dateObj = null;
                                JSONArray durationObj = null;
                                JSONArray QuantityObj = null;
                                JSONArray daysObj = null;
                                if (objectslist.get(position).getJSONArray("drug") != null) {
                                    drugObj = objectslist.get(position).getJSONArray("drug");
                                }
                                if (objectslist.get(position).getJSONArray("dosage") != null) {
                                    dosageObj = objectslist.get(position).getJSONArray("dosage");
                                }
                                if (objectslist.get(position).getJSONArray("drugStartDate") != null) {
                                    dateObj = objectslist.get(position).getJSONArray("drugStartDate");
                                }
                                if (objectslist.get(position).getJSONArray("duration") != null) {
                                    durationObj = objectslist.get(position).getJSONArray("duration");
                                }

                                if (objectslist.get(position).getJSONArray("quantity") != null) {
                                    QuantityObj = objectslist.get(position).getJSONArray("quantity");
                                }
                                if (objectslist.get(position).getJSONArray("days") != null) {
                                    daysObj = objectslist.get(position).getJSONArray("days");
                                }

                                if (drugObj != null) {
                                    System.out.println("drugObj length" + drugObj.length());

                                    for (int j = 0; j < drugObj.length(); j++) {

                                        LayoutInflater inflater1 = LayoutInflater.from(getActivity());
                                        View patient_treatment_details1 = inflater1.inflate(R.layout.patientmedinfodrugsinflate, null);

                                        EditText drug = (EditText) patient_treatment_details1.findViewById(R.id.ctmdrugedit);
                                        EditText dosage = (EditText) patient_treatment_details1.findViewById(R.id.ctmdrugdosage);
                                        TextView drugdate = (TextView) patient_treatment_details1.findViewById(R.id.ctmdrugdate);
                                        EditText duration = (EditText) patient_treatment_details1.findViewById(R.id.ctmdrugduration);
                                        EditText quantity = (EditText) patient_treatment_details1.findViewById(R.id.ctmdrugquantity);
                                        final ImageView iv_monday = (ImageView) patient_treatment_details1.findViewById(R.id.ivdays1_monday);
                                        final ImageView iv_tuesday = (ImageView) patient_treatment_details1.findViewById(R.id.ivdays1_tuesday);
                                        final ImageView iv_wed = (ImageView) patient_treatment_details1.findViewById(R.id.ivdays1_wed);
                                        final ImageView iv_thu = (ImageView) patient_treatment_details1.findViewById(R.id.ivdays1_thu);
                                        final ImageView iv_fri = (ImageView) patient_treatment_details1.findViewById(R.id.ivdays1_fri);
                                        final ImageView iv_sat = (ImageView) patient_treatment_details1.findViewById(R.id.ivdays1_sat);
                                        final ImageView iv_sun = (ImageView) patient_treatment_details1.findViewById(R.id.ivdays1_sun);
                                        //final CheckBox	daily=(CheckBox)patient_treatment_details1.findViewById(R.id.ctmdrugdaily);


                                        lldrugs.addView(patient_treatment_details1);

                                        try {
                                            drug.setText(drugObj.getString(j));
                                            dosage.setText(dosageObj.getString(j));
                                            duration.setText(durationObj.getString(j));

                                            String drug_Quantity = QuantityObj.getString(j).replaceAll("\\(.*?\\)", "");

                                            quantity.setText(drug_Quantity);
                                            drugdate.setText(dateObj.getString(j));

                                            if (daysObj != null) {
                                                if (daysObj.length() != 0) {

                                                    if (daysObj.getString(j).equals("ALL")) {
                                                        System.out.println("workinggggggggggg");
                                                        //daily.setChecked(true);
                                                        iv_monday.setImageResource(R.drawable.days_monday_2);
                                                        iv_tuesday.setImageResource(R.drawable.days_tuesday_2);
                                                        iv_wed.setImageResource(R.drawable.days_wed_2);
                                                        iv_thu.setImageResource(R.drawable.days_thu_2);
                                                        iv_fri.setImageResource(R.drawable.days_fri_2);
                                                        iv_sat.setImageResource(R.drawable.days_sat_2);
                                                        iv_sun.setImageResource(R.drawable.days_sun_2);

                                                    } else {
                                                        String var = daysObj.getString(j);
                                                        System.out.println("variableeeee" + var);
                                                        System.out.println("length" + var.length());
                                                        //days=new String[var.length()];
                                                        days = var.split(",");
                                                        for (int k = 0; k < days.length; k++) {

                                                            System.out.println("days" + days);
                                                            System.out.println("days valueeeeeeeeee" + days[k]);

                                                            if (days[k].equals("Mo")) {
                                                                iv_monday.setImageResource(R.drawable.days_monday_2);
                                                            }
                                                            if (days[k].equals("Tu")) {
                                                                iv_tuesday.setImageResource(R.drawable.days_tuesday_2);
                                                            }
                                                            if (days[k].equals("We")) {
                                                                iv_wed.setImageResource(R.drawable.days_wed_2);
                                                            }
                                                            if (days[k].equals("Th")) {
                                                                iv_thu.setImageResource(R.drawable.days_thu_2);
                                                            }
                                                            if (days[k].equals("Fr")) {
                                                                iv_fri.setImageResource(R.drawable.days_fri_2);
                                                            }
                                                            if (days[k].equals("Sa")) {
                                                                iv_sat.setImageResource(R.drawable.days_sat_2);
                                                            }
                                                            if (days[k].equals("Su")) {
                                                                iv_sun.setImageResource(R.drawable.days_sun_2);
                                                            }
                                                        }


                                                    }

                                                    drug.setEnabled(false);
                                                    dosage.setEnabled(false);
                                                    drugdate.setEnabled(false);
                                                    duration.setEnabled(false);
                                                    //daily.setEnabled(false);
                                                    quantity.setEnabled(false);
                                                } else {

                                                    drug.setVisibility(View.GONE);
                                                    dosage.setVisibility(View.GONE);
                                                    drugdate.setVisibility(View.GONE);
                                                    duration.setVisibility(View.GONE);
                                                    //	daily.setVisibility(View.GONE);
                                                    quantity.setVisibility(View.GONE);

                                                }
                                            }


                                        } catch (JSONException ex) {
                                            // TODO Auto-generated catch block
                                            ex.printStackTrace();
                                        }

                                    }
                                } else {
                                    prescription.setVisibility(patient_info.INVISIBLE);
                                }
                            }
                        }
                    } else {
                        ProgressDialog.dismiss();
                        Toast.makeText(getActivity(), "Please Save Current Vitals,Diagnosis and Prescription Details", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    ProgressDialog.dismiss();
                    Toast.makeText(getActivity(), "Invalid Patient", Toast.LENGTH_SHORT).show();
                }

            }
        });
    }


    private void Setpatientidpatientname() {
        // TODO Auto-generated method stub
        mtvPatientid.setText(patientIdvalue);
        if (patientobject != null) {
            String fname;
            String lname;
            if (patientobject.getString("firstName") == null) {
                fname = "FirstName";
            } else {
                fname = patientobject.getString("firstName");
            }

            if (patientobject.getString("lastName") == null) {
                lname = "LastName";
            } else {
                lname = patientobject.getString("lastName");
            }

            mtvPatientname.setText(fname + " " + lname);
        }

    }

    private void Initialize_Components(View view) {
        // TODO Auto-generated method stub

        //lvpatientdetails=(ExpandableListView)view.findViewById(R.id.lvPatientitems);
        llmedhistcontainer = (LinearLayout) view.findViewById(R.id.llmedhistcontainer);
        mtvPatientid = (TextView) view.findViewById(R.id.cduniqueid);
        mtvPatientname = (TextView) view.findViewById(R.id.tvfirst_last_name);
    }

    @Override
    public void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
        CrashReporter.getInstance().trackScreenView("Patient Current Medical information");

        if (patientobject != null) {

            String fname = patientobject.getString("firstName");
            String lname = patientobject.getString("lastName");
            System.out.println("name........" + fname);

        }
    }

    public void compairing_code_taken_disease_fromparse(String diseasename, EditText ettext) {
        String[] value = diseasename.split(",");
        if (value != null)
            for (int k = 0; k < value.length; k++) {

                System.out.println("code valuessss from parse" + value[k]);
            }
        String[] myaccrArray = null;
        if (value != null) {
            myaccrArray = new String[value.length];
            for (int j = 0; j < value.length; j++) {
                for (int k = 0; k < diseasenamearraylist.size(); k++) {
                    if ((diseasenamearraylist.get(k).getIcdnumber()) != null) {
                        String icd_from_list = diseasenamearraylist.get(k).getIcdnumber().toString().trim();
                        String icd_from_selected = value[j].toString().trim();
                        if (icd_from_list.equalsIgnoreCase(icd_from_selected)) {
                            myaccrArray[j] = diseasenamearraylist.get(k).getDiseasename();
                        }
                    }
                }
                String disease = "";
                for (int i = 0; i < myaccrArray.length; i++) {
                    disease += myaccrArray[i] + "\n";
                }

                ettext.setText(disease);

            }
        }

    }
    public void justifyListViewHeightBasedOnChildren(ListView listView) {

        ListAdapter adapter = listView.getAdapter();

        if (adapter == null) {
            return;
        }
        ViewGroup vg = listView;
        int totalHeight = 0;
        for (int i = 0; i < adapter.getCount(); i++) {
            View listItem = adapter.getView(i, null, vg);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }

        ViewGroup.LayoutParams par = listView.getLayoutParams();
        par.height = totalHeight + (listView.getDividerHeight() * (adapter.getCount() - 1));
        listView.setLayoutParams(par);
        listView.requestLayout();
    }
}